//--------------------------------------------------------------
//
// Riva ing. Franco M.
// Besana Brianza (MI)
//
//--------------------------------------------------------------
//
// PROJECT:	  RSR008 - Low Cost Collimator
// FILE:      sys_main.h
// REVISIONS: 0.00 - 22/09/2008 - First Definition
//
//--------------------------------------------------------------
#define MAX_MSG_SIZE 	21
#define MAX_QUEUE_SIZE  20
#define DISPLAY_RATE_W_MSG	1500
#define DISPLAY_RATE_WO_MSG	100

#define START_FIRST_LINE	0x00
#define END_FIRST_LINE		0x13
#define START_SECOND_LINE	0x20
#define END_SECOND_LINE		0x33
#define CLEAR_FIRST_LINE	0xF1
#define CLEAR_SECOND_LINE	0xF2
#define CLEAR_BOTH_LINES	0xF3
#define NO_OP				0xFF

#define DISPLAY_START				'D'
#define DISPLAY_SHOW_VERSION_RSR008	'8'
#define DISPLAY_SHOW_VERSION_ASR001	'1'
#define DISPLAY_SHOW_VERSION_ASR003	'3'
#define DISPLAY_SHOW_VERSION_IRIDE	'4'
#define DISPLAY_SHOW_VERSION_ROT	'5'
#define DISPLAY_SHOW_MAIN			'M'
#define DISPLAY_RUNNING				'R'
#define DISPLAY_HOLDING				'H'



#ifdef _SYS_DISPLAY_
	volatile uword g_uwDisplayManagerTimer=false;
	uchar g_ucActualDisplayType=DISPLAY_NONE;
	char cbNewDisplayString[8];
	uchar g_ucReqDisplayOperation;
	uchar s_ucDisplayQueueStatus=DISPLAY_START;
#else
	extern volatile uword g_uwDisplayManagerTimer;
	extern uchar g_ucActualDisplayType;
	extern cbNewDisplayString[8];
	extern uchar g_ucReqDisplayOperation;
	extern uchar s_ucDisplayQueueStatus;
#endif



uint16_t inches_to_cm(uint16_t n);
uint16_t inches_to_mm(uint16_t n);
uint16_t mm_to_inches (uint16_t n);
uint16_t cm_to_inches (uint16_t n);
uint16_t val_abs (int16_t u1, int16_t u2);

void sys_display_init(void);
void sys_display_version(uchar ucStep);
void sys_display_filter(UINT8 error);
void sys_display_update_screen(void);
void sys_display_clear_screen(void);
void sys_display_main_screen(void);
bool sys_display_queue_msg(uchar* pucMessage);
void sys_display_manager(void);

void scorrimento_filtro(void);
const char* ricava_pt_nome_filtro(UINT8 filtro);

