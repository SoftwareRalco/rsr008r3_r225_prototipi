#define _SYS_HIGH_LEVEL_

//--------------------------------------------------------------
// INCLUDE FILES
//--------------------------------------------------------------
#include "include.h"
#include "string.h"
#include "math.h"

//--------------------------------------------------------------
// GLOBAL VARIABLES
//--------------------------------------------------------------
static uword g_uwZeroDffCrossToBeSent=0;
static bool g_bCrossFormatModified=false;
static bool g_bCrossFormatNotAllowed=false;

static uword g_uwZeroDffLongToBeSent=0;
static bool g_bLongFormatModified=false;
static bool g_bLongFormatNotAllowed=false;

#define FILTER_INIT                 'I'
#define FILTER_CHECK_HOME           'C'
#define FILTER_HOME                 'H'
#define FILTER_START_MOVE           'S'
#define FILTER_WAIT_ZERO            'W'
#define FILTER_IS_HOME_REQ          'R'
#define FILTER_GO_TO_POS            'P'
#define FILTER_WAIT                 'G'
static uchar g_ucFilterStatus=FILTER_INIT;


#define LIGHT_INIT  0
#define LIGHT_HOME  1
#define LIGHT_ON    2


enum {
    LIGHT_PREP_OFF = 0,        // deve essere uguale a LIGHT_INIT
    LIGHT_OFF,
    LIGHT_PREP_ON_INFINITO,
    LIGHT_ON_INFINITO,
    LIGHT_PREP_TRIGGERED_TASTO,
    LIGHT_PREP_TRIGGERED_CAN,
    LIGHT_TRIGGERED
};
static uchar  g_ucLightControllerState=LIGHT_INIT;

static uword    g_uwTargetCrossMm=0;
static uword    g_uwTargetLongMm=0;


typedef struct{
    uword uwMm;
    uword uwStep[STEPS_DFF];
}StepDff;


enum {
    ST_FILTER_PREP_RESET = 0,
    ST_FILTER_WAIT_RESET,
    ST_FILTER_AFTER_RESET,
    ST_FILTER_PREP_IDLE,
    ST_FILTER_IDLE,
    ST_FILTER_WAIT_MOV,
    ST_FILTER_PREP_ERROR,
    ST_FILTER_WAIT_SBY,
    ST_FILTER_ERROR
};


uword g_uwCrossDffX=0xffff;
uword g_uwCrossDffY=0xffff;
uword g_uwLongDffX=0xffff;
uword g_uwLongDffY=0xffff;
uword g_uwIrisDffX=0xffff;
uword g_uwIrisDffY=0xffff;


//--------------------------------------------------------------
bool LinCrossMm(uword uwActualDff, uword uwSteps, uword* puwMm)
{
    UINT32 format, temp32;
    double fl;

    //Limits check
/*
    if ((uwActualDff < DFF_MIN) || (uwActualDff > DFF_MAX))
    {
        if (uwActualDff)
        {
            alarm_dff |= ALARM_DFF_CROSS;
        //Dff Error
        }
        *puwMm=0;
        return false;
    }
*/

 //   alarm_dff &= ~ALARM_DFF_CROSS;

    if (inclinazione_cross)
    {
        temp32 = (UINT32)uwActualDff * mult_incl_cross;     // mult_incl_.. e' in formato 24.8
        temp32 >>= 8;
        uwActualDff = (UINT16)temp32;
    }

    if (ee_ConfigColl.CorrType == TIPO_CORR_CM)
    {
        uwActualDff += (UINT16)ee_ConfigColl.scCorrCrossDff;
    }

    g_uwCrossDffX = uwActualDff;

    if (uwSteps == 0)   // se uguale a 0 torna con 0
    {
        *puwMm = 0;
    }
    else
    {
        if (ee_ConfigColl.OffsetZero == TRUE)
        {
        temp32 = ((OFFSET_CROSS * (g_MaxSteps[STEPPER_CROSS] - (UINT32)uwSteps)) / g_MaxSteps[STEPPER_CROSS]) + 1;
        temp32 += (UINT32)uwSteps;
        }
        else
        {
            temp32 = ((OFFSET_CROSS * (g_MaxSteps[STEPPER_CROSS] - (UINT32)uwSteps)) / g_MaxSteps[STEPPER_CROSS]);
            if ((UINT32)uwSteps >= temp32)
            {
                temp32 = (UINT32)uwSteps - temp32 + 1;
            }
            else
            {
                temp32 = 1;
            }
        }
        format = temp32 * (UINT32)uwActualDff * (UINT32)g_RefSize[STEPPER_CROSS];
        format /= (CAL_FIXED * (UINT32)g_RefSteps[STEPPER_CROSS]);
        if (ee_ConfigColl.CorrType == TIPO_CORR_PERC)
        {
            format = ((format * 1000UL) / (1000UL + (SINT32)ee_ConfigColl.scCorrCrossDff));
        }
        *puwMm = (UINT16)format;   // in mm
    }
    return true;
}




//***************************************************************
//  ritorna con numero di passi da fare calcolati da DFF e mm
//  se ritorna FALSE il formato non e' raggiungibile
//***************************************************************
bool LinCrossOverDff(uword uwActualDff, uword uwActualMm, uword* puwDffSteps)
{
    UINT8 flag;
    UINT32 passi, temp32;

    flag = TRUE;

    g_uwTargetCrossMm = uwActualMm;

    //Limits check
/*
    if ((uwActualDff < DFF_MIN) || (uwActualDff > DFF_MAX))
    {
        if (uwActualDff)
        {
            alarm_dff |= ALARM_DFF_CROSS;
        }
        //Dff Error
        *puwDffSteps=0;
        return false;
    }
*/

  //  alarm_dff &= ~ALARM_DFF_CROSS;

    if (inclinazione_cross)
    {
        temp32 = (UINT32)uwActualDff * mult_incl_cross;     // mult_incl_.. e' in formato 24.8
        temp32 >>= 8;
        uwActualDff = (UINT16)temp32;
    }

    if (ee_ConfigColl.CorrType == TIPO_CORR_CM)
    {
        uwActualDff += (UINT16)ee_ConfigColl.scCorrCrossDff;
    }

    g_uwCrossDffY = uwActualDff;

    passi = (UINT32)uwActualMm;
    if (ee_ConfigColl.CorrType == TIPO_CORR_PERC)
    {
        passi *= (1000UL + (SINT32)ee_ConfigColl.scCorrCrossDff);   // somma percentuale di correzione
        passi /= 1000UL;
    }
    passi *= (CAL_FIXED * (UINT32)g_RefSteps[STEPPER_CROSS]);
    passi /= ((UINT32)uwActualDff * (UINT32)g_RefSize[STEPPER_CROSS]);

    if (passi > g_MaxSteps[STEPPER_CROSS])
    {
        passi = g_MaxSteps[STEPPER_CROSS];
        flag = FALSE;       // segnala errore
    }

    if (ee_ConfigColl.OffsetZero == TRUE)
    {
    temp32 = (OFFSET_CROSS * (g_MaxSteps[STEPPER_CROSS] - passi)) / g_MaxSteps[STEPPER_CROSS];

    if (passi > temp32)
        passi = passi - temp32;
    else
        passi = 0;
    }
    else
    {
        temp32 = (OFFSET_CROSS * (g_MaxSteps[STEPPER_CROSS] - passi)) / g_MaxSteps[STEPPER_CROSS];
        passi += temp32;
    }

    *puwDffSteps = (UINT16)passi;
    
    return flag;

}


//--------------------------------------------------------------
bool LinLongMm(uword uwActualDff, uword uwSteps, uword* puwMm)
{
    UINT32 format, temp32;


    //Limits check
/*
    if ((uwActualDff < DFF_MIN) || (uwActualDff > DFF_MAX))
    {
        if (uwActualDff)
        {
            alarm_dff |= ALARM_DFF_LONG;
        //Dff Error
        }
        *puwMm=0;
        return false;
    }
*/
 //   alarm_dff &= ~ALARM_DFF_LONG;

    if (inclinazione_long)
    {
        temp32 = (UINT32)uwActualDff * mult_incl_long;     // mult_incl_.. e' in formato 24.8
        temp32 >>= 8;
        uwActualDff = (UINT16)temp32;
    }

    if (ee_ConfigColl.CorrType == TIPO_CORR_CM)
    {
        uwActualDff += (UINT16)ee_ConfigColl.scCorrLongDff;
    }

    g_uwLongDffX=uwActualDff;

    if (uwSteps == 0)   // se uguale a 0 torna con 0
    {
        *puwMm = 0;
    }
    else
    {
        if (ee_ConfigColl.OffsetZero == TRUE)
        {
        temp32 = ((OFFSET_LONG * (g_MaxSteps[STEPPER_LONG] - (UINT32)uwSteps)) / g_MaxSteps[STEPPER_LONG]) + 1;
        temp32 += (UINT32)uwSteps;
        }
        else
        {
            temp32 = ((OFFSET_LONG * (g_MaxSteps[STEPPER_LONG] - (UINT32)uwSteps)) / g_MaxSteps[STEPPER_LONG]);
            if ((UINT32)uwSteps >= temp32)
            {
                temp32 = (UINT32)uwSteps - temp32 + 1;
            }
            else
            {
                temp32 = 1;
            }
        }
        format = temp32 * (UINT32)uwActualDff * (UINT32)g_RefSize[STEPPER_LONG];
        format /= (CAL_FIXED * (UINT32)g_RefSteps[STEPPER_LONG]);
        if (ee_ConfigColl.CorrType == TIPO_CORR_PERC)
        {
            format = (format * 1000UL) / (1000UL + (SINT32)ee_ConfigColl.scCorrLongDff);
        }
        *puwMm = (UINT16)format;   // in mm
    }
    return true;
}



//***************************************************************
//  ritorna con numero di passi da fare calcolati da DFF e mm
//  se ritorna FALSE il formato non e' raggiungibile
//***************************************************************
bool LinLongOverDff(uword uwActualDff, uword uwActualMm, uword* puwDffSteps)
{
    UINT8 flag;
    UINT32 passi, temp32;

    flag = TRUE;

    g_uwTargetLongMm=uwActualMm;

    //Limits check
/*
    if (uwActualDff<DFF_MIN || uwActualDff>DFF_MAX)
    {
        if (uwActualDff)
        {
            alarm_dff |= ALARM_DFF_LONG;
        //Dff Error
        }
        *puwDffSteps=0;
        return false;
    }
*/
 //   alarm_dff &= ~ALARM_DFF_LONG;

    if (inclinazione_long)
    {
        temp32 = (UINT32)uwActualDff * mult_incl_long;     // mult_incl_.. e' in formato 24.8
        temp32 >>= 8;
        uwActualDff = (UINT16)temp32;
    }

    if (ee_ConfigColl.CorrType == TIPO_CORR_CM)
    {
        uwActualDff += (UINT16)ee_ConfigColl.scCorrLongDff;
    }

    g_uwLongDffY=uwActualDff;

    passi = (UINT32)uwActualMm;
    if (ee_ConfigColl.CorrType == TIPO_CORR_PERC)
    {
        passi *= (1000UL + (SINT32)ee_ConfigColl.scCorrLongDff);   // somma percentuale di correzione
        passi /= 1000UL;
    }
    passi *= (CAL_FIXED * (UINT32)g_RefSteps[STEPPER_LONG]);
    passi /= ((UINT32)uwActualDff * (UINT32)g_RefSize[STEPPER_LONG]);

    if (passi > g_MaxSteps[STEPPER_LONG])
    {
        passi = g_MaxSteps[STEPPER_LONG];
        flag = FALSE;       // segnala errore di formato
    }

    if (ee_ConfigColl.OffsetZero == TRUE)
    {
    temp32 = (OFFSET_LONG * (g_MaxSteps[STEPPER_LONG] - passi)) / g_MaxSteps[STEPPER_LONG];

    if (passi > temp32)
        passi = passi - temp32;
    else
        passi = 0;
    }
    else
    {
        temp32 = (OFFSET_LONG * (g_MaxSteps[STEPPER_LONG] - passi)) / g_MaxSteps[STEPPER_LONG];
        passi += temp32;
    }

    *puwDffSteps = (UINT16)passi;

    return flag;
}





//***********************************************************************
//  calcola la dimensione dell'iride dai passi del motore
//***********************************************************************
bool LinIrisMm(uword uwActualDff, uword uwSteps, uword* puwMm, UINT8 pot)
{
    UINT8 i;
    UINT32 A, B, C, D, E;

    //Limits check
/*
    if ((uwActualDff < DFF_MIN) || (uwActualDff > DFF_MAX))
    {
        //Dff Error
        *puwMm=0;
        return false;
    }
*/

    if (stato_iride == IRIDE_RESET)
        return FALSE;

    if (ee_ConfigColl.CorrType == TIPO_CORR_CM)
    {
        uwActualDff += (UINT16)ee_ConfigColl.scCorrIrideDff;
    }

    g_uwIrisDffX = uwActualDff;

    i = 0;
    while(((UINT32)uwSteps >= ee_IrisSteps.iride_calib_passi[i]) && (i < (IRIS_FORMAT_MAX + 1)))
        i++;

    if (pot == 0)
    {
        A = (UINT32)iride_calib_mm[i - 1];
        C = (UINT32)(iride_calib_mm[i] - iride_calib_mm[i - 1]);
    }
    else
    {
        A = (UINT32)ee_IrisSteps.iride_calib_pot[i - 1];
        C = (UINT32)(ee_IrisSteps.iride_calib_pot[i] - ee_IrisSteps.iride_calib_pot[i - 1]);
    }
    B = (UINT32)(uwSteps - ee_IrisSteps.iride_calib_passi[i - 1]);
    D = (UINT32)(ee_IrisSteps.iride_calib_passi[i] - ee_IrisSteps.iride_calib_passi[i - 1]);

//*********************************************
//         y2-y1                  Dff
//  Y = (-------- (x-x1) + y1) * --------
//         x2-x1                  Dff_Cal
//*********************************************

    E = B * C * 10UL;
    E /= D;
    if ((E % 10) >= 5)
        E += 10;
    E /= 10;
    E += A;
    if (pot == 0)
    {
        E *= (UINT32)uwActualDff;
        E /= CAL_FIXED;
        if (ee_ConfigColl.CorrType == TIPO_CORR_PERC)
        {
            E = ((E * 1000UL) / (1000UL + (SINT32)ee_ConfigColl.scCorrIrideDff));
        }
        *puwMm = (UINT16)E;   // in mm
    }
    else
    {
        pos_iride = (SINT16)E;  // valore teorico ADC
    }
    return true;

}



//***********************************************************************
//  calcola i passi da mandare all'iride per raggiungere una dimensione
//***********************************************************************
bool LinIrisOverDff(uword uwActualDff, uword uwActualMm, uword* puwDffSteps)
{
    UINT8 i;
    UINT32 A, B, C, D, E, temp32;

    //Limits check
/*
    if ((uwActualDff < DFF_MIN) || (uwActualDff > DFF_MAX))
    {
        if (uwActualDff)
        {
            alarm_dff |= ALARM_DFF_LONG;
        //Dff Error
        }
//        *puwDffSteps=0;
        *puwDffSteps = ee_IrisSteps.iride_calib_passi[IRIS_FORMAT_MAX + 1];
        return false;
    }
*/
    alarm_dff &= ~ALARM_DFF_IRIS;

    if (ee_ConfigColl.CorrType == TIPO_CORR_CM)
    {
        uwActualDff += (UINT16)ee_ConfigColl.scCorrIrideDff;
    }

    g_uwLongDffY=uwActualDff;

//    adatta i mm con ActualDff e CAL_FIXED

    temp32 = (UINT32)uwActualMm;
    if (ee_ConfigColl.CorrType == TIPO_CORR_PERC)       // corregge se necessario
    {
        temp32 *= (1000UL + (SINT32)ee_ConfigColl.scCorrIrideDff);   // percentuale di correzione
        temp32 /= 1000UL;
    }
    temp32 *= CAL_FIXED;
    temp32 /= (UINT32)uwActualDff;
    uwActualMm = (UINT16)temp32;
                // cerca la calibrazione corretta
    i = 0;
    while(((UINT32)uwActualMm >= iride_calib_mm[i]) && (i < (IRIS_FORMAT_MAX + 1)))
        i++;

    if (i == 0)
    {
        *puwDffSteps = 0;
        return TRUE;
    }

    A = (UINT32)ee_IrisSteps.iride_calib_passi[i - 1];
    C = (UINT32)(ee_IrisSteps.iride_calib_passi[i] - ee_IrisSteps.iride_calib_passi[i - 1]);
    B = (UINT32)(uwActualMm - iride_calib_mm[i - 1]);
    D = (UINT32)(iride_calib_mm[i] - iride_calib_mm[i - 1]);

//*********************************************
//         y2-y1                  Dff
//  Y = (-------- (x-x1) + y1) * --------
//         x2-x1                  Dff_Cal
//*********************************************

    E = B * C * 10UL;
    E /= D;
    if ((E % 10) >= 5)
        E += 10;
    E /= 10;
    E += A;
//    E *= CAL_FIXED;
//    E /= (UINT32)uwActualDff;

    if (E > ee_IrisSteps.iride_calib_passi[IRIS_FORMAT_MAX + 1])
        E = ee_IrisSteps.iride_calib_passi[IRIS_FORMAT_MAX + 1];
    *puwDffSteps = (UINT16)E;
    return true;
}




//--------------------------------------------------------------
void sys_control_check_for_manual(void)
{

    static bool s_bFirstTime=true;

    g_ucManualRequest=0;

    if (g_ucOpenCloseFromCan)   // se movimento in manuale esce
        return;


    if (KEY_MANUAL)                 // chiave girata in manuale
        g_ucManualRequest = 100;

    if (g_bManualRequestFromCan)    // se richiesto da CANBUS forza il manuale
        g_ucManualRequest = 1;
    if (g_bManualRequestFromRemote)
        g_ucManualRequest=2;


    switch (g_ucOrienteer)
    {
    case RX_TOMO:
        g_ucManualRequest = 40;
        break;
    case RX_UNDER:
        if ((DffValida[RX_UNDER] == FALSE) || (FormatoValido[RX_UNDER] == FALSE))
        {
            g_ucManualRequest = 2;
        }
        else
        {
            if (dff_attuale < ee_ConfigColl.uwMinDff)
                g_ucManualRequest=9;
            else if (dff_attuale > ee_ConfigColl.uwMaxDff)
                g_ucManualRequest=10;
        }
        break;
    case RX_LEFT:
        if ((DffValida[RX_LEFT] == FALSE) || (FormatoValido[RX_LEFT] == FALSE))
        {
            g_ucManualRequest = 2;
        }
        break;
    case RX_RIGHT:
        if ((DffValida[RX_RIGHT] == FALSE) || (FormatoValido[RX_RIGHT] == FALSE))
        {
            g_ucManualRequest = 3;
        }
        break;
    case RX_ERROR:
        g_ucManualRequest=11;
        break;
    }

    if (g_ucManualRequest == 0)
    {
        ManualStatus[STEPPER_CROSS] = FALSE;
        ManualStatus[STEPPER_LONG] = FALSE;
        iride_manual = FALSE;
        return;
    }

    //Require manual status
//    g_bCrossManual=true;
//    g_bLongManual=true;
    ManualStatus[STEPPER_CROSS] = TRUE;
    ManualStatus[STEPPER_LONG] = TRUE;
    iride_manual = TRUE;
    return;


}

// TODO: cancellare funzioni non piu' utilizzate
// TODO: cancellare variabili non piu' utilizzate

//--------------------------------------------------------------
void sys_cross_init(void){

    g_MaxSteps[STEPPER_CROSS] = ee_ConfigBoards[STEPPER_A].uwMaxSteps;
    g_RefSize[STEPPER_CROSS] = (UINT32)ee_ConfigBoards[STEPPER_A].uwRefSize;
    g_RefSteps[STEPPER_CROSS] = (UINT32)ee_ConfigBoards[STEPPER_A].uwRefSteps;
    g_ManualSpeed[STEPPER_CROSS] = (UINT32)ee_ConfigColl.ManualSpeed;
    g_uwZeroDffCrossToBeSent=495;

}

//--------------------------------------------------------------







//--------------------------------------------------------------
void sys_long_init(void){

    g_MaxSteps[STEPPER_LONG] = ee_ConfigBoards[STEPPER_B].uwMaxSteps;
    g_RefSize[STEPPER_LONG] = (UINT32)ee_ConfigBoards[STEPPER_B].uwRefSize;
    g_RefSteps[STEPPER_LONG] = (UINT32)ee_ConfigBoards[STEPPER_B].uwRefSteps;
    g_ManualSpeed[STEPPER_LONG] = (UINT32)ee_ConfigColl.ManualSpeed;

    g_MaxSteps[STEPPER_LONG2] = ee_ConfigBoards[STEPPER_D].uwMaxSteps;
    g_RefSize[STEPPER_LONG2] = (UINT32)ee_ConfigBoards[STEPPER_D].uwRefSize;
    g_RefSteps[STEPPER_LONG2] = (UINT32)ee_ConfigBoards[STEPPER_D].uwRefSteps;
    g_ManualSpeed[STEPPER_LONG2] = (UINT32)ee_ConfigColl.ManualSpeed;

    g_uwZeroDffLongToBeSent=495;

}



//***********************************
//  funzione di controllo filtro
//***********************************
void sys_filter_process(void)
{
    UINT8 temp8;
    UINT8 flag;


    if (ee_ConfigColl.ucFilter == 0)        // se non configurato esce
    {
        LedStatus[STEPPER_FILTER] = GREEN;
        CollInReset &= ~STATO_FILTRO_RESET;
        return;
    }

    scorrimento_filtro();

    if (g_uwTimeSecondCounter < 3)
    {
        CollInReset |= STATO_FILTRO_RESET;
        return;
    }

    if (ee_FilterConfig.uwType == FILTER_5_HOLES)           // ******************
    {                                                       // ****  5 FORI  ****
        switch (g_ucFilterStatus)                           // ******************
        {
        case FILTER_INIT:
            filter_status_moving = YELLOW;
            g_ucActualFilterType = 4;      // per portarlo sul filtro giusto
            g_ucPuntaFiltro = 0;
            g_ucFilterRequired = ee_FilterConfig.Config_Filtro[g_ucPuntaFiltro];
            filtro_cambiato = FALSE;
            g_ucFilterStatus = ST_FILTER_PREP_RESET;
            break;

        case ST_FILTER_PREP_RESET:
            filter_status_moving = YELLOW;
            g_ucActualFilterType = 4;      // per portarlo sul filtro giusto
            LedStatus[STEPPER_FILTER] = RED;
            config_movimento(STEPPER_FILTER, MSG_MOV_HOME, 0, MOV_RESET);
            g_ucFilterStatus = ST_FILTER_WAIT_RESET;
            break;

        case ST_FILTER_WAIT_RESET:
            if (g_bReqHomeStepperC == FALSE)
            {
                if (Steppers[STEPPER_C].StepperData.StepperBits.bErrorHome == FALSE)    // controlla se non andato in errore
                {
                    g_uwFilterDelay = 30;
                    g_ucFilterStatus = ST_FILTER_AFTER_RESET;
                }
                else
                {
                    g_ucFilterStatus = ST_FILTER_PREP_ERROR;
                }
            }
            break;

        case ST_FILTER_AFTER_RESET:
            if (g_uwFilterDelay == 0)
            {
                numero_fori_filter = 0;
                pos_filtro_attuale = 0;
                g_uwFilterDelay = FILTER_PAUSE;
                CollInReset &= ~STATO_FILTRO_RESET;
                g_ucFilterStatus = ST_FILTER_PREP_IDLE;
            }
            break;

        case ST_FILTER_PREP_IDLE:
            if (g_uwFilterDelay == 0)
            {
//                sys_control_set_stepper(STEPPER_C, false, false);
                LedStatus[STEPPER_FILTER] = GREEN;
                filter_status_moving = GREEN;
                g_ucFilterStatus = ST_FILTER_IDLE;
            }
            break;

        case ST_FILTER_IDLE:
            if (g_ucFilterRequired == g_ucActualFilterType)     // controlla richiesto cambio filtro
            {
                reset_filter_try = TRUE;
                filtro_cambiato = TRUE;
                flag = TRUE;
                if (HOME_C)     // se centrato sul foro esamina altrimenti esegue reset
                {
                    if (numero_fori_filter == 0)        // se nessuno ha mosso allora porta in posizione
                    {
                        flag = FALSE;
                    }
                }

                if (flag == TRUE)      // qui deve fare il reset
                {
                    g_ucFilterStatus = ST_FILTER_PREP_RESET;   // esegue il reset
                }
            }
            else
            {
                if (reset_filter_try == TRUE)
                {
                    reset_filter_try = FALSE;
                    try_filter = 0;
                    allarmi_totale &= ~ALARM_FILTER;
                }

                flag = FALSE;
                if (HOME_C == TRUE)     // se centrato sul foro esamina altrimenti esegue reset
                {
                    if (ee_ConfigColl.ucFilterCheck)        // controlla se necessita del reset prima del movimento
                    {
                        if (filtro_cambiato == TRUE)
                        {
                            filtro_cambiato = FALSE;
                            if ((_filter_in_reset == TRUE) && (numero_fori_filter == 0))       // se gia' in reset non resettare ancora
                            {
                                flag = TRUE;
                            }
                        }
                        else
                        {
                            flag = TRUE;
                        }
                    }
                    else
                    {
                        if (numero_fori_filter == 0)        // se nessuno ha mosso allora porta in posizione
                        {
                            flag = TRUE;
                        }
                    }
                }


                if (flag == FALSE)      // qui deve portarsi in posizione
                {
                    g_ucFilterStatus = ST_FILTER_PREP_RESET;   // esegue il reset
                }
                else
                {
                    if (g_ucFilterRequired > 3)
                        g_ucFilterRequired = 0;
                    filtreq = g_ucFilterRequired;
                    g_bUpdateRele=true;

                    if (ee_ConfigColl.ucFilterCheck)        // calcola la posizione ed il numero dei fori
                    {
                        switch (filtreq)
                        {
                        case 0:
                            temp8 = 2;
                            fori_da_contare = 3;
                            break;
                        case 1:
                            temp8 = 3;
                            fori_da_contare = 4;
                            break;
                        case 2:
                            temp8 = 0;
                            fori_da_contare = 0;
                            break;
                        case 3:
                            temp8 = 1;
                            fori_da_contare = 2;
                            break;
                        }
                    }
                    else        // qui si porta al foro direttamente
                    {
                        switch (filtreq)
                        {
                        case 0:
                            temp8 = 2;
                            break;
                        case 1:
                            temp8 = 3;
                            break;
                        case 2:
                            temp8 = 0;
                            break;
                        case 3:
                            temp8 = 1;
                            break;
                        }
                        if (temp8 >= pos_filtro_attuale)
                            fori_da_contare = temp8 - pos_filtro_attuale;
                        else
                            fori_da_contare = pos_filtro_attuale - temp8;

                        if ((pos_filtro_attuale == 0) || (temp8 == 0))
                        {
                            if (pos_filtro_attuale || temp8)
                            {
                                fori_da_contare++;
                            }
                        }
                    }
                    pos_filtro_attuale = temp8;

                    LedStatus[STEPPER_FILTER] = RED;
//                    g_lDestination[STEPPER_FILTER] = (long)temp8 * (long)ee_FilterConfig.uwSteps;
//                    g_bMoveAbsStepperC = true;
                    config_movimento(STEPPER_FILTER, MSG_MOV_ABS, (long)temp8 * (long)ee_FilterConfig.uwSteps, MOV_FAST);
                    timer_motor_on[STEPPER_FILTER] = TIME_WAIT_STOP;
                    Steppers[STEPPER_FILTER].StepperData.StepperBits.bMoving = 1;
                    if (temp8)
                        _filter_in_reset = FALSE;
                    g_ucFilterStatus = ST_FILTER_WAIT_MOV;   // wait for the new position
                }
            }
            break;

        case ST_FILTER_WAIT_MOV:        // aspetta la fine del movimento
            filter_status_moving = YELLOW;
            if (Steppers[STEPPER_FILTER].StepperData.StepperBits.bMoving == 0)
            {
                if (g_uwFilterDelay == 0)       // attende stabilizzazione sensore
                {
                    if (ee_ConfigColl.ucFilterCheck)        // controlla se deve contare i fori
                    {
                        flag = FALSE;
                        if (numero_fori_filter == fori_da_contare)
                        {
                            if (HOME_C == TRUE)     // se foro presente allora tutto OK
                            {
                                flag = TRUE;
                            }
                        }
                    }
                    else
                    {
                        flag = TRUE;
                        if (HOME_C == FALSE)     // se foro presente allora tutto OK
                            flag = FALSE;
                        // TODO: controllare la lettura se conta giusto il numero dei fori
                        if (numero_fori_filter != fori_da_contare)
                            flag = FALSE;
                    }

                    if (flag == TRUE)     // se foro presente allora tutto OK
                    {
                        numero_fori_filter = 0;
                        g_ucActualFilterType = filtreq;
                        sys_display_filter(FALSE);       // visualizza nome filtro
                        g_ucFilterStatus = ST_FILTER_PREP_IDLE;
                    }
                    else
                    {                   // altrimenti riprova
                        g_ucFilterStatus = ST_FILTER_PREP_ERROR;
                    }
                }
            }
            else
            {
                g_uwFilterDelay = 30;       // delay per lettura sensore HOME
            }
            break;

        case ST_FILTER_PREP_ERROR:

//            sys_control_set_stepper(STEPPER_C, false, false);
            LedStatus[STEPPER_FILTER] = RED;
            err_counter_filter++;
            if (++try_filter >= 3)
            {
                allarmi_totale |= ALARM_FILTER;
                filter_status_moving = RED;
                g_ucFilterRequired = g_ucActualFilterType;
            }

            reset_driver = TRUE;
            g_uwFilterDelay = FILTER_PAUSE;
            g_ucFilterStatus = ST_FILTER_WAIT_SBY;
            break;

        case ST_FILTER_WAIT_SBY:
            if (g_uwFilterDelay == 0)
            {
                g_ucFilterStatus = ST_FILTER_ERROR;
            }
            break;

        case ST_FILTER_ERROR:
            if (g_ucFilterRequired != g_ucActualFilterType)     // richiesto cambio filtro
            {
                if (come_from_error_filter == TRUE)
                {
                    come_from_error_filter = FALSE;
                    try_filter = 0;
                    allarmi_totale &= ~ALARM_FILTER;
                }

                filtro_cambiato = TRUE;
                g_uwFilterDelay = FILTER_PAUSE;
                g_ucFilterStatus = ST_FILTER_PREP_RESET;
            }
            else
            {
                come_from_error_filter = TRUE;
                sys_display_filter(TRUE);       // display errore
            }
            break;

        default:
            g_ucFilterStatus = FILTER_INIT;
            break;

        }
    }                                                       // ******************
    else                                                    // ****  1 FORO  ****
    {                                                       // ******************

        switch (g_ucFilterStatus)
        {
        case FILTER_INIT:
            CollInReset |= STATO_FILTRO_RESET;
            if (g_uwTimeSecondCounter < 3)
                break;
            g_ucActualFilterType = 0;      // per portarlo sul filtro giusto
            filtro_cambiato = FALSE;
            g_ucFilterStatus = ST_FILTER_PREP_RESET;
            break;

        case ST_FILTER_PREP_RESET:
            LedStatus[STEPPER_FILTER] = RED;
            g_bReqHomeStepperC = TRUE;
            g_ucFilterStatus = ST_FILTER_WAIT_RESET;
            break;

        case ST_FILTER_WAIT_RESET:
            if (g_bReqHomeStepperC == FALSE)
            {
                if (Steppers[STEPPER_C].StepperData.StepperBits.bErrorHome == FALSE)    // controlla se non andato in errore
                {
                    g_uwFilterDelay = 30;
                    g_ucFilterStatus = ST_FILTER_AFTER_RESET;
                }
                else
                {
                    g_ucFilterStatus = ST_FILTER_PREP_ERROR;
                }
            }
            break;

        case ST_FILTER_AFTER_RESET:
            if (g_uwFilterDelay == 0)
            {
                _filter_in_reset = TRUE;
                g_uwFilterDelay = FILTER_PAUSE;
                CollInReset &= ~STATO_FILTRO_RESET;
                g_ucFilterStatus = ST_FILTER_PREP_IDLE;
            }
            break;

        case ST_FILTER_PREP_IDLE:
            if (g_uwFilterDelay == 0)
            {
                LedStatus[STEPPER_FILTER] = GREEN;
                g_ucFilterStatus = ST_FILTER_IDLE;
            }
            break;

        case ST_FILTER_IDLE:
            if (g_ucFilterRequired == g_ucActualFilterType)     // controlla richiesto cambio filtro
            {
                reset_filter_try = TRUE;
                filtro_cambiato = TRUE;
            }
            else
            {
                if (reset_filter_try == TRUE)
                {
                    reset_filter_try = FALSE;
                    try_filter = 0;
                    allarmi_totale &= ~ALARM_FILTER;
                }

                flag = FALSE;       // resetta di default
                if (ee_ConfigColl.ucFilterCheck)        // controlla se necessita del reset prima del movimento
                {
                    if (filtro_cambiato == TRUE)
                    {
                        filtro_cambiato = FALSE;
                    }
                    else
                    {
                        flag = TRUE;
                    }
                }
                else
                {
                    flag = TRUE;
                }


                if (flag == FALSE)      // qui deve portarsi in posizione
                {
                    g_ucFilterStatus = ST_FILTER_PREP_RESET;   // esegue il reset
                }
                else
                {
                    if (g_ucFilterRequired > 3)
                        g_ucFilterRequired = 0;
                    filtreq = g_ucFilterRequired;
                    g_bUpdateRele=true;

                    LedStatus[STEPPER_FILTER] = RED;
                    g_lDestination[STEPPER_FILTER] = g_ucFilterRequired * ee_FilterConfig.uwSteps;
                    g_bMoveAbsStepperC = true;
                    Steppers[STEPPER_FILTER].StepperData.StepperBits.bMoving = 1;
                    g_ucActualFilterType = g_ucFilterRequired;
                    g_ucFilterStatus = ST_FILTER_WAIT_MOV;   // wait for the new position
                }
            }
            break;

        case ST_FILTER_WAIT_MOV:        // aspetta la fine del movimento
            if (Steppers[STEPPER_FILTER].StepperData.StepperBits.bMoving == 0)
            {
                g_ucActualFilterType = filtreq;
                sys_display_filter(FALSE);       // visualizza nome filtro
                g_ucFilterStatus = ST_FILTER_PREP_IDLE;
            }
            else
            {
                g_uwFilterDelay = 30;       // delay per lettura sensore HOME
            }
            break;

        case ST_FILTER_PREP_ERROR:
            LedStatus[STEPPER_FILTER] = RED;
            err_counter_filter++;
            if (++try_filter >= 3)
            {
                allarmi_totale |= ALARM_FILTER;
                g_ucFilterRequired = g_ucActualFilterType;
            }

            reset_driver = TRUE;
            g_uwFilterDelay = FILTER_PAUSE;
            g_ucFilterStatus = ST_FILTER_WAIT_SBY;
            break;

        case ST_FILTER_WAIT_SBY:
            if (g_uwFilterDelay == 0)
            {
                g_ucFilterStatus = ST_FILTER_ERROR;
            }
            break;

        case ST_FILTER_ERROR:
            if (g_ucFilterRequired != g_ucActualFilterType)     // richiesto cambio filtro
            {
                if (come_from_error_filter == TRUE)
                {
                    come_from_error_filter = FALSE;
                    try_filter = 0;
                    allarmi_totale &= ~ALARM_FILTER;
                }

                filtro_cambiato = TRUE;
                g_uwFilterDelay = FILTER_PAUSE;
                g_ucFilterStatus = ST_FILTER_PREP_IDLE;
            }
            else
            {
                come_from_error_filter = TRUE;
                sys_display_filter(TRUE);       // display errore
            }
            break;

        default:
            g_ucFilterStatus = FILTER_INIT;
            break;

        }

/*
        switch (g_ucFilterStatus)
        {
        case FILTER_INIT:
            if (!g_bInitEnd)
                break;
            g_ucFilterStatus=FILTER_CHECK_HOME;
            break;
        case FILTER_CHECK_HOME:
            if (!ee_ConfigColl.ucFilter)
                break;

            g_bReqHomeStepperC=true;
            g_ucActualFilterType = 0;
            g_ucFilterStatus=FILTER_WAIT;
            break;

        case FILTER_WAIT:
            if (g_bReqHomeIrqC == FALSE)
            {
                if (Steppers[STEPPER_C].StepperData.StepperBits.bErrorHome == true)
                {
                    asm("nop ");
                }
                else
                {
                    g_ucFilterStatus=FILTER_HOME;
                }
            }
            break;

        case FILTER_HOME:
            ////////////////
            //Filter Manager
            ////////////////
            if (!ee_ConfigColl.ucFilter)
                break;
            if (g_ucFilterRequired==g_ucActualFilterType)
                break;

            if (Steppers[STEPPER_FILTER].StepperData.StepperBits.bMoving)   //0.61
                break;

            if(!ee_ConfigColl.ucFilterCheck){
                //Usual Filter without zero check
                g_bUpdateRele=true;
                g_lDestination[STEPPER_FILTER]=g_ucFilterRequired*ee_FilterConfig.uwSteps;
                g_bMoveAbsStepperC=true;
                g_ucActualFilterType=g_ucFilterRequired;
                if (g_bInitEnd)
                    sys_display_filter(FALSE);
                break;
            }
            g_bUpdateRele=true;
            g_bReqHomeStepperC=true;            //RTZ with RESET
            g_ucFilterStatus=FILTER_WAIT_ZERO;
            break;
        case FILTER_WAIT_ZERO:
            if (g_bReqHomeStepperC)
                break;
            //g_uwFilterDelay=200;
            g_ucFilterStatus=FILTER_IS_HOME_REQ;
            break;

        case FILTER_IS_HOME_REQ:
            //if (g_bFilterReqHome){
            //  g_bFilterReqHome=false;
            //  g_bReqHomeStepperC=true;
            //}
            g_uwFilterDelay=REV_FILTER_DELAY;           //0.53
            g_ucFilterStatus=FILTER_GO_TO_POS;
            break;

        case FILTER_GO_TO_POS:
            if (g_uwFilterDelay)                        //0.53
                break;
            g_bUpdateRele=true;
            g_lDestination[STEPPER_FILTER]=g_ucFilterRequired*ee_FilterConfig.uwSteps;
            g_bMoveAbsStepperC=true;
            g_ucActualFilterType=g_ucFilterRequired;
            if (g_bInitEnd)
                sys_display_filter(FALSE);
            g_ucFilterStatus=FILTER_HOME;
            break;
        }
*/

    }
}



//--------------------------------------------------------------
void sys_iris_process(void)
{
    static UINT8 first_move;
    static irisInReset = FALSE;
    static UINT8 conteggioStatus;
    UINT8 check_status;
    UINT16 tempSteps;

    if(ee_ConfigColl.ucIrisPresent == IRIDE_NO)
    {
        LedStatus[STEPPER_IRIS] = GREEN;
        CollInReset &= ~STATO_IRIDE_RESET;
        return;
    }


    if (stato_iride & IRIDE_MOVE)        // per segnalare il led
    {
        LedStatus[STEPPER_IRIS] = RED;
    }
    else
    {
        LedStatus[STEPPER_IRIS] = GREEN;
    }

    switch (g_ucIrisStatus)
    {
    case IRIS_INIT:
        CollInReset |= STATO_IRIDE_RESET;
        LedStatus[STEPPER_IRIS] = RED;
        first_move = 1;
        irisInReset = FALSE;
        if (g_uwTimeSecondCounter < 4)
            break;

//        timerWaitIride = 0;
        num_formato_iride = 0;
        g_ucIrisStatus = IRIS_READ_CALIB;
        break;

    case IRIS_READ_CALIB:
        if (msg_Iride_COM == FALSE)
        {
            if (num_formato_iride < (IRIS_FORMAT_MAX + 2))
            {
                msg_Iride_ID = ee_CanColl.unIdIride + MSG_IRIS_CALIB;     // lettura valore di taratura
                msg_Iride_DLC = 8;
                msg_Iride_DATA[0] = MSG_IRIS_CAL_READ_TAR;       // sotto-comando di lettura
                msg_Iride_DATA[1] = num_formato_iride;      // indice del valore da leggere
                msg_Iride_COM = TRUE;
                num_formato_iride++;
            }
            else
            {
                timer_iride = 5;        // per dare il tempo di leggere i messaggi in risposta
                g_ucIrisStatus = IRIS_WAIT_CALIB;
            }
        }
        break;

    case IRIS_WAIT_CALIB:
        if (timer_iride == 0)
        {
            msg_Iride_ID = ee_CanColl.unIdIride + MSG_IRIS_CALIB;     // set dei passi massimi
            msg_Iride_DLC = 4;
            msg_Iride_DATA[0] = MSG_IRIS_CAL_PMAX;
            msg_Iride_DATA[1] = ee_IrisSteps.iride_calib_passi[IRIS_FORMAT_MAX + 1] >> 8;        // Passi Massimi
            msg_Iride_DATA[2] = ee_IrisSteps.iride_calib_passi[IRIS_FORMAT_MAX + 1] & 0xFF;
            msg_Iride_DATA[3] = 0;      // non salva in eeprom
            msg_Iride_COM = TRUE;
            g_ucIrisStatus = IRIS_INIT_WAIT;
        }
        break;

    case IRIS_INIT_WAIT:
        if (msg_Iride_COM == FALSE)
        {
            LedStatus[STEPPER_IRIS] = RED;
            msg_Iride_ID = ee_CanColl.unIdIride + MSG_IRIS_MOV;
            msg_Iride_DLC = 3;
            msg_Iride_DATA[0] = 0x00;
            msg_Iride_DATA[1] = ee_IrisSteps.iride_calib_pot[0] >> 8;        // posizione di reset
            msg_Iride_DATA[2] = ee_IrisSteps.iride_calib_pot[0] & 0xFF;
            msg_Iride_COM = TRUE;
            stato_iride |= IRIDE_MOVE;
            timer_iride = TIMEOUT_RESET_IRIDE;
            NewValIris = FALSE;
            irisInReset = TRUE;
            conteggioStatus = 0;
            g_ucIrisStatus = IRIS_HOME_AUTO_WAIT;

            g_uwIrisFormatFromCan = LastIrisFormat;
            muovi_iride = TRUE;
        }
        break;

    case IRIS_HOME_AUTO_WAIT:
        if (msg_Iride_COM == FALSE)
        {
            if (irisInReset == TRUE)
            {
                if (NewValIris == TRUE)
                {
                    NewValIris = FALSE;
                    if (++conteggioStatus >= 2)
                    {
                        irisInReset = FALSE;
                        check_status = TRUE;
                    }
                }
            }
            else
            {
                check_status = TRUE;
            }

            if (check_status == TRUE)
            {
                if ((stato_iride & IRIDE_MOVE) == 0)        // se fermo vai in attesa comandi
                {
                    if (first_move == 0)
                    {
                        CollInReset &= ~STATO_IRIDE_RESET;
                        if (ee_ConfigColl.ucIrisEnabled == TRUE)    // se presente ed abilitato vai ad attendere comandi
                            g_ucIrisStatus = IRIS_READY;
                        else
                            g_ucIrisStatus = IRIS_DISABLED;     // se presente ma disabilitato allora aspetta
                    }
                    else
                    {                   // porta a tutto aperto
                        first_move = 0;
                        msg_Iride_ID = ee_CanColl.unIdIride + MSG_IRIS_MOV;
                        msg_Iride_DLC = 7;
                        tempSteps = ee_IrisSteps.iride_calib_passi[IRIS_FORMAT_MAX + 1];
                        msg_Iride_DATA[0] = 0x01;       // muovi assoluto
                        msg_Iride_DATA[1] = 0;     // posizione da raggiungere
                        msg_Iride_DATA[2] = 0;
                        msg_Iride_DATA[3] = tempSteps >> 8;     // posizione da raggiungere
                        msg_Iride_DATA[4] = tempSteps & 0xFF;
                        msg_Iride_DATA[5] = tempSteps >> 8;     // massimo non superabile
                        msg_Iride_DATA[6] = tempSteps & 0xFF;
                        msg_Iride_COM = TRUE;
                        stato_iride |= IRIDE_MOVE;
                        timer_iride = TIMEOUT_MOVE_IRIDE;
                        muovi_iride = FALSE;
                        g_ucIrisStatus = IRIS_HOME_AUTO_WAIT;
                    }
                }
            }
        }

        if (timer_iride == 0)       // se scaduto timeout ferma tutto
        {
            msg_Iride_ID = ee_CanColl.unIdIride + MSG_IRIS_MOV;     // STOP motore
            msg_Iride_DLC = 1;
            msg_Iride_DATA[0] = 0xFF;
            msg_Iride_COM = TRUE;
            g_ucIrisStatus = IRIS_HOME_ERROR;
        }
        break;

    case IRIS_HOME_ERROR:
        err_counter_iride++;
        allarmi_totale |= ALARM_IRIS;
        first_move = 1;
        g_ucIrisStatus = IRIS_READY;
        break;

    case IRIS_READY:
        if (msg_Iride_COM == FALSE)
        {
            if (ReqHomeIris == TRUE)        // se reset per allarme reinizializza
            {
                ReqHomeIris = FALSE;
                g_ucIrisStatus = IRIS_INIT;
            }
            else
            {
                if (g_ucOpenCloseFromCan & OPEN_IRIS)       // controllo apertura e chiusura remota
                {
                    send_tasto_remoto(OPEN_IRIS);
                    g_ucIrisStatus = IRIS_WAIT_RILASCIO_TASTO_OPEN;
                }
                else if (g_ucOpenCloseFromCan & CLOSE_IRIS)
                {
                    send_tasto_remoto(CLOSE_IRIS);
                    g_ucIrisStatus = IRIS_WAIT_RILASCIO_TASTO_CLOSE;
                }
                else if (EnableIrisKey != EnableIrisKey_old)
                {
                    EnableIrisKey_old = EnableIrisKey;
                    set_passi_max = FALSE;
                    msg_Iride_ID = ee_CanColl.unIdIride + MSG_IRIS_OPTION;     // Enable/Disable TASTI
                    msg_Iride_DLC = 2;
                    msg_Iride_DATA[0] = EnableIrisKey ? 0 : 1;      // il msg e' di disabilitazione
                    msg_Iride_DATA[1] = 0;      // non salva in eeprom
                    msg_Iride_COM = TRUE;
                }
                else if (set_passi_max == TRUE)
                {
                    set_passi_max = FALSE;
                    msg_Iride_ID = ee_CanColl.unIdIride + MSG_IRIS_CALIB;     // set dei passi massimi
                    msg_Iride_DLC = 4;
                    msg_Iride_DATA[0] = MSG_IRIS_CAL_PMAX;
                    msg_Iride_DATA[1] = ee_IrisSteps.iride_calib_passi[IRIS_FORMAT_MAX + 1] >> 8;        // Passi Massimi
                    msg_Iride_DATA[2] = ee_IrisSteps.iride_calib_passi[IRIS_FORMAT_MAX + 1] & 0xFF;
                    msg_Iride_DATA[3] = 0;      // non salva in eeprom
                    msg_Iride_COM = TRUE;
                }
                else if (set_passi_campoiride == TRUE)
                {
                    set_passi_campoiride = FALSE;
                    msg_Iride_ID = ee_CanColl.unIdIride + MSG_IRIS_CALIB;     // set dei passi massimi
                    msg_Iride_DLC = 4;
                    msg_Iride_DATA[0] = MSG_IRIS_CAL_PMAX;
                    if (iride_manual == FALSE)
                    {
                        if (CampoIride)     // se Campo Iride inviato con messaggio allora stabilisce il nuovo massimo
                        {
                            LinIrisOverDff(g_uwActualDff, CampoIride, &tempSteps);
                            msg_Iride_DATA[1] = tempSteps >> 8;     // massimo non superabile
                            msg_Iride_DATA[2] = tempSteps & 0xFF;
                        }
                        else
                        {
                            LinIrisOverDff(g_uwActualDff, LastIrisFormat, &tempSteps);
                            msg_Iride_DATA[1] = tempSteps >> 8;     // massimo non superabile
                            msg_Iride_DATA[2] = tempSteps & 0xFF;
                        }
                    }
                    else
                    {
                        msg_Iride_DATA[1] = ee_IrisSteps.iride_calib_passi[IRIS_FORMAT_MAX + 1] >> 8;        // Passi Massimi
                        msg_Iride_DATA[2] = ee_IrisSteps.iride_calib_passi[IRIS_FORMAT_MAX + 1] & 0xFF;
                    }
                    msg_Iride_DATA[3] = 0;      // non salva in eeprom
                    msg_Iride_COM = TRUE;
                }
                else if (muovi_iride == TRUE)
                {
                    muovi_iride = FALSE;
                    if ((fluoroscopia_on == TRUE) || (IrideIndipendente == TRUE))
                    {
                        LinIrisOverDff(g_uwActualDff, g_uwIrisFormatFromCan, &tempSteps);
                    }
                    else
                    {
                        tempSteps = ee_IrisSteps.iride_calib_passi[IRIS_FORMAT_MAX + 1];   // tutto aperto
                    }
                    Iride_PassiMax = tempSteps;
                    if ((tempSteps != posizione_iride) || (iride_manual != iride_manual_old))
                    {
                        iride_manual_old = iride_manual;
                        msg_Iride_ID = ee_CanColl.unIdIride + MSG_IRIS_MOV;
                        msg_Iride_DLC = 7;
                        msg_Iride_DATA[0] = 0x01;       // muovi assoluto
                        msg_Iride_DATA[1] = 0;     // posizione da raggiungere
                        msg_Iride_DATA[2] = 0;
                        msg_Iride_DATA[3] = tempSteps >> 8;     // posizione da raggiungere
                        msg_Iride_DATA[4] = tempSteps & 0xFF;
                        if (iride_manual == FALSE)
                        {
                            if (CampoIride)     // se Campo Iride inviato con messaggio allora stabilisce il nuovo massimo
                            {
                                LinIrisOverDff(g_uwActualDff, CampoIride, &tempSteps);
                                msg_Iride_DATA[5] = tempSteps >> 8;     // massimo non superabile
                                msg_Iride_DATA[6] = tempSteps & 0xFF;
                            }
                            else
                            {
                                msg_Iride_DATA[5] = tempSteps >> 8;     // massimo non superabile
                                msg_Iride_DATA[6] = tempSteps & 0xFF;
                            }
                        }
                        else
                        {
                            msg_Iride_DATA[5] = ee_IrisSteps.iride_calib_passi[IRIS_FORMAT_MAX + 1] >> 8;        // Passi Massimi
                            msg_Iride_DATA[6] = ee_IrisSteps.iride_calib_passi[IRIS_FORMAT_MAX + 1] & 0xFF;
                        }
                        msg_Iride_COM = TRUE;
                        stato_iride |= IRIDE_MOVE;
                        timer_iride = TIMEOUT_MOVE_IRIDE;
                        g_ucIrisStatus = IRIS_HOME_AUTO_WAIT;
                    }
                }
                else if (ee_ConfigColl.ucIrisEnabled == FALSE)      // arrivata disabilitazione
                {
                    first_move = 1;
                    g_ucIrisStatus = IRIS_HOME_AUTO_WAIT;       // in questo modo porta tutto aperto
                }
            }
        }
        break;

    case IRIS_WAIT_RILASCIO_TASTO_OPEN:
        if (msg_Iride_COM == FALSE)
        {
            if ((g_ucOpenCloseFromCan & OPEN_IRIS) == 0)
            {
                send_tasto_remoto(NONE_IRIS);
                g_ucIrisStatus = IRIS_READY;
            }
        }
        break;

    case IRIS_WAIT_RILASCIO_TASTO_CLOSE:
        if (msg_Iride_COM == FALSE)
        {
            if ((g_ucOpenCloseFromCan & CLOSE_IRIS) == 0)
            {
                send_tasto_remoto(NONE_IRIS);
                g_ucIrisStatus = IRIS_READY;
            }
        }
        break;

    case IRIS_DISABLED:
        if (ee_ConfigColl.ucIrisEnabled == TRUE)    // se presente ed abilitato vai ad attendere comandi
        {
            g_ucIrisStatus = IRIS_READY;
        }
        break;

    default:
        LedStatus[STEPPER_IRIS] = GREEN;
        break;
    }

}


//************************************************
//  manda valore tasto remoto
//************************************************
void send_tasto_remoto(UINT8 op)
{
    msg_Iride_ID = ee_CanColl.unIdIride + MSG_IRIS_MOV;     // manda tasto remoto
    msg_Iride_DLC = 3;
    msg_Iride_DATA[0] = 0x04;
    msg_Iride_DATA[1] = (op == CLOSE_IRIS) ? 0x01 : 0x00;
    msg_Iride_DATA[2] = (op == OPEN_IRIS) ? 0x01 : 0x00;
    msg_Iride_COM = TRUE;
}



//--------------------------------------------------------------
void sys_control_led_and_relays(uchar ucLed){

    if (g_uwActualCrossToBeSent<MIN_CLOSED || g_uwActualLongToBeSent<MIN_CLOSED)
        Relays.Status.bit.r_chiuse=true;
    else
        Relays.Status.bit.r_chiuse=false;

    switch (ucLed){

        case NONE:
            Relays.Status.bit.r_ready = 0;
            Relays.Status.bit.r_man   = 0;
            Relays.Status.bit.r_hold  = 0;
            Relays.Status.bit.r_raggi = 0;
            break;

        case RED:
            Relays.Status.bit.r_ready = 0;
            Relays.Status.bit.r_man   = 0;
            Relays.Status.bit.r_hold  = 1;
            Relays.Status.bit.r_raggi = 0;
            break;

        case YELLOW:
            Relays.Status.bit.r_ready = 0;
            Relays.Status.bit.r_man   = 1;
            Relays.Status.bit.r_hold  = 0;
            Relays.Status.bit.r_raggi = 1;
            break;

        case GREEN:
            Relays.Status.bit.r_ready = 1;
            Relays.Status.bit.r_man   = 0;
            Relays.Status.bit.r_hold  = 0;
            Relays.Status.bit.r_raggi = 1;
            break;

    }

    g_ucLedToBeOn=ucLed;

}


//--------------------------------------------------------------
void sys_control_led(void){

    if (g_uwTimerLed)
        return;

    if (((allarmi_totale & ALARM_CHECK) || (alarm_dff)) && (g_ucManualRequest == 0))
    {
        g_uwTimerLed = DELAY_LED;
        sys_control_led_and_relays(RED);
        return;
    }

    //All Green?
    while (true){
        if (LedStatus[STEPPER_CROSS] != GREEN)
            break;
        if (LedStatus[STEPPER_LONG] != GREEN)
            break;
        if (LedStatus[STEPPER_LONG2] != GREEN)
            break;
        if (LedStatus[STEPPER_IRIS] != GREEN)
            break;
        if (LedStatus[STEPPER_FILTER] != GREEN)
            break;
        g_uwTimerLed=0;
        sys_control_led_and_relays(GREEN);
        return;
    }

    //One Red?
    while (true){
        if (LedStatus[STEPPER_CROSS] == RED)
            break;
        if (LedStatus[STEPPER_LONG] == RED)
            break;
        if (LedStatus[STEPPER_LONG2] == RED)
            break;
        if (LedStatus[STEPPER_IRIS] == RED)
            break;
        if (LedStatus[STEPPER_FILTER] == RED)
            break;
        g_uwTimerLed = DELAY_LED;
        sys_control_led_and_relays(YELLOW);
        return;
    }

    g_uwTimerLed = DELAY_LED;
    sys_control_led_and_relays(RED);
}


//--------------------------------------------------------------
void sys_control_inclination(void)
{

    static UINT8 lato_prec = 0xFF;
    static SINT32 s_uwOldXValue=0;
    static SINT32 s_uwOldYValue=0;
    static UINT8 firstTomo = TRUE;
    SINT32 uwActualXValue;
    SINT32 uwActualYValue;
    SINT32 x1, y1, x2, y2, stemp32;
    SINT16 stemp16;

    if (!g_bInclSync)
        return;

    g_bInclSync=false;

    if (DebugIncl == TRUE)
    {
        g_ucOrienteer = InclinazioneCan;

        if (g_bTomography == TRUE)
        {
            if (firstTomo == TRUE)
            {
                firstTomo = FALSE;
                TomographyDFF = DisplayDFF;
            }
            g_ucOrienteer = RX_TOMO;
        }

        switch (g_ucOrienteer)
        {
        case RX_TOMO:
            g_nYDegrees = 0;
            g_nDegrees = 0;
            break;
        case RX_UNDER:
            g_nYDegrees = 0;
            g_nDegrees = 0;
            firstTomo = TRUE;
            break;
        case RX_LEFT:
            g_nYDegrees = 1;
            g_nDegrees = 90;
            firstTomo = TRUE;
            break;
        case RX_RIGHT:
            g_nYDegrees = -1;
            g_nDegrees = 90;
            firstTomo = TRUE;
            break;
        }
    }
    else
    {
        if (g_bTomography == TRUE)
        {
            if (firstTomo == TRUE)
            {
                firstTomo = FALSE;
                TomographyDFF = DisplayDFF;
            }
            g_uwAlarmCounterOrient=DELAY_ORIENTEER;
            g_nDegrees=0;
            g_ucOrienteer = RX_TOMO;
        }
        else
        {
            if (ee_ConfigColl.ucFiltraggioIncl)
            {
                valoreAsseX = media_valore_analogico[ADXL_X];
                valoreAsseY = media_valore_analogico[ADXL_Y];
            }
            else
            {
                valoreAsseX = g_nFilteredAdc[ADXL_X];
                valoreAsseY = g_nFilteredAdc[ADXL_Y];
            }

            firstTomo = TRUE;
            if (g_bExclIncl || !ee_ConfigColl.ucInclinometer){
                g_uwAlarmCounterOrient=DELAY_ORIENTEER;
                g_nDegrees=0;
                g_ucOrienteer = RX_UNDER;
                return;
            }


            uwActualXValue = valoreAsseX;
            uwActualYValue = valoreAsseY;


            if ((uwActualXValue == s_uwOldXValue) && (uwActualYValue == s_uwOldYValue))
                return;

            s_uwOldXValue = uwActualXValue;
            s_uwOldYValue = uwActualYValue;

            x1 = (SINT32)ee_OffsetColl.nInclOffsetY;
            if (uwActualYValue >= ee_OffsetColl.nInclOffsetY)       // confronto con punto centrale di Zero
            {
                x2 = (SINT32)ee_OffsetColl.InclOffsetDxY;
                y2 = 90L;
            }
            else
            {
                x2 = (SINT32)ee_OffsetColl.InclOffsetSxY;
                y2 = -90L;
            }
            stemp32 = ((SINT32)uwActualYValue - x1) * 100L;
            stemp32 *= y2;
            stemp32 /= (x2 - x1);
            g_nYDegrees = stemp32;

            x1 = (SINT32)ee_OffsetColl.nInclOffsetX;
            if (g_nYDegrees >= 0)       // confronto con punto centrale di Zero
            {
                x2 = (SINT32)ee_OffsetColl.InclOffsetDxX;
                y2 = 90L;
            }
            else
            {
                x2 = (SINT32)ee_OffsetColl.InclOffsetSxX;
                y2 = -90L;
            }
            stemp32 = ((SINT32)uwActualXValue - x1) * 100L;
            stemp32 *= y2;
            stemp32 /= (x2 - x1);
            g_nXDegrees = stemp32;

            if (g_nXDegrees >= 0)
                g_nXDegrees += 5L;
            else
                g_nXDegrees -= 5L;

            if (g_nYDegrees >= 0)
                g_nYDegrees += 5L;
            else
                g_nYDegrees -= 5L;

            stemp32 = g_nXDegrees + g_nYDegrees;        // somma algebrica

            stemp32 /= 200;
            g_nDegrees = (SINT16)stemp32;
            g_nDegrees = val_abs(g_nDegrees, 0);

            if (val_abs(g_nDegrees, 0) <= ee_ConfigColl.ucInclDegrees){
                g_uwAlarmCounterOrient=DELAY_ORIENTEER;
                g_ucOrienteer = RX_UNDER;
            }
            if ((val_abs(g_nDegrees, 90) <= ee_ConfigColl.ucInclDegrees) && (g_nYDegrees < 0)){
                g_uwAlarmCounterOrient=DELAY_ORIENTEER;
                g_ucOrienteer = RX_LEFT;
            }
            if ((val_abs(g_nDegrees, 90) <= ee_ConfigColl.ucInclDegrees) && (g_nYDegrees > 0)){
                g_uwAlarmCounterOrient=DELAY_ORIENTEER;
                g_ucOrienteer = RX_RIGHT;
            }

            if(!g_uwAlarmCounterOrient)
                g_ucOrienteer = RX_ERROR;
        }
    }


    if (g_ucOrienteer != lato_prec)
    {
        lato_prec = g_ucOrienteer;
        if (g_ucOrienteer != RX_ERROR)
            ResetPosizione = TRUE;
                            // per muovere i rele' della ASR003
        TimerNewIncl = TIME_STAB_INCLINAZIONE;  // attesa del tempo di stabilizzazione
        switch (g_ucOrienteer)
        {
        case RX_TOMO:
        case RX_ERROR:
        case RX_UNDER:
            Relays.Status.bit.Out6 = 0;     // set delle uscite relay VERTICALE
            Relays.Status.bit.Out7 = 0;
            break;
        case RX_LEFT:
            Relays.Status.bit.Out6 = 0;     // set delle uscite relay SINISTRA
            Relays.Status.bit.Out7 = 1;
            break;
        case RX_RIGHT:
            Relays.Status.bit.Out6 = 1;     // set delle uscite relay DESTRA
            Relays.Status.bit.Out7 = 0;
            break;
        }
        g_bUpdateRele =true;        // forza il messaggio in uscita
    }

}

//--------------------------------------------------------------
void sys_control_update_data(void){

    g_lCrossActualSteps=g_lVPos[STEPPER_CROSS];
    g_lLongActualSteps=g_lVPos[STEPPER_LONG];
    g_lLong2ActualSteps=g_lVPos[STEPPER_LONG2];
//  g_lIrisActualSteps=g_lVPos[STEPPER_FILTER];
    g_lIrisActualSteps = posizione_iride;

}

//--------------------------------------------------------------
void sys_control_keyboard(void){

    UINT8 i;
    static bool s_bKeyFilterPressed=false;
    static bool s_bKeyLampPressed=false;
    uchar ucTempTorque;


    if ((EnableFilterKey == TRUE) || (ManualStatus[STEPPER_CROSS] == TRUE))
    {
        // TODO: da completare Motore FILTRO
        if (FILTER_SWITCH && !g_uwPushFilterButtonDelay && !s_bKeyFilterPressed && (Steppers[STEPPER_FILTER].StepperData.StepperBits.bMoving == 0) ){
            g_uwPushFilterButtonDelay=300;
            s_bKeyFilterPressed=true;

            for (i = 0; i <= 3; i++)        // trova la posizione del filtro attuale
            {
                if (ee_FilterConfig.Config_Filtro[i] == g_ucActualFilterType)
                    break;
            }

            if (i < 3)
                g_ucPuntaFiltro = i + 1;
            else
                g_ucPuntaFiltro = 0;

            g_ucFilterRequired = ee_FilterConfig.Config_Filtro[g_ucPuntaFiltro];
        }

        if (!FILTER_SWITCH)
            s_bKeyFilterPressed=false;
    }


    if ((EnableLampKey == TRUE) || (ManualStatus[STEPPER_CROSS] == TRUE))
    {
        if (LAMP_SWITCH && !g_uwPushLampButtonDelay && !s_bKeyLampPressed){
            g_uwPushLampButtonDelay=300;
            s_bKeyLampPressed=true;
            if (g_bLightState)
                g_bRequestLightOff=true;
            else
                g_bRequestLightOn=true;
        }

        if (!LAMP_SWITCH)
            s_bKeyLampPressed=false;
    }
}


//--------------------------------------------------------------
void sys_control_light(void){

/*
#ifndef SYS_CANOPEN

    switch (g_ucLightControllerState){

        case LIGHT_INIT:
            g_ucLightControllerState=LIGHT_HOME;
            break;
        case LIGHT_HOME:
            TIMER_STOP;
            if (g_bRequestLightOff)                         //0.52
                g_bRequestLightOff=false;
            if (!g_bRequestLightOn && !g_bRequestLightMan)
                break;
            if (g_bRequestLightOn || g_bRequestLightMan){   //0.48
                TIMER_START;
                g_bRequestLightMan=false;
                g_bRequestLightOn=false;
                g_uwLightTimer=g_CanLightTimer;

                g_bLightState=true;
                g_ucLightControllerState=LIGHT_ON;
                break;
            }
            //Man Light
            TIMER_START;
            g_bRequestLightOn=false;
            g_bRequestLightMan=false;
            g_uwLightTimer=LIGHT_TIME_MAN;
            g_bLightState=true;
            g_ucLightControllerState=LIGHT_ON;
            break;
        case LIGHT_ON:
            if (g_bRequestLightOff){
                g_bRequestLightOff=false;
                g_uwLightTimer=0;
                g_bLightState=false;
                g_ucLightControllerState=LIGHT_HOME;
                break;
            }
            if (g_bRequestLightOn || g_bRequestLightMan){   //0.52
                g_bRequestLightMan=false;
                g_bRequestLightOn=false;
            }
            if (g_uwLightTimer)
                break;
            g_bLightState=false;
            g_ucLightControllerState=LIGHT_HOME;
            break;
    }

#else
*/
    switch (g_ucLightControllerState)
    {
    case LIGHT_PREP_OFF:
        Luce_TIMER_STOP();
        g_bLightState = FALSE;
        g_ucLightControllerState = LIGHT_OFF;
        break;

    case LIGHT_OFF:
        if ((g_bRequestLightMan == TRUE) || (g_bRequestLightOn == TRUE))
        {
            g_bRequestLightMan = FALSE;
            g_bRequestLightOn = FALSE;
            g_ucLightControllerState = LIGHT_PREP_TRIGGERED_TASTO;
        }
        else
        {
            if (RequestLightStatus == COM_LIGHT_ON)
            {
                RequestLightStatus = COM_LIGHT_IDLE;
                g_ucLightControllerState = LIGHT_PREP_ON_INFINITO;
            }
            else if (RequestLightStatus == COM_LIGHT_OFF)
            {
                RequestLightStatus = COM_LIGHT_IDLE;
                g_ucLightControllerState = LIGHT_PREP_OFF;
            }
            else if (RequestLightStatus == COM_LIGHT_TRIG)
            {
                RequestLightStatus = COM_LIGHT_IDLE;
                g_ucLightControllerState = LIGHT_PREP_TRIGGERED_CAN;
            }
        }
        break;

    case LIGHT_PREP_ON_INFINITO:
        Luce_TIMER_START();
        g_bLightState = TRUE;
        g_uwLightTimer = 9000;      // 15 minuti
        g_ucLightControllerState = LIGHT_ON_INFINITO;
        break;

    case LIGHT_ON_INFINITO:
        g_bRequestLightMan = FALSE;
        g_bRequestLightOn = FALSE;
        if (g_bRequestLightOff == TRUE)
        {
            g_bRequestLightOff = FALSE;
            g_ucLightControllerState = LIGHT_PREP_OFF;
        }
        else
        {
            if (g_uwLightTimer == 0)        // controllo scadenza timer
            {
                g_ucLightControllerState = LIGHT_PREP_OFF;
            }

            if (RequestLightStatus == COM_LIGHT_TRIG)
            {
                RequestLightStatus = COM_LIGHT_IDLE;
                g_ucLightControllerState = LIGHT_PREP_TRIGGERED_CAN;
            }
            else if (RequestLightStatus == COM_LIGHT_OFF)
            {
                RequestLightStatus = COM_LIGHT_IDLE;
                g_ucLightControllerState = LIGHT_PREP_OFF;
            }
            else if (RequestLightStatus == COM_LIGHT_ON)
            {
                RequestLightStatus = COM_LIGHT_IDLE;
                g_ucLightControllerState = LIGHT_PREP_ON_INFINITO;
            }
        }
        break;

    case LIGHT_PREP_TRIGGERED_TASTO:
        Luce_TIMER_START();
        g_bLightState = TRUE;
        if (KeyLightTimer < 50)
        {
            g_uwLightTimer = 50;
        }
        else
        {
            g_uwLightTimer = KeyLightTimer;
        }
        g_ucLightControllerState = LIGHT_TRIGGERED;
        break;

    case LIGHT_PREP_TRIGGERED_CAN:
        Luce_TIMER_START();
        g_bLightState = TRUE;
        if (g_CanLightTimer == 0)
        {
            g_ucLightControllerState = LIGHT_PREP_ON_INFINITO;
        }
        else
        {
            g_uwLightTimer = g_CanLightTimer;
            g_ucLightControllerState = LIGHT_TRIGGERED;
        }
        break;

    case LIGHT_TRIGGERED:
        g_bRequestLightMan = FALSE;
        g_bRequestLightOn = FALSE;
        if (g_bRequestLightOff == TRUE)
        {
            g_bRequestLightOff = FALSE;
            g_ucLightControllerState = LIGHT_PREP_OFF;
        }
        else
        {
            if (g_uwLightTimer == 0)        // controllo scadenza timer
            {
                g_ucLightControllerState = LIGHT_PREP_OFF;
            }

            if (RequestLightStatus == COM_LIGHT_TRIG)
            {
                RequestLightStatus = COM_LIGHT_IDLE;
                g_ucLightControllerState = LIGHT_PREP_TRIGGERED_CAN;
            }
            else if (RequestLightStatus == COM_LIGHT_OFF)
            {
                RequestLightStatus = COM_LIGHT_IDLE;
                g_ucLightControllerState = LIGHT_PREP_OFF;
            }
            else if (RequestLightStatus == COM_LIGHT_ON)
            {
                RequestLightStatus = COM_LIGHT_IDLE;
                g_ucLightControllerState = LIGHT_PREP_ON_INFINITO;
            }
        }
        break;

    }


//#endif
}





//******************************************************************
//  selezione dei formati fissi
//******************************************************************
void SelezioneFormatoFisso(void)
{
    UINT16 uwCross, uwLong, uwIris;
    UINT16 tempFormat, tempSteps;

    if ((RemoteStatus.bytes[0] & 0x80) == 0)        // input per FORMATO CAMPO QUADRO
    {
        LastFixedIris = 0;
        fluoroscopia_on = FALSE;
        g_ucFixedFormat = 0;
        if (RemoteStatus.bytes[0] & 0x40)
            g_ucFixedFormat = 1;
        else if (RemoteStatus.bytes[0] & 0x20)
            g_ucFixedFormat = 2;
        else if (RemoteStatus.bytes[0] & 0x10)
            g_ucFixedFormat = 3;
        else if (RemoteStatus.bytes[0] & 0x08)
            g_ucFixedFormat = 4;
        else if (RemoteStatus.bytes[0] & 0x04)
            g_ucFixedFormat = 5;

        if (g_ucFixedFormat)
        {
            if (g_ucFixedFormat != LastFixedShutter)
            {
                LastFixedShutter = g_ucFixedFormat;

                NewIrisFormat[FRMT_VERT_003] = TRUE;    // muove iride se esiste
                FormatoIride[FRMT_VERT_003] = 0;        // e' finto perche' porta ai passi massimi

                if (ee_ConfigColl.ucCmUnit == CM_UNIT)
                {
                    uwCross = (UINT16)ee_FixFormatColl.ucFixedCmCross[g_ucFixedFormat-1] * 10;
                    uwLong = (UINT16)ee_FixFormatColl.ucFixedCmLong[g_ucFixedFormat-1] * 10;
                }
                else
                {
                    uwCross = (UINT16)ee_FixFormatColl.ucFixedInCross[g_ucFixedFormat-1] * 10;
                    uwCross = inches_to_cm(uwCross);
                    uwLong = (UINT16)ee_FixFormatColl.ucFixedInLong[g_ucFixedFormat-1] * 10;
                    uwLong = inches_to_cm(uwLong);
                }

                FormatoCross[FRMT_VERT_003] = uwCross;
                FormatoLong[FRMT_VERT_003] = uwLong;
                NewFormat[FRMT_VERT_003] = TRUE;
            }
        }
        else
        {
            LastFixedShutter = 0;
            //asm(" nop");
        }
    }
    else                // qui FORMATO CAMPO TONDO IRIDE
    {
        LastFixedShutter = 0;
        if ((ee_ConfigColl.ucIrisPresent) &&
            (ee_ConfigColl.ucIrisEnabled))
        {
            fluoroscopia_on = TRUE;
            g_ucFixedFormat = 0;
            if (RemoteStatus.bytes[0] & 0x40)
                g_ucFixedFormat = 1;
            else if (RemoteStatus.bytes[0] & 0x20)
                g_ucFixedFormat = 2;
            else if (RemoteStatus.bytes[0] & 0x10)
                g_ucFixedFormat = 3;
            else if (RemoteStatus.bytes[0] & 0x08)
                g_ucFixedFormat = 4;
            else if (RemoteStatus.bytes[0] & 0x04)
                g_ucFixedFormat = 5;

            if (g_ucFixedFormat)
            {
                if (g_ucFixedFormat != LastFixedIris)
                {
                    LastFixedIris = g_ucFixedFormat;

                    if (ee_ConfigColl.ucCmUnit == CM_UNIT)
                    {
                        uwIris = (UINT16)ee_FixFormatColl.ucFixedCmIris[g_ucFixedFormat-1] * 10;
                    }
                    else
                    {
                        uwIris = (UINT16)ee_FixFormatColl.ucFixedInIris[g_ucFixedFormat-1] * 10;
                        uwIris = inches_to_cm(uwIris);
                    }
                    if (uwIris > iride_calib_mm[IRIS_FORMAT_MAX + 1])    // apre al massimo
                        uwIris = iride_calib_mm[IRIS_FORMAT_MAX + 1];    // apre al massimo

                    NewIrisFormat[FRMT_VERT_003] = TRUE;    // muove iride se esiste
                    FormatoIride[FRMT_VERT_003] = uwIris;        // e' finto perche' porta ai passi massimi

                    LinIrisOverDff(g_uwActualDff, uwIris, &tempSteps);
                    LinIrisMm(g_uwActualDff, (uword)tempSteps, &tempFormat, FALSE);
                    uwCross = tempFormat + (UINT16)ee_ConfigColl.OffsetLamelle;    // si mette a tot mm in piu'
                    uwLong = uwCross;
                    FormatoCross[FRMT_VERT_003] = uwCross;
                    FormatoLong[FRMT_VERT_003] = uwLong;
                    NewFormat[FRMT_VERT_003] = TRUE;
                }
            }
            else
            {
                LastFixedIris = 0;
            }
        }
        else
        {
            fluoroscopia_on = FALSE;
        }
    }
}




//******************************************************************
//  controllo del messaggio da ASR003
//******************************************************************
void sys_control_remote(void)
{

    UINT16 uwTempDff1, uwTempDff2;
    long lTemp;
    static bool s_bExtLightStatus=false;
    static UINT8 counter = 0;

    if (g_bInitEnd == FALSE)
        return;

    if (!g_bNewRemote)
        return;
    g_bNewRemote=false;

    if (CollInReset)        // se in fase di reset non controlla messaggi da scheda esterna
        return;

    //Calibration Jumper on ASR003
    if (RemoteStatus.data.bCalibration)
        g_bUserCalRequest = true;
    else
        g_bUserCalRequest = false;

    if (counter < 2)
    {
        counter++;
        return;
    }



    //New Command to Open/Close blades?
    if (RemoteStatus.data.bOpenIris)
        g_ucOpenCloseFromCan |= OPEN_IRIS;
    else
        g_ucOpenCloseFromCan &= ~OPEN_IRIS;
    if (RemoteStatus.data.bCloseIris)
        g_ucOpenCloseFromCan |= CLOSE_IRIS;
    else
        g_ucOpenCloseFromCan &= ~CLOSE_IRIS;
    if (RemoteStatus.data.bOpenLong)
        g_ucOpenCloseFromCan |= OPEN_LONG;
    else
        g_ucOpenCloseFromCan &= ~OPEN_LONG;
    if (RemoteStatus.data.bCloseLong)
        g_ucOpenCloseFromCan |= CLOSE_LONG;
    else
        g_ucOpenCloseFromCan &= ~CLOSE_LONG;
    if (RemoteStatus.data.bOpenCross)
        g_ucOpenCloseFromCan |= OPEN_CROSS;
    else
        g_ucOpenCloseFromCan &= ~OPEN_CROSS;
    if (RemoteStatus.data.bCloseCross)
        g_ucOpenCloseFromCan |= CLOSE_CROSS;
    else
        g_ucOpenCloseFromCan &= ~CLOSE_CROSS;

    if (g_ucOpenCloseFromCan & MOVE_CROSS) // se si muove una lamella non deve seguire l'iride
        ShutterFollow[STEPPER_CROSS] = FALSE;
    if (g_ucOpenCloseFromCan & MOVE_LONG)
        ShutterFollow[STEPPER_LONG] = FALSE;

/*
    if (g_uwMovingTimer)
        return;
*/

//***********************
//  sezione DFF
//***********************

    if (g_ucOrienteer == RX_TOMO)
    {
        __asm("nop");
    }



    //New Vertical DFF
    if (g_ucOrienteer == RX_UNDER)
    {
        if (TimerNewIncl == 0)
        {
            if (ee_ConfigColl.ucVertDff == VERT_DFF_DIFF)
            {
                lTemp = ((long)RemoteAdc[DFF_STAT] - (long)ee_CalColl.uwStand100) * 500L;
                lTemp /= ((long)ee_CalColl.uwStandUp - (long)ee_CalColl.uwStandLo50);
                lTemp += 1250L;
                uwTempDff1 = lTemp / 10;
                IngressoDFF[DFFVAL_VERT_POT_STATIVO] = uwTempDff1;
                if (RemoteAdc[DFF_TAB] < ee_CalColl.uwLowTable)     // posizione tavolo
                {
                    uwTempDff2 = 0;
                }
                else
                {
                    uwTempDff2 = (RemoteAdc[DFF_TAB] - ee_CalColl.uwLowTable) * 25;
                    uwTempDff2 /= (ee_CalColl.uwTable25 - ee_CalColl.uwLowTable);
                }
                IngressoDFF[DFFVAL_VERT_POT_TABLE] = uwTempDff2;
            }
            else if (ee_ConfigColl.ucVertDff == VERT_DFF_SINGLE)
            {
                if (ee_ConfigColl.ucInputDffPot == POT_SINGLE_STAND)    // potenziometro STATIVO
                {
                    lTemp = ((long)RemoteAdc[DFF_STAT] - (long)ee_CalColl.uwStand100) * 500L;
                    lTemp /= ((long)ee_CalColl.uwStandUp - (long)ee_CalColl.uwStandLo50);
                    lTemp += 1000L;
                    uwTempDff1 = lTemp / 10;
                    IngressoDFF[DFFVAL_VERT_POT_STATIVO] = uwTempDff1;
                }
                else    // potenziometro TAVOLO
                {
                    if (RemoteAdc[DFF_TAB] < ee_CalColl.uwLowTable)     // posizione tavolo
                    {
                        uwTempDff2 = 0;
                    }
                    else
                    {
                        uwTempDff2 = (RemoteAdc[DFF_TAB] - ee_CalColl.uwLowTable) * 25;
                        uwTempDff2 /= (ee_CalColl.uwTable25 - ee_CalColl.uwLowTable);
                        uwTempDff2 += ee_CalColl.uwCollTable;
                        uwTempDff2 -= 25;
                    }
                    IngressoDFF[DFFVAL_VERT_POT_TABLE] = uwTempDff2;
                }
            }
        }
    }


        //New Lateral DFF DX
    if (g_ucOrienteer == RX_RIGHT)       // CONTROLLO DFF LATERALE
    {
        switch (ee_ConfigColl.ucLatDffDx)
        {
        case LAT_DFF_DISC:
            if (RemoteStatus.data.bHorDff1)
                uwTempDff1 = ee_CalColl.uwDffHor[0];
            else if (RemoteStatus.data.bHorDff2)
                uwTempDff1 = ee_CalColl.uwDffHor[1];
            else if (RemoteStatus.data.bHorDff3)
                uwTempDff1 = ee_CalColl.uwDffHor[2];
            else if (RemoteStatus.data.bHorDff4)
                uwTempDff1 = ee_CalColl.uwDffHor[3];
            else if (RemoteStatus.data.bHorDff5)
                uwTempDff1 = ee_CalColl.uwDffHor[4];
            else if (RemoteStatus.data.bHorPanDff1)
                uwTempDff1 = ee_CalColl.uwDffHor[5];
            else if (RemoteStatus.data.bHorPanDff2)
                uwTempDff1 = ee_CalColl.uwDffHor[6];
            else
                uwTempDff1 = 0;

            if (uwTempDff1 && (uwTempDff1 != IngressoDFF[DFFVAL_LAT_DX_003]))
            {
                NewDFF[DFFVAL_LAT_DX_003] = TRUE;
            }
            IngressoDFF[DFFVAL_LAT_DX_003] = uwTempDff1;
            break;

        case LAT_DFF_POT:
            if (TimerNewIncl == 0)
            {
                lTemp = ((SINT32)RemoteAdc[DFF_STAT] - (SINT32)ee_CalColl.DffDxPotMin);
                lTemp *= ((SINT32)ee_CalColl.DffDxMisMax - (SINT32)ee_CalColl.DffDxMisMin);
                lTemp /= ((SINT32)ee_CalColl.DffDxPotMax - (SINT32)ee_CalColl.DffDxMisMin);
                lTemp += (SINT32)ee_CalColl.DffDxMisMin;
                IngressoDFF[DFFVAL_LAT_DX_003] = (UINT16)lTemp;
            }
            break;
        }
    }
    else
    {
        IngressoDFF[DFFVAL_LAT_DX_003] = 0;
    }


    //New Lateral DFF SX
    if (g_ucOrienteer == RX_LEFT)       // CONTROLLO DFF LATERALE
    {
        switch (ee_ConfigColl.ucLatDffSx)
        {
        case LAT_DFF_DISC:
            if (RemoteStatus.data.bHorDff1)
                uwTempDff1 = ee_CalColl.uwDffHor[0];
            else if (RemoteStatus.data.bHorDff2)
                uwTempDff1 = ee_CalColl.uwDffHor[1];
            else if (RemoteStatus.data.bHorDff3)
                uwTempDff1 = ee_CalColl.uwDffHor[2];
            else if (RemoteStatus.data.bHorDff4)
                uwTempDff1 = ee_CalColl.uwDffHor[3];
            else if (RemoteStatus.data.bHorDff5)
                uwTempDff1 = ee_CalColl.uwDffHor[4];
            else if (RemoteStatus.data.bHorPanDff1)
                uwTempDff1 = ee_CalColl.uwDffHor[5];
            else if (RemoteStatus.data.bHorPanDff2)
                uwTempDff1 = ee_CalColl.uwDffHor[6];
            else
                uwTempDff1 = 0;

            if (uwTempDff1 && (uwTempDff1 != IngressoDFF[DFFVAL_LAT_SX_003]))
            {
                NewDFF[DFFVAL_LAT_SX_003] = TRUE;
            }
            IngressoDFF[DFFVAL_LAT_SX_003] = uwTempDff1;
            break;

        case LAT_DFF_POT:
            if (TimerNewIncl == 0)
            {
                lTemp = ((SINT32)RemoteAdc[DFF_STAT] - (SINT32)ee_CalColl.DffSxPotMin);
                lTemp *= ((SINT32)ee_CalColl.DffSxMisMax - (SINT32)ee_CalColl.DffSxMisMin);
                lTemp /= ((SINT32)ee_CalColl.DffSxPotMax - (SINT32)ee_CalColl.DffSxMisMin);
                lTemp += (SINT32)ee_CalColl.DffSxMisMin;
                IngressoDFF[DFFVAL_LAT_SX_003] = (UINT16)lTemp;
            }
            break;
        }
    }
    else
    {
        IngressoDFF[DFFVAL_LAT_SX_003] = 0;
    }



//****************************************
//  sezione FORMATO e BUCKY
//****************************************

    //New Vertical Bucky?
    if (g_ucOrienteer == RX_UNDER)
    {
        switch (ee_ConfigColl.ucVertRec)
        {
        case VERT_REC_BUCKY:
            CalcolaBucky(RX_UNDER, FRMT_VERT_003);
            break;
        case VERT_REC_FISSI:
            SelezioneFormatoFisso();
            break;
        }
    }
    else
    {
        LastFixedShutter = 0;
        LastFixedIris = 0;
        CassettoStabile[RX_UNDER] = FALSE;
        g_bTableCassette[RX_UNDER] = FALSE;
        timer_wait_bucky[RX_UNDER] = TIME_WAIT_BUCKY;
    }


    //New Left Bucky?
    if (g_ucOrienteer == RX_LEFT)
    {
        if (ee_ConfigColl.ucLatRecSx == LAT_REC_BUCKY)
        {
            CalcolaBucky(RX_LEFT, FRMT_LAT_SX_003);
        }
    }
    else
    {
        CassettoStabile[RX_LEFT] = FALSE;
        g_bTableCassette[RX_LEFT] = FALSE;
        timer_wait_bucky[RX_LEFT] = TIME_WAIT_BUCKY;
    }

    //New Right Bucky?
    if (g_ucOrienteer == RX_RIGHT)
    {
        if (ee_ConfigColl.ucLatRecDx == LAT_REC_BUCKY)
        {
            CalcolaBucky(RX_RIGHT, FRMT_LAT_DX_003);
        }
    }
    else
    {
        CassettoStabile[RX_RIGHT] = FALSE;
        g_bTableCassette[RX_RIGHT] = FALSE;
        timer_wait_bucky[RX_RIGHT] = TIME_WAIT_BUCKY;
    }



//***********************
//  gestione FILTRO
//***********************

    //New Command for Filter?
    if (RemoteStatus.data.bFilter1)
        g_ucPuntaFiltro = 0;
    if (RemoteStatus.data.bFilter2)
        g_ucPuntaFiltro = 3;
    if (RemoteStatus.data.bFilter3)
        g_ucPuntaFiltro = 2;
    if (RemoteStatus.data.bFilter4)
        g_ucPuntaFiltro = 1;
    g_ucFilterRequired = ee_FilterConfig.Config_Filtro[g_ucPuntaFiltro];

    //New Command for Lamp?
    if (RemoteStatus.data.bLamp){
        if (!s_bExtLightStatus){
            s_bExtLightStatus=true;
            if (g_bLightState)
                g_bRequestLightOff=true;
            else
                g_bRequestLightOn=true;
        }
    } else {
        s_bExtLightStatus=false;
    }

    //New Manual Request?
    if (RemoteStatus.data.bManual){
        g_bManualRequestFromRemote=true;
    } else {
        g_bManualRequestFromRemote=false;
    }

    //New Tomography?
    if (RemoteStatus.data.bTomography){
        g_bTomography=true;
    } else {
        g_bTomography=false;
    }

    //Exclude Inclinometer
    if (RemoteStatus.data.bInclExcl)
        g_bExclIncl=true;
    else
        g_bExclIncl=false;

}



//***********************************************************
//  calcolo del formato e presenza bucky
//***********************************************************
void CalcolaBucky(UINT8 incl, UINT8 formato)
{
    UINT16 uwCross, uwLong;
    UINT8 bCross, bLong;
    UINT8 presCass;
    UINT8 flag;

    flag = TRUE;
    switch (incl)
    {
    case RX_UNDER:
        bCross = BUCKY_CROSS;
        bLong = BUCKY_LONG;
        presCass = RemoteStatus.data.bTableBucky;
        break;
    case RX_LEFT:
        bCross = LEFT_CROSS;
        bLong = LEFT_LONG;
        presCass = RemoteStatus.data.bLeftBucky || RemoteStatus.data.bLeftPan;
        if (RemoteStatus.data.bLeftPan)
        {
            uwCross = PAN_CROSS;
            uwLong = PAN_LONG;
            flag = FALSE;
        }
        break;
    case RX_RIGHT:
        bCross = RIGHT_CROSS;
        bLong = RIGHT_LONG;
        presCass = RemoteStatus.data.bRightBucky || RemoteStatus.data.bRightPan;
        if (RemoteStatus.data.bRightPan)
        {
            uwCross = PAN_CROSS;
            uwLong = PAN_LONG;
            flag = FALSE;
        }
        break;
    }


    if (flag == TRUE)
    {
        if (ee_ConfigColl.ucCmUnit == CM_UNIT)
        {
            uwCross = sys_scan_cross(RemoteAdc[bCross], incl, true)*10;
            uwLong = sys_scan_long(RemoteAdc[bLong], incl, true)*10;
        }
        else
        {
            //Inches
            uwCross = sys_scan_cross(RemoteAdc[bCross], incl, false)*10;
            uwLong = sys_scan_long(RemoteAdc[bLong], incl, false)*10;
            uwCross = inches_to_cm(uwCross);
            uwLong = inches_to_cm(uwLong);
        }
    }

    if (val_abs(FormatoCross[formato], uwCross) > 5)
    {
        timer_wait_bucky[incl] = TIME_WAIT_BUCKY;
        CassettoStabile[incl] = FALSE;
        FormatoCross[formato] = uwCross;
    }
    if (val_abs(FormatoLong[formato], uwLong) > 5)
    {
        timer_wait_bucky[incl] = TIME_WAIT_BUCKY;
        CassettoStabile[incl] = FALSE;
        FormatoLong[formato] = uwLong;
    }

    if (presCass)  // se cassetto inserito
    {
        if (timer_wait_bucky[incl] == 0)
        {
            if (CassettoStabile[incl] == FALSE)
            {
                CassettoStabile[incl] = TRUE;
                g_bTableCassette[incl] = TRUE;
                NewFormat[formato] = TRUE;
            }
        }
    }
    else
    {
        CassettoStabile[incl] = FALSE;
        g_bTableCassette[incl] = FALSE;
        timer_wait_bucky[incl] = TIME_WAIT_BUCKY;
    }
}










//--------------------------------------------------------------
uword sys_scan_cross(uword uwTempValue, uchar ucOrient, bool bCm){
    uchar ucIndex;
    uword uwTemp1;
    uword uwTemp2;
    uword uwDelta;
    uword uwCrossCandidate[MAX_CASSETTE];
    uword uwDiffCandidate1;
    uword uwDiffCandidate2;
    uchar ucCandidate;

    ucIndex=0;
    memset(&uwCrossCandidate, MAX_CASSETTE, 0);
    uwDiffCandidate1=0;
    uwDiffCandidate2=0;
    ucCandidate=0;

    if (bCm){
        switch (ucOrient){
            case RX_UNDER:
                ucCandidate=0;
                while (true){
                    if (ucIndex<2)
                        uwDelta=DELTA_ADC_07_MM;
                    else
                        uwDelta=DELTA_ADC_15_MM;

                    if (ee_TopBuckyColl.uwTopBuckyCrossAdcCm[ucIndex]){
                        uwTemp1=ee_TopBuckyColl.uwTopBuckyCrossAdcCm[ucIndex]-uwDelta;
                        uwTemp2=ee_TopBuckyColl.uwTopBuckyCrossAdcCm[ucIndex]+uwDelta;
                    }
                    else{
                        uwTemp1=0;
                        uwTemp2=0;
                    }

                    if ((uwTempValue>=uwTemp1)&&(uwTempValue<=uwTemp2)){
                        uwCrossCandidate[ucCandidate]=ee_TopBuckyColl.uwTopBuckyCrossCm[ucIndex];
                        ucCandidate++;
                    }

                    if (ucIndex<(MAX_CASSETTE-1))
                        ucIndex++;
                    else
                        break;
                }
                switch (ucCandidate){
                    case 1:
                        return (uwCrossCandidate[0]);
                    case 2:
                        uwDiffCandidate1 = val_abs(uwCrossCandidate[0], uwTempValue);
                        uwDiffCandidate2 = val_abs(uwCrossCandidate[1], uwTempValue);
                        if (uwDiffCandidate1<=uwDiffCandidate1)
                            return (uwCrossCandidate[0]);
                        else
                            return (uwCrossCandidate[1]);
                    default:
                        break;
                }
                return 0;
            case RX_LEFT:
                while (true){
                    if (ucIndex<2)
                        uwDelta=DELTA_ADC_07_MM;
                    else
                        uwDelta=DELTA_ADC_15_MM;

                    if (ee_SxBuckyColl.uwSxBuckyCrossAdcCm[ucIndex]){
                        uwTemp1=ee_SxBuckyColl.uwSxBuckyCrossAdcCm[ucIndex]-uwDelta;
                        uwTemp2=ee_SxBuckyColl.uwSxBuckyCrossAdcCm[ucIndex]+uwDelta;
                    } else {
                        uwTemp1=0;
                        uwTemp2=0;
                    }

                    if ((uwTempValue>=uwTemp1)&&(uwTempValue<=uwTemp2)){
                        uwCrossCandidate[ucCandidate]=ee_SxBuckyColl.uwSxBuckyCrossCm[ucIndex];
                        ucCandidate++;
                    }

                    if (ucIndex<(MAX_CASSETTE-1))
                        ucIndex++;
                    else
                        break;
                }
                switch (ucCandidate){
                    case 1:
                        return (uwCrossCandidate[0]);
                    case 2:
                        uwDiffCandidate1 = val_abs(uwCrossCandidate[0], uwTempValue);
                        uwDiffCandidate2 = val_abs(uwCrossCandidate[1], uwTempValue);
                        if (uwDiffCandidate1<=uwDiffCandidate1)
                            return (uwCrossCandidate[0]);
                        else
                            return (uwCrossCandidate[1]);
                    default:
                        break;
                }
                return 0;
            case RX_RIGHT:
                while (true){
                    if (ucIndex<2)
                        uwDelta=DELTA_ADC_07_MM;
                    else
                        uwDelta=DELTA_ADC_15_MM;

                    if (ee_DxBuckyColl.uwDxBuckyCrossAdcCm[ucIndex]){
                        uwTemp1=ee_DxBuckyColl.uwDxBuckyCrossAdcCm[ucIndex]-uwDelta;
                        uwTemp2=ee_DxBuckyColl.uwDxBuckyCrossAdcCm[ucIndex]+uwDelta;
                    } else {
                        uwTemp1=0;
                        uwTemp2=0;
                    }

                    if ((uwTempValue>=uwTemp1)&&(uwTempValue<=uwTemp2)){
                        uwCrossCandidate[ucCandidate]=ee_DxBuckyColl.uwDxBuckyCrossCm[ucIndex];
                        ucCandidate++;
                    }

                    if (ucIndex<(MAX_CASSETTE-1))
                        ucIndex++;
                    else
                        break;
                }
                switch (ucCandidate){
                    case 1:
                        return (uwCrossCandidate[0]);
                    case 2:
                        uwDiffCandidate1 = val_abs(uwCrossCandidate[0], uwTempValue);
                        uwDiffCandidate2 = val_abs(uwCrossCandidate[1], uwTempValue);
                        if (uwDiffCandidate1<=uwDiffCandidate1)
                            return (uwCrossCandidate[0]);
                        else
                            return (uwCrossCandidate[1]);
                    default:
                        break;
                }
                return 0;
            default:
                return 0;
        }
        return 0;
    }


    //Inches
    switch (ucOrient){
        case RX_UNDER:
            while (true){
                if (ucIndex<2)
                    uwDelta=DELTA_ADC_03_IN;
                else
                    uwDelta=DELTA_ADC_06_IN;

                if (ee_TopBuckyColl.uwTopBuckyCrossAdcIn[ucIndex]){
                    uwTemp1=ee_TopBuckyColl.uwTopBuckyCrossAdcIn[ucIndex]-uwDelta;
                    uwTemp2=ee_TopBuckyColl.uwTopBuckyCrossAdcIn[ucIndex]+uwDelta;
                }
                else{
                    uwTemp1=0;
                    uwTemp2=0;
                }

                if ((uwTempValue>=uwTemp1)&&(uwTempValue<=uwTemp2)){
                    uwCrossCandidate[ucCandidate]=ee_TopBuckyColl.uwTopBuckyCrossIn[ucIndex];
                    ucCandidate++;
                }

                if (ucIndex<(MAX_CASSETTE-1))
                    ucIndex++;
                else
                    break;
            }
            switch (ucCandidate){
                case 1:
                    return (uwCrossCandidate[0]);
                case 2:
                    uwDiffCandidate1 = val_abs(uwCrossCandidate[0], uwTempValue);
                    uwDiffCandidate2 = val_abs(uwCrossCandidate[1], uwTempValue);
                    if (uwDiffCandidate1<=uwDiffCandidate1)
                        return (uwCrossCandidate[0]);
                    else
                        return (uwCrossCandidate[1]);
                default:
                    break;
            }
            return 0;
        case RX_LEFT:
            while (true){
                if (ucIndex<2)
                    uwDelta=DELTA_ADC_03_IN;
                else
                    uwDelta=DELTA_ADC_06_IN;

                if (ee_SxBuckyColl.uwSxBuckyCrossAdcIn[ucIndex]){
                    uwTemp1=ee_SxBuckyColl.uwSxBuckyCrossAdcIn[ucIndex]-uwDelta;
                    uwTemp2=ee_SxBuckyColl.uwSxBuckyCrossAdcIn[ucIndex]+uwDelta;
                } else {
                    uwTemp1=0;
                    uwTemp2=0;
                }

                if ((uwTempValue>=uwTemp1)&&(uwTempValue<=uwTemp2)){
                    uwCrossCandidate[ucCandidate]=ee_SxBuckyColl.uwSxBuckyCrossIn[ucIndex];
                    ucCandidate++;
                }

                if (ucIndex<(MAX_CASSETTE-1))
                    ucIndex++;
                else
                    break;
            }
            switch (ucCandidate){
                case 1:
                    return (uwCrossCandidate[0]);
                case 2:
                    uwDiffCandidate1 = val_abs(uwCrossCandidate[0], uwTempValue);
                    uwDiffCandidate2 = val_abs(uwCrossCandidate[1], uwTempValue);
                    if (uwDiffCandidate1<=uwDiffCandidate1)
                        return (uwCrossCandidate[0]);
                    else
                        return (uwCrossCandidate[1]);
                default:
                    break;
            }
            return 0;
        case RX_RIGHT:
            while (true){
                if (ucIndex<2)
                    uwDelta=DELTA_ADC_03_IN;
                else
                    uwDelta=DELTA_ADC_06_IN;

                if (ee_DxBuckyColl.uwDxBuckyCrossAdcIn[ucIndex]){
                    uwTemp1=ee_DxBuckyColl.uwDxBuckyCrossAdcIn[ucIndex]-uwDelta;
                    uwTemp2=ee_DxBuckyColl.uwDxBuckyCrossAdcIn[ucIndex]+uwDelta;
                } else {
                    uwTemp1=0;
                    uwTemp2=0;
                }

                if ((uwTempValue>=uwTemp1)&&(uwTempValue<=uwTemp2)){
                    uwCrossCandidate[ucCandidate]=ee_DxBuckyColl.uwDxBuckyCrossIn[ucIndex];
                    ucCandidate++;
                }

                if (ucIndex<(MAX_CASSETTE-1))
                    ucIndex++;
                else
                    break;
            }
            switch (ucCandidate){
                case 1:
                    return (uwCrossCandidate[0]);
                case 2:
                    uwDiffCandidate1 = val_abs(uwCrossCandidate[0], uwTempValue);
                    uwDiffCandidate2 = val_abs(uwCrossCandidate[1], uwTempValue);
                    if (uwDiffCandidate1<=uwDiffCandidate1)
                        return (uwCrossCandidate[0]);
                    else
                        return (uwCrossCandidate[1]);
                default:
                    break;
            }
            return 0;
        default:
            return 0;
    }

    return 0;
}

//--------------------------------------------------------------
uword sys_scan_long(uword uwTempValue, uchar ucOrient, bool bCm){
    uchar ucIndex;
    uword uwTemp1;
    uword uwTemp2;
    uword uwDelta;
    uword uwLongCandidate[MAX_CASSETTE];
    uword uwDiffCandidate1;
    uword uwDiffCandidate2;
    uchar ucCandidate;

    ucIndex=0;
    memset(&uwLongCandidate, MAX_CASSETTE, 0);
    uwDiffCandidate1=0;
    uwDiffCandidate2=0;
    ucCandidate=0;

    if (bCm){
        switch (ucOrient){
            case RX_UNDER:
                while (true){
                    if (ucIndex<2)
                        uwDelta=DELTA_ADC_07_MM;
                    else
                        uwDelta=DELTA_ADC_15_MM;

                    if (ee_TopBuckyColl.uwTopBuckyLongAdcCm[ucIndex]){
                        uwTemp1=ee_TopBuckyColl.uwTopBuckyLongAdcCm[ucIndex]-uwDelta;
                        uwTemp2=ee_TopBuckyColl.uwTopBuckyLongAdcCm[ucIndex]+uwDelta;
                    } else {
                        uwTemp1 = 0;
                        uwTemp2 = 0;
                    }
                    if ((uwTempValue>uwTemp1)&&(uwTempValue<uwTemp2)){
                        uwLongCandidate[ucCandidate]=ee_TopBuckyColl.uwTopBuckyLongCm[ucIndex];
                        ucCandidate++;
                    }

                    if (ucIndex<(MAX_CASSETTE-1))
                        ucIndex++;
                    else
                        break;
                }
                switch (ucCandidate){
                    case 1:
                        return (uwLongCandidate[0]);
                    case 2:
                        uwDiffCandidate1 = val_abs(uwLongCandidate[0], uwTempValue);
                        uwDiffCandidate2 = val_abs(uwLongCandidate[1], uwTempValue);
                        if (uwDiffCandidate1<=uwDiffCandidate1)
                            return (uwLongCandidate[0]);
                        else
                            return (uwLongCandidate[1]);
                    default:
                        break;
                }
                return 0;
            case RX_LEFT:
                while (true){
                    if (ucIndex<2)
                        uwDelta=DELTA_ADC_07_MM;
                    else
                        uwDelta=DELTA_ADC_15_MM;

                    if (ee_SxBuckyColl.uwSxBuckyLongAdcCm[ucIndex]){
                        uwTemp1=ee_SxBuckyColl.uwSxBuckyLongAdcCm[ucIndex]-uwDelta;
                        uwTemp2=ee_SxBuckyColl.uwSxBuckyLongAdcCm[ucIndex]+uwDelta;
                    } else {
                        uwTemp1 = 0;
                        uwTemp2 = 0;
                    }

                    if ((uwTempValue>uwTemp1)&&(uwTempValue<uwTemp2)){
                        uwLongCandidate[ucCandidate]=ee_SxBuckyColl.uwSxBuckyLongCm[ucIndex];
                        ucCandidate++;
                    }

                    if (ucIndex<(MAX_CASSETTE-1))
                        ucIndex++;
                    else
                        break;
                }
                switch (ucCandidate){
                    case 1:
                        return (uwLongCandidate[0]);
                    case 2:
                        uwDiffCandidate1 = val_abs(uwLongCandidate[0], uwTempValue);
                        uwDiffCandidate2 = val_abs(uwLongCandidate[1], uwTempValue);
                        if (uwDiffCandidate1<=uwDiffCandidate1)
                            return (uwLongCandidate[0]);
                        else
                            return (uwLongCandidate[1]);
                    default:
                        break;
                }
                return 0;
            case RX_RIGHT:
                while (true){
                    if (ucIndex<2)
                        uwDelta=DELTA_ADC_07_MM;
                    else
                        uwDelta=DELTA_ADC_15_MM;

                    if (ee_DxBuckyColl.uwDxBuckyLongAdcCm[ucIndex]){
                        uwTemp1=ee_DxBuckyColl.uwDxBuckyLongAdcCm[ucIndex]-uwDelta;
                        uwTemp2=ee_DxBuckyColl.uwDxBuckyLongAdcCm[ucIndex]+uwDelta;
                    } else {
                        uwTemp1 = 0;
                        uwTemp2 = 0;
                    }

                    if ((uwTempValue>uwTemp1)&&(uwTempValue<uwTemp2)){
                        uwLongCandidate[ucCandidate]=ee_DxBuckyColl.uwDxBuckyLongCm[ucIndex];
                        ucCandidate++;
                    }

                    if (ucIndex<(MAX_CASSETTE-1))
                        ucIndex++;
                    else
                        break;
                }
                switch (ucCandidate){
                    case 1:
                        return (uwLongCandidate[0]);
                    case 2:
                        uwDiffCandidate1 = val_abs(uwLongCandidate[0], uwTempValue);
                        uwDiffCandidate2 = val_abs(uwLongCandidate[1], uwTempValue);
                        if (uwDiffCandidate1<=uwDiffCandidate1)
                            return (uwLongCandidate[0]);
                        else
                            return (uwLongCandidate[1]);
                    default:
                        break;
                }
                return 0;
            default:
                return 0;
        }
        return 0;
    }

    //Inches
    switch (ucOrient){
        case RX_UNDER:
            while (true){
                if (ucIndex<2)
                    uwDelta=DELTA_ADC_03_IN;
                else
                    uwDelta=DELTA_ADC_06_IN;

                if (ee_TopBuckyColl.uwTopBuckyLongAdcIn[ucIndex]){
                    uwTemp1=ee_TopBuckyColl.uwTopBuckyLongAdcIn[ucIndex]-uwDelta;
                    uwTemp2=ee_TopBuckyColl.uwTopBuckyLongAdcIn[ucIndex]+uwDelta;
                } else {
                    uwTemp1 = 0;
                    uwTemp2 = 0;
                }
                if ((uwTempValue>uwTemp1)&&(uwTempValue<uwTemp2)){
                    uwLongCandidate[ucCandidate]=ee_TopBuckyColl.uwTopBuckyLongIn[ucIndex];
                    ucCandidate++;
                }

                if (ucIndex<(MAX_CASSETTE-1))
                    ucIndex++;
                else
                    break;
            }
            switch (ucCandidate){
                case 1:
                    return (uwLongCandidate[0]);
                case 2:
                    uwDiffCandidate1 = val_abs(uwLongCandidate[0], uwTempValue);
                    uwDiffCandidate2 = val_abs(uwLongCandidate[1], uwTempValue);
                    if (uwDiffCandidate1<=uwDiffCandidate1)
                        return (uwLongCandidate[0]);
                    else
                        return (uwLongCandidate[1]);
                default:
                    break;
            }
            return 0;
        case RX_LEFT:
            while (true){
                if (ucIndex<2)
                    uwDelta=DELTA_ADC_03_IN;
                else
                    uwDelta=DELTA_ADC_06_IN;

                if (ee_SxBuckyColl.uwSxBuckyLongAdcIn[ucIndex]){
                    uwTemp1=ee_SxBuckyColl.uwSxBuckyLongAdcIn[ucIndex]-uwDelta;
                    uwTemp2=ee_SxBuckyColl.uwSxBuckyLongAdcIn[ucIndex]+uwDelta;
                } else {
                    uwTemp1 = 0;
                    uwTemp2 = 0;
                }

                if ((uwTempValue>uwTemp1)&&(uwTempValue<uwTemp2)){
                    uwLongCandidate[ucCandidate]=ee_SxBuckyColl.uwSxBuckyLongIn[ucIndex];
                    ucCandidate++;
                }

                if (ucIndex<(MAX_CASSETTE-1))
                    ucIndex++;
                else
                    break;
            }
            switch (ucCandidate){
                case 1:
                    return (uwLongCandidate[0]);
                case 2:
                    uwDiffCandidate1 = val_abs(uwLongCandidate[0], uwTempValue);
                    uwDiffCandidate2 = val_abs(uwLongCandidate[1], uwTempValue);
                    if (uwDiffCandidate1<=uwDiffCandidate1)
                        return (uwLongCandidate[0]);
                    else
                        return (uwLongCandidate[1]);
                default:
                    break;
            }
            return 0;
        case RX_RIGHT:
            while (true){
                if (ucIndex<2)
                    uwDelta=DELTA_ADC_03_IN;
                else
                    uwDelta=DELTA_ADC_06_IN;

                if (ee_DxBuckyColl.uwDxBuckyLongAdcIn[ucIndex]){
                    uwTemp1=ee_DxBuckyColl.uwDxBuckyLongAdcIn[ucIndex]-uwDelta;
                    uwTemp2=ee_DxBuckyColl.uwDxBuckyLongAdcIn[ucIndex]+uwDelta;
                } else {
                    uwTemp1 = 0;
                    uwTemp2 = 0;
                }

                if ((uwTempValue>uwTemp1)&&(uwTempValue<uwTemp2)){
                    uwLongCandidate[ucCandidate]=ee_DxBuckyColl.uwDxBuckyLongIn[ucIndex];
                    ucCandidate++;
                }

                if (ucIndex<(MAX_CASSETTE-1))
                    ucIndex++;
                else
                    break;
            }
            switch (ucCandidate){
                case 1:
                    return (uwLongCandidate[0]);
                case 2:
                    uwDiffCandidate1 = val_abs(uwLongCandidate[0], uwTempValue);
                    uwDiffCandidate2 = val_abs(uwLongCandidate[1], uwTempValue);
                    if (uwDiffCandidate1<=uwDiffCandidate1)
                        return (uwLongCandidate[0]);
                    else
                        return (uwLongCandidate[1]);
                default:
                    break;
            }
            return 0;
        default:
            return 0;
    }
    return 0;
}
