//--------------------------------------------------------------
// PRIMA STESURA
// Riva ing. Franco M.
// Besana Brianza (MI)
//
// REVISIONI SUCCESSIVE
// Giovanni Corvini
// Melegnano (MI)
//--------------------------------------------------------------
//
// PROJECT:	  RSR008 - Low Cost Collimator
// FILE:      sys_high_level.h
// REVISIONS: 0.00 - 22/09/2008 - First Definition
//
//--------------------------------------------------------------

#define TIME_WAIT_IRIDE             30      // n ms

#define TIME_STAB_INCLINAZIONE      5      // n*100 ms

#define COM_LIGHT_IDLE  0
#define COM_LIGHT_OFF   1
#define COM_LIGHT_ON    2
#define COM_LIGHT_TRIG  3


#define IRIS_OPEN 		0
#define IRIS_CLOSE 		1
#define TIMEOUT_RESET_IRIDE     200  // in 1/10 secondo
#define TIMEOUT_MOVE_IRIDE      100  // in 1/10 secondo per raggiungimento posizione

#define MIN_CROSS				50
#define MAX_CROSS				600
#define MAX_CROSS_MAN			480
#define DELTA_CROSS_OPEN_CLOSE		10
#define DELTA_CROSS_OPEN_CLOSE_MAN	10
#define	DELTA_CROSS_MIN			1
#define	DELTA_CROSS_MAX			20
#define DELAY_CROSS				50
#define DELTA_CROSS_TOL			5
#define INVALID_CROSS_FORMAT	0xFFFF

#define MIN_LONG				50
#define MAX_LONG				600
#define MAX_LONG_MAN			480
#define DELTA_LONG_OPEN_CLOSE	10
#define DELTA_LONG_OPEN_CLOSE_MAN	10
#define	DELTA_LONG_MIN			1
#define	DELTA_LONG_MAX			20
#define DELAY_LONG				50

#define DELTA_DFF_MIN			0       //
#define DELTA_DFF_MAX_POS		25
#define DELTA_DFF_MAX_NEG		-DELTA_DFF_MAX_POS

#define DELTA_PERC_MIN			0       //
#define DELTA_PERC_MAX_POS		100     // corrisponde al 10%
#define DELTA_PERC_MAX_NEG		-DELTA_PERC_MAX_POS

#define DELTA_LONG_TOL			5
#define INVALID_LONG_FORMAT		0xFFFF


#define DELTA_CROSS_MAX_POS		50
#define DELTA_CROSS_MAX_NEG		-DELTA_CROSS_MAX_POS
#define DELTA_LONG_MAX_POS		50
#define DELTA_LONG_MAX_NEG		-DELTA_LONG_MAX_POS

#define CLOSE_CROSS	0x20
#define OPEN_CROSS	0x10
#define CLOSE_LONG	0x08
#define OPEN_LONG 	0x04
#define MOVE_CROSS  (OPEN_CROSS | CLOSE_CROSS)
#define MOVE_LONG   (OPEN_LONG | CLOSE_LONG)
#define CLOSE_IRIS	0x02
#define OPEN_IRIS 	0x01
#define NONE_IRIS   0x00

//#define DFF_MAX		255
#define DFF_MAX		300
#define DFF_MIN		50
#define DFT_MAX		15
#define DFT_MIN		0
#define AL_INCL_MIN	1
#define AL_INCL_MAX	15
#define STEPS_DFF	(DFF_MAX-DFF_MIN+1+25) // numero di DFF presenti + Correzione_DFF
#define NUM_FORMAT	22
#define DFF_DEF		100

#define CAL_FIXED           100UL     // in cm per taratura collimatore
#define OFFSET_CROSS        50UL     // in passi per offset apertura sullo 0
#define OFFSET_LONG         50UL     // in passi per offset apertura sullo 0


#define DELTA_CROSS_1CM	5
#define DELTA_LONG_1CM	5

#define K_FILTER_STEPS_OLD  4262
#define K_FILTER_STEPS_NEW  4439
#define FILTER_RESET_MIN_OLD    100     // numero di passi di differenza tra i 2 fori
#define FILTER_RESET_MAX_OLD    300     // numero di passi di differenza tra i 2 fori
#define FILTER_RESET_MIN_NEW    100     // numero di passi di differenza tra i 2 fori
#define FILTER_RESET_MAX_NEW    300     // numero di passi di differenza tra i 2 fori
#define FILTER_RESET_MIN_DEF    FILTER_RESET_MIN_OLD
#define FILTER_RESET_MAX_DEF    FILTER_RESET_MAX_OLD
#define K_FILTER_STEPS_DEF  K_FILTER_STEPS_OLD
#define MOTORE_FILTRO_DEF   MOTORE_FILTRO_OLD
#define K_FILTER_STEPS_DEF  K_FILTER_STEPS_OLD

#define REV_FILTER_DELAY	200		//0.53
#define FILTER_PAUSE        100

#define DELAY_ORIENTEER	250	//250 msec
#define DELAY_LED		25	//25 msec

#define MIN_CLOSED	30

#define MAX_CROSS_OPENCLOSE	223
#define MAX_LONG_OPENCLOSE	224

#define LIGHT_TIME_0	(15 * BASE_TIMER_LUCE)
#define LIGHT_TIME_1	(30 * BASE_TIMER_LUCE)
#define LIGHT_TIME_2	(45 * BASE_TIMER_LUCE)
#define LIGHT_TIME_3	(60 * BASE_TIMER_LUCE)
#define LIGHT_TIME_MAN	(5 * BASE_TIMER_LUCE)

#define DFF1_FILTER_DEPTH	2
#define DFF1_REDUCE_FILTER	1	//WARNING: DFF_FILTER_DEPTH=(2^DFF_REDUCE_FILTER)

#define DFF2_FILTER_DEPTH	2
#define DFF2_REDUCE_FILTER	1	//WARNING: DFF_FILTER_DEPTH=(2^DFF_REDUCE_FILTER)

#define PAN_CROSS	350
#define PAN_LONG	900


#define CROSS_INIT					'!'
#define CROSS_HOME_MAN				'a'
#define CROSS_HOME_AUTO				'A'
#define CROSS_CHECK_IF_RTZ 			'B'
#define CROSS_GO_TO_HOME			'C'
#define CROSS_HOME_DELAY 			'c'
#define CROSS_GO_TO_POSITION 		'D'
#define CROSS_POSITION_DELAY		'd'
#define CROSS_GO_TO_COLLIMATION 	'E'
#define CROSS_WAIT_STABLE_COLLIMATION 'e'
#define CROSS_HOME_ZERO_DFF			'F'
#define CROSS_REMOTE_MOVE			'f'
#define CROSS_HOLD_FROM_MAN			'G'
#define CROSS_HOLD_FROM_AUTO		'g'

#define LONG_INIT					'!'
#define LONG_HOME_MAN 				'a'
#define LONG_HOME_AUTO				'A'
#define LONG_CHECK_IF_RTZ 			'B'
#define LONG_GO_TO_HOME				'C'
#define LONG_HOME_DELAY 			'c'
#define LONG_GO_TO_POSITION 		'D'
#define LONG_POSITION_DELAY 		'd'
#define LONG_GO_TO_COLLIMATION 		'E'
#define LONG_WAIT_STABLE_COLLIMATION 'e'
#define LONG_HOME_ZERO_DFF			'F'
#define LONG_REMOTE_MOVE			'f'
#define LONG_HOLD_FROM_MAN			'G'
#define LONG_HOLD_FROM_AUTO			'g'

enum {
    IRIS_INIT = 0,
    IRIS_READ_CALIB,
    IRIS_WAIT_CALIB,
    IRIS_INIT_WAIT,
    IRIS_HOME_AUTO_WAIT,
    IRIS_HOME_ERROR,
    IRIS_READY,
    IRIS_WAIT_RILASCIO_TASTO_OPEN,
    IRIS_WAIT_RILASCIO_TASTO_CLOSE,
    IRIS_DISABLED
};


#ifdef _SYS_HIGH_LEVEL_
    #define ex_high
    uchar g_ucCrossStatus=CROSS_INIT;
    uchar g_ucCrossLedReq = RED;
    uchar g_ucLongStatus=LONG_INIT;
    uchar g_ucLongLedReq = RED;

	long g_lCrossActualSteps;
	uchar g_ucActualCross;
	uword g_uwLastReceivedCrossFormat=0;
	bool g_bCrossManual=true;

	long g_lLongActualSteps;
	long g_lLong2ActualSteps;
	uchar g_ucActualLong;
	uword g_uwLastReceivedLongFormat=0;
	bool g_bLongManual=true;

	long g_lIrisActualSteps;
	uchar g_ucActualIris;

	bool g_bTimeToShowAngle = false;
	bool g_bInclSync = true;
	volatile uword g_uwPushLampButtonDelay=0;
	volatile uword g_uwPushFilterButtonDelay=0;
	volatile uword g_uwAlarmCounterOrient;
	volatile uword g_uwTimerLed=0;
	volatile uword g_uwLightTimer=0;
    uchar g_ucManualRequest=0;
    bool g_bBackupAutoLimits=false;
	bool g_bStepperARequiredToStop=false;
	bool g_bStepperBRequiredToStop=false;
	bool g_bCrossMoveInverted=false;
	bool g_bLongMoveInverted=false;
	bool g_bLeftCassette=false;
	bool g_bRightCassette=false;
	bool g_bTomography=false;

//	uword g_uwMaxCrossSteps;	//0.61 Max Steps in RAM
//	uword g_uwMaxLongSteps;

	bool g_bValidLateralFormat=false;

#else
    #define ex_high extern
    extern uchar g_ucCrossStatus;
    extern uchar g_ucCrossLedReq;
    extern uchar g_ucLongStatus;
    extern uchar g_ucLongLedReq;

    extern long g_lCrossActualSteps;
	extern uchar g_ucActualCross;
	extern uword g_uwLastReceivedCrossFormat;
	extern bool g_bCrossManual;

	extern long g_lLongActualSteps;
	extern long g_lLong2ActualSteps;
	extern uchar g_ucActualLong;
	extern uword g_uwLastReceivedLongFormat;
	extern bool g_bLongManual;

	extern long g_lIrisActualSteps;
	extern uchar g_ucActualIris;

	extern bool g_bTimeToShowAngle;
	extern bool	g_bInclSync;
	extern volatile uword g_uwPushLampButtonDelay;
	extern volatile uword g_uwPushFilterButtonDelay;
	extern volatile uword g_uwAlarmCounterOrient;
	extern volatile uword g_uwTimerLed;
	extern volatile uword g_uwLightTimer;
    extern uchar g_ucManualRequest;
    extern bool g_bBackupAutoLimits;
	extern bool g_bStepperARequiredToStop;
	extern bool g_bStepperBRequiredToStop;
	extern bool g_bCrossMoveInverted;
	extern bool g_bLongMoveInverted;
	extern bool g_bLeftCassette;
	extern bool g_bRightCassette;
	extern bool g_bTomography;
//	extern uword g_uwMaxCrossSteps;
//	extern uword g_uwMaxLongSteps;

	extern bool g_bValidLateralFormat;
#endif


ex_high UINT8 g_bTableCassette[5], CassettoStabile[5];
ex_high UINT8 msg_Iride_COM, msg_Iride_DLC, msg_Iride_DATA[8];
ex_high UINT16 msg_Iride_ID;
ex_high UINT8 muovi_iride, set_passi_max, IrideIndipendente;
ex_high UINT8 set_passi_campoiride;
ex_high UINT8 g_ucFixedFormat;
ex_high UINT16 s_uwLastCrossFormat;
ex_high UINT16 s_uwLastLongFormat;


ex_high UINT32 g_MaxSteps[4];
ex_high UINT32 g_RefSize[4];
ex_high UINT32 g_RefSteps[4];
ex_high UINT32 g_ManualSpeed[4];
ex_high UINT8 ManualStatus[4];
ex_high UINT32 LastReceivedFormat[4];
ex_high UINT8 ShutterFollow[4];

ex_high UINT8 TimerLed[8];
ex_high UINT8 LedStatus[8];     // ATTENZIONE: il valore STEPPER_IRIS e' 4

ex_high uchar g_ucIrisStatus;
ex_high uchar g_ucIrisLedReq;

ex_high SINT32 g_nXDegrees, g_nYDegrees;
ex_high	SINT16 g_nDegrees;

ex_high UINT8 EnableFilterKey, EnableLampKey, EnableKnobs, EnableIrisKey, EnableIrisKey_old;
ex_high volatile UINT8 timer_wait_bucky[5];

ex_high UINT8 RequestLightStatus;
ex_high volatile UINT8 TimerNewIncl;

ex_high UINT8 LastFixedShutter;
ex_high UINT8 LastFixedIris;




//********************************
//  PROTOTIPI DI FUNZIONE
//********************************


bool LinCrossMm(uword uwActualDff, uword uwSteps, uword* puwMm);
bool LinLongMm(uword uwActualDff, uword uwSteps, uword* puwMm);
bool LinIrisMm(uword uwActualDff, uword uwSteps, uword* puwMm, UINT8 pot);

void sys_cross_init(void);
void sys_long_init(void);
void sys_filter_process(void);
void sys_iris_process(void);
void sys_control_check_for_manual(void);
void sys_control_led_and_relays(uchar ucLed);
void sys_control_led(void);
void sys_control_inclination(void);
void sys_control_update_data(void);
void sys_control_keyboard(void);
void sys_control_light(void);
void sys_control_remote(void);

void SelezioneFormatoFisso(void);
void CalcolaBucky(UINT8 incl, UINT8 formato);

uword sys_scan_cross(uword uwTempValue, uchar ucOrient, bool bCm);
uword sys_scan_long(uword uwTempValue, uchar ucOrient, bool bCm);
void send_tasto_remoto(UINT8 op);
bool LinIrisOverDff(uword uwActualDff, uword uwActualMm, uword* puwDffSteps);

bool LinCrossOverDff(uword uwActualDff, uword uwActualMm, uword* puwDffSteps);
bool LinLongOverDff(uword uwActualDff, uword uwActualMm, uword* puwDffSteps);
