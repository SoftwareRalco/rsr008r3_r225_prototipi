#define _SYS_TEST_

//--------------------------------------------------------------
// PRIMA STESURA
// Riva ing. Franco M.
// Besana Brianza (MI)
//
// REVISIONI SUCCESSIVE
// Giovanni Corvini
// Melegnano (MI)
//--------------------------------------------------------------
//
// PROJECT:	  RSR008 - Low Cost Collimator
// FILE:      sys_lcd_driver.c
// REVISIONS: 0.00 - 22/09/2008 - First Definition
//
//--------------------------------------------------------------

//--------------------------------------------------------------
// INCLUDE FILES
//--------------------------------------------------------------
#include "include.h"
/*
#include "sfr32C87.h"
#include "sys_defines.h"
#include "sys_can_under_test.h"
#include "sys_lcd_driver.h"
#include "sys_display.h"
#include "virtEE_Variable.h"    // Virtual EEPROM (variable size records) function definitions
#include "sys_control.h"
#include "sys_test.h"
#include "sys_main.h"
*/
#include "string.h"
#include "math.h"

