/*
 * can_main.c
 *
 *  Created on: 24 mag 2017
 *      Author: daniele
 */

#define _SYS_CAN_MAIN_

//--------------------------------------------------------------
// INCLUDE FILES
//--------------------------------------------------------------
#include <math.h>
#include "include.h"
#include "string.h"


//--------------------------------------------------------------
// GLOBAL VARIABLES
//--------------------------------------------------------------
#define CAN_PROCESS_INIT        'I'
#define CAN_PROCESS_HOME        'H'

static uchar g_ucCanProcessState = CAN_PROCESS_INIT;

//--------------------------------------------------------------
// INTERRUPT FUNCTIONS
//--------------------------------------------------------------
//#pragma INTERRUPT /E  can0_int()

//void can0_int(void)
//{
//
//    asm("NOP");
//    asm("NOP");
//    asm("NOP");
//    asm("NOP");
//    asm("NOP");
//    asm("NOP");
//    asm("NOP");
//    asm("NOP");
//    asm("NOP");
//
//
//}




//--------------------------------------------------------------
// LOCAL FUNCTIONS
//--------------------------------------------------------------

//--------------------------------------------------------------
// SYSTEM FUNCTIONS
//--------------------------------------------------------------


//static void sys_can_process_0(void)

void sys_can_7A0(void)
{
    uchar ucRecvBytes[9];
    uchar ucRecDlc;
    UINT16 ApeIride;
    uword uwVertCrossFromCan;
    uword uwVertLongFromCan;
    UINT16 temp16;
    UINT8 i;
    //  static uchar s_ucOldOpenCloseCommand=0;
    UINT16 tempFormat, tempSteps;
    double f_temp;

    /////////////////
    // 7A0 Command
    /////////////////


    ucRecDlc = rx_temp.dlc; // legge messaggio arrivato
    //    rxid = rx_temp.id;

    for (i = 0; i < 8; i++)
        ucRecvBytes[i] = rx_temp.data[i];


    switch (ucRecDlc) // controlla dlc per tipo messaggio
    {
    case 8: //      messaggio 7A0 standard
        g_bAckReceived = true;

        if ((ucRecvBytes[1] & 0x08) == 0) // se non deve ignorare il formato analizza la DFF
        {
            temp16 = (UINT16)ucRecvBytes[3];
            if (ucRecvBytes[2] & 0x40)
                temp16 |= 0x100;
            IngressoDFF[DFFVAL_VERT_CAN] = temp16;
        }

        if (ee_ConfigColl.ucCanProtocol == CAN_STANDARD)
        {
            if (ucRecvBytes[0] & 0x80) // manual adjustement
                g_bManualRequestFromCan = true;
            else
                g_bManualRequestFromCan = false;

        }
        else if (ee_ConfigColl.ucCanProtocol == CAN_GMM)
        { //0.33
            if (ucRecvBytes[0] & 0x80)
            { // manual adjustement
                g_bManualRequestFromCan = true;
            }
            if (ucRecvBytes[0] & 0x40)
            { // auto adjustement
                g_bManualRequestFromCan = false;
            }
        }
        else // SEDECAL ignora Auto/Manual
        {
           __asm("nop");
        }

        // controllo movimenti manuali
        if (ucRecvBytes[0] & 0x01)
            g_ucOpenCloseFromCan |= OPEN_IRIS;
        else
            g_ucOpenCloseFromCan &= ~OPEN_IRIS;

        if (ucRecvBytes[0] & 0x02)
            g_ucOpenCloseFromCan |= CLOSE_IRIS;
        else
            g_ucOpenCloseFromCan &= ~CLOSE_IRIS;

        if (ucRecvBytes[0] & 0x04)
            g_ucOpenCloseFromCan |= OPEN_LONG;
        else
            g_ucOpenCloseFromCan &= ~OPEN_LONG;

        if (ucRecvBytes[0] & 0x08)
            g_ucOpenCloseFromCan |= CLOSE_LONG;
        else
            g_ucOpenCloseFromCan &= ~CLOSE_LONG;

        if (ucRecvBytes[0] & 0x10)
            g_ucOpenCloseFromCan |= OPEN_CROSS;
        else
            g_ucOpenCloseFromCan &= ~OPEN_CROSS;

        if (ucRecvBytes[0] & 0x20)
            g_ucOpenCloseFromCan |= CLOSE_CROSS;
        else
            g_ucOpenCloseFromCan &= ~CLOSE_CROSS;


        if (ucRecvBytes[1] & 0x04) // se FLUOROSCOPIA (campo tondo) o RADIOLOGIA (campo quadro)
        {
            if ((ee_ConfigColl.ucIrisPresent == IRIDE_YES) && (IrideIndipendente == FALSE) && ((g_ucOrienteer == RX_UNDER) || (g_ucOrienteer == RX_TOMO)))
            {
                fluoroscopia_on = TRUE;
            }
            else
            {
                fluoroscopia_on = FALSE;
            }
        }
        else
        {
            fluoroscopia_on = FALSE;
        }

        if (g_ucOpenCloseFromCan & MOVE_CROSS) // se si muove una lamella non deve seguire l'iride
            ShutterFollow[STEPPER_CROSS] = FALSE;
        if (g_ucOpenCloseFromCan & MOVE_LONG)
            ShutterFollow[STEPPER_LONG] = FALSE;


        if ((g_ucOpenCloseFromCan == 0) && (s_ucOldOpenCloseCommand == 0))
        { //0.52
            if ((ucRecvBytes[1] & 0x08) == 0) // se non deve ignorare il formato analizza il tutto
            {

                uwVertCrossFromCan = ucRecvBytes[4];
                uwVertCrossFromCan <<= 8;
                uwVertCrossFromCan += ucRecvBytes[5];
                ApeIride = uwVertCrossFromCan; // copia formato per eventuale iride
                if (uwVertCrossFromCan > ee_OffsetColl.nMaxCross)
                    uwVertCrossFromCan = ee_OffsetColl.nMaxCross;
                if (uwVertCrossFromCan < formato_min[STEPPER_CROSS])
                    uwVertCrossFromCan = formato_min[STEPPER_CROSS];

                uwVertLongFromCan = ucRecvBytes[6];
                uwVertLongFromCan <<= 8;
                uwVertLongFromCan += ucRecvBytes[7];
                if (uwVertLongFromCan > ee_OffsetColl.nMaxLong)
                    uwVertLongFromCan = ee_OffsetColl.nMaxLong;
                if (uwVertLongFromCan < formato_min[STEPPER_LONG])
                    uwVertLongFromCan = formato_min[STEPPER_LONG];

                if (fluoroscopia_on == FALSE)
                {
                    FormatoCross[FRMT_VERT_CAN] = uwVertCrossFromCan;
                    FormatoLong[FRMT_VERT_CAN] = uwVertLongFromCan;
                    NewFormat[FRMT_VERT_CAN] = TRUE;

                    if ((ee_ConfigColl.ucIrisPresent == IRIDE_YES) && (IrideIndipendente == FALSE))
                    {
                        //                                g_uwIrisFormatFromCan = iride_calib_mm[IRIS_FORMAT_MAX + 1];    // apre al massimo
                        if ((ucRecvBytes[1] & 0x10) == 0) // vede se portarsi al massimo o al campo definito
                        {
                            temp16 = iride_calib_mm[IRIS_FORMAT_MAX + 1]; // apre al massimo
                        }
                        else
                        {
                            temp16 = CampoIride; // apre al campo massimo definito
                        }
                        NewIrisFormat[FRMT_VERT_CAN] = TRUE;
                        FormatoIride[FRMT_VERT_CAN] = temp16;
                    }
                }
                else
                {
                    if ((ee_ConfigColl.ucIrisPresent == IRIDE_YES) && (IrideIndipendente == FALSE))
                    {
                        if (CampoIride) // se massimo definito mantiene quello
                        {
                            if (ApeIride > CampoIride)
                                ApeIride = CampoIride;
                        }
                        else
                        {
                            if (ApeIride > iride_calib_mm[IRIS_FORMAT_MAX + 1]) // apre al massimo
                                ApeIride = iride_calib_mm[IRIS_FORMAT_MAX + 1]; // apre al massimo
                        }
                        temp16 = ApeIride;

                        LinIrisOverDff(IngressoDFF[DFFVAL_VERT_CAN], temp16, &tempSteps);
                        LinIrisMm(IngressoDFF[DFFVAL_VERT_CAN], (uword)tempSteps, &tempFormat, FALSE);
                        FormatoCross[FRMT_VERT_CAN] = tempFormat + (UINT16)ee_ConfigColl.OffsetLamelle; // si mette a tot mm in piu'
                        FormatoLong[FRMT_VERT_CAN] = FormatoCross[FRMT_VERT_CAN];
                        NewFormat[FRMT_VERT_CAN] = TRUE;
                        NewIrisFormat[FRMT_VERT_CAN] = TRUE;
                        FormatoIride[FRMT_VERT_CAN] = temp16;
                    }
                }
            }
        }

        s_ucOldOpenCloseCommand = g_ucOpenCloseFromCan;

        switch (ee_ConfigColl.ucCanProtocol)
        {
        case CAN_STANDARD: // se 1 inverte lo stato della luce
            if (ucRecvBytes[1] & 0x01) // se 0 lascia inalterato
            {
                if (g_uwLightTimer == 0)
                    g_bRequestLightOn = true;
                else
                    g_uwLightTimer = 0;
            }
            break;

        case CAN_SEDECAL:
            if (ucRecvBytes[1] & 0x01) // se 1 accende - se 0 lascia inalterato
            {
                g_bRequestLightOn = true;
            }
            break;

        case CAN_GMM: // se 1 accende - se 0 spegne
            if (ucRecvBytes[1] & 0x01)
                g_bRequestLightOn = true;
            else
                g_bRequestLightOff = true;
            break;

        }

        //Change Filter
        if (ee_ConfigColl.ucCanProtocol != CAN_GMM)
        {
            if (ucRecvBytes[1] & 0x02)
            {
                for (i = 0; i <= 3; i++) // trova la posizione del filtro attuale
                {
                    if (ee_FilterConfig.Config_Filtro[i] == g_ucActualFilterType)
                        break;
                }

                if (i < 3)
                    g_ucPuntaFiltro = i + 1;
                else
                    g_ucPuntaFiltro = 0;
            }
        }
        else
        { //0.69A
            g_ucPuntaFiltro = 0;
            if ((ucRecvBytes[1] & 0xA0) == 0x20)
                g_ucPuntaFiltro = 3;
            else if ((ucRecvBytes[1] & 0xA0) == 0x80)
                g_ucPuntaFiltro = 2;
            else if ((ucRecvBytes[1] & 0xA0) == 0xA0)
                g_ucPuntaFiltro = 1;
        }
        g_ucFilterRequired = ee_FilterConfig.Config_Filtro[g_ucPuntaFiltro];


        //Extended Manual (no limits)
        if (ee_ConfigColl.ucCanProtocol == CAN_STANDARD)
        {
            __asm("nop");
/*
            if (ucRecvBytes[1]&0x80)
            {
                ee_ConfigColl.ucAutoLimits = 0;
            }
            else
            {
                ee_ConfigColl.ucAutoLimits = g_bBackupAutoLimits;
            }
*/
        }
        break;

                //***************************************************
    case 2: //      messaggio 7A0 con valori di inclinazione
        if (ucRecvBytes[0] <= 70) // CROSS
        {
            inclinazione_cross = ucRecvBytes[0];
            f_temp = 1.0 / cos(((double)inclinazione_cross * 3.14159) / 180.0);
            f_temp *= 256.0;
            mult_incl_cross = (UINT32)f_temp; // 24.8
            s_uwLastCrossDff = 0;
            formato_old[STEPPER_CROSS] = 0xFFFF; // forza il ricalcolo
        }
        else
        {
            inclinazione_cross = 0;
            mult_incl_cross = 0;
            formato_old[STEPPER_CROSS] = 0xFFFF; // forza il ricalcolo
        }

        if (ucRecvBytes[1] <= 70) // LONG
        {
            inclinazione_long = ucRecvBytes[1];
            f_temp = 1.0 / cos(((double)inclinazione_long * 3.14159) / 180.0);
            f_temp *= 256.0;
            mult_incl_long = (UINT32)f_temp; // 24.8
            s_uwLastLongDff = 0;
            formato_old[STEPPER_LONG] = 0xFFFF; // forza il ricalcolo
        }
        else
        {
            inclinazione_long = 0;
            mult_incl_long = 0;
            formato_old[STEPPER_LONG] = 0xFFFF; // forza il ricalcolo
        }
        break;

        //***************************************************
    case 6: //      messaggio 7A0 con campo Iride
        switch (ucRecvBytes[0])     // sub-command
        {
        case 0x01:      // comando di movimento Iride
            ApeIride = ucRecvBytes[1];
            ApeIride <<= 8;
            ApeIride += ucRecvBytes[2];
            if (CampoIride) // se massimo definito mantiene quello
            {
                if (ApeIride > CampoIride)
                    ApeIride = CampoIride;
            }
            else
            {
                if (ApeIride > iride_calib_mm[IRIS_FORMAT_MAX + 1]) // apre al massimo
                    ApeIride = iride_calib_mm[IRIS_FORMAT_MAX + 1]; // apre al massimo
            }
            NewIrisFormat[FRMT_VERT_CAN] = TRUE;
            FormatoIride[FRMT_VERT_CAN] = ApeIride;
            break;
        }
        break;
    }

}

//--------------------------------------------------------------

void sys_can_7A1(void)
//static void sys_can_process_1(void)
{

    uchar ucRecDlc;
    uchar ucRecvBytes[9];
    uword uwLatSxCrossFromCan;
    uword uwLatSxLongFromCan;
    uword uwLatDxCrossFromCan;
    uword uwLatDxLongFromCan;

    /////////////////
    // 7A1 Command
    /////////////////

    ucRecDlc = rx_temp.dlc;
    if (ucRecDlc == 8)
    {
        ucRecvBytes[0] = rx_temp.data[0];
        ucRecvBytes[1] = rx_temp.data[1];
        ucRecvBytes[2] = rx_temp.data[2];
        ucRecvBytes[3] = rx_temp.data[3];
        ucRecvBytes[4] = rx_temp.data[4];
        ucRecvBytes[5] = rx_temp.data[5];
        ucRecvBytes[6] = rx_temp.data[6];
        ucRecvBytes[7] = rx_temp.data[7];

        //          memcpy(ucRecvBytes, rx_temp.data, 8);
        uwLatSxCrossFromCan = ucRecvBytes[0];
        uwLatSxCrossFromCan <<= 8;
        uwLatSxCrossFromCan += ucRecvBytes[1];
        if (uwLatSxCrossFromCan > ee_OffsetColl.nMaxCross)
            uwLatSxCrossFromCan = ee_OffsetColl.nMaxCross;
        uwLatSxLongFromCan = ucRecvBytes[2];
        uwLatSxLongFromCan <<= 8;
        uwLatSxLongFromCan += ucRecvBytes[3];
        if (uwLatSxLongFromCan > ee_OffsetColl.nMaxLong)
            uwLatSxLongFromCan = ee_OffsetColl.nMaxLong;

        uwLatDxCrossFromCan = ucRecvBytes[4];
        uwLatDxCrossFromCan <<= 8;
        uwLatDxCrossFromCan += ucRecvBytes[5];
        if (uwLatDxCrossFromCan > ee_OffsetColl.nMaxCross)
            uwLatDxCrossFromCan = ee_OffsetColl.nMaxCross;

        uwLatDxLongFromCan = ucRecvBytes[6];
        uwLatDxLongFromCan <<= 8;
        uwLatDxLongFromCan += ucRecvBytes[7];
        if (uwLatDxLongFromCan > ee_OffsetColl.nMaxLong)
            uwLatDxLongFromCan = ee_OffsetColl.nMaxLong;

        FormatoCross[FRMT_LAT_SX_CAN] = uwLatSxCrossFromCan;
        FormatoLong[FRMT_LAT_SX_CAN] = uwLatSxLongFromCan;
        NewFormat[FRMT_LAT_SX_CAN] = TRUE;
        FormatoCross[FRMT_LAT_DX_CAN] = uwLatDxCrossFromCan;
        FormatoLong[FRMT_LAT_DX_CAN] = uwLatDxLongFromCan;
        NewFormat[FRMT_LAT_DX_CAN] = TRUE;
    }

}

//--------------------------------------------------------------

void sys_can_7A2(void)
//static void sys_can_process_2(void)
{
    UINT8 ucRecDlc;
    UINT16 uwTempDff;

    /////////////////
    // 7A2 Command
    /////////////////

    ucRecDlc = rx_temp.dlc;
    if (ucRecDlc == 8)
    {
        uwTempDff = (UINT16)rx_temp.data[0];
        uwTempDff |= ((UINT16)rx_temp.data[1] << 8);

        if (uwTempDff > DFF_MAX)
            uwTempDff = DFF_MAX;
        IngressoDFF[DFFVAL_LAT_DX_CAN] = uwTempDff;
        IngressoDFF[DFFVAL_LAT_SX_CAN] = uwTempDff;
    }

}



//--------------------------------------------------------------
//void sys_can_process_3(void)

void sys_can_7A4(void)
{
    UINT8 ucRecDlc;


    /////////////////
    // 7A4 Command
    /////////////////

    ucRecDlc = rx_temp.dlc;
    if (ucRecDlc == 8)
    {
        if (rx_temp.data[0] == 0x80) // manual adjustement
        {
            g_bManualRequestFromCan = TRUE;
        }
        if (rx_temp.data[0] == 0x40) // auto adjustement
        {
            g_bManualRequestFromCan = FALSE;
        }
    }

}

//--------------------------------------------------------------
//static void sys_can_process_4(void)

void sys_can_7A8(void)
{

    uchar ucRecDlc;
    uword uwTemp;
    uchar ucRecvBytes[9];

    /////////////////
    // 7A8 Command
    /////////////////

    ucRecDlc = rx_temp.dlc;
    if (ucRecDlc == 8)
    {
        ucRecvBytes[0] = rx_temp.data[0];
        ucRecvBytes[1] = rx_temp.data[1];
        ucRecvBytes[2] = rx_temp.data[2];
        ucRecvBytes[3] = rx_temp.data[3];
        ucRecvBytes[4] = rx_temp.data[4];
        ucRecvBytes[5] = rx_temp.data[5];
        ucRecvBytes[6] = rx_temp.data[6];
        ucRecvBytes[7] = rx_temp.data[7];

        uwTemp = (uword)ucRecvBytes[0] << 8;
        uwTemp |= ucRecvBytes[1];
        if (uwTemp > MAX_CROSS)
            uwTemp = MAX_CROSS;
        ee_OffsetColl.nMaxCross = uwTemp;

        uwTemp = (uword)ucRecvBytes[2] << 8;
        uwTemp |= ucRecvBytes[3];
        if (uwTemp > MAX_LONG)
            uwTemp = MAX_LONG;
        ee_OffsetColl.nMaxLong = uwTemp;

        if (ee_ConfigColl.EnFormatiMin == TRUE)
        {
            formato_min[STEPPER_CROSS] = (UINT16)ucRecvBytes[4] << 8;
            formato_min[STEPPER_CROSS] |= (UINT16)ucRecvBytes[5];

            formato_min[STEPPER_LONG] = (UINT16)ucRecvBytes[6] << 8;
            formato_min[STEPPER_LONG] |= (UINT16)ucRecvBytes[7];
        }
    }

}

//--------------------------------------------------------------
//static void sys_can_process_5(void)

void sys_can_7A3(void)
{

    uchar ucRecDlc;
    uchar ucRecvBytes[9];
    UINT16 temp16;
    UINT8 invia;

    /////////////////
    // 7A3 Command
    /////////////////

    ucRecDlc = rx_temp.dlc;
    ucRecvBytes[0] = rx_temp.data[0];
    ucRecvBytes[1] = rx_temp.data[1];
    ucRecvBytes[2] = rx_temp.data[2];
    ucRecvBytes[3] = rx_temp.data[3];
    ucRecvBytes[4] = rx_temp.data[4];
    ucRecvBytes[5] = rx_temp.data[5];
    ucRecvBytes[6] = rx_temp.data[6];
    ucRecvBytes[7] = rx_temp.data[7];

    tx_temp.id = ee_CanColl.unIdStatus + STATUS_RISP_A3;
    tx_temp.dlc = 8;
    tx_temp.data[0] = ucRecvBytes[0];
    tx_temp.data[1] = 0;
    tx_temp.data[2] = 0;
    tx_temp.data[3] = 0;
    tx_temp.data[4] = 0;
    tx_temp.data[5] = 0;
    tx_temp.data[6] = 0;
    tx_temp.data[7] = 0;

    invia = FALSE;
    switch (ucRecvBytes[0])
    {
    case 0x01:
        //Set New Filter
        g_ucPuntaFiltro = ucRecvBytes[1] & 0x03;
        g_ucFilterRequired = ee_FilterConfig.Config_Filtro[g_ucPuntaFiltro];
        tx_temp.data[1] = g_ucPuntaFiltro;
        invia = TRUE;
        break;

    case 0x02:
        //Requires serial number
        g_b7F2Time = true;
        invia = TRUE;
        break;

    case 0x03:
        if (!g_uwLightTimer)
            g_bRequestLightOn = true;
        else
            g_uwLightTimer = 0;
        invia = TRUE;
        break;

    case 0x04:
        //0.51
        LED_SIGNAL_ON;
        invia = TRUE;
        break;

    case 0x05:
        //0.51
        LED_SIGNAL_OFF;
        invia = TRUE;
        break;

    case 0x06: // modifica velocita' lamelle in AUTOMATICO
        temp16 = (UINT16)ucRecvBytes[1] << 8;
        temp16 |= (UINT16)ucRecvBytes[2];
        if ((temp16 >= FREQ_SET_MIN) && (temp16 <= FREQ_SET_MAX))
        {
            Steppers[STEPPER_A].uwFMaxFast = temp16;
            ee_ConfigBoards[STEPPER_A].uwFMaxFast = temp16;
            Steppers[STEPPER_B].uwFMaxFast = temp16;
            ee_ConfigBoards[STEPPER_B].uwFMaxFast = temp16;
            tx_temp.data[1] = ucRecvBytes[1];
            tx_temp.data[2] = ucRecvBytes[2];
            invia = TRUE;
        }
        break;

    case 0x07: // modifica velocita' lamelle in MANUALE
        temp16 = (UINT16)ucRecvBytes[1] << 8;
        temp16 |= (UINT16)ucRecvBytes[2];
        if ((temp16 >= FREQ_SET_MIN) && (temp16 <= FREQ_SET_MAX))
        {
            if (temp16 < DEF_FMIN)
            {
                Steppers[STEPPER_A].uwFMin = temp16;
                Steppers[STEPPER_A].uwFMax = temp16;
                ee_ConfigBoards[STEPPER_A].uwFMin = temp16; //Fmin: era 1000
                ee_ConfigBoards[STEPPER_A].uwFMax = temp16; //Fmax: era 1500
                Steppers[STEPPER_B].uwFMin = temp16;
                Steppers[STEPPER_B].uwFMax = temp16;
                ee_ConfigBoards[STEPPER_B].uwFMin = temp16; //Fmin: era 1000
                ee_ConfigBoards[STEPPER_B].uwFMax = temp16; //Fmax: era 1500
            }
            else
            {
                Steppers[STEPPER_A].uwFMin = DEF_FMIN;
                Steppers[STEPPER_A].uwFMax = temp16;
                ee_ConfigBoards[STEPPER_A].uwFMin = DEF_FMIN; //Fmin: era 1000
                ee_ConfigBoards[STEPPER_A].uwFMax = temp16; //Fmax: era 1500
                Steppers[STEPPER_B].uwFMin = DEF_FMIN;
                Steppers[STEPPER_B].uwFMax = temp16;
                ee_ConfigBoards[STEPPER_B].uwFMin = DEF_FMIN; //Fmin: era 1000
                ee_ConfigBoards[STEPPER_B].uwFMax = temp16; //Fmax: era 1500
            }
            tx_temp.data[1] = ucRecvBytes[1];
            tx_temp.data[2] = ucRecvBytes[2];
            invia = TRUE;
        }
        break;

    case 0x08:
        if (ucRecvBytes[1] & 0x01)
        {
            g_bRequestLightOn = true;
        }
        else
        {
            g_bRequestLightOff = true;
        }
        tx_temp.data[1] = ucRecvBytes[1] & 0x01;
        invia = TRUE;
        break;

    case 0x09:
        //Exclude Inclinometer
        if (ucRecvBytes[1] & 0x01)
        {
            g_bExclIncl = true;
        }
        else
        {
            g_bExclIncl = false;
        }
        tx_temp.data[1] = ucRecvBytes[1] & 0x01;
        invia = TRUE;
        break;

    case 0x0A:
        // Campo massimo per IRIDE
        temp16 = (UINT16)ucRecvBytes[1] << 8;
        temp16 |= (UINT16)ucRecvBytes[2];
        CampoIride = temp16;
        set_passi_campoiride = TRUE; // forza il reinvio se stesso formato
        tx_temp.data[1] = ucRecvBytes[1];
        tx_temp.data[2] = ucRecvBytes[2];
        invia = TRUE;
        break;

    case 0x0B:
        // Set tipo di movimentazione in manuale
        ee_ConfigColl.TipoManualSpost = ucRecvBytes[1];
        tx_temp.data[1] = ucRecvBytes[1];
        invia = TRUE;
        break;

    case 0x0C:
        // Set mm/sec
        if ((ucRecvBytes[1] == STEPPER_CROSS) || (ucRecvBytes[1] == STEPPER_LONG))
        {
            temp16 = (UINT16)ucRecvBytes[2] << 8;
            temp16 |= (UINT16)ucRecvBytes[3];
            g_ManualSpeed[ucRecvBytes[1]] = (UINT32)temp16;
            tx_temp.data[1] = ucRecvBytes[1];
            tx_temp.data[2] = ucRecvBytes[2];
            tx_temp.data[3] = ucRecvBytes[3];
            invia = TRUE;
        }
        break;

    case 0x0D:
        // Enable/Disable MANOPOLE
        if ((ucRecvBytes[1] == 0) || (ucRecvBytes[1] == 1))
        {
            EnableKnobs = ucRecvBytes[1];
            tx_temp.data[1] = ucRecvBytes[1];
            invia = TRUE;
        }
        break;

    case 0x0E:
        // Enable/Disable TASTO FILTRO
        if ((ucRecvBytes[1] == 0) || (ucRecvBytes[1] == 1))
        {
            EnableFilterKey = ucRecvBytes[1];
            tx_temp.data[1] = ucRecvBytes[1];
            invia = TRUE;
        }
        break;

    case 0x0F:
        // Enable/Disable TASTO LUCE
        if ((ucRecvBytes[1] == 0) || (ucRecvBytes[1] == 1))
        {
            EnableLampKey = ucRecvBytes[1];
            tx_temp.data[1] = ucRecvBytes[1];
            invia = TRUE;
        }
        break;

    case 0x10:
        //  Enable/Disable Iride
        if (ucRecvBytes[1] == 0x01)
            ee_ConfigColl.ucIrisEnabled = TRUE;
        else
            ee_ConfigColl.ucIrisEnabled = FALSE;
        if (ucRecvBytes[2] == 0x01)
            g_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
        Iris_Variable_init(FALSE);
        sys_display_main_screen();
        tx_temp.data[1] = ucRecvBytes[1];
        tx_temp.data[2] = ucRecvBytes[2];
        invia = TRUE;
        break;

    case 0x11:
        //  se TRUE pilota l'IRIDE INDIPENDENTE
        if (ucRecvBytes[1] == 1)
        {
            IrideIndipendente = TRUE;
            fluoroscopia_on = FALSE;
        }
        else
        {
            IrideIndipendente = FALSE;
            muovi_iride = TRUE;
        }
        if (ucRecvBytes[2] == 1)
        {
            ee_ConfigColl.ucIrisIndipendent = IrideIndipendente;
            g_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
        }
        tx_temp.data[1] = ucRecvBytes[1];
        tx_temp.data[2] = ucRecvBytes[2];
        invia = TRUE;
        break;

    case 0x12:
        // Enable/Disable TASTI IRIDE
        if ((ucRecvBytes[1] == 0) || (ucRecvBytes[1] == 1))
        {
            EnableIrisKey = ucRecvBytes[1];
            tx_temp.data[1] = ucRecvBytes[1];
            invia = TRUE;
        }
        break;

    case 0x13:
        //  Tempo di accensione luce
        if ((ucRecvBytes[1] >= 5) && (ucRecvBytes[1] <= 60))
        {
            KeyLightTimer = (UINT16)ucRecvBytes[1] * BASE_TIMER_LUCE;
            if (ucRecvBytes[2] == 1)
            {
                ee_ConfigColl.TempoAccensioneLamp = ucRecvBytes[1];
                g_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            }
            tx_temp.data[1] = ucRecvBytes[1];
            tx_temp.data[2] = ucRecvBytes[2];
            invia = TRUE;
        }
        break;

    case 0x14:
        //  Tempo di accensione luce
        if (ucRecvBytes[1] == 0)
        {
            RequestLightStatus = COM_LIGHT_OFF;
            tx_temp.data[1] = ucRecvBytes[1];
            invia = TRUE;
        }
        else if (ucRecvBytes[1] <= 60)
        {
            g_CanLightTimer = (UINT16)ucRecvBytes[1] * BASE_TIMER_LUCE;
            RequestLightStatus = COM_LIGHT_TRIG;
            tx_temp.data[1] = ucRecvBytes[1];
            invia = TRUE;
        }
        break;

    case 0x15:
        //  per gestione TFT
        switch (ucRecvBytes[1])     // subCommand
        {
        case 0x01:      // icona raggi X
            switch (ucRecvBytes[2])
            {
            case 0x00:
                icona_RaggiX = FALSE;
                break;
            case 0x01:
                icona_RaggiX = TRUE;
                break;
            default:
                ucRecvBytes[2] = icona_RaggiX;
                break;
            }
            break;

        case 0x02:      // stato SID
            switch (ucRecvBytes[2])
            {
            case 0x00:
                icona_sid = TIPO_SID_SOTTO;
                break;
            case 0x01:
                icona_sid = TIPO_SID_SOPRA;
                break;
            case 0x02:
                icona_sid = TIPO_SID_FUORI;
                break;
            default:
                ucRecvBytes[2] = icona_sid;
                break;
            }
            break;

        case 0x03:      // angolo colonna
            angolo_colonna = ucRecvBytes[2];
            break;

        default:
            ucRecvBytes[1] = 0xFF;      // subCommand non riconosciuto
            break;
        }

            // ritrasmette messaggio
        tx_temp.data[1] = ucRecvBytes[1];
        tx_temp.data[2] = ucRecvBytes[2];
        tx_temp.data[3] = ucRecvBytes[3];
        invia = TRUE;

        if (icona_sid != icona_sid_hold)
        {
            switch (ucRecvBytes[2])     // imposta solo su cambio di stato
            {
            case 0x00:
                g_bManualRequestFromCan = FALSE;
                break;
            case 0x01:
                g_bManualRequestFromCan = FALSE;
                break;
            case 0x02:
                g_bManualRequestFromCan = TRUE;
                break;
            }

            if (icona_sid_hold == TIPO_SID_FUORI)
            {
    //            FormatoValido[RX_UNDER] = TRUE;
                if ((FormatoCross[FRMT_VERT_CAN] == 0) && (FormatoLong[FRMT_VERT_CAN] == 0))
                {
                    FormatoCross[FRMT_VERT_CAN] = g_uwActualCrossToBeSent;
                    FormatoLong[FRMT_VERT_CAN] = g_uwActualLongToBeSent;
                }
                if (FormatoCross[FRMT_VERT_CAN] > ee_OffsetColl.nMaxCross)
                    FormatoCross[FRMT_VERT_CAN] = ee_OffsetColl.nMaxCross;
                if (FormatoLong[FRMT_VERT_CAN] > ee_OffsetColl.nMaxLong)
                    FormatoLong[FRMT_VERT_CAN] = ee_OffsetColl.nMaxLong;
                NewFormat[FRMT_VERT_CAN] = TRUE;
            }
            else
            {
                if (icona_sid == TIPO_SID_FUORI)
                {
                    if ((FormatoCross[FRMT_VERT_CAN] == 0) && (FormatoLong[FRMT_VERT_CAN] == 0))
                    {
                        FormatoCross[FRMT_VERT_CAN] = calc_steps2size(sid_manuale, STEPPER_CROSS, i_MotPosizione[STEPPER_CROSS]);
                        FormatoLong[FRMT_VERT_CAN] = calc_steps2size(sid_manuale, STEPPER_LONG, i_MotPosizione[STEPPER_LONG]);
                        NewFormat[FRMT_VERT_CAN] = TRUE;
                    }
                }
            }
        }
        break;

//************************************************************

    case 0xF0:
        if (ucRecvBytes[1] == 0x00)
        {
            EnableInvioEventi = FALSE;
            tx_temp.data[1] = ucRecvBytes[1];
            invia = TRUE;
        }
        else if (ucRecvBytes[1] == 0x01)
        {
            EnableInvioEventi = TRUE;
            tx_temp.data[1] = ucRecvBytes[1];
            invia = TRUE;
        }
        else
            {
            tx_temp.data[1] = 0xFF;
            invia = TRUE;
            }
        break;

    case 0xF1:
        if (ucRecvBytes[1] == 0x00)
        {
            sys_control_init(FALSE);
            TestPassi = FALSE;
            rimbalzo_home_shutter = RIMBALZO_HOME;
            stato_master = MASTER_FIRST;
            tx_temp.data[1] = ucRecvBytes[1];
            invia = TRUE;
        }
        else if (ucRecvBytes[1] == 0x01)
        {
            sys_control_init(FALSE);
            TestPassi = TRUE;
            rimbalzo_home_shutter = RIMBALZO_HOME_TEST;
            stato_master = MASTER_FIRST;
            tx_temp.data[1] = ucRecvBytes[1];
            invia = TRUE;
        }
            else
        {
            tx_temp.data[1] = 0xFF;
            invia = TRUE;
        }
        break;

    case 0xFD:
        // Simulazione Inclinazione
        DebugIncl = ucRecvBytes[1];
        InclinazioneCan = ucRecvBytes[2];
        tx_temp.data[1] = ucRecvBytes[1];
        tx_temp.data[2] = ucRecvBytes[2];
        invia = TRUE;
        break;

    case 0xFE:
        // Collimator RESET
        if (ucRecvBytes[1] == 0x55)
        {
            resetCollimator();
		}
        else if (ucRecvBytes[1] == 0x56)
        {
            ee_ConfigColl.ucSwContext = 1;
            g_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            tx_temp.data[1] = ucRecvBytes[1];
            invia = TRUE;
        }
        break;

    }


    if (invia == TRUE)
    {
        if (ee_ConfigColl.ucRisposte7F8)
        {
            copy_tx_temp(FALSE);
        }
    }


}

//--------------------------------------------------------------

static void sys_can_process_6(void)
{

    // TODO: da completare Can Remote
//    c0sbs = 0x06; //6
//    if ((c0mctl6.byte & 0x03) == 1)
//    {
//
//        c0mctl6.byte = 0x00;
//        c0mctl6.byte = 0x70; //Remote
//        c0mctl6.byte = 0x60; //Remote
//    }

}




//--------------------------------------------------------------//
//static void sys_can_process_7(void)

void sys_can_ROT_VERSION(void)
{
    g_cbRotVersion[0] = rx_temp.data[0];
    g_cbRotVersion[1] = rx_temp.data[1];
    g_cbRotVersion[2] = rx_temp.data[2];
    g_cbRotVersion[3] = rx_temp.data[3];
    g_cbRotVersion[4] = rx_temp.data[4];
    g_cbRotVersion[5] = rx_temp.data[5];
    g_cbRotVersion[6] = rx_temp.data[6];
    g_cbRotVersion[7] = rx_temp.data[7];
    g_bRotk = TRUE;

}




void sys_can_IRIS_VERSION(void)
{
    g_cbIrisVersion[0] = rx_temp.data[0];
    g_cbIrisVersion[1] = rx_temp.data[1];
    g_cbIrisVersion[2] = rx_temp.data[2];
    g_cbIrisVersion[3] = rx_temp.data[3];
    g_cbIrisVersion[4] = rx_temp.data[4];
    g_cbIrisVersion[5] = rx_temp.data[5];
    g_cbIrisVersion[6] = rx_temp.data[6];
    g_cbIrisVersion[7] = rx_temp.data[7];
    g_bIridek = TRUE;

}



void sys_can_IRIS_STATUS(void)
{
    uchar ucRecDlc;
    UINT8 pt;
    //    uchar ucTempCanMessage[8];
    UINT16 temp16;
    uchar ucRecvBytes[9];

    ucRecDlc = rx_temp.dlc;
    ucRecvBytes[0] = rx_temp.data[0];
    ucRecvBytes[1] = rx_temp.data[1];
    ucRecvBytes[2] = rx_temp.data[2];
    ucRecvBytes[3] = rx_temp.data[3];
    ucRecvBytes[4] = rx_temp.data[4];
    ucRecvBytes[5] = rx_temp.data[5];
    ucRecvBytes[6] = rx_temp.data[6];
    ucRecvBytes[7] = rx_temp.data[7];

    if (rx_temp.data[7] == 0x01) // messaggio di stato
    {
        posizione_iride = (UINT32)rx_temp.data[0] << 24;
        posizione_iride |= (UINT32)rx_temp.data[1] << 16;
        posizione_iride |= (UINT32)rx_temp.data[2] << 8;
        posizione_iride |= (UINT32)rx_temp.data[3];
        stato_iride = (UINT16)rx_temp.data[4];
        if (stato_iride == 0x00)
        { // solo in reset manda 0x00
            stato_iride = IRIDE_RESET;
        }
        adc_iride = (UINT16)rx_temp.data[5] << 8;
        adc_iride |= (UINT16)rx_temp.data[6];
        NewValIris = TRUE;
    }
    else if (rx_temp.data[7] == 0x02) // messaggio valore di taratura
    {
        pt = rx_temp.data[0]; // puntatore alla taratura
        temp16 = (UINT16)rx_temp.data[1] << 8; // valore ADC
        temp16 |= (UINT16)rx_temp.data[2];
        ee_IrisSteps.iride_calib_pot[pt] = temp16;
        temp16 = (UINT16)rx_temp.data[3] << 8; // valore PASSI
        temp16 |= (UINT16)rx_temp.data[4];
        ee_IrisSteps.iride_calib_passi[pt] = temp16;
    }

}




//--------------------------------------------------------------
//static void sys_can_process_8(void)

static void sys_can_DISPLAY_1(void)
{
    uchar ucRecDlc;
    uchar ucTempPos;

    ucRecDlc = rx_temp.dlc;
    ucTempPos = rx_temp.data[0];
    if (g_ucActualDisplayType == DISPLAY_FREE)
    {
        if ((ucTempPos >= CLEAR_FIRST_LINE) && (ucTempPos <= CLEAR_BOTH_LINES))
            g_ucReqDisplayOperation = ucTempPos;

        if ((ucTempPos <= END_FIRST_LINE) || ((ucTempPos >= START_SECOND_LINE) && (ucTempPos <= END_SECOND_LINE)))
        {
            g_ucReqDisplayOperation = ucTempPos;
            memcpy(cbNewDisplayString, &rx_temp.data[1], 7);
        }
    }
}

//--------------------------------------------------------------
//static void sys_can_process_9(void)

void sys_can_7A7(void)
{

    uchar ucRecDlc;
    UINT16 unTemp1;
    UINT16 unTemp2;
    UINT16 unTemp3;
    UINT16 unTemp4;

    /////////////////
    // 7A7 Command
    /////////////////

    ucRecDlc = rx_temp.dlc;
    if (ucRecDlc == 8)
    {
        unTemp1 = (UINT16)rx_temp.data[0] << 8;
        unTemp1 += (UINT16)rx_temp.data[1];
        unTemp2 = (UINT16)rx_temp.data[2] << 8;
        unTemp2 += (UINT16)rx_temp.data[3];
        unTemp3 = (UINT16)rx_temp.data[4] << 8;
        unTemp3 += (UINT16)rx_temp.data[5];
        unTemp4 = (UINT16)rx_temp.data[6] << 8;
        unTemp4 += (UINT16)rx_temp.data[7];

        if ((UpdateRates.un7F0 == 0) && (unTemp1))
        {
            invia7F0 = TRUE;
        }
        UpdateRates.un7F0 = unTemp1;
        UpdateRates.un7F1 = unTemp2;
        UpdateRates.un7F9 = unTemp3;
        UpdateRates.un7FC = unTemp4;
    }

}

//--------------------------------------------------------------
//static void sys_can_process_10(void){

void sys_can_7A5(void)
{

    uchar ucRecDlc;
    UINT16 uwTempDff;

    /////////////////
    // 7A5 Command
    /////////////////

    ucRecDlc = rx_temp.dlc;
    if (ucRecDlc == 8)
    {
        uwTempDff = (UINT16)rx_temp.data[0];
        uwTempDff |= (UINT16)rx_temp.data[1] << 8;
        if (uwTempDff > DFF_MAX)
            uwTempDff = DFF_MAX;

        IngressoDFF[DFFVAL_VERT_CAN] = uwTempDff;

        if ((FormatoCross[FRMT_VERT_CAN] == 0) && (FormatoLong[FRMT_VERT_CAN] == 0))
        {
            FormatoCross[FRMT_VERT_CAN] = calc_steps2size(IngressoDFF[DFFVAL_VERT_CAN], STEPPER_CROSS, i_MotPosizione[STEPPER_CROSS]);
            FormatoLong[FRMT_VERT_CAN] = calc_steps2size(IngressoDFF[DFFVAL_VERT_CAN], STEPPER_LONG, i_MotPosizione[STEPPER_LONG]);
            if (ManualStatus[STEPPER_CROSS] == FALSE)
            {
                if (FormatoCross[FRMT_VERT_CAN] > ee_OffsetColl.nMaxCross)
                    FormatoCross[FRMT_VERT_CAN] = ee_OffsetColl.nMaxCross;
                if (FormatoLong[FRMT_VERT_CAN] > ee_OffsetColl.nMaxLong)
                    FormatoLong[FRMT_VERT_CAN] = ee_OffsetColl.nMaxLong;
            }
            NewFormat[FRMT_VERT_CAN] = TRUE;
        }
    }

}

//--------------------------------------------------------------
//static void sys_can_process_11(void)

void sys_can_7A9(void)
{
    uchar ucRecDlc;


    ucRecDlc = rx_temp.dlc;

    if (ucRecDlc == 8)
    {
        g_CanLightTimer = rx_temp.data[0];
        if (g_CanLightTimer < 5)
            g_CanLightTimer = 5;
        if (g_CanLightTimer > 60)
            g_CanLightTimer = 60;
        g_CanLightTimer *= BASE_TIMER_LUCE;
        KeyLightTimer = g_CanLightTimer;
    }
}




//******************************************************++
//  riceve da scheda esterna 003
//******************************************************++

void sys_can_7AA(UINT8 test)
{
    RemoteStatus.bytes[0] = rx_temp.data[0];
    RemoteStatus.bytes[1] = rx_temp.data[1];
    RemoteStatus.bytes[2] = rx_temp.data[2];
    RemoteStatus.bytes[3] = rx_temp.data[3];
    RemoteStatus.bytes[4] = rx_temp.data[4];
    RemoteStatus.bytes[5] = rx_temp.data[5];
    RemoteStatus.bytes[6] = rx_temp.data[6];
    RemoteStatus.bytes[7] = rx_temp.data[7];
    g_bNewRemote = true;

    if (test == TRUE)
    {
        if (RemoteStatus.bytes[6] & 0x10)
            g_bUserCalRequest = true;
        else
            g_bUserCalRequest = false;
    }
}



//******************************************************++
//  riceve da scheda esterna 003
//******************************************************++

void sys_can_7AB(UINT8 test)
{
/*

    if (test == FALSE)
    {
        if (g_uwMovingTimer)
            return;
    }
*/

    RemoteAdc[DFF_STAT] = rx_temp.data[0];
    RemoteAdc[DFF_STAT] <<= 8;
    RemoteAdc[DFF_STAT] += rx_temp.data[1];
    RemoteAdc[DFF_TAB] = rx_temp.data[2];
    RemoteAdc[DFF_TAB] <<= 8;
    RemoteAdc[DFF_TAB] += rx_temp.data[3];
    RemoteAdc[BUCKY_CROSS] = rx_temp.data[4];
    RemoteAdc[BUCKY_CROSS] <<= 8;
    RemoteAdc[BUCKY_CROSS] += rx_temp.data[5];
    RemoteAdc[BUCKY_LONG] = rx_temp.data[6];
    RemoteAdc[BUCKY_LONG] <<= 8;
    RemoteAdc[BUCKY_LONG] += rx_temp.data[7];
    g_bNewRemote = true;
}



//******************************************************++
//  riceve da scheda esterna 003
//******************************************************++

void sys_can_7AC(UINT8 test)
{
/*
    if (test == FALSE)
    {
        if (g_uwMovingTimer)
            return;
    }
*/

    RemoteAdc[LEFT_CROSS] = rx_temp.data[0];
    RemoteAdc[LEFT_CROSS] <<= 8;
    RemoteAdc[LEFT_CROSS] += rx_temp.data[1];
    RemoteAdc[LEFT_LONG] = rx_temp.data[2];
    RemoteAdc[LEFT_LONG] <<= 8;
    RemoteAdc[LEFT_LONG] += rx_temp.data[3];
    RemoteAdc[RIGHT_CROSS] = rx_temp.data[4];
    RemoteAdc[RIGHT_CROSS] <<= 8;
    RemoteAdc[RIGHT_CROSS] += rx_temp.data[5];
    RemoteAdc[RIGHT_LONG] = rx_temp.data[6];
    RemoteAdc[RIGHT_LONG] <<= 8;
    RemoteAdc[RIGHT_LONG] += rx_temp.data[7];
    g_bNewRemote = true;
}

void sys_can_7AD(void)
{
    schar scTemp;


    if (rx_temp.data[0]&0x01)
    {
        ee_ConfigColl.ucLanguage = ENG_LANG;
        sys_display_main_screen();
    }
    if (rx_temp.data[0]&0x02)
    {
        ee_ConfigColl.ucLanguage = ITA_LANG;
        sys_display_main_screen();
    }
    if (rx_temp.data[0]&0x04)
    {
        ee_ConfigColl.ucCmUnit = CM_UNIT;
        sys_display_main_screen();
    }
    if (rx_temp.data[0]&0x08)
    {
        ee_ConfigColl.ucCmUnit = INCHES_UNIT;
        sys_display_main_screen();
    }

    if (rx_temp.data[0]&0x10)
    {
        scTemp = (schar)rx_temp.data[2];
        if (ee_ConfigColl.CorrType == TIPO_CORR_CM)
        {
            if (((scTemp >= 0) && (scTemp <= DELTA_DFF_MAX_POS)) ||
                    ((scTemp < 0) && (scTemp >= DELTA_DFF_MAX_NEG)))
            {
                ee_ConfigColl.scCorrCrossDff = scTemp;
            }
        }
        else
        {
            if (((scTemp >= 0) && (scTemp <= DELTA_PERC_MAX_POS)) ||
                    ((scTemp < 0) && (scTemp >= DELTA_PERC_MAX_NEG)))
            {
                ee_ConfigColl.scCorrCrossDff = scTemp;
            }
        }
    }

    if (rx_temp.data[0]&0x20)
    {
        scTemp = (schar)rx_temp.data[3];
        if (ee_ConfigColl.CorrType == TIPO_CORR_CM)
        {
            if (((scTemp >= 0) && (scTemp <= DELTA_DFF_MAX_POS)) ||
                    ((scTemp < 0) && (scTemp >= DELTA_DFF_MAX_NEG)))
            {
                ee_ConfigColl.scCorrLongDff = scTemp;
            }
        }
        else
        {
            if (((scTemp >= 0) && (scTemp <= DELTA_PERC_MAX_POS)) ||
                    ((scTemp < 0) && (scTemp >= DELTA_PERC_MAX_NEG)))
            {
                ee_ConfigColl.scCorrLongDff = scTemp;
            }
        }
    }
    if (rx_temp.data[0] & 0x80)
    {
        switch (rx_temp.data[1])
        {
        case 0x01: // enable/disable Iride
            if (rx_temp.data[2] == 0x01)
                ee_ConfigColl.ucIrisEnabled = TRUE;
            else
                ee_ConfigColl.ucIrisEnabled = FALSE;
            if (rx_temp.data[3] == 0x01)
                g_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            Iris_Variable_init(FALSE);
            sys_display_main_screen();
            break;
        }
    }
}



//***********************************************
//  ricezione da ASR003 per versione software
//***********************************************

void sys_can_7AE(void)
{
    g_cbAsr003Version[0] = rx_temp.data[0];
    g_cbAsr003Version[1] = rx_temp.data[1];
    g_cbAsr003Version[2] = rx_temp.data[2];
    g_cbAsr003Version[3] = rx_temp.data[3];
    g_cbAsr003Version[4] = rx_temp.data[4];
    g_cbAsr003Version[5] = rx_temp.data[5];
    g_cbAsr003Version[6] = rx_temp.data[6];
    g_cbAsr003Version[7] = rx_temp.data[7];
    g_b7AETime = false;
    g_bAsr003Ok = true;
}

void sys_can_7AF(void)
{
    CAN_config_Analyze(0, TRUE);
}







//--------------------------------------------------------------
//static void sys_can_process_12(void)

void sys_can_DISPLAY_0(void)
{

    uchar ucRecDlc;


    ucRecDlc = rx_temp.dlc;
    if (ucRecDlc == 8)
    {
        g_ucActualDisplayType = rx_temp.data[0];
        if ((rx_temp.data[0] >= DISPLAY_STANDARD) && (rx_temp.data[0] <= DISPLAY_FREE))
            g_ucActualDisplayType = rx_temp.data[0];
    }

}


//*********************************************
//  controlla e trasmette il messaggio 0x100
//*********************************************
void check_tx_100(void)
{
    uchar ucByte1;
    uchar ucByte2;
    UINT8 flag, tipo;

    if (g_b100Time || g_bUpdateRele)
    {
        g_b100Time = false;
        g_bUpdateRele = false;
        Relays.Status.bit.InReset = !g_bInitEnd;
        if (CollInReset)
            Relays.Status.bit.InReset = 1;
        ucByte1 = (uchar)Relays.Status.word;
        ucByte2 = Relays.Status.word >> 8;

        flag = 0;
        if ((ucByte1 != val_7d0a) || (ucByte2 != val_7d0b))
        {
            val_7d0a = ucByte1;
            val_7d0b = ucByte2;
            flag = 1;
        }

        if (flag || manda_7d0)
        {
            manda_7d0 = false;
            if (ee_ConfigColl.ucAlarm_0x100 == ALARM_0x100_NO) // se disabilitato il messaggio e' vecchio
            {
                tipo = 1;
            }
            else
            {
                tipo = 2; // altrimenti e' nuovo con gli allarmi
            }

            if (tipo == 1)
            {
                tx_temp.dlc = 2;
                if (ee_ConfigColl.ucCanProtocol == CAN_GMM)
                { //0.68
                    tx_temp.id = 0x7D0;
                }
                else
                {
                    tx_temp.id = ID_ALLARMI;
                }
                tx_temp.data[0] = ucByte1;
                tx_temp.data[1] = ucByte2;
            }
            else
            {
                tx_temp.dlc = 8;
                if (ee_ConfigColl.ucCanProtocol == CAN_GMM)
                { //0.68
                    tx_temp.id = 0x7D0;
                }
                else
                {
                    tx_temp.id = ID_ALLARMI;
                }
                tx_temp.data[0] = ucByte1;
                tx_temp.data[1] = ucByte2;
                tx_temp.data[2] = allarmi_totale | try_filter;
                tx_temp.data[3] = err_counter_filter;
                tx_temp.data[4] = err_counter_cross;
                tx_temp.data[5] = err_counter_long;
                tx_temp.data[6] = err_counter_iride;
            }
            copy_tx_temp(FALSE);
        }
    }
}








//******************************************************
//  invia tasti pannello frontale
//******************************************************
void invio_tasti_villa(void)
{
    static UINT8 tasti_prec = 0;
    UINT8 invia_tasti;
    UINT16 ricarica_tasti;

    if (ee_ConfigColl.TastieraVilla == FALSE)
        return;

    invia_tasti = FALSE;

    if (tasti_villa)
    {
        if (tasti_prec == 0)
        {
            invia_tasti = TRUE;
        }
        ricarica_tasti = 80;
    }
    else
    {
        ricarica_tasti = 1000;
    }
    tasti_prec = tasti_villa;

    if (timer_invio_tasti_villa == 0)
    {
        invia_tasti = TRUE;
    }

    if (invia_tasti)
    {
        timer_invio_tasti_villa = ricarica_tasti;
        tx_temp.id = ee_CanColl.unIdStatus + ee_ConfigColl.IdOffsetTastieraVilla;
        tx_temp.dlc = 1;
        tx_temp.data[0] = tasti_villa;
        copy_tx_temp(FALSE);
    }
}











//--------------------------------------------------------------
//static void sys_can_process_13(void)

void sys_can_tx_status(void)
{

    uchar ucByte1;
    uchar ucByte2;
    uchar ucByte3;
    uchar ucByte4;
    uchar ucByte5;
    uchar ucByte6;
    uchar ucByte7;
    uchar ucByte8;
    UINT8 i;
    static uchar s_ucMessage = 0;
    static UINT8 iride_inviato = FALSE;
    SINT16 stemp16;
    UINT16 temp16;
    strLogEvento *ptrEv;


    for (i = 0; i < 8; i++) // azzera buffer
        tx_temp.data[i] = 0;


    if (msg_Iride_COM == TRUE) // controlla se messaggio per iride
    {
        if (iride_inviato == FALSE)     // se non pendente allora spedisce
        {
            iride_inviato = TRUE;
            tx_temp.id = msg_Iride_ID;
            tx_temp.dlc = msg_Iride_DLC;

            for (i = 0; i < 8; i++)
                tx_temp.data[i] = msg_Iride_DATA[i];
            copy_tx_temp(FALSE);
            timerWaitIride = TIME_WAIT_IRIDE;
            return;
        }
        else
        {
            if (timerWaitIride == 0)
            {
                iride_inviato = FALSE;
                msg_Iride_COM = FALSE;
            }
        }
    }


    switch (s_ucMessage)
    {
    case 0:
        check_tx_100();
        s_ucMessage = 1;
        break;

    case 1:
    if (UpdateRates.un7F0)
        {
            mask_7F0_invio[0] = 0;
            mask_7F0_invio[1] = 0;
            mask_7F0_invio[2] = 0;
            mask_7F0_invio[3] = 0;
            mask_7F0_invio[4] = 0;
            mask_7F0_invio[5] = 0;
            mask_7F0_invio[6] = 0;
            mask_7F0_invio[7] = 0;
                                        //  BYTE 0
            val_7F0_now[0] = 0;
            if (!g_bInitEnd)
                val_7F0_now[0] |= 0x01;
            if (CollInReset)
                val_7F0_now[0] |= 0x01;
            if (g_bLightState)
                val_7F0_now[0] |= 0x40;
            mask_7F0_invio[0] = 0x41;

                                        //  BYTE 1
            val_7F0_now[1] = 0;
            if (g_ucStatusLed != YELLOW)
                val_7F0_now[1] = 0x40;
            if (CheckProximity() == TRUE)
                val_7F0_now[1] |= 0x80;
            if (IN_SWITCH_ROTATION)
                val_7F0_now[1] |= 0x20;
            mask_7F0_invio[1] = 0xE0;

                                        //  BYTE 2
            val_7F0_now[2] = 0;
            switch (g_ucActualFilterType)
            {
            case 0:
                break;
            case 1:
                val_7F0_now[2] |= 0x01;
                break;
            case 2:
                val_7F0_now[2] |= 0x02;
                break;
            case 3:
                val_7F0_now[2] |= 0x03;
                break;
            }
            if (Steppers[STEPPER_LONG].StepperData.StepperBits.bMoving)
                val_7F0_now[2] |= 0x80;
            if (Steppers[STEPPER_CROSS].StepperData.StepperBits.bMoving)
                val_7F0_now[2] |= 0x40;
            if (Steppers[STEPPER_FILTER].StepperData.StepperBits.bMoving)
                val_7F0_now[2] |= 0x20;
            if (stato_iride & IRIDE_MOVE) // e' in movimento?
                val_7F0_now[2] |= 0x10;
            if (g_uwActualDff & 0xFF00)
                val_7F0_now[2] |= 0x04;
            mask_7F0_invio[2] = 0xF3;
                                        //  BYTE 3
            val_7F0_now[3] = g_uwActualDff & 0xFF;
            val_7F0_now[4] = (uchar)(g_uwActualCrossToBeSent >> 8);
            val_7F0_now[5] = (uchar)g_uwActualCrossToBeSent;
            val_7F0_now[6] = (uchar)(g_uwActualLongToBeSent >> 8);
            val_7F0_now[7] = (uchar)g_uwActualLongToBeSent;

            for (i = 0; i < 8; i++)     // confronta bit che provocano l'invio immediato (es: movimento e luce)
            {
                if ((val_7F0_now[i] ^ val_7F0_old[i]) & mask_7F0_invio[i])
                {
                    val_7F0_old[i] = val_7F0_now[i];
                    invia7F0 = TRUE;
                }
            }

            for (i = 0; i < 8; i++)
            {
                if (val_7F0_now[i] != val_7F0_old[i])
                {
                    val_7F0_old[i] = val_7F0_now[i];
                    stato_7F0_fast = TRUE;
                }
            }

            if (stato_7F0_fast)
            {
                if (timer_invio_7F0 >= UpdateRates.un7F0)
                {
                    invia7F0 = TRUE;
                }
            }
            else
            {
                if (timer_invio_7F0 >= 1000)
                {
                    invia7F0 = TRUE;
                }
            }


            if (invia7F0)
            {
                invia7F0 = FALSE;
                timer_invio_7F0 = 0;
                stato_7F0_fast = FALSE;

                tx_temp.dlc = 8;
                tx_temp.id = ee_CanColl.unIdStatus;
                tx_temp.data[0] = val_7F0_now[0];
                tx_temp.data[1] = val_7F0_now[1];
                tx_temp.data[2] = val_7F0_now[2];
                tx_temp.data[3] = val_7F0_now[3];
                tx_temp.data[4] = val_7F0_now[4];
                tx_temp.data[5] = val_7F0_now[5];
                tx_temp.data[6] = val_7F0_now[6];
                tx_temp.data[7] = val_7F0_now[7];
                copy_tx_temp(FALSE);
            }

            //One-shot?
            if (UpdateRates.un7F0 < 100)
                UpdateRates.un7F0 = 0;
        }
        s_ucMessage = 2;
        break;

    case 2:
        if (g_b7F1Time)
        {
            g_b7F1Time = false;
            tx_temp.dlc = 8;
            tx_temp.id = ee_CanColl.unIdStatus + 1;

            stemp16 = g_nDegrees;
            if (stemp16 > 127)
                stemp16 = 127;
            if (g_nYDegrees >= 0)
                stemp16 = -stemp16;

            tx_temp.data[0] = (UINT8)stemp16;
            tx_temp.data[1] = 0;
            tx_temp.data[2] = 0;

            /*if (g_ucActualFilterType==1)
                tx_temp.data[3] = 3;
            else if (g_ucActualFilterType==3)
                    tx_temp.data[3] = 1;
                 else*/
            tx_temp.data[3] = g_ucActualFilterType;

            tx_temp.data[4] = LateralDFF & 0xFF;
            tx_temp.data[5] = (LateralDFF >> 8) & 0xFF;
            tx_temp.data[6] = 0;
            tx_temp.data[7] = 0;

            copy_tx_temp(FALSE);

            //One-shot?
            if (UpdateRates.un7F1 < 100)
                UpdateRates.un7F1 = 0;
        }
        s_ucMessage = 3;
        break;

    case 3:
        if (g_b7F9Time)
        {
            g_b7F9Time = false;
            ucByte1 = 0;
            if (g_bLightState)
                ucByte1 |= 0x01;
            if (g_ucStatusLed != RED)
                ucByte1 |= 0x20;
            if (Relays.Status.bit.r_chiuse)
                ucByte1 |= 0x80;

            ucByte2 = 0;
            if (g_ucStatusLed == YELLOW)
                ucByte2 |= 0x01;
            if (g_ucStatusLed == RED)
                ucByte2 |= 0x02;
            //              if (g_ucStatusLed==GREEN)
            //                  ucByte2|=0x04;
            if (g_ucStatusLed == GREEN)
            {
                if (timer_LED_GREEN == 0)
                    ucByte2 |= 0x04;
            }
            else
            {
                timer_LED_GREEN = 120;
            }

            if (!KEY_AUTO)
            {
                ucByte2 |= 0x08;
            }

            ucByte3 = 0;
            if (ee_ConfigColl.ucCmUnit != CM_UNIT)
                ucByte3 |= 0x80;
            if (ee_ConfigColl.ucLanguage != ITA_LANG)
                ucByte3 |= 0x20;
            if (ee_ConfigColl.ucVertDff == VERT_DFF_FIXED)
                ucByte3 |= 0x10;
            if (ee_ConfigColl.ucFilter)
                ucByte3 |= 0x08;
            if (ee_ConfigColl.ucIrisPresent)
                ucByte3 |= 0x04;
            if (ee_ConfigColl.ucLong)
                ucByte3 |= 0x02;
            if (ee_ConfigColl.ucCross)
                ucByte3 |= 0x01;

            ucByte4 = 0;
            if (ee_ConfigColl.ucVertDff == VERT_DFF_DIFF)
                ucByte4 |= 0x01;
            if (ee_ConfigColl.ucVertRec == VERT_REC_BUCKY)
                ucByte4 |= 0x02;
            if (ee_ConfigColl.ucLatRecDx == LAT_REC_BUCKY)
                ucByte4 |= 0x04;
            if (ee_ConfigColl.ucLatRecSx == LAT_REC_BUCKY)
                ucByte4 |= 0x08;

            ucByte5 = 0;
            if (ee_ConfigColl.ucFilterType == MM2_FILTER)
                ucByte5 |= 0x01;
            if (ee_ConfigColl.ucVertRec == VERT_REC_CAN ||
                    ee_ConfigColl.ucVertRec == VERT_REC_ATS)
                ucByte5 |= 0x02;
            if (ee_ConfigColl.ucLatRecDx == LAT_REC_CAN ||
                    ee_ConfigColl.ucVertRec == VERT_REC_ATS)
                ucByte5 |= 0x04;
            if (ee_ConfigColl.ucLatRecSx == LAT_REC_CAN ||
                    ee_ConfigColl.ucVertRec == VERT_REC_ATS)
                ucByte5 |= 0x08;
            if (ee_ConfigColl.ucLatDffSx == LAT_DFF_CAN)
                ucByte5 |= 0x10;
            if (ee_ConfigColl.ucLatDffDx == LAT_DFF_CAN)
                ucByte5 |= 0x20;
            if (ee_ConfigColl.ucShowDff)
                ucByte5 |= 0x40;

            ucByte7 = 0;
            if (g_InternalFlag.bit.bEepromError)
                ucByte7 |= 0x01;
            if (g_InternalFlag.bit.bEepromInitialized)
                ucByte7 |= 0x02;
            if (g_InternalFlag.bit.bEepromUpdated)
                ucByte7 |= 0x04;

            ucByte6 = (g_uwActualIrisToBeSent >> 8) & 0xFF;
            ucByte8 = g_uwActualIrisToBeSent & 0xFF;

            tx_temp.dlc = 8;
            tx_temp.id = ee_CanColl.unIdStatus + 9;
            tx_temp.data[0] = ucByte1;
            tx_temp.data[1] = ucByte2;
            tx_temp.data[2] = ucByte3;
            tx_temp.data[3] = ucByte4;
            tx_temp.data[4] = ucByte5;
            tx_temp.data[5] = ucByte6;
            tx_temp.data[6] = ucByte7;
            tx_temp.data[7] = ucByte8;
            copy_tx_temp(FALSE);

            //One-shot?
            if (UpdateRates.un7F9 < 100)
                UpdateRates.un7F9 = 0;
        }
        s_ucMessage = 4;
        break;
    case 4:
        if (g_b7FCTime)
        {
            g_b7FCTime = false;
            tx_temp.dlc = 8;
            tx_temp.id = ee_CanColl.unIdStatus + 0x0C;
            tx_temp.data[0] = g_lVPos[STEPPER_CROSS] >> 8;
            tx_temp.data[1] = g_lVPos[STEPPER_CROSS];
            tx_temp.data[2] = g_lVPos[STEPPER_LONG] >> 8;
            tx_temp.data[3] = g_lVPos[STEPPER_LONG];
            tx_temp.data[4] = g_lVPos[STEPPER_FILTER] >> 8;
            tx_temp.data[5] = g_lVPos[STEPPER_FILTER];
            //TODO: tx_temp.data[6] = (FULL_B << 4) | (FULL_A << 3) | (HOME_C << 2) | (HOME_B << 1) | (HOME_A);
            tx_temp.data[7] = 0;
            copy_tx_temp(FALSE);
            //One-shot?
            if (UpdateRates.un7FC < 100)
                UpdateRates.un7FC = 0;
        }
        s_ucMessage++;
        break;
    case 5:
        if (g_b7F2Time)
        {
            g_b7F2Time = false;
            tx_temp.dlc = 8;
            tx_temp.id = ee_CanColl.unIdStatus + 2;

            tx_temp.data[0] = ee_RSR008B_Version.cbSerial[0];
            tx_temp.data[1] = ee_RSR008B_Version.cbSerial[1];
            tx_temp.data[2] = ee_RSR008B_Version.cbSerial[2];
            tx_temp.data[3] = ee_RSR008B_Version.cbSerial[3];
            tx_temp.data[4] = ee_RSR008B_Version.cbSerial[4];
            tx_temp.data[5] = ee_RSR008B_Version.cbSerial[5];
            tx_temp.data[6] = ee_RSR008B_Version.cbSerial[6];
            tx_temp.data[7] = ee_RSR008B_Version.cbSerial[7];
            copy_tx_temp(FALSE);
        }
        s_ucMessage = 0;
        if (ee_ConfigColl.ucVertRec == VERT_REC_ATS)
            s_ucMessage = 6;
        else
            s_ucMessage = 12;
        break;
    case 6:
        if (g_b082Time)
        {
            g_b082Time = false;
            tx_temp.id = 0x082;
            tx_temp.dlc = 4;

            tx_temp.data[0] = 0x03;
            tx_temp.data[1] = 0xE8;
            tx_temp.data[2] = 0;
            tx_temp.data[3] = 0;
            copy_tx_temp(FALSE);
        }
        s_ucMessage = 7;
        break;
    case 7:
        if (g_b083Time)
        {
            g_b083Time = false;
            tx_temp.id = 0x083;
            tx_temp.dlc = 8;

            tx_temp.data[0] = 0x40;
            tx_temp.data[1] = 0;
            tx_temp.data[2] = 0;
            tx_temp.data[3] = 0;
            tx_temp.data[4] = 0;
            tx_temp.data[5] = 0;
            tx_temp.data[6] = 0;
            tx_temp.data[7] = 0;
            copy_tx_temp(FALSE);
        }
        s_ucMessage = 8;
        break;
    case 8:
        if (g_b084Time)
        {
            g_b084Time = false;
            tx_temp.id = 0x084;
            tx_temp.dlc = 3;

            tx_temp.data[0] = 0x0F;
            tx_temp.data[1] = 0xA0;
            tx_temp.data[2] = 0;
            copy_tx_temp(FALSE);
        }
        s_ucMessage = 9;
        break;
    case 9:
        if (g_b085Time && !g_bAckReceived)
        {
            g_b085Time = false;
            tx_temp.id = 0x085;
            tx_temp.dlc = 8;

            tx_temp.data[0] = 0x56;
            tx_temp.data[1] = 0x65;
            tx_temp.data[2] = 0x72;
            tx_temp.data[3] = 0x20;
            tx_temp.data[4] = 0x34;
            tx_temp.data[5] = 0x2E;
            tx_temp.data[6] = 0x30;
            tx_temp.data[7] = 0x30;
            copy_tx_temp(FALSE);
        }
        s_ucMessage = 10;
        break;
    case 10:
        if (g_b101Time && !g_bAckReceived)
        {
            g_b101Time = false;
            tx_temp.id = 0x101;
            tx_temp.dlc = 8;

            tx_temp.data[0] = 0;
            tx_temp.data[1] = 0x02;
            tx_temp.data[2] = 0;
            tx_temp.data[3] = 0;
            tx_temp.data[4] = 0;
            tx_temp.data[5] = 0;
            tx_temp.data[6] = 0;
            tx_temp.data[7] = 0;
            copy_tx_temp(FALSE);
        }
        s_ucMessage = 11;
        break;
    case 11:
        if (g_b103Time)
        {
            g_b103Time = false;
            tx_temp.id = 0x103;
            tx_temp.dlc = 3;

            tx_temp.data[0] = 0x0F;
            tx_temp.data[1] = 0x0A;
            tx_temp.data[2] = 0x00;
            copy_tx_temp(FALSE);
        }
        s_ucMessage = 12;
        break;
    case 12:
        //Version request to ASR003
        if (g_b7AETime)
        {
            g_b7AETime = false;
            tx_temp.id = 0x7AE;
            tx_temp.dlc = 8;
            tx_temp.data[0] = 0;
            tx_temp.data[1] = 0;
            tx_temp.data[2] = 0;
            tx_temp.data[3] = 0;
            tx_temp.data[4] = 0;
            tx_temp.data[5] = 0;
            tx_temp.data[6] = 0;
            tx_temp.data[7] = 0;
            copy_tx_temp(FALSE);
        }
        s_ucMessage = 13;
        break;

    case 13:                // 0x7F5
        if (ee_ConfigColl.TempoInvio7F5)
        {
                                            //  BYTE 0
            temp16 = g_nFilteredAdc[POT_CROSS];
            val_7F5_now[0] = temp16 >> 8;
            val_7F5_now[1] = temp16 & 0xFF;
                                            //  BYTE 2
            temp16 = g_nFilteredAdc[POT_LONG];
            val_7F5_now[2] = temp16 >> 8;
            val_7F5_now[3] = temp16 & 0xFF;
                                            //  BYTE 4
            val_7F5_now[4] = 0;
            val_7F5_now[5] = 0;
            val_7F5_now[6] = 0;
            val_7F5_now[7] = 0;

    /*
            for (i = 0; i < 8; i++)     // confronta bit che provocano l'invio immediato (es: movimento e luce)
            {
                if ((val_7F5_now[i] ^ val_7F5_old[i]) & mask_7F5_invio[i])
                {
                    val_7F5_old[i] = val_7F5_now[i];
                    invia7F5 = TRUE;
                }
            }
    */

            for (i = 0; i < 8; i++)
            {
                if (val_7F5_now[i] != val_7F5_old[i])
                {
                    val_7F5_old[i] = val_7F5_now[i];
                    stato_7F5_fast = TRUE;
                }
            }

            if (stato_7F5_fast)
            {
                if (timer_invio_7F5 >= ee_ConfigColl.TempoInvio7F5)
                {
                    invia7F5 = TRUE;
                }
            }
            else
            {
                if (timer_invio_7F5 >= 1000)
                {
                    invia7F5 = TRUE;
                }
            }


            if (invia7F5)
            {
                invia7F5 = FALSE;
                timer_invio_7F5 = 0;
                stato_7F5_fast = FALSE;

                tx_temp.dlc = 8;
                tx_temp.id = ee_CanColl.unIdStatus + 5;
                tx_temp.data[0] = val_7F5_now[0];
                tx_temp.data[1] = val_7F5_now[1];
                tx_temp.data[2] = val_7F5_now[2];
                tx_temp.data[3] = val_7F5_now[3];
                tx_temp.data[4] = val_7F5_now[4];
                tx_temp.data[5] = val_7F5_now[5];
                tx_temp.data[6] = val_7F5_now[6];
                tx_temp.data[7] = val_7F5_now[7];
                copy_tx_temp(FALSE);
            }
        }
        s_ucMessage = 14;
        break;

    case 14:                //*********************************************************
                            //  controlla se deve trasmettere gli eventi
                            //*********************************************************
        if (ixLogEvI != ixLogEvF)
        {
            ptrEv = &fifo_eventi[ixLogEvF];			// punta all'evento
            if (++ixLogEvF >= NUM_MAX_EVENTI)
                ixLogEvF = 0;

            if (EnableInvioEventi == TRUE)
            {
                tx_temp.id = ee_CanColl.unIdStatus + 4;
                tx_temp.dlc = 8;
                tx_temp.data[0] = ptrEv->Evento;
                tx_temp.data[1] = ptrEv->Motore;
                tx_temp.data[2] = ptrEv->Par1;
                tx_temp.data[3] = ptrEv->Par2;
                tx_temp.data[4] = (UINT8)(ptrEv->dato >> 24);
                tx_temp.data[5] = (UINT8)(ptrEv->dato >> 16);
                tx_temp.data[6] = (UINT8)(ptrEv->dato >> 8);
                tx_temp.data[7] = (UINT8)(ptrEv->dato);
                copy_tx_temp(FALSE);
            }
        }
        s_ucMessage = 15;
        break;

    case 15:
        if (ee_ConfigColl.TempoInvioInclinometro)
        {
            if (timer_invio_inclinometro >= ee_ConfigColl.TempoInvioInclinometro)
            {
                timer_invio_inclinometro = 0;
                tx_temp.id = ee_CanColl.unIdStatus + 7;
                tx_temp.dlc = 8;
                tx_temp.data[0] = 0x01;
                temp16 = valoreAsseX;
                tx_temp.data[1] = temp16 >> 8;
                tx_temp.data[2] = temp16 & 0xFF;
                temp16 = valoreAsseY;
                tx_temp.data[3] = temp16 >> 8;
                tx_temp.data[4] = temp16 & 0xFF;
                copy_tx_temp(FALSE);
            }
        }
        s_ucMessage = 0;
        break;

    }

}




//************************************************
//  ricezione messaggi in modo WORK
//************************************************

void sys_can_work(void)
{
    uword uwStartupTime;
    CAN_STD_DATA_DEF *ptr;
    UINT16 Base;
    UINT8 SubCommand;


    sys_Main_GetSecond(&uwStartupTime);
    if (uwStartupTime > 10)
        g_b7AETime = false;

    sys_can_tx_status(); // manda messaggi di stato se tempo

    if (ixRI != ixRF) // ******  REMOTE  ******
    {
        // TODO: da completare dis irq canbus
        //can00e = 0; // can01 = can0 rx irq
        Fill_rxTemp(&rx_dati_R[ixRF]);
        if (++ixRF >= NUM_BUFF_RX_R)
            ixRF = 0;

        // TODO: da completare en irq canbus
        //can00e = 1; // can01 = can0 rx irq

        Base = rx_temp.id & 0xFFF0;
        SubCommand = rx_temp.id & 0x0F;

        if (Base == ee_CanColl.unIdOtherCommands) // comandi con base collimatore
        {
            switch (SubCommand)
            {
            case CAN_CONFR_COLLIMATOR:
                CAN_config_firmware(TRUE);
                break;
            }
        }
    }

    if (ix0I != ix0F) // controlla se messaggio in coda
    { // copia in buffer temporaneo
        //TODO: da completare dis irq canbus
        //can00e = 0; // can01 = can0 rx irqdis irq canbus
        ptr = &rx_dati_0[ix0F];
        rx_temp.id = ptr->id;
        rx_temp.dlc = ptr->dlc;

        rx_temp.data[0] = ptr->data.data[0];
        rx_temp.data[1] = ptr->data.data[1];
        rx_temp.data[2] = ptr->data.data[2];
        rx_temp.data[3] = ptr->data.data[3];
        rx_temp.data[4] = ptr->data.data[4];
        rx_temp.data[5] = ptr->data.data[5];
        rx_temp.data[6] = ptr->data.data[6];
        rx_temp.data[7] = ptr->data.data[7];

        if (++ix0F >= NUM_BUFF_RX_O)
            ix0F = 0;
        //TODO: da completare en irq canbus
        //can00e = 1; // can01 = can0 rx irq

        Base = rx_temp.id & 0xFFF0;
        SubCommand = rx_temp.id & 0x000F;

        if (rx_temp.id == ee_CanColl.unIdCommand) // comando 7A0
        {
            sys_can_7A0();
        }
        else if (Base == ee_CanColl.unIdIride) // ricezione da IRIDE
        {
            switch (SubCommand)
            {
            case 0:     // Versione firmware
                sys_can_IRIS_VERSION();
                break;
            case 1:     // Status
                sys_can_IRIS_STATUS();
                break;
            }
        }
        else if (Base == ee_CanColl.unIdRot) // ricezione da ROTAZIONE
        {
            switch (SubCommand)
            {
            case 0:     // Versione firmware
                sys_can_ROT_VERSION();
                break;
            }
        }
        else if (Base == ee_CanColl.unIdDisplay) // ricezione per scrittura DISPLAY
        {
            switch (SubCommand)
            {
            case 0:
                sys_can_DISPLAY_0();
                break;
            case 1:
                sys_can_DISPLAY_1();
                break;
            }
        }
        else if (Base == ID_OtherCommand) // ricezione comandi 0x7Ax
        {
            switch (SubCommand) // comandi con base 0xA0
            {
            case 0x01:
                sys_can_7A1();
                break;
            case 0x02:
                sys_can_7A2();
                break;
            case 0x03:
                sys_can_7A3();
                break;
            case 0x04:
                sys_can_7A4();
                break;
            case 0x05:
                sys_can_7A5();
                break;
            case 0x07:
                sys_can_7A7();
                break;
            case 0x08:
                sys_can_7A8();
                break;
            case 0x09:
                sys_can_7A9();
                break;
            case 0x0A:
                if (ee_ConfigColl.ASR003)
                {
                    sys_can_7AA(FALSE);
                }
                break;
            case 0x0B:
                if (ee_ConfigColl.ASR003)
                {
                    sys_can_7AB(FALSE);
                }
                break;
            case 0x0C:
                if (ee_ConfigColl.ASR003)
                {
                    sys_can_7AC(FALSE);
                }
                break;
            case 0x0D:
                sys_can_7AD();
                break;
            case 0x0E:
                if (ee_ConfigColl.ASR003)
                {
                    sys_can_7AE();
                }
                break;
            case 0x0F:
                sys_can_7AF();
                break;
            }
        }
        else if (Base == 0x7A0) // ricezione da ASR003 se configurata
        {
            if (ee_ConfigColl.ASR003)
            {
                switch (SubCommand) // comandi con base 0xA0
                {
                case 0x0A:
                    sys_can_7AA(FALSE);
                    break;
                case 0x0B:
                    sys_can_7AB(FALSE);
                    break;
                case 0x0C:
                    sys_can_7AC(FALSE);
                    break;
                case 0x0E:
                    sys_can_7AE();
                    break;
                }
            }
        }
    }

}


//******************************************
//  controlla se sensore di prossimita'
//  esiste ed e' attivato
//******************************************

UINT8 CheckProximity(void)
{
    UINT8 ret;

    ret = FALSE;

    if (buffer_digitali[IN_PROXY] == TRUE)
        ret = TRUE;

//    if (ee_ConfigColl.ucAnalogInput == DIGITAL_IN)
//    {
//        if (ee_ConfigColl.ucTipoProx == TIPO_PROX_PNP)
//        {
//            if (ANA_DIG_IN_PNP == 1)
//                ret = TRUE;
//        }
//        else
//        {
//            if (ANA_DIG_IN_NPN == 0)
//                ret = TRUE;
//        }
//    }
//
    return ret;
}


