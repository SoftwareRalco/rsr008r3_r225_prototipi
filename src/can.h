
#ifndef CAN_H_
#define CAN_H_

#include "r_can_api.h"


/* Bit set defines */
#define     SLOT_15 0x0001
#define     SLOT_14 0x0002
#define     SLOT_13 0x0004
#define     SLOT_12 0x0008
#define     SLOT_11 0x0010
#define     SLOT_10 0x0020
#define     SLOT_9  0x0040
#define     SLOT_8  0x0080
#define     SLOT_7  0x0100
#define     SLOT_6  0x0200
#define     SLOT_5  0x0400
#define     SLOT_4  0x0800
#define     SLOT_3  0x1000
#define     SLOT_2  0x2000
#define     SLOT_1  0x4000
#define     SLOT_0  0x8000



#ifdef CAN_H
#define can_ext
/* Mem. area for bit set defines */
const UINT16 bit_set[16] = {SLOT_0,  SLOT_1,  SLOT_2,  SLOT_3,
                            SLOT_4,  SLOT_5,  SLOT_6,  SLOT_7,
                            SLOT_8,  SLOT_9,  SLOT_10, SLOT_11,
                            SLOT_12, SLOT_13, SLOT_14, SLOT_15 };
#else
#define can_ext extern
extern const UINT16 bit_set[16];
#endif


//*******************
//**  DEFINIZIONI  **
//*******************

#define FLAG_CAN_EVENT_RX_COMPLETE                  (1u << 0)
#define FLAG_CAN_EVENT_TX_COMPLETE                  (1u << 1)
#define FLAG_CAN_EVENT_ERR_BUS_OFF                  (1u << 2)
#define FLAG_CAN_EVENT_BUS_RECOVERY                 (1u << 3)
#define FLAG_CAN_EVENT_ERR_PASSIVE                  (1u << 4)
#define FLAG_CAN_EVENT_MAILBOX_OVERWRITE_OVERRUN    (1u << 5)
#define FLAG_CAN_UNEXPECTED_EVENT                   (1u << 6)


#define CAN_TFT_FW_VER       0x01
#define CAN_TFT_APERTURE     0x02
#define CAN_TFT_ICONE        0x03



#define BASE_TIMER_LUCE     10

#define ID_CONFIGURAZIONE   0x600
#define ID_CONFIG_WRITE     0x600
#define ID_CONFIG_REQ       0x601
#define ID_CONFIG_READ      0x602

#define CAN_SLOT_STANDARD   0x01
#define CAN_SLOT_MOTORI     0x02
#define CAN_SLOT_NO_MOTORI  0x04
#define CAN_SLOT_CONFIG     0x08

#define DEF_TIME_LED_CAN_ON     4       // in 1/100 secondo
#define DEF_TIME_LED_CFG_ON     100       // in 1/100 secondo


#define SLOT_ID_COLL        0       // slot di ricezione con base ID_COLL
#define SLOT_ID_7Ax         1
#define SLOT_MOT_0          2       // slot comandi specifici motore ID_MOTORI
#define SLOT_MOT_1          3       // slot comandi specifici motore
#define SLOT_MOT_2          4       // slot comandi specifici motore
#define SLOT_CONFIG_STD     5       // slot comandi per configurazioni varie ID_CONFIG
#define SLOT_ID_IRIDE       6       // slot per ricezione status iride
#define SLOT_ID_ROTAZIONE   7       // slot per ricezione status rotazione
#define SLOT_CONFIG_REM     8       // slot comandi per configurazioni varie ID_CONFIG
#define SLOT_ID_DISPLAY     9       // slot per ricezione status iride

#define SLOT_TX             10      // slot per trasmissione messaggi



/* Interrupts */
#define     CAN0_LVL        7
#define     CAN1_LVL        6
#define     CAN2_LVL        5
#define     CAN3_LVL        4
#define     CAN4_LVL        3
#define     CAN5_LVL        2
#define     CAN_ERROR_LVL   1     /* 0 = CAN Error interrupt disabled */

#define     SENDING_DELAY   (100000UL)
#define     POLLING_DELAY   (100000UL)

/* CAN ERRORS */
#define     NO_ERROR            0
#define     CAN_BUS_ERROR           1
#define     CAN_BUS_ERROR_PASSIVE       2
#define     CAN_BUS_OFF         4
/* Can peripheral while loop timer defines */
#define     MAX_CAN_SW_DELAY        100
#define     RESET_CAN0_SW_TMR       can0_sw_tmr=MAX_CAN_SW_DELAY
#define     RESET_CAN1_SW_TMR       can1_sw_tmr=MAX_CAN_SW_DELAY
#define     DEC_CHK_CAN0_SW_TMR     (--can0_sw_tmr != 0)
#define     DEC_CHK_CAN1_SW_TMR     (--can1_sw_tmr != 0)
/* One bit flag for each while loop in canx.c */
#define     CAN_SW_RST_ERR          0x01
#define     CAN_SW_TRM_ERR          0x02
#define     CAN_SW_TRM_DATA_ERR     0x04
#define     CAN_SW_CHK_TRM_ERR      0x08
#define     CAN_SW_SET_REC_ERR      0x10
#define     CAN_SW_GET_ERR          0x20
#define     CAN_SW_ABORT_ERR        0x40
#define     CAN_SW_OTHER_ERR        0x80

#define     DATA_NOT_RECIEVED       0x01

#ifdef SYS_CANOPEN
#define SLOT_REMOTE         11       // ricezione messaggi remote
#endif

typedef struct
{
    UINT16 id;
    UINT8 dlc;
    UINT8 remote;
    union
    {
        UINT8 data[8];
        UINT64 data_8;
    } data;
} CAN_STD_DATA_DEF;

typedef struct
{
    unsigned short id;
    unsigned char dlc;
    UINT8 remote;
    unsigned char data[8];
} CAN_MOD_DATA_DEF;


//*****************
//**  VARIABILI  **
//*****************

can_ext CAN_MOD_DATA_DEF rx_temp;
//can_ext CAN_STD_DATA_DEF mov_master;

can_ext UINT16 can0_sw_tmr;
can_ext UINT8 can0_sw_err;

#define NUM_BUFF_RX_R       4           // comandi in ricezione REMOTE
can_ext UINT16 ixRI, ixRF;
can_ext CAN_STD_DATA_DEF rx_dati_R[NUM_BUFF_RX_R];

#define NUM_BUFF_RX_C       4           // comandi in ricezione per configurazione
can_ext UINT16 ixCI, ixCF;
can_ext CAN_STD_DATA_DEF rx_dati_C[NUM_BUFF_RX_C];


#define NUM_BUFF_RX_O       64           // comandi in ricezione
can_ext UINT16 ix0I, ix0F;
can_ext CAN_STD_DATA_DEF rx_dati_0[NUM_BUFF_RX_O];

#define NUM_BUFF_TX_CAN     32           // messaggi in trasmissione
can_ext UINT16 ixTI, ixTF;
can_ext CAN_STD_DATA_DEF tx_dati_0[NUM_BUFF_TX_CAN];
can_ext CAN_MOD_DATA_DEF tx_temp;
can_ext UINT8 block_tx;

#define NUM_BUFF_RX_M       64           // comandi in ricezione per collimatore
can_ext UINT16 ixMI, ixMF;
can_ext CAN_STD_DATA_DEF rx_dati_M[NUM_BUFF_RX_M];


can_ext UINT16 timer_tx_can;        // 1/100 s - timeout per slot libero in trasmissione

can_ext UINT8 num_motore;
can_ext UINT16 ID_OtherCommand;


//*****************************
//**  PROTOTIPI DI FUNZIONE  **
//*****************************

void put_can_fifo(UINT8 slot);
void CAN_crea_slot(UINT8 tipo);
void CAN_init(UINT8 mode);


void gestione_irq_canbus(can_callback_args_t * p_args);


#endif /* CAN_H_ */
//
//
//
//
//
//
//
//
//
//
//
///* Bit set defines */
//#define		SLOT_15 0x0001
//#define		SLOT_14 0x0002
//#define		SLOT_13 0x0004
//#define		SLOT_12 0x0008
//#define		SLOT_11 0x0010
//#define		SLOT_10 0x0020
//#define		SLOT_9  0x0040
//#define		SLOT_8  0x0080
//#define		SLOT_7  0x0100
//#define		SLOT_6  0x0200
//#define		SLOT_5  0x0400
//#define		SLOT_4  0x0800
//#define		SLOT_3  0x1000
//#define		SLOT_2  0x2000
//#define		SLOT_1  0x4000
//#define		SLOT_0  0x8000
//
//
//
//#ifdef CAN_H
//#define can_ext
///* Mem. area for bit set defines */
//const UINT16 bit_set[16] = {SLOT_0,  SLOT_1,  SLOT_2,  SLOT_3,
//							SLOT_4,  SLOT_5,  SLOT_6,  SLOT_7,
//							SLOT_8,  SLOT_9,  SLOT_10, SLOT_11,
//				            SLOT_12, SLOT_13, SLOT_14, SLOT_15 };
//#else
//#define can_ext extern
//extern const UINT16 bit_set[16];
//#endif
//
//
////*******************
////**  DEFINIZIONI  **
////*******************
//
//#define BASE_TIMER_LUCE     10
//
//#define ID_CONFIGURAZIONE   0x600
//#define ID_CONFIG_WRITE     0x600
//#define ID_CONFIG_REQ       0x601
//#define ID_CONFIG_READ      0x602
//
//#define CAN_SLOT_STANDARD   0x01
//#define CAN_SLOT_MOTORI     0x02
//#define CAN_SLOT_NO_MOTORI  0x04
//#define CAN_SLOT_CONFIG     0x08
//
//#define DEF_TIME_LED_CAN_ON     4       // in 1/100 secondo
//#define DEF_TIME_LED_CFG_ON     100       // in 1/100 secondo
//
//
////  maschera impostata come 0xFFF0
//#define SLOT_ID_7Ax         1
//#define SLOT_MOT_0          2       // slot comandi specifici motore ID_MOTORI
//#define SLOT_MOT_1          3       // slot comandi specifici motore
//#define SLOT_MOT_2          4       // slot comandi specifici motore
//#define SLOT_CONFIG_STD     5       // slot comandi per configurazioni varie ID_CONFIG
//#define SLOT_ID_IRIDE       6       // slot per ricezione status iride
//#define SLOT_ID_ROTAZIONE   7       // slot per ricezione status rotazione
//#define SLOT_ID_ASR003      8       // slot per ricezione ASR003
//#define SLOT_ID_DISPLAY     9       // slot per ricezione comandi display
//
//#define SLOT_TX             10      // slot per trasmissione messaggi
//
////  maschera impostata come 0xFFFF
//#define SLOT_CONFIG_REM     14       // slot comandi per configurazioni
//#define SLOT_ID_COLL        15       // slot di ricezione con base ID_COLL
//
//
//
///* Interrupts */
//#define		CAN0_LVL        7
//#define		CAN1_LVL        6
//#define		CAN2_LVL        5
//#define		CAN3_LVL        4
//#define		CAN4_LVL        3
//#define		CAN5_LVL        2
//#define 	CAN_ERROR_LVL  	1     /* 0 = CAN Error interrupt disabled */
//
//#define 	SENDING_DELAY	(100000UL)
//#define 	POLLING_DELAY	(100000UL)
//
///* CAN ERRORS */
//#define		NO_ERROR			0
//#define		CAN_BUS_ERROR			1
//#define		CAN_BUS_ERROR_PASSIVE		2
//#define		CAN_BUS_OFF			4
///* Can peripheral while loop timer defines */
//#define     MAX_CAN_SW_DELAY		100
//#define     RESET_CAN0_SW_TMR		can0_sw_tmr=MAX_CAN_SW_DELAY
//#define     RESET_CAN1_SW_TMR		can1_sw_tmr=MAX_CAN_SW_DELAY
//#define     DEC_CHK_CAN0_SW_TMR		(--can0_sw_tmr != 0)
//#define     DEC_CHK_CAN1_SW_TMR		(--can1_sw_tmr != 0)
///* One bit flag for each while loop in canx.c */
//#define		CAN_SW_RST_ERR			0x01
//#define		CAN_SW_TRM_ERR			0x02
//#define		CAN_SW_TRM_DATA_ERR		0x04
//#define		CAN_SW_CHK_TRM_ERR		0x08
//#define		CAN_SW_SET_REC_ERR		0x10
//#define		CAN_SW_GET_ERR			0x20
//#define		CAN_SW_ABORT_ERR		0x40
//#define		CAN_SW_OTHER_ERR		0x80
//
//#define		DATA_NOT_RECIEVED		0x01
//
//#ifdef SYS_CANOPEN
//#define SLOT_REMOTE         11       // ricezione messaggi remote
//#endif
//
//typedef struct
//{
//    UINT16 id;
//    UINT8 dlc;
//    UINT8 remote;
//    union
//	{
//        UINT8 data[8];
//        UINT64 data_8;
//	} data;
//} CAN_STD_DATA_DEF;
//
//typedef struct
//{
//    unsigned short id;
//    unsigned char dlc;
//    UINT8 remote;
//    unsigned char data[8];
//} CAN_MOD_DATA_DEF;
//
//
////*****************
////**  VARIABILI  **
////*****************
//
//can_ext CAN_MOD_DATA_DEF rx_temp;
////can_ext CAN_STD_DATA_DEF mov_master;
//
//can_ext UINT16 can0_sw_tmr;
//can_ext UINT8 can0_sw_err;
//
//#define NUM_BUFF_RX_R       4           // comandi in ricezione REMOTE
//can_ext UINT16 ixRI, ixRF;
//can_ext CAN_STD_DATA_DEF rx_dati_R[NUM_BUFF_RX_R];
//
//#define NUM_BUFF_RX_C       4           // comandi in ricezione per configurazione
//can_ext UINT16 ixCI, ixCF;
//can_ext CAN_STD_DATA_DEF rx_dati_C[NUM_BUFF_RX_C];
//
//
//#define NUM_BUFF_RX_O       64           // comandi in ricezione
//can_ext UINT16 ix0I, ix0F;
//can_ext CAN_STD_DATA_DEF rx_dati_0[NUM_BUFF_RX_O];
//
//#define NUM_BUFF_TX_CAN     32           // messaggi in trasmissione
//can_ext UINT16 ixTI, ixTF;
//can_ext CAN_STD_DATA_DEF tx_dati_0[NUM_BUFF_TX_CAN];
//can_ext CAN_MOD_DATA_DEF tx_temp;
//can_ext UINT8 block_tx;
//
//#define NUM_BUFF_RX_M       64           // comandi in ricezione per collimatore
//can_ext UINT16 ixMI, ixMF;
//can_ext CAN_STD_DATA_DEF rx_dati_M[NUM_BUFF_RX_M];
//
//can_ext UINT16 timer_tx_can;        // 1/100 s - timeout per slot libero in trasmissione
//
//can_ext UINT8 num_motore;
//
//
//
////*****************************
////**  PROTOTIPI DI FUNZIONE  **
////*****************************
//
//void put_can_fifo(UINT8 slot);
//void CAN_crea_slot(UINT8 tipo);
//void CAN_init(UINT8 mode);
//void set_rec_std_dataframe_can0(unsigned short  slot_nr, unsigned short sid, UINT8 irq);
//void set_rec_rem_dataframe_can0(unsigned short  slot_nr, unsigned short sid, UINT8 irq);
//
//
