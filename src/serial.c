

#define _SYS_SERIAL_

//--------------------------------------------------------------
// PRIMA STESURA
// Riva ing. Franco M.
// Besana Brianza (MI)
//
// REVISIONI SUCCESSIVE
// Giovanni Corvini
// Melegnano (MI)
//--------------------------------------------------------------
//
// PROJECT:	  RSR008 - Low Cost Collimator
// FILE:      sys_lcd_driver.c
// REVISIONS: 0.00 - 22/09/2008 - First Definition
//
//--------------------------------------------------------------

//--------------------------------------------------------------
// INCLUDE FILES
//--------------------------------------------------------------
#include "include.h"
/*
#include "sfr32C87.h"
#include "sys_defines.h"
#include "sys_can_under_test.h"
#include "sys_lcd_driver.h"
#include "sys_display.h"
#include "virtEE_Variable.h"    // Virtual EEPROM (variable size records) function definitions
#include "sys_control.h"
#include "sys_test.h"
#include "sys_main.h"
*/
#include "string.h"
#include "math.h"

//--------------------------------------------------------------
// GLOBAL VARIABLES
//--------------------------------------------------------------
#define SERIAL_PROCESS_INIT			    'I'
#define SERIAL_PROCESS_HOME			    'H'

static uchar g_ucSerialProcessState= SERIAL_PROCESS_INIT;

//--------------------------------------------------------------
// INTERRUPT FUNCTIONS
//--------------------------------------------------------------

//--------------------------------------------------------------
// LOCAL FUNCTIONS
//--------------------------------------------------------------

//--------------------------------------------------------------
// SYSTEM FUNCTIONS
//--------------------------------------------------------------





//*************************************************
//  irq di ricezione seriale 0
//*************************************************
//#pragma interrupt /E uart0_receive
void uart0_receive(void)
{
    UINT8 dato;

//TODO: serial irq
/*
    ir_s0ric = 0;
    dato = u0rb;
*/
    if (num_seriale_config == 0)
    {
        FifoSerC[PtSerIC] = dato;   // legge carattere dalla seriale
        PtSerIC++;
        PtSerIC &= (DIM_FIFO_SER - 1);        // n caratteri massimi nel buffer circolare
        timer_rx_ser = TIME_INTERCHAR_RX;
    }
    else if (num_seriale_metro == 0)
    {
        FifoSerM[PtSerIM] = dato;   // legge carattere dalla seriale
        PtSerIM++;
        PtSerIM &= (DIM_FIFO_SER - 1);        // n caratteri massimi nel buffer circolare
        timer_rx_metro = TIME_INTERCHAR_RX_M;
    }

}

//*************************************************
//  irq di ricezione seriale 1
//*************************************************
//#pragma interrupt /E uart1_receive
void uart1_receive(void)
{
    UINT8 dato;

    //TODO: serial irq
    /*
    ir_s1ric = 0;
    dato = u1rb;
*/
    if (num_seriale_config == 1)
    {
        FifoSerC[PtSerIC] = dato;   // legge carattere dalla seriale
        PtSerIC++;
        PtSerIC &= (DIM_FIFO_SER - 1);        // n caratteri massimi nel buffer circolare
        timer_rx_ser = TIME_INTERCHAR_RX;
    }
    else if (num_seriale_metro == 1)
    {
        FifoSerM[PtSerIM] = dato;   // legge carattere dalla seriale
        PtSerIM++;
        PtSerIM &= (DIM_FIFO_SER - 1);        // n caratteri massimi nel buffer circolare
        timer_rx_metro = TIME_INTERCHAR_RX_M;
    }

}








//***********************************
//  inizializza seriale
//***********************************
#define b4800 207
#define b9600 103
#define b19200 51
#define b38400 25
#define b57600 17
#define b115200 8
#define b250000 3

//#define speed_seriale   b38400
//#define speed_seriale   b115200
#define SPEED_SERIALE   b9600

void serial_init_config(void)
{
    if (num_seriale_config == -1)
        return;

    stato_rx_ser = ST_WAIT_STX;     // inizializzazione variabili d'uso
    PtSerIC = 0;
    PtSerFC = 0;
    _ok_tx_ser = FALSE;
    _ok_dato_seriale = FALSE;

//TODO: serial config
//    switch (num_seriale_config)
//    {
//    case 0:
//        u0mr = 0x05;// 8 bit - internal clock - 1 stop - no
//        u0c0 = 0x10; // clock f1sio - no CTS/RTS
//        u0brg = SPEED_SERIALE;
//
//        u0tb = u0rb;
//        u0tb = 0;
//        u0c1 = 0x05; // rx + tx
//
//        ps0_2 = 0;
//        ps0_3 = 1;
//        s0ric = SERIAL_PRIO;        // abilita irq
//        break;
//
//    case 1:
//        u1mr = 0x05;// 8 bit - internal clock - 1 stop - no
//        u1c0 = 0x10; // clock f1sio - no CTS/RTS
//        u1brg = SPEED_SERIALE;
//
//        u1tb = u1rb;
//        u1tb = 0;
//        u1c1 = 0x05; // rx + tx
//
//        ps0_6 = 0;
//        ps0_7 = 1;
//        s1ric = SERIAL_PRIO;        // abilita irq
//        break;
//    }

}


#define SPEED_SERIALE_METRO   b9600

void serial_init_metro(void)
{
    if (num_seriale_metro == -1)
        return;

//TODO: serial init metro
/*
    switch (num_seriale_metro)
    {
    case 0:
        u0mr = 0x15;// 8 bit - internal clock - 2 stop - no
        u0c0 = 0x10; // clock f1sio - no CTS/RTS
        u0brg = SPEED_SERIALE_METRO;

        u0tb = u0rb;
        u0tb = 0;
        u0c1 = 0x05; // rx + tx

        ps0_2 = 0;
        ps0_3 = 1;
        s0ric = SERIAL_PRIO;        // abilita irq
        break;

    case 1:
        u1mr = 0x15;// 8 bit - internal clock - 2 stop - no
        u1c0 = 0x10; // clock f1sio - no CTS/RTS
        u1brg = SPEED_SERIALE_METRO;

        u1tb = u1rb;
        u1tb = 0;
        u1c1 = 0x05; // rx + tx

        ps0_6 = 0;
        ps0_7 = 1;
        s1ric = SERIAL_PRIO;        // abilita irq
        break;
    }
*/
    PtSerIM = 0;
    PtSerFM = 0;

}



/*
void sys_serial_init(void){

	memset(cbTxBuffer, 0, TX_SIZE);
	memset(cbRxBuffer, 0, RX_SIZE);
	g_bNewSerialMessageReceived=false;

}
*/






//***********************************************************
//  Riceve messaggio di configurazione da seriale
//***********************************************************
void check_rx_messaggio(void)
{
    UINT8 i, flag, tipo;
    UINT8 buffer[8];
    UINT8 *ptchar;
    UINT16 temp16;

    if (num_seriale_config == -1)
        return;

    if (_ok_tx_ser == TRUE)      // se sta trasmettendo allora esce
        return;

    if (rx_msg() == TRUE)
    {
        flag = FALSE;
        for (i = 0; i < MSG_SER_FINE; i++)          // ricerca messaggio arrivato
        {
            if (strstr(buff_rx_ser, pt_messaggi_ser[i].pt_msg))
            {
                flag = TRUE;        // se esiste marca il tipo
                tipo = pt_messaggi_ser[i].num_msg;
                break;
            }
        }

        if (flag == TRUE)       // esegue operazione richiesta
        {
            switch (tipo)      // riconosce messaggio
            {
            case MSG_SER_INIT:        // risponde con OK
                for (i = 0; i < 8; i++)
                {
                    buff_rx_can[i] = 0;   // setta il buffer a 0
                }
                _ok_dato_seriale = TRUE;
//                invia_ser_ok();
                break;

            case MSG_SER_CONFIG_WRITE:        // configura un parametro 0x600
            case MSG_SER_CONFIG_REQ:          // legge un parametro 0x601
                if (strlen(buff_rx_ser) == SER_LEN_CONFIG)
                {
                    flag = TRUE;                // controlla se la stringa e' tutto OK
                    for (i = 0; i < 8; i++)
                    {
                        if (conv_hex2byte(&buff_rx_ser[(i*3)+5], &buff_rx_can[i]) == FALSE)   // controlla se tutto a posto
                        {
                            flag = FALSE;
                        }
                    }

                    if (flag == TRUE)
                    {
                        _ok_dato_seriale = TRUE;
//                        invia_ser_ok();
                    }
                }
                break;

            }
        }
    }

    if (_ok_dato_seriale == TRUE)
    {
        _ok_dato_seriale = FALSE;
        switch (tipo)
        {
        case MSG_SER_INIT:
            CAN_config_Analyze(1, TRUE);
            break;
        case MSG_SER_CONFIG_WRITE:
            CAN_config_Analyze(1, TRUE);
            break;
        case MSG_SER_CONFIG_REQ:
            CAN_config_Analyze(1, FALSE);
            break;
        }
    }

}




//***************************************
//  converte da HEX 2 ASCII a BYTE
// torna con FALSE se qualcosa non va
//***************************************
UINT8 conv_hex2byte(UINT8 *pt, UINT8 *dest)
{
    UINT8 temp, dato;

    temp = 0;
    dato = *pt;

    if ((dato >= 'a') && (dato <= 'a'))     // trasforma in UPPERCASE
        dato -= 32;

    if ((dato >= 'A') && (dato <= 'F'))         // se lettera
    {
        dato -= 55;
    }
    else if ((dato < '0') || (dato > '9'))      // se fuori dal range ritorna
    {
        return FALSE;
    }
    else
    {
        dato -= 48;     // cifra, toglie 0x30
    }
    temp = dato * 16;

    pt++;
    dato = *pt;
    if ((dato >= 'a') && (dato <= 'a'))     // trasforma in UPPERCASE
        dato -= 32;

    if ((dato >= 'A') && (dato <= 'F'))         // se lettera
    {
        dato -= 55;
    }
    else if ((dato < '0') || (dato > '9'))      // se fuori dal range ritorna
    {
        return FALSE;
    }
    else
    {
        dato -= 48;     // cifra, toglie 0x30
    }
    temp += dato;
    *dest = temp;

    return TRUE;
}






/***************************************
**  Ricezione messaggio da pc               FALSE = no msg;  TRUE = messaggio ricevuto
***************************************/
UINT8 rx_msg(void)
{
    unsigned char count;

    count = 0;
    while (1)
    {
        if (getchar() == FALSE)
            return FALSE;

        if ((timer_rx_ser == 0) || (charrx == STX_IN))     // se arriva '<' o timeout reinizia
            stato_rx_ser = ST_WAIT_STX;

        switch (stato_rx_ser)
        {
        case ST_WAIT_STX:
            punt_rx_ser = 0;
            if (count == 10)        // esce ogni tot caratteri per evitare il loop
                return FALSE;
            if (charrx == STX_IN)        // controlla se messaggio
            {
                buff_rx_ser[punt_rx_ser++] = charrx;
                stato_rx_ser = ST_WAIT_STRING;
            }
            else
            {
                count++;
            }
            break;

        case ST_WAIT_STRING:
            if (charrx == ETX)
            {
                stato_rx_ser = ST_WAIT_STX;
                buff_rx_ser[punt_rx_ser++] = 0;     // terminatore
                return TRUE;        // qui messaggio arrivato OK
            }
            else
            {
                if (punt_rx_ser < DIM_BUFF_SER)     // salva solo se c'e' spazio
                {
                    buff_rx_ser[punt_rx_ser++] = charrx;
                }
            }
            break;
        }

    }

    return TRUE;

}



// **********************************************************************
// * Descrizione:   Ricevo carattere da seriale                         *
// *                                                                    *
// * Returns:       Byte ricevuto o -1 se timeout                       *
// *                                                                    *
// * Parameters:    Timeout                                             *
// **********************************************************************

UINT8 getchar(void)
{
    if (PtSerIC != PtSerFC)
    {
        charrx = FifoSerC[PtSerFC++];
        PtSerFC &= (DIM_FIFO_SER - 1);
        return TRUE;
    }

    return FALSE;
}



//***********************************************************
//  accoda il byte seriale ricevuto nella fifo
//  da chiamare in loop veloce
//***********************************************************
void ricevi_byte_seriale(void)
{
    UINT8 flag, dato;

    if (num_seriale_config == -1)
        return;

    flag = FALSE;

//TODO: rx byte rs232
/*
    switch (num_seriale_config)
    {
    case 0:
        if (ir_s0ric)
        {
            ir_s0ric = 0;
            dato = u0rb;
            flag = TRUE;
        }
        break;

    case 1:
        if (ir_s1ric)
        {
            ir_s1ric = 0;
            dato = u1rb;
            flag = TRUE;
        }
        break;
    }
*/
    if (flag == TRUE)
    {
        FifoSerC[PtSerIC] = dato;   // legge carattere dalla seriale
        PtSerIC++;
        PtSerIC &= (DIM_FIFO_SER - 1);        // n caratteri massimi nel buffer circolare
        timer_rx_ser = TIME_INTERCHAR_RX;
    }

}






//**************************
// invia OK
//**************************
void invia_ser_ok(void)
{
    strcpy(buff_tx_ser, msg_ser_tx_ok);
    invia_tx_ser();
}





//*********************************
//  invia il messaggio nel buffer
//*********************************
void invia_tx_ser(void)
{
    strcat(buff_tx_ser, msg_ser_tx_0d0a);
    punt_tx_ser = 0;
    _ok_tx_ser = TRUE;
}





//***************************
//  trasmette un carattere
//***************************
void tx_ser_char(void)
{
    UINT8 dato;

    if (_ok_tx_ser == TRUE)
    {
        dato = buff_tx_ser[punt_tx_ser++];
        if (dato)
        {
//TODO: tx seriale
//            switch (num_seriale_config)
//            {
//            case 0:
//                u0tb = dato;
//                break;
//            case 1:
//                u1tb = dato;
//                break;
//            }
        }
        else
        {
            _ok_tx_ser = FALSE;
        }
    }
}




//***************************
//  trasmette un carattere
//***************************
void tx_ser_metro(void)
{
    UINT8 dato;

    if (_ok_tx_metro == TRUE)
    {
        dato = buff_tx_metro[punt_tx_metro++];

        //TODO: tx byte metro
/*
        switch (num_seriale_metro)
        {
        case 0:
            u0tb = dato;
            break;
        case 1:
            u1tb = dato;
            break;
        }
*/
        if (punt_tx_metro == 2)
        {
            _ok_tx_metro = FALSE;
        }

    }
}


