//--------------------------------------------------------------
// PRIMA STESURA
// Riva ing. Franco M.
// Besana Brianza (MI)
//
// REVISIONI SUCCESSIVE
// Giovanni Corvini
// Melegnano (MI)
//--------------------------------------------------------------
//
// PROJECT:	  RSR008 - Low Cost Collimator
// FILE:      sys_test.h
// REVISIONS: 0.00 - 22/09/2008 - First Definition
//
//--------------------------------------------------------------
#define BAUD_1200 416
#define BAUD_2400 207
#define BAUD_4800 103
#define BAUD_9600  51
#define BAUD_19200 25
#define BAUD_38400 17

#define TIME_INTERCHAR_RX_M      50     // in ms
#define TIME_INTERCHAR_RX       100     // in ms
#define STX_IN                  '>'
#define STX_OUT                 '<'
#define ETX                     0x0D

enum {
    ST_WAIT_STX = 0,
    ST_LEN,
    ST_WAIT_STRING
};



typedef struct
{
    const UINT8 *const pt_msg;
    UINT8 num_msg;
} STR_MSG_SER;


#define SER_LEN_CONFIG      28      // numero di bytes della stringa

#ifdef _SYS_SERIAL_
#define seriale_ext
const UINT8 msg_ser_tx_ok[] = "<OK";
const UINT8 msg_ser_tx_read[] = "<602:";

const UINT8 msg_ser_init[] = "INIT:";
const UINT8 msg_ser_config_write[] = "600:";
const UINT8 msg_ser_config_req[] = "601:";
const UINT8 msg_ser_tx_0d0a[] = { 0x0D, 0x0A, 0x00 };

enum {
    MSG_SER_INIT = 0,
    MSG_SER_CONFIG_WRITE,
    MSG_SER_CONFIG_REQ,
    MSG_SER_FINE
};

const STR_MSG_SER pt_messaggi_ser[] = {
    { &msg_ser_init[0],         MSG_SER_INIT          },
    { &msg_ser_config_write[0], MSG_SER_CONFIG_WRITE  },
    { &msg_ser_config_req[0],   MSG_SER_CONFIG_REQ    },
};

#else
#define seriale_ext extern
extern const UINT8 msg_ser_tx_read[];
#endif


#define DIM_FIFO_SER    32      // deve essere una potenza di 2
#define DIM_BUFF_SER    40
seriale_ext SINT8 num_seriale_config;
seriale_ext SINT8 num_seriale_metro;
seriale_ext UINT8 FifoSerC[DIM_FIFO_SER];
seriale_ext UINT8 PtSerIC, PtSerFC;
seriale_ext volatile UINT16 timer_rx_ser;
seriale_ext UINT8 stato_rx_ser;
seriale_ext UINT8 buff_rx_ser[DIM_BUFF_SER], buff_tx_ser[DIM_BUFF_SER];
seriale_ext UINT8 charrx, punt_rx_ser, punt_tx_ser;
seriale_ext UINT8 buff_rx_can[8];
seriale_ext UINT16 val_c[3];
seriale_ext UINT16 mis_c[3];

seriale_ext UINT8 buff_rx_metro[DIM_BUFF_SER], buff_tx_metro[DIM_BUFF_SER];
seriale_ext UINT8 punt_rx_metro, punt_tx_metro;
seriale_ext volatile UINT8 timer_metro, timer_rx_metro;
seriale_ext UINT16 misura_metro;

seriale_ext union
    {
        UINT8 all;
        struct bitt8 _bit;
    } volatile flag_ser;
#define _comm                   flag_ser._bit.bit0
#define _ok_tx_ser              flag_ser._bit.bit1
#define _ok_dato_seriale        flag_ser._bit.bit2
#define _ok_tx_metro            flag_ser._bit.bit3


seriale_ext UINT8 FifoSerM[DIM_FIFO_SER];
seriale_ext UINT8 PtSerIM, PtSerFM;


void serial_init_metro(void);
void serial_init_config(void);
UINT8 getchar(void);
void ricevi_byte_seriale(void);
void invia_tx_ser(void);
void tx_ser_char(void);
UINT8 rx_msg(void);

void invia_ser_ok(void);
UINT8 conv_hex2byte(UINT8 *pt, UINT8 *dest);

void tx_ser_metro(void);
void check_rx_messaggio(void);

