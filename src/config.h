

#ifdef CONFIG_H
#define config_ext

const uword BuckyMatrix[MAX_CASSETTE*8]={
    0,  13,  18,  20,  24,  30,  35,  40,  43,   50,    //Cross Cm
    0,   3,   5,   6,   9,  11,  13,  15,  18,   20,    //Cross Inches
    0, 260, 360, 400, 480, 600, 700, 800, 860, 1023,    //Cross ADC Cm
    0, 150, 254, 305, 457, 559, 660, 762, 914, 1023,    //Cross ADC Inches
    0,  13,  18,  20,  24,  30,  35,  40,  43,   50,    //Long  Cm
    0,   3,   5,   6,   9,  11,  13,  15,  18,   20,    //Long  Inches
    0, 260, 360, 400, 480, 600, 700, 800, 860, 1023,    //Long  ADC Cm
    0, 150, 254, 305, 457, 559, 660, 762, 914, 1023     //Long  ADC Inches
};


#else
#define config_ext extern
extern const uword BuckyMatrix[MAX_CASSETTE*8];
#endif


//*******************
//**  DEFINIZIONI  **
//*******************

#define DEF_TIME_TARATURA   240 // in secondi
//#define DEF_TIME_TARATURA   20 // in secondi
#define RISP_IDLE       0xFF
#define RISP_OK         0x01
#define RISP_FALSE      0x00


#define COM_TAR_NONE        0
#define COM_TAR_POT_MIN     1
#define COM_TAR_POS_MIN     2
#define COM_TAR_POS_MED     3
#define COM_TAR_POT_MAX     4
#define COM_TAR_POS_MAX     5
#define COM_TEST_POS_ABS    6
#define       COM_POS_0         0       // in decimi di grado
#define       COM_POS_1       300       // in decimi di grado
#define       COM_POS_2      -300       // in decimi di grado
#define COM_TEST_IRIS_RES   7
#define COM_TEST_IRIS_POS   8
#define COM_TAR_IN_TARATURA     10
#define COM_TAR_SET_POS_NOW     11
#define COM_TAR_SET_POS_0       12
#define COM_TAR_SET_PASSIMAX    13
#define COM_TAR_OUT_TARATURA    14
#define COM_TAR_SET_CALIB       15
#define COM_TAR_ENABLE_BUTTON   16
#define COM_TAR_IRIS_RES_0     17
#define COM_TAR_IRIS_RES_FF    18
#define COM_TAR_IRIS_STOP      19


#define CMD_HOME		0x00
#define CMD_MOVE_ABS	0x01
#define CMD_MOVE_REL	0x02
#define CMD_SET_POS		0xFE
#define CMD_STOP		0xFF

#define CMD_RESET		0x0001
#define CMD_RELEASE		0x0002
#define CMD_INV_RESET	0x0003

#define CMD_ABORT		0xFF

#define MAX_ADD			0x7F0


//  REMOTE CANBUS
#define CAN_CONFR_FIRMWARE       0x00       // 0x700 + scostamento
#define CAN_CONFR_SERIAL         0x03
#define CAN_CONFR_DIAG           0x07

#define CAN_CONFR_COLLIMATOR     0x06       // 0x7A6 remote in WORK


#define NOK	0
#define OK	1

#define CONFIG_RS232     0
#define CONFIG_SERIAL_NO	1
#define CONFIG_LANGUAGE		2
		#define LINGUA_ITA		1
		#define LINGUA_ENG		2
#define CONFIG_UNITS		3
		#define CM_UNITS		1
		#define INCHES_UNITS	2
#define CONFIG_SHOW_DFF		4
        #define SHOW_DFF_MIN    0
        #define SHOW_DFF_NO		0
		#define SHOW_DFF_YES	1
        #define SHOW_DFF_MAX    1
#define CONFIG_FILTER		5
		#define FILTER_NO		0
		#define FILTER_YES		1
#define CONFIG_FILTER_TYPE	6
		#define FILTER_1MM		1
		#define FILTER_2MM		2
		#define FILTER_CU		3
		#define FILTER_CUSTOM		4
                #define FILTER_NUM_MAX          5
#define CONFIG_AUTOLIGHT	7
		#define AUTOLIGHT_NO	0
		#define AUTOLIGHT_YES	1
#define CONFIG_INCLIN		8
		#define INCLIN_NO		0
		#define INCLIN_YES		1
#define CONFIG_AUTOLIM		9
		#define AUTOLIM_NO		0
		#define AUTOLIM_YES		1
#define CONFIG_DFF_LAT_SX	0x0A
		#define DFF_LAT_CAN		1
		#define DFF_LAT_DISC	2
		#define DFF_LAT_POT 	3
#define CONFIG_REC_LAT_SX	0x0B
		#define REC_LAT_SX_NO		0
		#define REC_LAT_SX_CAN		1
		#define REC_LAT_SX_BUCKY	2
#define CONFIG_REC_LAT_DX	0x0C
		#define REC_LAT_DX_NO		0
		#define REC_LAT_DX_CAN		1
		#define REC_LAT_DX_BUCKY	2
#define CONFIG_DFF_VERT		0x0D
		#define DFF_VERT_CAN	1
		#define DFF_VERT_FIX 	2
		#define DFF_VERT_SINGLE 3
		#define DFF_VERT_DIFF 	4
#define CONFIG_REC_VERT		0x0E
		#define REC_VERT_NO		0
		#define REC_VERT_CAN 	1
		#define REC_VERT_ATS 	2
		#define REC_VERT_BUCKY 	3
		#define REC_VERT_FISSI	4
#define CONFIG_DFF_FIXED	0x0F
#define CONFIG_DFF_MAX		0x10
#define CONFIG_DFF_MIN		0x11
#define CONFIG_DFT_VERT			0x12
#define CONFIG_INCL_AL		0x13
#define CONFIG_FILTER_RTZ	0x14
		#define FILTER_RTZ_NO	0
		#define FILTER_RTZ_YES	1
#define CONFIG_SHOW_LOCK	0x15
		#define LOCK_SHOW_NO	0
		#define LOCK_SHOW_YES	1
#define CONFIG_SHOW_ANGLE	0x16
		#define ANGLE_SHOW_NO	0
		#define ANGLE_SHOW_YES	1
#define CONFIG_SQUARE_RTZ	0x17
		#define SQUARE_RTZ_NO	0
		#define SQUARE_RTZ_YES	1
#define CONFIG_CAL_INCL		0x18
#define CONFIG_MAN_DELTA_CROSS	0x19
#define CONFIG_MAN_DELTA_LONG	0x1A
#define CONFIG_CROSS_DFF	0x1B
#define CONFIG_LONG_DFF		0x1C
#define CONFIG_DISPLAY_MODE	0x1D
		#define DISPLAY_NONE		0
		#define DISPLAY_STANDARD	1
		#define DISPLAY_SEDECAL		2
		#define DISPLAY_FREE		3
#define	CONFIG_CROSS_MAX	0x1E
#define	CONFIG_LONG_MAX		0x1F
#define	CONFIG_ENC_SENS		0x20
#define	CONFIG_ENC_INV		0x21
#define	CONFIG_SQUARE_INV	0x22
#define	CONFIG_CAN_PROTO	0x23
		#define CAN_STANDARD	1
		#define CAN_SEDECAL		2
		#define CAN_GMM			3
#define CONFIG_CAN_SPEED	0x24
		#define CAN_1000K		1
		#define CAN_500K		2
		#define CAN_250K		3
		#define CAN_125K		4
		#define CAN_100K		5
		#define CAN_50K			6
		#define CAN_20K			7
		#define CAN_10K			8
#define	CONFIG_FILTER_STEPS	0x28
		#define FILTER_STEPS_MIN	1
		#define FILTER_STEPS_MAX	12000
#define	CONFIG_IN_ANALOG	0x29
		#define DIGITAL_IN		1
		#define ANALOG_IN		2
#define	CONFIG_IN_ADD_CMD	0x2A
#define CONFIG_SPEED_FAST   0x2B
#define CONFIG_CROSS_REF    0x2C
#define CONFIG_LONG_REF     0x2D
#define CONFIG_MSG_ROT      0x2E
#ifdef SYS_CANOPEN
		#define ADDRESS_ROT 0x7A0
#else
		#define ADDRESS_ROT 0x740
#endif
#define CONFIG_FILTER_HOLES 0x2F
        #define FILTER_1_HOLE       1
        #define FILTER_5_HOLES      2   // default
#define CONFIG_HARDWARE     0x30
        #define HARDWARE_NO_POT     0
        #define HARDWARE_YES_POT    1   // default
#define	CONFIG_POT_OPEN  	0x31
#define	CONFIG_POT_CLOSE    0x32
#define CONFIG_0x100        0x33
        #define ALARM_0x100_NO      0
        #define ALARM_0x100_YES     1   // default
#define CONFIG_ROTAZIONE    0x34
        #define ROTAZIONE_NO        0
        #define ROTAZIONE_YES       1
#define CONFIG_IRIDE        0x35
        #define IRIDE_NO            0
        #define IRIDE_YES           1
#define CONFIG_MSG_IRIDE    0x36
		#define ADDRESS_IRIS 0x750
#define CONFIG_TIPO_PROX    0x37
        #define TIPO_PROX_PNP   0       // collegato su J12
        #define TIPO_PROX_NPN   1       // default - collegato su J3 pin 4
#define CONFIG_FIXED_FORMAT 0x38
        #define COM_FIXED_NUM       1       // numero di formati fissi
        #define COM_FIXED_CM        2       // dimensioni formato in cm
        #define COM_FIXED_INCH      3       // dimensioni formato in pollici
        #define COM_FIXED_NUM_IRIS  4       // numero di formati fissi Iride
        #define COM_FIXED_CM_IRIS   5       // dimensioni formato in cm Iride
        #define COM_FIXED_INCH_IRIS 6       // dimensioni formato in pollici Iride
#define CONFIG_FILTER_SEQ   0x39
#define	CONFIG_LCD_FREE     0x3A
		#define LCD_PROTO   	0       // segue l'impostazione del protocollo
		#define LCD_FREE   		1
#define	CONFIG_LCD_NAME     0x3B
#define CONFIG_SPEED_MAN    0x3C
#define CONFIG_DIS_IRIDE    0x3D
#define CONFIG_OFFSET_LAM   0x3E
#define CONFIG_METRO_US     0x3F
#define METRO_US_OFF            0
#define METRO_US_ON             1
#define CONFIG_OFFSET_METRO 0x40
#define CONFIG_MOTORE_FILTRO 0x41
#define MOTORE_FILTRO_OLD   1
#define MOTORE_FILTRO_NEW   2
#define MOTORE_FILTRO_FAST  3
#define CONFIG_TIPO_CORR    0x42
#define TIPO_CORR_CM        1
#define TIPO_CORR_PERC      2
#define CONFIG_MANOPOLE     0x43
#define CONFIG_TIPO_SPOST_MAN 0x44
#define TIPO_MAN_SPOST_OLD       1
#define TIPO_MAN_SPOST_MM        2
#define CONFIG_SPOST_MM     0x45
#define SPOST_MM_DEFAULT    100
#define SPOST_MM_MIN        10
#define SPOST_MM_MAX        500
#define CONFIG_POT_SINGLE   0x46
#define POT_SINGLE_STAND  1
#define POT_SINGLE_TABLE  2
#define CONFIG_DFF_LAT_DX	0x47
#define CONFIG_DFT_SX    	0x48
#define CONFIG_DFT_DX    	0x49
#define CONFIG_FORMATI_MIN  0x4A
#define CONFIG_MANUAL_COLL  0x4B    // collimazione in manuale enable/disable
#define MANUAL_COLL_DIS  0
#define MANUAL_COLL_EN   1
#define CONFIG_VISUA_APE_IRIS 0x4C    // visualizza apertura iride o lineette
#define CONFIG_MSG_7F5      0x4D    // tempo di invio msg 0x7F5
#define CONFIG_NOME_FILTRO_A      0x4E    // configurazione nomi filtro
#define CONFIG_NOME_FILTRO_B      0x4F    // configurazione nomi filtro
#define CONFIG_CORR_IRIDE         0x50
#define CONFIG_FILTRAGGIO_INCL    0x51    // filtraggio inclinometro enable/disable
#define CONFIG_ADDR_OTHER_COM     0x52    // ID per comandi 0x..1 a 0x..2
#define CONFIG_RISPOSTE_7F8       0x53    // enable/disable Riposte su 0x7F8
#define CONFIG_TASTIERA_VILLA     0x54    // enable/disable Enmulazione Tastiera Villa
#define CONFIG_ASR003             0x55    // enable/disable ricezione messaggi da ASR003
#define CONFIG_DISPLAY_ANGOLO_TUBO 0x56   // enable/disable visualizzazione angolo tubo
#define CONFIG_JITTER_KNOBS       0x57    // valore per JITTER manopole - se 0: disabilita controllo Jitter
#define CONFIG_ENTER_CONFIG       0x58    // numero di secondi per entrare in configurazione
#define CONFIG_INVIO_XY_INCL      0x59    // tempo di invio msg ASSI XY INCLINOMETRO
#define CONFIG_OFFSET_ZERO        0x5A    // enable/disable arrotondamenti con OFFSET_CROSS e OFFSET_LONG


#define CONFIG_SAVE			0x64
#define CONFIG_DEFAULT      0xFD
#define CONFIG_RESET_ALL    0xFE
#define CONFIG_ENABLE_VARI  0xFF




//*****************
//**  VARIABILI  **
//*****************

config_ext UINT8 enable_variazione;
config_ext volatile UINT8 timeout_taratura;
config_ext volatile UINT8 tempo_gio;
config_ext UINT8 num_formato_iride;
config_ext SINT32 tar_cross, tar_long;
config_ext SINT32 tar_cross_temp, tar_long_temp;
config_ext volatile UINT8 timer_led_config;
config_ext UINT8 stato_scelta;
config_ext SINT16 conferma;


//*****************************
//**  PROTOTIPI DI FUNZIONE  **
//*****************************
void Fill_rxTemp(CAN_STD_DATA_DEF *ptr);
void sys_can_config(void);
void fill_msg_stato(UINT8 stepper);
void copy_tx_temp(UINT8 remote);
void manda_msg_stato(void);
void sys_can_ROT_STATUS(void);
void gestione_tastiera(void);
void CAN_config_Analyze(UINT8 source, UINT8 write);

UINT8 check_stepper_interno(UINT16 id);
void config_movimento(UINT8 num_mot, UINT8 comando, SINT32 passi, UINT8 tipomov);
void config_mode(UINT8 num_mot, UINT8 mask);
void config_speed(UINT8 num_mot, UINT8 mask);
void send_mot_value(UINT8 num_mot);

void sys_can_send_taratura(UINT8 com);
void CAN_config_firmware(UINT8 mode);

SINT16 EncoderCalib(SINT16 *value, UINT16 Mult);
SINT16 CheckChangeCrossEnc(void);
void dec2bin(UINT8 val, UINT8 *buff);

