//--------------------------------------------------------------
// PRIMA STESURA
// Riva ing. Franco M.
// Besana Brianza (MI)
//
// REVISIONI SUCCESSIVE
// Giovanni Corvini
// Melegnano (MI)
//--------------------------------------------------------------
//
// PROJECT:	  RSR008 - Low Cost Collimator
// FILE:      sys_control.h
// REVISIONS: 0.00 - 22/09/2008 - First Definition
//
//--------------------------------------------------------------

// allarmi su allarmi_totale
#define ALARM_CHECK     0x78        // mask per generazione allarme - senza filtro, gestito a parte
#define ALARM_FILTER    0x80
#define ALARM_CROSS     0x40
#define ALARM_LONG      0x20
#define ALARM_IRIS      0x10
#define ALARM_RESET     0x08

#define ALARM_ON        0x10        // allarmi potenziometri
#define ALARM_OFF       0xEF

//#define ALARM_DFF_TOTALE
// allarmi su alarm_dff
#define ALARM_DFF_CROSS      0x01        // allarme DFF sotto a DFF_MIN
#define ALARM_FORMAT_CROSS   0x02        // allarme formato non raggiungibile
#define ALARM_DFF_LONG       0x10        // allarme DFF sotto a DFF_MIN
#define ALARM_DFF_IRIS       0x20        // allarme DFF sotto a DFF_MIN


#define REV_DELAY		  		50	//   50		msec
#define HIGH_POWER_DELAY		0	//   0  	msec	NO WAIT
#define LOW_POWER_DELAY			1000//   1   	sec
#define HOME_MIN_TIME			1	//   1    	sec
#define HOME_DEFAULT_TIME		15	//   15    	sec
#define HOME_FILTER_DEFAULT_TIME    4	//   in secondi
#define HOME_MAX_TIME			240	//   4 	  	min
#define STEPPER_MAX_THR			250	//		  	steps
#define AFTER_RESET_DELAY		1000// 	 1    	sec
#define THR_DEFAULT_STEPS		10	//	10		steps
#define DELAY_HOME_POS			25	//	25		msec
#define	STARTUP_CONTROL_DELAY	500	//	500     msec
#define NOTORQUE_DEFAULT_TIME 	255	//  infinite time
#define NOTORQUE_FILT_DEF_TIME	255	//  infinite time






enum {
    MASTER_FIRST = 0,
    MASTER_RESET,
    MASTER_RESET_1,
    MASTER_WAIT_RESET,
    MASTER_WAIT_MEMO,
    MASTER_WAIT_MOVIMENTO,
    MASTER_CHECK_VALORI,
    MASTER_WAIT_LITTLE_BIT,
    MASTER_WAIT_OPEN_ALL_END,
    MASTER_MOVE,
    MASTER_IDLE,
    MASTER_ALL_PREP_STOP,
    MASTER_ALL_STOPPED,
    MASTER_ERR,
    MASTER_PREP_READY,
    MASTER_READY,
};


enum {
    ST_MOV_RESET = 0,

    ST_MOV_CHECK_CHIAVE,
    ST_MOV_CHECK_MANUALE,
    ST_MOV_CHECK_FORMATO,
    ST_MOV_WAIT_MANUALE,
    ST_MOV_WAIT_STOP,
    ST_MOV_CHECK_ENCODER,
    ST_MOV_ENCODER_IN_MOVIMENTO,
};



/////////////////////////////
//Global Variables
/////////////////////////////
#ifdef _SYS_CONTROL_
#define control_ext
    volatile UINT8 timer_res_a, timer_res_b, timer_res_d;
	bool g_bReqHomeStepperA=false;
	bool g_bMoveAbsStepperA=false;
	bool g_bMoveRelStepperA=false;
	bool g_bSetPosStepperA=false;
	bool g_bStopStepperA=false;
	bool g_bReqReleaseStepperA=false;
	bool g_bInvHomeStepperA=false;
	bool g_bReqHomePendingStepperA=false;

    bool g_bReqHomeStepperB=false;
	bool g_bMoveAbsStepperB=false;
	bool g_bMoveRelStepperB=false;
	bool g_bSetPosStepperB=false;
	bool g_bStopStepperB=false;
	bool g_bReqReleaseStepperB=false;
	bool g_bInvHomeStepperB=false;
	bool g_bReqHomePendingStepperB=false;

	bool g_bReqHomeIrqC=false;
	volatile bool g_bReqHomeStepperC=false;
	bool g_bMoveAbsStepperC=false;
	bool g_bMoveRelStepperC=false;
	bool g_bSetPosStepperC=false;
	bool g_bStopStepperC=false;
	bool g_bReqReleaseStepperC=false;
	bool g_bInvHomeStepperC=false;
	bool g_bReqHomePendingStepperC=false;

	bool g_bReqHomeStepperD;
    bool g_bMoveAbsStepperD;
    bool g_bMoveRelStepperD;
    bool g_bSetPosStepperD;
    bool g_bStopStepperD;
    bool g_bReqReleaseStepperD;
    bool g_bInvHomeStepperD;

    uchar g_ucSetWindow=0;
	uword g_uwErrorMessages=0;
	bool g_bSerialUpdated=true;
    bool ReqHomeIris = FALSE;

#else
#define control_ext extern

    extern volatile UINT8 timer_res_a, timer_res_b, timer_res_d;
	extern bool g_bReqHomeStepperA;
	extern bool g_bMoveAbsStepperA;
	extern bool g_bMoveRelStepperA;
	extern bool g_bSetPosStepperA;
	extern bool g_bStopStepperA;
	extern bool g_bReqReleaseStepperA;
	extern bool g_bInvHomeStepperA;
	extern bool g_bReqHomePendingStepperA;

	extern bool g_bReqHomeStepperB;
	extern bool g_bMoveAbsStepperB;
	extern bool g_bMoveRelStepperB;
	extern bool g_bSetPosStepperB;
	extern bool g_bStopStepperB;
	extern bool g_bReqReleaseStepperB;
	extern bool g_bInvHomeStepperB;
	extern bool g_bReqHomePendingStepperB;

	extern bool g_bReqHomeIrqC;
	extern volatile bool g_bReqHomeStepperC;
	extern bool g_bMoveAbsStepperC;
	extern bool g_bMoveRelStepperC;
	extern bool g_bSetPosStepperC;
	extern bool g_bStopStepperC;
	extern bool g_bReqReleaseStepperC;
	extern bool g_bInvHomeStepperC;
	extern bool g_bReqHomePendingStepperC;

	extern bool g_bReqHomeStepperD;
	extern bool g_bMoveAbsStepperD;
	extern bool g_bMoveRelStepperD;
	extern bool g_bSetPosStepperD;
	extern bool g_bStopStepperD;
	extern bool g_bReqReleaseStepperD;
	extern bool g_bInvHomeStepperD;

	extern uchar g_ucSetWindow;
	extern bool g_bSerialUpdated;

    extern bool ReqHomeIris;
#endif


control_ext UINT8 stato_master;
control_ext UINT8 indice_taratura;
control_ext UINT8 in_calibrazione;
#define STATO_LAMELLE_RESET     0x01
#define STATO_IRIDE_RESET       0x02
#define STATO_FILTRO_RESET      0x04
control_ext UINT8 CollInReset;
control_ext UINT16 CurrentDFF;

//********************************
//  PROTOTIPI DI FUNZIONE
//********************************

void sys_control_init(UINT8 flag);

void sys_control_master(UINT8 lamelle /* = FALSE */);
void sys_control_process(void);

void sys_analog_controller(void);


void Carica_DFF_Formato(void);
UINT8 CheckNuovoBucky(UINT8 incl, UINT8 formato);

void gestione_cross_long(UINT8 num_mot);
UINT8 check_com_coll(void);


UINT32 CheckMaxShutter(UINT8 slave, UINT16 uwActualDff, UINT8 flag);
UINT8 calc_size2steps(UINT8 slave, UINT32 dff, SINT32 formato, UINT32 *steps, UINT8 no_err);
SINT32 calc_steps2size(UINT16 dff, UINT8 slave, SINT32 steps);

void sys_control_set_stepper(uchar ucStepper, bool bOn, bool bEnd);

UINT32 calcola_speed_manuale(UINT8 slave);
void load_frequenze(void);

void genera_evento(UINT8 evento, UINT8 motore, UINT8 Par1, UINT8 Par2, UINT32 dato);
void controlla_generazione_eventi(void);

