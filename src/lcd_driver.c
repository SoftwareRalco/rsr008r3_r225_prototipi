#define _SYS_LCD_

//--------------------------------------------------------------
// PRIMA STESURA
// Riva ing. Franco M.
// Besana Brianza (MI)
//
// REVISIONI SUCCESSIVE
// Giovanni Corvini
// Melegnano (MI)
//--------------------------------------------------------------
//
// PROJECT:	  RSR008 - Low Cost Collimator
// FILE:      sys_lcd_driver.c
// REVISIONS: 0.00 - 22/09/2008 - First Definition
//
//--------------------------------------------------------------

//--------------------------------------------------------------
// INCLUDE FILES
//--------------------------------------------------------------
#include "include.h"
/*
#include "sfr32C87.h"
#include "sys_defines.h"

#include "sys_lcd_driver.h"
#include "virtEE_Variable.h"    // Virtual EEPROM (variable size records) function definitions
#include "sys_control.h"
#include "sys_can_main.h"
#include "sys_main.h"
*/
#include "string.h"
#include "main_thread.h"

const unsigned char symbol1 [8] = {0x08,0x14,0x14,0x14,0x1c,0x1d,0x1e,0x1d};	//Lock
const unsigned char symbol2 [8] = {0x02,0x05,0x05,0x05,0x1c,0x1d,0x1e,0x1d};	//Unlock
const unsigned char symbol3 [8] = {0x08,0x14,0x08,0x00,0x00,0x00,0x00,0x00};	//Degrees
const unsigned char symboln [8] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};	//Clear


//--------------------------------------------------------------
// GLOBAL VARIABLES
//--------------------------------------------------------------

//--------------------------------------------------------------
// INTERRUPT FUNCTIONS
//--------------------------------------------------------------

//--------------------------------------------------------------
// LOCAL FUNCTIONS
//--------------------------------------------------------------


//--------------------------------------------------------------
// SYSTEM FUNCTIONS
//--------------------------------------------------------------
void sys_lcd_out(UINT8 c)
{
//	char a;

    buffer_lcd[ptLcdMain++] = c;
}

//--------------------------------------------------------------
void sys_lcd_mode(UINT8 c)
{
	char a;

    if (c < 0xC0)
        a = c - 0x80;           // indirizzo prima riga
    else
        a = c - 0xC0 + 20;      // indirizzo seconda riga
    ptLcdMain = a;
}

//--------------------------------------------------------------
void sys_print_msg (char r, const char s[]){

	int i;

	i = 0;
	sys_lcd_mode (r);
	while (s[i] != '\0'){
		sys_lcd_out (s[i]);
		i++;
	}
}

//--------------------------------------------------------------
void sys_print_integer (char r, uint16_t n, uchar c, bool b){

	bool bSign=false;

	sys_lcd_mode (r);
	if (c>4){
		if (b || (n / 10000)){
			sys_lcd_out ((n / 10000)+'0');
			bSign=true;
		}
		else {
			if (!bSign)
				sys_lcd_out (' ');
			else
				sys_lcd_out ('0');
		}
	}
	if (c>3){
		if (b || ((n % 10000) / 1000)){
			sys_lcd_out (((n % 10000) / 1000)+'0');
			bSign=true;
		}
		else {
			if (!bSign)
				sys_lcd_out (' ');
			else
				sys_lcd_out ('0');
		}
	}
	if (c>2){
		if (b || ((n % 1000) / 100)){
			sys_lcd_out (((n % 1000) / 100)+'0');
			bSign=true;
		}
		else {
			if (!bSign)
				sys_lcd_out (' ');
			else
				sys_lcd_out ('0');
		}
	}
	if (c>1){
		if (b || ((n % 100) / 10)){
			sys_lcd_out (((n % 100) / 10)+'0');
			bSign=true;
		}
		else {
			if (!bSign)
				sys_lcd_out (' ');
			else
				sys_lcd_out ('0');
		}
	}

	sys_lcd_out ((n % 10)+'0');
}

//--------------------------------------------------------------
void sys_print_byte (char r, uchar n, bool b){

	bool bSign=false;

	sys_lcd_mode (r);

	if (b || (n / 100)){
		sys_lcd_out ((n / 100)+'0');
		bSign=true;
	}
	else {
		if (!bSign)
			sys_lcd_out (' ');
		else
			sys_lcd_out ('0');
	}
	if (b || (n % 100)/10){
		sys_lcd_out ((n % 100)/10+'0');
		bSign=true;
	}
	else {
		if (!bSign)
			sys_lcd_out (' ');
		else
			sys_lcd_out ('0');
	}
	sys_lcd_out ((n % 10)+'0');
}

//--------------------------------------------------------------
void sys_print_number (char r, uint16_t n, char d, char p, bool b){
    uint16_t xx [5] = {1,10,100,1000,10000};
	char i;
	bool bSign=false;

	sys_lcd_mode (r);
	if (!n){
		for (i=0;i<d-1;i++)
			sys_lcd_out (' ');
		sys_lcd_out ('0');
			sys_lcd_out (' ');
		return;
	}

	for (i=0;i<d;i++){
		if (p==(i+1))
			bSign=true;

		if (p == i)
			sys_lcd_out ('.');
		if (b||((n/xx[d-i-1])%10)){
			bSign=true;
			sys_lcd_out ('0'+(n/xx[d-i-1])%10);
		}
		else{
			if (bSign)
				sys_lcd_out ('0');
			else
				sys_lcd_out (' ');
		}
	}

}



//****************************************************
//  out del carattere LCD - chiamata da irq
//****************************************************
void write_char(void)
{
    static UINT8 linea = 0;

    switch (stato_write_char)
    {
    case ST_LCD_L1:             // Set scrittura su indirizzo 0
        lcd_command_4bit(MODO_2_NIBBLE, LCD_HOME_L1);      // setta address prima linea
        punt_buff_lcd = 0;
        linea = 0;
        stato_write_char = ST_LCD_WL1;
        break;

    case ST_LCD_WL1:
    case ST_LCD_WL2:

        if (buffer_lcd[linea + punt_buff_lcd] == 0)
            buffer_lcd[linea + punt_buff_lcd] = ' ';

/*
        if ((_cursor_set == TRUE) && (_blink_cursore == TRUE) && (cursor_pos == (linea + punt_buff_lcd)))
            lcd_data(0xFF);     // visualizza cursore
        else
            lcd_data(buffer_lcd[linea + punt_buff_lcd]);
*/
        lcd_data(buffer_lcd[linea + punt_buff_lcd]);


        if (++punt_buff_lcd >= 20)
        {
            if (linea == 0)
                stato_write_char = ST_LCD_L2;
            else
                stato_write_char = ST_LCD_L1;
        }
        break;

    case ST_LCD_L2:             // Set scrittura su indirizzo 0
        lcd_command_4bit(MODO_2_NIBBLE, LCD_HOME_L2);      // setta address seconda linea
        punt_buff_lcd = 0;
        linea = 20;
        stato_write_char = ST_LCD_WL2;
        break;

    }

}




/*****************************
**  inizializza LCD
*****************************/
void lcd_init(void)
{
    UINT8 i;


    punt_buff_lcd = 0;
    stato_write_char = ST_LCD_L1;


    lcd_command_4bit(MODO_1_NIBBLE, 0x30);      //
    g_uwTestTimer = 8;
    while (g_uwTestTimer);
//    wait_tmr1_100us(50);

    lcd_command_4bit(MODO_1_NIBBLE, 0x30);      //
    g_uwTestTimer = 2;
    while (g_uwTestTimer);
//    wait_tmr1_100us(2);

    lcd_command_4bit(MODO_1_NIBBLE, 0x30);      //
    g_uwTestTimer = 2;
    while (g_uwTestTimer);
//    wait_tmr1_100us(2);

    lcd_command_4bit(MODO_1_NIBBLE, 0x20);      //
    g_uwTestTimer = 2;
    while (g_uwTestTimer);
//    wait_tmr1_100us(2);

    lcd_command_4bit(MODO_2_NIBBLE, 0x28);      //
    g_uwTestTimer = 2;
    while (g_uwTestTimer);
//    wait_tmr1_100us(2);

    lcd_command_4bit(MODO_2_NIBBLE, 0x08);      //
    g_uwTestTimer = 2;
    while (g_uwTestTimer);
//    wait_tmr1_100us(2);

    lcd_command_4bit(MODO_2_NIBBLE, 0x06);      //
    g_uwTestTimer = 2;
    while (g_uwTestTimer);
//    wait_tmr1_100us(2);

    lcd_command_4bit(MODO_2_NIBBLE, 0x0C);      //
    g_uwTestTimer = 2;
    while (g_uwTestTimer);
//    wait_tmr1_100us(2);

    lcd_command_4bit(MODO_2_NIBBLE, 0x80);      // setta la prima linea
    g_uwTestTimer = 2;
    while (g_uwTestTimer);
//    wait_tmr1_100us(2);


    lcd_command_4bit(MODO_2_NIBBLE, 0x48);
//	sys_lcd_mode (0x48);
	for (i=0;i<8;i++)
    {
        g_uwTestTimer = 2;
        while (g_uwTestTimer);
		lcd_data(symbol1[i]);
    }
	for (i=0;i<8;i++)
    {
        g_uwTestTimer = 2;
        while (g_uwTestTimer);
		lcd_data(symbol2[i]);
    }
	for (i=0;i<8;i++)
    {
        g_uwTestTimer = 2;
        while (g_uwTestTimer);
		lcd_data(symbol3[i]);
    }
	for (i=0;i<8;i++)
    {
        g_uwTestTimer = 2;
        while (g_uwTestTimer);
		lcd_data(symboln[i]);
    }
	for (i=0;i<8;i++)
    {
        g_uwTestTimer = 2;
        while (g_uwTestTimer);
		lcd_data(symboln[i]);
    }
	for (i=0;i<8;i++)
    {
        g_uwTestTimer = 2;
        while (g_uwTestTimer);
		lcd_data(symboln[i]);
    }
	for (i=0;i<8;i++)
    {
        g_uwTestTimer = 2;
        while (g_uwTestTimer);
		lcd_data(symboln[i]);
    }
	for (i=0;i<8;i++)
    {
        g_uwTestTimer = 2;
        while (g_uwTestTimer);
		lcd_data(symboln[i]);
    }

	for (i = 0; i < 8; i++)
    {
		g_cbAsr001Version[i] = '*';
		g_cbAsr003Version[i] = '*';
	}

}




/*******************************************************
**  comandi to LCD
********************************************************/
void lcd_command_4bit(unsigned char modo, unsigned char data)
{
    uint16_t temp16;

    temp16 = (data >> 4) & 0x0F;        // nibble H
    g_ioport.p_api->portWrite(IOPORT_PORT_08, temp16, 0x000F);

    g_ioport.p_api->pinWrite(PIN_LCD_RS, IOPORT_LEVEL_LOW);
    g_ioport.p_api->pinWrite(PIN_LCD_RW, IOPORT_LEVEL_LOW);
    __asm("nop");
    __asm("nop");

    g_ioport.p_api->pinWrite(PIN_LCD_EN, IOPORT_LEVEL_HIGH);
    __asm("nop");
    __asm("nop");
    g_ioport.p_api->pinWrite(PIN_LCD_EN, IOPORT_LEVEL_LOW);
    __asm("nop");
    __asm("nop");

    if (modo == MODO_2_NIBBLE)
    {
        temp16 = data & 0x0F;        // nibble L
        g_ioport.p_api->portWrite(IOPORT_PORT_08, temp16, 0x000F);
        g_ioport.p_api->pinWrite(PIN_LCD_RS, IOPORT_LEVEL_LOW);     // accede al CR
        g_ioport.p_api->pinWrite(PIN_LCD_RW, IOPORT_LEVEL_LOW);

        g_ioport.p_api->pinWrite(PIN_LCD_EN, IOPORT_LEVEL_HIGH);
        __asm("nop");
        __asm("nop");
        g_ioport.p_api->pinWrite(PIN_LCD_EN, IOPORT_LEVEL_LOW);
        __asm("nop");
        __asm("nop");
    }

}






/*******************************************************
**  comandi to LCD
********************************************************/
void lcd_data(UINT8 data)
{
    uint16_t temp16;

    temp16 = (data >> 4) & 0x0F;        // nibble H
    g_ioport.p_api->portWrite(IOPORT_PORT_08, temp16, 0x000F);

    g_ioport.p_api->pinWrite(PIN_LCD_RS, IOPORT_LEVEL_HIGH);        // registro DR
    g_ioport.p_api->pinWrite(PIN_LCD_RW, IOPORT_LEVEL_LOW);
    __asm("nop");
    __asm("nop");

    g_ioport.p_api->pinWrite(PIN_LCD_EN, IOPORT_LEVEL_HIGH);
    __asm("nop");
    __asm("nop");
    g_ioport.p_api->pinWrite(PIN_LCD_EN, IOPORT_LEVEL_LOW);
    __asm("nop");
    __asm("nop");

    temp16 = data & 0x0F;        // nibble L
    g_ioport.p_api->portWrite(IOPORT_PORT_08, temp16, 0x000F);
    g_ioport.p_api->pinWrite(PIN_LCD_RS, IOPORT_LEVEL_HIGH);        // registro DR
    g_ioport.p_api->pinWrite(PIN_LCD_RW, IOPORT_LEVEL_LOW);

    g_ioport.p_api->pinWrite(PIN_LCD_EN, IOPORT_LEVEL_HIGH);
    __asm("nop");
    __asm("nop");
    g_ioport.p_api->pinWrite(PIN_LCD_EN, IOPORT_LEVEL_LOW);
    __asm("nop");
    __asm("nop");

}




