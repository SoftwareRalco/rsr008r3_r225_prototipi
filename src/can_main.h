/*
 * can_main.h
 *
 *  Created on: 24 mag 2017
 *      Author: daniele
 */


//--------------------------------------------------------------
// PRIMA STESURA
// Riva ing. Franco M.
// Besana Brianza (MI)
//
// REVISIONI SUCCESSIVE
// Giovanni Corvini
// Melegnano (MI)
//--------------------------------------------------------------
//
// PROJECT:   RSR008 - Low Cost Collimator
// FILE:      sys_can_main.h
// REVISIONS: 0.00 - 22/09/2008 - First Definition
//
//--------------------------------------------------------------

#ifdef _SYS_CAN_MAIN_
#define can_main_ext
    //Message Timers
    bool g_b100Time=false;
    bool g_b7F0Time=false;
    bool g_b7F1Time=false;
    bool g_b7F2Time=false;
    bool g_b7F9Time=false;
    bool g_b7FCTime=false;
	bool g_bUpdateRele=false;
    bool g_b103Time=false;
    bool g_b082Time=false;
    bool g_b083Time=false;
    bool g_b084Time=false;
    bool g_b085Time=false;
    bool g_b101Time=false;
    bool g_b7AETime=false;

    //Versions
    char g_cbAsr001Version[9];
    char g_cbAsr003Version[9];
	char g_cbIrisVersion[8];
	char g_cbRotVersion[8];
	bool g_bAsr001Ok=false;
	bool g_bAsr003Ok=false;
	bool g_bIridek=false;
	bool g_bRotk=false;

    //Cross
    uword g_uwActualCrossToBeSent;

    //Long
    uword g_uwActualLongToBeSent;

    //Iris
    uword g_uwActualIrisToBeSent;
    UINT16 g_uwIrisFormatFromCan;

    //Manual
	bool g_bManualRequestFromCan = FALSE;
	bool g_bManualRequestFromRemote=false;

	uchar g_ucOpenCloseFromCan=0;
    UINT8 s_ucOldOpenCloseCommand = 0;

    //Filter
	uchar g_ucFilterRequired=0;
    uchar g_ucPuntaFiltro;

    //Lamp
    bool g_bRequestLightOn=false;
    bool g_bRequestLightOff=false;
    bool g_bRequestLightMan=false;
    bool g_bLightState = false;
    UINT16 g_CanLightTimer = (30 * BASE_TIMER_LUCE);
    UINT16 KeyLightTimer = (30 * BASE_TIMER_LUCE);

    //Inclinometer
    bool g_bExclIncl=false;
    bool g_bLateralDff=false;

    //ATS
    bool g_bAckReceived=false;

#else
#define can_main_ext extern
    extern bool g_b100Time;
    extern bool g_b7F0Time;
    extern bool g_b7F1Time;
    extern bool g_b7F2Time;
    extern bool g_b7F9Time;
    extern bool g_b7FCTime;
    extern bool g_b103Time;
    extern bool g_b082Time;
    extern bool g_b083Time;
    extern bool g_b084Time;
    extern bool g_b085Time;
    extern bool g_b101Time;
    extern bool g_b7AETime;

	extern bool g_bUpdateRele;
    extern char g_cbAsr001Version[9];
    extern char g_cbAsr003Version[9];
    extern char g_cbIrisVersion[9];
    extern char g_cbRotVersion[9];
	extern bool g_bAsr001Ok;
	extern bool g_bAsr003Ok;
	extern bool g_bIridek;
	extern bool g_bRotk;
    extern uword g_uwActualCrossToBeSent;
    extern uword g_uwActualLongToBeSent;
    extern uword g_uwActualIrisToBeSent;
    extern UINT16 g_uwIrisFormatFromCan;
	extern bool g_bManualRequestFromCan;
	extern bool g_bManualRequestFromRemote;

	extern uchar g_ucOpenCloseFromCan;
    extern UINT8 s_ucOldOpenCloseCommand;

	extern uchar g_ucFilterRequired;
    extern uchar g_ucPuntaFiltro;

    extern bool g_bRequestLightOn;
    extern bool g_bRequestLightOff;
    extern bool g_bRequestLightMan;
    extern bool g_bLightState;
    extern bool g_bExclIncl;
    extern UINT16 g_CanLightTimer;
    extern UINT16 KeyLightTimer;

    extern bool g_bAckReceived;
#endif


//#define     MAX_CAN_SW_DELAY      0x2000
//#define     RESET_CAN0_SW_TMR     can0_sw_tmr=MAX_CAN_SW_DELAY
//#define     DEC_CHK_CAN0_SW_TMR       (--can0_sw_tmr != 0)
/* One bit flag for each while loop in canx.c */
//#define       CAN_SW_RST_ERR          0x01
//#define       CAN_SW_TRM_ERR          0x02
//#define       CAN_SW_TRM_DATA_ERR     0x04
//#define       CAN_SW_CHK_TRM_ERR      0x08
//#define       CAN_SW_SET_REC_ERR      0x10
//#define       CAN_SW_GET_ERR          0x20
//#define       CAN_SW_ABORT_ERR        0x40
//#define       CAN_SW_OTHER_ERR        0x80


//#define NUM_BUFF_RX_O       32           // comandi per Slave
//can_main_ext UINT8 ix0I, ix0F;
//can_main_ext CAN_STD_DATA_DEF rx_dati_0[NUM_BUFF_RX_O];
//
//can_main_ext UINT16   can0_sw_tmr;
//can_main_ext UINT8    can0_sw_err;
can_main_ext UINT16 freq_min, freq_max;

can_main_ext UINT8 val_7F0_old[8], val_7F0_now[8], mask_7F0_invio[8];
can_main_ext UINT8 manda_7d0;
can_main_ext UINT8 val_7d0a, val_7d0b;

can_main_ext UINT8 iride_manual, iride_manual_old;
can_main_ext UINT16 LastIrisFormat;
can_main_ext UINT16 s_uwLastCrossDff, s_uwLastLongDff;

can_main_ext UINT8 timer_LED_GREEN;
can_main_ext UINT16 CampoIride;

can_main_ext UINT8 DebugIncl, InclinazioneCan;

can_main_ext UINT8 stato_7F0_fast;
can_main_ext UINT16 timer_invio_7F0;
can_main_ext UINT8 invia7F0;

can_main_ext UINT8 val_7F5_old[8], val_7F5_now[8];
can_main_ext UINT8 stato_7F5_fast;
can_main_ext UINT16 timer_invio_7F5;
can_main_ext UINT8 invia7F5;

can_main_ext UINT8 icona_RaggiX, icona_RaggiX_hold;
can_main_ext UINT8 icona_sid, icona_sid_hold;
can_main_ext SINT8 angolo_colonna, angolo_colonna_hold;
can_main_ext SINT16 sid_display, sid_manuale;
can_main_ext UINT8 filter_moving_prec, filter_status_moving;

can_main_ext UINT8 tasti_villa;
can_main_ext UINT16 timer_invio_tasti_villa;

can_main_ext UINT16 timer_invio_inclinometro;


//********************************
//  PROTOTIPI DI FUNZIONE
//********************************

void sys_can_work(void);

//process sys_can_main_process(void);
void sys_can_init(UINT8 mode);

int check_rec_success_can0(unsigned short       slot_nr,
                           CAN_STD_DATA_DEF*    in_rec_data_p);
void get_message_can0(unsigned short  slot_nr, CAN_STD_DATA_DEF*  mailbox_data_p);

static void sys_can_process_7(void);

void sys_can_7A0(void);
void sys_can_7A1(void);
void sys_can_7A2(void);
void sys_can_7A3(void);
void sys_can_7A4(void);
void sys_can_7A5(void);
void sys_can_7A7(void);
void sys_can_7A8(void);
void sys_can_7A9(void);
void sys_can_7AA(UINT8 test);
void sys_can_7AB(UINT8 test);
void sys_can_7AC(UINT8 test);
void sys_can_7AD(void);
void sys_can_7AE(void);
void sys_can_7AF(void);

void sys_can_IRIS_STATUS(void);
static void sys_can_DISPLAY_0(void);
static void sys_can_DISPLAY_1(void);

void sys_can_tx_status(void);
UINT8 CheckProximity(void);
void check_tx_100(void);

void invio_tasti_villa(void);
