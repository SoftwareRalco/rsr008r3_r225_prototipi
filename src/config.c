

#define CONFIG_H

#include "string.h"
#include "ctype.h"

#include "include.h"
//#include "Flash_API.h"





//**********************************************
//  riempie buffer con il dato puntato
//**********************************************
void Fill_rxTemp(CAN_STD_DATA_DEF *ptr)
{
    rx_temp.id = ptr->id;
    rx_temp.dlc = ptr->dlc;

    rx_temp.data[0] = ptr->data.data[0];
    rx_temp.data[1] = ptr->data.data[1];
    rx_temp.data[2] = ptr->data.data[2];
    rx_temp.data[3] = ptr->data.data[3];
    rx_temp.data[4] = ptr->data.data[4];
    rx_temp.data[5] = ptr->data.data[5];
    rx_temp.data[6] = ptr->data.data[6];
    rx_temp.data[7] = ptr->data.data[7];
}





//*******************************************
//  mode = FALSE in configurazione
//  mode = TRUE in modalita' lavoro
//*******************************************
void CAN_config_firmware(UINT8 mode)
{
    if (mode == FALSE)
    {
        tx_temp.id = ee_CanAddressConfig[STEPPER_A].uwBoardAddress + CAN_CONFR_FIRMWARE;
    }
    else
    {
        tx_temp.id = ee_CanColl.unIdOtherCommands + CAN_CONFR_COLLIMATOR;
    }
    tx_temp.dlc = 8;
    tx_temp.data[0] = ee_RSR008B_Version.cbVersion[0];
    tx_temp.data[1] = ee_RSR008B_Version.cbVersion[1];
    tx_temp.data[2] = ee_RSR008B_Version.cbVersion[2];
    tx_temp.data[3] = ee_RSR008B_Version.cbVersion[3];
    tx_temp.data[4] = ee_RSR008B_Version.cbVersion[4];
    tx_temp.data[5] = ee_RSR008B_Version.cbVersion[5];
    tx_temp.data[6] = ee_RSR008B_Version.cbVersion[6];
    tx_temp.data[7] = ee_RSR008B_Version.cbVersion[7];
    copy_tx_temp(FALSE);     // transmit
}


void CAN_config_serial(void)
{
    tx_temp.id = ee_CanAddressConfig[STEPPER_A].uwBoardAddress + CAN_CONFR_SERIAL;
    tx_temp.dlc = 8;
    tx_temp.data[0] = ee_RSR008B_Version.cbSerial[0];
    tx_temp.data[1] = ee_RSR008B_Version.cbSerial[1];
    tx_temp.data[2] = ee_RSR008B_Version.cbSerial[2];
    tx_temp.data[3] = ee_RSR008B_Version.cbSerial[3];
    tx_temp.data[4] = ee_RSR008B_Version.cbSerial[4];
    tx_temp.data[5] = ee_RSR008B_Version.cbSerial[5];
    tx_temp.data[6] = ee_RSR008B_Version.cbSerial[6];
    tx_temp.data[7] = ee_RSR008B_Version.cbSerial[7];
    copy_tx_temp(FALSE);     // transmit
}


void CAN_config_diagnostic(void)
{
    tx_temp.id = ee_CanAddressConfig[STEPPER_A].uwBoardAddress + CAN_CONFR_DIAG;
    tx_temp.dlc = 2;
    tx_temp.data[0] = g_InternalFlag.byte;
    tx_temp.data[1] = 0x00;
    tx_temp.data[2] = 0x00;
    tx_temp.data[3] = 0x00;
    tx_temp.data[4] = 0x00;
    tx_temp.data[5] = 0x00;
    tx_temp.data[6] = 0x00;
    tx_temp.data[7] = 0x00;
    copy_tx_temp(FALSE);     // transmit
}





//************************************************
//  comunicazione in calibrazione/configurazione
//************************************************
void sys_can_config(void)
{
    UINT16 Base;
    UINT8 SubCommand;
    UINT32 passi;


    if (ixRI != ixRF)               // ******  REMOTE  ******
    {
        // TODO: da completare irq canbus
//        can00e = 0; // can01 = can0 rx irq
        Fill_rxTemp(&rx_dati_R[ixRF]);
        if (++ixRF >= NUM_BUFF_RX_R)
            ixRF = 0;
//        can00e = 1; // can01 = can0 rx irq

        Base = rx_temp.id & 0xFFF0;
        SubCommand = rx_temp.id & 0x0F;

        if ((rx_temp.id & 0xFFF0) == ee_CanAddressConfig[STEPPER_A].uwBoardAddress) // comandi con base collimatore
        {
            switch (SubCommand)
            {
            case CAN_CONFR_FIRMWARE:
                CAN_config_firmware(FALSE);
                break;
            case CAN_CONFR_SERIAL:
                CAN_config_serial();
                break;
            case CAN_CONFR_DIAG:
                CAN_config_diagnostic();
                break;
            }
        }
    }
    else if (ixCI != ixCF)           // ******  CONFIGURAZIONE  ******
    {
        // TODO: da completare irq canbus
//        can00e = 0; // can01 = can0 rx irq
        Fill_rxTemp(&rx_dati_C[ixCF]);
        if (++ixCF >= NUM_BUFF_RX_C)
            ixCF = 0;
//        can00e = 1; // can01 = can0 rx irq
        if (rx_temp.id == ID_CONFIG_WRITE)
        {
            CAN_config_Analyze(0, TRUE);        // analizza messaggio di configurazione
        }
        else if (rx_temp.id == ID_CONFIG_REQ)
        {
            CAN_config_Analyze(0, FALSE);        // analizza messaggio di configurazione
        }

    }
    else if (ix0I != ix0F)           // controlla se messaggio in coda
    {                               // copia in buffer temporaneo
        // TODO: da completare irq canbus
//        can00e = 0; // can01 = can0 rx irq
        Fill_rxTemp(&rx_dati_0[ix0F]);
        if (++ix0F >= NUM_BUFF_RX_O)
            ix0F = 0;
//        can00e = 1; // can01 = can0 rx irq

        Base = rx_temp.id & 0xFFF0;
        SubCommand = rx_temp.id & 0x000F;

        if (Base == ee_CanColl.unIdIride)    // ricezione da IRIDE
        {
            switch (SubCommand)
            {
            case 0x01:
                sys_can_IRIS_STATUS();
                break;
            }
        }
        else if (Base == ee_CanColl.unIdRot)    // ricezione da ROTAZIONE
        {
            switch (SubCommand)
            {
            case 0x01:
                sys_can_ROT_STATUS();
                break;
            }
        }
        else if (Base == 0x7A0)    // ricezione a ASR003
        {
            switch (SubCommand) // comandi con base 0x7A0
            {
            case 0x0A:
                sys_can_7AA(TRUE);
                break;
            case 0x0B:
                sys_can_7AB(TRUE);
                break;
            case 0x0C:
                sys_can_7AC(TRUE);
                break;
            case 0x0E:
                sys_can_7AE();
                break;
            }
        }
        else if (Base == ee_CanColl.unIdCommand)    // ricezione per collimatore
        {
            switch (SubCommand) // comandi con base 0x7A0 (default)
            {
            case 0x00:
                break;
            }
        }
        else if ((Base == ee_CanAddressConfig[STEPPER_A].uwBoardAddress) ||   // ricezione per Motore A
                (Base == ee_CanAddressConfig[STEPPER_B].uwBoardAddress) ||
                (Base == ee_CanAddressConfig[STEPPER_C].uwBoardAddress) ||
                (Base == ee_CanAddressConfig[STEPPER_D].uwBoardAddress))
        {
            num_motore = check_stepper_interno(rx_temp.id); //  ******************************
            if (num_motore != 0xFF)                     //  ****  COMANDI PER MOTORE  ****
            {                                           //  ******************************
                passi = (UINT32)rx_temp.data[4] & 0x000000FF;
                passi |= ((UINT32)rx_temp.data[3] << 8) & 0x0000FF00;
                passi |= ((UINT32)rx_temp.data[2] << 16) & 0x00FF0000;
                passi |= ((UINT32)rx_temp.data[1] << 24) & 0xFF000000;

                switch (rx_temp.id & 0x000F)        // comandi con base motore
                {
                case MSG_MOT_MOVE:
                    if (rx_temp.data[0] == MSG_MOV_HOME)
                        config_movimento(num_motore, rx_temp.data[0], passi, MOV_RESET);
                    else
                        config_movimento(num_motore, rx_temp.data[0], passi, MOV_FAST);
                    break;
                case MSG_MOT_SPEED:
                    config_speed(num_motore, rx_temp.data[0]);
                    break;
                case MSG_MOT_MODE:
                    config_mode(num_motore, rx_temp.data[0]);
                    break;
                case MSG_MOT_OPTION_2:
        //           config_option_2(num_motore);
                    break;
                case MSG_MOT_READ_VAL:
                    send_mot_value(num_motore);
                    break;
                }
            }
        }
    }

}







//*******************************************
//  controlla se stepper e' interno
// out:
// FF=non interno
// 0-3: uno dei quattro motori
//*******************************************
UINT8 check_stepper_interno(UINT16 id)
{
    id &= 0xFFF0;

    if (id == ee_CanAddressConfig[STEPPER_A].uwBoardAddress)
        return STEPPER_A;
    if (id == ee_CanAddressConfig[STEPPER_B].uwBoardAddress)
        return STEPPER_B;
    if (id == ee_CanAddressConfig[STEPPER_C].uwBoardAddress)
        return STEPPER_C;
    if (id == ee_CanAddressConfig[STEPPER_D].uwBoardAddress)
        return STEPPER_D;

    return 0xFF;

}






//**************************************************
//  movimenta un motore
//**************************************************
void config_movimento(UINT8 num_mot, UINT8 comando, SINT32 passi, UINT8 tipomov)
{
    UINT32 freq_min, freq_max;
    SINT32 temp32;

//         setta_motore(set_stepper, set_passi, set_speed, set_fmin, set_fmax, set_rampa);

    if ((num_mot == STEPPER_A) || (num_mot == STEPPER_B) || (num_mot == STEPPER_D))
    {
        switch (tipomov)
        {
        case MOV_RESET:
            freq_min = FREQ_MIN_RESET;       // 24.8 - effettua i calcoli necessari per la rampa
            if (TestPassi == FALSE)
            {
                freq_max = FREQ_MAX_RESET;
            }
            else
            {
                freq_max = FREQ_MAX_RESET_TEST_PHOTO;
            }
            break;
        case MOV_SLOW:
            freq_min = (UINT32)Steppers[num_mot].uwFMin;
            freq_max = (UINT32)Steppers[num_mot].uwFMax;
            break;
        case MOV_MM:
            if (ee_ConfigColl.TipoManualSpost == TIPO_MAN_SPOST_OLD)
            {
                freq_min = (UINT32)Steppers[num_mot].uwFMin;
                freq_max = (UINT32)Steppers[num_mot].uwFMax;
            }
            else
            {
                freq_min = FREQ_MOT_MIN;
                freq_max = calcola_speed_manuale(num_mot);
            }
            break;
        case MOV_FAST:
            freq_min = (UINT32)Steppers[num_mot].uwFMin;
            freq_max = (UINT32)Steppers[num_mot].uwFMaxFast;
            break;
        case MOV_ENC:
            freq_min = 200UL;
            freq_max = (((UINT32)EncoderSpeed[num_mot] * ((UINT32)ee_ConfigColl.EncAccel - 1)) / (UINT32)ee_ConfigColl.EncMaxSpeed) + 1;
            freq_max *= (UINT32)EncoderSpeed[num_mot];
            freq_max *= (UINT32)ee_ConfigColl.EncMaxFreq;
            freq_max /= ((UINT32)ee_ConfigColl.EncAccel * (UINT32)ee_ConfigColl.EncMaxSpeed);
            freq_max += freq_min;
            if (freq_max < freq_min)
                freq_max = freq_min;
            if (freq_max > (UINT32)ee_ConfigColl.EncMaxFreq)
                freq_max = (UINT32)ee_ConfigColl.EncMaxFreq;
            break;
        }

        passi_rampa[num_mot] = PASSI_RAMPA_LAMELLE;

        i_MotMinFreq[num_mot] = freq_min << 8;       // imposta le frequenza di lavoro
        i_MotMaxFreq[num_mot] = freq_max << 8;
        i_MotStepFreq[num_mot] = MOT_FREQ_STEP << 8;        // 24.8
        i_MotStopFreq[num_mot] = FREQ_MOT_STOP_LAMELLE << 8;
        i_MotFreqMotMult[num_mot] = FREQ_MOT_MULTIPLIER;


        flag_motore[num_mot] = TRUE;        // ferma irq copia parametri
        switch (comando)       // tipo di movimento
        {
        case MSG_MOV_HOME:     // HOME
            Reset_Run[num_mot] = TRUE;
            stato_home_mot[num_mot] = ST_HOME_OUT_SENS;
            Com_Irq[num_mot] &= ~COM_MOTORE_RESET;
            Comando_Motore[num_mot] = COM_MOTORE_RESET;
            break;
        case MSG_MOV_ABS:     // ASSOLUTO
            if (passi == g_MaxSteps[num_mot])
            {
                checkNewPhoto[num_mot] = TRUE;
            }
            else
            {
                PassiPhoto[num_mot] = 0;
            }
            m_MotPosizione[num_mot] = passi;
            break;
        case MSG_MOV_REL:     // RELATIVO
            temp32 = m_MotPosizione[num_mot] + passi;
            if (temp32 == g_MaxSteps[num_mot])
            {
                checkNewPhoto[num_mot] = TRUE;
            }
            else
            {
                PassiPhoto[num_mot] = 0;
            }
            m_MotPosizione[num_mot] = temp32;
            break;
        case MSG_MOV_SET:     // SET POSIZIONE ASSOLUTA
            i_MotPosizione[num_mot] = passi;
            g_lVPos[num_mot] = passi;
            m_MotPosizione[num_mot] = passi;
            mi_MotPosizione[num_mot] = passi;
            break;
        case MSG_MOV_STOP:     // STOP MOTORE
            Comando_Motore[num_mot] = COM_MOTORE_STOP;
            break;
        case MSG_MOV_BRAKE:     // STOP MOTORE con FRENATA
            Comando_Motore[num_mot] = COM_MOTORE_BRAKE;
            break;
        case MSG_MOV_RELEASE:     // RILASCIA MOTORE
            Comando_Motore[num_mot] = (COM_MOTORE_STOP | COM_MOTORE_RELEASE);
            break;
        }
        flag_motore[num_mot] = FALSE;
    }
    else
    {
        switch (tipomov)
        {
        case MOV_RESET:
            freq_min = (UINT32)Steppers[num_mot].uwFMin;       // 24.8 - effettua i calcoli necessari per la rampa
            freq_max = (UINT32)Steppers[num_mot].uwFMin + (((UINT32)Steppers[num_mot].uwFMin + (UINT32)Steppers[num_mot].uwFMax) / 2);
            break;
        case MOV_SLOW:
        case MOV_FAST:
            freq_min = (UINT32)Steppers[num_mot].uwFMin;
            freq_max = (UINT32)Steppers[num_mot].uwFMax;
            break;
        }

        switch (ee_FilterConfig.TipoMotore)
        {
        case MOTORE_FILTRO_OLD:
            passi_rampa[num_mot] = PASSI_RAMPA_FILTER_OLD;
            i_MotStepFreq[num_mot] = MOT_FILTER_STEP_OLD << 8;        // 24.8
            break;
        case MOTORE_FILTRO_NEW:
            passi_rampa[num_mot] = PASSI_RAMPA_FILTER_NEW;
            i_MotStepFreq[num_mot] = MOT_FILTER_STEP_NEW << 8;        // 24.8
            break;
        case MOTORE_FILTRO_FAST:
            passi_rampa[num_mot] = PASSI_RAMPA_FILTER_FAST;
            i_MotStepFreq[num_mot] = MOT_FILTER_STEP_FAST << 8;        // 24.8
            break;
        }

        i_MotMinFreq[num_mot] = freq_min << 8;       // imposta le frequenza di lavoro
        i_MotMaxFreq[num_mot] = freq_max << 8;
        i_MotStopFreq[num_mot] = FREQ_MOT_STOP_FILTRO << 8;
        i_MotFreqMotMult[num_mot] = FREQ_MOT_MULTIPLIER;


        flag_motore[num_mot] = TRUE;        // ferma irq copia parametri
        switch (comando)       // tipo di movimento
        {
        case MSG_MOV_HOME:     // HOME
            stato_home_mot[num_mot] = ST_HOME_OUT_SENS;
            g_bReqHomeStepperC = TRUE;
            Reset_Run[num_mot] = TRUE;
            Com_Irq[num_mot] &= ~COM_MOTORE_RESET;
            Comando_Motore[num_mot] = COM_MOTORE_RESET;
            break;
        case MSG_MOV_ABS:     // ASSOLUTO
            m_MotPosizione[num_mot] = passi;
            break;
        case MSG_MOV_REL:     // RELATIVO
            m_MotPosizione[num_mot] += passi;
            break;
        case MSG_MOV_SET:     // SET POSIZIONE ASSOLUTA
            i_MotPosizione[num_mot] = passi;
            g_lVPos[num_mot] = passi;
            m_MotPosizione[num_mot] = passi;
            mi_MotPosizione[num_mot] = passi;
            break;
        case MSG_MOV_STOP:     // STOP MOTORE
            Comando_Motore[num_mot] = COM_MOTORE_STOP;
            break;
        case MSG_MOV_BRAKE:     // STOP MOTORE con FRENATA
            Comando_Motore[num_mot] = COM_MOTORE_BRAKE;
            break;
        case MSG_MOV_RELEASE:     // RILASCIA MOTORE
            Comando_Motore[num_mot] = (COM_MOTORE_STOP | COM_MOTORE_RELEASE);
            break;
        }
        flag_motore[num_mot] = FALSE;

    }


}





//**************************************************
//  setta la velocita' del motore
//**************************************************
void config_speed(UINT8 num_mot, UINT8 mask)
{
    UINT16 temp16;

    if (enable_variazione == FALSE)
        return;

    if (mask & 0x01)    // Rampa
    {
        temp16 = (UINT16)rx_temp.data[5] << 8;
        temp16 |= (UINT16)rx_temp.data[6];
        if (temp16 < MIN_STEPPER_TRAMP)
            temp16 = MIN_STEPPER_TRAMP;
        if (temp16 > MAX_STEPPER_TRAMP)
            temp16 = MAX_STEPPER_TRAMP;
        if (ee_ConfigBoards[num_mot].uwRamp != temp16)
        {
            ee_ConfigBoards[num_mot].uwRamp = temp16;
            g_ulUpdateEeprom |= EEP_UPDATE_DSPEED;
        }
        Steppers[num_mot].uwTRamp = temp16;
    }

    if (mask & 0x02)    // FMax
    {
        temp16 = (UINT16)rx_temp.data[3] << 8;
        temp16 |= (UINT16)rx_temp.data[4];
        if (temp16 < MIN_STEPPER_FREQ)
            temp16 = MIN_STEPPER_FREQ;
        if (temp16 > MAX_STEPPER_FREQ)
            temp16 = MAX_STEPPER_FREQ;
        if (ee_ConfigBoards[num_mot].uwFMax != temp16)
        {
            ee_ConfigBoards[num_mot].uwFMax = temp16;
            g_ulUpdateEeprom |= EEP_UPDATE_DSPEED;
        }
        Steppers[num_mot].uwFMax = temp16;
    }

    if (mask & 0x04)    // FMin
    {
        temp16 = (UINT16)rx_temp.data[1] << 8;
        temp16 |= (UINT16)rx_temp.data[2];
        if (temp16 < MIN_STEPPER_FREQ)
            temp16 = MIN_STEPPER_FREQ;
        if (temp16 > MAX_STEPPER_FREQ)
            temp16 = MAX_STEPPER_FREQ;
        if (ee_ConfigBoards[num_mot].uwFMin != temp16)
        {
            ee_ConfigBoards[num_mot].uwFMin = temp16;
            g_ulUpdateEeprom |= EEP_UPDATE_DSPEED;
        }
        Steppers[num_mot].uwFMin = temp16;
    }


}




//**************************************************
//  setta la coppia e varie del motore
//**************************************************
void config_mode(UINT8 num_mot, UINT8 mask)
{
    UINT8 temp8;

    if (enable_variazione == FALSE)
        return;

    if (mask & 0x01)    // Fotosensore
    {
        temp8 = rx_temp.data[4] & 0x03;
        if (ee_ConfigBoards[num_mot].ucPhoto != temp8)
        {
            ee_ConfigBoards[num_mot].ucPhoto = temp8;
            g_ulUpdateEeprom |= EEP_UPDATE_DMODE;
        }
        Steppers[num_mot].ucLevel = temp8;
    }

    if (mask & 0x02)    // Decay
    {
        temp8 = rx_temp.data[3] & 0x03;
        if (ee_ConfigBoards[num_mot].ucDecay != temp8)
        {
            ee_ConfigBoards[num_mot].ucDecay = temp8;
            g_ulUpdateEeprom |= EEP_UPDATE_DMODE;
        }
    }

    if (mask & 0x04)    // Torque
    {
        temp8 = rx_temp.data[2] & 0x03;
        if (ee_ConfigBoards[num_mot].ucTorque != temp8)
        {
            ee_ConfigBoards[num_mot].ucTorque = temp8;
            g_ulUpdateEeprom |= EEP_UPDATE_DMODE;
        }
    }

    if (mask & 0x08)    // MicroStep
    {
        temp8 = rx_temp.data[1] & 0x0F;
        if (ee_ConfigBoards[num_mot].ucStep != temp8)
        {
            ee_ConfigBoards[num_mot].ucStep = temp8;
            g_ulUpdateEeprom |= EEP_UPDATE_DMODE;
        }
    }


}



//****************************************
//  invia dati relativi sl motore
//****************************************
void send_mot_value(UINT8 num_mot)
{
    UINT8 i, flag;

    if (rx_temp.dlc == 1)
    {
        flag = FALSE;
        for (i = 0; i < 8; i++)
            tx_temp.data[i] = 0;

        if (rx_temp.data[0] == 0x04)       // drive MODE
        {
            tx_temp.id = (rx_temp.id & 0xFFF0) + MSG_MOT_MODE;
            tx_temp.dlc = 4;
            tx_temp.data[0] = ee_ConfigBoards[num_mot].ucStep;
            tx_temp.data[1] = ee_ConfigBoards[num_mot].ucTorque;
            tx_temp.data[2] = ee_ConfigBoards[num_mot].ucDecay;
            tx_temp.data[3] = ee_ConfigBoards[num_mot].ucPhoto;
            copy_tx_temp(FALSE);     // transmit
        }

        if (rx_temp.data[0] == 0x05)       // drive SPEED
        {
            tx_temp.id = (rx_temp.id & 0xFFF0) + MSG_MOT_SPEED;
            tx_temp.dlc = 6;
            tx_temp.data[0] = ee_ConfigBoards[num_mot].uwFMin >> 8;
            tx_temp.data[1] = (UINT8)ee_ConfigBoards[num_mot].uwFMin;
            tx_temp.data[2] = ee_ConfigBoards[num_mot].uwFMax >> 8;
            tx_temp.data[3] = (UINT8)ee_ConfigBoards[num_mot].uwFMax;
            tx_temp.data[4] = ee_ConfigBoards[num_mot].uwRamp >> 8;
            tx_temp.data[5] = (UINT8)ee_ConfigBoards[num_mot].uwRamp;
            copy_tx_temp(FALSE);     // transmit
        }

    }
}








//--------------------------------------------------------------
void sys_can_ROT_STATUS(void)
{

    posizione_rot = (UINT32)rx_temp.data[0] << 24;
    posizione_rot |= (UINT32)rx_temp.data[1] << 16;
    posizione_rot |= (UINT32)rx_temp.data[2] << 8;
    posizione_rot |= (UINT32)rx_temp.data[3];
    adc_rot = (UINT16)rx_temp.data[5] << 8;
    adc_rot |= (UINT16)rx_temp.data[6];

}



//*************************************************
//  manda messaggio di stato se scaduto timer
//*************************************************
void manda_msg_stato(void)
{
                        // verifica se tempo di trasmettere un messaggio
    while (true)
    {
        if (g_bTimeCanStatusMessage)
            break;
        if (g_bUpdateA)
            break;
        if (g_bUpdateB)
            break;
        if (g_bUpdateC)
            break;
        if (g_bUpdateD)
            break;
        return;
    }

    if (g_bTimeCanStatusMessage)
    {
        g_bTimeCanStatusMessage = FALSE;
        g_bUpdateA = TRUE;
        g_bUpdateB = TRUE;
        g_bUpdateC = TRUE;
        g_bUpdateD = TRUE;
    }


    if (g_bUpdateA)
    {
        g_bUpdateA = FALSE;
        fill_msg_stato(STEPPER_A);
        return;
    }

    if (g_bUpdateB)
    {
        g_bUpdateB = FALSE;
        fill_msg_stato(STEPPER_B);
        return;
    }

    if (g_bUpdateC)
    {
        g_bUpdateC = FALSE;
        fill_msg_stato(STEPPER_C);
        return;
    }

    if (g_bUpdateD)
    {
        g_bUpdateD = FALSE;
        fill_msg_stato(STEPPER_D);
        return;
    }

}




//******************************************************
//  mette in fifo il messaggio di stato dello stepper
//******************************************************
void fill_msg_stato(UINT8 stepper)
{
    SINT32 stemp32;

    tx_temp.id = ee_CanAddressConfig[stepper].uwBoardAddress + MSG_MOT_STATO;
    tx_temp.dlc = 8;
    if (stepper == STEPPER_C)
    {
        stemp32 = g_lVPos[STEPPER_C];
    }
    else
    {
        stemp32 = i_MotPosizione[stepper];
    }
    tx_temp.data[0] = (stemp32 >> 24) & 0xFF;
    tx_temp.data[1] = (stemp32 >> 16) & 0xFF;
    tx_temp.data[2] = (stemp32 >> 8) & 0xFF;
    tx_temp.data[3] =  stemp32 & 0xFF;
    tx_temp.data[4] = Steppers[stepper].StepperData.StepperByte;
    tx_temp.data[5] = Steppers[stepper].ucStepperError;
    if (stepper == STEPPER_A)
    {
        tx_temp.data[6] = g_nFilteredAdc[POT_CROSS] >> 8;      // Pot MSB
        tx_temp.data[7] = g_nFilteredAdc[POT_CROSS] & 0xFF;      // Pot LSB
    }
    else if (stepper == STEPPER_B)
    {
        tx_temp.data[6] = g_nFilteredAdc[POT_LONG] >> 8;      // Pot MSB
        tx_temp.data[7] = g_nFilteredAdc[POT_LONG] & 0xFF;      // Pot LSB
    }
    else
    {
        tx_temp.data[6] = 0;
        tx_temp.data[7] = 0;
    }

    copy_tx_temp(FALSE);     // transmit
}




//****************************************************
//  copia da buffer temporaneo a FIFO TX
//****************************************************
void copy_tx_temp(UINT8 remote)
{
    CAN_STD_DATA_DEF *ptr;

    ptr = &tx_dati_0[ixTI];

    ptr->id = tx_temp.id;
    ptr->dlc = tx_temp.dlc;
    ptr->remote = remote;
    ptr->data.data[0] = tx_temp.data[0];
    ptr->data.data[1] = tx_temp.data[1];
    ptr->data.data[2] = tx_temp.data[2];
    ptr->data.data[3] = tx_temp.data[3];
    ptr->data.data[4] = tx_temp.data[4];
    ptr->data.data[5] = tx_temp.data[5];
    ptr->data.data[6] = tx_temp.data[6];
    ptr->data.data[7] = tx_temp.data[7];

    block_tx = TRUE;
    if (++ixTI >= NUM_BUFF_TX_CAN)
        ixTI = 0;
    block_tx = FALSE;

}




//********************************************************
//  chiede la conferma per un menu di calibrazione
//********************************************************
UINT8 chiedi_conferma(UINT8 *msgita, UINT8 *msgeng)
{
    UINT16 nA;
    UINT8 risp;


    risp = RISP_IDLE;
    switch (stato_scelta)
    {
    case 0:
        if (ee_ConfigColl.ucLanguage == ITA_LANG)
            sys_print_msg (0x80, msgita);
        else
            sys_print_msg (0x80, msgeng);
        g_wCrossMagnitude = 0;
        g_wLongMagnitude = 0;
        conferma = 0;
        stato_scelta = 1;
        break;

    case 1:
        if (ee_ConfigColl.ucLanguage == ITA_LANG){
            if (conferma == 0)
                sys_print_msg (0xC0,"Confermi? -> NO     ");
            else
                sys_print_msg (0xC0,"Confermi? -> SI     ");
        }
        if (ee_ConfigColl.ucLanguage == ENG_LANG){
            if (conferma == 0)
                sys_print_msg (0xC0,"Confirm? -> NO      ");
            else
                sys_print_msg (0xC0,"Confirm? -> YES     ");
        }
        stato_scelta = 2;
        break;

    case 2:
        nA = EncoderCalib(&g_wCrossMagnitude, 2);
        if (nA)
        {
            conferma += nA;
            if (conferma > 1)
                conferma = 1;
            if (conferma < 0)
                conferma = 0;
            timeout_taratura = DEF_TIME_TARATURA;
            stato_scelta = 1;
        }
        if (key_pressed)
        {
            key_pressed = FALSE;
            timeout_taratura = DEF_TIME_TARATURA;
            switch (val_tasto)
            {
            case KEY_FILTER:
                sys_print_msg (0xC0,"                    ");
                conferma = 0;
                risp = RISP_FALSE;
                break;
            case KEY_LAMP:
                write_msg = TRUE;
                if (conferma == 1)
                {
                    sys_print_msg (0xC0,"                    ");
                    risp = RISP_OK;
                }
                break;
            }
        }
        break;
    }

    return risp;
}




//********************************************************
//  chiede la conferma per un menu di calibrazione
//********************************************************
UINT8 chiedi_conferma_ulteriore(UINT8 *msgita, UINT8 *msgeng)
{
    UINT16 nA;
    UINT8 risp;


    risp = RISP_IDLE;
    switch (stato_scelta)
    {
    case 0:
        if (ee_ConfigColl.ucLanguage == ITA_LANG)
            sys_print_msg (0x80, msgita);
        else
            sys_print_msg (0x80, msgeng);
        g_wCrossMagnitude = 0;
        g_wLongMagnitude = 0;
        conferma = 0;
        stato_scelta = 1;
        break;

    case 1:
        if (ee_ConfigColl.ucLanguage == ITA_LANG){
            if (conferma == 0)
                sys_print_msg (0xC0,"Sei Sicuro? -> NO   ");
            else
                sys_print_msg (0xC0,"Sei Sicuro? -> SI   ");
        }
        if (ee_ConfigColl.ucLanguage == ENG_LANG){
            if (conferma == 0)
                sys_print_msg (0xC0,"Are you Sure? -> NO ");
            else
                sys_print_msg (0xC0,"Are you Sure? -> YES");
        }
        stato_scelta = 2;
        break;

    case 2:
        nA = EncoderCalib(&g_wCrossMagnitude, 2);
        if (nA)
        {
            conferma += nA;
            if (conferma > 1)
                conferma = 1;
            if (conferma < 0)
                conferma = 0;
            timeout_taratura = DEF_TIME_TARATURA;
            stato_scelta = 1;
        }
        if (key_pressed)
        {
            key_pressed = FALSE;
            timeout_taratura = DEF_TIME_TARATURA;
            switch (val_tasto)
            {
            case KEY_FILTER:
                sys_print_msg (0xC0,"                    ");
                conferma = 0;
                risp = RISP_FALSE;
                break;
            case KEY_LAMP:
                write_msg = TRUE;
                if (conferma == 1)
                {
                    sys_print_msg (0xC0,"                    ");
                    risp = RISP_OK;
                }
                break;
            }
        }
        break;
    }

    return risp;
}




//***********************************************
//  gestione tastiera per taratura rotazione
//  piu' eventuali varie
//***********************************************
void gestione_tastiera(void)
{
    static UINT8 buffer[30];
    UINT8 flag;
    SINT32 passi;
    SINT16 nA;


    if (g_ucWorkModality != CONFIGURATION)      // solo in CONFIGURATION
        return;


                    //  TARATURA SCHEDA DI ROTAZIONE

    if ((timeout_taratura == 0) &&
        (stato_tastiera > ST_KEY_IDLE))
    {
        stato_tastiera = ST_KEY_PREP_IDLE;
    }


    flag = FALSE;

    switch (stato_tastiera)
    {
    case ST_KEY_PREP_IDLE:
        Luce_TIMER_STOP();
        sys_print_msg (0x80, "                    ");
        s_ucDisplayQueueStatus = DISPLAY_RUNNING;
        stato_tastiera = ST_KEY_IDLE;
        break;

    case ST_KEY_IDLE:
        if (key_pressed)
        {
            key_pressed = FALSE;
            timeout_taratura = DEF_TIME_TARATURA;
            switch (val_tasto)
            {
            case KEY_FILTER:
                stato_tastiera = ST_KEY_ROTAZIONE_INIT;
                s_ucDisplayQueueStatus = DISPLAY_HOLDING;
                break;
            case KEY_LAMP:
                break;
            }
        }
        break;

//*************************************************************************************************************
    case ST_KEY_ROTAZIONE_INIT:
        if (ee_ConfigColl.ucRotation == ROTAZIONE_YES)
        {
            stato_scelta = 0;
            stato_tastiera = ST_KEY_ROTAZIONE_CONFIRM;        // solo se abilitata tara la rotazione
        }
        else
        {
            stato_tastiera = ST_KEY_END_ROTAZIONE;
        }
        s_ucDisplayQueueStatus = DISPLAY_HOLDING;
        break;

    case ST_KEY_ROTAZIONE_CONFIRM:
        switch (chiedi_conferma("Calibr. Rotazione   ", "Rotation Calibration"))
        {
        case RISP_IDLE:
            break;
        case RISP_OK:
            sys_can_send_taratura(COM_TAR_ENABLE_BUTTON);
            stato_tastiera = ST_KEY_M45;        // solo se abilitata tara la rotazione
            break;
        case RISP_FALSE:
            stato_tastiera = ST_KEY_END_ROTAZIONE;
            break;
        }
        break;

    case ST_KEY_M45:
        if (write_msg)
        {
            Luce_TIMER_START();
            write_msg = FALSE;
            sprintf(buffer, "-45 Gr:%+04ld A/D:%04u", posizione_rot, adc_rot);
            sys_print_msg (0x80, buffer);
        }

        if (key_pressed)
        {
            key_pressed = FALSE;
            timeout_taratura = DEF_TIME_TARATURA;
            switch (val_tasto)
            {
            case KEY_FILTER:
                write_msg = TRUE;
                stato_tastiera = ST_KEY_0;
                break;
            case KEY_LAMP:
                flag = TRUE;
                sys_can_send_taratura(COM_TAR_POT_MIN);
                sys_can_send_taratura(COM_TAR_POS_MIN);
                break;
            }
        }
        break;

    case ST_KEY_0:
        if (write_msg)
        {
            Luce_TIMER_START();
            write_msg = FALSE;
            sprintf(buffer, " 0  Gr:%+04ld A/D:%04u", posizione_rot, adc_rot);
            sys_print_msg (0x80, buffer);
        }

        if (key_pressed)
        {
            key_pressed = FALSE;
            timeout_taratura = DEF_TIME_TARATURA;
            switch (val_tasto)
            {
            case KEY_FILTER:
                write_msg = TRUE;
                stato_tastiera = ST_KEY_P45;
                break;
            case KEY_LAMP:
                flag = TRUE;
                sys_can_send_taratura(COM_TAR_POS_MED);
                break;
            }
        }
        break;

    case ST_KEY_P45:
        if (write_msg)
        {
            Luce_TIMER_START();
            write_msg = FALSE;
            sprintf(buffer, "+45 Gr:%+04ld A/D:%04u", posizione_rot, adc_rot);
            sys_print_msg (0x80, buffer);
        }

        if (key_pressed)
        {
            key_pressed = FALSE;
            Luce_TIMER_STOP();
            timeout_taratura = DEF_TIME_TARATURA;
            switch (val_tasto)
            {
            case KEY_FILTER:
                write_msg = TRUE;
                stato_tastiera = ST_KEY_END_ROTAZIONE;
                break;
            case KEY_LAMP:
                flag = TRUE;
                sys_can_send_taratura(COM_TAR_POT_MAX);
                sys_can_send_taratura(COM_TAR_POS_MAX);
                break;
            }
        }
        break;
    case ST_KEY_END_ROTAZIONE:
        stato_tastiera = ST_KEY_TAR_FORMATO_INIT;
        break;


//*************************************************************************************************************
    case ST_KEY_TAR_FORMATO_INIT:
        stato_scelta = 0;
        stato_tastiera = ST_KEY_TAR_FORMATO_CONFIRM;
        break;

    case ST_KEY_TAR_FORMATO_CONFIRM:
        switch (chiedi_conferma("Cal. Formato Max mm ", "Set Max Apertures mm"))
        {
        case RISP_IDLE:
            break;
        case RISP_OK:
            tar_cross = ee_ConfigBoards[STEPPER_A].uwRefSize;
            tar_long = ee_ConfigBoards[STEPPER_B].uwRefSize;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            stato_tastiera = ST_KEY_TAR_FORMATO;
            break;
        case RISP_FALSE:
            stato_tastiera = ST_KEY_TAR_FORMATO_END;
            break;
        }
        break;

    case ST_KEY_TAR_FORMATO:
        if (write_msg)
        {
            write_msg = FALSE;
            sprintf(buffer, "Cr:%03ld Lo:%03ld mm    ", tar_cross, tar_long);
            sys_print_msg (0x80, buffer);
        }

        if (g_wCrossMagnitude){
            tar_cross += EncoderCalib(&g_wCrossMagnitude, 2);
        }
        if (g_wLongMagnitude){
            tar_long += EncoderCalib(&g_wLongMagnitude, 2);
        }

        if (key_pressed)
        {
            key_pressed = FALSE;
            Luce_TIMER_STOP();
            timeout_taratura = DEF_TIME_TARATURA;
            switch (val_tasto)
            {
            case KEY_FILTER:
                write_msg = TRUE;
                stato_tastiera = ST_KEY_TAR_FORMATO_END;
                break;
            case KEY_LAMP:
                flag = TRUE;
                ee_ConfigBoards[STEPPER_A].uwRefSize= tar_cross;  // formato di riferimento in mm
                g_RefSize[STEPPER_A] = tar_cross;
                ee_ConfigBoards[STEPPER_B].uwRefSize= tar_long;  // formato di riferimento in mm
                g_RefSize[STEPPER_B] = tar_long;
                g_ulUpdateEeprom |= EEP_UPDATE_DSPEED;
                break;
            }
        }
        break;
    case ST_KEY_TAR_FORMATO_END:
        stato_tastiera = ST_KEY_TAR_PASSI_CROSS_INIT;
        break;


//*************************************************************************************************************
    case ST_KEY_TAR_PASSI_CROSS_INIT:
        stato_scelta = 0;
        stato_tastiera = ST_KEY_TAR_PASSI_CROSS_CONFIRM;
        break;

    case ST_KEY_TAR_PASSI_CROSS_CONFIRM:
        switch (chiedi_conferma("Calibr. Passi CROSS ", "Set Max CROSS Steps "))
        {
        case RISP_IDLE:
            break;
        case RISP_OK:
            g_bReqHomeStepperA = true;
            config_movimento(STEPPER_CROSS, MSG_MOV_HOME, 0, MOV_RESET);
            g_bReqHomeStepperB = true;
            config_movimento(STEPPER_LONG, MSG_MOV_HOME, 0, MOV_RESET);
            tar_cross = 0;
            tar_cross_temp = ee_ConfigBoards[STEPPER_A].uwMaxSteps;
            g_wCrossMagnitude = 0;
            g_MaxSteps[STEPPER_CROSS] = 10000;
            g_lVPos[STEPPER_A]=g_MaxSteps[STEPPER_CROSS];
            g_lPos[STEPPER_A]=g_MaxSteps[STEPPER_CROSS];
            stato_tastiera = ST_KEY_TAR_PASSI_CROSS;
            break;
        case RISP_FALSE:
            stato_tastiera = ST_KEY_TAR_PASSI_CROSS_END;
            break;
        }
        break;

    case ST_KEY_TAR_PASSI_CROSS:
        if (write_msg)
        {
            write_msg = FALSE;
            sprintf(buffer, "Cr. Steps:%04ld %04d ", tar_cross_temp, ee_ConfigBoards[STEPPER_A].uwMaxSteps);
            sys_print_msg (0x80, buffer);
        }

        if (g_wCrossMagnitude && !Steppers[STEPPER_CROSS].StepperData.StepperBits.bMoving){
            passi = EncoderCalib(&g_wCrossMagnitude, 8);
            if ((tar_cross - passi) < 0)
            {
                passi = tar_cross;
                tar_cross = 0;
            }
            else
            {
                tar_cross -= passi;
            }
            tar_cross_temp = tar_cross;
            g_lDestination[STEPPER_CROSS] = passi;
            config_movimento(STEPPER_CROSS, MSG_MOV_REL, g_lDestination[STEPPER_CROSS], MOV_FAST);
        }

        if (key_pressed)
        {
            key_pressed = FALSE;
            Luce_TIMER_STOP();
            timeout_taratura = DEF_TIME_TARATURA;
            switch (val_tasto)
            {
            case KEY_FILTER:
                write_msg = TRUE;
                g_MaxSteps[STEPPER_CROSS] = (UINT32)ee_ConfigBoards[STEPPER_A].uwMaxSteps;
                stato_tastiera = ST_KEY_TAR_PASSI_LONG_INIT;
                break;
            case KEY_LAMP:
                if (tar_cross != 0)
                {
                    flag = TRUE;
                    ee_ConfigBoards[STEPPER_A].uwMaxSteps= tar_cross;  // passi massimi da tutto chiuso a tutto aperto
    //                ee_ConfigBoards[STEPPER_A].uwRefSize= uwTemp2;  // formato di riferimento in mm
                    ee_ConfigBoards[STEPPER_A].uwRefSteps= tar_cross; // formato di riferimento in passi
                    ee_WindowConfig[WINDOW_2].lPosStepperA = tar_cross;
                    g_MaxSteps[STEPPER_CROSS] = tar_cross;
                    g_RefSteps[STEPPER_CROSS] = tar_cross;
                    g_ulUpdateEeprom |= EEP_UPDATE_DSPEED;
                    g_ulUpdateEeprom |= EEP_UPDATE_WINDOWS;
                }
                break;
            }
        }
        break;
    case ST_KEY_TAR_PASSI_CROSS_END:
        stato_tastiera = ST_KEY_TAR_PASSI_LONG_INIT;
        break;


//*************************************************************************************************************
    case ST_KEY_TAR_PASSI_LONG_INIT:
        stato_scelta = 0;
        stato_tastiera = ST_KEY_TAR_PASSI_LONG_CONFIRM;
        break;

    case ST_KEY_TAR_PASSI_LONG_CONFIRM:
        switch (chiedi_conferma("Calibr. Passi LONG  ", "Set Max LONG Steps  "))
        {
        case RISP_IDLE:
            break;
        case RISP_OK:
            g_bReqHomeStepperA = true;
            config_movimento(STEPPER_CROSS, MSG_MOV_HOME, 0, MOV_RESET);
            g_bReqHomeStepperB = true;
            config_movimento(STEPPER_LONG, MSG_MOV_HOME, 0, MOV_RESET);
            tar_long = 0;
            tar_long_temp = ee_ConfigBoards[STEPPER_B].uwMaxSteps;
            g_wLongMagnitude = 0;
            g_MaxSteps[STEPPER_LONG] = 10000;
            g_lVPos[STEPPER_B]=g_MaxSteps[STEPPER_LONG];
            g_lPos[STEPPER_B]=g_MaxSteps[STEPPER_LONG];
            stato_tastiera = ST_KEY_TAR_PASSI_LONG;
            break;
        case RISP_FALSE:
            stato_tastiera = ST_KEY_TAR_PASSI_LONG_END;
            break;
        }
        break;

    case ST_KEY_TAR_PASSI_LONG:
        if (write_msg)
        {
            write_msg = FALSE;
            sprintf(buffer, "Lo. Steps:%04ld %04d ", tar_long_temp, ee_ConfigBoards[STEPPER_B].uwMaxSteps);
            sys_print_msg (0x80, buffer);
        }

        if (g_wLongMagnitude && !Steppers[STEPPER_LONG].StepperData.StepperBits.bMoving){
            passi = EncoderCalib(&g_wLongMagnitude, 8);
            if ((tar_long - passi) < 0)
            {
                passi = tar_long;
                tar_long = 0;
            }
            else
            {
                tar_long -= passi;
            }
            tar_long_temp = tar_long;
            g_lDestination[STEPPER_LONG] = passi;
            config_movimento(STEPPER_LONG, MSG_MOV_REL, g_lDestination[STEPPER_LONG], MOV_FAST);
        }

        if (key_pressed)
        {
            key_pressed = FALSE;
            Luce_TIMER_STOP();
            timeout_taratura = DEF_TIME_TARATURA;
            switch (val_tasto)
            {
            case KEY_FILTER:
                write_msg = TRUE;
                g_MaxSteps[STEPPER_LONG] = ee_ConfigBoards[STEPPER_B].uwMaxSteps;
                stato_tastiera = ST_KEY_TAR_PASSI_LONG_END;
                break;
            case KEY_LAMP:
                if (tar_long != 0)
                {
                    flag = TRUE;
                    ee_ConfigBoards[STEPPER_B].uwMaxSteps= tar_long;  // passi massimi da tutto chiuso a tutto aperto
//                ee_ConfigBoards[STEPPER_B].uwRefSize= uwTemp2;  // formato di riferimento in mm
                    ee_ConfigBoards[STEPPER_B].uwRefSteps= tar_long; // formato di riferimento in passi
                    ee_WindowConfig[WINDOW_2].lPosStepperB = tar_long;
                    g_MaxSteps[STEPPER_LONG] = tar_long;
                    g_RefSteps[STEPPER_LONG] = tar_long;
                    g_ulUpdateEeprom |= EEP_UPDATE_DSPEED;
                    g_ulUpdateEeprom |= EEP_UPDATE_WINDOWS;
                }
                break;
            }
        }
        break;
    case ST_KEY_TAR_PASSI_LONG_END:
        stato_tastiera = ST_KEY_POT_OPEN_INIT;
        break;


//*************************************************************************************************************
    case ST_KEY_POT_OPEN_INIT:
        g_bReqHomeStepperA = true;
        config_movimento(STEPPER_CROSS, MSG_MOV_HOME, 0, MOV_RESET);
        g_bReqHomeStepperB = true;
        config_movimento(STEPPER_LONG, MSG_MOV_HOME, 0, MOV_RESET);
        if (ee_ConfigColl.ucHardwareType == HARDWARE_YES_POT)
        {
            stato_tastiera = ST_KEY_POT_OPEN;
        }
        else
        {
            stato_tastiera = ST_KEY_END_POT;
        }
        break;

    case ST_KEY_POT_OPEN:
        if (write_msg)
        {
            write_msg = FALSE;
            sprintf(buffer, "P.Open C:%04u L:%04u", g_nFilteredAdc[POT_CROSS], g_nFilteredAdc[POT_LONG]);
            sys_print_msg (0x80, buffer);
        }

        if (key_pressed)
        {
            key_pressed = FALSE;
            Luce_TIMER_STOP();
            timeout_taratura = DEF_TIME_TARATURA;
            switch (val_tasto)
            {
            case KEY_FILTER:
                write_msg = TRUE;
                g_lDestination[STEPPER_A] = 0;
                g_bMoveAbsStepperA = true;
                config_movimento(STEPPER_A, MSG_MOV_ABS, g_lDestination[STEPPER_A], MOV_SLOW);
                g_lDestination[STEPPER_B] = 0;
                g_bMoveAbsStepperB = true;
                config_movimento(STEPPER_B, MSG_MOV_ABS, g_lDestination[STEPPER_B], MOV_SLOW);
                stato_tastiera = ST_KEY_POT_CLOSE;
                break;
            case KEY_LAMP:
                flag = TRUE;
//                ee_ConfigColl.uwPotCross[NUM_TAR_POT - 1] = g_nFilteredAdc[POT_CROSS];
//                ee_ConfigColl.uwPotLong[NUM_TAR_POT - 1] = g_nFilteredAdc[POT_LONG];
//                g_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                break;
            }
        }
        break;

    case ST_KEY_POT_CLOSE:
        if (write_msg)
        {
            write_msg = FALSE;
            sprintf(buffer, "P.CloseC:%04u L:%04u", g_nFilteredAdc[POT_CROSS], g_nFilteredAdc[POT_LONG]);
            sys_print_msg (0x80, buffer);
        }

        if (key_pressed)
        {
            key_pressed = FALSE;
            Luce_TIMER_STOP();
            timeout_taratura = DEF_TIME_TARATURA;
            switch (val_tasto)
            {
            case KEY_FILTER:
                g_bReqHomeStepperA = true;
                config_movimento(STEPPER_CROSS, MSG_MOV_HOME, 0, MOV_RESET);
                g_bReqHomeStepperB = true;
                config_movimento(STEPPER_LONG, MSG_MOV_HOME, 0, MOV_RESET);
                stato_tastiera = ST_KEY_END_POT;
                break;
            case KEY_LAMP:
                flag = TRUE;
//                ee_ConfigColl.uwPotCross[0] = g_nFilteredAdc[POT_CROSS];
//                ee_ConfigColl.uwPotLong[0] = g_nFilteredAdc[POT_LONG];
//                g_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                break;
            }
        }
        break;
    case ST_KEY_END_POT:
        stato_tastiera = ST_KEY_IRIDE_RESET_INIT;
        break;


//*************************************************************************************************************
    case ST_KEY_IRIDE_RESET_INIT:
        if (ee_ConfigColl.ucIrisPresent == IRIDE_YES)
        {
            stato_scelta = 0;
            stato_tastiera = ST_KEY_IRIDE_CONFIRM;        // solo se abilitata tara la rotazione
        }
        else
        {
            stato_tastiera = ST_KEY_IRIDE_END;
        }
        break;

    case ST_KEY_IRIDE_CONFIRM:
        switch (chiedi_conferma("Cal. Manuale IRIDE  ", "IRIS Manual Calibr. "))
        {
        case RISP_IDLE:
            break;
        case RISP_OK:
            write_msg = TRUE;
            sys_can_send_taratura(COM_TAR_IN_TARATURA);     // toglie il controllo limiti dei tasti
            sys_can_send_taratura(COM_TAR_SET_POS_NOW);     // per abilitare il motore
            stato_tastiera = ST_KEY_IRIDE_RESET;        // solo se abilitata tara l'IRIDE
            break;
        case RISP_FALSE:
            stato_tastiera = ST_KEY_IRIDE_END;
            break;
        }
        break;

    case ST_KEY_IRIDE_RESET:
        if (write_msg)
        {
            write_msg = FALSE;
            sprintf(buffer, "iRESET %+05ld AD:%04u", posizione_iride, adc_iride);
            sys_print_msg (0x80, buffer);
        }

        if (key_pressed)
        {
            key_pressed = FALSE;
            timeout_taratura = DEF_TIME_TARATURA;
            switch (val_tasto)
            {
            case KEY_FILTER:
                write_msg = TRUE;
                num_formato_iride = 1;
                stato_tastiera = ST_KEY_IRIDE_FORMATI;
                break;
            case KEY_LAMP:
                flag = TRUE;
                num_formato_iride = 0;
                ee_IrisSteps.iride_calib_pot[0] = adc_iride;
                ee_IrisSteps.iride_calib_passi[0] = 0;
                sys_can_send_taratura(COM_TAR_SET_POS_0);     // per settare la posizione a 0
                sys_can_send_taratura(COM_TAR_SET_CALIB);     // per memorizzare la posizione
                g_ulUpdateEeprom |= EEP_UPDATE_IRIS;
                break;
            }
        }
        break;

    case ST_KEY_IRIDE_FORMATI:
        if (write_msg)
        {
            write_msg = FALSE;
            sprintf(buffer, "iFORM. %03ldmm AD:%04u", iride_calib_mm[num_formato_iride], adc_iride);
            sys_print_msg (0x80, buffer);
        }

        if (key_pressed)
        {
            key_pressed = FALSE;
            timeout_taratura = DEF_TIME_TARATURA;
            switch (val_tasto)
            {
            case KEY_FILTER:
                if (++num_formato_iride > IRIS_FORMAT_MAX)
                {
                    stato_tastiera = ST_KEY_IRIDE_MAX;
                }
                break;
            case KEY_LAMP:
                flag = TRUE;
                ee_IrisSteps.iride_calib_passi[num_formato_iride] = posizione_iride;   // salva i dati relativi al formato
                ee_IrisSteps.iride_calib_pot[num_formato_iride] = adc_iride;
                sys_can_send_taratura(COM_TAR_SET_CALIB);     // per memorizzare la posizione
                g_ulUpdateEeprom |= EEP_UPDATE_IRIS;
                break;
            }
        }
        break;

    case ST_KEY_IRIDE_MAX:
        if (write_msg)
        {
            write_msg = FALSE;
            sprintf(buffer, "iMAX   %+05ld AD:%04u", posizione_iride, adc_iride);
            sys_print_msg (0x80, buffer);
        }

        if (key_pressed)
        {
            key_pressed = FALSE;
            timeout_taratura = DEF_TIME_TARATURA;
            switch (val_tasto)
            {
            case KEY_FILTER:
                sys_can_send_taratura(COM_TAR_OUT_TARATURA);     // rimette il controllo limiti dei tasti
                sys_print_msg (0x80, "                    ");
                stato_tastiera = ST_KEY_IRIDE_END;
                break;
            case KEY_LAMP:
                flag = TRUE;
                ee_IrisSteps.iride_calib_passi[num_formato_iride] = posizione_iride;   // salva i dati relativi al formato
                ee_IrisSteps.iride_calib_pot[num_formato_iride] = adc_iride;
                Iride_PassiMax = posizione_iride;
                sys_can_send_taratura(COM_TAR_SET_PASSIMAX);     // per settare la posizione massima in passi
                sys_can_send_taratura(COM_TAR_SET_CALIB);     // per memorizzare la posizione
                g_ulUpdateEeprom |= EEP_UPDATE_IRIS;
                break;
            }
        }
        break;

    case ST_KEY_IRIDE_END:
        stato_tastiera = ST_KEY_AIRIDE_INIT;
        break;




//*************************************************************************************************************
    case ST_KEY_AIRIDE_INIT:
        if (ee_ConfigColl.ucIrisPresent == IRIDE_YES)
        {
            stato_scelta = 0;
            stato_tastiera = ST_KEY_AIRIDE_CONFIRM;        // solo se abilitata tara la rotazione
        }
        else
        {
            stato_tastiera = ST_KEY_AIRIDE_END;
        }
        break;

    case ST_KEY_AIRIDE_CONFIRM:
        switch (chiedi_conferma("Cal.Automatica IRIDE", "IRIS Auto Calibr.   "))
        {
        case RISP_IDLE:
            break;
        case RISP_OK:
            write_msg = TRUE;
            sys_can_send_taratura(COM_TAR_IN_TARATURA);     // toglie il controllo limiti dei tasti
            sys_can_send_taratura(COM_TAR_SET_POS_NOW);     // per abilitare il motore
            sys_print_msg (0x80,"POT. m=     M=      ");
            sys_print_msg (0xC0,"Step Max=           ");
            IrisPotMin = adc_iride;
            IrisPotMax = 0;
            IrisStepMax = 0;
            IrisStepTemp = 0;
            StatusConfig = 0;
            stato_tastiera = ST_KEY_AIRIDE_AUTOCAL;        // solo se abilitata tara l'IRIDE
            break;
        case RISP_FALSE:
            stato_tastiera = ST_KEY_AIRIDE_END;
            break;
        }
        break;

    case ST_KEY_AIRIDE_AUTOCAL:
        if (write_msg)
        {
            write_msg = FALSE;
            sys_print_number (0x87, IrisPotMin, 4, 0xff, false);
            sys_print_number (0x8E, IrisPotMax, 4, 0xff, false);
            sys_print_number (0xC9, IrisStepTemp, 4, 0xff, false);
        }
        switch (StatusConfig)
        {
        case 0:     // parte con il reset verso il minimo
            sys_can_send_taratura(COM_TAR_IRIS_RES_0);     // reset tutto chiuso
            timer_test = 10;
            StatusConfig++;
            break;
        case 1:     // attesa fino al blocco meccanico
            if (adc_iride < IrisPotMin)
            {
                IrisPotMin = adc_iride;
                timer_test = 10;
            }
            else
            {
                if (timer_test == 0)        // nessun cambiamento, passa allo step successivo
                {
                    sys_can_send_taratura(COM_TAR_IRIS_STOP);     // ferma motore
                    timer_test = 2;
                    StatusConfig++;
                }
            }
            break;
        case 2:     // parte con il tutto aperto
            if (timer_test == 0)        // nessun cambiamento, passa allo step successivo
            {
                sys_can_send_taratura(COM_TAR_SET_POS_0);     // setta passi a 0
                sys_can_send_taratura(COM_TAR_IRIS_RES_FF);     // reset tutto aperto
                FlagSetIris = TRUE;
                NewValIris = FALSE;
                NumMsg = 3;
                timer_test = 10;
                StatusConfig++;
            }
            break;
        case 3:     // attesa fino al blocco meccanico
            if (NewValIris)
            {
                NewValIris = FALSE;
                if (NumMsg)
                    NumMsg--;

                if (adc_iride > IrisPotMax)
                {
                    DiffAdc = adc_iride - IrisPotMax;
                    IrisPotMax = adc_iride;

                    if (NumMsg == 0)
                    {
                        if (DiffAdc > (DiffAdcPrec / 2))
                        {
                            DiffAdcPrec = DiffAdc;
                            if (FlagSetIris == TRUE)
                            {
                                IrisStepMax = posizione_iride;
                                IrisStepTemp = IrisStepMax;
                            }
                        }
                        else
                        {
                            FlagSetIris = FALSE;
                        }
                    }
                    timer_test = 10;
                }
                else
                {
                    DiffAdcPrec = 0;
                }
            }

            if (timer_test == 0)        // nessun cambiamento, passa allo step successivo
            {
                sys_can_send_taratura(COM_TAR_IRIS_STOP);     // ferma motore
                timer_test = 2;
                StatusConfig++;
            }
            break;
        case 4:
            if (timer_test == 0)
            {
                ee_IrisSteps.iride_calib_pot[0] = IrisPotMin + 4;   // setta il reset
                ee_IrisSteps.iride_calib_passi[0] = 0;
                sys_can_send_taratura(COM_TEST_IRIS_RES);     // reset normale
                timer_test = 2;

                StatusConfig++;
            }
            break;
        case 5:
            if (timer_test == 0)
            {
                if ((stato_iride & IRIDE_MOVE) == 0)        // controlla se fermo
                {
                    num_formato_iride = 0;
                    sys_can_send_taratura(COM_TAR_SET_CALIB);     // per memorizzare la posizione
                    num_formato_iride = 1;
                    sys_print_msg (0xC0,"Iris  mm=           ");
                    StatusConfig++;
                }
            }
            break;
        case 6:
            IrisStepTemp = iride_calib_mm[num_formato_iride];
            test_iris_passi = (IrisStepMax * iride_calib_perc[num_formato_iride]) / 1000UL;
            sys_can_send_taratura(COM_TEST_IRIS_POS);     // muove quasi tutto aperto
            write_msg = TRUE;
            timer_test = 20;
            StatusConfig++;
            break;
        case 7:
            if ((stato_iride & IRIDE_MOVE) == 0)        // controlla se fermo
            {
                if (timer_test == 0)
                {
                    ee_IrisSteps.iride_calib_passi[num_formato_iride] = posizione_iride;   // salva i dati relativi al formato
                    ee_IrisSteps.iride_calib_pot[num_formato_iride] = adc_iride;
                    sys_can_send_taratura(COM_TAR_SET_CALIB);     // per memorizzare la posizione
                    if (++num_formato_iride > (IRIS_FORMAT_MAX + 1))
                    {
                        Iride_PassiMax = posizione_iride;
                        sys_can_send_taratura(COM_TAR_SET_PASSIMAX);     // per settare la posizione massima in passi
                        sys_can_send_taratura(COM_TAR_OUT_TARATURA);     // rimette il controllo limiti dei tasti
                        sys_print_msg (0xCF,"OK!");
                        g_ulUpdateEeprom |= EEP_UPDATE_IRIS;
                        StatusConfig++;
                    }
                    else
                    {
                        StatusConfig = 6;
                    }
                }
            }
            else
            {
                timer_test = 9;
            }
            break;
        case 8:
            if (key_pressed)
            {
                key_pressed = FALSE;
                timeout_taratura = DEF_TIME_TARATURA;
                switch (val_tasto)
                {
                case KEY_FILTER:
                case KEY_LAMP:
                    stato_tastiera = ST_KEY_AIRIDE_END;
                    g_wCrossMagnitude = 0;
                    g_wLongMagnitude = 0;
                    break;
                }
            }
            break;
        }
        break;

    case ST_KEY_AIRIDE_END:
        stato_tastiera = ST_KEY_VIS_003_INIT;
        break;

//*************************************************************************************************************
    case ST_KEY_VIS_003_INIT:
        stato_scelta = 0;
        stato_tastiera = ST_KEY_VIS_003_CONFIRM;
        break;

    case ST_KEY_VIS_003_CONFIRM:
        switch (chiedi_conferma("Display dati ASR003 ", "Display ASR003 val. "))
        {
        case RISP_IDLE:
            break;
        case RISP_OK:
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            StatusConfig = 0;
            stato_tastiera = ST_KEY_VIS_003;
            break;
        case RISP_FALSE:
            stato_tastiera = ST_KEY_VIS_003_END;
            break;
        }
        break;

    case ST_KEY_VIS_003:
        nA = CheckChangeCrossEnc(); // selezione schermata
        if (nA)
        {
            write_msg = TRUE;
            timeout_taratura = DEF_TIME_TARATURA;
            StatusConfig += nA;
            if (StatusConfig > 3)
                StatusConfig = 3;
            if (StatusConfig < 0)
                StatusConfig = 0;
            break;
        }

        if (write_msg)      // visualizzazione
        {
            write_msg = FALSE;

            switch (StatusConfig)
            {
            case 0:
                dec2bin(RemoteStatus.bytes[0], &buffer[0]);
                dec2bin(RemoteStatus.bytes[1], &buffer[15]);
                sprintf(buffer, "%s %s  %d", &buffer[0], &buffer[15], StatusConfig + 1);
                sys_print_msg (0x80, buffer);
                dec2bin(RemoteStatus.bytes[2], &buffer[0]);
                dec2bin(RemoteStatus.bytes[3], &buffer[15]);
                sprintf(buffer, "%s %s   ", &buffer[0], &buffer[15]);
                sys_print_msg (0xC0, buffer);
                break;
            case 1:
                dec2bin(RemoteStatus.bytes[4], &buffer[0]);
                dec2bin(RemoteStatus.bytes[5], &buffer[15]);
                sprintf(buffer, "%s %s  %d", &buffer[0], &buffer[15], StatusConfig + 1);
                sys_print_msg (0x80, buffer);
                dec2bin(RemoteStatus.bytes[6], &buffer[0]);
                dec2bin(RemoteStatus.bytes[7], &buffer[15]);
                sprintf(buffer, "%s %s   ", &buffer[0], &buffer[15]);
                sys_print_msg (0xC0, buffer);
                break;
            case 2:
                sprintf(buffer, "Sta %04d Tab %04d  %d", RemoteAdc[DFF_STAT], RemoteAdc[DFF_TAB], StatusConfig + 1);
                sys_print_msg (0x80, buffer);
                sprintf(buffer, "CrV %04d LoV %04d   ", RemoteAdc[BUCKY_CROSS], RemoteAdc[BUCKY_LONG]);
                sys_print_msg (0xC0, buffer);
                break;
            case 3:
                sprintf(buffer, "CrL %04d LoL %04d  %d", RemoteAdc[LEFT_CROSS], RemoteAdc[LEFT_LONG], StatusConfig + 1);
                sys_print_msg (0x80, buffer);
                sprintf(buffer, "CrR %04d LoR %04d   ", RemoteAdc[RIGHT_CROSS], RemoteAdc[RIGHT_LONG]);
                sys_print_msg (0xC0, buffer);
                break;
            }
        }

        if (key_pressed)
        {
            key_pressed = FALSE;
            timeout_taratura = DEF_TIME_TARATURA;
            switch (val_tasto)
            {
            case KEY_FILTER:
                write_msg = TRUE;
                stato_tastiera = ST_KEY_VIS_003_END;
                break;
            case KEY_LAMP:
                break;
            }
        }
        break;
    case ST_KEY_VIS_003_END:
        stato_tastiera = ST_KEY_RESET_DEFAULT_INIT;
        break;


//*************************************************************************************************************
    case ST_KEY_RESET_DEFAULT_INIT:
        stato_scelta = 0;
        stato_tastiera = ST_KEY_RESET_DEFAULT_CONFIRM;
        break;

    case ST_KEY_RESET_DEFAULT_CONFIRM:
        switch (chiedi_conferma("Reset Valori Default", "Reset Default Values"))
        {
        case RISP_IDLE:
            break;
        case RISP_OK:
            stato_scelta = 0;
            stato_tastiera = ST_KEY_RESET_DEFAULT;
            break;
        case RISP_FALSE:
            stato_tastiera = ST_KEY_RESET_DEFAULT_END;
            break;
        }
        break;

    case ST_KEY_RESET_DEFAULT:
        switch (chiedi_conferma_ulteriore("Reset Valori Default", "Reset Default Values"))
        {
        case RISP_IDLE:
            break;
        case RISP_OK:
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            StatusConfig = 0;
            calcola_crc(TRUE);      // reset dati
            resetCollimator();
            write_msg = TRUE;
            stato_tastiera = ST_KEY_RESET_DEFAULT_END;
            break;
        case RISP_FALSE:
            write_msg = TRUE;
            stato_tastiera = ST_KEY_RESET_DEFAULT_END;
            break;
        }
        break;

    case ST_KEY_RESET_DEFAULT_END:
        stato_tastiera = ST_KEY_PREP_IDLE;
        break;


    }

//***************************************************************************************************

    if (flag == TRUE)
    {
        flag = FALSE;
        timer_display = 10;
        write_msg = FALSE;
        sprintf(buffer, "OK                  ");
        sys_print_msg (0x80, buffer);
    }
}





//*********************************************
//  trasforma in stringa in binario
//*********************************************
void dec2bin(UINT8 value, UINT8 *buff)
{
    UINT8 i,mask;

    mask = 0x80;
    for (i = 0; i < 8; i++)
    {
        if (value & mask)
        {
            buff[i] = '1';
        }
        else
        {
            buff[i] = '0';
        }
        mask >>= 1;
    }
    buff[8] = 0;
}




//************************************************
//  analizza il messaggio arrivato
//  source = 0: CANBUS
//  source = 1: SERIALE
//  write = 0: in lettura
//  write = 1: in scrittura
//************************************************
void CAN_config_Analyze(UINT8 source, UINT8 write)
{
    uchar ucRecvBytes[9];
    char cbTemp[35];
    static UINT32 s_ulUpdateEeprom = 0;
    schar scTemp, t1, t2;
    uword uwTemp, uwTemp2, uwTemp3;
    uchar ucTemp;
    UINT8 flag, i;
    char c;
    UINT8 WriteVal;



    WriteVal = write;


    if (source == 0)
    {
        ucRecvBytes[0] = rx_temp.data[0];
        ucRecvBytes[1] = rx_temp.data[1];
        ucRecvBytes[2] = rx_temp.data[2];
        ucRecvBytes[3] = rx_temp.data[3];
        ucRecvBytes[4] = rx_temp.data[4];
        ucRecvBytes[5] = rx_temp.data[5];
        ucRecvBytes[6] = rx_temp.data[6];
        ucRecvBytes[7] = rx_temp.data[7];
    }
    else
    {
        ucRecvBytes[0] = buff_rx_can[0];
        ucRecvBytes[1] = buff_rx_can[1];
        ucRecvBytes[2] = buff_rx_can[2];
        ucRecvBytes[3] = buff_rx_can[3];
        ucRecvBytes[4] = buff_rx_can[4];
        ucRecvBytes[5] = buff_rx_can[5];
        ucRecvBytes[6] = buff_rx_can[6];
        ucRecvBytes[7] = buff_rx_can[7];
    }
    ucRecvBytes[8] = 0;

    if (WriteVal == FALSE)
    {
        tx_temp.id = ID_CONFIG_READ;
        tx_temp.dlc = 8;
        tx_temp.data[0] = ucRecvBytes[0];
        tx_temp.data[1] = 0;
        tx_temp.data[2] = 0;
        tx_temp.data[3] = 0;
        tx_temp.data[4] = 0;
        tx_temp.data[5] = 0;
        tx_temp.data[6] = 0;
        tx_temp.data[7] = 0;
    }



    timer_led_config = 10;

    stato_tastiera = ST_KEY_PREP_IDLE;
    if (s_ucDisplayQueueStatus == DISPLAY_HOLDING)
        s_ucDisplayQueueStatus = DISPLAY_RUNNING;

    SET_LED_CAN_RX;

    if ((g_ucWorkModality == CONFIGURATION) ||
        (g_ucWorkModality == COLLIMATORWORK))
    {
        memset(cbTemp, ' ', 20);
        switch (ucRecvBytes[0])
        {

        case CONFIG_RS232:
            sys_display_queue_msg("RS232 COMM");
            break;

        case CONFIG_SERIAL_NO:
            /*
                           if (memcmp(&ee_RSR008B_Version.cbSerial, "S/N00000", 8)){
                                   sys_display_queue_msg("Command Error");
                                   break;
                               }
             */
            if (WriteVal == TRUE)
            {
                sprintf(cbTemp, "%s %d%d%d%d%d", "SERIAL = ", ucRecvBytes[1], ucRecvBytes[2], ucRecvBytes[3], ucRecvBytes[4], ucRecvBytes[5]);
                ucRecvBytes[1] += 0x30;
                ucRecvBytes[2] += 0x30;
                ucRecvBytes[3] += 0x30;
                ucRecvBytes[4] += 0x30;
                ucRecvBytes[5] += 0x30;
                memcpy(&ee_RSR008B_Version.cbSerial[3], &ucRecvBytes[1], 5);
                sys_display_queue_msg(cbTemp);
                s_ulUpdateEeprom |= EEP_UPDATE_SERIAL;
            }
            else
            {
                tx_temp.data[1] = ee_RSR008B_Version.cbSerial[3] & 0x0F;
                tx_temp.data[2] = ee_RSR008B_Version.cbSerial[4] & 0x0F;
                tx_temp.data[3] = ee_RSR008B_Version.cbSerial[5] & 0x0F;
                tx_temp.data[4] = ee_RSR008B_Version.cbSerial[6] & 0x0F;
                tx_temp.data[5] = ee_RSR008B_Version.cbSerial[7] & 0x0F;
            }
            break;

        case CONFIG_LANGUAGE:
            if (WriteVal == TRUE)
            {
                if ((ucRecvBytes[1] == LINGUA_ITA) || (ucRecvBytes[1] == LINGUA_ENG))
                {
                    if (ucRecvBytes[1] == LINGUA_ITA)
                    {
                        sys_display_queue_msg("LINGUA = ITALIANO");
                        ee_ConfigColl.ucLanguage = ITA_LANG;
                    }
                    else
                    {
                        sys_display_queue_msg("LANGUAGE = ENGLISH");
                        ee_ConfigColl.ucLanguage = ENG_LANG;
                    }
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                if (ee_ConfigColl.ucLanguage == ITA_LANG)
                {
                    tx_temp.data[1] = LINGUA_ITA;
                }
                else
                {
                    tx_temp.data[1] = LINGUA_ENG;
                }
            }
            break;

        case CONFIG_UNITS:
            if (WriteVal == TRUE)
            {
                if ((ucRecvBytes[1] == CM_UNITS) || (ucRecvBytes[1] == INCHES_UNITS))
                {
                    if (ucRecvBytes[1] == CM_UNITS)
                    {
                        sys_display_queue_msg("UNIT = CM");
                        ee_ConfigColl.ucCmUnit = CM_UNIT;
                    }
                    else
                    {
                        sys_display_queue_msg("UNIT = INCHES");
                        ee_ConfigColl.ucCmUnit = INCHES_UNIT;
                    }
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                if (ee_ConfigColl.ucCmUnit == CM_UNIT)
                {
                    tx_temp.data[1] = CM_UNITS;
                }
                else
                {
                    tx_temp.data[1] = INCHES_UNITS;
                }
            }
            break;

        case CONFIG_SHOW_DFF:
            if (WriteVal == TRUE)
            {
                if ((ucRecvBytes[1] == SHOW_DFF_NO) || (ucRecvBytes[1] == SHOW_DFF_YES))
                {
                    if (ucRecvBytes[1] == SHOW_DFF_NO)
                        sys_display_queue_msg("SHOW DFF = NO");
                    else
                        sys_display_queue_msg("SHOW DFF = YES");
                    ee_ConfigColl.ucShowDff = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucShowDff;
            }
            break;

        case CONFIG_FILTER:
            if (WriteVal == TRUE)
            {
                if ((ucRecvBytes[1] == FILTER_NO) || (ucRecvBytes[1] == FILTER_YES))
                {
                    if (ucRecvBytes[1] == FILTER_NO)
                        sys_display_queue_msg("FILTER = NO");
                    else
                        sys_display_queue_msg("FILTER = YES");
                    ee_ConfigColl.ucFilter = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucFilter;
            }
            break;

        case CONFIG_FILTER_TYPE:
            if (WriteVal == TRUE)
            {
                if ((ucRecvBytes[1] >= FILTER_1MM) && (ucRecvBytes[1] < FILTER_NUM_MAX))
                {
                    if (ucRecvBytes[1] == FILTER_1MM)
                    {
                        sys_display_queue_msg("FILTER = 1mm Al");
                        ee_ConfigColl.ucFilterType = MM1_FILTER;
                    }
                    if (ucRecvBytes[1] == FILTER_2MM)
                    {
                        sys_display_queue_msg("FILTER = 2mm Al");
                        ee_ConfigColl.ucFilterType = MM2_FILTER;
                    }
                    if (ucRecvBytes[1] == FILTER_CU)
                    {
                        sys_display_queue_msg("FILTER = 0.3 mm Cu");
                        ee_ConfigColl.ucFilterType = CU_FILTER;
                    }
                    if (ucRecvBytes[1] == FILTER_CUSTOM)
                    {
                        sys_display_queue_msg("FILTER = CUSTOM   ");
                        ee_ConfigColl.ucFilterType = CUSTOM_FILTER;
                    }
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                switch (ee_ConfigColl.ucFilterType)
                {
                case MM1_FILTER:
                    tx_temp.data[1] = FILTER_1MM;
                    break;
                case MM2_FILTER:
                    tx_temp.data[1] = FILTER_2MM;
                    break;
                case CU_FILTER:
                    tx_temp.data[1] = FILTER_CU;
                    break;
                case CUSTOM_FILTER:
                    tx_temp.data[1] = FILTER_CUSTOM;
                    break;
                default:
                    tx_temp.data[1] = 0xFF;
                    break;
                }
            }
            break;

        case CONFIG_AUTOLIGHT:
            if (WriteVal == TRUE)
            {
                if ((ucRecvBytes[1] == AUTOLIGHT_NO) || (ucRecvBytes[1] == AUTOLIGHT_YES))
                {
                    if (ucRecvBytes[1] == AUTOLIGHT_NO)
                        sys_display_queue_msg("AUTO LIGHT = NO");
                    else
                        sys_display_queue_msg("AUTO LIGHT = YES");
                    ee_ConfigColl.ucAutoLight = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucAutoLight;
            }
            break;

        case CONFIG_INCLIN:
            if (WriteVal == TRUE)
            {
                if ((ucRecvBytes[1] == INCLIN_NO) || (ucRecvBytes[1] == INCLIN_YES))
                {
                    if (ucRecvBytes[1] == INCLIN_NO)
                        sys_display_queue_msg("INCLINOMETER = NO");
                    else
                        sys_display_queue_msg("INCLINOMETER = YES");
                    ee_ConfigColl.ucInclinometer = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucInclinometer;
            }
            break;

        case CONFIG_AUTOLIM:
            if (WriteVal == TRUE)
            {
                if ((ucRecvBytes[1] == AUTOLIM_NO) || (ucRecvBytes[1] == AUTOLIM_YES))
                {
                    if (ucRecvBytes[1] == AUTOLIM_NO)
                        sys_display_queue_msg("AUTO LIMITS = NO");
                    else
                        sys_display_queue_msg("AUTO LIMITS = YES");
                    ee_ConfigColl.ucAutoLimits = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucAutoLimits;
            }
            break;

        case CONFIG_DFF_LAT_SX:
            if (WriteVal == TRUE)
            {
                if (ucRecvBytes[1] == DFF_LAT_CAN)
                {
                    sys_display_queue_msg("LATER.DFF SX = CAN  ");
                    ee_ConfigColl.ucLatDffSx = LAT_DFF_CAN;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                if (ucRecvBytes[1] == DFF_LAT_DISC)
                {
                    sys_display_queue_msg("LATER.DFF SX = DISCR");
                    ee_ConfigColl.ucLatDffSx = LAT_DFF_DISC;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                if (ucRecvBytes[1] == DFF_LAT_POT)
                {
                    sys_display_queue_msg("LATER.DFF = POT.    ");
                    ee_ConfigColl.ucLatDffSx = LAT_DFF_POT;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                switch (ee_ConfigColl.ucLatDffSx)
                {
                case LAT_DFF_CAN:
                    tx_temp.data[1] = DFF_LAT_CAN;
                    break;
                case LAT_DFF_DISC:
                    tx_temp.data[1] = DFF_LAT_DISC;
                    break;
                case LAT_DFF_POT:
                    tx_temp.data[1] = DFF_LAT_POT;
                    break;
                }
            }
            break;

        case CONFIG_REC_LAT_SX:
            if (WriteVal == TRUE)
            {
                if (ucRecvBytes[1] == REC_LAT_SX_NO)
                {
                    sys_display_queue_msg("SX LATERAL REC = NO");
                    ee_ConfigColl.ucLatRecSx = LAT_REC_NO;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                if (ucRecvBytes[1] == REC_LAT_SX_CAN)
                {
                    sys_display_queue_msg("SX LATERAL REC = CAN");
                    ee_ConfigColl.ucLatRecSx = LAT_REC_CAN;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                if (ucRecvBytes[1] == REC_LAT_SX_BUCKY)
                {
                    sys_display_queue_msg("SX LATERAL REC=BUCKY");
                    ee_ConfigColl.ucLatRecSx = LAT_REC_BUCKY;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                switch (ee_ConfigColl.ucLatRecSx)
                {
                case LAT_REC_NO:
                    tx_temp.data[1] = REC_LAT_SX_NO;
                    break;
                case LAT_REC_CAN:
                    tx_temp.data[1] = REC_LAT_SX_CAN;
                    break;
                case LAT_REC_BUCKY:
                    tx_temp.data[1] = REC_LAT_SX_BUCKY;
                    break;
                }
            }
            break;

        case CONFIG_REC_LAT_DX:
            if (WriteVal == TRUE)
            {
                if (ucRecvBytes[1] == REC_LAT_DX_NO)
                {
                    sys_display_queue_msg("DX LATERAL REC = NO");
                    ee_ConfigColl.ucLatRecDx = LAT_REC_NO;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                if (ucRecvBytes[1] == REC_LAT_DX_CAN)
                {
                    sys_display_queue_msg("DX LATERAL REC = CAN");
                    ee_ConfigColl.ucLatRecDx = LAT_REC_CAN;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                if (ucRecvBytes[1] == REC_LAT_DX_BUCKY)
                {
                    sys_display_queue_msg("DX LATERAL REC=BUCKY");
                    ee_ConfigColl.ucLatRecDx = LAT_REC_BUCKY;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                switch (ee_ConfigColl.ucLatRecDx)
                {
                case LAT_REC_NO:
                    tx_temp.data[1] = REC_LAT_DX_NO;
                    break;
                case LAT_REC_CAN:
                    tx_temp.data[1] = REC_LAT_DX_CAN;
                    break;
                case LAT_REC_BUCKY:
                    tx_temp.data[1] = REC_LAT_DX_BUCKY;
                    break;
                }
            }
            break;

        case CONFIG_DFF_VERT:
            if (WriteVal == TRUE)
            {
                if (ucRecvBytes[1] == DFF_VERT_CAN)
                {
                    sys_display_queue_msg("VERTICAL DFF = CAN  ");
                    ee_ConfigColl.ucVertDff = VERT_DFF_CAN;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                if (ucRecvBytes[1] == DFF_VERT_FIX)
                {
                    sys_display_queue_msg("VERTICAL DFF = FIXED");
                    ee_ConfigColl.ucVertDff = VERT_DFF_FIXED;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                if (ucRecvBytes[1] == DFF_VERT_SINGLE)
                {
                    sys_display_queue_msg("VERTICAL DFF =SINGLE");
                    ee_ConfigColl.ucVertDff = VERT_DFF_SINGLE;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                if (ucRecvBytes[1] == DFF_VERT_DIFF)
                {
                    sys_display_queue_msg("VERTICAL DFF = DIFF");
                    ee_ConfigColl.ucVertDff = VERT_DFF_DIFF;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                switch (ee_ConfigColl.ucVertDff)
                {
                case VERT_DFF_CAN:
                    tx_temp.data[1] = DFF_VERT_CAN;
                    break;
                case VERT_DFF_FIXED:
                    tx_temp.data[1] = DFF_VERT_FIX;
                    break;
                case VERT_DFF_SINGLE:
                    tx_temp.data[1] = DFF_VERT_SINGLE;
                    break;
                case VERT_DFF_DIFF:
                    tx_temp.data[1] = DFF_VERT_DIFF;
                    break;
                }
            }
            break;

        case CONFIG_REC_VERT:
            if (WriteVal == TRUE)
            {
                if (ucRecvBytes[1] == REC_VERT_NO)
                {
                    sys_display_queue_msg("VERTICAL REC = NO  ");
                    ee_ConfigColl.ucVertRec = VERT_REC_NO;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                if (ucRecvBytes[1] == REC_VERT_CAN)
                {
                    sys_display_queue_msg("VERTICAL REC = CAN  ");
                    ee_ConfigColl.ucVertRec = VERT_REC_CAN;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                if (ucRecvBytes[1] == REC_VERT_ATS)
                {
                    sys_display_queue_msg("VERTICAL REC = ATS  ");
                    ee_ConfigColl.ucVertRec = VERT_REC_ATS;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                if (ucRecvBytes[1] == REC_VERT_BUCKY)
                {
                    sys_display_queue_msg("VERTICAL REC = BUCKY");
                    ee_ConfigColl.ucVertRec = VERT_REC_BUCKY;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                if (ucRecvBytes[1] == REC_VERT_FISSI)
                {
                    sys_display_queue_msg("VERTICAL REC = FIXED");
                    ee_ConfigColl.ucVertRec = VERT_REC_FISSI;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                switch (ee_ConfigColl.ucVertRec)
                {
                case VERT_REC_NO:
                    tx_temp.data[1] = REC_VERT_NO;
                    break;
                case VERT_REC_CAN:
                    tx_temp.data[1] = REC_VERT_CAN;
                    break;
                case VERT_REC_ATS:
                    tx_temp.data[1] = REC_VERT_ATS;
                    break;
                case VERT_REC_BUCKY:
                    tx_temp.data[1] = REC_VERT_BUCKY;
                    break;
                case VERT_REC_FISSI:
                    tx_temp.data[1] = REC_VERT_FISSI;
                    break;
                }
            }
            break;

        case CONFIG_DFF_FIXED:
            if (WriteVal == TRUE)
            {
                uwTemp = (UINT16)ucRecvBytes[2] << 8;
                uwTemp |= (UINT16)ucRecvBytes[1];
                if ((uwTemp >= DFF_MIN) && (uwTemp <= DFF_MAX))
                {
                    sprintf(cbTemp, "%s %d cm", "DFF FIXED = ", uwTemp);
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.uwFixedDff = uwTemp;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.uwFixedDff & 0x00FF;
                tx_temp.data[2] = ee_ConfigColl.uwFixedDff >> 8;
            }
            break;

        case CONFIG_DFF_MAX:
            if (WriteVal == TRUE)
            {
                uwTemp = (UINT16)ucRecvBytes[2] << 8;
                uwTemp |= (UINT16)ucRecvBytes[1];
                if ((uwTemp >= DFF_MIN) && (uwTemp <= DFF_MAX))
                {
                    sprintf(cbTemp, "%s %d cm", "DFF MAX = ", uwTemp);
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.uwMaxDff = uwTemp;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.uwMaxDff & 0x00FF;
                tx_temp.data[2] = ee_ConfigColl.uwMaxDff >> 8;
            }
            break;

        case CONFIG_DFF_MIN:
            if (WriteVal == TRUE)
            {
                uwTemp = (UINT16)ucRecvBytes[2] << 8;
                uwTemp |= (UINT16)ucRecvBytes[1];
                if ((uwTemp >= DFF_MIN) && (uwTemp <= DFF_MAX))
                {
                    sprintf(cbTemp, "%s %d cm", "DFF MIN = ", uwTemp);
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.uwMinDff = uwTemp;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.uwMinDff & 0x00FF;
                tx_temp.data[2] = ee_ConfigColl.uwMinDff >> 8;
            }
            break;

        case CONFIG_DFT_VERT:
            if (WriteVal == TRUE)
            {
                if (ucRecvBytes[1] <= DFT_MAX)
                {
                    sprintf(cbTemp, "%s %d cm", "DFT = ", ucRecvBytes[1]);
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.ucDftVert = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucDftVert;
            }
            break;

        case CONFIG_INCL_AL:
            if (WriteVal == TRUE)
            {
                if ((ucRecvBytes[1] >= AL_INCL_MIN) && (ucRecvBytes[1] <= AL_INCL_MAX))
                {
                    sprintf(cbTemp, "%s %d", "INCL ALARM = ", ucRecvBytes[1]);
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.ucInclDegrees = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucInclDegrees;
            }
            break;

        case CONFIG_FILTER_RTZ:
            if (WriteVal == TRUE)
            {
                if ((ucRecvBytes[1] == FILTER_RTZ_NO) || (ucRecvBytes[1] == FILTER_RTZ_YES))
                {
                    if (ucRecvBytes[1] == FILTER_RTZ_NO)
                        sys_display_queue_msg("FILTER RTZ = NO");
                    else
                        sys_display_queue_msg("FILTER RTZ = YES");
                    ee_ConfigColl.ucFilterCheck = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucFilterCheck;
            }
            break;

        case CONFIG_SHOW_LOCK:
            if (WriteVal == TRUE)
            {
                if ((ucRecvBytes[1] == LOCK_SHOW_NO) || (ucRecvBytes[1] == LOCK_SHOW_YES))
                {
                    if (ucRecvBytes[1] == LOCK_SHOW_NO)
                        sys_display_queue_msg("SHOW LOCK = NO");
                    else
                        sys_display_queue_msg("SHOW LOCK = YES");
                    ee_ConfigColl.ucShowLock = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucShowLock;
            }
            break;

        case CONFIG_SHOW_ANGLE:
            if (WriteVal == TRUE)
            {
                if ((ucRecvBytes[1] == ANGLE_SHOW_NO) || (ucRecvBytes[1] == ANGLE_SHOW_YES))
                {
                    if (ucRecvBytes[1] == ANGLE_SHOW_NO)
                        sys_display_queue_msg("SHOW ANGLE = NO");
                    else
                        sys_display_queue_msg("SHOW ANGLE = YES");
                    ee_ConfigColl.ucShowAngle = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucShowAngle;
            }
            break;

        case CONFIG_SQUARE_RTZ:
            if (WriteVal == TRUE)
            {
                if ((ucRecvBytes[1] == SQUARE_RTZ_NO) || (ucRecvBytes[1] == SQUARE_RTZ_YES))
                {
                    if (ucRecvBytes[1] == SQUARE_RTZ_NO)
                        sys_display_queue_msg("BLADE RESET = NO");
                    else
                        sys_display_queue_msg("BLADE RESET = YES");
                    ee_ConfigColl.bSquareRtz = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.bSquareRtz;
            }
            break;

        case CONFIG_CAL_INCL:
            if (WriteVal == TRUE)
            {
                if ((ucRecvBytes[1] == 0x01) || (ucRecvBytes[1] == 0x02))
                {
                    uwTemp = (UINT16)ucRecvBytes[3] << 8;
                    uwTemp |= (UINT16)ucRecvBytes[4];
                    uwTemp2 = (UINT16)ucRecvBytes[5] << 8;
                    uwTemp2 |= (UINT16)ucRecvBytes[6];
                    switch (ucRecvBytes[2])
                    {
                    case 0:       // verticale
                        if (ucRecvBytes[1] == 0x01)     // acquisizione automatica
                        {
                            ee_OffsetColl.nInclOffsetX = g_nFilteredAdc[ADXL_X];    // acquisizione
                            ee_OffsetColl.nInclOffsetY = g_nFilteredAdc[ADXL_Y];
                        }
                        else
                        {
                            ee_OffsetColl.nInclOffsetX = uwTemp;        // scrittura diretta del dato
                            ee_OffsetColl.nInclOffsetY = uwTemp2;
                        }
                        sprintf(cbTemp, "%s%d %s%d", "  0  X=", ee_OffsetColl.nInclOffsetX, "Y=", ee_OffsetColl.nInclOffsetY);
                        sys_display_queue_msg(cbTemp);
                        s_ulUpdateEeprom |= EEP_UPDATE_OFFSET;
                        break;
                    case 1:       // guarda verso destra
                        if (ucRecvBytes[1] == 0x01)     // acquisizione automatica
                        {
                            ee_OffsetColl.InclOffsetDxX = g_nFilteredAdc[ADXL_X];
                            ee_OffsetColl.InclOffsetDxY = g_nFilteredAdc[ADXL_Y];
                        }
                        else
                        {
                            ee_OffsetColl.InclOffsetDxX = uwTemp;        // scrittura diretta del dato
                            ee_OffsetColl.InclOffsetDxY = uwTemp2;
                        }
                        sprintf(cbTemp, "%s%d %s%d", "-90R X=", ee_OffsetColl.InclOffsetDxX, "Y=", ee_OffsetColl.InclOffsetDxY);
                        sys_display_queue_msg(cbTemp);
                        s_ulUpdateEeprom |= EEP_UPDATE_OFFSET;
                        break;
                    case 2:       // guarda verso sinistra
                        if (ucRecvBytes[1] == 0x01)     // acquisizione automatica
                        {
                            ee_OffsetColl.InclOffsetSxX = g_nFilteredAdc[ADXL_X];
                            ee_OffsetColl.InclOffsetSxY = g_nFilteredAdc[ADXL_Y];
                        }
                        else
                        {
                            ee_OffsetColl.InclOffsetSxX = uwTemp;        // scrittura diretta del dato
                            ee_OffsetColl.InclOffsetSxY = uwTemp2;
                        }
                        sprintf(cbTemp, "%s%d %s%d", "+90L X=", ee_OffsetColl.InclOffsetSxX, "Y=", ee_OffsetColl.InclOffsetSxY);
                        sys_display_queue_msg(cbTemp);
                        s_ulUpdateEeprom |= EEP_UPDATE_OFFSET;
                        break;
                    default:
                        sys_display_queue_msg("Command Error");
                        break;
                    }
                }
                else
                {
                    sys_display_queue_msg("Command Error");
                }
            }
            else
            {
                if ((ucRecvBytes[1] == 0x01) || (ucRecvBytes[1] == 0x02))
                {
                    tx_temp.data[1] = ucRecvBytes[1];
                tx_temp.data[2] = ucRecvBytes[2];
                switch (ucRecvBytes[2])
                {
                case 0:       // verticale
                    tx_temp.data[3] = ee_OffsetColl.nInclOffsetX >> 8;
                    tx_temp.data[4] = ee_OffsetColl.nInclOffsetX & 0x00FF;
                    tx_temp.data[5] = ee_OffsetColl.nInclOffsetY >> 8;
                    tx_temp.data[6] = ee_OffsetColl.nInclOffsetY & 0x00FF;
                    break;
                case 1:       // guarda verso destra
                    tx_temp.data[3] = ee_OffsetColl.InclOffsetDxX >> 8;
                    tx_temp.data[4] = ee_OffsetColl.InclOffsetDxX & 0x00FF;
                    tx_temp.data[5] = ee_OffsetColl.InclOffsetDxY >> 8;
                    tx_temp.data[6] = ee_OffsetColl.InclOffsetDxY & 0x00FF;
                    break;
                case 2:       // guarda verso sinistra
                    tx_temp.data[3] = ee_OffsetColl.InclOffsetSxX >> 8;
                    tx_temp.data[4] = ee_OffsetColl.InclOffsetSxX & 0x00FF;
                    tx_temp.data[5] = ee_OffsetColl.InclOffsetSxY >> 8;
                    tx_temp.data[6] = ee_OffsetColl.InclOffsetSxY & 0x00FF;
                    break;
                default:
                    tx_temp.data[3] = 0xFF;
                    tx_temp.data[4] = 0xFF;
                    tx_temp.data[5] = 0xFF;
                    tx_temp.data[6] = 0xFF;
                    break;
                }
            }
                else
                {
                    tx_temp.data[1] = 0xFF;
                }
            }
            break;

        case CONFIG_MAN_DELTA_CROSS:
            if (WriteVal == TRUE)
            {
                if (ucRecvBytes[1] >= DELTA_CROSS_MIN && ucRecvBytes[1] <= DELTA_CROSS_MAX)
                {
                    sprintf(cbTemp, "%s %d mm", "MOV.CROSS =", ucRecvBytes[1]);
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.ucDeltaCrossOpenCloseMan = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucDeltaCrossOpenCloseMan;
            }
            break;

        case CONFIG_MAN_DELTA_LONG:
            if (WriteVal == TRUE)
            {
                if (ucRecvBytes[1] >= DELTA_LONG_MIN && ucRecvBytes[1] <= DELTA_LONG_MAX)
                {
                    sprintf(cbTemp, "%s %d mm", "MOV.LONG =", ucRecvBytes[1]);
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.ucDeltaLongOpenCloseMan = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucDeltaLongOpenCloseMan;
            }
            break;

        case CONFIG_CROSS_DFF:
            if (WriteVal == TRUE)
            {
                if (ee_ConfigColl.CorrType == TIPO_CORR_CM)
                {
                    scTemp=(schar)ucRecvBytes[1];
                    if (((scTemp>=0)&&(scTemp<=DELTA_DFF_MAX_POS))||
                        ((scTemp<0)&&(scTemp>=DELTA_DFF_MAX_NEG))){
                        sprintf(cbTemp, "%s %d cm", "CORR.SID CROSS=", scTemp);
                        sys_display_queue_msg(cbTemp);
                        ee_ConfigColl.scCorrCrossDff=scTemp;
                        s_ulUpdateEeprom|=EEP_UPDATE_COLLIMATOR;
                        break;
                    }
                }
                else
                {
                    scTemp = (schar) ucRecvBytes[1];
                    if (((scTemp >= 0) && (scTemp <= DELTA_PERC_MAX_POS)) ||
                        ((scTemp < 0) && (scTemp >= DELTA_PERC_MAX_NEG)))
                    {
                        t1 = scTemp / 10;
                        t2 = scTemp % 10;
                        c = ' ';
                        if (scTemp < 0)
                        {
                            c = '-';
                            t1 = -t1;
                            t2 = -t2;
                        }
                        sprintf(cbTemp, "%s %c%01d.%01d %%", "CORR.% CROSS =", c, t1, t2);
                        sys_display_queue_msg(cbTemp);
                        ee_ConfigColl.scCorrCrossDff = scTemp;
                        s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                        break;
                    }
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.scCorrCrossDff;
            }
            break;

        case CONFIG_LONG_DFF:
            if (WriteVal == TRUE)
            {
                if (ee_ConfigColl.CorrType == TIPO_CORR_CM)
                {
                    scTemp=(schar)ucRecvBytes[1];
                    if (((scTemp>=0)&&(scTemp<=DELTA_DFF_MAX_POS))||
                        ((scTemp<0)&&(scTemp>=DELTA_DFF_MAX_NEG))){
                        sprintf(cbTemp, "%s %d cm", "CORR.SID LONG=", scTemp);
                        sys_display_queue_msg(cbTemp);
                        ee_ConfigColl.scCorrLongDff=scTemp;
                        s_ulUpdateEeprom|=EEP_UPDATE_COLLIMATOR;
                        break;
                    }
                }
                else
                {
                    scTemp = (schar) ucRecvBytes[1];
                    if (((scTemp >= 0) && (scTemp <= DELTA_PERC_MAX_POS)) ||
                        ((scTemp < 0) && (scTemp >= DELTA_PERC_MAX_NEG)))
                    {
                        t1 = scTemp / 10;
                        t2 = scTemp % 10;
                        c = ' ';
                        if (scTemp < 0)
                        {
                            c = '-';
                            t1 = -t1;
                            t2 = -t2;
                        }
                        sprintf(cbTemp, "%s %c%01d.%01d %%", "CORR.% LONG =", c, t1, t2);
                        sys_display_queue_msg(cbTemp);
                        ee_ConfigColl.scCorrLongDff = scTemp;
                        s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                        break;
                    }
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.scCorrLongDff;
            }
            break;

        case CONFIG_DISPLAY_MODE:
            if (WriteVal == TRUE)
            {
                switch (ucRecvBytes[1])
                {
                    //case DISPLAY_NONE:
                    //  sprintf(cbTemp, "DISPLAY=NO");  //Disabled for safety reason
                    //  break;
                case DISPLAY_STANDARD:
                    sprintf(cbTemp, "DISPLAY=STANDARD");
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.ucDisplayType = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                case DISPLAY_SEDECAL:
                    sprintf(cbTemp, "DISPLAY=SEDECAL");
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.ucDisplayType = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                case DISPLAY_FREE:
                    sprintf(cbTemp, "DISPLAY=FREE");
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.ucDisplayType = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                default:
                    sys_display_queue_msg("Command Error");
                    break;
                }
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucDisplayType;
            }
            break;

        case CONFIG_CROSS_MAX:
            if (WriteVal == TRUE)
            {
                uwTemp = (uword) ucRecvBytes[1] << 8;
                uwTemp |= ucRecvBytes[2];
                if (uwTemp > MAX_CROSS)
                    uwTemp = MAX_CROSS;
                sprintf(cbTemp, "%s %d mm", "MAX CROSS=", uwTemp);
                sys_display_queue_msg(cbTemp);
                ee_OffsetColl.nMaxCross = uwTemp;
                s_ulUpdateEeprom |= EEP_UPDATE_OFFSET;
            }
            else
            {
                tx_temp.data[1] = ee_OffsetColl.nMaxCross >> 8;
                tx_temp.data[2] = ee_OffsetColl.nMaxCross & 0x00FF;
            }
            break;

        case CONFIG_LONG_MAX:
            if (WriteVal == TRUE)
            {
                uwTemp = (uword) ucRecvBytes[1] << 8;
                uwTemp |= ucRecvBytes[2];
                if (uwTemp > MAX_LONG)
                    uwTemp = MAX_LONG;
                sprintf(cbTemp, "%s %d mm", "MAX LONG=", uwTemp);
                sys_display_queue_msg(cbTemp);
                ee_OffsetColl.nMaxLong = uwTemp;
                s_ulUpdateEeprom |= EEP_UPDATE_OFFSET;
            }
            else
            {
                tx_temp.data[1] = ee_OffsetColl.nMaxLong >> 8;
                tx_temp.data[2] = ee_OffsetColl.nMaxLong & 0x00FF;
            }
            break;

        case CONFIG_ENC_SENS:
            if (WriteVal == TRUE)
            {
                ucTemp = (schar) ucRecvBytes[1];
                if (ucTemp > 0xFA)
                    ucTemp = 0xFA;
                sprintf(cbTemp, "%s %d ms", "ENCOD SENS=", ucTemp);
                sys_display_queue_msg(cbTemp);
                ee_OffsetColl.ucEncoderSens = ucTemp;
                s_ulUpdateEeprom |= EEP_UPDATE_OFFSET;
            }
            else
            {
                tx_temp.data[1] = ee_OffsetColl.ucEncoderSens;
            }
            break;

        case CONFIG_ENC_INV:
            if (WriteVal == TRUE)
            {
                ucTemp = (schar) ucRecvBytes[1];
                if (ucTemp > 0xFA)
                    ucTemp = 0xFA;
                sprintf(cbTemp, "%s %d ms", "ENCOD INV=", ucTemp);
                sys_display_queue_msg(cbTemp);
                ee_OffsetColl.ucEncoderInv = ucTemp;
                s_ulUpdateEeprom |= EEP_UPDATE_OFFSET;
            }
            else
            {
                tx_temp.data[1] = ee_OffsetColl.ucEncoderInv;
            }
            break;

        case CONFIG_SQUARE_INV:
            if (WriteVal == TRUE)
            {
                if (ucRecvBytes[1] == OK)
                {
                    sys_display_queue_msg("CROSS/LONG ROTATED");
                    ee_OffsetColl.bSquareRotated = true;
                    s_ulUpdateEeprom |= EEP_UPDATE_OFFSET;
                    break;
                }
                if (ucRecvBytes[1] == NOK)
                {
                    sys_display_queue_msg("CROSS/LONG NORMAL");
                    ee_OffsetColl.bSquareRotated = false;
                    s_ulUpdateEeprom |= EEP_UPDATE_OFFSET;
                    break;
                }
                if ((ucRecvBytes[1] != OK) && (ucRecvBytes[1] != NOK))
                    sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_OffsetColl.bSquareRotated;
            }
            break;

        case CONFIG_CAN_PROTO:
            if (WriteVal == TRUE)
            {
                switch (ucRecvBytes[1])
                {
                case CAN_STANDARD:
                    sprintf(cbTemp, "CAN PROTO=STANDARD");
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.ucCanProtocol = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                case CAN_SEDECAL:
                    sprintf(cbTemp, "CAN PROTO=SEDECAL");
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.ucCanProtocol = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                case CAN_GMM:
                    sprintf(cbTemp, "CAN PROTO=GMM");
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.ucCanProtocol = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                default:
                    sys_display_queue_msg("Command Error");
                    break;
                }
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucCanProtocol;
            }
            break;

        case CONFIG_CAN_SPEED:
            if (WriteVal == TRUE)
            {
                switch (ucRecvBytes[1])
                {
                case CAN_1000K:
                    sprintf(cbTemp, "CAN SPEED=1MBit/s");
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.ucCanSpeed = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                case CAN_500K:
                    sprintf(cbTemp, "CAN SPEED=500kBit/s");
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.ucCanSpeed = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                case CAN_250K:
                    sprintf(cbTemp, "CAN SPEED=250kBit/s");
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.ucCanSpeed = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                case CAN_125K:
                    sprintf(cbTemp, "CAN SPEED=125kBit/s");
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.ucCanSpeed = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                case CAN_100K:
                    sprintf(cbTemp, "CAN SPEED=100kBit/s");
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.ucCanSpeed = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                case CAN_50K:
                    sprintf(cbTemp, "CAN SPEED=50kBit/s");
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.ucCanSpeed = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                case CAN_20K:
                    sprintf(cbTemp, "CAN SPEED=20kBit/s");
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.ucCanSpeed = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                case CAN_10K:
                    sprintf(cbTemp, "CAN SPEED=10kBit/s");
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.ucCanSpeed = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                default:
                    sys_display_queue_msg("Command Error");
                    break;
                }
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucCanSpeed;
            }
            break;

        case CONFIG_FILTER_STEPS:
            if (WriteVal == TRUE)
            {
                uwTemp = (uword) ucRecvBytes[1] << 8;
                uwTemp |= ucRecvBytes[2];
                if (uwTemp < FILTER_STEPS_MIN)
                    uwTemp = FILTER_STEPS_MIN;
                if (uwTemp > FILTER_STEPS_MAX)
                    uwTemp = FILTER_STEPS_MAX;
                sprintf(cbTemp, "%s %d", "FILTER STEPS=", uwTemp);
                sys_display_queue_msg(cbTemp);
                ee_FilterConfig.uwSteps = uwTemp;
                s_ulUpdateEeprom |= EEP_UPDATE_FILTER;
            }
            else
            {
                tx_temp.data[1] = ee_FilterConfig.uwSteps >> 8;
                tx_temp.data[2] = ee_FilterConfig.uwSteps & 0x00FF;
            }
            break;

        case CONFIG_IN_ANALOG:
            if (WriteVal == TRUE)
            {
                switch (ucRecvBytes[1])
                {
                case DIGITAL_IN:
                    sprintf(cbTemp, "DIGITAL ENABLE");
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.ucAnalogInput = DIGITAL_IN;
                    ;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                case ANALOG_IN:
                    sprintf(cbTemp, "DIGITAL DISABLE");
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.ucAnalogInput = ANALOG_IN;
                    ;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                default:
                    sys_display_queue_msg("Command Error");
                    break;
                }
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucAnalogInput;
            }
            break;

        case CONFIG_IN_ADD_CMD:
            if (WriteVal == TRUE)
            {
                uwTemp = (uword) ucRecvBytes[1] << 8;
                uwTemp |= ucRecvBytes[2];
                if (uwTemp > MAX_ADD)
                    uwTemp = MAX_ADD;
                sprintf(cbTemp, "%s %x ", "CMD ADDRESS=", uwTemp);
                sys_display_queue_msg(cbTemp);
                ee_CanColl.unIdCommand = uwTemp;
                s_ulUpdateEeprom |= EEP_UPDATE_CAN;
            }
            else
            {
                tx_temp.data[1] = ee_CanColl.unIdCommand >> 8;
                tx_temp.data[2] = ee_CanColl.unIdCommand & 0x00FF;
            }
            break;

        case CONFIG_SPEED_FAST:
            if (WriteVal == TRUE)
            {
                uwTemp = (uword) ucRecvBytes[1] << 8;
                uwTemp |= ucRecvBytes[2];
                if (uwTemp < FREQ_SET_MIN)
                    uwTemp = FREQ_SET_MIN;
                if (uwTemp > FREQ_SET_MAX)
                    uwTemp = FREQ_SET_MAX;
                sprintf(cbTemp, "%s %d", "SPEED FAST=", uwTemp);
                sys_display_queue_msg(cbTemp);
                ee_ConfigBoards[STEPPER_A].uwFMaxFast = uwTemp;
                ee_ConfigBoards[STEPPER_B].uwFMaxFast = uwTemp;
                ee_ConfigBoards[STEPPER_D].uwFMaxFast = uwTemp;
                Steppers[STEPPER_A].uwFMaxFast = uwTemp;
                Steppers[STEPPER_B].uwFMaxFast = uwTemp;
                Steppers[STEPPER_D].uwFMaxFast = uwTemp;
                s_ulUpdateEeprom |= EEP_UPDATE_DSPEED;
            }
            else
            {
                tx_temp.data[1] = ee_ConfigBoards[STEPPER_A].uwFMaxFast >> 8;
                tx_temp.data[2] = ee_ConfigBoards[STEPPER_A].uwFMaxFast & 0x00FF;
            }
            break;

        case CONFIG_CROSS_REF:
            if (WriteVal == TRUE)
            {
                uwTemp = (uword) ucRecvBytes[1] << 8;
                uwTemp |= ucRecvBytes[2];
                uwTemp2 = (uword) ucRecvBytes[3] << 8;
                uwTemp2 |= ucRecvBytes[4];
                uwTemp3 = (uword) ucRecvBytes[5] << 8;
                uwTemp3 |= ucRecvBytes[6];
                sprintf(cbTemp, "%d %d %d", uwTemp, uwTemp2, uwTemp3);
                sys_display_queue_msg(cbTemp);
                ee_ConfigBoards[STEPPER_A].uwMaxSteps = uwTemp; // passi massimi da tutto chiuso a tutto aperto
                ee_ConfigBoards[STEPPER_A].uwRefSize = uwTemp2; // formato di riferimento in mm
                ee_ConfigBoards[STEPPER_A].uwRefSteps = uwTemp3; // formato di riferimento in passi
                ee_WindowConfig[WINDOW_2].lPosStepperA = uwTemp;
                g_MaxSteps[STEPPER_CROSS] = uwTemp;
                g_RefSize[STEPPER_CROSS] = uwTemp2;
                g_RefSteps[STEPPER_CROSS] = uwTemp3;
                s_ulUpdateEeprom |= EEP_UPDATE_DSPEED;
                s_ulUpdateEeprom |= EEP_UPDATE_WINDOWS;
            }
            else
            {
                tx_temp.data[1] = ee_ConfigBoards[STEPPER_A].uwMaxSteps >> 8;
                tx_temp.data[2] = ee_ConfigBoards[STEPPER_A].uwMaxSteps & 0x00FF;
                tx_temp.data[3] = ee_ConfigBoards[STEPPER_A].uwRefSize >> 8;
                tx_temp.data[4] = ee_ConfigBoards[STEPPER_A].uwRefSize & 0x00FF;
                tx_temp.data[5] = ee_ConfigBoards[STEPPER_A].uwRefSteps >> 8;
                tx_temp.data[6] = ee_ConfigBoards[STEPPER_A].uwRefSteps & 0x00FF;
            }
            break;

        case CONFIG_LONG_REF:
            if (WriteVal == TRUE)
            {
                uwTemp = (uword) ucRecvBytes[1] << 8;
                uwTemp |= ucRecvBytes[2];
                uwTemp2 = (uword) ucRecvBytes[3] << 8;
                uwTemp2 |= ucRecvBytes[4];
                uwTemp3 = (uword) ucRecvBytes[5] << 8;
                uwTemp3 |= ucRecvBytes[6];
                sprintf(cbTemp, "%d %d %d", uwTemp, uwTemp2, uwTemp3);
                sys_display_queue_msg(cbTemp);
                ee_ConfigBoards[STEPPER_B].uwMaxSteps = uwTemp; // passi massimi da tutto chiuso a tutto aperto
                ee_ConfigBoards[STEPPER_B].uwRefSize = uwTemp2; // formato di riferimento in mm
                ee_ConfigBoards[STEPPER_B].uwRefSteps = uwTemp3; // formato di riferimento in passi
                ee_ConfigBoards[STEPPER_D].uwMaxSteps = uwTemp; // passi massimi da tutto chiuso a tutto aperto
                ee_ConfigBoards[STEPPER_D].uwRefSize = uwTemp2; // formato di riferimento in mm
                ee_ConfigBoards[STEPPER_D].uwRefSteps = uwTemp3; // formato di riferimento in passi
                ee_WindowConfig[WINDOW_2].lPosStepperB = uwTemp;
                g_MaxSteps[STEPPER_LONG] = uwTemp;
                g_RefSize[STEPPER_LONG] = uwTemp2;
                g_RefSteps[STEPPER_LONG] = uwTemp3;
                g_MaxSteps[STEPPER_LONG2] = uwTemp;
                g_RefSize[STEPPER_LONG2] = uwTemp2;
                g_RefSteps[STEPPER_LONG2] = uwTemp3;
                s_ulUpdateEeprom |= EEP_UPDATE_DSPEED;
                s_ulUpdateEeprom |= EEP_UPDATE_WINDOWS;
            }
            else
            {
                tx_temp.data[1] = ee_ConfigBoards[STEPPER_B].uwMaxSteps >> 8;
                tx_temp.data[2] = ee_ConfigBoards[STEPPER_B].uwMaxSteps & 0x00FF;
                tx_temp.data[3] = ee_ConfigBoards[STEPPER_B].uwRefSize >> 8;
                tx_temp.data[4] = ee_ConfigBoards[STEPPER_B].uwRefSize & 0x00FF;
                tx_temp.data[5] = ee_ConfigBoards[STEPPER_B].uwRefSteps >> 8;
                tx_temp.data[6] = ee_ConfigBoards[STEPPER_B].uwRefSteps & 0x00FF;
            }
            break;

        case CONFIG_MSG_ROT:
            if (WriteVal == TRUE)
            {
                uwTemp = (uword) ucRecvBytes[1] << 8;
                uwTemp |= ucRecvBytes[2];
                uwTemp &= 0xFFF0;
                if (uwTemp > MAX_ADD)
                    uwTemp = MAX_ADD;
                uwTemp &= 0xFFF0;
                sprintf(cbTemp, "%s %x ", "ROT ADDRESS=", uwTemp);
                sys_display_queue_msg(cbTemp);
                ee_CanColl.unIdRot = uwTemp;
                s_ulUpdateEeprom |= EEP_UPDATE_CAN;
            }
            else
            {
                tx_temp.data[1] = ee_CanColl.unIdRot >> 8;
                tx_temp.data[2] = ee_CanColl.unIdRot & 0x00FF;
            }
            break;

        case CONFIG_FILTER_HOLES:
            if (WriteVal == TRUE)
            {
                switch (ucRecvBytes[1])
                {
                case FILTER_1_HOLE:
                    sprintf(cbTemp, "FILTER 1 HOLE");
                    sys_display_queue_msg(cbTemp);
                    ee_FilterConfig.uwType = (UINT16) ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_FILTER;
                    break;
                case FILTER_5_HOLES:
                    sprintf(cbTemp, "FILTER 5 HOLES");
                    sys_display_queue_msg(cbTemp);
                    ee_FilterConfig.uwType = (UINT16) ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_FILTER;
                    break;
                default:
                    sys_display_queue_msg("Command Error");
                    break;
                }
            }
            else
            {
                tx_temp.data[1] = ee_FilterConfig.uwType;
            }
            break;

        case CONFIG_HARDWARE:
            if (WriteVal == TRUE)
            {
                if ((ucRecvBytes[1] == HARDWARE_NO_POT) || (ucRecvBytes[1] == HARDWARE_YES_POT))
                {
                    if (ucRecvBytes[1] == HARDWARE_NO_POT)
                        sys_display_queue_msg("POTENTIOM. = NO");
                    else
                        sys_display_queue_msg("POTENTIOM. = YES");
                    ee_ConfigColl.ucHardwareType = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucHardwareType;
            }
            break;

        case CONFIG_POT_OPEN:
            if (WriteVal == TRUE)
            {
                if (ucRecvBytes[1] == 0x01) // per conferma
                {
                    sprintf(cbTemp, "SET POT OPEN");
                    sys_display_queue_msg(cbTemp);
              //      ee_ConfigColl.uwPotCross[NUM_TAR_POT - 1] = g_nFilteredAdc[POT_CROSS];
              //     ee_ConfigColl.uwPotLong[NUM_TAR_POT - 1] = g_nFilteredAdc[POT_LONG];
              //    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                }
            }
            else
            {
                __asm(" nop");
            }
            break;

        case CONFIG_POT_CLOSE:
            if (WriteVal == TRUE)
            {
                if (ucRecvBytes[1] == 0x01) // per conferma
                {
                    sprintf(cbTemp, "SET POT CLOSE");
                    sys_display_queue_msg(cbTemp);
              //      ee_ConfigColl.uwPotCross[0] = g_nFilteredAdc[POT_CROSS];
              //      ee_ConfigColl.uwPotLong[0] = g_nFilteredAdc[POT_LONG];
              //      s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                }
            }
            else
            {
               __asm(" nop");
            }
            break;

        case CONFIG_0x100:
            if (WriteVal == TRUE)
            {
                if ((ucRecvBytes[1] == ALARM_0x100_NO) || (ucRecvBytes[1] == ALARM_0x100_YES))
                {
                    if (ucRecvBytes[1] == ALARM_0x100_NO)
                        sys_display_queue_msg("ALARM on 0x100 = NO");
                    else
                        sys_display_queue_msg("ALARM on 0x100 = YES");
                    ee_ConfigColl.ucAlarm_0x100 = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucAlarm_0x100;
            }
            break;

        case CONFIG_ROTAZIONE:
            if (WriteVal == TRUE)
            {
                if ((ucRecvBytes[1] == ROTAZIONE_NO) || (ucRecvBytes[1] == ROTAZIONE_YES))
                {
                    if (ucRecvBytes[1] == ROTAZIONE_NO)
                        sys_display_queue_msg("ROTATION = NO");
                    else
                        sys_display_queue_msg("ROTATION = YES");
                    ee_ConfigColl.ucRotation = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucRotation;
            }
            break;

        case CONFIG_IRIDE:
            if (WriteVal == TRUE)
            {
                if ((ucRecvBytes[1] == IRIDE_NO) || (ucRecvBytes[1] == IRIDE_YES))
                {
                    if (ucRecvBytes[1] == IRIDE_NO)
                    {
                        sys_display_queue_msg("IRIS = NO");
                        ee_ConfigBoards[STEPPER_A].uwMaxSteps = DEF_PASSI_A_STD; // passi massimi da tutto chiuso a tutto aperto
                        ee_ConfigBoards[STEPPER_A].uwRefSize = DEF_SIZE_A_STD; // formato di riferimento in mm
                        ee_ConfigBoards[STEPPER_A].uwRefSteps = DEF_PASSI_A_STD; // formato di riferimento in passi
                        ee_WindowConfig[WINDOW_2].lPosStepperA = DEF_PASSI_A_STD;
                        g_MaxSteps[STEPPER_CROSS] = DEF_PASSI_A_STD;
                        g_RefSize[STEPPER_CROSS] = DEF_SIZE_A_STD;
                        g_RefSteps[STEPPER_CROSS] = DEF_PASSI_A_STD;
                        ee_ConfigBoards[STEPPER_B].uwMaxSteps = DEF_PASSI_B_STD; // passi massimi da tutto chiuso a tutto aperto
                        ee_ConfigBoards[STEPPER_B].uwRefSize = DEF_SIZE_B_STD; // formato di riferimento in mm
                        ee_ConfigBoards[STEPPER_B].uwRefSteps = DEF_PASSI_B_STD; // formato di riferimento in passi
                        ee_WindowConfig[WINDOW_2].lPosStepperB = DEF_PASSI_B_STD;
                        g_MaxSteps[STEPPER_LONG] = DEF_PASSI_B_STD;
                        g_RefSize[STEPPER_LONG] = DEF_SIZE_B_STD;
                        g_RefSteps[STEPPER_LONG] = DEF_PASSI_B_STD;
                        ee_ConfigBoards[STEPPER_D].uwMaxSteps = DEF_PASSI_B_STD; // passi massimi da tutto chiuso a tutto aperto
                        ee_ConfigBoards[STEPPER_D].uwRefSize = DEF_SIZE_B_STD; // formato di riferimento in mm
                        ee_ConfigBoards[STEPPER_D].uwRefSteps = DEF_PASSI_B_STD; // formato di riferimento in passi
                        ee_WindowConfig[WINDOW_2].lPosStepperB = DEF_PASSI_B_STD;
                        g_MaxSteps[STEPPER_LONG2] = DEF_PASSI_B_STD;
                        g_RefSize[STEPPER_LONG2] = DEF_SIZE_B_STD;
                        g_RefSteps[STEPPER_LONG2] = DEF_PASSI_B_STD;
                        s_ulUpdateEeprom |= EEP_UPDATE_DSPEED;
                        s_ulUpdateEeprom |= EEP_UPDATE_WINDOWS;
                    }
                    else
                    {
                        sys_display_queue_msg("IRIS = YES");
                        ee_ConfigBoards[STEPPER_A].uwMaxSteps = DEF_PASSI_A_IRIDE; // passi massimi da tutto chiuso a tutto aperto
                        ee_ConfigBoards[STEPPER_A].uwRefSize = DEF_SIZE_A_IRIDE; // formato di riferimento in mm
                        ee_ConfigBoards[STEPPER_A].uwRefSteps = DEF_PASSI_A_IRIDE; // formato di riferimento in passi
                        ee_WindowConfig[WINDOW_2].lPosStepperA = DEF_PASSI_A_IRIDE;
                        g_MaxSteps[STEPPER_CROSS] = DEF_PASSI_A_IRIDE;
                        g_RefSize[STEPPER_CROSS] = DEF_SIZE_A_IRIDE;
                        g_RefSteps[STEPPER_CROSS] = DEF_PASSI_A_IRIDE;
                        ee_ConfigBoards[STEPPER_B].uwMaxSteps = DEF_PASSI_B_IRIDE; // passi massimi da tutto chiuso a tutto aperto
                        ee_ConfigBoards[STEPPER_B].uwRefSize = DEF_SIZE_B_IRIDE; // formato di riferimento in mm
                        ee_ConfigBoards[STEPPER_B].uwRefSteps = DEF_PASSI_B_IRIDE; // formato di riferimento in passi
                        ee_WindowConfig[WINDOW_2].lPosStepperB = DEF_PASSI_B_IRIDE;
                        g_MaxSteps[STEPPER_LONG] = DEF_PASSI_B_IRIDE;
                        g_RefSize[STEPPER_LONG] = DEF_SIZE_B_IRIDE;
                        g_RefSteps[STEPPER_LONG] = DEF_PASSI_B_IRIDE;
                        ee_ConfigBoards[STEPPER_D].uwMaxSteps = DEF_PASSI_B_IRIDE; // passi massimi da tutto chiuso a tutto aperto
                        ee_ConfigBoards[STEPPER_D].uwRefSize = DEF_SIZE_B_IRIDE; // formato di riferimento in mm
                        ee_ConfigBoards[STEPPER_D].uwRefSteps = DEF_PASSI_B_IRIDE; // formato di riferimento in passi
                        ee_WindowConfig[WINDOW_2].lPosStepperB = DEF_PASSI_B_IRIDE;
                        g_MaxSteps[STEPPER_LONG2] = DEF_PASSI_B_IRIDE;
                        g_RefSize[STEPPER_LONG2] = DEF_SIZE_B_IRIDE;
                        g_RefSteps[STEPPER_LONG2] = DEF_PASSI_B_IRIDE;
                        s_ulUpdateEeprom |= EEP_UPDATE_DSPEED;
                        s_ulUpdateEeprom |= EEP_UPDATE_WINDOWS;
                    }
                    ee_ConfigColl.ucIrisPresent = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucIrisPresent;
            }
            break;


        case CONFIG_MSG_IRIDE:
            if (WriteVal == TRUE)
            {
                uwTemp = (uword) ucRecvBytes[1] << 8;
                uwTemp |= ucRecvBytes[2];
                uwTemp &= 0xFFF0;
                if (uwTemp > MAX_ADD)
                    uwTemp = MAX_ADD;
                uwTemp &= 0xFFF0;
                sprintf(cbTemp, "%s %x ", "IRIS ADDRESS=", uwTemp);
                sys_display_queue_msg(cbTemp);
                ee_CanColl.unIdIride = uwTemp;
                s_ulUpdateEeprom |= EEP_UPDATE_CAN;
            }
            else
            {
                tx_temp.data[1] = ee_CanColl.unIdIride >> 8;
                tx_temp.data[2] = ee_CanColl.unIdIride & 0x00FF;
            }
            break;

        case CONFIG_TIPO_PROX:
            if (WriteVal == TRUE)
            {
                if ((ucRecvBytes[1] == TIPO_PROX_PNP) || (ucRecvBytes[1] == TIPO_PROX_NPN))
                {
                    if (ucRecvBytes[1] == TIPO_PROX_PNP)
                        sys_display_queue_msg("PROX. TYPE = PNP");
                    else
                        sys_display_queue_msg("PROX. TYPE = NPN");
                    ee_ConfigColl.ucTipoProx = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucTipoProx;
            }
            break;

        case CONFIG_FIXED_FORMAT:
            if (WriteVal == TRUE)
            {
                ucTemp = 0;
                switch (ucRecvBytes[1]) // sotto-comando
                {
                case COM_FIXED_NUM:
                    if (ucRecvBytes[2] <= 5)
                    {
                        ee_FixFormatColl.ucNumFormats = ucRecvBytes[2];
                        sprintf(cbTemp, "N.FORMATS = %d ", ucRecvBytes[2]);
                        sys_display_queue_msg(cbTemp);
                        ucTemp = 1;
                    }
                    break;
                case COM_FIXED_CM:
                    if ((ucRecvBytes[2] >= 1) && (ucRecvBytes[2] <= 5))
                    {
                        if ((ucRecvBytes[3] <= 43) && (ucRecvBytes[4] <= 43))
                        {
                            ee_FixFormatColl.ucFixedCmCross[ucRecvBytes[2] - 1] = ucRecvBytes[3];
                            ee_FixFormatColl.ucFixedCmLong[ucRecvBytes[2] - 1] = ucRecvBytes[4];
                            sprintf(cbTemp, "FORMAT %d = %dx%d cm", ucRecvBytes[2], ucRecvBytes[3], ucRecvBytes[4]);
                            sys_display_queue_msg(cbTemp);
                            ucTemp = 1;
                        }
                    }
                    break;
                case COM_FIXED_INCH:
                    if ((ucRecvBytes[2] >= 1) && (ucRecvBytes[2] <= 5))
                    {
                        if ((ucRecvBytes[3] <= 17) && (ucRecvBytes[4] <= 17))
                        {
                            ee_FixFormatColl.ucFixedInCross[ucRecvBytes[2] - 1] = ucRecvBytes[3];
                            ee_FixFormatColl.ucFixedInLong[ucRecvBytes[2] - 1] = ucRecvBytes[4];
                            sprintf(cbTemp, "FORMAT %d=%d\"x%d\"", ucRecvBytes[2], ucRecvBytes[3], ucRecvBytes[4]);
                            sys_display_queue_msg(cbTemp);
                            ucTemp = 1;
                        }
                    }
                    break;
                case COM_FIXED_NUM_IRIS:
                    if (ucRecvBytes[2] <= 5)
                    {
                        ee_FixFormatColl.ucNumFormatsIris = ucRecvBytes[2];
                        sprintf(cbTemp, "N.IRIS FORMATS=%d ", ucRecvBytes[2]);
                        sys_display_queue_msg(cbTemp);
                        ucTemp = 1;
                    }
                    break;
                case COM_FIXED_CM_IRIS:
                    if ((ucRecvBytes[2] >= 1) && (ucRecvBytes[2] <= 5))
                    {
                        if (ucRecvBytes[3] <= 43)
                        {
                            ee_FixFormatColl.ucFixedCmIris[ucRecvBytes[2] - 1] = ucRecvBytes[3];
                            sprintf(cbTemp, "IRIS FORMAT %d=%d cm", ucRecvBytes[2], ucRecvBytes[3]);
                            sys_display_queue_msg(cbTemp);
                            ucTemp = 1;
                        }
                    }
                    break;
                case COM_FIXED_INCH_IRIS:
                    if ((ucRecvBytes[2] >= 1) && (ucRecvBytes[2] <= 5))
                    {
                        if (ucRecvBytes[3] <= 17)
                        {
                            ee_FixFormatColl.ucFixedInIris[ucRecvBytes[2] - 1] = ucRecvBytes[3];
                            sprintf(cbTemp, "IRIS FORMAT %d=%d\"", ucRecvBytes[2], ucRecvBytes[3]);
                            sys_display_queue_msg(cbTemp);
                            ucTemp = 1;
                        }
                    }
                    break;
                }

                if (ucTemp)
                {
                    s_ulUpdateEeprom |= EEP_UPDATE_FORMAT;
                }
                else
                {
                    sys_display_queue_msg("Command Error");
                }
            }
            else            // ****  LETTURA DATI  ****
            {
                tx_temp.data[1] = ucRecvBytes[1];       // copia sotto comando
                switch (ucRecvBytes[1]) // sotto-comando
                {
                case COM_FIXED_NUM:
                    tx_temp.data[2] = ee_FixFormatColl.ucNumFormats;
                    break;
                case COM_FIXED_CM:
                    if ((ucRecvBytes[2] >= 1) && (ucRecvBytes[2] <= 5))
                    {
                        tx_temp.data[2] = ucRecvBytes[2];
                        tx_temp.data[3] = ee_FixFormatColl.ucFixedCmCross[ucRecvBytes[2] - 1];
                        tx_temp.data[4] = ee_FixFormatColl.ucFixedCmLong[ucRecvBytes[2] - 1];
                    }
                    break;
                case COM_FIXED_INCH:
                    if ((ucRecvBytes[2] >= 1) && (ucRecvBytes[2] <= 5))
                    {
                        tx_temp.data[2] = ucRecvBytes[2];
                        tx_temp.data[3] = ee_FixFormatColl.ucFixedInCross[ucRecvBytes[2] - 1];
                        tx_temp.data[4] = ee_FixFormatColl.ucFixedInLong[ucRecvBytes[2] - 1];
                    }
                    break;
                case COM_FIXED_NUM_IRIS:
                    tx_temp.data[2] = ee_FixFormatColl.ucNumFormatsIris;
                    break;
                case COM_FIXED_CM_IRIS:
                    if ((ucRecvBytes[2] >= 1) && (ucRecvBytes[2] <= 5))
                    {
                        tx_temp.data[2] = ucRecvBytes[2];
                        tx_temp.data[3] = ee_FixFormatColl.ucFixedCmIris[ucRecvBytes[2] - 1];
                    }
                    break;
                case COM_FIXED_INCH_IRIS:
                    if ((ucRecvBytes[2] >= 1) && (ucRecvBytes[2] <= 5))
                    {
                        tx_temp.data[2] = ucRecvBytes[2];
                        tx_temp.data[3] = ee_FixFormatColl.ucFixedInIris[ucRecvBytes[2] - 1];
                    }
                    break;
                }
            }
            break;

        case CONFIG_FILTER_SEQ:
            if (WriteVal == TRUE)
            {
                flag = FALSE;
                for (i = 1; i <= 4; i++)
                {
                    if (ucRecvBytes[i] > 3)
                    {
                        flag = TRUE;
                    }
                }

                if (flag == FALSE)
                {
                    ee_FilterConfig.Config_Filtro[0] = ucRecvBytes[1];
                    ee_FilterConfig.Config_Filtro[1] = ucRecvBytes[2];
                    ee_FilterConfig.Config_Filtro[2] = ucRecvBytes[3];
                    ee_FilterConfig.Config_Filtro[3] = ucRecvBytes[4];
                    s_ulUpdateEeprom |= EEP_UPDATE_FILTER;
                    sprintf(cbTemp, "FILTER SEQ.=%d %d %d %d", ucRecvBytes[1], ucRecvBytes[2], ucRecvBytes[3], ucRecvBytes[4]);
                    sys_display_queue_msg(cbTemp);
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_FilterConfig.Config_Filtro[0];
                tx_temp.data[2] = ee_FilterConfig.Config_Filtro[1];
                tx_temp.data[3] = ee_FilterConfig.Config_Filtro[2];
                tx_temp.data[4] = ee_FilterConfig.Config_Filtro[3];
            }
            break;

        case CONFIG_LCD_FREE:
            if (WriteVal == TRUE)
            {
                switch (ucRecvBytes[1])
                {
                case LCD_PROTO:
                    sprintf(cbTemp, "LCD NAME=Protocol");
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.ucLcdFree = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                case LCD_FREE:
                    sprintf(cbTemp, "LCD NAME=Free");
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.ucLcdFree = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                default:
                    sys_display_queue_msg("Command Error");
                    break;
                }
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucLcdFree;
            }
            break;

        case CONFIG_LCD_NAME:
            if (WriteVal == TRUE)
            {
                for (i = 1; i < 8; i++) // controllo del nome, se caratteri stampabili
                {
                    ucTemp = ucRecvBytes[i];
                    if ((isalnum(ucTemp) == 0) && (ucTemp != ' '))
                    {
                        ucTemp = ' '; // sostituisce carattere non stampabile con uno spazio
                    }
                    ee_ConfigColl.ucLcdName[i - 1] = ucTemp;
                }

                sprintf(cbTemp, "NAME=%s", ee_ConfigColl.ucLcdName);
                sys_display_queue_msg(cbTemp);
                s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucLcdName[0];
                tx_temp.data[2] = ee_ConfigColl.ucLcdName[1];
                tx_temp.data[3] = ee_ConfigColl.ucLcdName[2];
                tx_temp.data[4] = ee_ConfigColl.ucLcdName[3];
                tx_temp.data[5] = ee_ConfigColl.ucLcdName[4];
                tx_temp.data[6] = ee_ConfigColl.ucLcdName[5];
                tx_temp.data[7] = ee_ConfigColl.ucLcdName[6];
            }
            break;

        case CONFIG_SPEED_MAN:
            if (WriteVal == TRUE)
            {
                uwTemp = (UINT16) ucRecvBytes[1] << 8;
                uwTemp |= (UINT16) ucRecvBytes[2];
                if (uwTemp < FREQ_SET_MIN)
                    uwTemp = FREQ_SET_MIN;
                if (uwTemp > FREQ_SET_MAX)
                    uwTemp = FREQ_SET_MAX;
                sprintf(cbTemp, "%s %d", "SPEED MAN=", uwTemp);
                sys_display_queue_msg(cbTemp);
                if (uwTemp < DEF_FMIN)
                {
                    ee_ConfigBoards[STEPPER_A].uwFMin = uwTemp; //Fmin: era 1000
                    ee_ConfigBoards[STEPPER_A].uwFMax = uwTemp; //Fmax: era 1500
                    ee_ConfigBoards[STEPPER_B].uwFMin = uwTemp; //Fmin: era 1000
                    ee_ConfigBoards[STEPPER_B].uwFMax = uwTemp; //Fmax: era 1500
                }
                else
                {
                    ee_ConfigBoards[STEPPER_A].uwFMin = DEF_FMIN; //Fmin: era 1000
                    ee_ConfigBoards[STEPPER_A].uwFMax = uwTemp; //Fmax: era 1500
                    ee_ConfigBoards[STEPPER_B].uwFMin = DEF_FMIN; //Fmin: era 1000
                    ee_ConfigBoards[STEPPER_B].uwFMax = uwTemp; //Fmax: era 1500
                }
                s_ulUpdateEeprom |= EEP_UPDATE_DSPEED;
            }
            else
            {
                tx_temp.data[1] = ee_ConfigBoards[STEPPER_A].uwFMax >> 8;
                tx_temp.data[2] = ee_ConfigBoards[STEPPER_A].uwFMax & 0x00FF;
            }
            break;

        case CONFIG_DIS_IRIDE:
            if (WriteVal == TRUE)
            {
                uwTemp = ucRecvBytes[1];
                if (uwTemp)
                {
                    ee_ConfigColl.ucIrisEnabled = TRUE;
                    sprintf(cbTemp, "%s", "IRIS ENABLED");
                }
                else
                {
                    ee_ConfigColl.ucIrisEnabled = FALSE;
                    sprintf(cbTemp, "%s", "IRIS DISABLED");
                }
                sys_display_queue_msg(cbTemp);
                s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucIrisEnabled;
            }
            break;

        case CONFIG_OFFSET_LAM:
            if (WriteVal == TRUE)
            {
                uwTemp = ucRecvBytes[1];
                if (uwTemp > 20)
                    uwTemp = 20;
                sprintf(cbTemp, "%s %d", "OFFSET =", uwTemp);
                sys_display_queue_msg(cbTemp);
                ee_ConfigColl.OffsetLamelle = uwTemp;
                s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.OffsetLamelle;
            }
            break;

        case CONFIG_METRO_US:
            if (WriteVal == TRUE)
            {
                uwTemp = ucRecvBytes[1];
                if (uwTemp)
                {
                    ee_ConfigColl.ucMeterEnabled = TRUE;
                    sprintf(cbTemp, "%s", "US METER ENABLED");
                }
                else
                {
                    ee_ConfigColl.ucMeterEnabled = FALSE;
                    sprintf(cbTemp, "%s", "US METER DISABLED");
                }
                sys_display_queue_msg(cbTemp);
                s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucMeterEnabled;
            }
            break;

        case CONFIG_OFFSET_METRO:
            if (WriteVal == TRUE)
            {
                uwTemp = (uword) ucRecvBytes[1] << 8;
                uwTemp |= ucRecvBytes[2];
                sprintf(cbTemp, "%s %d", "OFFSET METER =", uwTemp);
                sys_display_queue_msg(cbTemp);
                ee_ConfigColl.OffsetMetro = uwTemp;
                s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.OffsetMetro >> 8;
                tx_temp.data[2] = ee_ConfigColl.OffsetMetro & 0x00FF;
            }
            break;

        case CONFIG_MOTORE_FILTRO:
            if (WriteVal == TRUE)
            {
                if ((ucRecvBytes[1] == MOTORE_FILTRO_OLD) || (ucRecvBytes[1] == MOTORE_FILTRO_NEW))
                {
                    if (ucRecvBytes[1] == MOTORE_FILTRO_OLD)
                    {
                        sys_display_queue_msg("FILTER MOTOR = OLD");
                        ee_FilterConfig.uwSteps = K_FILTER_STEPS_OLD;
                        ee_FilterConfig.StepsResetMin = FILTER_RESET_MIN_OLD;
                        ee_FilterConfig.StepsResetMax = FILTER_RESET_MAX_OLD;
                        ee_ConfigBoards[STEPPER_C].uwFMin   = 500;
                        ee_ConfigBoards[STEPPER_C].uwFMax   = 10000;
                        ee_ConfigBoards[STEPPER_FILTER].ucStep   = 0x08;    //Step: 1/16
                        ee_ConfigBoards[STEPPER_C].ucTorque = TORQUE_FILTRO_OLD;    //Torque
                    }
                    else
                    {
                        sys_display_queue_msg("FILTER MOTOR = NEW");
                        ee_FilterConfig.uwSteps = K_FILTER_STEPS_NEW;
                        ee_FilterConfig.StepsResetMin = FILTER_RESET_MIN_NEW;
                        ee_FilterConfig.StepsResetMax = FILTER_RESET_MAX_NEW;
                        ee_ConfigBoards[STEPPER_C].uwFMin   = 500;
                        ee_ConfigBoards[STEPPER_C].uwFMax   = 10000;
                        ee_ConfigBoards[STEPPER_FILTER].ucStep   = 0x06;    //Step: 1/4
                        ee_ConfigBoards[STEPPER_C].ucTorque = TORQUE_FILTRO_NEW;    //Torque
                    }
                    ee_FilterConfig.TipoMotore = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_FILTER;
                    s_ulUpdateEeprom |= EEP_UPDATE_DMODE;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_FilterConfig.TipoMotore;
            }
            break;

        case CONFIG_TIPO_CORR:
            if (WriteVal == TRUE)
            {
                if ((ucRecvBytes[1] == TIPO_CORR_CM) || (ucRecvBytes[1] == TIPO_CORR_PERC))
                {
                    if (ucRecvBytes[1] == TIPO_CORR_CM)
                    {
                        sys_display_queue_msg("CORR.TYPE=CM on SID");
                    }
                    else
                    {
                        sys_display_queue_msg("CORR.TYPE=% on APERT");
                    }
                    ee_ConfigColl.CorrType = ucRecvBytes[1];
                    ee_ConfigColl.scCorrCrossDff = 0;
                    ee_ConfigColl.scCorrLongDff = 0;
                    ee_ConfigColl.scCorrIrideDff = 0;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.CorrType;
            }
            break;

        case CONFIG_MANOPOLE:
            if (WriteVal == TRUE)
            {
                uwTemp = (uword)ucRecvBytes[1] << 8;
                uwTemp |= ucRecvBytes[2];
                if (uwTemp < FREQ_SET_MIN)
                    uwTemp = FREQ_SET_MIN;
                if (uwTemp > FREQ_SET_MAX)
                    uwTemp = FREQ_SET_MAX;
                sprintf(cbTemp, "%s %d S=%d A=%d", "KNOBS=", uwTemp, ucRecvBytes[3], ucRecvBytes[4]);
                sys_display_queue_msg(cbTemp);
                ee_ConfigColl.EncMaxFreq = uwTemp;
                ee_ConfigColl.EncMaxSpeed = ucRecvBytes[3];
                ee_ConfigColl.EncAccel = ucRecvBytes[4];
                s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.EncMaxFreq >> 8;
                tx_temp.data[2] = ee_ConfigColl.EncMaxFreq & 0x00FF;
                tx_temp.data[3] = ee_ConfigColl.EncMaxSpeed;
                tx_temp.data[4] = ee_ConfigColl.EncAccel;
            }
            break;

        case CONFIG_TIPO_SPOST_MAN:
            if (WriteVal == TRUE)
            {
                if ((ucRecvBytes[1] == TIPO_MAN_SPOST_OLD) || (ucRecvBytes[1] == TIPO_MAN_SPOST_MM))
                {
                    if (ucRecvBytes[1] == TIPO_MAN_SPOST_OLD)
                    {
                        sys_display_queue_msg("MAN.MOVE=CONST FREQ.");
                    }
                    else
                    {
                        sys_display_queue_msg("MAN.MOVE=mm/sec");
                    }
                    ee_ConfigColl.TipoManualSpost = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.TipoManualSpost;
            }
            break;

        case CONFIG_SPOST_MM:
            if (WriteVal == TRUE)
            {
                uwTemp = (uword)ucRecvBytes[1] << 8;
                uwTemp |= ucRecvBytes[2];
                if (uwTemp < SPOST_MM_MIN)
                    uwTemp = SPOST_MM_MIN;
                if (uwTemp > SPOST_MM_MAX)
                    uwTemp = SPOST_MM_MAX;
                sprintf(cbTemp, "%s %d mm/sec ", "MAN.MOVE=", uwTemp);
                sys_display_queue_msg(cbTemp);
                ee_ConfigColl.ManualSpeed = uwTemp;
                g_ManualSpeed[STEPPER_CROSS] = uwTemp;
                g_ManualSpeed[STEPPER_LONG] = uwTemp;
                g_ManualSpeed[STEPPER_LONG2] = uwTemp;
                s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ManualSpeed >> 8;
                tx_temp.data[2] = ee_ConfigColl.ManualSpeed & 0x00FF;
            }
            break;

        case CONFIG_POT_SINGLE:
            if (WriteVal == TRUE)
            {
                if ((ucRecvBytes[1] == POT_SINGLE_STAND) || (ucRecvBytes[1] == POT_SINGLE_TABLE))
                {
                    if (ucRecvBytes[1] == POT_SINGLE_STAND)
                        sys_display_queue_msg("SINGLE POT = STAND");
                    else
                        sys_display_queue_msg("SINGLE POT = TABLE");
                    ee_ConfigColl.ucInputDffPot = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucInputDffPot;
            }
            break;

        case CONFIG_DFF_LAT_DX:
            if (WriteVal == TRUE)
            {
                if (ucRecvBytes[1] == DFF_LAT_CAN)
                {
                    sys_display_queue_msg("LATER.DFF DX = CAN  ");
                    ee_ConfigColl.ucLatDffDx = LAT_DFF_CAN;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                if (ucRecvBytes[1] == DFF_LAT_DISC)
                {
                    sys_display_queue_msg("LATER.DFF DX = DISCR");
                    ee_ConfigColl.ucLatDffDx = LAT_DFF_DISC;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                if (ucRecvBytes[1] == DFF_LAT_POT)
                {
                    sys_display_queue_msg("LATER.DFF = POT.    ");
                    ee_ConfigColl.ucLatDffDx = LAT_DFF_POT;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                switch (ee_ConfigColl.ucLatDffDx)
                {
                case LAT_DFF_CAN:
                    tx_temp.data[1] = DFF_LAT_CAN;
                    break;
                case LAT_DFF_DISC:
                    tx_temp.data[1] = DFF_LAT_DISC;
                    break;
                case LAT_DFF_POT:
                    tx_temp.data[1] = DFF_LAT_POT;
                    break;
                }
            }
            break;

        case CONFIG_DFT_SX:
            if (WriteVal == TRUE)
            {
                if (ucRecvBytes[1] <= DFT_MAX)
                {
                    sprintf(cbTemp, "%s %d cm", "DFT SX = ", ucRecvBytes[1]);
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.ucDftSx = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucDftSx;
            }
            break;

        case CONFIG_DFT_DX:
            if (WriteVal == TRUE)
            {
                if (ucRecvBytes[1] <= DFT_MAX)
                {
                    sprintf(cbTemp, "%s %d cm", "DFT DX = ", ucRecvBytes[1]);
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.ucDftDx = ucRecvBytes[1];
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucDftDx;
            }
            break;

        case CONFIG_FORMATI_MIN:
            if (WriteVal == TRUE)
            {
                uwTemp = ucRecvBytes[1];
                if (uwTemp)
                {
                    ee_ConfigColl.EnFormatiMin = TRUE;
                    sprintf(cbTemp, "%s", "ENABLE MIN. FORMAT");
                }
                else
                {
                    ee_ConfigColl.EnFormatiMin = FALSE;
                    sprintf(cbTemp, "%s", "DISABLE MIN. FORMAT");
                }
                sys_display_queue_msg(cbTemp);
                s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.EnFormatiMin;
            }
            break;

        case CONFIG_MANUAL_COLL:
            if (WriteVal == TRUE)
            {
                uwTemp = ucRecvBytes[1];
                if (uwTemp)
                {
                    ee_ConfigColl.ucManualColl = MANUAL_COLL_EN;
                    sprintf(cbTemp, "%s", "ENABLE MAN. COLLIM.");
                }
                else
                {
                    ee_ConfigColl.ucManualColl = MANUAL_COLL_DIS;
                    sprintf(cbTemp, "%s", "DISABLE MAN. COLLIM.");
                }
                sys_display_queue_msg(cbTemp);
                s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucManualColl;
            }
            break;

        case CONFIG_VISUA_APE_IRIS:
            if (WriteVal == TRUE)
            {
                uwTemp = ucRecvBytes[1];
                if (uwTemp)
                {
                    ee_ConfigColl.ucVisuaApeIris = TRUE;
                    sprintf(cbTemp, "%s", "EN. DISPLAY IRIS AP.");
                }
                else
                {
                    ee_ConfigColl.ucVisuaApeIris = FALSE;
                    sprintf(cbTemp, "%s", "DIS.DISPLAY IRIS AP.");
                }
                sys_display_queue_msg(cbTemp);
                s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucVisuaApeIris;
            }
            break;

        case CONFIG_MSG_7F5:
            if (WriteVal == TRUE)
            {
                uwTemp = (uword)ucRecvBytes[1] << 8;
                uwTemp |= ucRecvBytes[2];
                if ((uwTemp == 0) || ((uwTemp >= 50) && (uwTemp <= 1000)))
                {
                    sprintf(cbTemp, "Msg 0x7F5= %d ms", uwTemp);
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.TempoInvio7F5 = uwTemp;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                }
                else
                {
                    sys_display_queue_msg("Command Error");
                }
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.TempoInvio7F5 >> 8;
                tx_temp.data[2] = ee_ConfigColl.TempoInvio7F5 & 0x00FF;
            }
            break;

        case CONFIG_NOME_FILTRO_A:
            if (WriteVal == TRUE)
            {
                scTemp = ucRecvBytes[1];        // numero del filtro
                if ((scTemp >= 1) && (scTemp <= 4))
                {
                    scTemp--;
                    for (i = 2; i < 8; i++) // controllo del nome, se caratteri stampabili
                    {
                        ucTemp = ucRecvBytes[i];
                        if (((ucTemp < 0x20) || (ucTemp > 0x7F)) && (ucTemp != 0))
                        {
                            ucTemp = ' ';       // sostituisce carattere non stampabile con uno spazio
                        }
                        ee_ConfigColl.NomiFiltroCustom[scTemp][i - 2] = ucTemp;
                    }
                    sprintf(cbTemp, "NAME %c=%s", ucRecvBytes[1] + 0x30, &ee_ConfigColl.NomiFiltroCustom[scTemp][0]);
                    sys_display_queue_msg(cbTemp);
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                }
            }
            else
            {
                scTemp = ucRecvBytes[1];        // numero del filtro
                scTemp--;
                tx_temp.data[1] = ucRecvBytes[1];
                tx_temp.data[2] = ee_ConfigColl.NomiFiltroCustom[scTemp][0];
                tx_temp.data[3] = ee_ConfigColl.NomiFiltroCustom[scTemp][1];
                tx_temp.data[4] = ee_ConfigColl.NomiFiltroCustom[scTemp][2];
                tx_temp.data[5] = ee_ConfigColl.NomiFiltroCustom[scTemp][3];
                tx_temp.data[6] = ee_ConfigColl.NomiFiltroCustom[scTemp][4];
                tx_temp.data[7] = ee_ConfigColl.NomiFiltroCustom[scTemp][5];
            }
            break;

        case CONFIG_NOME_FILTRO_B:
            if (WriteVal == TRUE)
            {
                scTemp = ucRecvBytes[1];        // numero del filtro
                if ((scTemp >= 1) && (scTemp <= 4))
                {
                    scTemp--;
                    for (i = 2; i < 8; i++) // controllo del nome, se caratteri stampabili
                    {
                        ucTemp = ucRecvBytes[i];
                        if (((ucTemp < 0x20) || (ucTemp > 0x7F)) && (ucTemp != 0))
                        {
                            ucTemp = ' ';       // sostituisce carattere non stampabile con uno spazio
                        }
                        ee_ConfigColl.NomiFiltroCustom[scTemp][i + 4] = ucTemp;
                    }
                    sprintf(cbTemp, "NAME %c=%s", ucRecvBytes[1] + 0x30, &ee_ConfigColl.NomiFiltroCustom[scTemp][0]);
                    sys_display_queue_msg(cbTemp);
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                }
            }
            else
            {
                scTemp = ucRecvBytes[1];        // numero del filtro
                scTemp--;
                tx_temp.data[1] = ucRecvBytes[1];
                tx_temp.data[2] = ee_ConfigColl.NomiFiltroCustom[scTemp][6];
                tx_temp.data[3] = ee_ConfigColl.NomiFiltroCustom[scTemp][7];
                tx_temp.data[4] = ee_ConfigColl.NomiFiltroCustom[scTemp][8];
                tx_temp.data[5] = ee_ConfigColl.NomiFiltroCustom[scTemp][9];
                tx_temp.data[6] = ee_ConfigColl.NomiFiltroCustom[scTemp][10];
                tx_temp.data[7] = ee_ConfigColl.NomiFiltroCustom[scTemp][11];
            }
            break;

        case CONFIG_CORR_IRIDE:
            if (WriteVal == TRUE)
            {
                if (ee_ConfigColl.CorrType == TIPO_CORR_CM)
                {
                    scTemp=(schar)ucRecvBytes[1];
                    if (((scTemp>=0)&&(scTemp<=DELTA_DFF_MAX_POS))||
                        ((scTemp<0)&&(scTemp>=DELTA_DFF_MAX_NEG))){
                        sprintf(cbTemp, "%s %d cm", "CORR.SID IRIS =", scTemp);
                        sys_display_queue_msg(cbTemp);
                        ee_ConfigColl.scCorrIrideDff=scTemp;
                        s_ulUpdateEeprom|=EEP_UPDATE_COLLIMATOR;
                        break;
                    }
                }
                else
                {
                    scTemp = (schar) ucRecvBytes[1];
                    if (((scTemp >= 0) && (scTemp <= DELTA_PERC_MAX_POS)) ||
                        ((scTemp < 0) && (scTemp >= DELTA_PERC_MAX_NEG)))
                    {
                        t1 = scTemp / 10;
                        t2 = scTemp % 10;
                        c = ' ';
                        if (scTemp < 0)
                        {
                            c = '-';
                            t1 = -t1;
                            t2 = -t2;
                        }
                        sprintf(cbTemp, "%s %c%01d.%01d %%", "CORR.% IRIS = ", c, t1, t2);
                        sys_display_queue_msg(cbTemp);
                        ee_ConfigColl.scCorrIrideDff = scTemp;
                        s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                        break;
                    }
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.scCorrIrideDff;
            }
            break;

        case CONFIG_FILTRAGGIO_INCL:
            if (WriteVal == TRUE)
            {
                uwTemp = ucRecvBytes[1];
                if (uwTemp)
                {
                    ee_ConfigColl.ucFiltraggioIncl = TRUE;
                    sprintf(cbTemp, "%s", "ENABLE INCL FILTER");
                }
                else
                {
                    ee_ConfigColl.ucFiltraggioIncl = FALSE;
                    sprintf(cbTemp, "%s", "DISABLE INCL FILTER");
                }
                sys_display_queue_msg(cbTemp);
                s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucFiltraggioIncl;
            }
            break;

        case CONFIG_ADDR_OTHER_COM:
            if (WriteVal == TRUE)
            {
                uwTemp = (uword) ucRecvBytes[1] << 8;
                uwTemp |= ucRecvBytes[2];
                if (uwTemp > MAX_ADD)
                    uwTemp = MAX_ADD;
                sprintf(cbTemp, "%s %x ", "7A1 ADDRESS=", uwTemp);
                sys_display_queue_msg(cbTemp);
                ee_CanColl.unIdOtherCommands = uwTemp;
                s_ulUpdateEeprom |= EEP_UPDATE_CAN;
            }
            else
            {
                tx_temp.data[1] = ee_CanColl.unIdOtherCommands >> 8;
                tx_temp.data[2] = ee_CanColl.unIdOtherCommands & 0x00FF;
            }
            break;

        case CONFIG_RISPOSTE_7F8:
            if (WriteVal == TRUE)
            {
                uwTemp = ucRecvBytes[1];
                if (uwTemp)
                {
                    ee_ConfigColl.ucRisposte7F8 = TRUE;
                    sprintf(cbTemp, "%s", "ENABLE MSG 7F8 ");
                }
                else
                {
                    ee_ConfigColl.ucRisposte7F8 = FALSE;
                    sprintf(cbTemp, "%s", "DISABLE MSG 7F8");
                }
                sys_display_queue_msg(cbTemp);
                s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ucRisposte7F8;
            }
            break;

        case CONFIG_TASTIERA_VILLA:
            if (WriteVal == TRUE)
            {
                uwTemp = ucRecvBytes[1];
                if (uwTemp)
                {
                    ee_ConfigColl.TastieraVilla = TRUE;
                    ee_ConfigColl.IdOffsetTastieraVilla = ucRecvBytes[2] & 0x0F;
                    sprintf(cbTemp, "%s %d", "EN. TABLE BUTTONS ", ee_ConfigColl.IdOffsetTastieraVilla);
                }
                else
                {
                    ee_ConfigColl.TastieraVilla = FALSE;
                    sprintf(cbTemp, "%s", "DISAB. TABLE BUTTONS");
                }
                sys_display_queue_msg(cbTemp);
                s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.TastieraVilla;
                tx_temp.data[2] = ee_ConfigColl.IdOffsetTastieraVilla;
            }
            break;

        case CONFIG_ASR003:
            if (WriteVal == TRUE)
            {
                uwTemp = ucRecvBytes[1];
                if (uwTemp)
                {
                    ee_ConfigColl.ASR003 = TRUE;
                    sprintf(cbTemp, "%s", "ENABLE ASR003 ");
                }
                else
                {
                    ee_ConfigColl.ASR003 = FALSE;
                    sprintf(cbTemp, "%s", "DISABLE ASR003 ");
                }
                sys_display_queue_msg(cbTemp);
                s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.ASR003;
            }
            break;

        case CONFIG_DISPLAY_ANGOLO_TUBO:
            if (WriteVal == TRUE)
            {
                uwTemp = ucRecvBytes[1];
                if (uwTemp)
                {
                    ee_ConfigColl.displayAngoloTubo = TRUE;
                    sprintf(cbTemp, "%s", "DISPLAY TUBE ANGLE");
                }
                else
                {
                    ee_ConfigColl.displayAngoloTubo = FALSE;
                    sprintf(cbTemp, "%s", "NOT DISPLAY TUBE ");
                }
                sys_display_queue_msg(cbTemp);
                s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.displayAngoloTubo;
            }
            break;

        case CONFIG_JITTER_KNOBS:
            if (WriteVal == TRUE)
            {
                sprintf(cbTemp, "%s %d", "JITTER =", ucRecvBytes[1]);
                sys_display_queue_msg(cbTemp);
                ee_ConfigColl.JitterKnobs = ucRecvBytes[1];
                s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.JitterKnobs;
            }
            break;

        case CONFIG_ENTER_CONFIG:
            if (WriteVal == TRUE)
            {
                uwTemp = (uword) ucRecvBytes[1] << 8;
                uwTemp |= ucRecvBytes[2];
                sprintf(cbTemp, "%s %d ", "CONFIG TIME =", uwTemp);
                sys_display_queue_msg(cbTemp);
                ee_ConfigColl.decSecondsConfig = uwTemp;
                s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.decSecondsConfig >> 8;
                tx_temp.data[2] = ee_ConfigColl.decSecondsConfig & 0x00FF;
            }
            break;

        case CONFIG_INVIO_XY_INCL:
            if (WriteVal == TRUE)
            {
                uwTemp = (uword)ucRecvBytes[1] << 8;
                uwTemp |= ucRecvBytes[2];
                if ((uwTemp == 0) || ((uwTemp >= 50) && (uwTemp <= 2000)))
                {
                    sprintf(cbTemp, "Msg INCL.= %d ms", uwTemp);
                    sys_display_queue_msg(cbTemp);
                    ee_ConfigColl.TempoInvioInclinometro = uwTemp;
                    s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
                }
                else
                {
                    sys_display_queue_msg("Command Error");
                }
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.TempoInvioInclinometro >> 8;
                tx_temp.data[2] = ee_ConfigColl.TempoInvioInclinometro & 0x00FF;
            }
            break;

        case CONFIG_OFFSET_ZERO:
            if (WriteVal == TRUE)
            {
                uwTemp = ucRecvBytes[1];
                if (uwTemp)
                {
                    ee_ConfigColl.OffsetZero = TRUE;
                    sprintf(cbTemp, "%s", "OFFSET ZERO = TRUE");
                }
                else
                {
                    ee_ConfigColl.OffsetZero = FALSE;
                    sprintf(cbTemp, "%s", "OFFSET ZERO = FALSE");
                }
                sys_display_queue_msg(cbTemp);
                s_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            }
            else
            {
                tx_temp.data[1] = ee_ConfigColl.OffsetZero;
            }
            break;





        case CONFIG_DEFAULT:
            // Collimator RESET
            if (WriteVal == TRUE)
            {
                if ((ucRecvBytes[1] == 0x55) && (ucRecvBytes[2] == 0xAA))
                {
                    calcola_crc(TRUE);      // reset dati
                    resetCollimator();
                    break;
                }
            }
            else
            {
                __asm(" nop");
            }
            break;


        case CONFIG_RESET_ALL:
            // Collimator RESET
            if (WriteVal == TRUE)
            {
                if (ucRecvBytes[1] == 0x55)
                {
                    resetCollimator();
                }
            }
            else
            {
                __asm(" nop");
            }
            break;

        case CONFIG_ENABLE_VARI:
            if (WriteVal == TRUE)
            {
                enable_variazione = ucRecvBytes[1];
            }
            else
            {
                __asm(" nop");
            }
            break;

        case CONFIG_SAVE:
            if (WriteVal == TRUE)
            {
                if (ucRecvBytes[1] == OK)
                {
                    timer_led_config = 50;
                    g_ulUpdateEeprom = s_ulUpdateEeprom;
                    s_ulUpdateEeprom = 0;
                    sys_display_queue_msg("Saving Configuration");
                    break;
                }
                if (ucRecvBytes[1] == NOK)
                {
                    timer_led_config = 5;
                    s_ulUpdateEeprom = 0;
                    g_ulUpdateEeprom = 0;
                    sys_display_queue_msg("Changes Discarded");
                    break;
                }
                sys_display_queue_msg("Command Error");
            }
            else
            {
                __asm(" nop");
            }
            break;

        default:
            sys_display_queue_msg("Command Error");
            break;
        }


        if (source == 0)        // canbus
        {
            if (WriteVal == FALSE)  // in canbus risponde solo se dato in lettura
            {
                copy_tx_temp(FALSE);
            }
        }
        else if (source == 1)       // seriale
        {
            if (WriteVal == TRUE)   // dato scritto - risponde OK
            {
                invia_ser_ok();
            }
            else                // dato letto, risponde con valore letto
            {
                // TODO
                // copia in buffer seriale il dato letto
                strcpy(buff_tx_ser, msg_ser_tx_read);
                for (i = 0; i < 8; i++)
                {
                    sprintf(cbTemp, "%02X", tx_temp.data[i]);
                    strcat(buff_tx_ser, cbTemp);
                    if (i < 7)
                    {
                        strcat(buff_tx_ser, ",");
                    }
                }
                invia_tx_ser();     // invia buffer
            }
        }
    }
}







//****************************************************************
//  invia messaggio per taratura schede esterne
//****************************************************************
void sys_can_send_taratura(UINT8 com)
{
    UINT8 i;
    CAN_STD_DATA_DEF *ptr;

    for (i=0; i<8; i++)
        tx_temp.data[i] = 0;


    switch (com)
    {
    case COM_TAR_ENABLE_BUTTON:
        tx_temp.dlc = 1;
        tx_temp.id = ee_CanColl.unIdRot + 0x06;
        tx_temp.data[0] = 0x00;
        break;

    case COM_TAR_POT_MIN:
        tx_temp.dlc = 1;
        tx_temp.id = ee_CanColl.unIdRot + 0x0A;
        tx_temp.data[0] = 0x05;
        break;

    case COM_TAR_POS_MIN:
        tx_temp.dlc = 5;
        tx_temp.id = ee_CanColl.unIdRot + 0x0A;
        tx_temp.data[0] = 0x01;
        tx_temp.data[1] = 0xFF;
        tx_temp.data[2] = 0xFF;
        tx_temp.data[3] = 0xFE;
        tx_temp.data[4] = 0x3E;
        break;

    case COM_TAR_POS_MED:
        tx_temp.dlc = 5;
        tx_temp.id = ee_CanColl.unIdRot + 0x0A;
        tx_temp.data[0] = 0x02;
        tx_temp.data[1] = 0x00;
        tx_temp.data[2] = 0x00;
        tx_temp.data[3] = 0x00;
        tx_temp.data[4] = 0x00;
        break;

    case COM_TAR_POT_MAX:
        tx_temp.dlc = 1;
        tx_temp.id = ee_CanColl.unIdRot + 0x0A;
        tx_temp.data[0] = 0x06;
        break;

    case COM_TAR_POS_MAX:
        tx_temp.dlc = 5;
        tx_temp.id = ee_CanColl.unIdRot + 0x0A;
        tx_temp.data[0] = 0x03;
        tx_temp.data[1] = 0x00;
        tx_temp.data[2] = 0x00;
        tx_temp.data[3] = 0x01;
        tx_temp.data[4] = 0xC2;
        break;

    case COM_TEST_POS_ABS:
        tx_temp.dlc = 5;
        tx_temp.id = ee_CanColl.unIdRot + 0x02;
        tx_temp.data[0] = 0x01;
        tx_temp.data[1] = (gradi_rot >> 24) & 0xFF;
        tx_temp.data[2] = (gradi_rot >> 16) & 0xFF;
        tx_temp.data[3] = (gradi_rot >> 8) & 0xFF;
        tx_temp.data[4] = gradi_rot & 0xFF;
        break;

                            // *************************
                            // ****  COMANDI IRIDE  ****
                            // *************************
    case COM_TAR_IN_TARATURA:
        tx_temp.dlc = 2;
        tx_temp.id = ee_CanColl.unIdIride + MSG_IRIS_CALIB;
        tx_temp.data[0] = MSG_IRIS_CAL_TAR;
        tx_temp.data[1] = 0x01;
        break;

    case COM_TAR_SET_POS_NOW:
        tx_temp.dlc = 5;
        tx_temp.id = ee_CanColl.unIdIride + MSG_IRIS_MOV;
        tx_temp.data[0] = 0xFE;
        tx_temp.data[1] = (posizione_iride >> 24) & 0xFF;
        tx_temp.data[2] = (posizione_iride >> 16) & 0xFF;
        tx_temp.data[3] = (posizione_iride >> 8) & 0xFF;
        tx_temp.data[4] = posizione_iride & 0xFF;
        break;

    case COM_TAR_SET_POS_0:
        tx_temp.dlc = 5;
        tx_temp.id = ee_CanColl.unIdIride + MSG_IRIS_MOV;
        tx_temp.data[0] = 0xFE;
        tx_temp.data[1] = 0;
        tx_temp.data[2] = 0;
        tx_temp.data[3] = 0;
        tx_temp.data[4] = 0;
        break;

    case COM_TAR_SET_PASSIMAX:
        tx_temp.dlc = 4;
        tx_temp.id = ee_CanColl.unIdIride + MSG_IRIS_CALIB;
        tx_temp.data[0] = MSG_IRIS_CAL_PMAX;
        tx_temp.data[1] = (Iride_PassiMax >> 8) & 0xFF;
        tx_temp.data[2] = Iride_PassiMax & 0xFF;
        tx_temp.data[3] = 1;     // salva in eeprom
        break;

    case COM_TAR_OUT_TARATURA:
        tx_temp.dlc = 2;
        tx_temp.id = ee_CanColl.unIdIride + MSG_IRIS_CALIB;
        tx_temp.data[0] = MSG_IRIS_CAL_TAR;
        tx_temp.data[1] = 0x00;
        break;

    case COM_TEST_IRIS_RES:
        tx_temp.dlc = 3;
        tx_temp.id = ee_CanColl.unIdIride + MSG_IRIS_MOV;
        tx_temp.data[0] = 0x00;
        tx_temp.data[1] = ee_IrisSteps.iride_calib_pot[0] >> 8;
        tx_temp.data[2] = ee_IrisSteps.iride_calib_pot[0] & 0xFF;
        stato_iride |= IRIDE_MOVE;
        break;

    case COM_TAR_IRIS_RES_0:
        tx_temp.dlc = 3;
        tx_temp.id = ee_CanColl.unIdIride + MSG_IRIS_MOV;
        tx_temp.data[0] = 0x00;
        tx_temp.data[1] = 0;
        tx_temp.data[2] = 0;
        stato_iride |= IRIDE_MOVE;
        break;

    case COM_TAR_IRIS_RES_FF:
        tx_temp.dlc = 3;
        tx_temp.id = ee_CanColl.unIdIride + MSG_IRIS_MOV;
        tx_temp.data[0] = 0x00;
        tx_temp.data[1] = 0xFF;
        tx_temp.data[2] = 0xFF;
        stato_iride |= IRIDE_MOVE;
        break;

    case COM_TAR_IRIS_STOP:
        tx_temp.dlc = 1;
        tx_temp.id = ee_CanColl.unIdIride + MSG_IRIS_MOV;
        tx_temp.data[0] = 0xFF;
        stato_iride |= IRIDE_MOVE;
        break;

    case COM_TEST_IRIS_POS:
        tx_temp.dlc = 7;
        tx_temp.id = ee_CanColl.unIdIride + MSG_IRIS_MOV;
        tx_temp.data[0] = 0x01;
        tx_temp.data[1] = (test_iris_passi >> 24) & 0xFF;
        tx_temp.data[2] = (test_iris_passi >> 16) & 0xFF;
        tx_temp.data[3] = (test_iris_passi >> 8) & 0xFF;
        tx_temp.data[4] = test_iris_passi & 0xFF;
        tx_temp.data[5] = (test_iris_passi >> 8) & 0xFF;;
        tx_temp.data[6] = test_iris_passi & 0xFF;
        break;

    case COM_TAR_SET_CALIB:
        tx_temp.dlc = 8;
        tx_temp.id = ee_CanColl.unIdIride + MSG_IRIS_CALIB;
        tx_temp.data[0] = MSG_IRIS_CAL_SET_TAR;
        tx_temp.data[1] = num_formato_iride;
        tx_temp.data[2] = ee_IrisSteps.iride_calib_pot[num_formato_iride] >> 8;
        tx_temp.data[3] = ee_IrisSteps.iride_calib_pot[num_formato_iride] & 0xFF;
        tx_temp.data[4] = ee_IrisSteps.iride_calib_passi[num_formato_iride] >> 8;
        tx_temp.data[5] = ee_IrisSteps.iride_calib_passi[num_formato_iride] & 0xFF;
        break;

    }

    copy_tx_temp(FALSE);     // transmit

}



//**********************************************
//  calcola valore con accelerazione
//  ATTENZIONE: Mult deve essere >= 2
//**********************************************
SINT16 EncoderCalib(SINT16 *value, UINT16 Mult)
{
    UINT16 temp16;

    temp16 = *value;
    if (*value < 0)
        temp16 = -(*value);

    if (temp16 <= MAGN_DIV_1)
    {
        temp16 >>= 2;
    }
    else if (temp16 <= MAGN_DIV_2)
    {
        temp16 >>= 1;
    }
    else if (temp16 <= MAGN_DIV_3)
    {
        temp16 *= (Mult >> 1);
    }
    else
    {
        temp16 *= Mult;
    }

    if (*value < 0)
        temp16 = -temp16;

    if (temp16)
        *value = 0;
    return temp16;

}



//*********************************************
//  se l'encoder cross supera una certa soglia
//  torna valido
//*********************************************
SINT16 CheckChangeCrossEnc(void)
{
    static SINT8 dir;

    if (dir != EncoderDir[STEPPER_CROSS])
    {
        dir = EncoderDir[STEPPER_CROSS];
        if (dir == ENC_DIR_PIU)
            g_wCrossMagnitude = 6;
        else
            g_wCrossMagnitude = -6;
    }

    if (g_wCrossMagnitude > 8)
    {
        g_wCrossMagnitude = 0;
        return 1;
    }
    else if (g_wCrossMagnitude < -8)
    {
        g_wCrossMagnitude = 0;
        return -1;
    }

    return 0;
}
