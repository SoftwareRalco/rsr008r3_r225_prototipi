/*
 * timer.c
 *
 *  Created on: 23 mag 2017
 *      Author: daniele
 */


#define IRQ_H

#include "include.h"
#include "main_thread.h"


#include "r_gpt.h"
#include "r_gpt_cfg.h"
#include "hw/hw_gpt_private.h"
#include "r_gpt_private_api.h"
#include "r_cgc.h"





/// definizione funzioni locali
void sys_clock (void);
void scan_kb(void);
void gestione_antirimbalzo(void);
void sys_adc_driver(void);
void debounce_tasti_villa(void);
void gestione_idle(void);

/// variabili interne
static uword g_uw10MsecCounter=0;
static uword g_uw50MsecCounter=0;
static uchar g_uw100MsecCounter=0;
static uword g_uw500MsecCounter=0;
static uword g_uw1000MsecCounter=0;
static uword g_uw10000MsecCounter=0;



#define GPT_ERROR_RETURN(a, err) SSP_ERROR_RETURN((a), (err), &g_module_name[0], &g_gpt_version)
#define GPT_MAX_CLOCK_COUNTS_32 (0xFFFFFFFFULL)
#define GPT_MAX_CLOCK_COUNTS_16 (0xFFFFUL)




static uchar g_ucEncCrossGray;
static signed short g_wActualCrossPosition;
static signed short g_wSaveCrossMagnitude;
static uchar g_ucEncLongGray;
static signed short g_wActualLongPosition;
static signed short g_wSaveLongMagnitude;
static uword g_uwCrossErrors=0;
static uword g_uwLongErrors=0;

static signed short s_wLastCrossPosition;
static signed short s_wLastLongPosition;
static int s_wTempCrossMagnitude=0;
static int s_wTempLongMagnitude=0;

#define NO_CODE     0xFF
const uchar AceTabInv[] = {
    NO_CODE,    // 0
    56,         // 1
    40,         // 2
    55,         // 3
    24,         // 4
    NO_CODE,    // 5
    39,         // 6
    52,         // 7
    8,          // 8
    57,         // 9
    NO_CODE,    // 10
    NO_CODE,           // 11
    23,           // 12
    NO_CODE,           // 13
    36,           // 14
    13,           // 15
    120,           // 16
    NO_CODE,           // 17
    41,           // 18
    54,           // 19
    NO_CODE,           // 20
    NO_CODE,           // 21
    NO_CODE,           // 22
    53,           // 23
    7,           // 24
    NO_CODE,           // 25
    NO_CODE,           // 26
    NO_CODE,           // 27
    20,           // 28
    19,           // 29
    125,           // 30
    18,           // 31
    104,           // 32
    105,           // 33
    NO_CODE,           // 34
    NO_CODE,           // 35
    25,           // 36
    106,           // 37
    38,           // 38
    NO_CODE,           // 39
    NO_CODE,           // 40
    58,           // 41
    NO_CODE,           // 42
    NO_CODE,           // 43
    NO_CODE,           // 44
    NO_CODE,           // 45
    37,           // 46
    14,           // 47
    119,           // 48
    118,           // 49
    NO_CODE,           // 50
    NO_CODE,           // 51
    NO_CODE,           // 52
    107,           // 53
    NO_CODE,           // 54
    NO_CODE,           // 55
    4,           // 56
    NO_CODE,           // 57
    3,           // 58
    NO_CODE,           // 59
    109,           // 60
    108,           // 61
    2,           // 62
    1,           // 63
    88,           // 64
    NO_CODE,           // 65
    89,           // 66
    NO_CODE,           // 67
    NO_CODE,           // 68
    NO_CODE,           // 69
    NO_CODE,           // 70
    51,           // 71
    9,           // 72
    10,           // 73
    90,           // 74
    NO_CODE,           // 75
    22,           // 76
    11,           // 77
    NO_CODE,           // 78
    12,           // 79
    NO_CODE,           // 80
    NO_CODE,           // 81
    42,           // 82
    43,           // 83
    NO_CODE,           // 84
    NO_CODE,           // 85
    NO_CODE,           // 86
    NO_CODE,           // 87
    NO_CODE,           // 88
    NO_CODE,           // 89
    NO_CODE,           // 90
    NO_CODE,           // 91
    21,           // 92
    NO_CODE,           // 93
    126,           // 94
    127,           // 95
    103,           // 96
    NO_CODE,           // 97
    102,           // 98
    NO_CODE,           // 99
    NO_CODE,           // 100
    NO_CODE,           // 101
    NO_CODE,           // 102
    NO_CODE,           // 103
    NO_CODE,           // 104
    NO_CODE,           // 105
    91,           // 106
    NO_CODE,           // 107
    NO_CODE,           // 108
    NO_CODE,           // 109
    NO_CODE,           // 110
    NO_CODE,           // 111
    116,           // 112
    117,           // 113
    NO_CODE,           // 114
    NO_CODE,           // 115
    115,           // 116
    NO_CODE,           // 117
    NO_CODE,           // 118
    NO_CODE,           // 119
    93,           // 120
    94,           // 121
    92,           // 122
    NO_CODE,           // 123
    114,           // 124
    95,           // 125
    113,           // 126
    0,           // 127
    72,           // 128
    71,           // 129
    NO_CODE,           // 130
    68,           // 131
    73,           // 132
    NO_CODE,           // 133
    NO_CODE,           // 134
    29,           // 135
    NO_CODE,           // 136
    70,           // 137
    NO_CODE,           // 138
    69,           // 139
    NO_CODE,           // 140
    NO_CODE,           // 141
    35,           // 142
    34,           // 143
    121,           // 144
    NO_CODE,           // 145
    122,           // 146
    NO_CODE,           // 147
    74,           // 148
    NO_CODE,           // 149
    NO_CODE,           // 150
    30,           // 151
    6,           // 152
    NO_CODE,           // 153
    123,           // 154
    NO_CODE,           // 155
    NO_CODE,           // 156
    NO_CODE,           // 157
    124,           // 158
    17,           // 159
    NO_CODE,           // 160
    NO_CODE,           // 161
    NO_CODE,           // 162
    67,           // 163
    26,           // 164
    NO_CODE,           // 165
    27,           // 166
    28,           // 167
    NO_CODE,           // 168
    59,           // 169
    NO_CODE,           // 170
    NO_CODE,           // 171
    NO_CODE,           // 172
    NO_CODE,           // 173
    NO_CODE,           // 174
    15,           // 175
    NO_CODE,           // 176
    NO_CODE,           // 177
    NO_CODE,           // 178
    NO_CODE,           // 179
    NO_CODE,           // 180
    NO_CODE,           // 181
    NO_CODE,           // 182
    NO_CODE,           // 183
    5,           // 184
    NO_CODE,           // 185
    NO_CODE,           // 186
    NO_CODE,           // 187
    110,           // 188
    NO_CODE,           // 189
    111,           // 190
    16,           // 191
    87,           // 192
    84,           // 193
    NO_CODE,           // 194
    45,           // 195
    86,           // 196
    85,           // 197
    NO_CODE,           // 198
    50,           // 199
    NO_CODE,           // 200
    NO_CODE,           // 201
    NO_CODE,           // 202
    46,           // 203
    NO_CODE,           // 204
    NO_CODE,           // 205
    NO_CODE,           // 206
    33,           // 207
    NO_CODE,           // 208
    83,           // 209
    NO_CODE,           // 210
    44,           // 211
    75,           // 212
    NO_CODE,           // 213
    NO_CODE,           // 214
    31,           // 215
    NO_CODE,           // 216
    NO_CODE,           // 217
    NO_CODE,           // 218
    NO_CODE,           // 219
    NO_CODE,           // 220
    NO_CODE,           // 221
    NO_CODE,           // 222
    32,           // 223
    100,           // 224
    61,           // 225
    101,           // 226
    66,           // 227
    NO_CODE,           // 228
    62,           // 229
    NO_CODE,           // 230
    49,           // 231
    99,           // 232
    60,           // 233
    NO_CODE,           // 234
    47,           // 235
    NO_CODE,           // 236
    NO_CODE,           // 237
    NO_CODE,           // 238
    48,           // 239
    77,           // 240
    82,           // 241
    78,           // 242
    65,           // 243
    76,           // 244
    63,           // 245
    NO_CODE,           // 246
    64,           // 247
    98,           // 248
    81,           // 249
    79,           // 250
    80,           // 251
    97,           // 252
    96,           // 253
    112,           // 254
    NO_CODE,           // 255
};








//***************************************************************
//  controllo dei passi su arrivo fotocellula
//***************************************************************
void checkPhotoError(UINT8 stepper)
{
    UINT8 photo;
    SINT32 temp32, diff;

    if (checkNewPhoto[stepper])
    {
        temp32 = i_MotPosizione[stepper];
        diff = g_MaxSteps[stepper] - temp32;
        if (diff == 0)
        {
            checkNewPhoto[stepper] = FALSE;
            PassiPhoto[stepper] = -1;
            generaEventoPassi[stepper] = TRUE;
        }
        else
        {
            switch (stepper)
            {
            case STEPPER_A:
                photo = (FULL_A != Steppers[STEPPER_A].ucLevel) ? TRUE : FALSE;
                break;
            case STEPPER_B:
                photo = (FULL_B != Steppers[STEPPER_B].ucLevel) ? TRUE : FALSE;
                break;
            case STEPPER_D:
                photo = (FULL_D != Steppers[STEPPER_D].ucLevel) ? TRUE : FALSE;
                break;
            default:
                photo = FALSE;
                break;
            }

            if (photo)
            {
                checkNewPhoto[stepper] = FALSE;
                if (diff > 1000000)
                    diff = 1000000;
                PassiPhoto[stepper] = diff;
                generaEventoPassi[stepper] = TRUE;
            }
        }
    }
}





//********************************************************
//  chiamata da IRQ da timer
//********************************************************
void irq_motore_1_callback (timer_callback_args_t * p_args)
{
    ssp_err_t err;
    UINT8 passo;

    g_ioport.p_api->pinWrite(PIN_STEP_A_CLK, IOPORT_LEVEL_LOW);
    pt_st_mot = &stato_motore_irq[STEPPER_A];
    i_Pos = &i_MotPosizione[STEPPER_A];
    mi_Pos = &mi_MotPosizione[STEPPER_A];
    opmot = &op_mot[STEPPER_A];
    FreqAct = &i_MotActFreq[STEPPER_A];
    StopMotFreq = &i_MotStopFreq[STEPPER_A];

    passo = MOT_controlla_passi_motore(STEPPER_A);
    if (passo)
    {
        switch (stato_motore_irq[STEPPER_A])       // setta la direzione con il pin di controllo
        {
        case ST_MOT_AVANTI:
            g_ioport.p_api->pinWrite(PIN_STEP_A_DIR, IOPORT_LEVEL_HIGH);
            i_MotPosizione[STEPPER_A]++;
            g_lVPos[STEPPER_A]++;
            g_ioport.p_api->pinWrite(PIN_STEP_A_CLK, IOPORT_LEVEL_HIGH);
            break;
        case ST_MOT_INDIETRO:
            g_ioport.p_api->pinWrite(PIN_STEP_A_DIR, IOPORT_LEVEL_LOW);
            i_MotPosizione[STEPPER_A]--;
            g_lVPos[STEPPER_A]--;
            g_ioport.p_api->pinWrite(PIN_STEP_A_CLK, IOPORT_LEVEL_HIGH);
            break;
        }
    }

    if (TestPassi)
    {
        checkPhotoError(STEPPER_A);
    }

    if (Com_Irq[STEPPER_A] & COM_MOTORE_RESET)      // richiesta di reset
    {
        if (passo)
        {
            if (stato_home_mot[STEPPER_A] == ST_HOME_CHECK_1_FORO)     // in attesa di fotocellula
            {
                if (HomeSensor[STEPPER_A] == TRUE)      // qui in reset
                {
                    Reset_Run[STEPPER_A] = FALSE;
                    m_MotPosizione[STEPPER_A] = g_MaxSteps[STEPPER_CROSS];
                    i_MotPosizione[STEPPER_A] = g_MaxSteps[STEPPER_CROSS];
                    mi_MotPosizione[STEPPER_A] = g_MaxSteps[STEPPER_CROSS];
                    g_lVPos[STEPPER_A] = g_MaxSteps[STEPPER_CROSS];
                    g_bReqHomeStepperA = false;//End Home Request
                    Comando_Motore[STEPPER_A] |= COM_MOTORE_STOP;
                }
            }
        }
    }

}





//********************************************************
//  chiamata da IRQ da timer
//********************************************************
void irq_motore_2_callback (timer_callback_args_t * p_args)
{
    ssp_err_t err;
    UINT8 passo;

    g_ioport.p_api->pinWrite(PIN_STEP_B_CLK, IOPORT_LEVEL_LOW);
    pt_st_mot = &stato_motore_irq[STEPPER_B];
    i_Pos = &i_MotPosizione[STEPPER_B];
    mi_Pos = &mi_MotPosizione[STEPPER_B];
    opmot = &op_mot[STEPPER_B];
    FreqAct = &i_MotActFreq[STEPPER_B];
    StopMotFreq = &i_MotStopFreq[STEPPER_B];

    passo = MOT_controlla_passi_motore(STEPPER_B);
    if (passo)
    {
        switch (stato_motore_irq[STEPPER_B])       // setta la direzione con il pin di controllo
        {
        case ST_MOT_AVANTI:
            g_ioport.p_api->pinWrite(PIN_STEP_B_DIR, IOPORT_LEVEL_LOW);
            i_MotPosizione[STEPPER_B]++;
            g_lVPos[STEPPER_B]++;
            g_ioport.p_api->pinWrite(PIN_STEP_B_CLK, IOPORT_LEVEL_HIGH);
            break;
        case ST_MOT_INDIETRO:
            g_ioport.p_api->pinWrite(PIN_STEP_B_DIR, IOPORT_LEVEL_HIGH);
            i_MotPosizione[STEPPER_B]--;
            g_lVPos[STEPPER_B]--;
            g_ioport.p_api->pinWrite(PIN_STEP_B_CLK, IOPORT_LEVEL_HIGH);
            break;
        }
    }

    if (TestPassi)
    {
        checkPhotoError(STEPPER_B);
    }



    if (Com_Irq[STEPPER_B] & COM_MOTORE_RESET)      // richiesta di reset
    {
        if (passo)
        {
            if (stato_home_mot[STEPPER_B] == ST_HOME_CHECK_1_FORO)     // in attesa di fotocellula
            {
                if (HomeSensor[STEPPER_B] == TRUE)      // qui in reset
                {
                    Reset_Run[STEPPER_B] = FALSE;
                    m_MotPosizione[STEPPER_B] = g_MaxSteps[STEPPER_LONG];
                    i_MotPosizione[STEPPER_B] = g_MaxSteps[STEPPER_LONG];
                    mi_MotPosizione[STEPPER_B] = g_MaxSteps[STEPPER_LONG];
                    g_lVPos[STEPPER_B] = g_MaxSteps[STEPPER_LONG];
                    g_bReqHomeStepperB = false;//End Home Request
                    Comando_Motore[STEPPER_B] |= COM_MOTORE_STOP;
                }
            }
        }
    }

}



//********************************************************
//  chiamata da IRQ da timer
//********************************************************
void irq_motore_3_callback (timer_callback_args_t * p_args)
{
    ssp_err_t err;
    UINT8 passo;
    UINT8 flag;

    g_ioport.p_api->pinWrite(PIN_STEP_C_CLK, IOPORT_LEVEL_LOW);
    pt_st_mot = &stato_motore_irq[STEPPER_C];
    i_Pos = &i_MotPosizione[STEPPER_C];
    mi_Pos = &mi_MotPosizione[STEPPER_C];
    opmot = &op_mot[STEPPER_C];
    FreqAct = &i_MotActFreq[STEPPER_C];
    StopMotFreq = &i_MotStopFreq[STEPPER_C];

    passo = MOT_controlla_passi_motore(STEPPER_C);
    if (passo)
    {
        switch (stato_motore_irq[STEPPER_C])       // setta la direzione con il pin di controllo
        {
        case ST_MOT_AVANTI:
            g_ioport.p_api->pinWrite(PIN_STEP_C_DIR, IOPORT_LEVEL_HIGH);
            i_MotPosizione[STEPPER_C]++;
            g_lVPos[STEPPER_C]++;
            g_ioport.p_api->pinWrite(PIN_STEP_C_CLK, IOPORT_LEVEL_HIGH);
            break;
        case ST_MOT_INDIETRO:
            g_ioport.p_api->pinWrite(PIN_STEP_C_DIR, IOPORT_LEVEL_LOW);
            i_MotPosizione[STEPPER_C]--;
            g_lVPos[STEPPER_C]--;
            g_ioport.p_api->pinWrite(PIN_STEP_C_CLK, IOPORT_LEVEL_HIGH);
            break;
        }
    }

    if (g_bReqHomeStepperC == TRUE)     // se richiesta di Home esegue controlli
    {
        if (passo)
        {
            step_home++;
            flag = FALSE;
            switch (ee_FilterConfig.uwType)
            {
            case FILTER_1_HOLE:         // ****  filtro tradizionale con 1 FORO  ****
                if ((stato_home_mot[STEPPER_C] == ST_HOME_CHECK_1_FORO) && (HomeSensor[STEPPER_C] == TRUE))
                {
                    flag = TRUE;
                }
                break;

            case FILTER_5_HOLES:         // ****  filtro con 5 FORI  ****
                switch (stato_home_mot[STEPPER_C])
                {
                case ST_HOME_CHECK_1_FORO:
                    if (HomeSensor[STEPPER_C] == TRUE)
                    {
                        step_home = 0;
                        stato_home_mot[STEPPER_C] = ST_HOME_CHECK_1_PIENO;
                    }
                    break;

                case ST_HOME_CHECK_1_PIENO:
                    if (HomeSensor[STEPPER_C] == FALSE)
                    {
                        stato_home_mot[STEPPER_C] = ST_HOME_CHECK_2_FORO;
                    }
                    break;

                case ST_HOME_CHECK_2_FORO:
                    if (HomeSensor[STEPPER_C] == TRUE)
                    {                       // se trova il secondo foro allora ferma tutto
                        if ((step_home > ee_FilterConfig.StepsResetMin) && (step_home < ee_FilterConfig.StepsResetMax))
                        {
                            flag = TRUE;
                        }
                    }
                    else
                    {
                        if (step_home > ee_FilterConfig.StepsResetMax)
                        {
                            stato_home_mot[STEPPER_C] = ST_HOME_CHECK_1_FORO;
                        }
                    }
                    break;
                }
                break;
            }

            if (flag == TRUE)
            {
                g_bReqHomeStepperC = FALSE;
                Comando_Motore[STEPPER_C] |= COM_MOTORE_STOP;
            }
        }
    }

}



//********************************************************
//  chiamata da IRQ da timer
//********************************************************
void irq_motore_4_callback (timer_callback_args_t * p_args)
{
    ssp_err_t err;
    UINT8 passo;

    g_ioport.p_api->pinWrite(PIN_STEP_D_CLK, IOPORT_LEVEL_LOW);
    pt_st_mot = &stato_motore_irq[STEPPER_D];
    i_Pos = &i_MotPosizione[STEPPER_D];
    mi_Pos = &mi_MotPosizione[STEPPER_D];
    opmot = &op_mot[STEPPER_D];
    FreqAct = &i_MotActFreq[STEPPER_D];
    StopMotFreq = &i_MotStopFreq[STEPPER_D];

    passo = MOT_controlla_passi_motore(STEPPER_D);
    if (passo)
    {
        switch (stato_motore_irq[STEPPER_D])       // setta la direzione con il pin di controllo
        {
        case ST_MOT_AVANTI:
            g_ioport.p_api->pinWrite(PIN_STEP_D_DIR, IOPORT_LEVEL_HIGH);
            i_MotPosizione[STEPPER_D]++;
            g_lVPos[STEPPER_D]++;
            g_ioport.p_api->pinWrite(PIN_STEP_D_CLK, IOPORT_LEVEL_HIGH);
            break;
        case ST_MOT_INDIETRO:
            g_ioport.p_api->pinWrite(PIN_STEP_D_DIR, IOPORT_LEVEL_LOW);
            i_MotPosizione[STEPPER_D]--;
            g_lVPos[STEPPER_D]--;
            g_ioport.p_api->pinWrite(PIN_STEP_D_CLK, IOPORT_LEVEL_HIGH);
            break;
        }
    }

    if (TestPassi)
    {
        checkPhotoError(STEPPER_D);
    }



    if (Com_Irq[STEPPER_D] & COM_MOTORE_RESET)      // richiesta di reset
    {
        if (passo)
        {
            if (stato_home_mot[STEPPER_D] == ST_HOME_CHECK_1_FORO)     // in attesa di fotocellula
            {
                if (HomeSensor[STEPPER_D] == TRUE)      // qui in reset
                {
                    Reset_Run[STEPPER_D] = FALSE;
                    m_MotPosizione[STEPPER_D] = g_MaxSteps[STEPPER_LONG2];
                    i_MotPosizione[STEPPER_D] = g_MaxSteps[STEPPER_LONG2];
                    mi_MotPosizione[STEPPER_D] = g_MaxSteps[STEPPER_LONG2];
                    g_lVPos[STEPPER_D] = g_MaxSteps[STEPPER_LONG2];
                    g_bReqHomeStepperD = false;//End Home Request
                    Comando_Motore[STEPPER_D] |= COM_MOTORE_STOP;
                }
            }
        }
    }

}



//******************************************
//******************************************
//**  Controlla se arrivato in posizione  **
//******************************************
//******************************************
UINT8 MOT_controlla_passi_motore(UINT8 stepper)
{
    UINT8 flag_mot;

    flag_mot = FALSE;
    if (*i_Pos < *mi_Pos)     // deve andare avanti
    {
        flag_mot = TRUE;
        switch (*pt_st_mot)
        {
        case ST_MOT_FERMO:
            *opmot = OP_SPEED_RAMPA;
            *pt_st_mot = ST_MOT_AVANTI;
            break;
        case ST_MOT_AVANTI:
            *opmot = OP_SPEED_RAMPA;
            break;
        case ST_MOT_INDIETRO:
            *opmot = OP_SPEED_DEC;      // rallenta fino a fermarsi
            if (*FreqAct <= *StopMotFreq)
            {
                flag_mot = FALSE;
                *pt_st_mot = ST_MOT_FERMO;
            }
            break;
        }
    }
    else if (*i_Pos > *mi_Pos)        // deve tornare indietro
    {
        flag_mot = TRUE;
        switch (*pt_st_mot)
        {
        case ST_MOT_FERMO:
            *opmot = OP_SPEED_RAMPA;
            *pt_st_mot = ST_MOT_INDIETRO;
            break;
        case ST_MOT_INDIETRO:
            *opmot = OP_SPEED_RAMPA;
            break;
        case ST_MOT_AVANTI:
            *opmot = OP_SPEED_DEC;      // rallenta fino a fermarsi
            if (*FreqAct <= *StopMotFreq)
            {
                flag_mot = FALSE;
                *pt_st_mot = ST_MOT_FERMO;
            }
            break;
        }
    }
    else                    // qui ha raggiunto i passi, controlla la velocita' se possibile fermarsi
    {
        if (*pt_st_mot != ST_MOT_FERMO)
        {
            if (*FreqAct <= *StopMotFreq)
            {
                *opmot = OP_SPEED_FERMO;
                *pt_st_mot = ST_MOT_FERMO;
            }
            else
            {
                *opmot = OP_SPEED_DEC;        // rallenta al minimo
                flag_mot = TRUE;
            }
        }
    }

    return flag_mot;
}















// Gestione timer al 1ms
void irq_timer_1ms(timer_callback_args_t * p_args)
{
    sys_clock();
}





// gestione ricezione messaggi canbus
void irq_can_callback(can_callback_args_t * p_args)
{
    gestione_irq_canbus(p_args);
}









//******************************************************
//  chiamata al ms
//******************************************************
void sys_clock (void)
{
    ssp_err_t api_err;
    uchar ucIndex;
    UINT8 temp, i;
    static uchar s_ucLedTurn=0;
    uchar ucTemp;
    static uint16_t dacvalue = 0;

    g_uwTimeMilliSecondCounter++;               //System Time: 1mSec

    CAN_tx_msg();  // controlla trasmissione
    gestione_antirimbalzo();
//TODO:    sys_adc_driver();
    read_hw_manopole();


/*
    if (g_ucWorkModality == CONFIGURATION)
    {
        ricevi_byte_seriale();      // accoda nella fifo
    }
*/
//TODO:    if (reset_driver == TRUE)
//    {
//        if (sby_all_step == 1)
//        {
//            sby_all_step = 0;
//        }
//        else
//        {
//            sby_all_step = 1;
//            reset_driver = FALSE;
//        }
//    }


    MOT_ricalcola_motore(STEPPER_A);
    MOT_ricalcola_motore(STEPPER_B);
    MOT_ricalcola_motore(STEPPER_C);
    MOT_ricalcola_motore(STEPPER_D);


    if (++ContaIrq >= 4)        // divisione dei compiti sotto irq per non avere troppo tempo di cpu
        ContaIrq = 0;

    switch (ContaIrq)
    {
    case 0:
        break;

    case 1:
        if (lcd_stop == FALSE)
        {
            write_char();       // scrive un char su display
        }
        break;

    case 2:
        gestione_idle();
        break;

    case 3:
        if (lcd_stop == FALSE)
        {
            write_char();       // scrive un char su display
        }
        break;
    }

    Steppers[STEPPER_A].StepperData.StepperBits.bHome = HomeSensor[STEPPER_A];
//    Steppers[STEPPER_A].StepperData.StepperBits.bFull = HOME_A;
    Steppers[STEPPER_B].StepperData.StepperBits.bHome = HomeSensor[STEPPER_B];
//    Steppers[STEPPER_B].StepperData.StepperBits.bFull = HOME_B;
    Steppers[STEPPER_C].StepperData.StepperBits.bHome = HOME_C;

    //Delay - Timers
    for (ucIndex=0; ucIndex<MAX_STEPPER; ucIndex++){
        if (g_uwHomeDelay[ucIndex])
            g_uwHomeDelay[ucIndex]--;
        if (g_uwLowPowerDelay[ucIndex])
            g_uwLowPowerDelay[ucIndex]--;
        if (g_uwHighPowerDelay[ucIndex])
            g_uwHighPowerDelay[ucIndex]--;
        if (g_uwDelayedTimer[ucIndex])
            g_uwDelayedTimer[ucIndex]--;

        if (timer_motor_on[ucIndex])
        {
            if (--timer_motor_on[ucIndex] == 0)
            {
//                stato_mot[i].str.in_moto = 0;
                Steppers[ucIndex].StepperData.StepperBits.bMoving = 0;
            }
        }
    }


    timer_master_1ms();

    if (timer_invio_tasti_villa)
        timer_invio_tasti_villa--;

    if (timer_rx_ser)
        timer_rx_ser--;
    if (timer_rx_metro)
        timer_rx_metro--;
    if (timer_LED_GREEN)
        timer_LED_GREEN--;
    if (g_uwControlDelay)
        g_uwControlDelay--;
    if (g_uwFilterDelay)        //0.53
        g_uwFilterDelay--;
    if (g_uwTestTimer)
        g_uwTestTimer--;
    if (g_uwCalibrationTimer)
        g_uwCalibrationTimer--;
    if (g_uwDisplayManagerTimer)
        g_uwDisplayManagerTimer--;
    if (g_uwPushLampButtonDelay)
        g_uwPushLampButtonDelay--;
    if (g_uwPushFilterButtonDelay)
        g_uwPushFilterButtonDelay--;
    if (g_uwAlarmCounterOrient)
        g_uwAlarmCounterOrient--;
    if (g_uwTimerLed)
        g_uwTimerLed--;
    if (g_uwPushButtonDelay)
        g_uwPushButtonDelay--;
    if (g_uwWaitResetDelay)
        g_uwWaitResetDelay--;

    if (timerWaitIride)
        timerWaitIride--;

#ifdef SYS_CANOPEN
    if (timer_canopen)
        timer_canopen--;
    if (timer_canopen_wait)
        timer_canopen_wait--;
#endif


    g_bTimeCan = true;

        timer_invio_7F0++;
        timer_invio_7F5++;
        timer_invio_inclinometro++;
/*
    if (UpdateRates.un7F0){
        if (!(g_uwTimeMilliSecondCounter%UpdateRates.un7F0)){
            g_b7F0Time=true;
        }
    }
*/
    if (UpdateRates.un7F1){
        if (!(g_uwTimeMilliSecondCounter % UpdateRates.un7F1)){
            g_b7F1Time=true;
        }
    }
    if (UpdateRates.un7F9){
        if (!(g_uwTimeMilliSecondCounter % UpdateRates.un7F9)){
            g_b7F9Time=true;
        }
    }
    if (UpdateRates.un7FC){
        if (!(g_uwTimeMilliSecondCounter % UpdateRates.un7FC)){
            g_b7FCTime=true;
        }
    }
    if (UpdateRates.un100){
        if (!(g_uwTimeMilliSecondCounter % UpdateRates.un100))
            g_b100Time=true;
    }
    if (UpdateRates.un101){
        if (!(g_uwTimeMilliSecondCounter % UpdateRates.un101))
            g_b101Time=true;
    }
    if (UpdateRates.un103){
        if (!(g_uwTimeMilliSecondCounter % UpdateRates.un103))
            g_b103Time=true;
    }
    if (UpdateRates.un082){
        if (!(g_uwTimeMilliSecondCounter % UpdateRates.un082))
            g_b082Time=true;
    }
    if (UpdateRates.un083){
        if (!(g_uwTimeMilliSecondCounter % UpdateRates.un083))
            g_b083Time=true;
    }
    if (UpdateRates.un084){
        if (!(g_uwTimeMilliSecondCounter % UpdateRates.un084))
            g_b084Time=true;
    }
    if (UpdateRates.un085){
        if (!(g_uwTimeMilliSecondCounter % UpdateRates.un085))
            g_b085Time=true;
    }

    //LED MANAGER
    if (timer_led_config)
    {
        SET_LED_RED;
        SET_LED_YELLOW;
        SET_LED_GREEN;
//        if (++s_ucLedTurn > 2)
//            s_ucLedTurn = 0;
//        switch (s_ucLedTurn)
//        {
//        case 0:
//            SET_LED_RED;
//            break;
//        case 1:
//            SET_LED_GREEN;
//            break;
//        case 2:
//            SET_LED_YELLOW;
//            break;
//        }
    }
    else
    {
        switch (s_ucLedTurn)
        {
        case 0:
            //RED, YELLOW or GREEN
            switch (g_ucLedToBeOn)
            {
            case RED:
                if (allarmi_totale & ALARM_CHECK)
                {
                    if (led_error)
                    {
                        SET_LED_RED;
                        RES_LED_YELLOW;
                        RES_LED_GREEN;
                    }
                    else
                    {
                        RES_LED_RED;
                        RES_LED_YELLOW;
                        RES_LED_GREEN;
                        __asm("NOP ");
                    }
                }
                else
                {
                    SET_LED_RED;
                    RES_LED_YELLOW;
                    RES_LED_GREEN;
                }
                g_ucStatusLed = RED;
                break;
            case YELLOW:
                SET_LED_YELLOW;
                RES_LED_RED;
                RES_LED_GREEN;
                g_ucStatusLed = YELLOW;
                break;
            case GREEN:
                SET_LED_GREEN;
                RES_LED_YELLOW;
                RES_LED_RED;
                g_ucStatusLed = GREEN;
                break;
            }
            break;
        case 1:
            if (HomeSensor[STEPPER_A])
            {
                SET_LED_D19;
            }
            else
            {
                RES_LED_D19;
            }
            break;
        case 2:
            if (HomeSensor[STEPPER_B])
            {
                SET_LED_D20;
            }
            else
            {
                RES_LED_D20;
            }
            break;
        case 3:
            if (HomeSensor[STEPPER_C])
            {
                SET_LED_D21;
            }
            else
            {
                RES_LED_D21;
            }
            break;
        case 4:
            if (HomeSensor[STEPPER_D])
            {
                SET_LED_D22;
            }
            else
            {
                RES_LED_D22;
            }
            break;
        case 5:
            if (FULL_B)
            {
                //TODO: led HOME_                SET_LED_D15;
            }
            break;
        }
        if (++s_ucLedTurn > 5)
            s_ucLedTurn = 0;
    }

/*
    if (Steppers[STEPPER_A].StepperData.StepperBits.bMoving || Steppers[STEPPER_B].StepperData.StepperBits.bMoving)
    {
        g_uwMovingTimer = DELAY_MOVING;
    }
    else
    {
        if (g_uwMovingTimer)
            g_uwMovingTimer--;
    }
*/


    if (++g_uw10MsecCounter>=10){               //10 msec
        g_uw10MsecCounter=0;

        tx_ser_char();      // trasmette un carattere seriale se necessario
        tx_ser_metro();      // trasmette un carattere seriale se necessario

        for (i = STEPPER_A; i <= STEPPER_D; i++)
        {
            if (Timeout_stepper[i])
                Timeout_stepper[i]--;
        }

        if (timer_scorrimento)
            timer_scorrimento--;

        if (timer_led_config)
            timer_led_config--;
        if (timer_stop_a)
            timer_stop_a--;
        if (timer_stop_b)
            timer_stop_b--;
        if (timer_stop_iride)
            timer_stop_iride--;
        if (timer_res_a)
            timer_res_a--;
        if (timer_res_b)
            timer_res_b--;
        if (timer_res_d)
            timer_res_d--;
        if (rimb_key)
            rimb_key--;
        if (timer_spost_lamelle)
            timer_spost_lamelle--;

        if (++g_uw50MsecCounter>=25)
        {           //250 msec
            g_uw50MsecCounter=0;
            g_bTimeCanStatusMessage = true;
        }

        if (++g_uw100MsecCounter>=10)
        {           //100 msec
            g_bInclSync = true;
            g_uw100MsecCounter=0;
            visua_main++;

            if (timer_wait_bucky[RX_UNDER])
                timer_wait_bucky[RX_UNDER]--;
            if (timer_wait_bucky[RX_RIGHT])
                timer_wait_bucky[RX_RIGHT]--;
            if (timer_wait_bucky[RX_LEFT])
                timer_wait_bucky[RX_LEFT]--;

            if (g_uwLightTimer)
                g_uwLightTimer--;

            if (timer_keys_switch < 10000)
                timer_keys_switch++;
            if (timer_keys_lamp < 10000)
                timer_keys_lamp++;

            if (timer_alarm_calib)
                timer_alarm_calib--;

            if (timer_test)
                timer_test--;

            if (timer_luce_test)
                timer_luce_test--;

            if (TimerNewIncl)
                TimerNewIncl--;

            if (timer_metro)        // invio messaggio ogni 0.5 secondi
            {
                timer_metro--;
            }
            else
            {
                timer_metro = 4;
                buff_tx_metro[0] = 0x00;
                buff_tx_metro[1] = 0x54;        // risultato in cm
                punt_tx_metro = 0;
                _ok_tx_metro = TRUE;
            }

            if (timer_iride)
                timer_iride--;

            if (timer_display)
            {
                timer_display--;
            }
            else
            {
                timer_display = 3;
                write_msg = TRUE;
            }

            if (--timer_blink == 0)
            {
                timer_blink = 4;
                if (led_error)
                    led_error = 0;
                else
                    led_error = 1;
            }

            if (++g_uw1000MsecCounter>=10){     //  1 sec
                g_uw1000MsecCounter=0;

                if (g_uwTimeSecondCounter < 65000)
                {
                    g_uwTimeSecondCounter++;
                    if ((g_uwTimeSecondCounter <= 10) && (g_bAsr003Ok == FALSE))
                    {
                        g_b7AETime=true;
                    }
                }

                g_bTimeToShowAngle = true;

                if (timeout_taratura)           // per uscita automatica dal menu di taratura
                    timeout_taratura--;

                if (tempo_gio)
                    tempo_gio--;

                if (timer_rotazione)
                    timer_rotazione--;

                manda_7d0 = true;
                g_bTimeToShowFilterError=true;

                //Delay - Timers
                for (ucIndex=0; ucIndex<MAX_STEPPER; ucIndex++){
                    if (g_uwHomeTimeout[ucIndex])
                        g_uwHomeTimeout[ucIndex]--;
                    if (g_uwNoTqPowerDelay[ucIndex]&&g_uwNoTqPowerDelay[ucIndex]<0xFF)
                        g_uwNoTqPowerDelay[ucIndex]--;
                }
                if (++g_uw10000MsecCounter>=10){        //  10 sec
                    g_uw10000MsecCounter=0;
                }
            }
        }
    }

    //STEPPER C
    if (HOME_C == TRUE)     // conta i fori del filtro
    {
        if (home_cprec == 0)
        {
            home_cprec = 1;
            numero_fori_filter++;
        }
    }
    else
    {
        home_cprec = 0;
    }


    scan_kb();      // usata in taratura


    __asm ("NOP");
    __asm ("NOP");
    __asm ("NOP");
}


//************************************************
//  mette a riposo un motore se necessario
//************************************************
void gestione_idle(void)
{
    UINT8 i;

    for (i = STEPPER_A; i < MAX_STEPPER; i++)
    {
        if (timer_idle[i])
        {
            if (--timer_idle[i] == 0)
            {
                switch (i)
                {
                case STEPPER_A:
                    dac_value[STEPPER_A] = DAC_VALORI_IDLE[STEPPER_A];
                    break;

                case STEPPER_B:
                    dac_value[STEPPER_B] = DAC_VALORI_IDLE[STEPPER_B];
                    break;

                case STEPPER_C:
                    dac_value[STEPPER_C] = DAC_VALORI_IDLE[STEPPER_C];
                    break;

                case STEPPER_D:
                    dac_value[STEPPER_D] = DAC_VALORI_IDLE[STEPPER_D];
                    break;
                }
            }
        }
    }

}







static ssp_err_t gpt_divisor_select(gpt_instance_ctrl_t * const p_ctrl,
                                    uint64_t            *       p_period_counts,
                                    gpt_pclk_div_t      * const p_divisor)
{
    /** temp_period now represents PCLK counts, but it could potentially overflow 32 bits.  If so, convert it here and
     *  set divisor appropriately.
     */
    gpt_pclk_div_t temp_div = GPT_PCLK_DIV_BY_1;
    uint64_t max_counts     = GPT_MAX_CLOCK_COUNTS_32;

    if (TIMER_VARIANT_16_BIT == p_ctrl->variant)
    {
        max_counts = GPT_MAX_CLOCK_COUNTS_16;
    }
    while ((*p_period_counts > max_counts) && (temp_div < GPT_PCLK_DIV_BY_1024))
    {
        *p_period_counts >>= 2;
        temp_div++;
    }

    /** If period is still too large, return error */
    GPT_ERROR_RETURN((*p_period_counts <= max_counts), SSP_ERR_INVALID_ARGUMENT);

    *p_divisor = temp_div;

    return SSP_SUCCESS;
}


static ssp_err_t gpt_period_to_pclk (gpt_instance_ctrl_t * const p_ctrl,
                                     timer_size_t          const period,
                                     timer_unit_t          const unit,
                                     timer_size_t        * const p_period_pclk,
                                     gpt_pclk_div_t      * const p_divisor)
{
    uint64_t temp_period = period;
    ssp_err_t err = SSP_SUCCESS;

    /** Read the current PCLK frequency from the clock module. */
    uint32_t pclk_freq_hz = 0;
    g_cgc_on_cgc.systemClockFreqGet(CGC_SYSTEM_CLOCKS_PCLKD, &pclk_freq_hz);

    /** Convert period to PCLK counts so it can be set in hardware. */
    switch (unit)
    {
        case TIMER_UNIT_PERIOD_RAW_COUNTS:
            temp_period = period;
            break;
        case TIMER_UNIT_FREQUENCY_KHZ:
            temp_period = (pclk_freq_hz * 1ULL) / (1000 * period);
            break;
        case TIMER_UNIT_FREQUENCY_HZ:
            temp_period = (pclk_freq_hz * 1ULL) / period;
            break;
        case TIMER_UNIT_PERIOD_NSEC:
            temp_period = (period * (pclk_freq_hz * 1ULL)) / 1000000000;
            break;
        case TIMER_UNIT_PERIOD_USEC:
            temp_period = (period * (pclk_freq_hz * 1ULL)) / 1000000;
            break;
        case TIMER_UNIT_PERIOD_MSEC:
            temp_period = (period * (pclk_freq_hz * 1ULL)) / 1000;
            break;
        case TIMER_UNIT_PERIOD_SEC:
            temp_period = period * (pclk_freq_hz * 1ULL);
            break;
        default:
            err = SSP_ERR_INVALID_ARGUMENT;
            break;
    }
    GPT_ERROR_RETURN(SSP_SUCCESS == err, err);

    err = gpt_divisor_select(p_ctrl, &temp_period, p_divisor);
    GPT_ERROR_RETURN(SSP_SUCCESS == err, err);

    /** If the period is valid, return to caller. */
    *p_period_pclk = (timer_size_t) temp_period;

    return SSP_SUCCESS;
} /* End of function gpt_period_to_pclk */





ssp_err_t GIO_GPT_PeriodSet (timer_ctrl_t * const p_api_ctrl,
                           timer_size_t   const period,
                           timer_unit_t const   unit)
{
    gpt_instance_ctrl_t * p_ctrl = (gpt_instance_ctrl_t *) p_api_ctrl;

    /** Delay must be converted to PCLK counts before it can be set in registers */
    ssp_err_t     err          = SSP_SUCCESS;
    timer_size_t      pclk_counts  = 0;
    gpt_pclk_div_t pclk_divisor = GPT_PCLK_DIV_BY_1;
    err = gpt_period_to_pclk(p_ctrl, period, unit, &pclk_counts, &pclk_divisor);

    /** Make sure period is valid. */
    GPT_ERROR_RETURN((SSP_SUCCESS == err), err);

    /** Store current status, then stop timer before setting divisor register */
    GPT_BASE_PTR p_gpt_reg = (GPT_BASE_PTR) p_ctrl->p_reg;
//    gpt_start_status_t status = HW_GPT_CounterStartBitGet(p_gpt_reg);
 //   HW_GPT_CounterStartStop(p_gpt_reg, GPT_STOP);
    HW_GPT_DivisorSet(p_gpt_reg, pclk_divisor);
    HW_GPT_TimerCycleSet(p_gpt_reg, pclk_counts - 1);

    /** Reset counter in case new cycle is less than current count value, then restore state (counting or stopped). */
//    HW_GPT_CounterSet(p_gpt_reg, 0);
//    HW_GPT_CounterStartStop(p_gpt_reg, status);

    return SSP_SUCCESS;
} /* End of function R_GPT_PeriodSet */





ssp_err_t MOT_Timer_StartStop (timer_ctrl_t * const p_api_ctrl, gpt_start_status_t status)
{
    gpt_instance_ctrl_t * p_ctrl = (gpt_instance_ctrl_t *) p_api_ctrl;

    /** Store current status, then stop timer before setting divisor register */
    GPT_BASE_PTR p_gpt_reg = (GPT_BASE_PTR) p_ctrl->p_reg;
    HW_GPT_CounterStartStop(p_gpt_reg, status);

    return SSP_SUCCESS;
}






//*****************************************
//*****************************************
//**  ricalcola la velocita' del motore  **
//*****************************************
//*****************************************
void MOT_ricalcola_motore(UINT8 stepper)
{
    UINT8 flag_set, i, flag_first, operazione;
    SINT32 temp32;
    UINT32 *ActFreq, *MinFreq, *MaxFreq;
    UINT8 *Comando;
    SINT32 *iPos, *miPos, *mPos;

    switch (stepper)
    {
    case STEPPER_A:
        ActFreq = &i_MotActFreq[STEPPER_A];       // assegna puntatore
        MinFreq = &i_MotMinFreq[STEPPER_A];
        MaxFreq = &i_MotMaxFreq[STEPPER_A];
        Comando = &Comando_Motore[STEPPER_A];
        iPos = &i_MotPosizione[STEPPER_A];
        miPos = &mi_MotPosizione[STEPPER_A];
        mPos = &m_MotPosizione[STEPPER_A];
        NumPassiRampa = &passi_rampa[STEPPER_A];
        break;
    case STEPPER_B:
        ActFreq = &i_MotActFreq[STEPPER_B];       // assegna puntatore
        MinFreq = &i_MotMinFreq[STEPPER_B];
        MaxFreq = &i_MotMaxFreq[STEPPER_B];
        Comando = &Comando_Motore[STEPPER_B];
        iPos = &i_MotPosizione[STEPPER_B];
        miPos = &mi_MotPosizione[STEPPER_B];
        mPos = &m_MotPosizione[STEPPER_B];
        NumPassiRampa = &passi_rampa[STEPPER_B];
        break;
    case STEPPER_C:
        ActFreq = &i_MotActFreq[STEPPER_C];       // assegna puntatore
        MinFreq = &i_MotMinFreq[STEPPER_C];
        MaxFreq = &i_MotMaxFreq[STEPPER_C];
        Comando = &Comando_Motore[STEPPER_C];
        iPos = &i_MotPosizione[STEPPER_C];
        miPos = &mi_MotPosizione[STEPPER_C];
        mPos = &m_MotPosizione[STEPPER_C];
        NumPassiRampa = &passi_rampa[STEPPER_C];
        break;
    case STEPPER_D:
        ActFreq = &i_MotActFreq[STEPPER_D];       // assegna puntatore
        MinFreq = &i_MotMinFreq[STEPPER_D];
        MaxFreq = &i_MotMaxFreq[STEPPER_D];
        Comando = &Comando_Motore[STEPPER_D];
        iPos = &i_MotPosizione[STEPPER_D];
        miPos = &mi_MotPosizione[STEPPER_D];
        mPos = &m_MotPosizione[STEPPER_D];
        NumPassiRampa = &passi_rampa[STEPPER_D];
        break;
    }

    flag_first = FALSE;

    if (flag_motore[stepper] == FALSE)
    {
              // controlla se deve stoppare il motore
        if (*Comando & COM_MOTORE_STOP)
        {
            *Comando &= ~COM_MOTORE_STOP;
            *Comando &= ~COM_MOTORE_RESET;
            *Comando &= ~COM_MOTORE_BRAKE;
            Com_Irq[stepper] &= ~COM_MOTORE_RESET;

            switch (stepper)
            {
            case STEPPER_A:
                MOT_Timer_StartStop(g_timer_mot1.p_ctrl, GPT_STOP);
                GIO_GPT_PeriodSet(g_timer_mot1.p_ctrl, 20000, TIMER_UNIT_PERIOD_USEC);  // per 50 Hz
                *mPos = *iPos;
                *miPos = *iPos;
                *ActFreq = 1;
                op_mot[STEPPER_A] = OP_SPEED_FERMO;
                MOT_Timer_StartStop(g_timer_mot1.p_ctrl, GPT_START);
                break;
            case STEPPER_B:
                MOT_Timer_StartStop(g_timer_mot2.p_ctrl, GPT_STOP);
                GIO_GPT_PeriodSet(g_timer_mot2.p_ctrl, 20000, TIMER_UNIT_PERIOD_USEC);  // per 50 Hz
                *mPos = *iPos;
                *miPos = *iPos;
                *ActFreq = 1;
                op_mot[STEPPER_B] = OP_SPEED_FERMO;
                MOT_Timer_StartStop(g_timer_mot2.p_ctrl, GPT_START);
                break;
            case STEPPER_C:
                MOT_Timer_StartStop(g_timer_mot3.p_ctrl, GPT_STOP);
                GIO_GPT_PeriodSet(g_timer_mot3.p_ctrl, 20000, TIMER_UNIT_PERIOD_USEC);  // per 50 Hz
//                *mPos = *iPos;
//                *miPos = *iPos;
                *iPos = 0;
                *mPos = 0;
                *miPos = 0;
                *ActFreq = 1;
                op_mot[STEPPER_C] = OP_SPEED_FERMO;
                MOT_Timer_StartStop(g_timer_mot3.p_ctrl, GPT_START);
                break;
            case STEPPER_D:
                MOT_Timer_StartStop(g_timer_mot4.p_ctrl, GPT_STOP);
                GIO_GPT_PeriodSet(g_timer_mot4.p_ctrl, 20000, TIMER_UNIT_PERIOD_USEC);  // per 50 Hz
                *mPos = *iPos;
                *miPos = *iPos;
                *ActFreq = 1;
                op_mot[STEPPER_D] = OP_SPEED_FERMO;
                MOT_Timer_StartStop(g_timer_mot4.p_ctrl, GPT_START);
                break;
            }
        }
                        // controlla se deve spegnere il motore
        if (*Comando & COM_MOTORE_RELEASE)
        {
            *Comando &= ~COM_MOTORE_RELEASE;
            switch (stepper)
            {
            case STEPPER_A:
                g_ioport.p_api->pinWrite(PIN_STEP_A_ENABLE, IOPORT_LEVEL_LOW);
                break;
            case STEPPER_B:
                g_ioport.p_api->pinWrite(PIN_STEP_B_ENABLE, IOPORT_LEVEL_LOW);
                break;
            case STEPPER_C:
                g_ioport.p_api->pinWrite(PIN_STEP_C_ENABLE, IOPORT_LEVEL_LOW);
                break;
            case STEPPER_D:
                g_ioport.p_api->pinWrite(PIN_STEP_D_ENABLE, IOPORT_LEVEL_LOW);
                break;
            }
            return;
        }


        if (*Comando & COM_MOTORE_RESET)
        {
            if ((Com_Irq[stepper] & COM_MOTORE_RESET) == 0)
            {
                flag_first = TRUE;
                timer_motor_on[stepper] = TIME_WAIT_STOP;

                switch (stato_home_mot[stepper])
                {
                case ST_HOME_OUT_SENS:
//                    if (stepper == STEPPER_D)
//                    {
//                        Timeout_stepper[stepper] = 0;
//                        stato_home_mot[stepper] = ST_HOME_WAIT_OUT_SENS;
//                    }
//                    else
                    {
                        if (HomeSensor[stepper] == TRUE)       // se in home deve uscire dalla fotocellula
                        {
                            if (*iPos < -1000000L)
                                *iPos = -1000000L;
                            *miPos = -2000000000L;      // posizione da raggiungere (2 miliardi)
                            *mPos = -2000000000L;      // posizione da raggiungere (2 miliardi)
                            if (stepper != STEPPER_C)
                            {
                                Timeout_stepper[stepper] = 100;     // in 1/100 sec
                            }
                            else
                            {
                                Timeout_stepper[stepper] = 40;     // in 1/100 sec
                            }
                            stato_home_mot[stepper] = ST_HOME_WAIT_OUT_SENS;
                        }
                        else
                        {
                            Timeout_stepper[stepper] = 0;
                            stato_home_mot[stepper] = ST_HOME_WAIT_OUT_SENS;
                        }
                    }
                    break;

                case ST_HOME_WAIT_OUT_SENS:
                    if (Timeout_stepper[stepper] == 0)
                    {
                        if (stepper != STEPPER_C)
                        {
                            if (*iPos > 1000000L)
                                *iPos = 1000000L;
                            *miPos = 2000000000L;      // posizione da raggiungere (2 miliardi)
                            *mPos = 2000000000L;      // posizione da raggiungere (2 miliardi)
                        }
                        else
                        {
                            *miPos = -2000000000L;      // posizione da raggiungere (2 miliardi)
                            *mPos = -2000000000L;      // posizione da raggiungere (2 miliardi)
                        }
                        Timeout_stepper[stepper] = TIMEOUT_RESET;
                        stato_home_mot[stepper] = ST_HOME_CHECK_1_FORO;
                        Com_Irq[stepper] = COM_MOTORE_RESET;
                    }
                    break;
                }
            }
            else
            {
                       // gestione Timeout Reset
                if (Timeout_stepper[stepper] == 0)      // scattato il timeout
                {
                    ScattatoTimeout[stepper] = TRUE;
                    *Comando |= COM_MOTORE_STOP;       // ferma motore per problemi
                }
            }
        }
        else
        {
            if (*mPos != *miPos)
            {
                flag_first = TRUE;
                timer_motor_on[stepper] = TIME_WAIT_STOP;
            }
        }
    }


    flag_set = FALSE;

    operazione = op_mot[stepper];

    if (*Comando & COM_MOTORE_BRAKE)
    {
        operazione = OP_SPEED_BRAKE;
    }
    else if (operazione == OP_SPEED_RAMPA)
    {
        if (*ActFreq > *MaxFreq)      // deve rallentare se superiore alla frequenza massima
        {
            operazione = OP_SPEED_DEC;
        }
        else        // controlla se in rampa o no
        {
            temp32 = labs(*iPos - *miPos);
            if (temp32 < (*NumPassiRampa + 1000UL))   // calcola frequenza teorica se inferiore ai passi rampa
            {
                temp32 *= i_MotFreqMotMult[stepper];
                temp32 += (*MinFreq) >> 8;       // frequenza teorica
                temp32 <<= 8;       // il controllo e' 24.8
            }
            else
            {
                temp32 = FREQ_OVER_TEORICA << 8;
            }
            if (*ActFreq < temp32)     // se frequenza inferiore allora incrementa
            {
                operazione = OP_SPEED_INC;
            }
            else if (*ActFreq > temp32)     // se frequenza superiore allora decrementa
            {
                operazione = OP_SPEED_DEC;
            }
        }
    }

                            // varia la velocita' del motore se necessario
    switch (operazione)
    {
    case OP_SPEED_FERMO:
        if (*ActFreq)
        {
            *ActFreq = 0;
            flag_set = TRUE;
        }
        frequenza_timer = 50UL << 8;
        break;

    case OP_SPEED_INC:
        if (*ActFreq == 0)
        {
            *ActFreq = *MinFreq;
            flag_set = TRUE;
        }
        else if (*ActFreq < *MaxFreq)      // controlla frequenza di rampa
        {
            *ActFreq += i_MotStepFreq[stepper];
            if (*ActFreq >= *MaxFreq)     // se ha raggiunto il massimo setta cambia stato
            {
                *ActFreq = *MaxFreq;
            }
            flag_set = TRUE;
        }
        frequenza_timer = *ActFreq;
        break;

    case OP_SPEED_DEC:
        if (*ActFreq > *MinFreq)      // controllo per decelerazione
        {
            if (*ActFreq > i_MotStepFreq[stepper])
            {
                *ActFreq -= i_MotStepFreq[stepper];
                if (*ActFreq < *MinFreq)
                {
                    *ActFreq = *MinFreq;
                }
            }
            else
            {
                *ActFreq = *MinFreq;
            }
            flag_set = TRUE;
        }
        frequenza_timer = *ActFreq;
        break;

    case OP_SPEED_BRAKE:
        if (*ActFreq > *MinFreq)      // controllo per decelerazione
        {
            if (*ActFreq > i_MotStepFreq[stepper])
            {
                *ActFreq -= i_MotStepFreq[stepper];
                if (*ActFreq <= *MinFreq)
                {
                    *Comando |= COM_MOTORE_STOP;       // ferma motore
                }
            }
            else
            {
                *Comando |= COM_MOTORE_STOP;       // ferma motore
            }
            flag_set = TRUE;
        }
        else
        {
            *Comando |= COM_MOTORE_STOP;       // ferma motore
        }
        frequenza_timer = *ActFreq;
        break;
    }


    if ((flag_set == TRUE) || (flag_first == TRUE))
    {
//        temp32 = ((MOTOR_CLOCK << 8) / frequenza_timer) - 1;
        temp32 = (1000000UL) / (frequenza_timer >> 8);
        switch (stepper)
        {
        case STEPPER_A:
            GIO_GPT_PeriodSet(g_timer_mot1.p_ctrl, temp32, TIMER_UNIT_PERIOD_USEC);
            if (flag_first == TRUE)
            {
                if (ee_ConfigBoards[STEPPER_A].ucStep != Set_MicroStep[STEPPER_A])
                {
                    Set_MicroStep[STEPPER_A] = ee_ConfigBoards[STEPPER_A].ucStep;
                    set_TB67S109_uStep(STEPPER_A, Set_MicroStep[STEPPER_A]);
                }
                g_ioport.p_api->pinWrite(PIN_STEP_A_ENABLE, IOPORT_LEVEL_HIGH);
            }
            break;
        case STEPPER_B:
            GIO_GPT_PeriodSet(g_timer_mot2.p_ctrl, temp32, TIMER_UNIT_PERIOD_USEC);
            if (flag_first == TRUE)
            {
                if (ee_ConfigBoards[STEPPER_B].ucStep != Set_MicroStep[STEPPER_B])
                {
                    Set_MicroStep[STEPPER_B] = ee_ConfigBoards[STEPPER_B].ucStep;
                    set_TB67S109_uStep(STEPPER_B, Set_MicroStep[STEPPER_B]);
                }
                g_ioport.p_api->pinWrite(PIN_STEP_B_ENABLE, IOPORT_LEVEL_HIGH);
            }
            break;
        case STEPPER_C:
            GIO_GPT_PeriodSet(g_timer_mot3.p_ctrl, temp32, TIMER_UNIT_PERIOD_USEC);
            if (flag_first == TRUE)
            {
                if (ee_ConfigBoards[STEPPER_C].ucStep != Set_MicroStep[STEPPER_C])
                {
                    Set_MicroStep[STEPPER_C] = ee_ConfigBoards[STEPPER_C].ucStep;
                    set_TB67S109_uStep(STEPPER_C, Set_MicroStep[STEPPER_C]);
                }
                g_ioport.p_api->pinWrite(PIN_STEP_C_ENABLE, IOPORT_LEVEL_HIGH);
            }
            break;
        case STEPPER_D:
            GIO_GPT_PeriodSet(g_timer_mot4.p_ctrl, temp32, TIMER_UNIT_PERIOD_USEC);
            if (flag_first == TRUE)
            {
                if (ee_ConfigBoards[STEPPER_D].ucStep != Set_MicroStep[STEPPER_D])
                {
                    Set_MicroStep[STEPPER_D] = ee_ConfigBoards[STEPPER_D].ucStep;
                    set_TB67S109_uStep(STEPPER_D, Set_MicroStep[STEPPER_D]);
                }
                g_ioport.p_api->pinWrite(PIN_STEP_D_ENABLE, IOPORT_LEVEL_HIGH);
            }
            break;
        }
        timer_idle[stepper] = 125;    // dec ogni 4 ms
    }

        // controllo potenza motore
    if (timer_idle[stepper])
    {
        dac_value[stepper] = DAC_VALORI_RUN[stepper];
    }
    if (flag_first == TRUE)
    {
        *miPos = *mPos;
    }


    if (*ActFreq)
    {
        timer_motor_on[stepper] = TIME_WAIT_STOP;
        timer_idle[stepper] = 125;    // dec ogni 4 ms
    }

    if (timer_motor_on[stepper])
    {
        Steppers[stepper].StepperData.StepperBits.bMoving = 1;
    }

/*
    if (timer_motor_on[stepper])
        stato_mot[stepper].str.in_moto = 1;
*/

}









//******************************************
//  antirimbalzo ingressi digitali
//******************************************
void gestione_antirimbalzo(void)
{
    UINT8 i;
    ioport_level_t pinlevel;
    ssp_err_t err;
    UINT8 temp_dig[NUM_DIG];

    for (i = 0; i < NUM_DIG; i++)   // azzera i bit
        temp_dig[i] = 0;

                       // stato dei pin hardware
    g_ioport.p_api->pinRead(PIN_HOME_A, &pinlevel);
    if (pinlevel != Steppers[STEPPER_A].ucLevel)     // confronta con valore impostato
        temp_dig[PHOTO_A] = 1;

    g_ioport.p_api->pinRead(PIN_HOME_B, &pinlevel);
    if (pinlevel != Steppers[STEPPER_B].ucLevel)
        temp_dig[PHOTO_B] = 1;

    g_ioport.p_api->pinRead(PIN_HOME_C, &pinlevel);
    if (pinlevel != ee_ConfigBoards[STEPPER_C].ucPhoto)
        temp_dig[PHOTO_C] = 1;

    g_ioport.p_api->pinRead(PIN_HOME_D, &pinlevel);
    if (pinlevel != ee_ConfigBoards[STEPPER_D].ucPhoto)
        temp_dig[PHOTO_D] = 1;

    g_ioport.p_api->pinRead(PIN_KEY_PHYSICAL, &pinlevel);
    if (pinlevel == 0)
        temp_dig[IN_KEY] = 1;

    g_ioport.p_api->pinRead(PIN_IN_PROXY, &pinlevel);
    if (pinlevel == 0)
        temp_dig[IN_PROXY] = 1;

    g_ioport.p_api->pinRead(PIN_LAMP_SWITCH, &pinlevel);
    if (pinlevel == 0)
        temp_dig[TASTO_LUCE] = 1;

    g_ioport.p_api->pinRead(PIN_FILTER_SWITCH, &pinlevel);
    if (pinlevel == 0)
        temp_dig[TASTO_FILTRO] = 1;

    g_ioport.p_api->pinRead(PIN_SWITCH_ROT, &pinlevel);
    if (pinlevel == 0)
        temp_dig[IN_SWITCH_ROT] = 1;

    for (i = 0; i < NUM_DIG; i++)
    {
        if (temp_dig[i])    // se pin attivato
        {
            if (rimb_dig[i] < RIMBALZI_DIG[i])
            {
                if (++rimb_dig[i] == RIMBALZI_DIG[i])
                {
                    if (buffer_digitali[i] == FALSE)
                    {
                        buffer_latch[i] = TRUE;
                        buffer_digitali[i] = TRUE;
                    }
                }
            }
        }
        else
        {
            if (rimb_dig[i] == 0)
            {
                buffer_digitali[i] = FALSE;       // azzera solo il buffer
            }
            else
            {
                rimb_dig[i]--;
            }
        }
    }

    HomeSensor[STEPPER_A] = buffer_digitali[PHOTO_A];
    HomeSensor[STEPPER_B] = buffer_digitali[PHOTO_B];
    HomeSensor[STEPPER_C] = buffer_digitali[PHOTO_C];
    HomeSensor[STEPPER_D] = buffer_digitali[PHOTO_D];
    FILTER_SWITCH = buffer_digitali[TASTO_FILTRO];
    LAMP_SWITCH = buffer_digitali[TASTO_LUCE];
    HOME_C = buffer_digitali[PHOTO_C];
    KEY_MANUAL = buffer_digitali[IN_KEY] ? FALSE : TRUE;




//
//    if (FULL_A != Steppers[STEPPER_A].ucLevel)      // qui in reset
//    {
//        if (rimb_dig[STEPPER_A] == rimbalzo_home_shutter)
//            HomeSensor[STEPPER_A] = TRUE;                  // qui in Home
//        else
//            rimb_dig[STEPPER_A]++;
//    }
//    else
//    {
//        if (rimb_dig[STEPPER_A] == 0)
//            HomeSensor[STEPPER_A] = FALSE;
//        else
//            rimb_dig[STEPPER_A]--;
//    }
//
//    if (FULL_B != Steppers[STEPPER_B].ucLevel)      // qui in reset
//    {
//        if (rimb_dig[STEPPER_B] == rimbalzo_home_shutter)
//            HomeSensor[STEPPER_B] = TRUE;                  // qui in Home
//        else
//            rimb_dig[STEPPER_B]++;
//    }
//    else
//    {
//        if (rimb_dig[STEPPER_B] == 0)
//            HomeSensor[STEPPER_B] = FALSE;
//        else
//            rimb_dig[STEPPER_B]--;
//    }
//
//    if (FILTER_SENS != ee_ConfigBoards[STEPPER_C].ucPhoto)      // FILTER HOME SENSOR
//    {
//        if (rimb_home == RIMBALZO_HOME_FILTRO)
//            HOME_C = TRUE;                  // qui in Home
//        else
//            rimb_home++;
//    }
//    else
//    {
//        if (rimb_home == 0)
//            HOME_C = FALSE;
//        else
//            rimb_home--;
//    }
//
//    g_ioport.p_api->pinRead(PIN_KEY_PHYSICAL, &pinlevel);
//    if (pinlevel == 1)      // CHIAVE
//    {
//        if (rimb_key == RIMBALZO_KEY)
//            KEY_MANUAL = 1;                  // qui in Home
//        else
//            rimb_key++;
//    }
//    else
//    {
//        if (rimb_key == 0)
//            KEY_MANUAL = 0;
//        else
//            rimb_key--;
//    }
//
//    if (HOME_B == 0)      // usato per switch Rotazione
//    {
//        if (rimb_dig[IX_SENS_HOME_B] == RIMBALZO_SW_ROT)
//            HomeSensor[IX_SENS_HOME_B] = TRUE;                  // qui in Home
//        else
//            rimb_dig[IX_SENS_HOME_B]++;
//    }
//    else
//    {
//        if (rimb_dig[IX_SENS_HOME_B] == 0)
//            HomeSensor[IX_SENS_HOME_B] = FALSE;
//        else
//            rimb_dig[IX_SENS_HOME_B]--;
//    }
//
//    g_ioport.p_api->pinRead(PIN_FILTER_SWITCH, &pinlevel);
//    if (pinlevel == 0)      // tasto frontale
//    {
//        if (rimb_key_filter == RIMBALZO_KEY)
//            FILTER_SWITCH = 1;                  // qui in Home
//        else
//            rimb_key_filter++;
//    }
//    else
//    {
//        if (rimb_key_filter == 0)
//            FILTER_SWITCH = 0;
//        else
//            rimb_key_filter--;
//    }
//
//    g_ioport.p_api->pinRead(PIN_LAMP_SWITCH, &pinlevel);
//    if (pinlevel == 0)      // tasto frontale
//    {
//        if (rimb_key_lamp == RIMBALZO_KEY)
//            LAMP_SWITCH = 1;                  // qui in Home
//        else
//            rimb_key_lamp++;
//    }
//    else
//    {
//        if (rimb_key_lamp == 0)
//            LAMP_SWITCH = 0;
//        else
//            rimb_key_lamp--;
//    }
//

}






/***************************************************
**  scansione dei tasti
***************************************************/
void scan_kb(void)
{
    ioport_level_t pinFilter, pinLamp;
    ssp_err_t err;
    UINT8 codtasto;



    codtasto = 0;
    g_ioport.p_api->pinRead(PIN_FILTER_SWITCH, &pinFilter);
    g_ioport.p_api->pinRead(PIN_LAMP_SWITCH, &pinLamp);
    if (pinFilter == 0)
    {
        codtasto = KEY_FILTER;
    }
    else if (pinLamp == 0)
    {
        codtasto = KEY_LAMP;
    }

    if (codtasto)      // se ha letto un tasto
    {
        if (ctkb != MAXKB)
        {
            if (++ctkb == MAXKB)
            {
                if (azionato == FALSE)
                {
                    val_tasto = codtasto;
                    key_pressed = TRUE;
                    key_press_event = TRUE;
                    azionato = TRUE;
                }
            }
        }
    }
    else
    {                       // qui nessun tasto premuto
        if (ctkb > 4)
        {
            ctkb -= 4;
        }
        else
        {
            ctkb = 0;
            if (azionato == TRUE)
            {
                azionato = FALSE;
                key_released_event = TRUE;
            }
        }
    }
}






//****************************************
//  legge manopole SX e DX
//****************************************
void read_hw_manopole(void)
{
    static UINT8 stato_encoder = 0;
    static uint8_t contatore = 0;
    UINT8 temp8;
    uint16_t temp16;


    if (++contatore < 12)
        return;

    contatore = 0;


    switch (stato_encoder)
    {
    case 0:    // lettura CROSS
        g_ioport.p_api->portRead(IOPORT_PORT_05, &temp16);
        g_ioport.p_api->pinWrite(PIN_ENC_EN_C, IOPORT_LEVEL_HIGH);
        Calcola_Encoder(STEPPER_CROSS, (uint8_t)temp16);
        EncoderValue[STEPPER_CROSS] = -EncoderValue[STEPPER_CROSS];
        g_wCrossMagnitude += EncoderValue[STEPPER_CROSS];
        if (EncoderValue[STEPPER_CROSS] > 0)
            EncoderDir[STEPPER_CROSS] = ENC_DIR_PIU;
        else if (EncoderValue[STEPPER_CROSS] < 0)
            EncoderDir[STEPPER_CROSS] = ENC_DIR_MENO;
        g_ioport.p_api->pinWrite(PIN_ENC_EN_L, IOPORT_LEVEL_LOW);
        stato_encoder = 1;
        break;

    case 1:    // lettura LONG
        g_ioport.p_api->portRead(IOPORT_PORT_05, &temp16);
        g_ioport.p_api->pinWrite(PIN_ENC_EN_L, IOPORT_LEVEL_HIGH);
        Calcola_Encoder(STEPPER_LONG, (uint8_t)temp16);
        g_wLongMagnitude += EncoderValue[STEPPER_LONG];
        if (EncoderValue[STEPPER_LONG] > 0)
            EncoderDir[STEPPER_LONG] = ENC_DIR_PIU;
        else if (EncoderValue[STEPPER_LONG] < 0)
            EncoderDir[STEPPER_LONG] = ENC_DIR_MENO;
        g_ioport.p_api->pinWrite(PIN_ENC_EN_C, IOPORT_LEVEL_LOW);
        EncoderValue[STEPPER_LONG2] = EncoderValue[STEPPER_LONG];
        stato_encoder = 0;
        break;

    default:
        stato_encoder = 0;
        break;
    }

}





//*****************************************************************
//  calcola lo spostamento dell'encoder
//*****************************************************************
#define KNOB_DIR_UP     1
#define KNOB_DIR_DOWN   2
void Calcola_Encoder(UINT8 stepper, UINT8 value)
{
    static UINT8 direzione = 0;
    static SINT8 value0[3], value1[3];
    SINT8 diff;
    UINT8 temp8;
    UINT16 temp16;

    value = AceTabInv[value];
    if (value != NO_CODE)
    {
        // convert to signed char value
        EncoderLast[stepper] = EncoderActual[stepper];
        EncoderActual[stepper] = value;

        value <<= 1;
        value0[stepper] = value;

        // calculate difference
        diff = (SINT8)(value0[stepper] - value1[stepper]) / 2;
        if (diff > 0)       // conteggio in salita
        {
            if (direzione != KNOB_DIR_UP)       // se cambia direzione deve valutare
            {
                if (diff <= ee_ConfigColl.JitterKnobs)
                {
                    EncoderLast[stepper] = EncoderActual[stepper];
                }
                else
                {
                    value1[stepper] = value0[stepper];
                    direzione = KNOB_DIR_UP;
                }
            }
            else
            {
                value1[stepper] = value0[stepper];
            }
        }
        else if (diff < 0)
        {
            if (direzione != KNOB_DIR_DOWN)       // se cambia direzione deve valutare
            {
                if ((-diff) <= ee_ConfigColl.JitterKnobs)
                {
                    EncoderLast[stepper] = EncoderActual[stepper];
                }
                else
                {
                    value1[stepper] = value0[stepper];
                    direzione = KNOB_DIR_DOWN;
                }
            }
            else
            {
                value1[stepper] = value0[stepper];
            }
        }

//        EncoderLast[stepper] = EncoderActual[stepper];
//        EncoderActual[stepper] = value;
        if ((EncoderLast[stepper] > 96) && (EncoderActual[stepper] < 32))        // oltrepassato lo 0 in salita
        {
            EncoderValue[stepper] = (UINT16)(EncoderActual[stepper] + 128 - EncoderLast[stepper]);
        }
        else if ((EncoderLast[stepper] < 32) && (EncoderActual[stepper] > 95))        // oltrepassato lo 0 in discesa
        {
            temp8 = 128 - EncoderActual[stepper];
            temp8 += EncoderLast[stepper];
            temp16 = (UINT16)temp8;
            EncoderValue[stepper] = -(SINT16)temp16;
        }
        else
        {
            EncoderValue[stepper] = (UINT16)EncoderActual[stepper] - (UINT16)EncoderLast[stepper];
        }

        if ((EnableKnobs == FALSE) && (ManualStatus[stepper] == FALSE))
        {
            EncoderValue[stepper] = 0;
        }
    }
    else
    {
        EncoderErrors[stepper]++;
    }
}




