


#ifdef MAIN_H
#define main_ext
#else
#define main_ext extern
#endif

#define TIPO_SID_SOTTO      0
#define TIPO_SID_SOPRA      1
#define TIPO_SID_FUORI      2


#define NUM_CHAR_CUSTOM_FILTER  12      // numero massimo di caratteri per il filtro personalizzato

//#define TIME_SWITCH_CONFIG  20  // 1/10 secondo // ATTENZIONE: deve essere inferiore agli altri
//#define TIME_SWITCH_CALIBR  40  // 1/10 secondo
#define TIME_SWITCH_TEST    40  // 1/10 secondo

#define TIME_WAIT_BUCKY     9   // in 1/10 secondo per attesa nuova cassetta

#define STEPPER_CROSS   STEPPER_A
#define STEPPER_LONG    STEPPER_B
#define STEPPER_FILTER  STEPPER_C
#define STEPPER_LONG2   STEPPER_D
#define STEPPER_IRIS    4           // attenzione agli array

#define ENC_DIR_PIU     0
#define ENC_DIR_MENO    1

#define MOTORE_FILTRO_DEF   MOTORE_FILTRO_OLD
#define FILTER_RESET_MIN_OLD    100     // numero di passi di differenza tra i 2 fori
#define FILTER_RESET_MAX_OLD    300     // numero di passi di differenza tra i 2 fori

#define FREQ_SET_MIN         500UL      // per controllo in set impostazioni
#define FREQ_SET_MAX        7000UL      // per controllo in set impostazioni
#define FREQ_MIN_RESET       200UL        // frequenza durante reset
#define FREQ_MAX_RESET      1500UL        // frequenza durante reset
#define FREQ_MAX_RESET_TEST_PHOTO    500UL

#define FREQ_MOT_MIN         100UL      // per calcoli vari
#define FREQ_MOT_MAX        7000UL
#define TEMPO_RAMPA          100UL      // in ms
#define DEF_TIME_STEPPER       1UL       // ms per aggiornamento rampa motori stepper
#define FREQ_OVER_TEORICA  10000UL

#define MOT_FREQ_STEP       (((FREQ_MOT_MAX - FREQ_MOT_MIN) * DEF_TIME_STEPPER) / TEMPO_RAMPA)
#define PASSI_RAMPA         (((FREQ_MOT_MIN + FREQ_MOT_MAX) * TEMPO_RAMPA) / 2000UL)        // passi totali durante la rampa
#define FREQ_MOT_MULTIPLIER ((FREQ_MOT_MAX - FREQ_MOT_MIN) / PASSI_RAMPA)               // moltiplicatore per la frequenza teorica
#define ACCEL_LAMELLE       (((FREQ_MOT_MAX - FREQ_MOT_MIN) * 1000UL) / TEMPO_RAMPA)
#define PASSI_RAMPA_LAMELLE ((((FREQ_MOT_MAX * FREQ_MOT_MAX) - (FREQ_MOT_MIN * FREQ_MOT_MIN)) / (2 * ACCEL_LAMELLE)) * 2)

#define FREQ_FILTER_MAX             20000UL
#define FREQ_FILTER_MIN             500UL
#define TEMPO_RAMPA_FILTER_OLD      100UL       // in ms
#define TEMPO_RAMPA_FILTER_NEW      100UL       // in ms
#define TEMPO_RAMPA_FILTER_FAST     50UL        // in ms

#define MOT_FILTER_STEP_OLD     (((FREQ_FILTER_MAX - FREQ_FILTER_MIN) * DEF_TIME_STEPPER) / TEMPO_RAMPA_FILTER_OLD)
#define ACCEL_FILTER_OLD        (((FREQ_FILTER_MAX - FREQ_FILTER_MIN) * 1000UL) / TEMPO_RAMPA_FILTER_OLD)
#define PASSI_RAMPA_FILTER_OLD  (((FREQ_FILTER_MAX * FREQ_FILTER_MAX) - (FREQ_FILTER_MIN * FREQ_FILTER_MIN)) / (2 * ACCEL_FILTER_OLD))

#define MOT_FILTER_STEP_NEW     (((FREQ_FILTER_MAX - FREQ_FILTER_MIN) * DEF_TIME_STEPPER) / TEMPO_RAMPA_FILTER_NEW)
#define ACCEL_FILTER_NEW        (((FREQ_FILTER_MAX - FREQ_FILTER_MIN) * 1000UL) / TEMPO_RAMPA_FILTER_NEW)
#define PASSI_RAMPA_FILTER_NEW  (((FREQ_FILTER_MAX * FREQ_FILTER_MAX) - (FREQ_FILTER_MIN * FREQ_FILTER_MIN)) / (2 * ACCEL_FILTER_NEW))

#define MOT_FILTER_STEP_FAST    (((FREQ_FILTER_MAX - FREQ_FILTER_MIN) * DEF_TIME_STEPPER) / TEMPO_RAMPA_FILTER_FAST)
#define ACCEL_FILTER_FAST       (((FREQ_FILTER_MAX - FREQ_FILTER_MIN) * 1000UL) / TEMPO_RAMPA_FILTER_FAST)
#define PASSI_RAMPA_FILTER_FAST (((FREQ_FILTER_MAX * FREQ_FILTER_MAX) - (FREQ_FILTER_MIN * FREQ_FILTER_MIN)) / (2 * ACCEL_FILTER_FAST))


#define FREQ_MOT_STOP_LAMELLE       2000UL   // frequenza massima per consentire lo stop motore
#define FREQ_MOT_STOP_FILTRO        2000UL   // frequenza massima per consentire lo stop motore

//
//
//
//
//
//
//
//
//
//#define FREQ_MIN_RESET               200UL        // frequenza durante reset
//#define FREQ_MAX_RESET              1500UL        // frequenza durante reset
//#define FREQ_MAX_RESET_TEST_PHOTO    500UL
//
//#define FREQ_SET_MIN         500UL      // per controllo in set impostazioni
//#define FREQ_SET_MAX        7000UL      // per controllo in set impostazioni
//
//#define FREQ_MOT_MIN         100UL      // per calcoli vari
//#define FREQ_MOT_MAX        7000UL
//#define FREQ_OVER_TEORICA  10000UL
//#define TEMPO_RAMPA          100UL      // in ms
//#define DEF_TIME_STEPPER       1UL       // ms per aggiornamento rampa motori stepper
//#define PASSI_RAMPA         (((FREQ_MOT_MIN + FREQ_MOT_MAX) * TEMPO_RAMPA) / 2000UL)        // passi totali durante la rampa
//#define FREQ_MOT_MULTIPLIER ((FREQ_MOT_MAX - FREQ_MOT_MIN) / PASSI_RAMPA)               // moltiplicatore per la frequenza teorica
//
//#define DEF_TIME_STEPPER       4UL       // ms per aggiornamento rampa motori stepper
//#define MOT_FREQ_STEP       (((FREQ_MOT_MAX - FREQ_MOT_MIN) * DEF_TIME_STEPPER) / TEMPO_RAMPA)
//
//#define FREQ_MOT_STOP       2000UL   // frequenza massima per consentire lo stop motore

#define DEF_ENC_MAX_SPEED      4UL      // riferimento massimo velocita' encoder per avere la frequenza massima
#define DEF_ENC_MAX_FREQ    7000UL      // frequenza massima in movimentazione con encoder
#define DEF_ENC_ACCEL          3UL      // valore di accelerazione


#define TIME_WAIT_STOP      30      // in ms
#define TIMEOUT_RESET      1800      // in 1/100 secondo

#define TIME_WAIT_MAN_MOV   30  // in 1/100 sec attesa prima di controllare cross_follow e long_follow
#define DEF_FMIN        1000    // frequenza minima motori lamelle

#define MAGN_DIV          1       // per rallentare un po' la lettura dell'encoder in calibrazione
#define MAGN_DIV_1        5       // per rallentare un po' la lettura dell'encoder in calibrazione
#define MAGN_DIV_2        7       // per rallentare un po' la lettura dell'encoder in calibrazione
#define MAGN_DIV_3        9       // per rallentare un po' la lettura dell'encoder in calibrazione

#define CROSS_FORMAT_CM 30
#define LONG_FORMAT_CM  30
#define CROSS_FORMAT_IN 12
#define LONG_FORMAT_IN  12
#define IRIS_FORMAT_CM  30
#define IRIS_FORMAT_IN  12

#define DEF_PASSI_A_STD     7050
#define DEF_SIZE_A_STD      495

#define DEF_PASSI_B_STD     5540
#define DEF_SIZE_B_STD      495


#define DEF_PASSI_A_IRIDE   6950
#define DEF_SIZE_A_IRIDE    435

#define DEF_PASSI_B_IRIDE   5500
#define DEF_SIZE_B_IRIDE    435


//*******  SEZIONE IRIDE  ********
#define OFFSET_LAMELLE      8      // in mm per apertura tangente rispetto all'iride
#define IRIS_FORMAT_MAX     7       // numero di formati di calibrazione
#define MSG_IRIS_VER        0x0000      // remote - versione firmware
#define MSG_IRIS_MOV        0x0002      // messaggio di movimentazione
#define MSG_IRIS_MODE       0x0004     // modo motore
#define MSG_IRIS_SPEED      0x0005     // drive speed
#define MSG_IRIS_OPTION     0x0006     //
#define MSG_IRIS_SN         0x000C     //
#define MSG_IRIS_CALIB      0x000D     //
#define MSG_IRIS_CAL_CURR       0x00
#define MSG_IRIS_CAL_TIME_TX    0x01
#define MSG_IRIS_CAL_PMAX       0x02
#define MSG_IRIS_CAL_TAR        0x03    // set taratura IN/OUT
#define MSG_IRIS_CAL_SPEED_KEY  0x04    // drive speed con movimento tasti
#define MSG_IRIS_CAL_SET_TAR    0x05    // set dei valori di taratura
#define MSG_IRIS_CAL_READ_TAR   0x06    // legge i valori di taratura


#define OFFSET_METRO        270      // in mm per offset metro ad ultrasuoni con fuoco

#define CLOCK_16MHZ         //To use on Real Board
#define SYS_CLOCK 16000000UL        // clock di sistema
#define MOTOR_CLOCK 2000000UL       // TODO: MOTOR_CLOCK verificare valore freq Timers: 1:8

#define ENTER_LCD_MODE
#define EXIT_LCD_MODE

#define TIME_CHECK_POT      40          // in 1/100 secondo per pausa controllo potenziometri
#define DELTA_POT_ALARM     20         // delta massimo in adc tra il valore teorico ed il valore rilevato

#define RIMBALZO_KEY            25        // in 1/100 ms - antirimbalzo chiave
#define RIMBALZO_HOME           3         // in ms - antirimbalzo sensore di home
#define RIMBALZO_HOME_TEST      1         // in ms - antirimbalzo sensore di home
#define RIMBALZO_HOME_FILTRO    5         // in ms - antirimbalzo sensore di home per filtro
#define RIMBALZO_SW_ROT         10          // in ms

#define MIN_STEPPER_FREQ    40
#define MAX_STEPPER_FREQ    20000
#define MIN_STEPPER_TRAMP   50
#define MAX_STEPPER_TRAMP   1000

#define MAX_DESTINATION     100000
#define NUM_CAL             3

//#define DEFAULT_DAC           122     // corrente massima 478mA
//#define DEFAULT_DAC           140     // corrente massima 550 mA
#define DEFAULT_DAC         166     // corrente massima 650 mA
#define IDLE_DAC            120     // corrente massima 470

#define MAXIMUM_DAC         0xCC
#define TORQUE_RUN          0xE0

#define TORQUE_LAMELLE      0x01    // 70% del DAC
#define TORQUE_FILTRO_OLD   0x01
#define TORQUE_FILTRO_NEW   0x03    // 100%


#define ZERO_G_VOLTAGE      234 //2.5V
#define DELTA_G_VOLTAGE     81  //0.7V
#define DELTA_ADXL_ST       50  //bits




//  COMANDI con base .ID_MOTORI
#define MSG_MOT_STATO      0x0001
#define MSG_MOT_MOVE       0x0002
#define MSG_MOV_HOME    0x00        // sotto-comando ID xx2h Movimento Motore
#define MSG_MOV_ABS     0x01
#define MSG_MOV_REL     0x02
#define MSG_MOV_RELEASE 0x03
#define MSG_MOV_BRAKE   0xFD
#define MSG_MOV_SET     0xFE
#define MSG_MOV_STOP    0xFF

#define MSG_MOT_MODE       0x0004
#define MSG_MOT_SPEED      0x0005
#define MSG_MOT_OPTION     0x0006
#define MSG_MOT_OPTION_2   0x0007
#define MSG_MOT_SET_VARIE  0x000D
#define MSG_MOT_READ_VAL   0x000E






#define LAMP_SWITCH_HW  p10_3
#define LASER1_ON    0 //TODO: p10_5=0
#define LASER1_OFF   p10_5=1
#define LASER2_ON    p10_6=0
#define LASER2_OFF   p10_6=1
//#define TIMER_START  0 //TODO: p10_7=1
#define CHECK_LUCE_ON   (p10_7)
#define CHECK_LUCE_OFF  (p10_7 == 0)
//#define TIMER_STOP   0 //TODO: p10_7=0
#define PIN_LUCE     0 //TODO: p10_7

//Hardware Pins: PORT 5
//TODO: P5 address LED frontale
#define SET_LED_D16     (pca8575_port0.bit8.bit7 = 0) //ucTemp=p5;ucTemp&=0xF1;p5=ucTemp
#define RES_LED_D16     (pca8575_port0.bit8.bit7 = 1) //ucTemp=p5;ucTemp&=0xF1;p5=ucTemp

#define SET_LED_D17     (pca8575_port0.bit8.bit6 = 0) //ucTemp=p5;ucTemp&=0xF1;p5=ucTemp
#define RES_LED_D17     (pca8575_port0.bit8.bit6 = 1) //ucTemp=p5;ucTemp&=0xF1;p5=ucTemp

#define SET_LED_D18     (pca8575_port0.bit8.bit5 = 0) //ucTemp=p5;ucTemp&=0xF1;p5=ucTemp
#define RES_LED_D18     (pca8575_port0.bit8.bit5 = 1) //ucTemp=p5;ucTemp&=0xF1;p5=ucTemp

#define SET_LED_D19     (pca8575_port0.bit8.bit4 = 0)
#define RES_LED_D19     (pca8575_port0.bit8.bit4 = 1)

#define SET_LED_D20     (pca8575_port0.bit8.bit3 = 0)
#define RES_LED_D20     (pca8575_port0.bit8.bit3 = 1)

#define SET_LED_D21     (pca8575_port0.bit8.bit2 = 0)
#define RES_LED_D21     (pca8575_port0.bit8.bit2 = 1)

#define SET_LED_D22     (pca8575_port0.bit8.bit1 = 0)
#define RES_LED_D22     (pca8575_port0.bit8.bit1 = 1)

#define SET_LED_D23     (pca8575_port0.bit8.bit0 = 0)
#define RES_LED_D23     (pca8575_port0.bit8.bit0 = 1)

#define FAN_ON          //TODO: p5_4=0
#define FAN_OFF         //TODO: p5_4=1
#define sby_all_step    //TODO: p5_6
#define rst_all_step    //TODO: p5_7

//Hardware Pins: PORT 6
//TODO: P6 varie
#define ADXL_ST_ON      //TODO: p6_0=1
#define ADXL_ST_OFF     //TODO: p6_0=0
#define LED_SIGNAL_OFF  (g_ioport.p_api->pinWrite(PIN_LED_D31, IOPORT_LEVEL_HIGH))  //p6_0=1
#define LED_SIGNAL_ON   (g_ioport.p_api->pinWrite(PIN_LED_D31, IOPORT_LEVEL_LOW))   //p6_0=0

#define SW_TIMER_DIS    //TODO:p6_1
#define SW_TIMER_EN     //TODO:!p6_1
#define SW_TIMER_1      //TODO:!p6_2
#define SW_TIMER_2      //TODO:!p6_3

#define US_POWER        0 //TODO:p6_1


//Hardware Pins: PORT 7
//TODO: P7 varie
#define FILTER_SENS     0 //TODO:p7_7
//#define HOME_C            p7_7
#define CLK_C           0 //TODO:p7_6
#define HOME_B          0 //TODO:p7_5
#define CLK_B           0 //TODO:p7_4
#define HOME_A          0 //TODO:p7_3
#define CLK_A           0 //TODO:p7_2
#define FULL_A          0 //TODO:p7_1
#define FULL_B          0 //TODO:p7_0
#define FULL_C          0 //TODO:true
#define FULL_D          0 //TODO:p7_0

#define IN_SWITCH_ROTATION      0       //TODO: IN_SWITCH_ROTATION - HomeSensor[IX_SENS_HOME_B]

//Hardware Pins: PORT 8
//TODO: P8 varie
#define P6_ENC          0 //TODO:p8_0
#define P5_ENC          0 //TODO:p8_1


#define ENA_ENC_CROSS   0 //TODO:p8_6=0;
#define DIS_ENC_CROSS   0 //TODO:p8_6=1;
#define ENA_ENC_LONG    0 //TODO:p8_7=0;
#define DIS_ENC_LONG    0 //TODO:p8_7=1;

//Hardware Pins: PORT 9
#define P1_ENC          0 //TODO:p9_0
#define P2_ENC          0 //TODO:p9_1
#define P3_ENC          0 //TODO:p9_2
#define P4_ENC          0 //TODO:p9_3
#define ANA_DIG_IN_PNP  0 //TODO:p9_5
#define ANA_DIG_IN_NPN  0 //TODO:p7_3
#define P7_ENC          0 //TODO:p9_6
#define P8_ENC          0 //TODO:p9_7

//Timer interrupt
#define timer_step_a_val    ta1
#define timer_step_a_s      ta1s
#define timer_step_a_int    ta1ic
#define timer_step_a_mode   ta1mr

#define timer_step_b_val    ta2
#define timer_step_b_s      ta2s
#define timer_step_b_int    ta2ic
#define timer_step_b_mode   ta2mr

#define timer_step_c_val    ta3
#define timer_step_c_s      ta3s
#define timer_step_c_int    ta3ic
#define timer_step_c_mode   ta3mr

#define _O_PIN_A            //TODO:p7_2
#define _O_PIN_B            //TODO:p7_4

//Eeprom exchange data
#define EEP_UPDATE_DMODE        0x00000001
#define EEP_UPDATE_DSPEED       0x00000002
#define EEP_UPDATE_WINDOWS      0x00000004
#define EEP_UPDATE_CAN_ADDRESS  0x00000008
#define EEP_UPDATE_OPTIONS      0x00000010
#define EEP_UPDATE_SERIAL       0x00000020
#define EEP_UPDATE_CAL          0x00000040
#define EEP_UPDATE_COLLIMATOR   0x00000080
#define EEP_UPDATE_OFFSET       0x00000100
#define EEP_UPDATE_TOPBUCKY     0x00000200
#define EEP_UPDATE_SXBUCKY      0x00000400
#define EEP_UPDATE_DXBUCKY      0x00000800
#define EEP_UPDATE_CALIB        0x00001000
#define EEP_UPDATE_FILTER       0x00002000
#define EEP_UPDATE_IRIS         0x00004000
#define EEP_UPDATE_CAN          0x00008000
#define EEP_UPDATE_FORMAT       0x00010000

#ifdef SYS_CANOPEN
#define EEP_UPDATE_CANOPEN      0x00020000
#endif


//Macro
#define SET_LED_RED     SET_LED_D17
#define RES_LED_RED     RES_LED_D17

#define SET_LED_YELLOW  SET_LED_D16
#define RES_LED_YELLOW  RES_LED_D16

#define SET_LED_GREEN   SET_LED_D18
#define RES_LED_GREEN   RES_LED_D18

#define SET_LED_CAN_RX  //TODO: SET_LED_D09
#define SET_LED_CAN_TX  //TODO: SET_LED_D10
#define RES_LED_CAN_RX  //TODO: RES_LED_D09
#define RES_LED_CAN_TX  //TODO: RES_LED_D10

#define RES_ALL_OUTPUTS TIMER_STOP; LASER2_OFF; LASER1_OFF
#define SET_ALL_OUTPUTS TIMER_START; LASER2_ON; LASER1_ON

//#define READ_ENCODER   (p9&0xCF) | ((p8 & 0x01) << 5) | ((p8 & 0x02) << 3)

#define KEY_AUTO    (!KEY_MANUAL)


//Steppers
enum {
    OP_SPEED_FERMO = 1,
    OP_SPEED_INC,
    OP_SPEED_INC_MEZZO,
    OP_SPEED_DEC,
    OP_SPEED_DEC_0,
    OP_SPEED_RAMPA,
    OP_SPEED_BRAKE
};


enum {
    ST_MOT_RESET = 0x10,        // 0x10
    ST_MOT_RESET_1,             // 0x11
    ST_MOT_IN_RESET,            // 0x12
    ST_MOT_HOME,                // 0x13
    ST_MOT_FERMO,               // 0x14
    ST_MOT_RUN,                 // 0x15
    ST_MOT_FRENA_E_HOME,        // 0x16
    ST_MOT_IDLE,                // 0x17
    ST_MOT_AVANTI,              // 0x18
    ST_MOT_INDIETRO             // 0x19

};

// attenzione definiti a bit
#define COM_MOTORE_NULL     0x00
#define COM_MOTORE_STOP     0x01
#define COM_MOTORE_RELEASE  0x02        // deve fare uno stop e poi il rilascia
#define COM_MOTORE_RESET    0x04
#define COM_MOTORE_BRAKE    0x08        // ferma il motore con rampa

#define STEP_RESET_RANGE    50      // numero di passi per range di accettazione per reset 2 fori


//Analog channels
typedef enum {
    ADXL_X = 0, ADXL_Y, POT_CROSS, POT_LONG, MAX_AD_CHANNELS
};
//Photocell levels
typedef enum {
    PHOTO_LOW = 0, PHOTO_HIGH, PHOTO_POT
};
//Global defines
typedef enum{
    RX_ERROR = 0, RX_UNDER, RX_LEFT, RX_RIGHT, RX_TOMO
};

typedef enum{
    ITA_LANG = 0, ENG_LANG, MAX_LANG
};

typedef enum{
    VERT_DFF_SINGLE=0, VERT_DFF_DIFF, VERT_DFF_FIXED, VERT_DFF_CAN, VERT_DFF_MAX
};

typedef enum{
    LAT_DFF_DISC=0, LAT_DFF_CAN, LAT_DFF_POT, LAT_DFF_MAX
};

typedef enum{
    VERT_REC_NO=0, VERT_REC_BUCKY, VERT_REC_CAN, VERT_REC_ATS, VERT_REC_FISSI, VERT_REC_MAX
};

typedef enum{
    LAT_REC_NO=0, LAT_REC_BUCKY, LAT_REC_CAN, LAT_REC_MAX
};

typedef enum{
    INCHES_UNIT=0, CM_UNIT, MAX_UNIT
};

typedef enum{
    MM1_FILTER=0, MM2_FILTER, CU_FILTER, CUSTOM_FILTER      // ATTENZIONE: ucFilterType e' 2 bit! (accetta i valori da 0 a 3)
};

typedef enum{
    DFF_STAT=0, DFF_TAB, BUCKY_CROSS, BUCKY_LONG, LEFT_CROSS, LEFT_LONG, RIGHT_CROSS, RIGHT_LONG
};

enum {
    ST_KEY_PREP_IDLE = 0,
    ST_KEY_IDLE,
    ST_KEY_ROTAZIONE_INIT,
    ST_KEY_ROTAZIONE_CONFIRM,
    ST_KEY_M45,
    ST_KEY_0,
    ST_KEY_P45,
    ST_KEY_END_ROTAZIONE,
    ST_KEY_TAR_FORMATO_INIT,
    ST_KEY_TAR_FORMATO_CONFIRM,
    ST_KEY_TAR_FORMATO,
    ST_KEY_TAR_FORMATO_END,
    ST_KEY_TAR_PASSI_CROSS_INIT,
    ST_KEY_TAR_PASSI_CROSS_CONFIRM,
    ST_KEY_TAR_PASSI_CROSS,
    ST_KEY_TAR_PASSI_CROSS_END,
    ST_KEY_TAR_PASSI_LONG_INIT,
    ST_KEY_TAR_PASSI_LONG_CONFIRM,
    ST_KEY_TAR_PASSI_LONG,
    ST_KEY_TAR_PASSI_LONG_END,
    ST_KEY_POT_OPEN_INIT,
    ST_KEY_POT_OPEN,
    ST_KEY_POT_CLOSE,
    ST_KEY_END_POT,
    ST_KEY_IRIDE_RESET_INIT,
    ST_KEY_IRIDE_CONFIRM,
    ST_KEY_IRIDE_RESET,
    ST_KEY_IRIDE_FORMATI,
    ST_KEY_IRIDE_MAX,
    ST_KEY_IRIDE_END,
    ST_KEY_AIRIDE_INIT,
    ST_KEY_AIRIDE_CONFIRM,
    ST_KEY_AIRIDE_AUTOCAL,
    ST_KEY_AIRIDE_END,
    ST_KEY_VIS_003_INIT,
    ST_KEY_VIS_003_CONFIRM,
    ST_KEY_VIS_003,
    ST_KEY_VIS_003_END,
    ST_KEY_RESET_DEFAULT_INIT,
    ST_KEY_RESET_DEFAULT_CONFIRM,
    ST_KEY_RESET_DEFAULT,
    ST_KEY_RESET_DEFAULT_END,

};


enum {
    ST_HOME_OUT_SENS = 0,
    ST_HOME_WAIT_OUT_SENS,
    ST_HOME_CHECK_1_FORO,
    ST_HOME_CHECK_1_PIENO,
    ST_HOME_CHECK_2_FORO
};

#define KEY_FILTER    1
#define KEY_LAMP      2


//Digital Filtering of ADC Channels
#define ADXLX_ORDER     32
#define ADXLY_ORDER     32
#define CROSS_ORDER     32
#define LONG_ORDER      32
#define ADXLX_REDUCE    5
#define ADXLY_REDUCE    5
#define CROSS_REDUCE    5
#define LONG_REDUCE     5

//Interrupt priority
#define NO_PRIO     0
#define LO_PRIO     1   // timer ed encoder
#define SERIAL_PRIO 1   // seriale
#define MI_PRIO     4   // can
#define CAN_RX_PRIO 5
#define HI_PRIO     7   // motori

//Bucky Management
#define MIN_CASSETTE 1
#define MAX_CASSETTE 10

#define DELTA_ADC_15_MM 32      // era 22
#define DELTA_ADC_06_IN 32      // era 22

#define DELTA_ADC_07_MM 32      // era 10
#define DELTA_ADC_03_IN 32      // era 10


/////////////////////////////
// Can Message Rates
/////////////////////////////
typedef struct {
    uint16_t un100;
    uint16_t un7F0;
    uint16_t un7F1;
    uint16_t un7F8;
    uint16_t un7F9;
    uint16_t un7FA;
    uint16_t un7FB;
    uint16_t un7FC;
    uint16_t un082;
    uint16_t un083;
    uint16_t un084;
    uint16_t un085;
    uint16_t un101;
    uint16_t un103;
}UpdateRate;

/////////////////////////////
//Stepper Definition
/////////////////////////////
typedef struct {
    union{
        struct {
            char bMoving    :1;
            char bMoveError :1;
            char bChecking  :1;
            char bDummyBits :2;
            char bFull      :1;
            char bErrorHome :1;
            char bHome      :1;
        } StepperBits;
        uchar StepperByte;
    }StepperData;
    uchar ucStepperError;
    uchar ucLevel;
    uword uwFMin;
    uword uwFMax;
    uword uwTRamp;
    uword uwFMaxFast;
    bool bDriveMode;
} Stepper;


/////////////////////////////
//ASR003 Relay Definition
/////////////////////////////
typedef struct {
    union {
        struct {
            bool r_ready:1;
            bool r_hold:1;
            bool r_man:1;
            bool r_chiuse:1;
            bool r_raggi:1;
            bool Out6:1;
            bool Out7:1;
            bool InReset:1;
            bool r_8:1;
            bool r_9:1;
            bool dummy:6;
        } bit;
        uword word;
    } Status;
} Relay;

typedef union b {
    struct bits {
        uchar ucByte0;          //0 //..
        uchar ucFiller1:7;      //1 //..
        uchar bLeftBucky:1;         //Ok
        uchar bLeftPan:1;       //2 //Ok
        uchar bRightBucky:1;        //Ok
        uchar bRightPan:1;          //Ok
        uchar bOpenLong:1;          //Ok
        uchar bCloseLong:1;         //Ok
        uchar bOpenCross:1;         //Ok
        uchar bCloseCross:1;        //Ok
        uchar bOpenIris:1;          //Ok
        uchar bCloseIris:1;     //3 //Ok
        uchar bFilter1:1;           //Ok
        uchar bFiller2:1;           //..
        uchar bHorDff1:1;           //Ok
        uchar bFilter2:1;           //Ok
        uchar bFilter3:1;           //Ok
        uchar bHorDff5:1;           //Ok
        uchar bHorDff4:1;           //Ok
        uchar bFiller3:1;       //4 //..
        uchar bHorDff2:1;           //Ok
        uchar bHorDff3:1;           //Ok
        uchar bHorPanDff1:1;        //Ok
        uchar bHorPanDff2:1;        //Ok
        uchar bFiller4:1;           //..
        uchar bInclExcl:1;          //Ok
        uchar bFiller5:1;           //..
        uchar bFiller6:3;       //5 //..
        uchar bFilter4:1;           //Ok
        uchar bLamp:1;              //Ok
        uchar bFiller7:3;           //..
        uchar bFiller8:4;       //6 //..
        uchar bCalibration:1;
        uchar bFiller9:3;           //..
        uchar bFillerA:3;       //7 //..
        uchar bTomography:1;        //Ok
        uchar bManual:1;            //Ok
        uchar bFillerB:2;           //..
        uchar bTableBucky:1;        //Ok
    } data;
    char bytes[8];
} RemoteFrame;


/////////////////////////////
//Windows Definition
/////////////////////////////
typedef enum {
    WINDOW_0, WINDOW_1, WINDOW_2, WINDOW_3,
    WINDOW_4, WINDOW_5, WINDOW_6, WINDOW_7,
    WINDOW_8, WINDOW_9, WINDOW_A, WINDOW_B,
    WINDOW_C, WINDOW_D, WINDOW_E, WINDOW_F,
    MAX_WINDOW
};

typedef enum{
    INVALID, COLLIMATORWORK, CALIBRATION, CONFIGURATION, BOARDTEST
};

/////////////////////////////
//Internal Flags Definition
/////////////////////////////
typedef struct {
    bool bEepromOk          :1;
    bool bEepromInitialized :1;
    bool bEepromError       :1;
    bool bEepromUpdated     :1;
    bool bVoid4             :1; //Available
    bool bVoid5             :1; //Available
    bool bJumper15          :1;
    bool bJumper17          :1;
} Bits;

typedef union {
    Bits bit;
    char byte;
}InternalFlag;

/////////////////////////////
//Virtual Eeprom Definitions
/////////////////////////////
typedef struct {
    uchar ucStep:4;
    uchar ucTorque:2;
    uchar ucDecay:2;
    uchar ucPhoto:2;
    uword uwFMin;
    uword uwFMax;
    uword uwRamp;
    uword uwFMaxFast;
    uword uwMaxSteps;   // STEPPER_A inteso come CROSS
    uword uwRefSize;    // STEPPER_B inteso come LONG
    uword uwRefSteps;
} BoardConfiguration;

typedef struct{
    schar cbVersion[9];
    schar cbSerial[9];
}VersionStruct;

typedef struct {
    uchar ucLanguage:1;
    uchar ucCmUnit:1;
    uchar ucShowDff:1;
    uchar ucRampTime:2;
    uchar ucFilter:1;
    uchar ucFilterType:2;
    uchar ucInclinometer:1;
    uchar ucAutoLimits:1;
    uchar ucLightTime:2;
    uchar ucLatDffSx:2;
    uchar ucLatRecSx:2;

    uchar ucVertDff:3;
    uchar ucVertRec:3;
    uchar ucLatRecDx:2;
    UINT16 uwFixedDff;
    UINT8 ucInputDffPot:2;
    UINT8 ucLatDffDx:2;
    UINT8 EnFormatiMin:1;
    UINT8 bytesp1:3;

    UINT16 uwMinDff;
    UINT16 uwMaxDff;

    UINT8 ucDftVert;
    UINT8 ucDftSx;
    UINT8 ucDftDx;
    uchar ucCross:1;
    uchar ucLong:1;
    uchar ucIrisPresent:1;
    uchar ucInclDegrees:4;
    uchar ucFilterCheck:1;

    uchar ucAutoLight:1;
    uchar ucShowLock:1;
    uchar ucShowAngle:1;
    uchar ucIrisEnabled:1;
    uchar ucIrisShow:1;
    uchar ucIrisIndipendent:1;
    uchar bSquareRtz:1;
    uchar ucMeterEnabled:1;

    schar scCorrCrossDff;
    schar scCorrLongDff;
    schar scCorrIrideDff;
    uchar ucDisplayType:3;
    uchar ucAnalogInput:2;
    uchar ucAlarm_0x100:1;
        uchar ucDummyBits:2;

    uchar ucDeltaCrossOpenClose;
    uchar ucDeltaLongOpenClose;
    uchar ucDeltaCrossOpenCloseMan;
    uchar ucDeltaLongOpenCloseMan;

    uchar ucCanProtocol;
    uchar ucLcdFree;
    uchar ucLcdName[8];
    uchar ucCanSpeed;

    UINT8 ucHardwareType;
    uchar ucRotation        :1;
    uchar ucTipoProx        :1;
    uchar ucSwContext       :1;
    uchar ucManualColl      :1;
    uchar ucVisuaApeIris    :1;
    uchar ucFiltraggioIncl  :1;
    uchar ucRisposte7F8     :1;
    uchar TastieraVilla     :1;

    UINT16 uwPotCross[5];
    UINT16 uwPotLong[5];

    UINT8 OffsetLamelle;
    UINT16 OffsetMetro;
    UINT8 CorrType;

    UINT16 EncMaxFreq;
    UINT8  EncMaxSpeed;
    UINT8  EncAccel;

    UINT8  TipoManualSpost;
    UINT16 ManualSpeed;

    UINT8  TempoAccensioneLamp;
    UINT16 TempoInvio7F5;
    UINT8  NomiFiltroCustom[4][NUM_CHAR_CUSTOM_FILTER + 1];
    UINT8  IdOffsetTastieraVilla;
    UINT8  ASR003               :1;
    UINT8  displayAngoloTubo    :1;
    UINT8  OffsetZero           :1;
    UINT8  spare                :5;
    UINT8  JitterKnobs;
    UINT16 decSecondsConfig;
    UINT16 TempoInvioInclinometro;
} CollConfigStruct;
#define NUM_TAR_POT     5       // numero di punti per taratura potenziometri Cross/Long


#define MAX_FORMATS 5
typedef struct {
    UINT8 ucFixedCmCross[MAX_FORMATS];
    UINT8 ucFixedCmLong[MAX_FORMATS];
    UINT8 ucFixedInCross[MAX_FORMATS];
    UINT8 ucFixedInLong[MAX_FORMATS];
    UINT8 ucFixedCmIris[MAX_FORMATS];
    UINT8 ucFixedInIris[MAX_FORMATS];
    UINT8 ucNumFormats;
    UINT8 ucNumFormatsIris;
} CollFormatStruct;


typedef struct{
    uword uwBoardAddress;
} CanAddressConfig;

typedef struct{
    int nInclOffsetX;
    int nInclOffsetY;
    int InclOffsetDxX;
    int InclOffsetDxY;
    int InclOffsetSxX;
    int InclOffsetSxY;
    int nMaxCross;
    int nMaxLong;
    uchar ucEncoderSens;
    uchar ucEncoderInv;
    uchar bSquareRotated;
    uchar ucDummyBits:8;
} CollOffsetStruct;

typedef struct{
    union{
        struct {
            bool bStepCheck         :1;
            bool bStartupReset      :1;
            uchar ucResidualTorque  :3;
            bool bDummyMove         :1;
            bool bRevToReset        :1;  //0.53
            uchar bOpenReset        :1;  //0.62
        } OptionBits;
        uchar OptionByte;
    }OptionData;
    uchar ucBladeResetTime;
    uchar ucStepperThreshold;
    uchar ucNoTorqueTime;
}OptionConfiguration;

typedef struct{
    long lPosStepperA;
    long lPosStepperB;
    long lPosStepperC;
}WindowConfiguration;

typedef struct{
    uword uwPosition[NUM_CAL];
    uword uwAnalog[NUM_CAL];
}CalAdcConfiguration;

typedef struct {
    uint16_t  unIdCommand;  // 7A0
    uint16_t  unIdStatus;   // 7F0
    uint16_t  unIdDisplay;  // 7C0
    uint16_t  unIdLamp;     // 700
    uint16_t  unIdRot;      // 740
    uint16_t  unIdIride;    // 750
    uint16_t  unIdOtherCommands;  // 7A1
} CollCanStruct;

typedef struct{
    uword uwTopBuckyCrossCm   [MAX_CASSETTE];
    uword uwTopBuckyCrossIn   [MAX_CASSETTE];
    uword uwTopBuckyCrossAdcCm[MAX_CASSETTE];
    uword uwTopBuckyCrossAdcIn[MAX_CASSETTE];
    uword uwTopBuckyLongCm  [MAX_CASSETTE];
    uword uwTopBuckyLongIn  [MAX_CASSETTE];
    uword uwTopBuckyLongAdcCm[MAX_CASSETTE];
    uword uwTopBuckyLongAdcIn[MAX_CASSETTE];
} CollTopBuckyStruct;

typedef struct{
    uword uwSxBuckyCrossCm [MAX_CASSETTE];
    uword uwSxBuckyCrossIn [MAX_CASSETTE];
    uword uwSxBuckyCrossAdcCm[MAX_CASSETTE];
    uword uwSxBuckyCrossAdcIn[MAX_CASSETTE];
    uword uwSxBuckyLongCm  [MAX_CASSETTE];
    uword uwSxBuckyLongIn  [MAX_CASSETTE];
    uword uwSxBuckyLongAdcCm[MAX_CASSETTE];
    uword uwSxBuckyLongAdcIn[MAX_CASSETTE];
} CollSxBuckyStruct;

typedef struct{
    uword uwDxBuckyCrossCm [MAX_CASSETTE];
    uword uwDxBuckyCrossIn [MAX_CASSETTE];
    uword uwDxBuckyCrossAdcCm[MAX_CASSETTE];
    uword uwDxBuckyCrossAdcIn[MAX_CASSETTE];
    uword uwDxBuckyLongCm  [MAX_CASSETTE];
    uword uwDxBuckyLongIn  [MAX_CASSETTE];
    uword uwDxBuckyLongAdcCm[MAX_CASSETTE];
    uword uwDxBuckyLongAdcIn[MAX_CASSETTE];
} CollDxBuckyStruct;

typedef struct {
    uword uwLowTable;
    uword uwTable25;
    uword uwStandUp;
    uword uwStandLo50;
    uword uwStand100;
    UINT16 uwCollTable;
    UINT16 uwDffHor[7];
    UINT8 ucDummy;
    UINT16 DffDxPotMin;
    UINT16 DffDxMisMin;
    UINT16 DffDxPotMax;
    UINT16 DffDxMisMax;
    UINT16 DffSxPotMin;
    UINT16 DffSxMisMin;
    UINT16 DffSxPotMax;
    UINT16 DffSxMisMax;
} CollCalibrateStruct;

typedef struct{
    uword uwSteps;
    UINT16 StepsResetMin;
    UINT16 StepsResetMax;
    UINT16 uwType;
    UINT8 Config_Filtro[4];
    UINT8 TipoMotore;
} FilterConfigStruct;

typedef struct{
    UINT32 iride_calib_passi[IRIS_FORMAT_MAX + 2];
    UINT16 iride_calib_pot[IRIS_FORMAT_MAX + 2];
} IrisStepsStruct;

typedef enum{CAN_LABEL=6, COLLIMATOR_LABEL, OFFSET_LABEL, TOP_BUCKY_LABEL, SX_BUCKY_LABEL, DX_BUCKY_LABEL, CAL_LABEL, FILTER_LABEL, IRIS_LABEL, FORMAT_LABEL};




typedef struct {
    //InternalFlag        VeeInternalFlag;
    VersionStruct       VeeVersionStruct;
    BoardConfiguration  VeeBoardConfiguration[MAX_STEPPER];
    CollConfigStruct    VeeCollConfigStruct;
    CollFormatStruct    VeeCollFormatStruct;
    CollOffsetStruct    VeeCollOffsetStruct;
    OptionConfiguration VeeOptionConfiguration[MAX_STEPPER];
    WindowConfiguration VeeWindowConfiguration[MAX_WINDOW];
    CollCanStruct       VeeCollCanStruct;
    CanAddressConfig    VeeCanAddressConfig[MAX_STEPPER];
    FilterConfigStruct  VeeFilterConfigStruct;
    IrisStepsStruct     VeeIrisStepsStruct;
} strVeeMainInfo;

#define MAININFOSIZE sizeof(strVeeMainInfo)       // ATTENZIONE: multipli di 4 !!!!!!!!!

typedef struct {
    CollTopBuckyStruct  VeeCollTopBuckyStruct;
    CollSxBuckyStruct   VeeCollSxBuckyStruct;
    CollDxBuckyStruct   VeeCollDxBuckyStruct;
    CollCalibrateStruct VeeCollCalibrateStruct;
} strVeeAnalog;
#define ANALOGSIZE sizeof(strVeeAnalog)       // ATTENZIONE: multipli di 4 !!!!!!!!!


typedef struct {
    UINT16 PassiMax;
    UINT16 Tar_mm[12];
    UINT16 Tar_Pot[12];
    UINT16 Tar_Passi[12];
    UINT16 spare;
} strVeeIride;
#define IRIDESIZE sizeof(strVeeIride)       // ATTENZIONE: multipli di 4 !!!!!!!!!


#define SERIALNUM_LEN   12      // 11 + terminatore

typedef struct {
    UINT16 check_ee;
    UINT16 checksum;        // per applicazione
    UINT32 programSize;
    UINT8 reboot;
    uint8_t  customCob;
    uint16_t  sdoCobRx;
    uint16_t  sdoCobTx;
    uint8_t programExist;
    uint8_t NuovoFirmware;  // se TRUE deve reimpostare i dati di configurazione (tranne setup-table e s/n)

    uint16_t crc16_main;        // crc16 per dati di config
    uint16_t crc16_analog;      // crc16 per dati di config
    uint16_t crc16_sn;      // crc16 per serial number
    uint16_t crc16_calibr;      // crc16 per setup table (calibrazione)
    uint16_t crc16_iride;      // crc16 per setup table

    uint8_t ResetConfig;
    uint8_t ResetSN;
    uint8_t ResetCalibr;
    uint8_t byteSpare;      // per allinearsi al byte pari

    uint8_t serialNumber[SERIALNUM_LEN];

    uint32_t firmwareVersion;       // high(16 bit) = major (tag), low(16b bit) = minor (revision)
    uint32_t stayboot;

    uint8_t configVersion;
    uint8_t calibrVersion;
} strVeeCheckInfo;    // size 28 + x

#define CHECKINFOSIZE sizeof(strVeeCheckInfo)       // ATTENZIONE: multipli di 4 !!!!!!!!!


/////////////////////////////
//Global Variables
/////////////////////////////


#ifdef _SYS_MAIN_
#define main_ext                                       //  0    1    2    3    4    5    6    7    8
    const UINT32 iride_calib_mm[IRIS_FORMAT_MAX + 2] =  { 80,  95, 130, 180, 300, 350, 400, 610, 720};
    const UINT32 iride_calib_perc[IRIS_FORMAT_MAX + 2] = { 0,  28,  80, 169, 387, 472, 569, 942, 960};   // in millesimi

    bool g_bTimeCan = false;
    bool g_bSyncI2C = false;
    bool g_bTimeCanStatusMessage = false;
    bool g_bUpdateA = false;
    bool g_bUpdateB = false;
    bool g_bUpdateC = false;
    bool g_bUpdateD = false;
    UINT32 g_ulUpdateEeprom=0;
    Stepper Steppers[MAX_STEPPER];
    InternalFlag g_InternalFlag;
//    VersionStruct ee_RSR008B_Version;
//    CollConfigStruct ee_ConfigColl;
    //Eeprom copy
//TODO:    NV_type EE_data;        /* Need NV_type structure to use  VirtEE API */
//    BoardConfiguration ee_ConfigBoards[MAX_STEPPER];
//    CanAddressConfig ee_CanAddressConfig[MAX_STEPPER];
//    OptionConfiguration ee_OptionConfig[MAX_STEPPER];
//    WindowConfiguration ee_WindowConfig[MAX_WINDOW];
//    CollCanStruct ee_CanColl;
//    CollTopBuckyStruct ee_TopBuckyColl;
//    CollSxBuckyStruct  ee_SxBuckyColl;
//    CollDxBuckyStruct  ee_DxBuckyColl;
//    CollCalibrateStruct ee_CalColl;
//    FilterConfigStruct ee_FilterConfig;
//    IrisStepsStruct ee_IrisSteps;
//    CollFormatStruct ee_FixFormatColl;
    Relay Relays;

    CalAdcConfiguration ee_CalAdcConfig[MAX_AD_CHANNELS];
//    CollOffsetStruct ee_OffsetColl;

    uword g_uwActFreq[MAX_STEPPER];     //unsigned int fact_a;
    uword g_uwSetFreq[MAX_STEPPER];     //unsigned int fset_a;
    uword g_uwRampTime[MAX_STEPPER];    //struct ramp
    uword g_uwAccStep[MAX_STEPPER];     //unsigned int acc_step_a;
    volatile uword g_uwHomeDelay[MAX_STEPPER];   //delay when home command
    volatile uword g_uwHomeTimeout[MAX_STEPPER]; //timeout when home command
    volatile uword g_uwLowPowerDelay[MAX_STEPPER];//delay when low power command
    volatile uword g_uwHighPowerDelay[MAX_STEPPER];//delay when high power command
    volatile uword g_uwNoTqPowerDelay[MAX_STEPPER];//delay when no torque required
    volatile uword g_uwDelayedTimer[MAX_STEPPER]; //delay to check home position

//  uword g_uwMovingTimer=0;
    volatile uword g_uwFilterDelay=0;

    volatile uword g_uwWaitResetDelay;           //delay after reset, before window
    volatile uword g_uwControlDelay=STARTUP_CONTROL_DELAY;
    long g_lPos[MAX_STEPPER];           //unsigned long pos_a;
    long g_lRun[MAX_STEPPER];           //unsigned long corsa_a;
    long g_lVPos[MAX_STEPPER];          //unsigned long v_pos_a;
    int g_nDelta[MAX_STEPPER];          //int delta_a;
    UINT32 g_lDecPos[MAX_STEPPER];      //unsigned long dec_p_a;
    char g_cDecel[MAX_STEPPER];         //char decell_a;
    long g_lFromPos[MAX_STEPPER];       //unsigned long from_a;
    long g_lMidPoint[MAX_STEPPER];  //unsigned long mid_point_a;
    long g_lDestination[MAX_STEPPER];
    UpdateRate UpdateRates;

    int g_nFilteredAdc[MAX_AD_CHANNELS];
        int campioni_analogico[MAX_AD_CHANNELS][8];
        int media_valore_analogico[MAX_AD_CHANNELS];
#define AN_RANGE    64
    UINT16 an_range_min[MAX_AD_CHANNELS];
    UINT16 an_range_max[MAX_AD_CHANNELS];
    uword g_uwRequiredPosition[MAX_STEPPER]={0xFFFF, 0xFFFF, 0xFFFF};
    uword g_uwActualPosition[MAX_STEPPER];
    bool g_bNewPidRequest[MAX_STEPPER]={false,false,false} ;

    volatile uword g_uwTimeSecondCounter=0;
    uchar g_ucWorkModality=INVALID;
    int g_wCrossMagnitude=0;
    int g_wLongMagnitude=0;
    int s_uwCrossCloseStepsRequired=0;
    int s_uwCrossOpenStepsRequired=0;
    int s_uwLongCloseStepsRequired=0;
    int s_uwLongOpenStepsRequired=0;
    bool g_bInitEnd=false;
    uchar g_ucActualFilterType=0;
    UINT16 g_uwActualDff = 0;
//  uchar g_ucLatDffToBeSent = 0;
//  UINT16 g_uwLatDffFromCan = 0;
//  UINT16 g_uwLateralDffSx = 0;
//  UINT16 g_uwLateralDffDx = 0;
//  UINT16 g_uwVertDffFromCan=0;
//  uchar g_ucDffAnalog=0;
//  uchar g_ucDffTable=0;
    uchar g_ucOrienteer=0;
    uchar g_ucLedToBeOn=NONE;
    uword g_uwCrossSyncTimer=0;
    uword g_uwLongSyncTimer=0;

//  uchar STEPPER_CROSS=STEPPER_A;
//  uchar STEPPER_LONG=STEPPER_B;
    uchar g_ucStatusLed=RED;
    uword RemoteAdc[8];
    RemoteFrame RemoteStatus;
    bool g_bNewRemote=false;


#else
#define main_ext extern
    extern const UINT32 iride_calib_mm[IRIS_FORMAT_MAX + 2];
    extern const UINT32 iride_calib_perc[IRIS_FORMAT_MAX + 2];

    extern bool g_bTimeCan;
    extern bool g_bSyncI2C;
    extern bool g_bTimeCanStatusMessage;
    extern bool g_bUpdateA;
    extern bool g_bUpdateB;
    extern bool g_bUpdateC;
    extern bool g_bUpdateD;

    extern const UINT8 R_release[];
    extern UINT32 g_ulUpdateEeprom;
    extern Stepper Steppers[MAX_STEPPER];
//    extern VersionStruct ee_RSR008B_Version;
//    extern CollConfigStruct ee_ConfigColl;
//    extern CollTopBuckyStruct ee_TopBuckyColl;
//    extern CollSxBuckyStruct  ee_SxBuckyColl;
//    extern CollDxBuckyStruct  ee_DxBuckyColl;
//    extern CollCalibrateStruct ee_CalColl;
//    extern FilterConfigStruct ee_FilterConfig;
//    extern IrisStepsStruct ee_IrisSteps;
//    extern CollFormatStruct ee_FixFormatColl;

    extern InternalFlag g_InternalFlag;
//TODO:    extern NV_type EE_data;     /* Need NV_type structure to use  VirtEE API */
//    extern BoardConfiguration ee_ConfigBoards[MAX_STEPPER];
//    extern CanAddressConfig ee_CanAddressConfig[MAX_STEPPER];
//    extern CollOffsetStruct ee_OffsetColl;

//    extern CalAdcConfiguration ee_CalAdcConfig[MAX_AD_CHANNELS];
//    extern OptionConfiguration ee_OptionConfig[MAX_STEPPER];
//    extern WindowConfiguration ee_WindowConfig[MAX_WINDOW];
//    extern CollCanStruct ee_CanColl;
    extern Relay Relays;

    extern uword g_uwActFreq[MAX_STEPPER];
    extern uword g_uwSetFreq[MAX_STEPPER];
    extern uword g_uwRampTime[MAX_STEPPER];
    extern uword g_uwAccStep[MAX_STEPPER];
    extern volatile uword g_uwHomeDelay[MAX_STEPPER];
    extern volatile uword g_uwHomeTimeout[MAX_STEPPER];
    extern volatile uword g_uwLowPowerDelay[MAX_STEPPER];
    extern volatile uword g_uwHighPowerDelay[MAX_STEPPER];
    extern volatile uword g_uwNoTqPowerDelay[MAX_STEPPER];
    extern volatile uword g_uwDelayedTimer[MAX_STEPPER];

    extern volatile uword g_uwWaitResetDelay;
    extern volatile uword g_uwControlDelay;
    extern long g_lPos[MAX_STEPPER];
    extern long g_lRun[MAX_STEPPER];
    extern long g_lVPos[MAX_STEPPER];
    extern int g_nDelta[MAX_STEPPER];
    extern UINT32 g_lDecPos[MAX_STEPPER];
    extern char g_cDecel[MAX_STEPPER];
    extern long g_lFromPos[MAX_STEPPER];
    extern long g_lMidPoint[MAX_STEPPER];
    extern long g_lDestination[MAX_STEPPER];
    extern UpdateRate UpdateRates;

    extern int g_nFilteredAdc[MAX_AD_CHANNELS];
        extern int campioni_analogico[MAX_AD_CHANNELS][8];
        extern int media_valore_analogico[MAX_AD_CHANNELS];
    extern uword g_uwRequiredPosition[MAX_STEPPER];
    extern uword g_uwActualPosition[MAX_STEPPER];
    extern bool g_bNewPidRequest[MAX_STEPPER];

    extern volatile uword g_uwTimeSecondCounter;
    extern uchar g_ucWorkModality;

    extern int  g_wCrossMagnitude;
    extern int g_wLongMagnitude;
    extern int s_uwCrossCloseStepsRequired;
    extern int s_uwCrossOpenStepsRequired;
    extern int s_uwLongCloseStepsRequired;
    extern int s_uwLongOpenStepsRequired;
    extern bool g_bInitEnd;
    extern uchar g_ucActualFilterType;
    extern UINT16 g_uwActualDff;
//  extern uchar g_ucLatDffToBeSent;
//  extern UINT16 g_uwLatDffFromCan;
//  extern UINT16 g_uwLateralDffSx;
//  extern UINT16 g_uwLateralDffDx;
//  extern UINT16 g_uwVertDffFromCan;
//  extern uchar g_ucDffAnalog;
//  extern uchar g_ucDffTable;
    extern uchar g_ucOrienteer;
    extern uchar g_ucLedToBeOn;
    extern uword g_uwCrossSyncTimer;
    extern uword g_uwLongSyncTimer;
//  extern uword g_uwMovingTimer;
    extern volatile uword g_uwFilterDelay;

//  extern uchar STEPPER_CROSS;
//  extern uchar STEPPER_LONG;
    extern uchar g_ucStatusLed;
    extern uword RemoteAdc[8];
    extern RemoteFrame RemoteStatus;
    extern bool g_bNewRemote;

#endif



main_ext strVeeCheckInfo veeCheckInfo;

main_ext strVeeMainInfo veeMainInfo;

//#define g_InternalFlag      veeMainInfo.VeeInternalFlag
#define ee_RSR008B_Version  veeMainInfo.VeeVersionStruct        // ok init
#define ee_ConfigBoards     veeMainInfo.VeeBoardConfiguration   // ok init
#define Temp_Motor_Config   veeMainInfo.VeeTemp_Motor_Config
#define ee_ConfigColl       veeMainInfo.VeeCollConfigStruct     // ok init
#define ee_FixFormatColl    veeMainInfo.VeeCollFormatStruct     // ok init
#define ee_CanAddressConfig veeMainInfo.VeeCanAddressConfig     // ok init
#define ee_OffsetColl       veeMainInfo.VeeCollOffsetStruct     // ok init
#define ee_OptionConfig     veeMainInfo.VeeOptionConfiguration  // ok init
#define ee_WindowConfig     veeMainInfo.VeeWindowConfiguration  // ok init
#define ee_CanColl          veeMainInfo.VeeCollCanStruct        // ok init
#define ee_FilterConfig     veeMainInfo.VeeFilterConfigStruct   // ok init
#define ee_IrisSteps        veeMainInfo.VeeIrisStepsStruct
#define ee_SetColl          veeMainInfo.VeeCollSettingStruct    // ok init


main_ext strVeeAnalog veeAnalog;

#define ee_TopBuckyColl     veeAnalog.VeeCollTopBuckyStruct     // ok init
#define ee_SxBuckyColl      veeAnalog.VeeCollSxBuckyStruct      // ok init
#define ee_DxBuckyColl      veeAnalog.VeeCollDxBuckyStruct      // ok init
#define ee_CalColl          veeAnalog.VeeCollCalibrateStruct    // ok init

main_ext strVeeIride dato_ee_iride;        // emulazione iride







main_ext union      // byte di stato dei motori
{
    UINT8 all;
    struct
    {
        UINT8   in_moto :1;     // bit basso
        UINT8   bo      :5;
        UINT8   por     :1;
        UINT8   home    :1;
    } str;
} volatile stato_mot[4];

#define DFFVAL_VERT_CAN         0
#define DFFVAL_VERT_POT_STATIVO 1
#define DFFVAL_VERT_POT_TABLE   2
#define DFFVAL_LAT_SX_CAN       3
#define DFFVAL_LAT_SX_003       4
#define DFFVAL_LAT_DX_CAN       5
#define DFFVAL_LAT_DX_003       6
main_ext UINT16 IngressoDFF[10];    // Valore DFF che arriva da tutti gli input
main_ext UINT8 NewDFF[10];


#define FRMT_VERT_CAN       0
#define FRMT_VERT_003       1
#define FRMT_LAT_SX_CAN     2
#define FRMT_LAT_SX_003     3
#define FRMT_LAT_DX_CAN     4
#define FRMT_LAT_DX_003     5
main_ext UINT16 FormatoCross[10];    // Formato CROSS
main_ext UINT16 FormatoLong[10];    // Formato LONG
main_ext UINT16 FormatoIride[10];    // Formato IRIDE
main_ext UINT8 NewFormat[10];       // Nuovo formato arrivato
main_ext UINT8 NewIrisFormat[10];       // Nuovo formato arrivato
main_ext UINT8 FormatoIrideValido;

main_ext UINT8 FormatoValido[5];        // flag se formato OK
main_ext UINT8 DffValida[5];        // flag se DFF OK
main_ext UINT8 AutoLimiti[5];
main_ext UINT8 AutoLimite;
main_ext UINT8 ResetPosizione;      // TRUE = ripeti il formato


main_ext uchar s_bFlag_a, s_bFlag_b, s_bFlag_c;
main_ext uword g_uwTimeMilliSecondCounter;
main_ext uword g_uwFilterAdxlXArray[ADXLX_ORDER];
main_ext uword g_uwFilterAdxlYArray[ADXLY_ORDER];
main_ext uword g_uwFilterCrossArray[CROSS_ORDER];
main_ext uword g_uwFilterLongArray[LONG_ORDER];
main_ext uchar s_ucFilterIndex1;
main_ext uchar s_ucFilterIndex2;
main_ext uchar s_ucFilterIndex3;
main_ext uchar s_ucFilterIndex4;
main_ext uword s_uwFilterOut1;
main_ext uword s_uwFilterOut2;
main_ext uword s_uwFilterOut3;
main_ext uword s_uwFilterOut4;
main_ext SINT16 pos_cross, pos_long, pos_iride;     // posizione teorica potenziometri per controllo allarme
main_ext UINT8 volatile timer_stop_a, timer_stop_b, timer_stop_iride;  // timer dec ogni 10ms
main_ext UINT8 motore_a_in_mov, motore_b_in_mov, motore_iride_in_mov;
main_ext UINT8 pos_chiave_prec;
main_ext SINT32 gradi_rot;

main_ext UINT8 fluoroscopia_on;
main_ext volatile UINT8 visua_main;
main_ext UINT16 Iris_MaxCross, Iris_MaxLong;

main_ext UINT8 inclinazione_cross, inclinazione_long;
main_ext UINT32 mult_incl_cross, mult_incl_long;

main_ext volatile UINT8 timer_alarm_calib;
main_ext volatile UINT8 timerWaitIride;

#ifdef SYS_CANOPEN
main_ext UINT16 timer_canopen;       // in ms tempo tra un msg e l'altro
main_ext UINT16 timer_canopen_wait;

#endif
main_ext union
    {
        UINT8 all;
        struct bitt8 bit8;
    } volatile flag_1;

#define _filter_in_reset       flag_1.bit8.bit0     // TRUE = in stato di reset


main_ext SINT32 posizione_rot;
main_ext UINT16 adc_rot;
main_ext volatile UINT8 timer_rotazione;

main_ext SINT32 posizione_iride, test_iris_passi;
main_ext UINT16 adc_iride;
main_ext UINT16 Iride_PassiMax;
main_ext volatile UINT8 timer_iride;
main_ext UINT8 stato_iride;
#define IRIDE_MOVE      0x01        // bit di movimento
#define IRIDE_RESET     0xFF        // in stato di reset
#define IRIDE_HOME      0x80        // bit di posizione HOME
#define IRIDE_MAN       0x08        // bit di movimento manuale


#define MAXKB   60      // in ms per antirimbalzo tasto
main_ext volatile UINT8 key_press_event, key_released_event;
main_ext volatile UINT8 key_pressed, azionato, val_tasto;
main_ext UINT8 ctkb;
main_ext volatile UINT8 timer_display, write_msg;
main_ext UINT8 stato_tastiera;

//main_ext volatile UINT8 rimb_dig[8];

main_ext volatile UINT8 rimb_home, rimb_key;
main_ext volatile UINT16 rimb_key_filter, rimb_key_lamp;
main_ext volatile UINT8 HOME_C, home_cprec, numero_fori_filter, fori_da_contare, pos_filtro_attuale;
main_ext UINT8 filtreq;
main_ext UINT8 stato_home_mot[MAX_STEPPER];
main_ext UINT16 step_home;
main_ext UINT8 filtro_cambiato;
main_ext UINT8 try_filter;
main_ext UINT8 reset_filter_try;
main_ext UINT8 allarmi_totale;
main_ext UINT8 alarm_dff;
main_ext UINT8 err_counter_filter, err_counter_cross, err_counter_long, err_counter_iride;
main_ext UINT8 come_from_error_filter;
main_ext bool g_bBlinkFilterTime;
main_ext bool g_bTimeToShowFilterError;
main_ext volatile UINT8 timer_blink, led_error;
main_ext UINT8 KEY_MANUAL;
main_ext UINT8 FILTER_SWITCH;
main_ext UINT8 LAMP_SWITCH;

main_ext bool reset_driver;
main_ext volatile UINT8 timer_spost_lamelle, era_con_tasti;



main_ext volatile UINT32 frequenza_timer;

main_ext SINT32 *i_Pos, *mi_Pos;
main_ext UINT8 *pt_st_mot, *opmot;
main_ext UINT32 *FreqAct;
main_ext UINT32 *NumPassiRampa;
main_ext UINT32 *StopMotFreq;

main_ext UINT8 stato_motore_irq[MAX_STEPPER];
main_ext UINT8 comando_main[MAX_STEPPER];
main_ext UINT8 flag_motore[MAX_STEPPER];
main_ext UINT8 Comando_Motore[MAX_STEPPER];      // TRUE = ferma motore in movimento
main_ext UINT16 timer_idle[MAX_STEPPER];
main_ext UINT8 Reset_Run[MAX_STEPPER];      // TRUE = reset in corso (azzerato da irq)

main_ext SINT32 i_MotPosizione[MAX_STEPPER], m_MotPosizione[MAX_STEPPER], mi_MotPosizione[MAX_STEPPER];        // posizione attuale
main_ext UINT32 passi_rampa[MAX_STEPPER];
main_ext UINT8 op_mot[MAX_STEPPER];

main_ext volatile UINT16 timer_motor_on[MAX_STEPPER];
main_ext UINT8 Com_Irq[MAX_STEPPER];

main_ext UINT32 i_MotMinFreq[MAX_STEPPER];          // frequenza minima movimento
main_ext UINT32 i_MotMaxFreq[MAX_STEPPER];          // frequenza massima movimento
main_ext UINT32 i_MotActFreq[MAX_STEPPER];          // frequenza attuale
main_ext UINT32 i_MotStepFreq[MAX_STEPPER];         // variazione della frequenza (calcolata ogni n ms)
main_ext UINT32 i_MotStopFreq[MAX_STEPPER];
main_ext UINT16 i_MotIncIrq[MAX_STEPPER];
main_ext UINT32 i_MotFreqMotMult[MAX_STEPPER];
main_ext volatile UINT16 Timeout_stepper[MAX_STEPPER];


//main_ext CAN_STD_DATA_DEF rx_temp;



main_ext UINT16 timer_tx_master;        // ogni ms
main_ext UINT8 ScattatoTimeout[MAX_STEPPER];

main_ext UINT8 ContaIrq;



main_ext UINT16 dff_attuale, LateralDFF, DisplayDFF, TomographyDFF;
main_ext UINT8 DisplayFTD;
main_ext UINT8 StatusPrec[MAX_STEPPER];
main_ext UINT8 KeyReturnAuto[MAX_STEPPER];
main_ext UINT8 MovimentoManuale[MAX_STEPPER];

main_ext UINT32 dff_can, dff_slave[MAX_STEPPER];
main_ext UINT8 stato_mov[MAX_STEPPER];
main_ext UINT16 timer_slave[MAX_STEPPER];
main_ext UINT16 timer_gestione[MAX_STEPPER];
main_ext SINT32 formato_mm[MAX_STEPPER];
main_ext SINT32 formato_old[MAX_STEPPER];
main_ext SINT32 formato_max[MAX_STEPPER];
main_ext SINT32 formato_min[MAX_STEPPER];
main_ext UINT8 force_tx_7F0[MAX_STEPPER];
//main_ext SINT32 steps_apertura[MAX_STEPPER];
main_ext UINT32 mask_back[MAX_STEPPER];
main_ext SINT32 passi_slave[MAX_STEPPER];

main_ext SINT16 EncoderValue[MAX_STEPPER];
main_ext UINT8 StopFromEncoder[MAX_STEPPER];

main_ext UINT16 misura_temp;
main_ext UINT8 misura_stato;

main_ext UINT8 EncoderActual[MAX_STEPPER], EncoderLast[MAX_STEPPER];
main_ext UINT8 EncoderErrors[MAX_STEPPER];
main_ext UINT8 EncoderSpeed[MAX_STEPPER];
main_ext UINT8 EncoderDir[MAX_STEPPER];

main_ext volatile UINT16 timer_keys_switch;       // 100 ms
main_ext volatile UINT16 timer_keys_lamp;       // 100 ms
main_ext UINT8 stato_switch_context;

main_ext volatile UINT16 timer_test, timer_luce_test;

main_ext volatile UINT8 timer_scorrimento;       // 10 ms
main_ext UINT8 FiltroInErrore;
main_ext UINT8 VisualizzaFiltro;

main_ext UINT16 ID_OtherCommand;

main_ext SINT16 valoreAsseX, valoreAsseY;



//********  STRUTTURA EVENTI  *********
typedef struct {
    UINT16 TimeStamp;
    UINT8 Evento;
    UINT8 Motore;
    UINT8 Par1;
    UINT8 Par2;
    UINT32 dato;
} strLogEvento;





// **************************************
//   generazione degli eventi ed errori
// **************************************
#define NUM_MAX_EVENTI      100
enum {
    EV_MOT_NONE = 0,        // 0
    EV_POWER_ON,            //
    EV_MOT_STARTED,         //
    EV_MOT_STOPPED,         //
    EV_DSC_STARTED,         //
    EV_DSC_STOPPED,         // 0x05
    EV_DONE,                //
    EV_INFO,                //
    EV_LIGHT,               //
    EV_CANBUS,              //
    EV_INIT_START,          // 0x0A
    EV_MASTER_READY,        //
    EV_CHIAVE,              //
    EV_FILTER_READY,        //
    EV_PASSI_PHOTO,         //
    EV_POTENZIOMETRO,       // 0x0F
        EV_TASTO_FRONTALE       //
};
#define EV_ERROR    0x40

//****  definizioni per EV_ERROR  ****
#define EV_ERROR_FRMT_ERR       0x01
#define EV_ERROR_FILTRO         0x02
#define EV_ERROR_TEMP_HIGH      0x03
#define EV_ERROR_TEMP_LOW       0x04
#define EV_ERROR_MOT_TIMEOUT    0x05
#define EV_ERROR_POT_SHUTTER    0x06
#define EV_ERROR_HOME_MOTORE    0x07
#define EV_ERROR_FAILURE        0x08
#define EV_ERROR_POT_CALIB      0x09
#define EV_ERROR_LED_LIGHT      0x0A
#define EV_ERROR_MEM_ERROR      0x0B
#define EV_ERROR_CAN_FIFO       0x0C
#define EV_ERROR_FILTER         0x0D



//****  definizioni per EV_INFO  ****
#define EV_INFO_TEMP_NORM       0x01
#define EV_INFO_LED_LIGHT       0x02

//****  definizioni per EV_LIGHT  ****
#define EV_LIGHT_CAN        0x01
#define EV_LIGHT_KEY        0x02
#define EV_LIGHT_TRIGGER    0x03
#define EV_LIGHT_MANOPOLA   0x04

//****  definizioni per EV_CANBUS e analisi registro errori ****
#define EV_CAN_ACTIVE       0x01
#define EV_CAN_PASSIVE      0x02
#define EV_CAN_BUSOFF_ENTRY 0x03
#define EV_CAN_BUSOFF_RECOV 0x04

//****  definizioni per EV_ERROR_MEM_ERROR  ****
#define MEM_ERR_CALIBR      0x01
#define MEM_ERR_SN          0x02
#define MEM_ERR_CONFIG      0x03

//****  definizioni per EV_ERROR_MEM_ERROR  ****
#define FIFO_ERR_STD        0x01
#define FIFO_ERR_CONF       0x02
#define FIFO_ERR_REM        0x03



//  ****  EVENTI  ****
main_ext strLogEvento fifo_eventi[NUM_MAX_EVENTI];
main_ext UINT16 ixLogEvI, ixLogEvF;
main_ext UINT16 timestamp_evento;



#define NUM_PASSI_TEST_PHOTO        50
main_ext UINT8 TestPassi;
main_ext UINT8 checkNewPhoto[MAX_STEPPER];
main_ext SINT32 PassiPhoto[MAX_STEPPER];
main_ext UINT8 generaEventoPassi[MAX_STEPPER];
main_ext UINT8 rimbalzo_home_shutter;

main_ext UINT8 EnableInvioEventi;


//*****************************
//  PROTOTIPI DI FUNZIONE
//*****************************

void sys_Main_GetMilliSecond(uword* puwTimeCounter);
void sys_Main_GetSecond(uword* puwTimeCounter);
void sys_adc_driver(void);
void scan_kb(void);

void gestione_antirimbalzo(void);
SINT16 calcola_pos_pot(UINT8 pot, UINT32 steps);
void controlla_posizione_pot(void);
void check_spostamento_lamelle(void);
void sys_delayed_check(void);
void check_and_move(void);

UINT8 MOT_controlla_passi_motore(UINT8 stepper);
void MOT_ricalcola_motore(UINT8 stepper);
void gestione_idle(void);
void CAN_tx_msg(void);
void Iris_Variable_init(UINT8 reset);
void receive_metro(void);

void Test_Fiera(void);
void Test_IMQ(void);

void mainApplication(void);
void variable_init(void);

void sys_init(void);
void ConfigurePortPins(void);
void sys_dataflash_init(void);
void sys_eeprom_sync(void);

void check_keys_switch_context(void);
void resetCollimator(void);

static uint16_t CRC16 (uint8_t *nData, uint16_t wLength);
void calcola_crc(UINT8 erase);


