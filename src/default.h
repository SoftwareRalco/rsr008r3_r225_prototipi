
//****************************************
//  DEFINIZIONI PERSONALIZZATE
//****************************************

//  Collimazione in modalita' manuale ABILITATA
#define DEF_MANUAL_COLL     MANUAL_COLL_EN
//  visualizza apertura iride quando in radiografia
#define DEF_VISUA_APE_IRIS  TRUE

#define ID_COM_COLL                 0x7A0
#define ID_ALLARMI                  0x100
#define ID_STATUS                   0x7F0
#define ID_COM_COLL_OTHER           0x7A0

#define STATUS_RISP_A3              0x08        // si usa con ID_STATUS (ID_STATUS + STATUS_RISP_A3)

#define DEF_RISPOSTE_7F8            FALSE
#define DEF_TASTIERA_VILLA          FALSE
#define DEF_ABILITAZIONE_ASR003     TRUE
#define DEF_DISPLAY_TUBO            FALSE

#define DEF_JITTER_KNOBS            1
#define DEF_SECONDS_CONFIG          25

