

#include "include.h"

//#include "sys_main.h"

/*
 *  all'inizio il collimatore esegue il reset e poi resta in attesa
 *  Ad ogni pressione viene cambiato il ciclo di test:
 *  1. tutto fermo motori in presa
 *  2. motori + luce 30 on + 30 off
 *  3. motori in movimento e luce spenta
 *  4. motori fermi e luce accesa
 *  5. ritorno al punto 1
 *
 */


//********************************************
//********************************************

#define TEMPO_MOV           80
#define TEMPO_TEST_LUCE     30  // in secondi
#define tastino             buffer_latch[IN_SWITCH_ROT]


void Test_IMQ(void)
{
    static UINT8 formato = 0;
    static UINT8 stato_test = 0;
    static UINT8 esegui_test_luce = FALSE;
    static UINT8 stato_test_luce = 0;
    int32_t Iride, Rotazione;

    UINT16 Cross, Long;

    switch (stato_test)
    {
    case 0:
        FormatoCross[FRMT_VERT_CAN] = 430;
        FormatoLong[FRMT_VERT_CAN] = 430;
        NewFormat[FRMT_VERT_CAN] = TRUE;
	    IngressoDFF[DFFVAL_VERT_CAN] = 100;
        g_bManualRequestFromCan = false;
        esegui_test_luce = FALSE;
        tastino = FALSE;
        stato_test_luce = 0;
        RequestLightStatus = COM_LIGHT_OFF;
        stato_test = 1;
        break;

    case 1:
        if (tastino == TRUE)
        {
            tastino = FALSE;
            timer_luce_test = 1;
            stato_test_luce = 0;
            esegui_test_luce = TRUE;
            stato_test = 2;
        }
        break;

    case 2:
        if (timer_test == 0)
        {
            timer_test = TEMPO_MOV;

            if (formato == 0)
            {
                formato = 1;
                Cross = 350;
                Long = 350;
                Iride = 10000;
                Rotazione = 10000;
            }
            else
            {
                formato = 0;
                Cross = 50;
                Long = 50;
                Iride = 500;
                Rotazione = 0;
            }
            FormatoCross[FRMT_VERT_CAN] = Cross;
            FormatoLong[FRMT_VERT_CAN] = Long;
            NewFormat[FRMT_VERT_CAN] = TRUE;

            if (g_ucPuntaFiltro < 3)
                g_ucPuntaFiltro++;
            else
                g_ucPuntaFiltro = 0;
            g_ucFilterRequired = ee_FilterConfig.Config_Filtro[g_ucPuntaFiltro];

            tx_temp.dlc = 5;
            tx_temp.id = 0x782;
            tx_temp.data[0] = 0x01;
            tx_temp.data[1] = (Iride >> 24) & 0xFF;
            tx_temp.data[2] = (Iride >> 16) & 0xFF;
            tx_temp.data[3] = (Iride >> 8) & 0xFF;
            tx_temp.data[4] = Iride & 0xFF;
            copy_tx_temp(FALSE);

            tx_temp.dlc = 5;
            tx_temp.id = 0x752;
            tx_temp.data[0] = 0x01;
            tx_temp.data[1] = (Rotazione >> 24) & 0xFF;
            tx_temp.data[2] = (Rotazione >> 16) & 0xFF;
            tx_temp.data[3] = (Rotazione >> 8) & 0xFF;
            tx_temp.data[4] = Rotazione & 0xFF;
            copy_tx_temp(FALSE);

        }

        if (tastino == TRUE)
        {
            tastino = FALSE;
            if (esegui_test_luce == TRUE)
            {
                esegui_test_luce = FALSE;
                RequestLightStatus = COM_LIGHT_OFF;
            }
            else
            {
                esegui_test_luce = FALSE;
                RequestLightStatus = COM_LIGHT_ON;
                stato_test = 3;
            }
        }
        break;

    case 3:
        if (tastino == TRUE)
        {
            tastino = FALSE;
            stato_test = 0;
        }
        break;

    default:
        stato_test = 0;
        break;
    }

    if (esegui_test_luce == TRUE) // per controllare la luce
    {
        if (timer_luce_test == 0)
        {
            timer_luce_test = TEMPO_TEST_LUCE * 10;
            if (stato_test_luce == 0)
            {
                stato_test_luce = 1;
                RequestLightStatus = COM_LIGHT_ON;
            }
            else
            {
                stato_test_luce = 0;
                RequestLightStatus = COM_LIGHT_OFF;
            }
        }
    }








/*


    static UINT8 st_test = 0;
    static UINT8 luce = FALSE;
    UINT8 filtro;
    UINT16 temp16, Cross, Long;
    static UINT16 max, min;


    temp16 = g_nFilteredAdc[POT_CROSS] >> 2;
    if (temp16 > max)
    {
        max = temp16;
        if (max >= 2)
            min = max - 2;
        else
            min = 0;
    }
    else if (temp16 < min)
    {
        min = temp16;
        max = temp16 + 2;
        if (min >= 255)
        {
            min = 254;
            max = 256;
        }
    }
    temp16 = (min + max) >> 1;

    IngressoDFF[DFFVAL_VERT_CAN] = temp16;
    //    g_uwActualDff = temp16;
    //    dff_attuale = temp16;

    if (HOME_B == 0) // controllo switch modalita'
    {
        st_test = 1;
        KeyLightTimer = 300;
        timer_test = 0;
        return;
    }

    g_bManualRequestFromCan = false;

    if (timer_test)
    {
        if ((timer_test <= 20) && (luce == TRUE))
        {
            luce = FALSE;
            KeyLightTimer = 50;
            g_bRequestLightOn = true;
        }
        return;
    }

    luce = TRUE;
    filtro = FALSE;

    switch (st_test)
    {
    case 0:
        timer_test = 200; // attesa 20 secondi
        break;


        //  *****  300 x 300  ****
    case 1:
        timer_test = TEMPO_MOV;
        Cross = 300;
        Long = 300;
        break;

        //  *****  240 x 180  ****
    case 2:
        timer_test = TEMPO_MOV;
        Cross = 180;
        Long = 240;
        break;

        //  *****  180 x 240  ****
    case 3:
        timer_test = TEMPO_MOV;
        Cross = 240;
        Long = 180;
        break;

        //  *****  100 x 130  ****
    case 4:
        timer_test = TEMPO_MOV;
        Cross = 100;
        Long = 130;
        break;

        //  *****  430 x 430  ****
    case 5:
        timer_test = TEMPO_MOV;
        Cross = 430;
        Long = 430;
        break;

    default:
        st_test = 1;
        break;

    }

    if (g_ucPuntaFiltro < 3)
        g_ucPuntaFiltro++;
    else
        g_ucPuntaFiltro = 0;

    g_ucFilterRequired = ee_FilterConfig.Config_Filtro[g_ucPuntaFiltro];

    FormatoCross[FRMT_VERT_CAN] = Cross;
    FormatoLong[FRMT_VERT_CAN] = Long;
    NewFormat[FRMT_VERT_CAN] = TRUE;

    st_test++;

*/

}
