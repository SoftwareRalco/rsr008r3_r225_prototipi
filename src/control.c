#define _SYS_CONTROL_

#include "include.h"
#include "string.h"

#define CONTROL_INIT            'I'
#define CONTROL_CHECK_PHOTO     'K'
#define CONTROL_CHECK_AUTORESET 'C'
#define CONTROL_WAIT_CMD        'W'
#define CONTROL_CHECK_HOME      'H'
#define CONTROL_MOVE_ABS        'A'
#define CONTROL_MOVE_REL        'R'
#define CONTROL_RELEASE_STEPPER 'L'

#define HOME_START  0
#define HOME_REV    1
#define HOME_FWD    2
#define HOME_STOP   3

#define STARTUP_STEPS   200
#define THR_START       0   //
#define THR_POSITION    0   //*0.1%=0.5%

static uchar g_ucControlState=CONTROL_INIT;
static uword g_uwMessageCounter=0;

static bool g_ucHomeStartedA = HOME_START;
static bool g_ucHomeStartedB = HOME_START;
static bool g_ucHomeStartedC = HOME_START;

//--------------------------------------------------------------
void sys_reset_stepper_var(uchar ucStepper){

    if (ucStepper>=MAX_STEPPER)
        return;

    g_lDecPos[ucStepper]=0;
    g_cDecel[ucStepper]=0;
    g_lPos[ucStepper]=0;
    g_lVPos[ucStepper]=0;
    g_lFromPos[ucStepper]=0;
}



//--------------------------------------------------------------
void sys_control_init(UINT8 flag)
{

//    dff_can = 0;      // in cm

    timer_tx_master = 100;    // attesa prima di effettuare il reset
    if (flag == TRUE)
    {
        stato_master = MASTER_FIRST;
        timer_tx_master = 1000;    // attesa prima di effettuare il reset
    }

    LedStatus[STEPPER_CROSS] = RED;
    LedStatus[STEPPER_LONG] = RED;
    LedStatus[STEPPER_LONG2] = RED;
    LedStatus[STEPPER_IRIS] = RED;

}



//****************************
//  chiamata ogni 1 ms
//****************************
void timer_master_1ms(void)
{
    UINT8 i;

    if (timer_tx_master)
        timer_tx_master--;

    for (i = 0; i < 8; i++)
    {
        if (TimerLed[i])
        {
            TimerLed[i]--;
        }
    }

    for (i = 0; i < MAX_STEPPER; i++)
    {
        if (timer_slave[i])
        {
            timer_slave[i]--;
        }
    }

}





void load_frequenze(void)
{
    UINT8 i;

    for (i = STEPPER_A; i < MAX_STEPPER; i++)       // carica valori di velocita' motori
    {
        Steppers[i].ucLevel = ee_ConfigBoards[i].ucPhoto;
        Steppers[i].uwFMin  = ee_ConfigBoards[i].uwFMin;
        Steppers[i].uwFMax  = ee_ConfigBoards[i].uwFMax;
        Steppers[i].uwTRamp = ee_ConfigBoards[i].uwRamp;
        Steppers[i].uwFMaxFast  = ee_ConfigBoards[i].uwFMaxFast;
    }

}




//*************************************
//  imposta i passi iniziali al reset
//*************************************
void SetPassiIniziali(void)
{
    UINT8 i;

    for (i = 0; i < 2; i++)
    {
        m_MotPosizione[i] = g_MaxSteps[i] - NUM_PASSI_TEST_PHOTO;
        i_MotPosizione[i] = g_MaxSteps[i] - NUM_PASSI_TEST_PHOTO;
        mi_MotPosizione[i] = g_MaxSteps[i] - NUM_PASSI_TEST_PHOTO;
        g_lVPos[i] = g_MaxSteps[i] - NUM_PASSI_TEST_PHOTO;
    }
}





//********************************************************************
//  inserisce in fifo_eventi i parametri specificati
//********************************************************************
void genera_evento(UINT8 evento, UINT8 motore, UINT8 Par1, UINT8 Par2, UINT32 dato)
{
    strLogEvento *ptrEv;

    ptrEv = &fifo_eventi[ixLogEvI];
    ptrEv->TimeStamp = timestamp_evento;
    ptrEv->Evento = evento;
    ptrEv->Motore = motore;
    ptrEv->Par1 = Par1;
    ptrEv->Par2 = Par2;
    ptrEv->dato = dato;

    if (++ixLogEvI >= NUM_MAX_EVENTI)        // aggiorna puntatore fifo
    {
        ixLogEvI = 0;
    }

    if (ixLogEvI == ixLogEvF)        // se raggiunge il piu' vecchio messaggio lo cancella
    {
        if (++ixLogEvF >= NUM_MAX_EVENTI)
            ixLogEvF = 0;
    }

}







//*****************************************************************
//  loop per controllare se un evento e' da generare
//*****************************************************************
void controlla_generazione_eventi(void)
{
    UINT8 i;
    ioport_level_t pinlevel;
    static UINT8 chiave_prec = 0xFF;        // per generare un evento
    static UINT8 light_prec = 0xFF;        // per generare un evento


    for (i = 0; i < 4; i++)
    {
        if (generaEventoPassi[i] == TRUE)
        {
            generaEventoPassi[i] = FALSE;
            genera_evento(EV_PASSI_PHOTO, i + 1, 0, 0, PassiPhoto[i]);
        }
    }

    if (KEY_AUTO)
    {
        if (chiave_prec != 0)
        {
            chiave_prec = 0;
            genera_evento(EV_CHIAVE, 0, 0, 0, 0);
        }
    }
    else
    {
        if (chiave_prec != 1)
        {
            chiave_prec = 1;
            genera_evento(EV_CHIAVE, 0, 0, 0, 1);
        }
    }

    if (key_press_event == TRUE)
    {
        key_press_event = FALSE;
        genera_evento(EV_TASTO_FRONTALE, val_tasto, 0, 0, TRUE);
    }

    if (key_released_event == TRUE)
    {
        key_released_event = FALSE;
        genera_evento(EV_TASTO_FRONTALE, val_tasto, 0, 0, FALSE);
    }

    if (Luce_Read_Pin() == IOPORT_LEVEL_HIGH)
    {
        if (light_prec != TRUE)
        {
            light_prec = TRUE;
            genera_evento(EV_LIGHT, 0, 0, 0, TRUE);
        }
    }
    else
    {
        if (light_prec != FALSE)
        {
            light_prec = FALSE;
            genera_evento(EV_LIGHT, 0, 0, 0, FALSE);
        }
    }

}









void push_msg_can(UINT16 id, UINT8 dlc, UINT8 d0, UINT8 d1, UINT8 d2, UINT8 d3, UINT8 d4, UINT8 d5, UINT8 d6, UINT8 d7)
{
    CAN_STD_DATA_DEF *ptr;

//    __disable_interrupts();
    __disable_irq();
    ptr = &rx_dati_0[ix0I];
    if (++ix0I >= NUM_BUFF_RX_O)        // aggiorna puntatore fifo
        ix0I = 0;
    if (ix0I == ix0F)        // se raggiunge il piu' vecchio messaggio lo cancella
    {
        //                Can0_overrun = TRUE;
        if (++ix0F >= NUM_BUFF_RX_O)
            ix0F = 0;
    }
//    __enable_interrupts();
    __enable_irq();
    ptr->dlc = dlc;
    ptr->id = id;
    ptr->data.data[0] = d0;
    ptr->data.data[1] = d1;
    ptr->data.data[2] = d2;
    ptr->data.data[3] = d3;
    ptr->data.data[4] = d4;
    ptr->data.data[5] = d5;
    ptr->data.data[6] = d6;
    ptr->data.data[7] = d7;
}




//*************************************************
//  Controllo funzionamento generale collimatore
//*************************************************
void sys_control_master(UINT8 lamelle)
{
    UINT8 i, flag;
    UINT32 temp32;


    switch (stato_master)
    {
    case MASTER_FIRST:
        if (timer_tx_master == 0)
        {
            load_frequenze();
            CollInReset |= STATO_LAMELLE_RESET;
            stato_master = MASTER_RESET;
        }
        break;

    case MASTER_RESET:      // reset di tutti gli slave
        if (timer_tx_master == 0)
        {
            config_movimento(STEPPER_CROSS, MSG_MOV_HOME, 0, MOV_RESET);
            config_movimento(STEPPER_LONG, MSG_MOV_HOME, 0, MOV_RESET);
            config_movimento(STEPPER_LONG2, MSG_MOV_HOME, 0, MOV_RESET);
            timer_tx_master = 400;
            ScattatoTimeout[STEPPER_CROSS] = FALSE;
            ScattatoTimeout[STEPPER_LONG] = FALSE;
            ScattatoTimeout[STEPPER_LONG2] = FALSE;
            stato_master = MASTER_RESET_1;
        }
        break;

    case MASTER_RESET_1:      // reset di tutti gli slave
        if (timer_tx_master == 0)
        {
            timer_tx_master = 400;
            stato_master = MASTER_WAIT_RESET;
        }
        break;

    case MASTER_WAIT_RESET:      // attesa della fine del reset
        if (timer_tx_master == 0)
        {
            timer_tx_master = 100;
            flag = TRUE;

            if (Comando_Motore[STEPPER_CROSS] & COM_MOTORE_RESET)
                flag = FALSE;
            if (Comando_Motore[STEPPER_LONG] & COM_MOTORE_RESET)
                flag = FALSE;
            if (Comando_Motore[STEPPER_LONG2] & COM_MOTORE_RESET)
                flag = FALSE;

            if (ScattatoTimeout[STEPPER_CROSS] == TRUE)     // allarme fotocellula non raggiunta
            {
                allarmi_totale |= ALARM_RESET;
            }

            if (flag == TRUE)
            {
                for (i = 0; i < MAX_STEPPER; i++)
                {
                    passi_slave[i] = 0;
                    dff_slave[i] = 0;
                    stato_mov[i] = ST_MOV_RESET;
                }
                           // controllo dell'esistenza dei potenziometri Cross e Long
                if (ee_ConfigColl.ucHardwareType == HARDWARE_YES_POT)
                {
                    indice_taratura = NUM_TAR_POT - 1;
                    timer_tx_master = 300;
                    stato_master = MASTER_WAIT_MEMO;
                }
                else
                {
                    in_calibrazione = FALSE;
                    stato_master = MASTER_PREP_READY;
                }

                if (TestPassi)
                {
                    SetPassiIniziali();
                }

            }
        }
        break;

    case MASTER_WAIT_MEMO:      // calibrazione potenziometri
        if (timer_tx_master == 0)
        {
            ee_ConfigColl.uwPotCross[indice_taratura] = g_nFilteredAdc[POT_CROSS];        // memorizza valori
            ee_ConfigColl.uwPotLong[indice_taratura] = g_nFilteredAdc[POT_LONG];

            if (indice_taratura)        // controlla se deve fare un altro movimento
            {
                indice_taratura--;
                temp32 = (g_MaxSteps[STEPPER_CROSS] * (UINT32)indice_taratura) / (NUM_TAR_POT - 1);
                config_movimento(STEPPER_CROSS, MSG_MOV_ABS, temp32, MOV_FAST);            // muove prossima apertura
                temp32 = (g_MaxSteps[STEPPER_LONG] * (UINT32)indice_taratura) / (NUM_TAR_POT - 1);
                config_movimento(STEPPER_LONG, MSG_MOV_ABS, temp32, MOV_FAST);            // muove prossima apertura
                timer_tx_master = 400;
                stato_master = MASTER_WAIT_MOVIMENTO;
            }
            else
            {
                stato_master = MASTER_CHECK_VALORI;
            }
        }
        break;

    case MASTER_WAIT_MOVIMENTO:
        if (timer_tx_master == 0)
        {
            timer_tx_master = 100;
            flag = TRUE;

            if (Steppers[STEPPER_CROSS].StepperData.StepperBits.bMoving)
                flag = FALSE;
            if (Steppers[STEPPER_LONG].StepperData.StepperBits.bMoving)
                flag = FALSE;

            if (flag == TRUE)       // se finito aspetta la memorizzazione
            {
                timer_tx_master = 300;
                stato_master = MASTER_WAIT_MEMO;
            }
        }
        break;

    case MASTER_CHECK_VALORI:      // calibrazione potenziometri
        if (timer_tx_master == 0)
        {
            if ((ee_ConfigColl.uwPotCross[NUM_TAR_POT - 1] - ee_ConfigColl.uwPotCross[0]) < 200)
            {
                for (i = 0; i < NUM_TAR_POT; i++)
                {
                    ee_ConfigColl.uwPotCross[i] = (1023 * i) / (NUM_TAR_POT - 1);
                }
            }

            if ((ee_ConfigColl.uwPotLong[NUM_TAR_POT - 1] - ee_ConfigColl.uwPotLong[0]) < 200)
            {
                for (i = 0; i < NUM_TAR_POT; i++)
                {
                    ee_ConfigColl.uwPotLong[i] = (1023 * i) / (NUM_TAR_POT - 1);
                }
            }
//            g_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            timer_tx_master = 100;
            stato_master = MASTER_WAIT_LITTLE_BIT;
        }
        break;

    case MASTER_WAIT_LITTLE_BIT:      // attesa per nuovo reset
        if (timer_tx_master == 0)
        {
            config_movimento(STEPPER_CROSS, MSG_MOV_ABS, g_MaxSteps[STEPPER_CROSS], MOV_FAST);
            config_movimento(STEPPER_LONG, MSG_MOV_ABS, g_MaxSteps[STEPPER_LONG], MOV_FAST);
            timer_tx_master = 400;
            stato_master = MASTER_WAIT_OPEN_ALL_END;
        }
        break;

    case MASTER_WAIT_OPEN_ALL_END:      // attesa della fine del reset
        if (timer_tx_master == 0)
        {
            timer_tx_master = 100;
            flag = TRUE;

            if (Steppers[STEPPER_CROSS].StepperData.StepperBits.bMoving)
                flag = FALSE;
            if (Steppers[STEPPER_LONG].StepperData.StepperBits.bMoving)
                flag = FALSE;

            if (flag == TRUE)
            {
                for (i = 0; i < MAX_STEPPER; i++)
                {
                    passi_slave[i] = 0;
                    dff_slave[i] = 0;
                    stato_mov[i] = ST_MOV_RESET;
                }
                in_calibrazione = FALSE;
                stato_master = MASTER_PREP_READY;
            }
        }
        break;

    case MASTER_PREP_READY:
        CollInReset &= ~STATO_LAMELLE_RESET;
        stato_master = MASTER_READY;
//        g_uwActualDff = sys_check_dff();
//        dff_attuale = g_uwActualDff;
        break;


    case MASTER_READY:
        if (lamelle == TRUE)
        {
            // controlla se messaggi arrivati da system per collimatore
            if (check_com_coll() == TRUE) // se RESET salta all'inizializzazione
            {
                sys_control_init(FALSE);
                stato_master = MASTER_FIRST;
            }
            else
            {
                Carica_DFF_Formato();        // carica la DFF dall'ingresso corretto ed il corrispondente formato
                gestione_cross_long(STEPPER_CROSS);
                gestione_cross_long(STEPPER_LONG);
                gestione_cross_long(STEPPER_LONG2);
            }
        }
        break;

    case MASTER_MOVE:
        break;

    case MASTER_IDLE:
        break;

    case MASTER_ALL_PREP_STOP:        // ferma tutti i motori e li rilascia
        if (timer_tx_master == 0)
        {
            config_movimento(STEPPER_CROSS, MSG_MOV_STOP, 0, MOV_SLOW);
            config_movimento(STEPPER_LONG, MSG_MOV_STOP, 0, MOV_SLOW);
            config_movimento(STEPPER_LONG2, MSG_MOV_STOP, 0, MOV_SLOW);
            stato_master = MASTER_ALL_STOPPED;
            timer_tx_master = 200;
        }
        break;

    case MASTER_ALL_STOPPED:
        if (check_com_coll() == TRUE)   // se RESET salta all'inizializzazione
        {
            sys_control_init(FALSE);
            stato_master = MASTER_FIRST;
        }
        break;

    case MASTER_ERR:
        break;
    }

}




//******************************************************************
//  Carica la corretta DFF ed il Formato in condizione VERTICALE
//  viene chiamata anche in tomografia
//******************************************************************
void CollimatoreVerticale(UINT8 Tomografia)
{
    DffValida[RX_UNDER] = TRUE;
    switch (ee_ConfigColl.ucVertDff)        // configurazione DFF verticale
    {
    case VERT_DFF_CAN:
        CurrentDFF = IngressoDFF[DFFVAL_VERT_CAN];
        break;
    case VERT_DFF_SINGLE:
        if (ee_ConfigColl.ucInputDffPot == POT_SINGLE_STAND)    // potenziometro STATIVO
        {
            CurrentDFF = IngressoDFF[DFFVAL_VERT_POT_STATIVO];
        }
        else                                                    // potenziometro TAVOLO
        {
            CurrentDFF = IngressoDFF[DFFVAL_VERT_POT_TABLE];
        }
        break;
    case VERT_DFF_DIFF:
        if (IngressoDFF[DFFVAL_VERT_POT_STATIVO] > IngressoDFF[DFFVAL_VERT_POT_TABLE])
            CurrentDFF = IngressoDFF[DFFVAL_VERT_POT_STATIVO] - IngressoDFF[DFFVAL_VERT_POT_TABLE];
        else
            CurrentDFF = 0;
        break;
    case VERT_DFF_FIXED:
        CurrentDFF = ee_ConfigColl.uwFixedDff;
        break;
    }

    switch (ee_ConfigColl.ucVertRec)        // Receptor VERTICALE
    {
    case VERT_REC_CAN:
    case VERT_REC_ATS:
        if ((NewFormat[FRMT_VERT_CAN] == TRUE) ||
            ((ResetPosizione == TRUE) && (FormatoValido[RX_UNDER] == TRUE)))
        {
            NewFormat[FRMT_VERT_CAN] = FALSE;
            formato_mm[STEPPER_CROSS] = FormatoCross[FRMT_VERT_CAN];
            formato_mm[STEPPER_LONG] = FormatoLong[FRMT_VERT_CAN];
            formato_mm[STEPPER_LONG2] = FormatoLong[FRMT_VERT_CAN];
            LastReceivedFormat[STEPPER_CROSS] = formato_mm[STEPPER_CROSS];
            LastReceivedFormat[STEPPER_LONG] = formato_mm[STEPPER_LONG];
            LastReceivedFormat[STEPPER_LONG2] = formato_mm[STEPPER_LONG2];
            FormatoValido[RX_UNDER] = TRUE;
            AutoLimite = FALSE;
        }

        if ((NewIrisFormat[FRMT_VERT_CAN] == TRUE) ||
            ((ResetPosizione == TRUE) && (FormatoIrideValido == TRUE)))
        {
            NewIrisFormat[FRMT_VERT_CAN] = FALSE;
            g_uwIrisFormatFromCan = FormatoIride[FRMT_VERT_CAN];
            LastIrisFormat = g_uwIrisFormatFromCan;
            FormatoIrideValido = TRUE;
            muovi_iride = TRUE;
        }
        break;
    case VERT_REC_BUCKY:
        FormatoValido[RX_UNDER] = CheckNuovoBucky(RX_UNDER, FRMT_VERT_003);
        DisplayFTD = TRUE;
        if (g_bTableCassette[RX_UNDER])      // se presente somma la distanza appropriata alla DFF se DFF con POT
        {
            DisplayFTD = FALSE;
            switch (ee_ConfigColl.ucVertDff)   // calcola se mettere/togliere Distanza con Tavolo
            {
            case VERT_DFF_CAN:
            case VERT_DFF_FIXED:
                break;
            case VERT_DFF_SINGLE:
            case VERT_DFF_DIFF:
                if (CurrentDFF)        // somma la distanza film/tavolo solo se CurrentDFF != 0
                    CurrentDFF += ee_ConfigColl.ucDftVert;
                break;
            }
        }
        else
        {
            switch (ee_ConfigColl.ucVertDff)   // calcola se mettere/togliere Distanza con Tavolo
            {
            case VERT_DFF_CAN:
            case VERT_DFF_FIXED:
                if (CurrentDFF >= ee_ConfigColl.ucDftVert)        // toglie la distanza film/tavolo solo se CurrentDFF != 0
                    CurrentDFF -= ee_ConfigColl.ucDftVert;
                else
                    CurrentDFF = 0;
                break;
            case VERT_DFF_SINGLE:
            case VERT_DFF_DIFF:
                break;
            }
        }
        break;

    case VERT_REC_NO:
        FormatoValido[RX_UNDER] = FALSE;
        FormatoIrideValido = FALSE;
        AutoLimite = FALSE;
        break;
    case VERT_REC_FISSI:
        if ((NewFormat[FRMT_VERT_003] == TRUE) ||
            ((ResetPosizione == TRUE) && (FormatoValido[RX_UNDER] == TRUE)))
        {
            NewFormat[FRMT_VERT_003] = FALSE;
            formato_mm[STEPPER_CROSS] = FormatoCross[FRMT_VERT_003];
            formato_mm[STEPPER_LONG] = FormatoLong[FRMT_VERT_003];
            formato_mm[STEPPER_LONG2] = FormatoLong[FRMT_VERT_003];
            LastReceivedFormat[STEPPER_CROSS] = formato_mm[STEPPER_CROSS];
            LastReceivedFormat[STEPPER_LONG] = formato_mm[STEPPER_LONG];
            LastReceivedFormat[STEPPER_LONG2] = formato_mm[STEPPER_LONG2];
            AutoLimite = TRUE;
            FormatoValido[RX_UNDER] = TRUE;
        }
        if ((NewIrisFormat[FRMT_VERT_003] == TRUE) ||
            ((ResetPosizione == TRUE) && (FormatoIrideValido == TRUE)))
        {
            NewIrisFormat[FRMT_VERT_003] = FALSE;
            g_uwIrisFormatFromCan = FormatoIride[FRMT_VERT_003];
            LastIrisFormat = g_uwIrisFormatFromCan;
            FormatoIrideValido = TRUE;
            muovi_iride = TRUE;
        }
        break;
    }
}




//******************************************************************
//  Carica la corretta DFF ed il Formato in condizione LATERALE
//******************************************************************
void CollimatoreLaterale(UINT8 incl)
{
    UINT8 DffLatConf, RecLatConf;
    UINT8 DffLatCan, DffLat003;
    UINT8 FrmLatCan, FrmLat003;
    UINT16 DFTval, POTmisMin, POTmisMax;


    DffValida[incl] = TRUE;     // prepara le variabili
    if (incl == RX_LEFT)        // inclinato a SINISTRA
    {
        DffLatConf = ee_ConfigColl.ucLatDffSx;
        DffLatCan = DFFVAL_LAT_SX_CAN;
        DffLat003 = DFFVAL_LAT_SX_003;
        RecLatConf = ee_ConfigColl.ucLatRecSx;
        FrmLatCan = FRMT_LAT_SX_CAN;
        FrmLat003 = FRMT_LAT_SX_003;
        DFTval = ee_ConfigColl.ucDftSx;
        POTmisMin = ee_CalColl.DffSxMisMin;
        POTmisMax = ee_CalColl.DffSxMisMax;
    }
    else        // inclinato a DESTRA
    {
        DffLatConf = ee_ConfigColl.ucLatDffDx;
        DffLatCan = DFFVAL_LAT_DX_CAN;
        DffLat003 = DFFVAL_LAT_DX_003;
        RecLatConf = ee_ConfigColl.ucLatRecDx;
        FrmLatCan = FRMT_LAT_DX_CAN;
        FrmLat003 = FRMT_LAT_DX_003;
        DFTval = ee_ConfigColl.ucDftDx;
        POTmisMin = ee_CalColl.DffDxMisMin;
        POTmisMax = ee_CalColl.DffDxMisMax;
    }

    switch (DffLatConf)       // preleva la DFF
    {
    case LAT_DFF_CAN:
        CurrentDFF = IngressoDFF[DffLatCan];
        break;
    case LAT_DFF_DISC:
        CurrentDFF = IngressoDFF[DffLat003];
        if (NewDFF[DffLat003])
        {
            NewDFF[DffLat003] = FALSE;
            ResetPosizione = TRUE;
        }
        break;
    case LAT_DFF_POT:
        CurrentDFF = IngressoDFF[DffLat003];
        break;
    }

    switch (RecLatConf)       // controllo del formato
    {
    case LAT_REC_CAN:
        if ((NewFormat[FrmLatCan] == TRUE) ||
            ((ResetPosizione == TRUE) && (FormatoValido[incl] == TRUE)))
        {
            NewFormat[FrmLatCan] = FALSE;
            formato_mm[STEPPER_CROSS] = FormatoCross[FrmLatCan];
            formato_mm[STEPPER_LONG] = FormatoLong[FrmLatCan];
            formato_mm[STEPPER_LONG2] = FormatoLong[FrmLatCan];
            LastReceivedFormat[STEPPER_CROSS] = formato_mm[STEPPER_CROSS];
            LastReceivedFormat[STEPPER_LONG] = formato_mm[STEPPER_LONG];
            LastReceivedFormat[STEPPER_LONG2] = formato_mm[STEPPER_LONG2];
            FormatoValido[incl] = TRUE;
            AutoLimite = FALSE;
        }
        break;
    case LAT_REC_BUCKY:
        FormatoValido[incl] = CheckNuovoBucky(incl, FrmLat003);
        DisplayFTD = TRUE;
        if (g_bTableCassette[incl])      // se presente somma la distanza appropriata alla DFF se DFF con POT
        {
            DisplayFTD = FALSE;
            switch (DffLatConf)   // calcola se mettere/togliere Distanza con Tavolo
            {
            case LAT_DFF_CAN:
                break;
            case LAT_DFF_DISC:
                break;
            case LAT_DFF_POT:
                if (CurrentDFF)        // somma la distanza film/tavolo solo se CurrentDFF != 0
                    CurrentDFF += DFTval;
                break;
            }
        }
        else
        {
            switch (DffLatConf)   // calcola se mettere/togliere Distanza con Tavolo
            {
            case LAT_DFF_CAN:
            case LAT_DFF_DISC:
                if (CurrentDFF >= DFTval)        // toglie la distanza film/tavolo solo se CurrentDFF != 0
                    CurrentDFF -= DFTval;
                else
                    CurrentDFF = 0;
                break;
            case LAT_DFF_POT:
                break;
            }
        }
        break;
    case LAT_REC_NO:
        FormatoValido[incl] = FALSE;
        break;
    }

    switch (DffLatConf)       // considerazioni aggiuntive sulla DFF
    {
    case LAT_DFF_CAN:
        break;
    case LAT_DFF_DISC:
        break;
    case LAT_DFF_POT:
        if (CurrentDFF < POTmisMin)
            DffValida[incl] = FALSE;
        else if (CurrentDFF > POTmisMax)
            DffValida[incl] = FALSE;
        break;
    }
}


//*************************************************************
//  utilizza la corretta DFF a seconda della configurazione
//*************************************************************
void Carica_DFF_Formato(void)
{
    static UINT8 KeyReturnAuto = FALSE, StatusPrec = FALSE;
    UINT8 flag;

/*
    if (KEY_AUTO)       // controllo posizione chiave per reset
    {
        if (KeyReturnAuto == FALSE)
        {
            KeyReturnAuto = TRUE;
            if (StatusPrec == FALSE)     // se in automatico ricarica formato
            {
                ResetPosizione = TRUE;
            }
        }
        StatusPrec = ManualStatus[STEPPER_CROSS];
    }
    else
    {
        KeyReturnAuto = FALSE;
    }
*/

    if (ManualStatus[STEPPER_CROSS] == FALSE)       // se in AUTO
    {
        if (KeyReturnAuto == FALSE)
        {
            KeyReturnAuto = TRUE;
            ResetPosizione = TRUE;
        }
    }
    else
    {
        KeyReturnAuto = FALSE;
    }



    DisplayFTD = FALSE;
    if (g_ucOrienteer == RX_UNDER)                  //  ********  VERTICALE  ********
    {
        CollimatoreVerticale(FALSE);
        if (CurrentDFF == 0)
        {
            DffValida[RX_UNDER] = FALSE;
        }
        else
        {
            dff_attuale = CurrentDFF;
            DisplayDFF = dff_attuale;
        }
    }
    else if ((g_ucOrienteer == RX_LEFT) ||                //  ********  SINISTRA  ********
            (g_ucOrienteer == RX_RIGHT))                  //  ********  DESTRA  ********
    {
        CollimatoreLaterale(g_ucOrienteer);
        if (CurrentDFF == 0)
        {
            DffValida[g_ucOrienteer] = FALSE;
            LateralDFF = 0;
            DisplayDFF = 0;
        }
        else
        {
            dff_attuale = CurrentDFF;
            DisplayDFF = dff_attuale;
            LateralDFF = dff_attuale;
        }
    }
    else if (g_ucOrienteer == RX_TOMO)               //  ********  TOMOGRAFIA  *********
    {
        __asm("nop");
        DisplayDFF = TomographyDFF;
    }
    else                                       //  ********  RX_ERROR  ********
    {
        DisplayDFF = 0;
        fluoroscopia_on = FALSE;        // TODO: sara' giusto?
        LateralDFF = 0;
    }


        //  ****  ADEGUAMENTO APERTURE A DISPLAY  ****

    flag = TRUE;    // controllo per l'aggiornamento delle aperture
    if (ee_ConfigColl.ucCanProtocol != CAN_SEDECAL)
    {
        if (KEY_MANUAL)     // con chiave in manuale non aggiorna aperture
        {
//        DisplayDFF = 0;    // e visualizza DFF = 0
            flag = FALSE;
        }
    }
    if (ee_ConfigColl.ucManualColl == MANUAL_COLL_DIS)
    {
        if (ManualStatus[STEPPER_CROSS] == TRUE)
        {
            flag = FALSE;       // disabilita collimazione se in manuale e configurato
        }
    }
    if (g_ucOrienteer == RX_TOMO)
    {
        flag = FALSE;       // disabilita adeguamento aperture a display se in Tomografia
    }

    if (flag == TRUE)
    {
        if (g_uwActualDff != dff_attuale)       // dff per ricalcolo delle aperture su display
        {
            g_uwActualDff = dff_attuale;
            muovi_iride = TRUE;
        }
    }

    ResetPosizione = FALSE;

}




//**********************************************************
//  verifica presenza nuovo bucky
//  ritorna con TRUE=formato valido
//**********************************************************
UINT8 CheckNuovoBucky(UINT8 incl, UINT8 formato)
{
    UINT8 ret;

    ret = FALSE;

    if (g_bTableCassette[incl])      // se presente preleva il formato
    {
        if (FormatoCross[formato] && FormatoLong[formato] && (CassettoStabile[incl] == TRUE))
        {
            if ((NewFormat[formato] == TRUE) || (ResetPosizione == TRUE))
            {
                NewFormat[formato] = FALSE;
                formato_mm[STEPPER_CROSS] = FormatoCross[formato];
                formato_mm[STEPPER_LONG] = FormatoLong[formato];
                formato_mm[STEPPER_LONG2] = FormatoLong[formato];
                LastReceivedFormat[STEPPER_CROSS] = formato_mm[STEPPER_CROSS];
                LastReceivedFormat[STEPPER_LONG] = formato_mm[STEPPER_LONG];
                LastReceivedFormat[STEPPER_LONG2] = formato_mm[STEPPER_LONG];
                AutoLimite = TRUE;
            }
            ret = TRUE;
        }
    }

    return ret;
}



//********************************************************
//  gestione movimentazione lamelle
//********************************************************
#define MANUAL_OPEN     0x01
#define MANUAL_CLOSE    0x02
void gestione_cross_long(UINT8 slave)
{
    UINT8 flag;
    UINT8 com_man;
    UINT32 passi;
    SINT16 encoder;
    SINT32 stemp32;
    SINT32 mm, mm_max;

    if (Steppers[slave].StepperData.StepperBits.bMoving)       // Gestione stato del LED
    {
        LedStatus[slave] = RED;
        TimerLed[slave] = 100;
    }
    else
    {
        if (TimerLed[slave] == 0)
        {
            if (ManualStatus[slave] == FALSE)   // qui in modalita' AUTOMATICA
            {
                LedStatus[slave] = GREEN;
            }
            else
            {
                LedStatus[slave] = YELLOW;
            }
        }
    }


    com_man = 0;
    switch (slave)        // lettura movimento manuale da canbus
    {
    case STEPPER_CROSS:
        if (g_ucOpenCloseFromCan & OPEN_CROSS)
        {
            com_man |= MANUAL_OPEN;
        }
        if (g_ucOpenCloseFromCan & CLOSE_CROSS)
        {
            com_man |= MANUAL_CLOSE;
        }
        break;

    case STEPPER_LONG:
        if (g_ucOpenCloseFromCan & OPEN_LONG)
        {
            com_man |= MANUAL_OPEN;
        }
        if (g_ucOpenCloseFromCan & CLOSE_LONG)
        {
            com_man |= MANUAL_CLOSE;
        }
        break;

    case STEPPER_LONG2:
        if (g_ucOpenCloseFromCan & OPEN_LONG)
        {
            com_man |= MANUAL_OPEN;
        }
        if (g_ucOpenCloseFromCan & CLOSE_LONG)
        {
            com_man |= MANUAL_CLOSE;
        }
        break;
    }

                        //  MACCHINA A STATI FINITI LAMELLA
    flag = FALSE;

    switch (stato_mov[slave])
    {
    case ST_MOV_RESET:
        stato_mov[slave] = ST_MOV_CHECK_CHIAVE;
        break;

    case ST_MOV_CHECK_CHIAVE:              // controllo se chiave girata in AUTO
        stato_mov[slave] = ST_MOV_CHECK_MANUALE;
        break;

    case ST_MOV_CHECK_MANUALE:              // qui motori non in modalita' manuale
        if (com_man)
        {
            MovimentoManuale[slave] = FALSE;
 //           calcola_speed_manuale(slave, FALSE);
            if (com_man & MANUAL_CLOSE)     // CHIUSURA manuale via canbus
            {
                mask_back[slave] = MANUAL_CLOSE;
                if (ManualStatus[slave] == FALSE)   // qui in modalita' AUTOMATICA
                {
                    calc_size2steps(slave, dff_attuale, formato_min[slave], &passi, TRUE);       // calcola passi per minimo
                    if (i_MotPosizione[slave] > passi)      // si muove se la lamella e' superiore al formato minimo
                    {
                        MovimentoManuale[slave] = TRUE;
                        config_movimento(slave, MSG_MOV_ABS, passi, MOV_MM);        // muove a velocita' costante
                        if (slave == STEPPER_CROSS)
                            alarm_dff &= ~ALARM_DFF_CROSS;
                        if (slave == STEPPER_LONG)
                            alarm_dff &= ~ALARM_DFF_LONG;
                    }
                }
                else            // qui in modalita' MANUALE - velocita' del motore fissa
                {
                    passi = 0;
                    if (i_MotPosizione[slave] > passi)      // si muove se la lamella e' superiore al formato minimo
                    {
                        MovimentoManuale[slave] = TRUE;
                        config_movimento(slave, MSG_MOV_ABS, passi, MOV_SLOW);
                        if (slave == STEPPER_CROSS)
                            alarm_dff &= ~ALARM_DFF_CROSS;
                        if (slave == STEPPER_LONG)
                            alarm_dff &= ~ALARM_DFF_LONG;
                    }
                }
            }
            else        // APERTURA manuale via canbus
            {
                mask_back[slave] = MANUAL_OPEN;    // comando di apertura
                if (ManualStatus[slave] == FALSE)   // qui in modalita' AUTOMATICA
                {
                    if (AutoLimite == TRUE)
                        formato_max[slave] = LastReceivedFormat[slave];
                    else
                        formato_max[slave] = CheckMaxShutter(slave, dff_attuale, 0);
                    calc_size2steps(slave, dff_attuale, formato_max[slave], &passi, TRUE);
                    if (i_MotPosizione[slave] < passi)      // si muove in apertura se la lamella e' sotto al massimo
                    {
                        config_movimento(slave, MSG_MOV_ABS, passi, MOV_MM);
                        MovimentoManuale[slave] = TRUE;
                    }
                }
                else            // qui in modalita' MANUALE
                {       // manda fino al limite massimo meccanico
                    if (i_MotPosizione[slave] < passi)      // si muove in apertura se la lamella e' sotto al massimo
                    {
                        config_movimento(slave, MSG_MOV_ABS, g_MaxSteps[slave], MOV_SLOW);
                        MovimentoManuale[slave] = TRUE;
                    }
                }
            }
            force_tx_7F0[slave] = TRUE;
            stato_mov[slave] = ST_MOV_WAIT_MANUALE;        // attende fine del movimento manuale
        }
        else
        {
            stato_mov[slave] = ST_MOV_CHECK_ENCODER;        // controlla encoder
        }
        break;

    case ST_MOV_WAIT_MANUALE:
        if (mask_back[slave] & com_man)
        {
            __asm ("nop");
        }
        else
        {
            config_movimento(slave, MSG_MOV_BRAKE, 0, MOV_SLOW);
 //           restore_speed(slave);
            timer_slave[slave] = 30;
            stato_mov[slave] = ST_MOV_WAIT_STOP;
        }
        break;

    case ST_MOV_WAIT_STOP:
        if (timer_slave[slave] == 0)
        {
            if (Steppers[slave].StepperData.StepperBits.bMoving == 0)
            {
                force_tx_7F0[slave] = TRUE;
                if (MovimentoManuale[slave] == TRUE)
                {
                    formato_mm[slave] = calc_steps2size(dff_attuale, slave, i_MotPosizione[slave]);
                    formato_old[slave] = formato_mm[slave];
                }
                passi_slave[slave] = i_MotPosizione[slave];
                stato_mov[slave] = ST_MOV_CHECK_ENCODER;
            }
        }
        break;

    case ST_MOV_CHECK_ENCODER:
        if (EncoderValue[slave] == 0)       // se nessun movimento da encoder salta via
        {
            stato_mov[slave] = ST_MOV_CHECK_FORMATO;        // controlla se arrivato un cambio di formato
        }
        else
        {
            MovimentoManuale[slave] = FALSE;
            stato_mov[slave] = ST_MOV_ENCODER_IN_MOVIMENTO;
        }
        break;

    case ST_MOV_ENCODER_IN_MOVIMENTO:
        if (EncoderValue[slave] == 0)       // se nessun movimento da encoder controlla il movimento
        {
            if (timer_slave[slave] == 0)
            {
                if (StopFromEncoder[slave] == TRUE)     // se primo passaggio allora stoppa il motore
                {
                    StopFromEncoder[slave] = FALSE;
                    config_movimento(slave, MSG_MOV_BRAKE, 0, MOV_ENC);
                    timer_slave[slave] = 10;
                }
                else if (Steppers[slave].StepperData.StepperBits.bMoving == 0)  // a motore fermo rileva la posizione
                {
                    force_tx_7F0[slave] = TRUE;
                    if (MovimentoManuale[slave] == TRUE)
                    {
                        formato_mm[slave] = calc_steps2size(dff_attuale, slave, i_MotPosizione[slave]);
                        formato_old[slave] = formato_mm[slave];
                    }
                    passi_slave[slave] = i_MotPosizione[slave];
                    stato_mov[slave] = ST_MOV_CHECK_ENCODER;        // controlla se arrivato un cambio di formato
                }
            }
        }
        else
        {
            ShutterFollow[slave] = FALSE;
            StopFromEncoder[slave] = TRUE;
            encoder = EncoderValue[slave];      // legge gli impulsi fatti
            EncoderValue[slave] = 0;

            if (ee_ConfigColl.ucAutoLight)      // accende la luce se configurata
                g_bRequestLightMan = true;

            if (encoder < 0)        // movimento verso la CHIUSURA LAMELLA
            {
                EncoderSpeed[slave] = -encoder;
                if (dff_attuale == 0)  // qui dff non presente, come se fosse a 100 in MANUALE
                {
                    passi = 0;
                    config_movimento(slave, MSG_MOV_ABS, passi, MOV_ENC);
                }
                else
                {                       // qui dff presente
                    if (ManualStatus[slave] == FALSE)   // qui in modalita' AUTOMATICA
                    {
                        calc_size2steps(slave, dff_attuale, formato_min[slave], &passi, TRUE);       // calcola passi per minimo
                    }
                    else
                    {
                        passi = 0;
                    }
                    if (i_MotPosizione[slave] > passi)      // si muove se la lamella e' superiore al formato minimo
                    {
                        config_movimento(slave, MSG_MOV_ABS, passi, MOV_ENC);
                        MovimentoManuale[slave] = TRUE;
                        if (slave == STEPPER_CROSS)
                            alarm_dff &= ~ALARM_DFF_CROSS;
                        if (slave == STEPPER_LONG)
                            alarm_dff &= ~ALARM_DFF_LONG;
                    }
                }
                force_tx_7F0[slave] = TRUE;
                timer_slave[slave] = 60;
            }
            else                // movimento verso l'APERTURA LAMELLA
            {
                EncoderSpeed[slave] = encoder;
                if ((dff_attuale == 0) || // qui dff non presente, come se fosse a 100 in manuale e puo' andare al limite
                    (ManualStatus[slave] == TRUE))  // anche se in manuale
                {
                    passi = g_MaxSteps[slave];
                }
                else
                {
                    if (AutoLimite == TRUE)
                        formato_max[slave] = LastReceivedFormat[slave];
                    else
                        formato_max[slave] = CheckMaxShutter(slave, dff_attuale, 0);
                    calc_size2steps(slave, dff_attuale, formato_max[slave], &passi, TRUE);
                }
                if (i_MotPosizione[slave] < passi)      // si muove in apertura se la lamella e' sotto al massimo
                {
                    config_movimento(slave, MSG_MOV_ABS, passi, MOV_ENC);
                    MovimentoManuale[slave] = TRUE;
                }
                force_tx_7F0[slave] = TRUE;
                timer_slave[slave] = 60;
            }

        }
        break;

    case ST_MOV_CHECK_FORMATO:

        flag = TRUE;    // abilita la collimazione
        if (ee_ConfigColl.ucCanProtocol != CAN_SEDECAL)
        {
            if (KEY_MANUAL)     // con chiave in manuale non deve collimare
            {
                flag = FALSE;
            }
        }
        if (ee_ConfigColl.ucManualColl == MANUAL_COLL_DIS)
        {
            if (ManualStatus[slave] == TRUE)
            {
                flag = FALSE;       // disabilita controllo formati
            }
        }
        if (g_ucOrienteer == RX_TOMO)
        {
            flag = FALSE;       // disabilita controllo formati se in Tomografia
        }

        if (flag == TRUE)       // se controllo abilitato
        {
            flag = FALSE;
            if (dff_attuale != dff_slave[slave])        // nuova DFF
            {
                dff_slave[slave] = dff_attuale;
                flag = TRUE;
            }

            if (formato_mm[slave] != formato_old[slave])    // nuovo formato
            {
                formato_old[slave] = formato_mm[slave];
                flag = TRUE;
            }
        }


        if (flag == TRUE)       // qui bisogna azionare il motore
        {
            calc_size2steps(slave, dff_attuale, formato_mm[slave], &passi, FALSE);
            config_movimento(slave, MSG_MOV_ABS, passi, MOV_FAST);
            force_tx_7F0[slave] = TRUE;
            timer_slave[slave] = 50;
        }
        else
        {
            if (timer_slave[slave] == 0)     // controlla lo stato del motore solo se motore fermo
            {
                if (Steppers[slave].StepperData.StepperBits.bMoving == 0)
                {
                    stato_mov[slave] = ST_MOV_CHECK_CHIAVE;
                }
            }
        }
        break;
    }

}




//************************************************************
//  se flag==0 deve limitare campo in fluoroscopia
//************************************************************
UINT32 CheckMaxShutter(UINT8 slave, UINT16 uwActualDff, UINT8 flag)
{
    UINT16 uwTempMm;


    if (slave == STEPPER_CROSS)
    {
        switch (g_ucOrienteer)
        {
        case RX_TOMO:
        case RX_UNDER:
            break;
        case RX_LEFT:
        if ((ee_ConfigColl.ucLatRecSx == LAT_REC_BUCKY) && (RemoteStatus.data.bLeftPan))
            return PAN_CROSS;
            break;
        case RX_RIGHT:
        if ((ee_ConfigColl.ucLatRecDx == LAT_REC_BUCKY) && (RemoteStatus.data.bRightPan))
            return PAN_CROSS;
            break;
        }

        LinCrossMm(uwActualDff, g_MaxSteps[STEPPER_CROSS], &uwTempMm);

        if (flag == 0)
        {
            if (fluoroscopia_on == TRUE)
            {
                if (uwTempMm <= Iris_MaxCross)
                    return uwTempMm;
                else
                    return Iris_MaxCross;
            }
            else
            {
                if (ee_OffsetColl.nMaxCross >= uwTempMm)
                    return uwTempMm;
                else
                    return ee_OffsetColl.nMaxCross;
            }
        }
        else
        {
            return uwTempMm;
        }
    }
    else
    {
        switch (g_ucOrienteer)
        {
        case RX_TOMO:
        case RX_UNDER:
            break;
        case RX_LEFT:
        if ((ee_ConfigColl.ucLatRecSx == LAT_REC_BUCKY) && (RemoteStatus.data.bLeftPan))
            return PAN_LONG;
            break;
        case RX_RIGHT:
        if ((ee_ConfigColl.ucLatRecDx == LAT_REC_BUCKY) && (RemoteStatus.data.bRightPan))
            return PAN_LONG;
            break;
        }

        LinLongMm(uwActualDff, g_MaxSteps[STEPPER_LONG], &uwTempMm);
        if (flag == 0)
        {
            if (fluoroscopia_on == TRUE)
            {
                if (uwTempMm <= Iris_MaxLong)
                    return uwTempMm;
                else
                    return Iris_MaxLong;
            }
            else
            {
                if (ee_OffsetColl.nMaxLong>=uwTempMm)
                    return uwTempMm;
                else
                    return ee_OffsetColl.nMaxLong;
            }
        }
        else
        {
            return uwTempMm;
        }
    }

}





//*****************************************************
//  controlla la movimentazione del motore partendo
//  dal formato arrivato
// se no_err == TRUE non genera errore se il numero
// dei passi e' fuori meccanicamente
//*****************************************************
UINT8 calc_size2steps(UINT8 slave, UINT32 dff, SINT32 formato, UINT32 *steps, UINT8 no_err)
{
    UINT8   ret, NoAlarm, EnableAlarm;
    UINT16  passicalc;


    // calcola la proiezione del formato a 100cm (dff in calibrazione)
    // ed in base alla distanza SFD (fuoco-collimatore)

    ret = TRUE;

/*
    if ((dff < DFF_MIN) || (dff > DFF_MAX))
    {
        if (dff)
        {
            if (slave == STEPPER_CROSS)
                alarm_dff |= ALARM_DFF_CROSS;
            if (slave == STEPPER_LONG)
                alarm_dff |= ALARM_DFF_LONG;
        //Dff Error
            *steps = 0;
        }
        return FALSE;
    }
*/
    if (no_err == FALSE)
    {
        if (slave == STEPPER_CROSS)
            alarm_dff &= ~ALARM_DFF_CROSS;
        if (slave == STEPPER_LONG)
            alarm_dff &= ~ALARM_DFF_LONG;
    }

    if (slave == STEPPER_CROSS)
    {
        NoAlarm = LinCrossOverDff(dff, (UINT16)formato, &passicalc);
        EnableAlarm = ALARM_DFF_CROSS;
    }
    else if (slave == STEPPER_LONG)
    {
        NoAlarm = LinLongOverDff(dff, (UINT16)formato, &passicalc);
        EnableAlarm = ALARM_DFF_LONG;
    }
    else if (slave == STEPPER_LONG2)
    {
        NoAlarm = LinLongOverDff(dff, (UINT16)formato, &passicalc);
        EnableAlarm = ALARM_DFF_LONG;
    }

    if (no_err == FALSE)
    {
        if (NoAlarm == FALSE)       // se e' in allarme controlla l'inclinazione
        {
            switch (g_ucOrienteer)
            {
            case RX_TOMO:
                break;
            case RX_UNDER:
                if ((dff >= ee_ConfigColl.uwMinDff) && (dff <= ee_ConfigColl.uwMaxDff))
                {
                    alarm_dff |= EnableAlarm;
                }
                break;
            case RX_ERROR:
                break;
            case RX_LEFT:
                switch (ee_ConfigColl.ucLatDffSx)       // considerazioni aggiuntive sulla DFF
                {
                case LAT_DFF_CAN:
                    if (dff_attuale)
                        alarm_dff |= EnableAlarm;
                    break;
                case LAT_DFF_DISC:
                    break;
                case LAT_DFF_POT:
                    if (dff_attuale)
                    {
                        if ((dff >= ee_CalColl.DffSxMisMin) && (dff <= ee_CalColl.DffSxMisMax))
                        {
                            alarm_dff |= EnableAlarm;
                        }
                    }
                    break;
                }
                break;
            case RX_RIGHT:
                switch (ee_ConfigColl.ucLatDffDx)       // considerazioni aggiuntive sulla DFF
                {
                case LAT_DFF_CAN:
                    if (dff_attuale)
                        alarm_dff |= EnableAlarm;
                    break;
                case LAT_DFF_DISC:
                    break;
                case LAT_DFF_POT:
                    if (dff_attuale)
                    {
                        if ((dff >= ee_CalColl.DffDxMisMin) && (dff <= ee_CalColl.DffDxMisMax))
                        {
                            alarm_dff |= EnableAlarm;
                        }
                    }
                    break;
                }
                break;
            }
        }
    }

    *steps = (UINT32)passicalc;
    return ret;
}








//*****************************************************************
//  trasforma da passi a mm secondo la taratura
//*****************************************************************
SINT32 calc_steps2size(UINT16 dff, UINT8 slave, SINT32 steps)
{
    UINT16 mm;


    if (slave == STEPPER_CROSS)
        LinCrossMm(dff, steps, &mm);
    else if (slave == STEPPER_LONG)
        LinLongMm(dff, steps, &mm);
    else if (slave == STEPPER_LONG2)
        LinLongMm(dff, steps, &mm);

    return (SINT32)mm;

}











//********************************************************
//  controlla messaggi arrivati
//********************************************************
UINT8 check_com_coll(void)
{
    UINT8 exit;
    UINT8 i, motore;
    UINT32 temp32;
    UINT16 temp16;

    exit = FALSE;
                        // controlla se qualche messaggio arrivato
    if (ixMI != ixMF)
    {                           // copia in buffer temporaneo
        rx_temp.id = rx_dati_M[ixMF].id;
        rx_temp.dlc = rx_dati_M[ixMF].dlc;

        for (i = 0; i < 8; i++)
            rx_temp.data[i] = rx_dati_M[ixMF].data.data[i];

        if (++ixMF >= NUM_BUFF_RX_M)
            ixMF = 0;

        switch (rx_temp.id & 0x000F)        // comandi con base .ID_COLL collimatore
        {
        case 0:
            break;
        default:
            break;
        }
    }

    return exit;
}







//**************************************************
//  calcola la velocita' di movimento
//  per le lamelle o iride e' lineare mm/sec
//**************************************************
UINT32 calcola_speed_manuale(UINT8 slave)
{
    UINT32 passi;
    UINT32 size;
    UINT32 temp32;

            // passi totali da reset al massimo formato
    passi = g_RefSteps[slave];
    size = g_RefSize[slave];

//    size /= 10;     // da 1/10 mm a mm

    temp32 = passi * (UINT32)g_ManualSpeed[slave];
    temp32 /= size;
    if (dff_attuale)
    {
        temp32 *= (UINT32)DFF_DEF;
        temp32 /= dff_attuale;
    }

    if (temp32 < FREQ_MOT_MIN)
        temp32 = FREQ_MOT_MIN;
    else if (temp32 > FREQ_MOT_MAX)
        temp32 = FREQ_MOT_MAX;

    return temp32;

}



