//--------------------------------------------------------------
// PRIMA STESURA
// Riva ing. Franco M.
// Besana Brianza (MI)
//
// REVISIONI SUCCESSIVE
// Giovanni Corvini
// Melegnano (MI)
//--------------------------------------------------------------
//
// PROJECT:	  RSR008 - Low Cost Collimator
// FILE:      sys_test.h
// REVISIONS: 0.00 - 22/09/2008 - First Definition
//
//--------------------------------------------------------------

#ifdef _SYS_TEST_
	volatile uword g_uwTestTimer=0;
	bool g_bStepperTestEnd=false;
#else
	extern volatile uword g_uwTestTimer;
	extern bool g_bStepperTestEnd;
#endif

void sys_board_selftest(void);
void sys_stepper_selftest(void);

