/*
 * include.h
 *
 *  Created on: 23 mag 2017
 *      Author: daniele
 */

#ifndef INCLUDE_H_
#define INCLUDE_H_

//#include "r_can_api.h"
#include "stdint.h"
#include "stdbool.h"
//#include "types.h"
#include "main_thread.h"

#include "global.h"
#include "custom.h"
#include "pin.h"
#include "irq.h"
#include "can.h"
#include "control.h"
#include "driver.h"
#include "mainApplication.h"
#include "high_level.h"
#include "can_main.h"
#include "lcd_driver.h"
#include "serial.h"
#include "config.h"
#include "calibration.h"
#include "display.h"
#include "test.h"

#include "default.h"

#endif /* INCLUDE_H_ */
