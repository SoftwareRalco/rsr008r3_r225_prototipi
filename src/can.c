

#define CAN_H

#include "include.h"
#include "main_thread.h"

#include "string.h"
#include "tx_api.h"

#define MAILBOX_TRANSMIT        0

extern can_mailbox_t g_can0_mailbox[];
extern can_bit_timing_cfg_t g_can0_bit_timing_cfg;
extern uint32_t g_can0_mailbox_mask[];



//**************************************************
//  irq da canbus
//**************************************************
void gestione_irq_canbus(can_callback_args_t * p_args)
{
    ssp_err_t err;
    CAN_STD_DATA_DEF *ptr;
    volatile can_frame_t receiveFrame;

    if(p_args->channel != 0) {
       // while(1);
        return;
    }

    switch(p_args->event)
    {
    case CAN_EVENT_RX_COMPLETE:
        err = g_can0.p_api->read (g_can0.p_ctrl, p_args->mailbox, &receiveFrame);
        if(err != SSP_SUCCESS)
        {
//          while(1)
//          {
//              tx_thread_sleep(10);
//          }
            return;
        }

        if (receiveFrame.type == CAN_FRAME_TYPE_REMOTE)
        {
            ptr = &rx_dati_R[ixRI];
            if (++ixRI >= NUM_BUFF_RX_R)        // aggiorna puntatore fifo
                ixRI = 0;
            if (ixRI == ixRF)        // se raggiunge il piu' vecchio messaggio lo cancella
            {
                if (++ixRF >= NUM_BUFF_RX_R)
                    ixRF = 0;
            }
        }
        else
        {
            if ((g_ucWorkModality == CONFIGURATION) && ((receiveFrame.id & 0xFFF0) == ID_CONFIGURAZIONE))
            {
                ptr = &rx_dati_C[ixCI];
                if (++ixCI >= NUM_BUFF_RX_C)        // aggiorna puntatore fifo
                    ixCI = 0;
                if (ixCI == ixCF)        // se raggiunge il piu' vecchio messaggio lo cancella
                {
                    if (++ixCF >= NUM_BUFF_RX_C)
                        ixCF = 0;
                }
            }
            else
            {
                ptr = &rx_dati_0[ix0I];
                if (++ix0I >= NUM_BUFF_RX_O)        // aggiorna puntatore fifo
                    ix0I = 0;
                if (ix0I == ix0F)        // se raggiunge il piu' vecchio messaggio lo cancella
                {
                    //                Can0_overrun = TRUE;
                    if (++ix0F >= NUM_BUFF_RX_O)
                        ix0F = 0;
                }
            }
        }
        ptr->dlc = receiveFrame.data_length_code;
        ptr->id = receiveFrame.id;
        ptr->data.data[0] = receiveFrame.data[0];
        ptr->data.data[1] = receiveFrame.data[1];
        ptr->data.data[2] = receiveFrame.data[2];
        ptr->data.data[3] = receiveFrame.data[3];
        ptr->data.data[4] = receiveFrame.data[4];
        ptr->data.data[5] = receiveFrame.data[5];
        ptr->data.data[6] = receiveFrame.data[6];
        ptr->data.data[7] = receiveFrame.data[7];

        tx_event_flags_set(&g_can_flags, FLAG_CAN_EVENT_RX_COMPLETE, TX_OR);

        timer_blinking[S_LED_RX_CAN] = DEF_TIME_LED_CAN_ON;     // accende led ricezione can
        Stato_Led[S_LED_RX_CAN] = LED_TIME_ON;
        break;

    case CAN_EVENT_TX_COMPLETE:
        tx_event_flags_set(&g_can_flags,FLAG_CAN_EVENT_TX_COMPLETE, TX_OR);
        break;

    case CAN_EVENT_ERR_BUS_OFF:
        tx_event_flags_set(&g_can_flags,FLAG_CAN_EVENT_ERR_BUS_OFF, TX_OR);
        break;

    case CAN_EVENT_BUS_RECOVERY:
        tx_event_flags_set(&g_can_flags,FLAG_CAN_EVENT_BUS_RECOVERY, TX_OR);
        break;

    case CAN_EVENT_ERR_PASSIVE:
        tx_event_flags_set(&g_can_flags,FLAG_CAN_EVENT_ERR_PASSIVE, TX_OR);
        break;

    case CAN_EVENT_MAILBOX_OVERWRITE_OVERRUN:
        tx_event_flags_set(&g_can_flags,FLAG_CAN_EVENT_MAILBOX_OVERWRITE_OVERRUN, TX_OR);
        break;

    default:
        tx_event_flags_set(&g_can_flags,FLAG_CAN_UNEXPECTED_EVENT, TX_OR);
        break;
    };

}










//******************************************
//  crea gli slot del CanBus in ricezione
//******************************************
void CAN_crea_slot(UINT8 tipo)
{
    UINT8 i;

    if (tipo == COLLIMATORWORK)
    {
        g_can0_mailbox_mask[0] = 0x1FFFFFF0;

        g_can0_mailbox[0].mailbox_id = 0;
        g_can0_mailbox[0].frame_type = CAN_FRAME_TYPE_DATA;
        g_can0_mailbox[0].mailbox_type = CAN_MAILBOX_TRANSMIT;

        g_can0_mailbox[1].mailbox_id = ee_CanColl.unIdOtherCommands;
        g_can0_mailbox[1].frame_type = CAN_FRAME_TYPE_DATA;
        g_can0_mailbox[1].mailbox_type = CAN_MAILBOX_RECEIVE;

        if (ee_ConfigColl.ucIrisPresent == IRIDE_YES)      // ricezione IRIDE
        {
            g_can0_mailbox[2].mailbox_id = ee_CanColl.unIdIride;
            g_can0_mailbox[2].frame_type = CAN_FRAME_TYPE_DATA;
            g_can0_mailbox[2].mailbox_type = CAN_MAILBOX_RECEIVE;
        }

        g_can0_mailbox_mask[1] = 0x1FFFFFFF;

        g_can0_mailbox[4].mailbox_id = ee_CanColl.unIdCommand;
        g_can0_mailbox[4].frame_type = CAN_FRAME_TYPE_DATA;
        g_can0_mailbox[4].mailbox_type = CAN_MAILBOX_RECEIVE;

        g_can0_mailbox[5].mailbox_id = ee_CanColl.unIdOtherCommands + CAN_CONFR_COLLIMATOR;
        g_can0_mailbox[5].frame_type = CAN_FRAME_TYPE_REMOTE;
        g_can0_mailbox[5].mailbox_type = CAN_MAILBOX_RECEIVE;

    }
    else
    {
        g_can0_mailbox_mask[0] = 0x1FFFFFF0;

        g_can0_mailbox[0].mailbox_id = 0;
        g_can0_mailbox[0].frame_type = CAN_FRAME_TYPE_DATA;
        g_can0_mailbox[0].mailbox_type = CAN_MAILBOX_TRANSMIT;

        g_can0_mailbox[1].mailbox_id = ee_CanAddressConfig[STEPPER_A].uwBoardAddress;
        g_can0_mailbox[1].frame_type = CAN_FRAME_TYPE_DATA;
        g_can0_mailbox[1].mailbox_type = CAN_MAILBOX_RECEIVE;

        g_can0_mailbox[2].mailbox_id = ee_CanAddressConfig[STEPPER_B].uwBoardAddress;
        g_can0_mailbox[2].frame_type = CAN_FRAME_TYPE_DATA;
        g_can0_mailbox[2].mailbox_type = CAN_MAILBOX_RECEIVE;

        g_can0_mailbox[3].mailbox_id = ee_CanAddressConfig[STEPPER_C].uwBoardAddress;
        g_can0_mailbox[3].frame_type = CAN_FRAME_TYPE_DATA;
        g_can0_mailbox[3].mailbox_type = CAN_MAILBOX_RECEIVE;

        g_can0_mailbox_mask[1] = 0x1FFFFFF0;

        g_can0_mailbox[4].mailbox_id = ID_CONFIGURAZIONE;
        g_can0_mailbox[4].frame_type = CAN_FRAME_TYPE_DATA;
        g_can0_mailbox[4].mailbox_type = CAN_MAILBOX_RECEIVE;

        g_can0_mailbox[5].mailbox_id = ee_CanAddressConfig[STEPPER_A].uwBoardAddress;
        g_can0_mailbox[5].frame_type = CAN_FRAME_TYPE_REMOTE;
        g_can0_mailbox[5].mailbox_type = CAN_MAILBOX_RECEIVE;

        g_can0_mailbox[6].mailbox_id = ee_CanColl.unIdIride;
        g_can0_mailbox[6].frame_type = CAN_FRAME_TYPE_DATA;
        g_can0_mailbox[6].mailbox_type = CAN_MAILBOX_RECEIVE;

        g_can0_mailbox[7].mailbox_id = ee_CanColl.unIdRot;
        g_can0_mailbox[7].frame_type = CAN_FRAME_TYPE_DATA;
        g_can0_mailbox[7].mailbox_type = CAN_MAILBOX_RECEIVE;

        g_can0_mailbox_mask[2] = 0x1FFFFFF0;

        g_can0_mailbox[8].mailbox_id = ee_CanColl.unIdOtherCommands;
        g_can0_mailbox[8].frame_type = CAN_FRAME_TYPE_DATA;
        g_can0_mailbox[8].mailbox_type = CAN_MAILBOX_RECEIVE;

        g_can0_mailbox[9].mailbox_id = ee_CanAddressConfig[STEPPER_D].uwBoardAddress;
        g_can0_mailbox[9].frame_type = CAN_FRAME_TYPE_DATA;
        g_can0_mailbox[9].mailbox_type = CAN_MAILBOX_RECEIVE;

    }

}






//****************************************************
//  inizializza canbus
//****************************************************
void CAN_init(UINT8 mode)
{
    UINT32 temp32;
    ssp_err_t err;
    UINT8 canspeed;

    err = g_can0.p_api->close(g_can0.p_ctrl);
//    if(err != SSP_SUCCESS) while (1);

    if (mode == CONFIGURATION)
    {
        canspeed = CAN_500K;
    }
    else
    {
        canspeed = ee_ConfigColl.ucCanSpeed;
    }

    switch (canspeed)
    {
    case CAN_1000K:
        g_can0_bit_timing_cfg.baud_rate_prescaler = 2;
        g_can0_bit_timing_cfg.time_segment_1 = CAN_TIME_SEGMENT1_TQ9;
        g_can0_bit_timing_cfg.time_segment_2 = CAN_TIME_SEGMENT2_TQ2;
        g_can0_bit_timing_cfg.synchronization_jump_width = CAN_SYNC_JUMP_WIDTH_TQ1;
        break;
    case CAN_500K:
        g_can0_bit_timing_cfg.baud_rate_prescaler = 3;
        g_can0_bit_timing_cfg.time_segment_1 = CAN_TIME_SEGMENT1_TQ12;
        g_can0_bit_timing_cfg.time_segment_2 = CAN_TIME_SEGMENT2_TQ3;
        g_can0_bit_timing_cfg.synchronization_jump_width = CAN_SYNC_JUMP_WIDTH_TQ1;
        break;
    case CAN_250K:
        g_can0_bit_timing_cfg.baud_rate_prescaler = 6;
        g_can0_bit_timing_cfg.time_segment_1 = CAN_TIME_SEGMENT1_TQ12;
        g_can0_bit_timing_cfg.time_segment_2 = CAN_TIME_SEGMENT2_TQ3;
        g_can0_bit_timing_cfg.synchronization_jump_width = CAN_SYNC_JUMP_WIDTH_TQ1;
        break;
    case CAN_125K:
        g_can0_bit_timing_cfg.baud_rate_prescaler = 12;
        g_can0_bit_timing_cfg.time_segment_1 = CAN_TIME_SEGMENT1_TQ12;
        g_can0_bit_timing_cfg.time_segment_2 = CAN_TIME_SEGMENT2_TQ3;
        g_can0_bit_timing_cfg.synchronization_jump_width = CAN_SYNC_JUMP_WIDTH_TQ1;
        break;
    case CAN_100K:
        g_can0_bit_timing_cfg.baud_rate_prescaler = 15;
        g_can0_bit_timing_cfg.time_segment_1 = CAN_TIME_SEGMENT1_TQ12;
        g_can0_bit_timing_cfg.time_segment_2 = CAN_TIME_SEGMENT2_TQ3;
        g_can0_bit_timing_cfg.synchronization_jump_width = CAN_SYNC_JUMP_WIDTH_TQ1;
        break;
    case CAN_50K:
        g_can0_bit_timing_cfg.baud_rate_prescaler = 30;
        g_can0_bit_timing_cfg.time_segment_1 = CAN_TIME_SEGMENT1_TQ12;
        g_can0_bit_timing_cfg.time_segment_2 = CAN_TIME_SEGMENT2_TQ3;
        g_can0_bit_timing_cfg.synchronization_jump_width = CAN_SYNC_JUMP_WIDTH_TQ1;
        break;
    case CAN_20K:
        g_can0_bit_timing_cfg.baud_rate_prescaler = 75;
        g_can0_bit_timing_cfg.time_segment_1 = CAN_TIME_SEGMENT1_TQ12;
        g_can0_bit_timing_cfg.time_segment_2 = CAN_TIME_SEGMENT2_TQ3;
        g_can0_bit_timing_cfg.synchronization_jump_width = CAN_SYNC_JUMP_WIDTH_TQ1;
        break;
    case CAN_10K:
        g_can0_bit_timing_cfg.baud_rate_prescaler = 150;
        g_can0_bit_timing_cfg.time_segment_1 = CAN_TIME_SEGMENT1_TQ12;
        g_can0_bit_timing_cfg.time_segment_2 = CAN_TIME_SEGMENT2_TQ3;
        g_can0_bit_timing_cfg.synchronization_jump_width = CAN_SYNC_JUMP_WIDTH_TQ1;
        break;
    default:        // 500 Kb
        g_can0_bit_timing_cfg.baud_rate_prescaler = 2;
        g_can0_bit_timing_cfg.time_segment_1 = CAN_TIME_SEGMENT1_TQ12;
        g_can0_bit_timing_cfg.time_segment_2 = CAN_TIME_SEGMENT2_TQ3;
        g_can0_bit_timing_cfg.synchronization_jump_width = CAN_SYNC_JUMP_WIDTH_TQ1;
        break;
    }

    CAN_crea_slot(g_ucWorkModality);

    ID_OtherCommand = ee_CanColl.unIdOtherCommands;

    err = g_can0.p_api->open(g_can0.p_ctrl, g_can0.p_cfg);
    if(err != SSP_SUCCESS) while (1);

    tx_event_flags_set(&g_can_flags, FLAG_CAN_EVENT_TX_COMPLETE, TX_OR);


    ixTI = 0;
    ixTF = 0;
    ix0I = 0;
    ix0F = 0;
    ixMI = 0;
    ixMF = 0;
    ixRI = 0;
    ixRF = 0;
    ixCI = 0;
    ixCF = 0;

}






//*********************************************
//  trasmette messaggio se presente in fifo
//*********************************************
void CAN_tx_msg(void)
{
    ssp_err_t status;
    can_frame_t transmitFrame;
    CAN_STD_DATA_DEF *ptr;

    if ((ixTI != ixTF) && (block_tx == FALSE))       // controlla se c'e' un messaggio in fifo da spedire
    {
        ptr = &tx_dati_0[ixTF];

        if (ptr->remote == FALSE)       // data o remote
        {
            transmitFrame.type = CAN_FRAME_TYPE_DATA;
        }
        else
        {
            transmitFrame.type = CAN_FRAME_TYPE_REMOTE;
        }

        transmitFrame.id = ptr->id;
        transmitFrame.data_length_code = ptr->dlc;
        transmitFrame.data[0] = ptr->data.data[0];
        transmitFrame.data[1] = ptr->data.data[1];
        transmitFrame.data[2] = ptr->data.data[2];
        transmitFrame.data[3] = ptr->data.data[3];
        transmitFrame.data[4] = ptr->data.data[4];
        transmitFrame.data[5] = ptr->data.data[5];
        transmitFrame.data[6] = ptr->data.data[6];
        transmitFrame.data[7] = ptr->data.data[7];

        status = g_can0.p_api->write(g_can0.p_ctrl, MAILBOX_TRANSMIT, &transmitFrame);
        if (status == SSP_SUCCESS)
        {
            if (++ixTF >= NUM_BUFF_TX_CAN)
            {
                ixTF = 0;
            }
        }
        else
        {
            if (timer_tx_can == 0)
            {
                can0_sw_err |= CAN_SW_TRM_ERR;
            }
        }
    }
    else
    {
        timer_tx_can = 10;  // in 1/100 secondo
    }
}


