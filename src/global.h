/*
 * config.h
 *
 *  Created on: 23 mag 2017
 *      Author: daniele
 */


//#ifndef GLOBAL_H_
//#define GLOBAL_H_

//  VERSIONE FIRMWARE
//#define VERSIONE_FW     "03.00  "       // NOTA: devono essere 7 caratteri
//#define VERSIONE_FIRMWARE   "R225T300"



//***********************
//****  DEFINIZIONI  ****
//***********************


typedef enum {
    STEPPER_A = 0, STEPPER_B, STEPPER_C, STEPPER_D, MAX_STEPPER
};


#define FLASH_MIN_PGM_SIZE_DF               4
#define FLASH_LEN_BLOCK_DATI                10      // lunghezza in blocchi
//  ATTENZIONE: il blocco 0 e' usato dal bootloader!!!!
#define FLASH_BLOCK_VIRT_EEPROM_MAIN        FLASH_DF_BLOCK_1
#define FLASH_BLOCK_VIRT_EEPROM_ANALOG      FLASH_DF_BLOCK_15
#define FLASH_BLOCK_VIRT_EEPROM_IRIDE       FLASH_DF_BLOCK_30
#define FLASH_BLOCK_VIRT_EEPROM_CHECK       FLASH_DF_BLOCK_45



#define bool  char          /* (1 bit) 0/1. Optionally use data bit */
#define true    1
#define false   0
#define TRUE    1
#define FALSE   0
#define ON      true
#define OFF     false
#define MOV_SLOW    0
#define MOV_FAST    1
#define MOV_ENC     2
#define MOV_RESET   3
#define MOV_MM      4

typedef signed char                             SINT8;
typedef unsigned char                           UINT8;
typedef signed short                            SINT16;
typedef unsigned short                          UINT16;
typedef signed long                             SINT32;
typedef unsigned long                           UINT32;
typedef unsigned long long                      UINT64;

typedef signed char                             schar;
typedef unsigned char                           uchar;
typedef unsigned short                          uword;
typedef unsigned long                           ulong;
typedef signed long                             slong;



typedef union    /* 8 / 16 bit IO conversion union */
{
    uint16_t u;
    int16_t i;
    struct
    {
        uchar b1;       /* MSB byte */
        uchar b0;       /* LSB byte */
    }b;
} union16;

typedef union   /* 8 / 16 / 32 bit IO conversion union */
{
    ulong ul;
    slong l;
    struct
    {
        uint16_t u1;        /* MSB word */
        uint16_t u0;        /* LSB word */
    }u;
    struct
    {
        uchar b3;       /* MSB byte (exp) */
        uchar b2;
        uchar b1;
        uchar b0;       /* LSB byte */
    }b;
} union32;

//Bit-Field
#define SetBit(x,y)     ((x) |= (1<<y))         //Byte
#define ResBit(x,y)     ((x) &=(~(1<<y)))       //Byte
#define CheckBit(x,y)   ((x) & (1<<y))          //Byte
#define SetBitI(x,y)    ((x) |= (uint16_t)(1<<y))   //Integer
#define ResBitI(x,y)    ((x) &= (uint16_t)(~(1<<y)))//Integer
#define CheckBitI(x,y)  ((x) & (uint16_t)(1<<y))    //Integer

//Processes
#define process void


struct bitt8
{
    unsigned bit0               :1;
    unsigned bit1               :1;
    unsigned bit2               :1;
    unsigned bit3               :1;
    unsigned bit4               :1;
    unsigned bit5               :1;
    unsigned bit6               :1;
    unsigned bit7               :1;
};

struct bitt8_t
{
    unsigned bit0               :1;
    unsigned bit1               :1;
    unsigned bit2               :1;
    unsigned bit3               :1;
    unsigned bit4               :1;
    unsigned bit5               :1;
    unsigned bit6               :1;
    unsigned bit7               :1;
};




//LEDS status
typedef enum{
    GREEN = 0, YELLOW, RED, NONE
};




//#endif /* GLOBAL_H_ */
