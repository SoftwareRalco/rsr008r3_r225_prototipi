#define _SYS_CALIBRATION_

//--------------------------------------------------------------
// PRIMA STESURA
// Riva ing. Franco M.
// Besana Brianza (MI)
//
// REVISIONI SUCCESSIVE
// Giovanni Corvini
// Melegnano (MI)
//
//--------------------------------------------------------------
//
// PROJECT:	  RSR008 - Low Cost Collimator
// FILE:      sys_calibration.c
// REVISIONS: 0.00 - 22/09/2008 - First Definition
//
//--------------------------------------------------------------

//--------------------------------------------------------------
// INCLUDE FILES
//--------------------------------------------------------------
#include "include.h"
#include "string.h"
#include "math.h"

#include "tx_api.h"

//--------------------------------------------------------------
// GLOBAL VARIABLES
//--------------------------------------------------------------
#define TIMER_CALIBRATION       2000

enum {

    CALIBRATION_INIT = 0,
    CALIBRATION_START,
    CALIBRATION_EXEC,
    CALIBRATION_ASR003_START,
    CALIBRATION_ASR003_WAIT,

    CALIBRATION_SHOW_LANGUAGE,
    CALIBRATION_WAIT_LANGUAGE,

    CALIBRATION_INIT_MEASURE,
    CALIBRATION_SHOW_MEASURE,
    CALIBRATION_WAIT_MEASURE,
    CALIBRATION_END_MEASURE,

    CALIBRATION_INIT_DFF_VERT,
    CALIBRATION_SHOW_DFF_VERT,
    CALIBRATION_WAIT_DFF_VERT,

    CALIBRATION_INIT_TIPO_INPUT,
    CALIBRATION_SHOW_TIPO_INPUT,
    CALIBRATION_WAIT_TIPO_INPUT,

    CALIBRATION_INIT_DFF_FIXED,
    CALIBRATION_SHOW_DFF_FIXED,
    CALIBRATION_WAIT_DFF_FIXED,

    CALIBRATION_INIT_LAT_DFF_SX,
    CALIBRATION_SHOW_LAT_DFF_SX,
    CALIBRATION_WAIT_LAT_DFF_SX,

    CALIBRATION_INIT_LAT_DFF_DX,
    CALIBRATION_SHOW_LAT_DFF_DX,
    CALIBRATION_WAIT_LAT_DFF_DX,

    CALIBRATION_INIT_REC_VERT,
    CALIBRATION_SHOW_REC_VERT,
    CALIBRATION_WAIT_REC_VERT,
    CALIBRATION_END_REC_VERT,

    CALIBRATION_INIT_REC_DX,
    CALIBRATION_SHOW_REC_DX,
    CALIBRATION_WAIT_REC_DX,

    CALIBRATION_INIT_REC_SX,
    CALIBRATION_SHOW_REC_SX,
    CALIBRATION_WAIT_REC_SX,
    CALIBRATION_END_REC_SX,

    CALIBRATION_INIT_POT_DX_MIN,
    CALIBRATION_SHOW_POT_DX_MIN,
    CALIBRATION_WAIT_POT_DX_MIN,
    CALIBRATION_END_POT_DX_MIN,

    CALIBRATION_INIT_POT_DX_MAX,
    CALIBRATION_SHOW_POT_DX_MAX,
    CALIBRATION_WAIT_POT_DX_MAX,
    CALIBRATION_END_POT_DX_MAX,

    CALIBRATION_INIT_POT_SX_MIN,
    CALIBRATION_SHOW_POT_SX_MIN,
    CALIBRATION_WAIT_POT_SX_MIN,
    CALIBRATION_END_POT_SX_MIN,

    CALIBRATION_INIT_POT_SX_MAX,
    CALIBRATION_SHOW_POT_SX_MAX,
    CALIBRATION_WAIT_POT_SX_MAX,
    CALIBRATION_END_POT_SX_MAX,

    CALIBRATION_INIT_DFF_MIN,
    CALIBRATION_SHOW_DFF_MIN,
    CALIBRATION_WAIT_DFF_MIN,

    CALIBRATION_INIT_DFF_MAX,
    CALIBRATION_SHOW_DFF_MAX,
    CALIBRATION_WAIT_DFF_MAX,
    CALIBRATION_END_DFF_MAX,

    CALIBRATION_INIT_DFT_VERT,
    CALIBRATION_SHOW_DFT_VERT,
    CALIBRATION_WAIT_DFT_VERT,
    CALIBRATION_END_DFT_VERT,

    CALIBRATION_INIT_DFT_SX,
    CALIBRATION_SHOW_DFT_SX,
    CALIBRATION_WAIT_DFT_SX,
    CALIBRATION_END_DFT_SX,

    CALIBRATION_INIT_DFT_DX,
    CALIBRATION_SHOW_DFT_DX,
    CALIBRATION_WAIT_DFT_DX,
    CALIBRATION_END_DFT_DX,

    CALIBRATION_INIT_SHOW_DFF,
    CALIBRATION_SHOW_SHOW_DFF,
    CALIBRATION_WAIT_SHOW_DFF,

    CALIBRATION_INIT_SHOW_KEY,
    CALIBRATION_SHOW_SHOW_KEY,
    CALIBRATION_WAIT_SHOW_KEY,
    CALIBRATION_END_SHOW_KEY,

    CALIBRATION_INIT_MAN_COLLIMATION,
    CALIBRATION_SHOW_MAN_COLLIMATION,
    CALIBRATION_WAIT_MAN_COLLIMATION,
    CALIBRATION_END_MAN_COLLIMATION,

    CALIBRATION_INIT_SHOW_ANGLE,
    CALIBRATION_SHOW_SHOW_ANGLE,
    CALIBRATION_WAIT_SHOW_ANGLE,
    CALIBRATION_END_SHOW_ANGLE,

    CALIBRATION_INIT_INCL_PRES,
    CALIBRATION_SHOW_INCL_PRES,
    CALIBRATION_WAIT_INCL_PRES,
    CALIBRATION_END_INCL_PRES,

    CALIBRATION_INIT_INCL_FILTRAGGIO,
    CALIBRATION_SHOW_INCL_FILTRAGGIO,
    CALIBRATION_WAIT_INCL_FILTRAGGIO,
    CALIBRATION_END_INCL_FILTRAGGIO,

    CALIBRATION_INIT_INCL_TAR,
    CALIBRATION_SHOW_INCL_TAR,
    CALIBRATION_WAIT_INCL_TAR,
    CALIBRATION_END_INCL_TAR,

    CALIBRATION_INIT_INCL_TAR_DX,
    CALIBRATION_SHOW_INCL_TAR_DX,
    CALIBRATION_WAIT_INCL_TAR_DX,
    CALIBRATION_END_INCL_TAR_DX,

    CALIBRATION_INIT_INCL_TAR_SX,
    CALIBRATION_SHOW_INCL_TAR_SX,
    CALIBRATION_WAIT_INCL_TAR_SX,
    CALIBRATION_END_INCL_TAR_SX,

    CALIBRATION_INIT_DEGREES,
    CALIBRATION_SHOW_DEGREES,
    CALIBRATION_WAIT_DEGREES,
    CALIBRATION_END_DEGREES,

    CALIBRATION_INIT_VERT_REC1,
    CALIBRATION_SHOW_VERT_REC1,
    CALIBRATION_WAIT_VERT_REC1,
    CALIBRATION_END_VERT_REC1,

    CALIBRATION_INIT_VERT_REC2,
    CALIBRATION_SHOW_VERT_REC2,
    CALIBRATION_WAIT_VERT_REC2,
    CALIBRATION_ERR_VERT_REC2,

    CALIBRATION_INIT_VERT_COLL,
    CALIBRATION_SHOW_VERT_COLL,
    CALIBRATION_WAIT_VERT_COLL,

    CALIBRATION_INIT_VERT_REC3,
    CALIBRATION_SHOW_VERT_REC3,
    CALIBRATION_WAIT_VERT_REC3,

    CALIBRATION_SHOW_VERT_REC4,
    CALIBRATION_WAIT_VERT_REC4,
    CALIBRATION_ERR_VERT_REC4,

    CALIBRATION_SHOW_VERT_REC5,
    CALIBRATION_WAIT_VERT_REC5,

    CALIBRATION_INIT_DFF_TYPE,
    CALIBRATION_LOAD_DFF_TYPE,
    CALIBRATION_SHOW_DFF_TYPE,
    CALIBRATION_WAIT_DFF_TYPE,
    CALIBRATION_END_DFF_TYPE,

    CALIBRATION_SHOW_CAL_TYPE_TOP,
    CALIBRATION_WAIT_CAL_TYPE_TOP,
    CALIBRATION_SHOW_BUCKY_TOP_FULL,
    CALIBRATION_WAIT_BUCKY_TOP_FULL,
    CALIBRATION_SHOW_BUCKY_TOP_QUICK,
    CALIBRATION_WAIT_BUCKY_TOP_QUICK,
    CALIBRATION_INIT_BUCKY_TOP_ACQ,
    CALIBRATION_SHOW_BUCKY_TOP_ACQ,
    CALIBRATION_WAIT_BUCKY_TOP_ACQ,

    CALIBRATION_SHOW_CAL_TYPE_SX,
    CALIBRATION_WAIT_CAL_TYPE_SX,
    CALIBRATION_SHOW_BUCKY_SX_FULL,
    CALIBRATION_WAIT_BUCKY_SX_FULL,
    CALIBRATION_SHOW_BUCKY_SX_QUICK,
    CALIBRATION_WAIT_BUCKY_SX_QUICK,
    CALIBRATION_SHOW_BUCKY_SX_ACQ,
    CALIBRATION_WAIT_BUCKY_SX_ACQ,

    CALIBRATION_SHOW_CAL_TYPE_DX,
    CALIBRATION_WAIT_CAL_TYPE_DX,
    CALIBRATION_SHOW_BUCKY_DX_FULL,
    CALIBRATION_WAIT_BUCKY_DX_FULL,
    CALIBRATION_SHOW_BUCKY_DX_QUICK,
    CALIBRATION_WAIT_BUCKY_DX_QUICK,
    CALIBRATION_SHOW_BUCKY_DX_ACQ,
    CALIBRATION_WAIT_BUCKY_DX_ACQ,
    CALIBRATION_END_BUCKY_DX_ACQ,

    CALIBRATION_INIT_FORMAT,
    CALIBRATION_LOAD_FORMAT,
    CALIBRATION_SHOW_FORMAT,
    CALIBRATION_WAIT_FORMAT,
    CALIBRATION_END_FORMAT,

    CALIBRATION_INIT_FORMAT_IRIS,
    CALIBRATION_LOAD_FORMAT_IRIS,
    CALIBRATION_SHOW_FORMAT_IRIS,
    CALIBRATION_WAIT_FORMAT_IRIS,
    CALIBRATION_END_FORMAT_IRIS,

    CALIBRATION_SHOW_NEXT,
    CALIBRATION_SHOW_END,
    CALIBRATION_WAIT_END,
    CALIBRATION_END,
};


#define MIN_BUCKY_CM    13
#define MAX_BUCKY_CM    43
#define MIN_BUCKY_IN     8
#define MAX_BUCKY_IN    17

static uchar g_ucCalibrationStatus=CALIBRATION_INIT;
static bool g_bCalFull=false;
static  uchar g_ucNMisCassetteTable=2;
static  uchar g_ucNMisCassetteDx=2;
static  uchar g_ucNMisCassetteSx=2;

const char g_ucTableMisCrossCm[MAX_CASSETTE] = {13, 18, 20, 24, 30, 35, 40, 43, 0 ,0 };
const char g_ucTableMisLongCm[MAX_CASSETTE]  = {13, 18, 20, 24, 30, 35, 40, 43, 0 ,0 };
const char g_ucTableMisCrossIn[MAX_CASSETTE] = { 8, 10, 11, 12, 14, 17,  0,  0, 0 ,0 };
const char g_ucTableMisLongIn[MAX_CASSETTE]  = { 8, 10, 11, 12, 14, 17,  0,  0, 0 ,0 };

//--------------------------------------------------------------
void wait_key_filter_up (uword uwDelayMsec){
    while (FILTER_SWITCH){
       g_uwPushButtonDelay = uwDelayMsec;
       while (g_uwPushButtonDelay)
       {
           if(!FILTER_SWITCH)
           {
               break;
           }
           tx_thread_sleep (1);
       }
    }
}

//--------------------------------------------------------------
void wait_key_exit_up (uword uwDelayMsec){
    while (LAMP_SWITCH){
       g_uwPushButtonDelay = uwDelayMsec;
       while (g_uwPushButtonDelay)
       {
           if(!LAMP_SWITCH)
           {
               break;
           }
           tx_thread_sleep (1);
       }

    }
}

//--------------------------------------------------------------
void wait_key_fast(uword uwDelayMsec){
    g_uwPushButtonDelay = uwDelayMsec;
    while (g_uwPushButtonDelay)
    {
        tx_thread_sleep (1);
    }

}

//--------------------------------------------------------------
void sys_board_calibration(void){

    static uword s_uwLastCrossSteps=-1;
    static uword s_uwLastLongSteps=-1;
    static UINT32 s_ulTempUpdateEeprom=0;
    static uchar s_ucSetCount=0;
    static uchar s_ucCassetteCounter=0;
    static SINT16 s_ucCmCounterCross=MIN_BUCKY_CM;
    static SINT16 s_ucInCounterCross=MIN_BUCKY_IN;
    static SINT16 s_ucCmCounterLong=MIN_BUCKY_CM;
    static SINT16 s_ucInCounterLong=MIN_BUCKY_IN;
    static SINT16 s_temp16, s_temp16a;

    bool bInsert;
    uchar ucIndex;
    uchar ucIndex2;
    uchar ucCrossCounter;
    uchar ucLongCounter;

    int nA;
    int nB;
    int nC;
    uword uwAlfaC;
    uword uwAlfaL;


    switch (g_ucCalibrationStatus)
    {
    case CALIBRATION_INIT:
        sys_print_msg (0x80,"RSR008r3 CALIBRATION");
        sys_print_msg (0xC0,"                    ");
        g_uwCalibrationTimer=TIMER_CALIBRATION;
        g_ucCalibrationStatus=CALIBRATION_START;
        break;

    case CALIBRATION_START:
        if (g_uwCalibrationTimer)
            break;
        wait_key_exit_up(250);
        g_wCrossMagnitude = 0;
        g_wLongMagnitude = 0;
//        sys_print_msg (0xC0,"Cross      Long     ");
//        g_uwCalibrationTimer=TIMER_CALIBRATION;
        g_ucCalibrationStatus=CALIBRATION_EXEC;
        break;

    case CALIBRATION_EXEC:
        if (g_bUserCalRequest){
            g_ucCalibrationStatus=CALIBRATION_ASR003_START;
            break;
        }
        g_ucCalibrationStatus = CALIBRATION_ASR003_START;
        break;
/*
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_ASR003_START;
            break;
        }
        if ((uword)g_lVPos[STEPPER_CROSS]!=s_uwLastCrossSteps){
            s_uwLastCrossSteps=(uword)g_lVPos[STEPPER_CROSS];
            sys_print_number (0xC6,s_uwLastCrossSteps,4,0xFF,false);
        }
        if ((uword)g_lVPos[STEPPER_LONG]!=s_uwLastLongSteps){
            s_uwLastLongSteps=(uword)g_lVPos[STEPPER_LONG];
            sys_print_number (0xD0,s_uwLastLongSteps,4,0xFF,false);
        }
        if (g_wCrossMagnitude && !Steppers[STEPPER_CROSS].StepperData.StepperBits.bMoving){
            g_lDestination[STEPPER_CROSS]=g_wCrossMagnitude * MAGNITUDE_RESOLUTION;
            g_wCrossMagnitude=0;
            g_bMoveRelStepperA=true;
            config_movimento(STEPPER_CROSS, MSG_MOV_REL, g_lDestination[STEPPER_CROSS], MOV_SLOW);
        }
        if (g_wLongMagnitude && !Steppers[STEPPER_LONG].StepperData.StepperBits.bMoving){
            g_lDestination[STEPPER_LONG]=g_wLongMagnitude * MAGNITUDE_RESOLUTION;
            g_wLongMagnitude=0;
            g_bMoveRelStepperB=true;
            config_movimento(STEPPER_LONG, MSG_MOV_REL, g_lDestination[STEPPER_LONG], MOV_SLOW);
        }
        break;
*/

    case CALIBRATION_ASR003_START:
        sys_display_clear_screen();
        sys_print_msg (0x80,"ANALOG SIGNAL CALIB.");
        sys_print_msg (0xC0,"Rem.Jumper to Start ");
        g_bUserCalInProgress=true;
        g_ucCalibrationStatus = CALIBRATION_ASR003_WAIT;
        //break;    //Fall Through

    case CALIBRATION_ASR003_WAIT:
        if (g_bUserCalRequest)
            break;
        g_wCrossMagnitude = 0;
        g_wLongMagnitude = 0;
        s_temp16 = ee_ConfigColl.ucLanguage;
        g_ucCalibrationStatus = CALIBRATION_SHOW_LANGUAGE;
        //break;    //Fall Through

//  *****************************************************************************************************
    case CALIBRATION_SHOW_LANGUAGE:
        switch (s_temp16)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Lingua              ");
            sys_print_msg (0xC0,"ITALIANO            ");
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Language            ");
            sys_print_msg (0xC0,"ENGLISH             ");
            break;
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_LANGUAGE;
        //break;    //Fall Through
    case CALIBRATION_WAIT_LANGUAGE:
        nA = CheckChangeCrossEnc();
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 >= MAX_LANG)
                s_temp16 = MAX_LANG - 1;
            else if (s_temp16 < 0)
                s_temp16 = 0;
            g_ucCalibrationStatus = CALIBRATION_SHOW_LANGUAGE;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_INIT_MEASURE;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_ConfigColl.ucLanguage = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_INIT_MEASURE;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;

//  *****************************************************************************************************
    case CALIBRATION_INIT_MEASURE:
        s_temp16 = ee_ConfigColl.ucCmUnit;

    case CALIBRATION_SHOW_MEASURE:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Unita' di misura    ");
            if (s_temp16 == CM_UNIT)
                sys_print_msg (0xc0,"CENTIMETRI          ");
            else
                sys_print_msg (0xc0,"POLLICI             ");
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Measuring Units     ");
            if (s_temp16 == CM_UNIT)
                sys_print_msg (0xc0,"CENTIMETERS         ");
            else
                sys_print_msg (0xc0,"INCHES              ");
            break;
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_MEASURE;
        //break;    //Fall Through
    case CALIBRATION_WAIT_MEASURE:
        nA = CheckChangeCrossEnc();
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 >= MAX_UNIT)
                s_temp16 = MAX_UNIT - 1;
            else if (s_temp16 < 0)
                s_temp16 = 0;
            g_ucCalibrationStatus = CALIBRATION_SHOW_MEASURE;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_END_MEASURE;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_ConfigColl.ucCmUnit = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_END_MEASURE;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;
    case CALIBRATION_END_MEASURE:


//  *****************************************************************************************************
    case CALIBRATION_INIT_INCL_PRES:
        s_temp16 = ee_ConfigColl.ucInclinometer;

    case CALIBRATION_SHOW_INCL_PRES:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Inclinom. Presente? ");
            switch (s_temp16){
            case INCLIN_NO:
                sys_print_msg (0xC0,"NO                  ");
                break;
            case INCLIN_YES:
                sys_print_msg (0xC0,"SI                  ");
                break;
            }
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Inclinom. Present?  ");
            switch (s_temp16){
            case INCLIN_NO:
                sys_print_msg (0xC0,"NO                  ");
                break;
            case INCLIN_YES:
                sys_print_msg (0xC0,"YES                 ");
                break;
            }
            break;
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_INCL_PRES;
        //break;    //Fall Through
    case CALIBRATION_WAIT_INCL_PRES:
        nA = CheckChangeCrossEnc();
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 > INCLIN_YES)
                s_temp16 = INCLIN_YES;
            else if (s_temp16 < 0)
                s_temp16 = 0;
            g_ucCalibrationStatus = CALIBRATION_SHOW_INCL_PRES;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_END_INCL_PRES;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_ConfigColl.ucInclinometer = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_END_INCL_PRES;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;
    case CALIBRATION_END_INCL_PRES:



//  *****************************************************************************************************
    case CALIBRATION_INIT_INCL_FILTRAGGIO:
        s_temp16 = ee_ConfigColl.ucFiltraggioIncl;
        if (ee_ConfigColl.ucInclinometer == INCLIN_YES){
            g_ucCalibrationStatus = CALIBRATION_SHOW_INCL_FILTRAGGIO;
        } else {
            g_ucCalibrationStatus = CALIBRATION_END_INCL_FILTRAGGIO;
        }
        break;

    case CALIBRATION_SHOW_INCL_FILTRAGGIO:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Filtraggio Inclin.? ");
            switch (s_temp16){
            case FALSE:
                sys_print_msg (0xC0,"NO                  ");
                break;
            default:
                sys_print_msg (0xC0,"SI                  ");
                break;
            }
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Inclinom. Filtering?");
            switch (s_temp16){
            case FALSE:
                sys_print_msg (0xC0,"NO                  ");
                break;
            default:
                sys_print_msg (0xC0,"YES                 ");
                break;
            }
            break;
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_INCL_FILTRAGGIO;
        //break;	//Fall Through
    case CALIBRATION_WAIT_INCL_FILTRAGGIO:
        nA = CheckChangeCrossEnc();
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 > TRUE)
                s_temp16 = TRUE;
            else if (s_temp16 < 0)
                s_temp16 = 0;
            g_ucCalibrationStatus = CALIBRATION_SHOW_INCL_FILTRAGGIO;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_END_INCL_FILTRAGGIO;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_ConfigColl.ucFiltraggioIncl = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_END_INCL_FILTRAGGIO;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;
    case CALIBRATION_END_INCL_FILTRAGGIO:


//  *****************************************************************************************************
    case CALIBRATION_INIT_INCL_TAR:
        if (ee_ConfigColl.ucInclinometer == INCLIN_YES){
            g_ucCalibrationStatus = CALIBRATION_SHOW_INCL_TAR;
        } else {
            g_ucCalibrationStatus = CALIBRATION_END_INCL_TAR;
        }
        break;

    case CALIBRATION_SHOW_INCL_TAR:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Offset Inclinom. 0  ");
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Inclinom. Offset 0  ");
            break;
        }
        sys_print_msg    (0xC0,"X:      Y:          ");
        sys_print_number (0xC2,g_nFilteredAdc[ADXL_X],4,0xff,false);
        sys_print_number (0xCA,g_nFilteredAdc[ADXL_Y],4,0xff,false);
        g_ucCalibrationStatus = CALIBRATION_WAIT_INCL_TAR;
        //break;    //Fall Through
    case CALIBRATION_WAIT_INCL_TAR:
        if (write_msg == TRUE)
        {
            write_msg = FALSE;
            g_ucCalibrationStatus = CALIBRATION_SHOW_INCL_TAR;
            break;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_END_INCL_TAR;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_OffsetColl.nInclOffsetX = g_nFilteredAdc[ADXL_X];
            ee_OffsetColl.nInclOffsetY = g_nFilteredAdc[ADXL_Y];
            s_ulTempUpdateEeprom |= EEP_UPDATE_OFFSET;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_END_INCL_TAR;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;

    case CALIBRATION_END_INCL_TAR:

//  *****************************************************************************************************
    case CALIBRATION_INIT_INCL_TAR_DX:
        if (ee_ConfigColl.ucInclinometer == INCLIN_YES){
            g_ucCalibrationStatus = CALIBRATION_SHOW_INCL_TAR_DX;
        } else {
            g_ucCalibrationStatus = CALIBRATION_END_INCL_TAR_DX;
        }
        break;

    case CALIBRATION_SHOW_INCL_TAR_DX:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Offset Inclinom. 90D");
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Inclinom. Offset 90R");
            break;
        }
        sys_print_msg    (0xC0,"X:      Y:          ");
        sys_print_number (0xC2,g_nFilteredAdc[ADXL_X],4,0xff,false);
        sys_print_number (0xCA,g_nFilteredAdc[ADXL_Y],4,0xff,false);
        g_ucCalibrationStatus = CALIBRATION_WAIT_INCL_TAR_DX;
        //break;    //Fall Through
    case CALIBRATION_WAIT_INCL_TAR_DX:
        if (write_msg == TRUE)
        {
            write_msg = FALSE;
            g_ucCalibrationStatus = CALIBRATION_SHOW_INCL_TAR_DX;
            break;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_END_INCL_TAR_DX;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_OffsetColl.InclOffsetDxX = g_nFilteredAdc[ADXL_X];
            ee_OffsetColl.InclOffsetDxY = g_nFilteredAdc[ADXL_Y];
            s_ulTempUpdateEeprom |= EEP_UPDATE_OFFSET;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_END_INCL_TAR_DX;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;

    case CALIBRATION_END_INCL_TAR_DX:

//  *****************************************************************************************************
    case CALIBRATION_INIT_INCL_TAR_SX:
        if (ee_ConfigColl.ucInclinometer == INCLIN_YES){
            g_ucCalibrationStatus = CALIBRATION_SHOW_INCL_TAR_SX;
        } else {
            g_ucCalibrationStatus = CALIBRATION_END_INCL_TAR_SX;
        }
        break;

    case CALIBRATION_SHOW_INCL_TAR_SX:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Offset Inclinom. 90S");
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Inclinom. Offset 90L");
            break;
        }
        sys_print_msg    (0xC0,"X:      Y:          ");
        sys_print_number (0xC2,g_nFilteredAdc[ADXL_X],4,0xff,false);
        sys_print_number (0xCA,g_nFilteredAdc[ADXL_Y],4,0xff,false);
        g_ucCalibrationStatus = CALIBRATION_WAIT_INCL_TAR_SX;
        //break;    //Fall Through
    case CALIBRATION_WAIT_INCL_TAR_SX:
        if (write_msg == TRUE)
        {
            write_msg = FALSE;
            g_ucCalibrationStatus = CALIBRATION_SHOW_INCL_TAR_SX;
            break;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_END_INCL_TAR_SX;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_OffsetColl.InclOffsetSxX = g_nFilteredAdc[ADXL_X];
            ee_OffsetColl.InclOffsetSxY = g_nFilteredAdc[ADXL_Y];
            s_ulTempUpdateEeprom |= EEP_UPDATE_OFFSET;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_END_INCL_TAR_SX;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;

    case CALIBRATION_END_INCL_TAR_SX:

//  *****************************************************************************************************
    case CALIBRATION_INIT_DEGREES:
        if (ee_ConfigColl.ucInclinometer == INCLIN_YES){
            s_temp16 = ee_ConfigColl.ucInclDegrees;
            g_ucCalibrationStatus = CALIBRATION_SHOW_DEGREES;
        } else {
            g_ucCalibrationStatus = CALIBRATION_END_DEGREES;
        }
        break;

    case CALIBRATION_SHOW_DEGREES:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Gradi di Tolleranza ");
            sys_print_msg (0xC0,"    Gradi           ");
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Degrees Tolerance   ");
            sys_print_msg (0xC0,"    Degrees         ");
            break;
        }
        sys_print_number (0xc0,s_temp16,3,0xff,false);
        g_ucCalibrationStatus = CALIBRATION_WAIT_DEGREES;
        //break;    //Fall Through
    case CALIBRATION_WAIT_DEGREES:
        nA = EncoderCalib(&g_wCrossMagnitude, 2);
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 > AL_INCL_MAX)
                s_temp16 = AL_INCL_MAX;
            if (s_temp16 < AL_INCL_MIN)
                s_temp16 = AL_INCL_MIN;
            g_ucCalibrationStatus = CALIBRATION_SHOW_DEGREES;
            break;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_END_DEGREES;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_ConfigColl.ucInclDegrees = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_END_DEGREES;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;

    case CALIBRATION_END_DEGREES:


//  *****************************************************************************************************
    case CALIBRATION_INIT_SHOW_ANGLE:
        if (ee_ConfigColl.ucInclinometer == INCLIN_YES){
            s_temp16 = ee_ConfigColl.ucShowAngle;
            g_ucCalibrationStatus = CALIBRATION_SHOW_SHOW_ANGLE;
        } else {
            g_ucCalibrationStatus = CALIBRATION_END_SHOW_ANGLE;
        }
        break;

    case CALIBRATION_SHOW_SHOW_ANGLE:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Vis. Angolo Orient. ");
            switch (s_temp16){
            case ANGLE_SHOW_NO:
                sys_print_msg (0xC0,"NO                  ");
                break;
            case ANGLE_SHOW_YES:
                sys_print_msg (0xC0,"SI                  ");
                break;
            }
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Angle Visualization ");
            switch (s_temp16){
            case ANGLE_SHOW_NO:
                sys_print_msg (0xC0,"NO                  ");
                break;
            case ANGLE_SHOW_YES:
                sys_print_msg (0xC0,"YES                 ");
                break;
            }
            break;
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_SHOW_ANGLE;
        //break;    //Fall Through
    case CALIBRATION_WAIT_SHOW_ANGLE:
        nA = CheckChangeCrossEnc();
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 > ANGLE_SHOW_YES)
                s_temp16 = ANGLE_SHOW_YES;
            else if (s_temp16 < 0)
                s_temp16 = 0;
            g_ucCalibrationStatus = CALIBRATION_SHOW_SHOW_ANGLE;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_END_SHOW_ANGLE;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_ConfigColl.ucShowAngle = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_END_SHOW_ANGLE;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;

    case CALIBRATION_END_SHOW_ANGLE:



//  *****************************************************************************************************
    case CALIBRATION_INIT_DFF_VERT:
        s_temp16 = ee_ConfigColl.ucVertDff;

    case CALIBRATION_SHOW_DFF_VERT:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Misura DFF Vert.    ");
            switch (s_temp16){
            case VERT_DFF_SINGLE:
                sys_print_msg (0xc0,"SINGOLA             ");
                break;
            case VERT_DFF_DIFF:
                sys_print_msg (0xc0,"DIFFERENZIALE       ");
                break;
            case VERT_DFF_FIXED:
                sys_print_msg (0xc0,"FISSA               ");
                break;
            case VERT_DFF_CAN:
                sys_print_msg (0xc0,"CAN                 ");
                break;
            }
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Vertical SID Meas.   ");
            switch (s_temp16){
            case VERT_DFF_SINGLE:
                sys_print_msg (0xc0,"SINGLE              ");
                break;
            case VERT_DFF_DIFF:
                sys_print_msg (0xc0,"DIFFERENTIAL        ");
                break;
            case VERT_DFF_FIXED:
                sys_print_msg (0xc0,"FIXED               ");
                break;
            case VERT_DFF_CAN:
                sys_print_msg (0xc0,"CAN                 ");
                break;
            }
            break;
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_DFF_VERT;
        //break;    //Fall Through
    case CALIBRATION_WAIT_DFF_VERT:
        nA = CheckChangeCrossEnc();
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 >= VERT_DFF_MAX)
                s_temp16 = VERT_DFF_MAX - 1;
            else if (s_temp16 < 0)
                s_temp16 = 0;
            g_ucCalibrationStatus = CALIBRATION_SHOW_DFF_VERT;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_INIT_TIPO_INPUT;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_ConfigColl.ucVertDff = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_INIT_TIPO_INPUT;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;




//  *****************************************************************************************************
    case CALIBRATION_INIT_TIPO_INPUT:
        if (ee_ConfigColl.ucVertDff == VERT_DFF_SINGLE){
            s_temp16 = ee_ConfigColl.ucInputDffPot;
            g_ucCalibrationStatus = CALIBRATION_SHOW_TIPO_INPUT;
        } else {
            g_ucCalibrationStatus = CALIBRATION_INIT_DFF_FIXED;
        }
        break;
    case CALIBRATION_SHOW_TIPO_INPUT:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Ingresso Potenziom.:");
            if (s_temp16 == POT_SINGLE_STAND)
                sys_print_msg (0xc0,"POT. STATIVO        ");
            else
                sys_print_msg (0xc0,"POT. TAVOLO         ");
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Potentiometer Input:");
            if (s_temp16 == POT_SINGLE_STAND)
                sys_print_msg (0xc0,"STAND POT.          ");
            else
                sys_print_msg (0xc0,"TABLE POT.          ");
            break;
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_TIPO_INPUT;
        //break;    //Fall Through
    case CALIBRATION_WAIT_TIPO_INPUT:
        nA = CheckChangeCrossEnc();
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 >= 3)
                s_temp16 = 2;
            else if (s_temp16 < 1)
                s_temp16 = 1;
            g_ucCalibrationStatus = CALIBRATION_SHOW_TIPO_INPUT;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_INIT_DFF_FIXED;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_ConfigColl.ucInputDffPot = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_INIT_DFF_FIXED;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;



//  *****************************************************************************************************
    case CALIBRATION_INIT_DFF_FIXED:
        if (ee_ConfigColl.ucVertDff == VERT_DFF_FIXED){
            s_temp16 = ee_ConfigColl.uwFixedDff;
            g_ucCalibrationStatus = CALIBRATION_SHOW_DFF_FIXED;
        } else {
            g_ucCalibrationStatus = CALIBRATION_INIT_LAT_DFF_DX;
        }
        break;

    case CALIBRATION_SHOW_DFF_FIXED:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"DFF Verticale Fissa ");
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Vertical Fixed SID  ");
            break;
        }
        if (ee_ConfigColl.ucCmUnit){
            sys_print_msg    (0xc0,"     cm             ");
            sys_print_number (0xc0,s_temp16,3,0xff,false);
        } else {
            sys_print_msg (0xc0,"      pollici       ");
            sys_print_number (0xc0,cm_to_inches(s_temp16),4,3,false);
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_DFF_FIXED;
        //break;    //Fall Through
    case CALIBRATION_WAIT_DFF_FIXED:
        nA = EncoderCalib(&g_wCrossMagnitude, 2);
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 > DFF_MAX)
                s_temp16 = DFF_MAX;
            if (s_temp16 < DFF_MIN)
                s_temp16 = DFF_MIN;
            g_ucCalibrationStatus = CALIBRATION_SHOW_DFF_FIXED;
            break;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_INIT_LAT_DFF_DX;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_ConfigColl.uwFixedDff = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_INIT_LAT_DFF_DX;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;


//  *****************************************************************************************************
    case CALIBRATION_INIT_LAT_DFF_DX:
        s_temp16 = ee_ConfigColl.ucLatDffDx;

    case CALIBRATION_SHOW_LAT_DFF_DX:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"DFF Later. Destra   ");
            switch (s_temp16){
            case LAT_DFF_DISC:
                sys_print_msg (0xc0,"DISCRETA            ");
                break;
            case LAT_DFF_CAN:
                sys_print_msg (0xc0,"CAN                 ");
                break;
            case LAT_DFF_POT:
                sys_print_msg (0xc0,"POTENZIOMETRO       ");
                break;
            }
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Right Lateral SID   ");
            switch (s_temp16){
            case LAT_DFF_DISC:
                sys_print_msg (0xc0,"DISCRETE            ");
                break;
            case LAT_DFF_CAN:
                sys_print_msg (0xc0,"CAN                 ");
                break;
            case LAT_DFF_POT:
                sys_print_msg (0xc0,"POTENTIOMETER       ");
                break;
            }
            break;
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_LAT_DFF_DX;
        //break;    //Fall Through
    case CALIBRATION_WAIT_LAT_DFF_DX:
        nA = CheckChangeCrossEnc();
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 >= LAT_DFF_MAX)
                s_temp16 = LAT_DFF_MAX - 1;
            else if (s_temp16 < 0)
                s_temp16 = 0;
            g_ucCalibrationStatus = CALIBRATION_SHOW_LAT_DFF_DX;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_INIT_LAT_DFF_SX;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_ConfigColl.ucLatDffDx = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_INIT_LAT_DFF_SX;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;




//  *****************************************************************************************************
    case CALIBRATION_INIT_LAT_DFF_SX:
        s_temp16 = ee_ConfigColl.ucLatDffSx;

    case CALIBRATION_SHOW_LAT_DFF_SX:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"DFF Later. Sinistra ");
            switch (s_temp16){
            case LAT_DFF_DISC:
                sys_print_msg (0xc0,"DISCRETA            ");
                break;
            case LAT_DFF_CAN:
                sys_print_msg (0xc0,"CAN                 ");
                break;
            case LAT_DFF_POT:
                sys_print_msg (0xc0,"POTENZIOMETRO       ");
                break;
            }
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Left Lateral SID    ");
            switch (s_temp16){
            case LAT_DFF_DISC:
                sys_print_msg (0xc0,"DISCRETE            ");
                break;
            case LAT_DFF_CAN:
                sys_print_msg (0xc0,"CAN                 ");
                break;
            case LAT_DFF_POT:
                sys_print_msg (0xc0,"POTENTIOMETER       ");
                break;
            }
            break;
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_LAT_DFF_SX;
        //break;    //Fall Through
    case CALIBRATION_WAIT_LAT_DFF_SX:
        nA = CheckChangeCrossEnc();
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 >= LAT_DFF_MAX)
                s_temp16 = LAT_DFF_MAX - 1;
            else if (s_temp16 < 0)
                s_temp16 = 0;
            g_ucCalibrationStatus = CALIBRATION_SHOW_LAT_DFF_SX;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_INIT_REC_VERT;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_ConfigColl.ucLatDffSx = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_INIT_REC_VERT;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;


//  *****************************************************************************************************
    case CALIBRATION_INIT_REC_VERT:
        s_temp16 = ee_ConfigColl.ucVertRec;

    case CALIBRATION_SHOW_REC_VERT:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Receptor Verticale  ");
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Vertical Receptor   ");
            break;
        }

        switch (s_temp16){
        case VERT_REC_NO:
            sys_print_msg (0xc0,"NO                  ");
            break;
        case VERT_REC_BUCKY:
            sys_print_msg (0xc0,"BUCKY               ");
            break;
        case VERT_REC_CAN:
            sys_print_msg (0xc0,"CAN                 ");
            break;
        case VERT_REC_ATS:
            sys_print_msg (0xc0,"ATS                 ");
            break;
        case VERT_REC_FISSI:
            sys_print_msg (0xc0,"FIXED FORMATS       ");
            break;
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_REC_VERT;
        //break;    //Fall Through
    case CALIBRATION_WAIT_REC_VERT:
        nA = CheckChangeCrossEnc();
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 >= VERT_REC_MAX)
                s_temp16 = VERT_REC_MAX - 1;
            else if (s_temp16 < 0)
                s_temp16 = 0;
            g_ucCalibrationStatus = CALIBRATION_SHOW_REC_VERT;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_END_REC_VERT;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_ConfigColl.ucVertRec = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_END_REC_VERT;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;
    case CALIBRATION_END_REC_VERT:



//  *****************************************************************************************************
    case CALIBRATION_INIT_FORMAT:
        if (ee_ConfigColl.ucVertRec != VERT_REC_FISSI)
        {
            g_ucCalibrationStatus = CALIBRATION_END_FORMAT;
        }
        else
        {
            s_ucSetCount = 0;
            g_ucCalibrationStatus = CALIBRATION_LOAD_FORMAT;
        }
        break;

    case CALIBRATION_LOAD_FORMAT:
        if (ee_ConfigColl.ucCmUnit){
            s_temp16 = ee_FixFormatColl.ucFixedCmCross[s_ucSetCount];
            s_temp16a = ee_FixFormatColl.ucFixedCmLong[s_ucSetCount];
        }
        else{
            s_temp16 = ee_FixFormatColl.ucFixedInCross[s_ucSetCount];
            s_temp16a = ee_FixFormatColl.ucFixedInLong[s_ucSetCount];
        }
        g_ucCalibrationStatus = CALIBRATION_SHOW_FORMAT;
        break;

    case CALIBRATION_SHOW_FORMAT:
        if (ee_ConfigColl.ucLanguage == ITA_LANG){
            sys_print_msg (0x80,"Formato Digitale #. ");
        }
        if (ee_ConfigColl.ucLanguage == ENG_LANG) {
            sys_print_msg (0x80,"Digital Format   #. ");
        }
        sys_lcd_mode (0x92);
        sys_lcd_out ('1'+ s_ucSetCount);

        if (ee_ConfigColl.ucCmUnit){
            sys_print_msg (0xc0,"   x    cm          ");
            sys_print_number(0xc1,s_temp16,2,0xff,false);
            sys_print_number(0xc4,s_temp16a,2,0xff,false);
        }
        else{
            if (ee_ConfigColl.ucLanguage == ITA_LANG)
                sys_print_msg (0xc0,"   x     pollici    ");
            if (ee_ConfigColl.ucLanguage == ENG_LANG)
                sys_print_msg (0xc0,"   x     inches     ");
            sys_print_number(0xc1,s_temp16,2,0xff,false);
            sys_print_number(0xc4,s_temp16a,2,0xff,false);
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_FORMAT;
        //break;    //Fall Through
    case CALIBRATION_WAIT_FORMAT:
        nA = EncoderCalib(&g_wCrossMagnitude, 2);
        if (nA)
        {
            s_temp16 += nA;
            if (ee_ConfigColl.ucCmUnit)
            {
                if (s_temp16 > 43)
                    s_temp16 = 43;
                if (s_temp16 < 0)
                    s_temp16 = 0;
            }
            else
            {
                if (s_temp16 > 17)
                    s_temp16 = 17;
                if (s_temp16 < 0)
                    s_temp16 = 0;
            }
            g_ucCalibrationStatus = CALIBRATION_SHOW_FORMAT;
            break;
        }
        nA = EncoderCalib(&g_wLongMagnitude, 2);
        if (nA)
        {
            s_temp16a += nA;
            if (ee_ConfigColl.ucCmUnit)
            {
                if (s_temp16a > 43)
                    s_temp16a = 43;
                if (s_temp16a < 0)
                    s_temp16a = 0;
            }
            else
            {
                if (s_temp16a > 17)
                    s_temp16a = 17;
                if (s_temp16a < 0)
                    s_temp16a = 0;
            }
            g_ucCalibrationStatus = CALIBRATION_SHOW_FORMAT;
            break;
        }
        if (FILTER_SWITCH){
            if (++s_ucSetCount <= 4) {
                g_ucCalibrationStatus = CALIBRATION_LOAD_FORMAT;
            }
            else {
                g_ucCalibrationStatus = CALIBRATION_END_FORMAT;
                s_ulTempUpdateEeprom |= EEP_UPDATE_FORMAT;
            }
            wait_key_filter_up(250);
            break;
        }
        if (LAMP_SWITCH){
            if (ee_ConfigColl.ucCmUnit){
                ee_FixFormatColl.ucFixedCmCross[s_ucSetCount] = s_temp16;
                ee_FixFormatColl.ucFixedCmLong[s_ucSetCount] = s_temp16a;
            }
            else{
                ee_FixFormatColl.ucFixedInCross[s_ucSetCount] = s_temp16;
                ee_FixFormatColl.ucFixedInLong[s_ucSetCount] = s_temp16a;
            }
            if (++s_ucSetCount <= 4) {
                g_ucCalibrationStatus = CALIBRATION_LOAD_FORMAT;
            }
            else {
                g_ucCalibrationStatus = CALIBRATION_END_FORMAT;
            }
            s_ulTempUpdateEeprom |= EEP_UPDATE_FORMAT;
            wait_key_exit_up(250);
            break;
        }
        break;
    case CALIBRATION_END_FORMAT:
        s_ucSetCount=0;
        g_wCrossMagnitude = 0;
        g_wLongMagnitude = 0;



// *****************************************************************************************************
    case CALIBRATION_INIT_FORMAT_IRIS:
        if ((ee_ConfigColl.ucVertRec != VERT_REC_FISSI) || ( ee_ConfigColl.ucIrisPresent == FALSE))
        {
            g_ucCalibrationStatus = CALIBRATION_END_FORMAT_IRIS;
        }
        else
        {
            s_ucSetCount = 0;
            g_ucCalibrationStatus = CALIBRATION_LOAD_FORMAT_IRIS;
        }
        break;

    case CALIBRATION_LOAD_FORMAT_IRIS:
        if (ee_ConfigColl.ucCmUnit){
            s_temp16 = ee_FixFormatColl.ucFixedCmIris[s_ucSetCount];
        }
        else{
            s_temp16 = ee_FixFormatColl.ucFixedInIris[s_ucSetCount];
        }
        g_ucCalibrationStatus = CALIBRATION_SHOW_FORMAT_IRIS;
        break;

    case CALIBRATION_SHOW_FORMAT_IRIS:
        if (ee_ConfigColl.ucLanguage == ITA_LANG) {
            sys_print_msg (0x80,"Form. Dig. Iride #. ");
        }
        if (ee_ConfigColl.ucLanguage == ENG_LANG) {
            sys_print_msg (0x80,"Iris Dig. Format #. ");
        }
        sys_lcd_mode (0x92);
        sys_lcd_out ('1'+ s_ucSetCount);

        if (ee_ConfigColl.ucCmUnit){
            sys_print_msg (0xc0,"    cm              ");
            sys_print_number(0xc1,s_temp16,2,0xff,false);
        }
        else{
            if (ee_ConfigColl.ucLanguage == ITA_LANG)
                sys_print_msg (0xc0,"    pollici         ");
            if (ee_ConfigColl.ucLanguage == ENG_LANG)
                sys_print_msg (0xc0,"    inches          ");
            sys_print_number(0xc1,s_temp16,2,0xff,false);
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_FORMAT_IRIS;
        //break;    //Fall Through
    case CALIBRATION_WAIT_FORMAT_IRIS:
        nA = EncoderCalib(&g_wCrossMagnitude, 2);
        if (nA)
        {
            s_temp16 += nA;
            if (ee_ConfigColl.ucCmUnit)
            {
                if (s_temp16 > 43)
                    s_temp16 = 43;
                if (s_temp16 < 0)
                    s_temp16 = 0;
            }
            else
            {
                if (s_temp16 > 17)
                    s_temp16 = 17;
                if (s_temp16 < 0)
                    s_temp16 = 0;
            }
            g_ucCalibrationStatus = CALIBRATION_SHOW_FORMAT_IRIS;
            break;
        }
        if (FILTER_SWITCH){
            if (++s_ucSetCount <= 4) {
                g_ucCalibrationStatus = CALIBRATION_LOAD_FORMAT_IRIS;
            }
            else {
                s_ucSetCount=0;
                g_ucCalibrationStatus = CALIBRATION_END_FORMAT_IRIS;
                s_ulTempUpdateEeprom |= EEP_UPDATE_FORMAT;
            }
            wait_key_filter_up(250);
            break;
        }
        if (LAMP_SWITCH){
            if (ee_ConfigColl.ucCmUnit){
                ee_FixFormatColl.ucFixedCmIris[s_ucSetCount] = s_temp16;
            }
            else{
                ee_FixFormatColl.ucFixedInIris[s_ucSetCount] = s_temp16;
            }
            if (++s_ucSetCount <= 4) {
                g_ucCalibrationStatus = CALIBRATION_LOAD_FORMAT_IRIS;
            }
            else {
                s_ucSetCount=0;
                g_ucCalibrationStatus = CALIBRATION_END_FORMAT_IRIS;
            }
            s_ulTempUpdateEeprom |= EEP_UPDATE_FORMAT;
            wait_key_exit_up(250);
            break;
        }
        break;
    case CALIBRATION_END_FORMAT_IRIS:
        g_wCrossMagnitude = 0;
        g_wLongMagnitude = 0;




//  *****************************************************************************************************
    case CALIBRATION_INIT_REC_DX:
        s_temp16 = ee_ConfigColl.ucLatRecDx;

    case CALIBRATION_SHOW_REC_DX:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Receptor Lat. Dx    ");
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Right Lat. Recep.   ");
            break;
        }

        switch (s_temp16){
        case LAT_REC_NO:
            sys_print_msg (0xC0,"NO                 ");
            break;
        case LAT_REC_BUCKY:
            sys_print_msg (0xC0,"BUCKY               ");
            break;
        case LAT_REC_CAN:
            sys_print_msg (0xC0,"CAN                 ");
            break;
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_REC_DX;
        //break;    //Fall Through
    case CALIBRATION_WAIT_REC_DX:
        nA = CheckChangeCrossEnc();
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 >= LAT_REC_MAX)
                s_temp16 = LAT_REC_MAX - 1;
            else if (s_temp16 < 0)
                s_temp16 = 0;
            g_ucCalibrationStatus = CALIBRATION_SHOW_REC_DX;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_INIT_REC_SX;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_ConfigColl.ucLatRecDx = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_INIT_REC_SX;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;


//  *****************************************************************************************************
    case CALIBRATION_INIT_REC_SX:
        s_temp16 = ee_ConfigColl.ucLatRecSx;

    case CALIBRATION_SHOW_REC_SX:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Receptor Lat. Sx    ");
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Left Lat. Recep.    ");
            break;
        }

        switch (s_temp16){
        case LAT_REC_NO:
            sys_print_msg (0xC0,"NO                 ");
            break;
        case LAT_REC_BUCKY:
            sys_print_msg (0xC0,"BUCKY               ");
            break;
        case LAT_REC_CAN:
            sys_print_msg (0xC0,"CAN                 ");
            break;
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_REC_SX;
        //break;    //Fall Through
    case CALIBRATION_WAIT_REC_SX:
        nA = CheckChangeCrossEnc();
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 >= LAT_REC_MAX)
                s_temp16 = LAT_REC_MAX - 1;
            else if (s_temp16 < 0)
                s_temp16 = 0;
            g_ucCalibrationStatus = CALIBRATION_SHOW_REC_SX;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_END_VERT_REC1;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_ConfigColl.ucLatRecSx = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_END_VERT_REC1;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;
    case CALIBRATION_END_VERT_REC1:


//  *****************************************************************************************************
    case CALIBRATION_INIT_DFT_VERT:
        if (ee_ConfigColl.ucVertRec == VERT_REC_BUCKY)
        {
            s_temp16 = ee_ConfigColl.ucDftVert;
            g_ucCalibrationStatus = CALIBRATION_SHOW_DFT_VERT;
        }
        else
        {
            g_ucCalibrationStatus = CALIBRATION_END_DFT_VERT;
        }
        break;

    case CALIBRATION_SHOW_DFT_VERT:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Dist. Film Tavolo   ");
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Film Table Distance ");
            break;
        }
        if (ee_ConfigColl.ucCmUnit){
            sys_print_msg    (0xc0,"     cm             ");
            sys_print_number (0xc0,s_temp16,3,0xff,false);
        } else {
            sys_print_msg (0xc0,"      pollici       ");
            sys_print_number (0xc0,cm_to_inches(s_temp16),4,3,false);
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_DFT_VERT;
        //break;    //Fall Through
    case CALIBRATION_WAIT_DFT_VERT:
        nA = EncoderCalib(&g_wCrossMagnitude, 2);
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 > DFT_MAX)
                s_temp16 = DFT_MAX;
            if (s_temp16 < 0)
                s_temp16 = 0;
            g_ucCalibrationStatus = CALIBRATION_SHOW_DFT_VERT;
            break;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_END_DFT_VERT;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_ConfigColl.ucDftVert = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_END_DFT_VERT;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;
    case CALIBRATION_END_DFT_VERT:


//  *****************************************************************************************************
    case CALIBRATION_INIT_DFT_SX:
        if (ee_ConfigColl.ucLatRecSx == LAT_REC_BUCKY)
        {
            s_temp16 = ee_ConfigColl.ucDftSx;
            g_ucCalibrationStatus = CALIBRATION_SHOW_DFT_SX;
        }
        else
        {
            g_ucCalibrationStatus = CALIBRATION_END_DFT_SX;
        }
        break;

    case CALIBRATION_SHOW_DFT_SX:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Dist. Film Piano SX ");
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Left Film-Plane Dist");
            break;
        }
        if (ee_ConfigColl.ucCmUnit){
            sys_print_msg    (0xc0,"     cm             ");
            sys_print_number (0xc0,s_temp16,3,0xff,false);
        } else {
            sys_print_msg (0xc0,"      pollici       ");
            sys_print_number (0xc0,cm_to_inches(s_temp16),4,3,false);
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_DFT_SX;
        //break;    //Fall Through
    case CALIBRATION_WAIT_DFT_SX:
        nA = EncoderCalib(&g_wCrossMagnitude, 2);
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 > DFT_MAX)
                s_temp16 = DFT_MAX;
            if (s_temp16 < 0)
                s_temp16 = 0;
            g_ucCalibrationStatus = CALIBRATION_SHOW_DFT_SX;
            break;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_END_DFT_SX;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_ConfigColl.ucDftSx = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_END_DFT_SX;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;
    case CALIBRATION_END_DFT_SX:


//  *****************************************************************************************************
    case CALIBRATION_INIT_DFT_DX:
        if (ee_ConfigColl.ucLatRecDx == LAT_REC_BUCKY)
        {
            s_temp16 = ee_ConfigColl.ucDftDx;
            g_ucCalibrationStatus = CALIBRATION_SHOW_DFT_DX;
        }
        else
        {
            g_ucCalibrationStatus = CALIBRATION_END_DFT_DX;
        }
        break;

    case CALIBRATION_SHOW_DFT_DX:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Dist. Film Piano DX ");
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Right FilmPlane Dist");
            break;
        }
        if (ee_ConfigColl.ucCmUnit){
            sys_print_msg    (0xc0,"     cm             ");
            sys_print_number (0xc0,s_temp16,3,0xff,false);
        } else {
            sys_print_msg (0xc0,"      pollici       ");
            sys_print_number (0xc0,cm_to_inches(s_temp16),4,3,false);
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_DFT_DX;
        //break;    //Fall Through
    case CALIBRATION_WAIT_DFT_DX:
        nA = EncoderCalib(&g_wCrossMagnitude, 2);
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 > DFT_MAX)
                s_temp16 = DFT_MAX;
            if (s_temp16 < 0)
                s_temp16 = 0;
            g_ucCalibrationStatus = CALIBRATION_SHOW_DFT_DX;
            break;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_END_DFT_DX;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_ConfigColl.ucDftDx = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_END_DFT_DX;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;
    case CALIBRATION_END_DFT_DX:


//  *****************************************************************************************************
    case CALIBRATION_INIT_VERT_REC1:
        if (ee_ConfigColl.ucVertDff == VERT_DFF_DIFF){
            g_ucCalibrationStatus = CALIBRATION_SHOW_VERT_REC1;
            break;
        }
        if ((ee_ConfigColl.ucVertDff == VERT_DFF_SINGLE) &&
            (ee_ConfigColl.ucInputDffPot == POT_SINGLE_TABLE))
        {
            g_ucCalibrationStatus = CALIBRATION_SHOW_VERT_REC1;
            break;
        }
        g_ucCalibrationStatus = CALIBRATION_INIT_VERT_REC2;
        break;

    case CALIBRATION_SHOW_VERT_REC1:
        if (ee_ConfigColl.ucLanguage == ITA_LANG){
            sys_print_msg (0x80,"Tavolo tutto basso  ");
        }
        if (ee_ConfigColl.ucLanguage == ENG_LANG){
            sys_print_msg (0x80,"Move Table Down     ");
        }

        sys_print_msg(0xc0,"     pts            ");
        g_ucCalibrationStatus = CALIBRATION_WAIT_VERT_REC1;
        //break;    //Fall Through

    case CALIBRATION_WAIT_VERT_REC1:
        sys_print_number(0xc0,RemoteAdc[DFF_TAB],4,0xff,false);
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_INIT_VERT_REC2;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_CalColl.uwLowTable = RemoteAdc[DFF_TAB];
            s_ulTempUpdateEeprom |= EEP_UPDATE_CALIB;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_INIT_VERT_REC2;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;

//  *****************************************************************************************************
    case CALIBRATION_INIT_VERT_REC2:
        if (ee_ConfigColl.ucVertDff == VERT_DFF_DIFF){
            g_ucCalibrationStatus = CALIBRATION_SHOW_VERT_REC2;
            break;
        }
        if ((ee_ConfigColl.ucVertDff == VERT_DFF_SINGLE) &&
            (ee_ConfigColl.ucInputDffPot == POT_SINGLE_TABLE))
        {
            g_ucCalibrationStatus = CALIBRATION_SHOW_VERT_REC2;
            break;
        }
        g_ucCalibrationStatus = CALIBRATION_INIT_VERT_REC3;
        break;

    case CALIBRATION_SHOW_VERT_REC2:
        if (ee_ConfigColl.ucLanguage == ITA_LANG){
            if (ee_ConfigColl.ucCmUnit)
                sys_print_msg (0x80,"Alzare tavolo +25cm ");
            else
                sys_print_msg (0x80,"Alzare tavolo +9.8\" ");
        }
        if (ee_ConfigColl.ucLanguage == ENG_LANG){
            if (ee_ConfigColl.ucCmUnit)
                sys_print_msg (0x80,"Set Table at +25 cm ");
            else
                sys_print_msg (0x80,"Set Table at +9.8\"  ");
        }
        sys_print_msg(0xc0,"     pts            ");
        g_ucCalibrationStatus = CALIBRATION_WAIT_VERT_REC2;
        //break;    //Fall Through

    case CALIBRATION_WAIT_VERT_REC2:
        sys_print_number(0xc0,RemoteAdc[DFF_TAB],4,0xff,false);

        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_INIT_VERT_COLL;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            if (RemoteAdc[DFF_TAB] > (ee_CalColl.uwLowTable + 30))     // controllo della coerenza dei valori
            {
                ee_CalColl.uwTable25 = RemoteAdc[DFF_TAB];
                s_ulTempUpdateEeprom |= EEP_UPDATE_CALIB;
                wait_key_exit_up(250);
                g_ucCalibrationStatus = CALIBRATION_INIT_VERT_COLL;
                g_wCrossMagnitude = 0;
                g_wLongMagnitude = 0;
            }
            else
            {
                if (ee_ConfigColl.ucLanguage == ITA_LANG){
                    sys_print_msg(0xC0,"Errore Valori POT.  ");
                }
                if (ee_ConfigColl.ucLanguage == ENG_LANG){
                    sys_print_msg(0xC0,"POT. Value Error!   ");
                }
                timer_alarm_calib = 30;
                g_ucCalibrationStatus = CALIBRATION_ERR_VERT_REC2;
            }
        }
        break;

    case CALIBRATION_ERR_VERT_REC2:
        if (timer_alarm_calib == 0)
        {
            g_ucCalibrationStatus = CALIBRATION_INIT_VERT_REC1;
        }
        break;




//  *****************************************************************************************************
    case CALIBRATION_INIT_VERT_COLL:
        if ((ee_ConfigColl.ucVertDff == VERT_DFF_SINGLE) &&
            (ee_ConfigColl.ucInputDffPot == POT_SINGLE_TABLE))
        {
            s_temp16 = ee_CalColl.uwCollTable;
            g_ucCalibrationStatus = CALIBRATION_SHOW_VERT_COLL;
            break;
        }
        g_ucCalibrationStatus = CALIBRATION_INIT_VERT_REC3;
        break;

    case CALIBRATION_SHOW_VERT_COLL:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Distanza FuocoTavolo");
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Focus-Table Distance");
            break;
        }
        if (ee_ConfigColl.ucCmUnit){
            sys_print_msg    (0xc0,"     cm             ");
            sys_print_number (0xc0,s_temp16,3,0xff,false);
        } else {
            sys_print_msg (0xc0,"      pollici       ");
            sys_print_number (0xc0,cm_to_inches(s_temp16),4,3,false);
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_VERT_COLL;
        //break;    //Fall Through
    case CALIBRATION_WAIT_VERT_COLL:
        nA = EncoderCalib(&g_wCrossMagnitude, 2);
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 > DFF_MAX)
                s_temp16 = DFF_MAX;
            if (s_temp16 < DFF_MIN)
                s_temp16 = DFF_MIN;
            g_ucCalibrationStatus = CALIBRATION_SHOW_VERT_COLL;
            break;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_INIT_VERT_REC3;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_CalColl.uwCollTable = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_CALIB;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_INIT_VERT_REC3;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;


//  *****************************************************************************************************
    case CALIBRATION_INIT_VERT_REC3:
        if (ee_ConfigColl.ucVertDff == VERT_DFF_DIFF){
            g_ucCalibrationStatus = CALIBRATION_SHOW_VERT_REC3;
            break;
        }
        if ((ee_ConfigColl.ucVertDff == VERT_DFF_SINGLE) &&
            (ee_ConfigColl.ucInputDffPot == POT_SINGLE_STAND))
        {
            g_ucCalibrationStatus = CALIBRATION_SHOW_VERT_REC3;
            break;
        }
        g_ucCalibrationStatus = CALIBRATION_SHOW_VERT_REC4;
        break;

    case CALIBRATION_SHOW_VERT_REC3:
        if (ee_ConfigColl.ucLanguage == ITA_LANG){
            sys_print_msg (0x80,"Stativo tutto alto  ");
        }
        if (ee_ConfigColl.ucLanguage == ENG_LANG){
            sys_print_msg (0x80,"Move Stand Full Up  ");
        }

        sys_print_msg(0xc0,"     pts            ");
        g_ucCalibrationStatus = CALIBRATION_WAIT_VERT_REC3;
        //break;    //Fall Through

    case CALIBRATION_WAIT_VERT_REC3:
        sys_print_number(0xc0,RemoteAdc[DFF_STAT],4,0xff,false);

        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_SHOW_VERT_REC4;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_CalColl.uwStandUp = RemoteAdc[DFF_STAT];
            s_ulTempUpdateEeprom |= EEP_UPDATE_CALIB;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_SHOW_VERT_REC4;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;

//  *****************************************************************************************************
    case CALIBRATION_SHOW_VERT_REC4:
        while (true){
            if (ee_ConfigColl.ucVertDff == VERT_DFF_DIFF)
                break;
            if ((ee_ConfigColl.ucVertDff == VERT_DFF_SINGLE) &&
                (ee_ConfigColl.ucInputDffPot == POT_SINGLE_STAND))
                break;
            g_ucCalibrationStatus = CALIBRATION_SHOW_VERT_REC5;
            return;
        }

        if (ee_ConfigColl.ucLanguage == ITA_LANG){
            if (ee_ConfigColl.ucCmUnit)
                sys_print_msg (0x80,"Abbassare di 50 cm  ");
            else
                sys_print_msg (0x80,"Abbassare di 19.7\"  ");
        }
        if (ee_ConfigColl.ucLanguage == ENG_LANG){
            if (ee_ConfigColl.ucCmUnit)
                sys_print_msg (0x80,"Lower 50 cm         ");
            else
                sys_print_msg (0x80,"Lower 19.7\"         ");
        }

        sys_print_msg(0xc0,"     pts            ");
        g_ucCalibrationStatus = CALIBRATION_WAIT_VERT_REC4;
        //break;    //Fall Through

    case CALIBRATION_WAIT_VERT_REC4:
        sys_print_number(0xc0,RemoteAdc[DFF_STAT],4,0xff,false);

        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_SHOW_VERT_REC5;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            if (RemoteAdc[DFF_STAT] < (ee_CalColl.uwStandUp - 20))     // controllo della coerenza dei valori
            {
                ee_CalColl.uwStandLo50 = RemoteAdc[DFF_STAT];
                s_ulTempUpdateEeprom |= EEP_UPDATE_CALIB;
                wait_key_exit_up(250);
                g_ucCalibrationStatus = CALIBRATION_SHOW_VERT_REC5;
                g_wCrossMagnitude = 0;
                g_wLongMagnitude = 0;
            }
            else
            {
                if (ee_ConfigColl.ucLanguage == ITA_LANG){
                    sys_print_msg(0xC0,"Errore Valori POT.  ");
                }
                if (ee_ConfigColl.ucLanguage == ENG_LANG){
                    sys_print_msg(0xC0,"POT. Value Error!   ");
                }
                timer_alarm_calib = 30;
                g_ucCalibrationStatus = CALIBRATION_ERR_VERT_REC4;
            }
        }
        break;

    case CALIBRATION_ERR_VERT_REC4:
        if (timer_alarm_calib == 0)
        {
            g_ucCalibrationStatus = CALIBRATION_INIT_VERT_REC3;
        }
        break;


//  *****************************************************************************************************
    case CALIBRATION_SHOW_VERT_REC5:
        while (true){
            if (ee_ConfigColl.ucVertDff == VERT_DFF_DIFF)
                break;
            if ((ee_ConfigColl.ucVertDff == VERT_DFF_SINGLE) &&
                (ee_ConfigColl.ucInputDffPot == POT_SINGLE_STAND))
                break;
            g_ucCalibrationStatus = CALIBRATION_INIT_DFF_TYPE;
            return;
        }

        if (ee_ConfigColl.ucLanguage == ITA_LANG){
            if (ee_ConfigColl.ucCmUnit)
                sys_print_msg (0x80,"Posizionare a 100cm ");
            else
                sys_print_msg (0x80,"Posizionare a 39.4\" ");
            sys_print_msg(0xc0,"FuocoTavolo      pts");
        }
        if (ee_ConfigColl.ucLanguage == ENG_LANG){
            if (ee_ConfigColl.ucCmUnit)
                sys_print_msg (0x80,"Set stand to 100 cm ");
            else
                sys_print_msg (0x80,"Set stand to 39.4\"  ");
            sys_print_msg(0xc0,"Focus-Table      pts");
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_VERT_REC5;
        //break;    //Fall Through

    case CALIBRATION_WAIT_VERT_REC5:
        sys_print_number(0xcc,RemoteAdc[DFF_STAT],4,0xff,false);
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_INIT_DFF_TYPE;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_CalColl.uwStand100 = RemoteAdc[DFF_STAT];
            s_ulTempUpdateEeprom |= EEP_UPDATE_CALIB;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_INIT_DFF_TYPE;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;

//  *****************************************************************************************************
    case CALIBRATION_INIT_DFF_TYPE:
        if ((ee_ConfigColl.ucLatDffSx != LAT_DFF_DISC) &&
           (ee_ConfigColl.ucLatDffDx != LAT_DFF_DISC)){
            g_ucCalibrationStatus = CALIBRATION_END_DFF_TYPE;
            return;
        }
        while (true){
            if (ee_ConfigColl.ucLatRecSx == LAT_REC_BUCKY)
                break;
            if (ee_ConfigColl.ucLatRecDx == LAT_REC_BUCKY)
                break;
            if (ee_ConfigColl.ucLatRecSx == LAT_REC_CAN)
                break;
            if (ee_ConfigColl.ucLatRecDx == LAT_REC_CAN)
                break;
            g_ucCalibrationStatus = CALIBRATION_END_DFF_TYPE;
            return;
        }
        s_ucSetCount=0;
        g_ucCalibrationStatus = CALIBRATION_LOAD_DFF_TYPE;
        break;

    case CALIBRATION_LOAD_DFF_TYPE:
        s_temp16 = ee_CalColl.uwDffHor[s_ucSetCount];
        g_ucCalibrationStatus = CALIBRATION_SHOW_DFF_TYPE;
//        break;

    case CALIBRATION_SHOW_DFF_TYPE:
        if (ee_ConfigColl.ucLanguage == ITA_LANG){
            if (s_ucSetCount < 5)
                sys_print_msg (0x80,"Misura DFF Oriz.#.  ");
            else
                sys_print_msg (0x80,"DFF panoramica  #.  ");
        }
        if (ee_ConfigColl.ucLanguage == ENG_LANG){
            if (s_ucSetCount < 5)
                sys_print_msg (0x80,"Horiz.SID Meas. #.  ");
            else
                sys_print_msg (0x80,"Panoramic  SID  #.  ");
        }
        sys_lcd_mode (0x91);
        sys_lcd_out ('1'+(s_ucSetCount % 5));

        if (ee_ConfigColl.ucCmUnit){
            sys_print_msg (0xc0,"    cm              ");
            sys_print_number(0xc0,s_temp16,3,0xff,false);
        }
        else{
            if (ee_ConfigColl.ucLanguage == ITA_LANG)
                sys_print_msg (0xc0,"     pollici        ");
            if (ee_ConfigColl.ucLanguage == ENG_LANG)
                sys_print_msg (0xc0,"     inches         ");
            sys_print_number(0xc0,cm_to_inches(s_temp16),3,2,false);
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_DFF_TYPE;
        //break;    //Fall Through
    case CALIBRATION_WAIT_DFF_TYPE:
        nA = EncoderCalib(&g_wCrossMagnitude, 2);
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 > DFF_MAX)
                s_temp16 = DFF_MAX;
            if (s_temp16 < 25)
                s_temp16 = 25;
            g_ucCalibrationStatus = CALIBRATION_SHOW_DFF_TYPE;
            break;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(250);
            if (++s_ucSetCount < 7)
                g_ucCalibrationStatus = CALIBRATION_LOAD_DFF_TYPE;
            else{
                s_ulTempUpdateEeprom |= EEP_UPDATE_CALIB;
                g_ucCalibrationStatus = CALIBRATION_END_DFF_TYPE;
            }
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_CalColl.uwDffHor[s_ucSetCount] = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_CALIB;
            wait_key_exit_up(250);
            if (++s_ucSetCount < 7)
                g_ucCalibrationStatus = CALIBRATION_LOAD_DFF_TYPE;
            else{
                s_ucSetCount=0;
                g_ucCalibrationStatus = CALIBRATION_END_DFF_TYPE;
            }
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;
    case CALIBRATION_END_DFF_TYPE:



//  *****************************************************************************************************
    case CALIBRATION_INIT_POT_DX_MIN:
        if (ee_ConfigColl.ucLatDffDx == LAT_DFF_POT)
        {
            s_temp16 = ee_CalColl.DffDxMisMin;
            g_ucCalibrationStatus = CALIBRATION_SHOW_POT_DX_MIN;
            break;
        }
        g_ucCalibrationStatus = CALIBRATION_END_POT_DX_MIN;
        break;

    case CALIBRATION_SHOW_POT_DX_MIN:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Pot. DFF DX min:    ");
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Min.RightPotSID:    ");
            break;
        }
        if (ee_ConfigColl.ucCmUnit){
            sys_print_msg    (0xc0,"     cm             ");
            sys_print_number (0xc0,s_temp16,3,0xff,false);
        } else {
            sys_print_msg (0xc0,"      pollici       ");
            sys_print_number (0xc0,cm_to_inches(s_temp16),4,3,false);
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_POT_DX_MIN;
        //break;    //Fall Through
    case CALIBRATION_WAIT_POT_DX_MIN:
        sys_print_number(0x90,RemoteAdc[DFF_STAT],4,0xff,false);
        nA = EncoderCalib(&g_wCrossMagnitude, 2);
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 > DFF_MAX)
                s_temp16 = DFF_MAX;
            if (s_temp16 < DFF_MIN)
                s_temp16 = DFF_MIN;
            g_ucCalibrationStatus = CALIBRATION_SHOW_POT_DX_MIN;
            break;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_END_POT_DX_MIN;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_CalColl.DffDxPotMin = RemoteAdc[DFF_STAT];
            ee_CalColl.DffDxMisMin = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_CALIB;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_END_POT_DX_MIN;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;
    case CALIBRATION_END_POT_DX_MIN:



//  *****************************************************************************************************
    case CALIBRATION_INIT_POT_DX_MAX:
        if (ee_ConfigColl.ucLatDffDx == LAT_DFF_POT)
        {
            s_temp16 = ee_CalColl.DffDxMisMax;
            g_ucCalibrationStatus = CALIBRATION_SHOW_POT_DX_MAX;
            break;
        }
        g_ucCalibrationStatus = CALIBRATION_END_POT_DX_MAX;
        break;

    case CALIBRATION_SHOW_POT_DX_MAX:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Pot. DFF DX max:    ");
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Max.RightPotSID:    ");
            break;
        }
        if (ee_ConfigColl.ucCmUnit){
            sys_print_msg    (0xc0,"     cm             ");
            sys_print_number (0xc0,s_temp16,3,0xff,false);
        } else {
            sys_print_msg (0xc0,"      pollici       ");
            sys_print_number (0xc0,cm_to_inches(s_temp16),4,3,false);
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_POT_DX_MAX;
        //break;    //Fall Through
    case CALIBRATION_WAIT_POT_DX_MAX:
        sys_print_number(0x90,RemoteAdc[DFF_STAT],4,0xff,false);
        nA = EncoderCalib(&g_wCrossMagnitude, 2);
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 > DFF_MAX)
                s_temp16 = DFF_MAX;
            if (s_temp16 < DFF_MIN)
                s_temp16 = DFF_MIN;
            g_ucCalibrationStatus = CALIBRATION_SHOW_POT_DX_MAX;
            break;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_END_POT_DX_MAX;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_CalColl.DffDxPotMax = RemoteAdc[DFF_STAT];
            ee_CalColl.DffDxMisMax = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_CALIB;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_END_POT_DX_MAX;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;
    case CALIBRATION_END_POT_DX_MAX:




//  *****************************************************************************************************
    case CALIBRATION_INIT_POT_SX_MIN:
        if (ee_ConfigColl.ucLatDffSx == LAT_DFF_POT)
        {
            s_temp16 = ee_CalColl.DffSxMisMin;
            g_ucCalibrationStatus = CALIBRATION_SHOW_POT_SX_MIN;
            break;
        }
        g_ucCalibrationStatus = CALIBRATION_END_POT_SX_MIN;
        break;

    case CALIBRATION_SHOW_POT_SX_MIN:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Pot. DFF SX min:    ");
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Min. LeftPotSID:    ");
            break;
        }
        if (ee_ConfigColl.ucCmUnit){
            sys_print_msg    (0xc0,"     cm             ");
            sys_print_number (0xc0,s_temp16,3,0xff,false);
        } else {
            sys_print_msg (0xc0,"      pollici       ");
            sys_print_number (0xc0,cm_to_inches(s_temp16),4,3,false);
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_POT_SX_MIN;
        //break;    //Fall Through
    case CALIBRATION_WAIT_POT_SX_MIN:
        sys_print_number(0x90,RemoteAdc[DFF_STAT],4,0xff,false);
        nA = EncoderCalib(&g_wCrossMagnitude, 2);
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 > DFF_MAX)
                s_temp16 = DFF_MAX;
            if (s_temp16 < DFF_MIN)
                s_temp16 = DFF_MIN;
            g_ucCalibrationStatus = CALIBRATION_SHOW_POT_SX_MIN;
            break;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_END_POT_SX_MIN;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_CalColl.DffSxPotMin = RemoteAdc[DFF_STAT];
            ee_CalColl.DffSxMisMin = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_CALIB;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_END_POT_SX_MIN;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;
    case CALIBRATION_END_POT_SX_MIN:


//  *****************************************************************************************************
    case CALIBRATION_INIT_POT_SX_MAX:
        if (ee_ConfigColl.ucLatDffSx == LAT_DFF_POT)
        {
            s_temp16 = ee_CalColl.DffSxMisMax;
            g_ucCalibrationStatus = CALIBRATION_SHOW_POT_SX_MAX;
            break;
        }
        g_ucCalibrationStatus = CALIBRATION_END_POT_SX_MAX;
        break;

    case CALIBRATION_SHOW_POT_SX_MAX:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Pot. DFF SX max:    ");
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Max. LeftPotSID:    ");
            break;
        }
        if (ee_ConfigColl.ucCmUnit){
            sys_print_msg    (0xc0,"     cm             ");
            sys_print_number (0xc0,s_temp16,3,0xff,false);
        } else {
            sys_print_msg (0xc0,"      pollici       ");
            sys_print_number (0xc0,cm_to_inches(s_temp16),4,3,false);
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_POT_SX_MAX;
        //break;    //Fall Through
    case CALIBRATION_WAIT_POT_SX_MAX:
        sys_print_number(0x90,RemoteAdc[DFF_STAT],4,0xff,false);
        nA = EncoderCalib(&g_wCrossMagnitude, 2);
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 > DFF_MAX)
                s_temp16 = DFF_MAX;
            if (s_temp16 < DFF_MIN)
                s_temp16 = DFF_MIN;
            g_ucCalibrationStatus = CALIBRATION_SHOW_POT_SX_MAX;
            break;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_END_POT_SX_MAX;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_CalColl.DffSxPotMax = RemoteAdc[DFF_STAT];
            ee_CalColl.DffSxMisMax = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_CALIB;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_END_POT_SX_MAX;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;
    case CALIBRATION_END_POT_SX_MAX:




//  *****************************************************************************************************
    case CALIBRATION_INIT_DFF_MIN:
        s_temp16 = ee_ConfigColl.uwMinDff;
        g_ucCalibrationStatus = CALIBRATION_SHOW_DFF_MIN;

    case CALIBRATION_SHOW_DFF_MIN:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Min DFF Verticale   ");
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Vertical SID Min    ");
            break;
        }
        if (ee_ConfigColl.ucCmUnit){
            sys_print_msg    (0xc0,"     cm             ");
            sys_print_number (0xc0,s_temp16,3,0xff,false);
        } else {
            sys_print_msg (0xc0,"      pollici       ");
            sys_print_number (0xc0,cm_to_inches(s_temp16),4,3,false);
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_DFF_MIN;
        //break;    //Fall Through
    case CALIBRATION_WAIT_DFF_MIN:
        nA = EncoderCalib(&g_wCrossMagnitude, 2);
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 > DFF_MAX)
                s_temp16 = DFF_MAX;
            if (s_temp16 < DFF_MIN)
                s_temp16 = DFF_MIN;
            g_ucCalibrationStatus = CALIBRATION_SHOW_DFF_MIN;
            break;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_INIT_DFF_MAX;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_ConfigColl.uwMinDff = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_INIT_DFF_MAX;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;

//  *****************************************************************************************************
    case CALIBRATION_INIT_DFF_MAX:
        s_temp16 = ee_ConfigColl.uwMaxDff;
        g_ucCalibrationStatus = CALIBRATION_SHOW_DFF_MAX;

    case CALIBRATION_SHOW_DFF_MAX:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Max DFF Verticale   ");
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Vertical SID Max    ");
            break;
        }
        if (ee_ConfigColl.ucCmUnit){
            sys_print_msg    (0xc0,"     cm             ");
            sys_print_number (0xc0,s_temp16,3,0xff,false);
        } else {
            sys_print_msg (0xc0,"      pollici       ");
            sys_print_number (0xc0,cm_to_inches(s_temp16),4,3,false);
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_DFF_MAX;
        //break;    //Fall Through
    case CALIBRATION_WAIT_DFF_MAX:
        nA = EncoderCalib(&g_wCrossMagnitude, 2);
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 > DFF_MAX)
                s_temp16 = DFF_MAX;
            if (s_temp16 < DFF_MIN)
                s_temp16 = DFF_MIN;
            g_ucCalibrationStatus = CALIBRATION_SHOW_DFF_MAX;
            break;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_END_DFF_MAX;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_ConfigColl.uwMaxDff = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_END_DFF_MAX;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;
    case CALIBRATION_END_DFF_MAX:


//  *****************************************************************************************************
    case CALIBRATION_INIT_MAN_COLLIMATION:
        s_temp16 = ee_ConfigColl.ucManualColl;

    case CALIBRATION_SHOW_MAN_COLLIMATION:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Collim. in Manuale  ");
            switch (s_temp16){
            case MANUAL_COLL_DIS:
                sys_print_msg (0xC0,"DISABILITATO        ");
                break;
            case MANUAL_COLL_EN:
                sys_print_msg (0xC0,"ABILITATO           ");
                break;
            }
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Collimat. in Manual ");
            switch (s_temp16){
            case MANUAL_COLL_DIS:
                sys_print_msg (0xC0,"DISABLED            ");
                break;
            case MANUAL_COLL_EN:
                sys_print_msg (0xC0,"ENABLED             ");
                break;
            }
            break;
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_MAN_COLLIMATION;
        //break;    //Fall Through
    case CALIBRATION_WAIT_MAN_COLLIMATION:
        nA = CheckChangeCrossEnc();
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 > MANUAL_COLL_EN)
                s_temp16 = MANUAL_COLL_EN;
            else if (s_temp16 < 0)
                s_temp16 = 0;
            g_ucCalibrationStatus = CALIBRATION_SHOW_MAN_COLLIMATION;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_END_MAN_COLLIMATION;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_ConfigColl.ucManualColl = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_END_MAN_COLLIMATION;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;
    case CALIBRATION_END_MAN_COLLIMATION:


//  *****************************************************************************************************
    case CALIBRATION_INIT_SHOW_DFF:
        s_temp16 = ee_ConfigColl.ucShowDff;

    case CALIBRATION_SHOW_SHOW_DFF:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Visualizzazione DFF ");
            switch (s_temp16){
            case SHOW_DFF_NO:
                sys_print_msg (0xC0,"NO                  ");
                break;
            case SHOW_DFF_YES:
                sys_print_msg (0xC0,"SI                  ");
                break;
            }
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"SID Visualization   ");
            switch (s_temp16){
            case SHOW_DFF_NO:
                sys_print_msg (0xC0,"NO                  ");
                break;
            case SHOW_DFF_YES:
                sys_print_msg (0xC0,"YES                 ");
                break;
            }
            break;
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_SHOW_DFF;
        //break;    //Fall Through
    case CALIBRATION_WAIT_SHOW_DFF:
        nA = CheckChangeCrossEnc();
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 > SHOW_DFF_MAX)
                s_temp16 = SHOW_DFF_MAX;
            else if (s_temp16 < SHOW_DFF_MIN)
                s_temp16 = SHOW_DFF_MIN;
            g_ucCalibrationStatus = CALIBRATION_SHOW_SHOW_DFF;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_INIT_SHOW_KEY;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_ConfigColl.ucShowDff = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_INIT_SHOW_KEY;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;


//  *****************************************************************************************************
    case CALIBRATION_INIT_SHOW_KEY:
        s_temp16 = ee_ConfigColl.ucShowLock;

    case CALIBRATION_SHOW_SHOW_KEY:
        switch (ee_ConfigColl.ucLanguage)
        {
        case ITA_LANG:
            sys_print_msg (0x80,"Vis. Stato Chiave   ");
            switch (s_temp16){
            case LOCK_SHOW_NO:
                sys_print_msg (0xC0,"NO                  ");
                break;
            case LOCK_SHOW_YES:
                sys_print_msg (0xC0,"SI                  ");
                break;
            }
            break;
        case ENG_LANG:
            sys_print_msg (0x80,"Key Status Visual.  ");
            switch (s_temp16){
            case LOCK_SHOW_NO:
                sys_print_msg (0xC0,"NO                  ");
                break;
            case LOCK_SHOW_YES:
                sys_print_msg (0xC0,"YES                 ");
                break;
            }
            break;
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_SHOW_KEY;
        //break;    //Fall Through
    case CALIBRATION_WAIT_SHOW_KEY:
        nA = CheckChangeCrossEnc();
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 > LOCK_SHOW_YES)
                s_temp16 = LOCK_SHOW_YES;
            else if (s_temp16 < 0)
                s_temp16 = 0;
            g_ucCalibrationStatus = CALIBRATION_SHOW_SHOW_KEY;
        }
        if (FILTER_SWITCH){
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_END_SHOW_KEY;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (LAMP_SWITCH){
            ee_ConfigColl.ucShowLock = s_temp16;
            s_ulTempUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_END_SHOW_KEY;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;
    case CALIBRATION_END_SHOW_KEY:

//  *****************************************************************************************************
    case CALIBRATION_SHOW_CAL_TYPE_TOP:
        g_ucCalibrationStatus = CALIBRATION_WAIT_CAL_TYPE_TOP;
        if (ee_ConfigColl.ucVertRec != VERT_REC_BUCKY){
            s_ucSetCount=0;
            g_ucCalibrationStatus = CALIBRATION_SHOW_CAL_TYPE_SX;
            break;
        }
        g_bCalFull = 0;
        if (ee_ConfigColl.ucLanguage == ITA_LANG){
            sys_print_msg (0x80,"Cal. Bucky Tavolo   ");
            sys_print_msg (0xC0,"Rapida              ");
        }
        if (ee_ConfigColl.ucLanguage == ENG_LANG){
            sys_print_msg (0x80,"Table Bucky Cal.    ");
            sys_print_msg (0xC0,"Quick               ");
        }
        //break;    //Fall Through

    case CALIBRATION_WAIT_CAL_TYPE_TOP:
        nA = CheckChangeCrossEnc();
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 > 1)
                s_temp16 = 1;
            else if (s_temp16 < 0)
                s_temp16 = 0;
            g_bCalFull = s_temp16;
            if (g_bCalFull == 0){
                if (ee_ConfigColl.ucLanguage == ITA_LANG)
                    sys_print_msg (0xC0,"Rapida              ");
                if (ee_ConfigColl.ucLanguage == ENG_LANG)
                    sys_print_msg (0xC0,"Quick               ");
            } else {
                if (ee_ConfigColl.ucLanguage == ITA_LANG)
                    sys_print_msg (0xC0,"Completa            ");
                if (ee_ConfigColl.ucLanguage == ENG_LANG)
                    sys_print_msg (0xC0,"Full                ");
            }
        }
        if (LAMP_SWITCH){
            wait_key_exit_up(250);
            if (g_bCalFull)
                g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_TOP_FULL;
            else
                g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_TOP_QUICK;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (FILTER_SWITCH){
            //Exit from the Procedure
            g_bCalFull=false;
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_SHOW_CAL_TYPE_SX;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;

    case CALIBRATION_SHOW_BUCKY_TOP_QUICK:
        g_ucCalibrationStatus = CALIBRATION_WAIT_BUCKY_TOP_QUICK;
        s_temp16 = 2;
        g_ucNMisCassetteTable = 2;
        //break;    //Fall Through

    case CALIBRATION_WAIT_BUCKY_TOP_QUICK:
        g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_TOP_ACQ;
        break;

    case CALIBRATION_SHOW_BUCKY_TOP_FULL:
        g_ucCalibrationStatus = CALIBRATION_WAIT_BUCKY_TOP_FULL;
        if (ee_ConfigColl.ucLanguage == ITA_LANG){
            sys_print_msg (0x80,"Imposta Numero      ");
            sys_print_msg (0xC0,"Cassette Tavolo     ");
        }
        if (ee_ConfigColl.ucLanguage == ENG_LANG){
            sys_print_msg (0x80,"Set Number of       ");
            sys_print_msg (0xC0,"Table Buckies       ");
        }
        sys_print_byte(0xCF,s_temp16,false);
        //break;    //Fall Through

    case CALIBRATION_WAIT_BUCKY_TOP_FULL:
        nA = EncoderCalib(&g_wCrossMagnitude, 2);
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 > MAX_CASSETTE)
                s_temp16 = MAX_CASSETTE;
            if (s_temp16 < MIN_CASSETTE)
                s_temp16 = MIN_CASSETTE;
            g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_TOP_FULL;
            break;
        }
        if (LAMP_SWITCH){
            s_ucCassetteCounter=0;
            s_ucCmCounterCross=MIN_BUCKY_CM;
            s_ucInCounterCross=MIN_BUCKY_IN;
            s_ucCmCounterLong=MIN_BUCKY_CM;
            s_ucInCounterLong=MIN_BUCKY_IN;
            g_ucNMisCassetteTable = s_temp16;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_TOP_ACQ;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (FILTER_SWITCH){
            //Exit from the Procedure
            wait_key_filter_up(100);
            s_ucCassetteCounter=0;
            s_ucCmCounterCross=MIN_BUCKY_CM;
            s_ucInCounterCross=MIN_BUCKY_IN;
            s_ucCmCounterLong=MIN_BUCKY_CM;
            s_ucInCounterLong=MIN_BUCKY_IN;
            g_ucCalibrationStatus = CALIBRATION_SHOW_CAL_TYPE_SX;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;

    case CALIBRATION_SHOW_BUCKY_TOP_ACQ:
        if (ee_ConfigColl.ucCmUnit){
            //Cm
            if (s_ucCassetteCounter<g_ucNMisCassetteTable){
                sys_print_msg (0x80,"Cross .T Cm ..:     ");
                sys_print_msg (0xC0,"Long  .T Cm ..:     ");
                sys_print_byte(0x84,s_ucCassetteCounter+1,false);
                sys_print_byte(0xC4,s_ucCassetteCounter+1,false);
                sys_print_byte(0x8B,s_ucCmCounterCross,false);
                sys_print_byte(0xCB,s_ucCmCounterLong,false);
            }
        } else {
            //Inches
            if (s_ucCassetteCounter<g_ucNMisCassetteTable){
                sys_print_msg (0x80,"Cross .T In ..:     ");
                sys_print_msg (0xC0,"Long  .T In ..:     ");
                sys_print_byte(0x84,s_ucCassetteCounter+1,false);
                sys_print_byte(0xC4,s_ucCassetteCounter+1,false);
                sys_print_byte(0x8B,s_ucInCounterCross,false);
                sys_print_byte(0xCB,s_ucInCounterLong,false);
            }
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_BUCKY_TOP_ACQ;
        //break;    //Fall Through

    case CALIBRATION_WAIT_BUCKY_TOP_ACQ:
        if (ee_ConfigColl.ucCmUnit){
            if (s_ucCassetteCounter<g_ucNMisCassetteTable){
                sys_print_number(0x8F,RemoteAdc[BUCKY_CROSS],4,0xff,false);
                sys_print_number(0xCF,RemoteAdc[BUCKY_LONG],4,0xff,false);
                nA = EncoderCalib(&g_wCrossMagnitude, 2);
                if (nA)
                {
                    s_ucCmCounterCross += nA;
                    if (s_ucCmCounterCross > MAX_BUCKY_CM)
                        s_ucCmCounterCross = MAX_BUCKY_CM;
                    if (s_ucCmCounterCross < MIN_BUCKY_CM)
                        s_ucCmCounterCross = MIN_BUCKY_CM;
                    g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_TOP_ACQ;
                    break;
                }
                nA = EncoderCalib(&g_wLongMagnitude, 2);
                if (nA)
                {
                    s_ucCmCounterLong += nA;
                    if (s_ucCmCounterLong > MAX_BUCKY_CM)
                        s_ucCmCounterLong = MAX_BUCKY_CM;
                    if (s_ucCmCounterLong < MIN_BUCKY_CM)
                        s_ucCmCounterLong = MIN_BUCKY_CM;
                    g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_TOP_ACQ;
                    break;
                }
                if (LAMP_SWITCH){
                    ee_TopBuckyColl.uwTopBuckyCrossCm[s_ucCassetteCounter] = s_ucCmCounterCross;
                    ee_TopBuckyColl.uwTopBuckyCrossAdcCm[s_ucCassetteCounter] = RemoteAdc[BUCKY_CROSS];
                    ee_TopBuckyColl.uwTopBuckyLongCm[s_ucCassetteCounter] = s_ucCmCounterLong;
                    ee_TopBuckyColl.uwTopBuckyLongAdcCm[s_ucCassetteCounter] = RemoteAdc[BUCKY_LONG];
                    s_ulTempUpdateEeprom |= EEP_UPDATE_TOPBUCKY;
                    s_ucCassetteCounter++;
                    wait_key_exit_up(250);
                    g_wCrossMagnitude = 0;
                    g_wLongMagnitude = 0;
                    g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_TOP_ACQ;
                    break;
                }
            } else {
                for (ucIndex=s_ucCassetteCounter; ucIndex<MAX_CASSETTE; ucIndex++){
                    ee_TopBuckyColl.uwTopBuckyCrossCm[ucIndex] = 0;
                    ee_TopBuckyColl.uwTopBuckyCrossAdcCm[ucIndex] = 0;
                    ee_TopBuckyColl.uwTopBuckyLongCm[ucIndex] = 0;
                    ee_TopBuckyColl.uwTopBuckyLongAdcCm[ucIndex] = 0;
                }
                if (g_bCalFull){
                    s_ucCassetteCounter=0;
                    s_ucCmCounterCross=MIN_BUCKY_CM;
                    s_ucCmCounterLong=MIN_BUCKY_CM;
                    g_ucCalibrationStatus = CALIBRATION_SHOW_CAL_TYPE_SX;
                    break;
                }
                ucCrossCounter=s_ucCassetteCounter;
                ucLongCounter=s_ucCassetteCounter;

                //Quick calibration
                nA = ee_TopBuckyColl.uwTopBuckyCrossAdcCm[1]-ee_TopBuckyColl.uwTopBuckyCrossAdcCm[0];
                nB = ee_TopBuckyColl.uwTopBuckyCrossCm[1]-ee_TopBuckyColl.uwTopBuckyCrossCm[0];
                nC = nA/nB;
                uwAlfaC = nC;
                nA = ee_TopBuckyColl.uwTopBuckyLongAdcCm[1]-ee_TopBuckyColl.uwTopBuckyLongAdcCm[0];
                nB = ee_TopBuckyColl.uwTopBuckyLongCm[1]-ee_TopBuckyColl.uwTopBuckyLongCm[0];
                nC = nA/nB;
                uwAlfaL = nC;

                for (ucIndex=0; ucIndex<MAX_CASSETTE; ucIndex++){
                    bInsert=true;
                    for (ucIndex2=0; ucIndex2<MAX_CASSETTE; ucIndex2++){
                        if (g_ucTableMisCrossCm[ucIndex]==ee_TopBuckyColl.uwTopBuckyCrossCm[ucIndex2]){
                            if (ee_TopBuckyColl.uwTopBuckyCrossCm[ucIndex2]){
                                bInsert=false;
                                break;
                            }
                        }
                    }
                    if (bInsert){
                        ee_TopBuckyColl.uwTopBuckyCrossCm[ucCrossCounter] = g_ucTableMisCrossCm[ucIndex];
                        nA = ee_TopBuckyColl.uwTopBuckyCrossCm[ucCrossCounter]-ee_TopBuckyColl.uwTopBuckyCrossCm[0];
                        ee_TopBuckyColl.uwTopBuckyCrossAdcCm[ucCrossCounter] = (uwAlfaC*nA)+ee_TopBuckyColl.uwTopBuckyCrossAdcCm[0];
                        ucCrossCounter++;
                    }
                    bInsert=true;
                    for (ucIndex2=0; ucIndex2<MAX_CASSETTE; ucIndex2++){
                        if (g_ucTableMisLongCm[ucIndex]==ee_TopBuckyColl.uwTopBuckyLongCm[ucIndex2]){
                            if (ee_TopBuckyColl.uwTopBuckyLongCm[ucIndex2]){
                                bInsert=false;
                                break;
                            }
                        }
                    }
                    if (bInsert){
                        ee_TopBuckyColl.uwTopBuckyLongCm[ucLongCounter] = g_ucTableMisLongCm[ucIndex];
                        nA = ee_TopBuckyColl.uwTopBuckyLongCm[ucLongCounter]-ee_TopBuckyColl.uwTopBuckyLongCm[0];
                        ee_TopBuckyColl.uwTopBuckyLongAdcCm[ucLongCounter] = (uwAlfaL*nA)+ee_TopBuckyColl.uwTopBuckyLongAdcCm[0];
                        ucLongCounter++;
                    }
                }
                s_ucCassetteCounter=0;
                s_ucCmCounterCross=MIN_BUCKY_CM;
                s_ucCmCounterLong=MIN_BUCKY_CM;
                g_ucCalibrationStatus = CALIBRATION_SHOW_CAL_TYPE_SX;
                break;
            }
        } else {
            //Inches
            if (s_ucCassetteCounter<g_ucNMisCassetteTable){
                sys_print_number(0x8F,RemoteAdc[BUCKY_CROSS],4,0xff,false);
                sys_print_number(0xCF,RemoteAdc[BUCKY_LONG],4,0xff,false);
                //Read data from CanBus
                nA = EncoderCalib(&g_wCrossMagnitude, 2);
                if (nA)
                {
                    s_ucInCounterCross += nA;
                    if (s_ucInCounterCross > MAX_BUCKY_IN)
                        s_ucInCounterCross = MAX_BUCKY_IN;
                    if (s_ucInCounterCross < MIN_BUCKY_IN)
                        s_ucInCounterCross = MIN_BUCKY_IN;
                    g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_TOP_ACQ;
                    break;
                }
                nA = EncoderCalib(&g_wLongMagnitude, 2);
                if (nA)
                {
                    s_ucInCounterLong += nA;
                    if (s_ucInCounterLong > MAX_BUCKY_IN)
                        s_ucInCounterLong = MAX_BUCKY_IN;
                    if (s_ucInCounterLong < MIN_BUCKY_IN)
                        s_ucInCounterLong = MIN_BUCKY_IN;
                    g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_TOP_ACQ;
                    break;
                }
                if (LAMP_SWITCH){
                    ee_TopBuckyColl.uwTopBuckyCrossIn[s_ucCassetteCounter] = s_ucInCounterCross;
                    ee_TopBuckyColl.uwTopBuckyCrossAdcIn[s_ucCassetteCounter] = RemoteAdc[BUCKY_CROSS];
                    ee_TopBuckyColl.uwTopBuckyLongIn[s_ucCassetteCounter] = s_ucInCounterLong;
                    ee_TopBuckyColl.uwTopBuckyLongAdcIn[s_ucCassetteCounter] = RemoteAdc[BUCKY_LONG];
                    s_ulTempUpdateEeprom |= EEP_UPDATE_TOPBUCKY;
                    s_ucCassetteCounter++;
                    wait_key_exit_up(250);
                    g_wCrossMagnitude = 0;
                    g_wLongMagnitude = 0;
                    g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_TOP_ACQ;
                    break;
                }
            } else {
                for (ucIndex=s_ucCassetteCounter; ucIndex<MAX_CASSETTE; ucIndex++){
                    ee_TopBuckyColl.uwTopBuckyCrossIn[ucIndex] = 0;
                    ee_TopBuckyColl.uwTopBuckyCrossAdcIn[ucIndex] = 0;
                    ee_TopBuckyColl.uwTopBuckyLongIn[ucIndex] = 0;
                    ee_TopBuckyColl.uwTopBuckyLongAdcIn[ucIndex] = 0;
                }
                if (g_bCalFull){
                    s_ucCassetteCounter=0;
                    s_ucInCounterCross=MIN_BUCKY_IN;
                    s_ucInCounterLong=MIN_BUCKY_IN;
                    g_ucCalibrationStatus = CALIBRATION_SHOW_CAL_TYPE_SX;
                    break;
                }
                ucCrossCounter=s_ucCassetteCounter;
                ucLongCounter=s_ucCassetteCounter;

                //Quick calibration
                nA = ee_TopBuckyColl.uwTopBuckyCrossAdcIn[1]-ee_TopBuckyColl.uwTopBuckyCrossAdcIn[0];
                nB = ee_TopBuckyColl.uwTopBuckyCrossIn[1]-ee_TopBuckyColl.uwTopBuckyCrossIn[0];
                nC = nA/nB;
                uwAlfaC = nC;
                nA = ee_TopBuckyColl.uwTopBuckyLongAdcIn[1]-ee_TopBuckyColl.uwTopBuckyLongAdcIn[0];
                nB = ee_TopBuckyColl.uwTopBuckyLongIn[1]-ee_TopBuckyColl.uwTopBuckyLongIn[0];
                nC = nA/nB;
                uwAlfaL = nC;

                for (ucIndex=0; ucIndex<MAX_CASSETTE; ucIndex++){
                    bInsert=true;
                    for (ucIndex2=0; ucIndex2<MAX_CASSETTE; ucIndex2++){
                        if (g_ucTableMisCrossIn[ucIndex]==ee_TopBuckyColl.uwTopBuckyCrossIn[ucIndex2]){
                            if (ee_TopBuckyColl.uwTopBuckyCrossIn[ucIndex2]){
                                bInsert=false;
                                break;
                            }
                        }
                    }
                    if (bInsert){
                        ee_TopBuckyColl.uwTopBuckyCrossIn[ucCrossCounter] = g_ucTableMisCrossIn[ucIndex];
                        nA = ee_TopBuckyColl.uwTopBuckyCrossIn[ucCrossCounter]-ee_TopBuckyColl.uwTopBuckyCrossIn[0];
                        ee_TopBuckyColl.uwTopBuckyCrossAdcIn[ucCrossCounter] = (uwAlfaC*nA)+ee_TopBuckyColl.uwTopBuckyCrossAdcIn[0];
                        ucCrossCounter++;
                    }
                    bInsert=true;
                    for (ucIndex2=0; ucIndex2<MAX_CASSETTE; ucIndex2++){
                        if (g_ucTableMisLongIn[ucIndex]==ee_TopBuckyColl.uwTopBuckyLongIn[ucIndex2]){
                            if (ee_TopBuckyColl.uwTopBuckyLongIn[ucIndex2]){
                                bInsert=false;
                                break;
                            }
                        }
                    }
                    if (bInsert){
                        ee_TopBuckyColl.uwTopBuckyLongIn[ucLongCounter] = g_ucTableMisLongIn[ucIndex];
                        nA = ee_TopBuckyColl.uwTopBuckyLongIn[ucLongCounter]-ee_TopBuckyColl.uwTopBuckyLongIn[0];
                        ee_TopBuckyColl.uwTopBuckyLongAdcIn[ucLongCounter] = (uwAlfaC*nA)+ee_TopBuckyColl.uwTopBuckyLongAdcIn[0];
                        ucLongCounter++;
                    }
                }
                s_ucCassetteCounter=0;
                s_ucInCounterCross=MIN_BUCKY_IN;
                s_ucInCounterLong=MIN_BUCKY_IN;
                g_ucCalibrationStatus = CALIBRATION_SHOW_CAL_TYPE_SX;
                break;
            }
        }
        break;
    case CALIBRATION_SHOW_CAL_TYPE_SX:
        g_ucCalibrationStatus = CALIBRATION_WAIT_CAL_TYPE_SX;
        if (ee_ConfigColl.ucLatRecSx != LAT_REC_BUCKY){
            s_ucSetCount=0;
            g_ucCalibrationStatus = CALIBRATION_SHOW_CAL_TYPE_DX;
            break;
        }
        g_bCalFull = 0;
        if (ee_ConfigColl.ucLanguage == ITA_LANG){
            sys_print_msg (0x80,"Cal. Bucky Lato Sx  ");
            sys_print_msg (0xC0,"Rapida              ");
        }
        if (ee_ConfigColl.ucLanguage == ENG_LANG){
            sys_print_msg (0x80,"Left Bucky Cal.     ");
            sys_print_msg (0xC0,"Quick               ");
        }
        g_wCrossMagnitude = 0;
        //break;    //Fall Through
    case CALIBRATION_WAIT_CAL_TYPE_SX:
        nA = CheckChangeCrossEnc();
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 > 1)
                s_temp16 = 1;
            else if (s_temp16 < 0)
                s_temp16 = 0;
            g_bCalFull = s_temp16;
            if (g_bCalFull == 0){
                if (ee_ConfigColl.ucLanguage == ITA_LANG)
                    sys_print_msg (0xC0,"Rapida              ");
                if (ee_ConfigColl.ucLanguage == ENG_LANG)
                    sys_print_msg (0xC0,"Quick               ");
            } else {
                if (ee_ConfigColl.ucLanguage == ITA_LANG)
                    sys_print_msg (0xC0,"Completa            ");
                if (ee_ConfigColl.ucLanguage == ENG_LANG)
                    sys_print_msg (0xC0,"Full                ");
            }
        }
        if (LAMP_SWITCH){
            wait_key_exit_up(250);
            if (g_bCalFull)
                g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_SX_FULL;
            else
                g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_SX_QUICK;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (FILTER_SWITCH){
            //Exit from the Procedure
            g_bCalFull=false;
            wait_key_filter_up(100);
            g_ucCalibrationStatus = CALIBRATION_SHOW_CAL_TYPE_DX;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;
    case CALIBRATION_SHOW_BUCKY_SX_QUICK:
        g_ucCalibrationStatus = CALIBRATION_WAIT_BUCKY_SX_QUICK;
        s_temp16 = 2;
        g_ucNMisCassetteSx = 2;

        //break;    //Fall Through

    case CALIBRATION_WAIT_BUCKY_SX_QUICK:
        g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_SX_ACQ;
        break;

    case CALIBRATION_SHOW_BUCKY_SX_FULL:
        g_ucCalibrationStatus = CALIBRATION_WAIT_BUCKY_SX_FULL;
        if (ee_ConfigColl.ucLanguage == ITA_LANG){
            sys_print_msg (0x80,"Imposta Numero      ");
            sys_print_msg (0xC0,"Cassette Lato Sx    ");
        }
        if (ee_ConfigColl.ucLanguage == ENG_LANG){
            sys_print_msg (0x80,"Set Number of       ");
            sys_print_msg (0xC0,"Left Side Buckies   ");
        }
        sys_print_byte(0xD0,s_temp16,false);
        //break;    //Fall Through

    case CALIBRATION_WAIT_BUCKY_SX_FULL:
        nA = EncoderCalib(&g_wCrossMagnitude, 2);
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 > MAX_CASSETTE)
                s_temp16 = MAX_CASSETTE;
            if (s_temp16 < MIN_CASSETTE)
                s_temp16 = MIN_CASSETTE;
            g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_SX_FULL;
            break;
        }
        if (LAMP_SWITCH){
            s_ucCassetteCounter=0;
            s_ucCmCounterCross=MIN_BUCKY_CM;
            s_ucInCounterCross=MIN_BUCKY_IN;
            s_ucCmCounterLong=MIN_BUCKY_CM;
            s_ucInCounterLong=MIN_BUCKY_IN;
            g_ucNMisCassetteSx = s_temp16;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_SX_ACQ;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (FILTER_SWITCH){
            //Exit from the Procedure
            wait_key_filter_up(100);
            s_ucCassetteCounter=0;
            s_ucCmCounterCross=MIN_BUCKY_CM;
            s_ucInCounterCross=MIN_BUCKY_IN;
            s_ucCmCounterLong=MIN_BUCKY_CM;
            s_ucInCounterLong=MIN_BUCKY_IN;
            g_ucCalibrationStatus = CALIBRATION_SHOW_CAL_TYPE_DX;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;

    case CALIBRATION_SHOW_BUCKY_SX_ACQ:
        if (ee_ConfigColl.ucCmUnit){
            //Cm
            if (s_ucCassetteCounter<g_ucNMisCassetteSx){
                sys_print_msg (0x80,"Cross .S Cm ..:     ");
                sys_print_msg (0xC0,"Long  .S Cm ..:     ");
                sys_print_byte(0x84,s_ucCassetteCounter+1,false);
                sys_print_byte(0xC4,s_ucCassetteCounter+1,false);
                sys_print_byte(0x8B,s_ucCmCounterCross,false);
                sys_print_byte(0xCB,s_ucCmCounterLong,false);
            }
        } else {
            //Inches
            if (s_ucCassetteCounter<g_ucNMisCassetteSx){
                sys_print_msg (0x80,"Cross .S In ..:     ");
                sys_print_msg (0xC0,"Long  .S In ..:     ");
                sys_print_byte(0x84,s_ucCassetteCounter+1,false);
                sys_print_byte(0xC4,s_ucCassetteCounter+1,false);
                sys_print_byte(0x8B,s_ucInCounterCross,false);
                sys_print_byte(0xCB,s_ucInCounterLong,false);
            }
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_BUCKY_SX_ACQ;
        //break;    //Fall Through

    case CALIBRATION_WAIT_BUCKY_SX_ACQ:
        if (ee_ConfigColl.ucCmUnit){
            if (s_ucCassetteCounter<g_ucNMisCassetteSx){
                sys_print_number(0x8F,RemoteAdc[LEFT_CROSS],4,0xff,false);
                sys_print_number(0xCF,RemoteAdc[LEFT_LONG],4,0xff,false);
                nA = EncoderCalib(&g_wCrossMagnitude, 2);
                if (nA)
                {
                    s_ucCmCounterCross += nA;
                    if (s_ucCmCounterCross > MAX_BUCKY_CM)
                        s_ucCmCounterCross = MAX_BUCKY_CM;
                    if (s_ucCmCounterCross < MIN_BUCKY_CM)
                        s_ucCmCounterCross = MIN_BUCKY_CM;
                    g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_SX_ACQ;
                    break;
                }
                nA = EncoderCalib(&g_wLongMagnitude, 2);
                if (nA)
                {
                    s_ucCmCounterLong += nA;
                    if (s_ucCmCounterLong > MAX_BUCKY_CM)
                        s_ucCmCounterLong = MAX_BUCKY_CM;
                    if (s_ucCmCounterLong < MIN_BUCKY_CM)
                        s_ucCmCounterLong = MIN_BUCKY_CM;
                    g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_SX_ACQ;
                    break;
                }
                if (LAMP_SWITCH){
                    ee_SxBuckyColl.uwSxBuckyCrossCm[s_ucCassetteCounter] = s_ucCmCounterCross;
                    ee_SxBuckyColl.uwSxBuckyCrossAdcCm[s_ucCassetteCounter] = RemoteAdc[LEFT_CROSS];
                    ee_SxBuckyColl.uwSxBuckyLongCm[s_ucCassetteCounter] = s_ucCmCounterLong;
                    ee_SxBuckyColl.uwSxBuckyLongAdcCm[s_ucCassetteCounter] = RemoteAdc[LEFT_LONG];
                    s_ulTempUpdateEeprom |= EEP_UPDATE_SXBUCKY;
                    s_ucCassetteCounter++;
                    wait_key_exit_up(250);
                    g_wCrossMagnitude = 0;
                    g_wLongMagnitude = 0;
                    g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_SX_ACQ;
                    break;
                }
            } else {
                for (ucIndex=s_ucCassetteCounter; ucIndex<MAX_CASSETTE; ucIndex++){
                    ee_SxBuckyColl.uwSxBuckyCrossCm[ucIndex] = 0;
                    ee_SxBuckyColl.uwSxBuckyCrossAdcCm[ucIndex] = 0;
                    ee_SxBuckyColl.uwSxBuckyLongCm[ucIndex] = 0;
                    ee_SxBuckyColl.uwSxBuckyLongAdcCm[ucIndex] = 0;
                }
                if (g_bCalFull){
                    s_ucCassetteCounter=0;
                    s_ucCmCounterCross=MIN_BUCKY_CM;
                    s_ucCmCounterLong=MIN_BUCKY_CM;
                    g_ucCalibrationStatus = CALIBRATION_SHOW_CAL_TYPE_DX;
                    break;
                }
                ucCrossCounter=s_ucCassetteCounter;
                ucLongCounter=s_ucCassetteCounter;

                //Quick calibration
                nA = ee_SxBuckyColl.uwSxBuckyCrossAdcCm[1]-ee_SxBuckyColl.uwSxBuckyCrossAdcCm[0];
                nB = ee_SxBuckyColl.uwSxBuckyCrossCm[1]-ee_SxBuckyColl.uwSxBuckyCrossCm[0];
                nC = nA/nB;
                uwAlfaC = nC;
                nA = ee_SxBuckyColl.uwSxBuckyLongAdcCm[1]-ee_SxBuckyColl.uwSxBuckyLongAdcCm[0];
                nB = ee_SxBuckyColl.uwSxBuckyLongCm[1]-ee_SxBuckyColl.uwSxBuckyLongCm[0];
                nC = nA/nB;
                uwAlfaL = nC;

                for (ucIndex=0; ucIndex<MAX_CASSETTE; ucIndex++){
                    bInsert=true;
                    for (ucIndex2=0; ucIndex2<MAX_CASSETTE; ucIndex2++){
                        if (g_ucTableMisCrossCm[ucIndex]==ee_SxBuckyColl.uwSxBuckyCrossCm[ucIndex2]){
                            if (ee_SxBuckyColl.uwSxBuckyCrossCm[ucIndex2]){
                                bInsert=false;
                                break;
                            }
                        }
                    }

                    if (bInsert){
                        ee_SxBuckyColl.uwSxBuckyCrossCm[ucCrossCounter] = g_ucTableMisCrossCm[ucIndex];
                        nA = ee_SxBuckyColl.uwSxBuckyCrossCm[ucCrossCounter]-ee_SxBuckyColl.uwSxBuckyCrossCm[0];
                        ee_SxBuckyColl.uwSxBuckyCrossAdcCm[ucCrossCounter] = (uwAlfaC*nA)+ee_SxBuckyColl.uwSxBuckyCrossAdcCm[0];
                        ucCrossCounter++;
                    }
                    bInsert=true;
                    for (ucIndex2=0; ucIndex2<MAX_CASSETTE; ucIndex2++){
                        if (g_ucTableMisLongCm[ucIndex]==ee_SxBuckyColl.uwSxBuckyLongCm[ucIndex2]){
                            if (ee_SxBuckyColl.uwSxBuckyLongCm[ucIndex2]){
                                bInsert=false;
                                break;
                            }
                        }
                    }
                    if (bInsert){
                        ee_SxBuckyColl.uwSxBuckyLongCm[ucLongCounter] = g_ucTableMisLongCm[ucIndex];
                        nA = ee_SxBuckyColl.uwSxBuckyLongCm[ucLongCounter]-ee_SxBuckyColl.uwSxBuckyLongCm[0];
                        ee_SxBuckyColl.uwSxBuckyLongAdcCm[ucLongCounter] = (uwAlfaL*nA)+ee_SxBuckyColl.uwSxBuckyLongAdcCm[0];
                        ucLongCounter++;
                    }
                }
                s_ucCassetteCounter=0;
                s_ucCmCounterCross=MIN_BUCKY_CM;
                s_ucCmCounterLong=MIN_BUCKY_CM;
                g_ucCalibrationStatus = CALIBRATION_SHOW_CAL_TYPE_DX;
                break;
            }
        } else {
            //Inches
            if (s_ucCassetteCounter<g_ucNMisCassetteSx){
                sys_print_number(0x8F,RemoteAdc[LEFT_CROSS],4,0xff,false);
                sys_print_number(0xCF,RemoteAdc[LEFT_LONG],4,0xff,false);
                nA = EncoderCalib(&g_wCrossMagnitude, 2);
                if (nA)
                {
                    s_ucInCounterCross += nA;
                    if (s_ucInCounterCross > MAX_BUCKY_IN)
                        s_ucInCounterCross = MAX_BUCKY_IN;
                    if (s_ucInCounterCross < MIN_BUCKY_IN)
                        s_ucInCounterCross = MIN_BUCKY_IN;
                    g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_SX_ACQ;
                    break;
                }
                nA = EncoderCalib(&g_wLongMagnitude, 2);
                if (nA)
                {
                    s_ucInCounterLong += nA;
                    if (s_ucInCounterLong > MAX_BUCKY_IN)
                        s_ucInCounterLong = MAX_BUCKY_IN;
                    if (s_ucInCounterLong < MIN_BUCKY_IN)
                        s_ucInCounterLong = MIN_BUCKY_IN;
                    g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_SX_ACQ;
                    break;
                }
                if (LAMP_SWITCH){
                    ee_SxBuckyColl.uwSxBuckyCrossIn[s_ucCassetteCounter] = s_ucInCounterCross;
                    ee_SxBuckyColl.uwSxBuckyCrossAdcIn[s_ucCassetteCounter] = RemoteAdc[LEFT_CROSS];
                    ee_SxBuckyColl.uwSxBuckyLongIn[s_ucCassetteCounter] = s_ucInCounterLong;
                    ee_SxBuckyColl.uwSxBuckyLongAdcIn[s_ucCassetteCounter] = RemoteAdc[LEFT_LONG];
                    s_ulTempUpdateEeprom |= EEP_UPDATE_SXBUCKY;
                    s_ucCassetteCounter++;
                    wait_key_exit_up(250);
                    g_wCrossMagnitude = 0;
                    g_wLongMagnitude = 0;
                    g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_SX_ACQ;
                    break;
                }
            } else {
                for (ucIndex=s_ucCassetteCounter; ucIndex<MAX_CASSETTE; ucIndex++){
                    ee_SxBuckyColl.uwSxBuckyCrossIn[ucIndex] = 0;
                    ee_SxBuckyColl.uwSxBuckyCrossAdcIn[ucIndex] = 0;
                    ee_SxBuckyColl.uwSxBuckyLongIn[ucIndex] = 0;
                    ee_SxBuckyColl.uwSxBuckyLongAdcIn[ucIndex] = 0;
                }
                if (g_bCalFull){
                    s_ucCassetteCounter=0;
                    s_ucInCounterCross=MIN_BUCKY_IN;
                    s_ucInCounterLong=MIN_BUCKY_IN;
                    g_ucCalibrationStatus = CALIBRATION_SHOW_CAL_TYPE_DX;
                    break;
                }
                ucCrossCounter=s_ucCassetteCounter;
                ucLongCounter=s_ucCassetteCounter;

                //Quick calibration
                nA = ee_SxBuckyColl.uwSxBuckyCrossAdcIn[1]-ee_SxBuckyColl.uwSxBuckyCrossAdcIn[0];
                nB = ee_SxBuckyColl.uwSxBuckyCrossIn[1]-ee_SxBuckyColl.uwSxBuckyCrossIn[0];
                nC = nA/nB;
                uwAlfaC = nC;
                nA = ee_SxBuckyColl.uwSxBuckyLongAdcIn[1]-ee_SxBuckyColl.uwSxBuckyLongAdcIn[0];
                nB = ee_SxBuckyColl.uwSxBuckyLongIn[1]-ee_SxBuckyColl.uwSxBuckyLongIn[0];
                nC = nA/nB;
                uwAlfaL = nC;

                for (ucIndex=0; ucIndex<MAX_CASSETTE; ucIndex++){
                    bInsert=true;
                    for (ucIndex2=0; ucIndex2<MAX_CASSETTE; ucIndex2++){
                        if (g_ucTableMisCrossIn[ucIndex]==ee_SxBuckyColl.uwSxBuckyCrossIn[ucIndex2]){
                            if (ee_SxBuckyColl.uwSxBuckyCrossIn[ucIndex2]){
                                bInsert=false;
                                break;
                            }
                        }
                    }
                    if (bInsert){
                        ee_SxBuckyColl.uwSxBuckyCrossIn[ucCrossCounter] = g_ucTableMisCrossIn[ucIndex];
                        nA = ee_SxBuckyColl.uwSxBuckyCrossIn[ucCrossCounter]-ee_SxBuckyColl.uwSxBuckyCrossIn[0];
                        ee_SxBuckyColl.uwSxBuckyCrossAdcIn[ucCrossCounter] = (uwAlfaC*nA)+ee_SxBuckyColl.uwSxBuckyCrossAdcIn[0];
                        ucCrossCounter++;
                    }
                    bInsert=true;
                    for (ucIndex2=0; ucIndex2<MAX_CASSETTE; ucIndex2++){
                        if (g_ucTableMisLongIn[ucIndex]==ee_SxBuckyColl.uwSxBuckyLongIn[ucIndex2]){
                            if (ee_SxBuckyColl.uwSxBuckyLongIn[ucIndex2]){
                                bInsert=false;
                                break;
                            }
                        }
                    }
                    if (bInsert){
                        ee_SxBuckyColl.uwSxBuckyLongIn[ucLongCounter] = g_ucTableMisLongIn[ucIndex];
                        nA = ee_SxBuckyColl.uwSxBuckyLongIn[ucLongCounter]-ee_SxBuckyColl.uwSxBuckyLongIn[0];
                        ee_SxBuckyColl.uwSxBuckyLongAdcIn[ucLongCounter] = (uwAlfaL*nA)+ee_SxBuckyColl.uwSxBuckyLongAdcIn[0];
                        ucLongCounter++;
                    }
                }
                s_ucCassetteCounter=0;
                s_ucInCounterCross=MIN_BUCKY_IN;
                s_ucInCounterLong=MIN_BUCKY_IN;
                g_ucCalibrationStatus = CALIBRATION_SHOW_CAL_TYPE_DX;
                break;
            }
        }
        break;
    case CALIBRATION_SHOW_CAL_TYPE_DX:
        g_ucCalibrationStatus = CALIBRATION_WAIT_CAL_TYPE_DX;
        if (ee_ConfigColl.ucLatRecDx != LAT_REC_BUCKY){
            s_ucSetCount=0;
            g_ucCalibrationStatus = CALIBRATION_END_BUCKY_DX_ACQ;
            break;
        }
        g_bCalFull = 0;
        if (ee_ConfigColl.ucLanguage == ITA_LANG){
            sys_print_msg (0x80,"Cal. Bucky Lato Dx  ");
            sys_print_msg (0xC0,"Rapida              ");
        }
        if (ee_ConfigColl.ucLanguage == ENG_LANG){
            sys_print_msg (0x80,"Right Bucky Cal.    ");
            sys_print_msg (0xC0,"Quick               ");
        }
        g_wCrossMagnitude = 0;
        //break;    //Fall Through

    case CALIBRATION_WAIT_CAL_TYPE_DX:
        nA = CheckChangeCrossEnc();
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 > 1)
                s_temp16 = 1;
            else if (s_temp16 < 0)
                s_temp16 = 0;
            g_bCalFull = s_temp16;
            if (g_bCalFull == 0){
                if (ee_ConfigColl.ucLanguage == ITA_LANG)
                    sys_print_msg (0xC0,"Rapida              ");
                if (ee_ConfigColl.ucLanguage == ENG_LANG)
                    sys_print_msg (0xC0,"Quick               ");
            } else {
                if (ee_ConfigColl.ucLanguage == ITA_LANG)
                    sys_print_msg (0xC0,"Completa            ");
                if (ee_ConfigColl.ucLanguage == ENG_LANG)
                    sys_print_msg (0xC0,"Full                ");
            }
        }
        if (LAMP_SWITCH){
            wait_key_exit_up(250);
            if (g_bCalFull)
                g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_DX_FULL;
            else
                g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_DX_QUICK;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (FILTER_SWITCH){
            //Exit from the Procedure
            g_bCalFull=false;
            wait_key_filter_up(30);
            g_ucCalibrationStatus = CALIBRATION_END_BUCKY_DX_ACQ;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;

    case CALIBRATION_SHOW_BUCKY_DX_QUICK:
        g_ucCalibrationStatus = CALIBRATION_WAIT_BUCKY_DX_QUICK;
        s_temp16 = 2;
        g_ucNMisCassetteDx = 2;
        //break;    //Fall Through

    case CALIBRATION_WAIT_BUCKY_DX_QUICK:
        g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_DX_ACQ;
        break;

    case CALIBRATION_SHOW_BUCKY_DX_FULL:
        g_ucCalibrationStatus = CALIBRATION_WAIT_BUCKY_DX_FULL;
        if (ee_ConfigColl.ucLanguage == ITA_LANG){
            sys_print_msg (0x80,"Imposta Numero      ");
            sys_print_msg (0xC0,"Cassette Lato Dx    ");
        }
        if (ee_ConfigColl.ucLanguage == ENG_LANG){
            sys_print_msg (0x80,"Set Number of       ");
            sys_print_msg (0xC0,"Right S. Buckies    ");
        }
        sys_print_byte(0xD0,s_temp16,false);
        g_wCrossMagnitude = 0;
        //break;    //Fall Through

    case CALIBRATION_WAIT_BUCKY_DX_FULL:
        nA = EncoderCalib(&g_wCrossMagnitude, 2);
        if (nA)
        {
            s_temp16 += nA;
            if (s_temp16 > MAX_CASSETTE)
                s_temp16 = MAX_CASSETTE;
            if (s_temp16 < MIN_CASSETTE)
                s_temp16 = MIN_CASSETTE;
            g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_DX_FULL;
            break;
        }
        if (LAMP_SWITCH){
            s_ucCassetteCounter=0;
            s_ucCmCounterCross=MIN_BUCKY_CM;
            s_ucInCounterCross=MIN_BUCKY_IN;
            s_ucCmCounterLong=MIN_BUCKY_CM;
            s_ucInCounterLong=MIN_BUCKY_IN;
            g_ucNMisCassetteDx = s_temp16;
            wait_key_exit_up(250);
            g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_DX_ACQ;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        if (FILTER_SWITCH){
            //Exit from the Procedure
            wait_key_filter_up(30);
            s_ucCassetteCounter=0;
            s_ucCmCounterCross=MIN_BUCKY_CM;
            s_ucInCounterCross=MIN_BUCKY_IN;
            s_ucCmCounterLong=MIN_BUCKY_CM;
            s_ucInCounterLong=MIN_BUCKY_IN;
            g_ucCalibrationStatus = CALIBRATION_END_BUCKY_DX_ACQ;
            g_wCrossMagnitude = 0;
            g_wLongMagnitude = 0;
            break;
        }
        break;

    case CALIBRATION_SHOW_BUCKY_DX_ACQ:
        if (ee_ConfigColl.ucCmUnit){
            //Cm
            if (s_ucCassetteCounter<g_ucNMisCassetteDx){
                sys_print_msg (0x80,"Cross .D Cm ..:     ");
                sys_print_msg (0xC0,"Long  .D Cm ..:     ");
                sys_print_byte(0x84,s_ucCassetteCounter+1,false);
                sys_print_byte(0xC4,s_ucCassetteCounter+1,false);
                sys_print_byte(0x8B,s_ucCmCounterCross,false);
                sys_print_byte(0xCB,s_ucCmCounterLong,false);
            }
        } else {
            //Inches
            if (s_ucCassetteCounter<g_ucNMisCassetteDx){
                sys_print_msg (0x80,"Cross .D In ..:     ");
                sys_print_msg (0xC0,"Long  .D In ..:     ");
                sys_print_byte(0x84,s_ucCassetteCounter+1,false);
                sys_print_byte(0xC4,s_ucCassetteCounter+1,false);
                sys_print_byte(0x8B,s_ucInCounterCross,false);
                sys_print_byte(0xCB,s_ucInCounterLong,false);
            }
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_BUCKY_DX_ACQ;
        //break;    //Fall Through

    case CALIBRATION_WAIT_BUCKY_DX_ACQ:
        if (ee_ConfigColl.ucCmUnit){
            if (s_ucCassetteCounter<g_ucNMisCassetteDx){
                sys_print_number(0x8F,RemoteAdc[RIGHT_CROSS],4,0xff,false);
                sys_print_number(0xCF,RemoteAdc[RIGHT_LONG],4,0xff,false);
                nA = EncoderCalib(&g_wCrossMagnitude, 2);
                if (nA)
                {
                    s_ucCmCounterCross += nA;
                    if (s_ucCmCounterCross > MAX_BUCKY_CM)
                        s_ucCmCounterCross = MAX_BUCKY_CM;
                    if (s_ucCmCounterCross < MIN_BUCKY_CM)
                        s_ucCmCounterCross = MIN_BUCKY_CM;
                    g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_DX_ACQ;
                    break;
                }
                nA = EncoderCalib(&g_wLongMagnitude, 2);
                if (nA)
                {
                    s_ucCmCounterLong += nA;
                    if (s_ucCmCounterLong > MAX_BUCKY_CM)
                        s_ucCmCounterLong = MAX_BUCKY_CM;
                    if (s_ucCmCounterLong < MIN_BUCKY_CM)
                        s_ucCmCounterLong = MIN_BUCKY_CM;
                    g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_DX_ACQ;
                    break;
                }
                if (LAMP_SWITCH){
                    ee_DxBuckyColl.uwDxBuckyCrossCm[s_ucCassetteCounter] = s_ucCmCounterCross;
                    ee_DxBuckyColl.uwDxBuckyCrossAdcCm[s_ucCassetteCounter] = RemoteAdc[RIGHT_CROSS];
                    ee_DxBuckyColl.uwDxBuckyLongCm[s_ucCassetteCounter] = s_ucCmCounterLong;
                    ee_DxBuckyColl.uwDxBuckyLongAdcCm[s_ucCassetteCounter] = RemoteAdc[RIGHT_LONG];
                    s_ulTempUpdateEeprom |= EEP_UPDATE_DXBUCKY;
                    s_ucCassetteCounter++;
                    wait_key_exit_up(250);
                    g_wCrossMagnitude = 0;
                    g_wLongMagnitude = 0;
                    g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_DX_ACQ;
                    break;
                }
            } else {
                for (ucIndex=s_ucCassetteCounter; ucIndex<MAX_CASSETTE; ucIndex++){
                    ee_DxBuckyColl.uwDxBuckyCrossCm[ucIndex] = 0;
                    ee_DxBuckyColl.uwDxBuckyCrossAdcCm[ucIndex] = 0;
                    ee_DxBuckyColl.uwDxBuckyLongCm[ucIndex] = 0;
                    ee_DxBuckyColl.uwDxBuckyLongAdcCm[ucIndex] = 0;
                }
                if (g_bCalFull){
                    s_ucCassetteCounter=0;
                    s_ucCmCounterCross=MIN_BUCKY_CM;
                    s_ucCmCounterLong=MIN_BUCKY_CM;
                    g_ucCalibrationStatus = CALIBRATION_END_BUCKY_DX_ACQ;
                    break;
                }
                ucCrossCounter=s_ucCassetteCounter;
                ucLongCounter=s_ucCassetteCounter;

                //Quick calibration
                nA = ee_DxBuckyColl.uwDxBuckyCrossAdcCm[1]-ee_DxBuckyColl.uwDxBuckyCrossAdcCm[0];
                nB = ee_DxBuckyColl.uwDxBuckyCrossCm[1]-ee_DxBuckyColl.uwDxBuckyCrossCm[0];
                nC = nA/nB;
                uwAlfaC = nC;
                nA = ee_DxBuckyColl.uwDxBuckyLongAdcCm[1]-ee_DxBuckyColl.uwDxBuckyLongAdcCm[0];
                nB = ee_DxBuckyColl.uwDxBuckyLongCm[1]-ee_DxBuckyColl.uwDxBuckyLongCm[0];
                nC = nA/nB;
                uwAlfaL = nC;

                for (ucIndex=0; ucIndex<MAX_CASSETTE; ucIndex++){
                    bInsert=true;
                    for (ucIndex2=0; ucIndex2<MAX_CASSETTE; ucIndex2++){
                        if (g_ucTableMisCrossCm[ucIndex]==ee_DxBuckyColl.uwDxBuckyCrossCm[ucIndex2]){
                            if (ee_DxBuckyColl.uwDxBuckyCrossCm[ucIndex2]){
                                bInsert=false;
                                break;
                            }
                        }
                    }
                    if (bInsert){
                        ee_DxBuckyColl.uwDxBuckyCrossCm[ucCrossCounter] = g_ucTableMisCrossCm[ucIndex];
                        nA = ee_DxBuckyColl.uwDxBuckyCrossCm[ucCrossCounter]-ee_DxBuckyColl.uwDxBuckyCrossCm[0];
                        ee_DxBuckyColl.uwDxBuckyCrossAdcCm[ucCrossCounter] = (uwAlfaC*nA)+ee_DxBuckyColl.uwDxBuckyCrossAdcCm[0];
                        ucCrossCounter++;
                    }
                    bInsert=true;
                    for (ucIndex2=0; ucIndex2<MAX_CASSETTE; ucIndex2++){
                        if (g_ucTableMisLongCm[ucIndex]==ee_DxBuckyColl.uwDxBuckyLongCm[ucIndex2]){
                            if (ee_DxBuckyColl.uwDxBuckyLongCm[ucIndex2]){
                                bInsert=false;
                                break;
                            }
                        }
                    }
                    if (bInsert){
                        ee_DxBuckyColl.uwDxBuckyLongCm[ucLongCounter] = g_ucTableMisLongCm[ucIndex];
                        nA = ee_DxBuckyColl.uwDxBuckyLongCm[ucLongCounter]-ee_DxBuckyColl.uwDxBuckyLongCm[0];
                        ee_DxBuckyColl.uwDxBuckyLongAdcCm[ucLongCounter] = (uwAlfaL*nA)+ee_DxBuckyColl.uwDxBuckyLongAdcCm[0];
                        ucLongCounter++;
                    }
                }
                s_ucCassetteCounter=0;
                s_ucCmCounterCross=MIN_BUCKY_CM;
                s_ucCmCounterLong=MIN_BUCKY_CM;
                g_ucCalibrationStatus = CALIBRATION_END_BUCKY_DX_ACQ;
                break;
            }
        } else {
            //Inches
            if (s_ucCassetteCounter<g_ucNMisCassetteDx){
                sys_print_number(0x8F,RemoteAdc[RIGHT_CROSS],4,0xff,false);
                sys_print_number(0xCF,RemoteAdc[RIGHT_LONG],4,0xff,false);
                nA = EncoderCalib(&g_wCrossMagnitude, 2);
                if (nA)
                {
                    s_ucInCounterCross += nA;
                    if (s_ucInCounterCross > MAX_BUCKY_IN)
                        s_ucInCounterCross = MAX_BUCKY_IN;
                    if (s_ucInCounterCross < MIN_BUCKY_IN)
                        s_ucInCounterCross = MIN_BUCKY_IN;
                    g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_DX_ACQ;
                    break;
                }
                nA = EncoderCalib(&g_wLongMagnitude, 2);
                if (nA)
                {
                    s_ucInCounterLong += nA;
                    if (s_ucInCounterLong > MAX_BUCKY_IN)
                        s_ucInCounterLong = MAX_BUCKY_IN;
                    if (s_ucInCounterLong < MIN_BUCKY_IN)
                        s_ucInCounterLong = MIN_BUCKY_IN;
                    g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_DX_ACQ;
                    break;
                }
                if (LAMP_SWITCH){
                    ee_DxBuckyColl.uwDxBuckyCrossIn[s_ucCassetteCounter] = s_ucInCounterCross;
                    ee_DxBuckyColl.uwDxBuckyCrossAdcIn[s_ucCassetteCounter] = RemoteAdc[RIGHT_CROSS];
                    ee_DxBuckyColl.uwDxBuckyLongIn[s_ucCassetteCounter] = s_ucInCounterLong;
                    ee_DxBuckyColl.uwDxBuckyLongAdcIn[s_ucCassetteCounter] = RemoteAdc[RIGHT_LONG];
                    s_ulTempUpdateEeprom |= EEP_UPDATE_DXBUCKY;
                    s_ucCassetteCounter++;
                    wait_key_exit_up(250);
                    g_wCrossMagnitude = 0;
                    g_wLongMagnitude = 0;
                    g_ucCalibrationStatus = CALIBRATION_SHOW_BUCKY_DX_ACQ;
                    break;
                }
            } else {
                for (ucIndex=s_ucCassetteCounter; ucIndex<MAX_CASSETTE; ucIndex++){
                    ee_DxBuckyColl.uwDxBuckyCrossIn[ucIndex] = 0;
                    ee_DxBuckyColl.uwDxBuckyCrossAdcIn[ucIndex] = 0;
                    ee_DxBuckyColl.uwDxBuckyLongIn[ucIndex] = 0;
                    ee_DxBuckyColl.uwDxBuckyLongAdcIn[ucIndex] = 0;
                }
                if (g_bCalFull){
                    s_ucCassetteCounter=0;
                    s_ucInCounterCross=MIN_BUCKY_IN;
                    s_ucInCounterLong=MIN_BUCKY_IN;
                    g_ucCalibrationStatus = CALIBRATION_END_BUCKY_DX_ACQ;
                    break;
                }
                ucCrossCounter=s_ucCassetteCounter;
                ucLongCounter=s_ucCassetteCounter;

                //Quick calibration
                nA = ee_DxBuckyColl.uwDxBuckyCrossAdcIn[1]-ee_DxBuckyColl.uwDxBuckyCrossAdcIn[0];
                nB = ee_DxBuckyColl.uwDxBuckyCrossIn[1]-ee_DxBuckyColl.uwDxBuckyCrossIn[0];
                nC = nA/nB;
                uwAlfaC = nC;
                nA = ee_DxBuckyColl.uwDxBuckyLongAdcIn[1]-ee_DxBuckyColl.uwDxBuckyLongAdcIn[0];
                nB = ee_DxBuckyColl.uwDxBuckyLongIn[1]-ee_DxBuckyColl.uwDxBuckyLongIn[0];
                nC = nA/nB;
                uwAlfaL = nC;

                for (ucIndex=0; ucIndex<MAX_CASSETTE; ucIndex++){
                    bInsert=true;
                    for (ucIndex2=0; ucIndex2<MAX_CASSETTE; ucIndex2++){
                        if (g_ucTableMisCrossIn[ucIndex]==ee_DxBuckyColl.uwDxBuckyCrossIn[ucIndex2]){
                            if (ee_DxBuckyColl.uwDxBuckyCrossIn[ucIndex2]){
                                bInsert=false;
                                break;
                            }
                        }
                    }
                    if (bInsert){
                        ee_DxBuckyColl.uwDxBuckyCrossIn[ucCrossCounter] = g_ucTableMisCrossIn[ucIndex];
                        nA = ee_DxBuckyColl.uwDxBuckyCrossIn[ucCrossCounter]-ee_DxBuckyColl.uwDxBuckyCrossIn[0];
                        ee_DxBuckyColl.uwDxBuckyCrossAdcIn[ucCrossCounter] = (uwAlfaC*nA)+ee_DxBuckyColl.uwDxBuckyCrossAdcIn[0];
                        ucCrossCounter++;
                    }
                    bInsert=true;
                    for (ucIndex2=0; ucIndex2<MAX_CASSETTE; ucIndex2++){
                        if (g_ucTableMisLongIn[ucIndex]==ee_DxBuckyColl.uwDxBuckyLongIn[ucIndex2]){
                            if (ee_DxBuckyColl.uwDxBuckyLongIn[ucIndex2]){
                                bInsert=false;
                                break;
                            }
                        }
                    }
                    if (bInsert){
                        ee_DxBuckyColl.uwDxBuckyLongIn[ucLongCounter] = g_ucTableMisLongIn[ucIndex];
                        nA = ee_DxBuckyColl.uwDxBuckyLongIn[ucLongCounter]-ee_DxBuckyColl.uwDxBuckyLongIn[0];
                        ee_DxBuckyColl.uwDxBuckyLongAdcIn[ucLongCounter] = (uwAlfaL*nA)+ee_DxBuckyColl.uwDxBuckyLongAdcIn[0];
                        ucLongCounter++;
                    }
                }
                s_ucCassetteCounter=0;
                s_ucInCounterCross=MIN_BUCKY_IN;
                s_ucInCounterLong=MIN_BUCKY_IN;
                g_ucCalibrationStatus = CALIBRATION_END_BUCKY_DX_ACQ;
                break;
            }
        }
        break;
    case CALIBRATION_END_BUCKY_DX_ACQ:


//****************************************************************************************************

    case CALIBRATION_SHOW_NEXT:
                                    // ************************
                                    // **  FINE CALIBRAZIONE **
                                    // ************************
    case CALIBRATION_SHOW_END:
        if (ee_ConfigColl.ucLanguage == ITA_LANG){
            sys_print_msg (0x80,"Calibraz. terminata ");
            sys_print_msg (0xc0,"LAMP per RESET      ");
        }
        if (ee_ConfigColl.ucLanguage == ENG_LANG){
            sys_print_msg (0x80,"End of Calibration  ");
            sys_print_msg (0xc0,"Press LAMP to RESET ");
        }
        g_ucCalibrationStatus = CALIBRATION_WAIT_END;
        //break;    //Fall Through

    case CALIBRATION_WAIT_END:
        g_ulUpdateEeprom |= s_ulTempUpdateEeprom;
        g_ucCalibrationStatus = CALIBRATION_END;
        break;

    case CALIBRATION_END:       // attesa tasto LAMPADA per RESET
        if (LAMP_SWITCH)
        {
            wait_key_exit_up(250);
            __asm(" nop");
            NVIC_SystemReset();
            __asm(" nop");
        }
        break;
    }

}


