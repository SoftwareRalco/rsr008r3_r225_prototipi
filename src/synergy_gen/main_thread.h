/* generated thread header file - do not edit */
#ifndef MAIN_THREAD_H_
#define MAIN_THREAD_H_
#include "bsp_api.h"
#include "tx_api.h"
#include "hal_data.h"
#ifdef __cplusplus
extern "C" void main_thread_entry(void);
#else
extern void main_thread_entry(void);
#endif
#include "r_can.h"
#include "r_can_api.h"
#include "sf_i2c.h"
#include "sf_i2c_api.h"
#include "r_flash_lp.h"
#include "r_flash_api.h"
#include "r_gpt.h"
#include "r_timer_api.h"
#include "r_adc.h"
#include "r_adc_api.h"
#ifdef __cplusplus
extern "C"
{
#endif
/** CAN on CAN Instance. */
extern const can_instance_t g_can0;
#ifndef irq_can_callback
void irq_can_callback(can_callback_args_t *p_args);
#endif
#define CAN_NO_OF_MAILBOXES_g_can0 (32)
/* SF I2C on SF I2C Instance. */
extern const sf_i2c_instance_t g_sf_i2c_device_io;
/* SF I2C on SF I2C Instance. */
extern const sf_i2c_instance_t g_sf_i2c_device_dac;
/* Flash on Flash LP Instance. */
extern const flash_instance_t g_flash0;
#ifndef NULL
void NULL(flash_callback_args_t *p_args);
#endif
/** Timer on GPT Instance. */
extern const timer_instance_t g_timer_mot4;
#ifndef irq_motore_4_callback
void irq_motore_4_callback(timer_callback_args_t *p_args);
#endif
/** Timer on GPT Instance. */
extern const timer_instance_t g_timer_mot3;
#ifndef irq_motore_3_callback
void irq_motore_3_callback(timer_callback_args_t *p_args);
#endif
/** Timer on GPT Instance. */
extern const timer_instance_t g_timer_mot2;
#ifndef irq_motore_2_callback
void irq_motore_2_callback(timer_callback_args_t *p_args);
#endif
/** Timer on GPT Instance. */
extern const timer_instance_t g_timer_mot1;
#ifndef irq_motore_1_callback
void irq_motore_1_callback(timer_callback_args_t *p_args);
#endif
/** Timer on GPT Instance. */
extern const timer_instance_t g_timer_1ms;
#ifndef irq_timer_1ms
void irq_timer_1ms(timer_callback_args_t *p_args);
#endif
/** ADC on ADC Instance. */
extern const adc_instance_t g_adc0;
#ifndef NULL
void NULL(adc_callback_args_t *p_args);
#endif
extern TX_EVENT_FLAGS_GROUP g_can_flags;
#ifdef __cplusplus
} /* extern "C" */
#endif
#endif /* MAIN_THREAD_H_ */
