/* generated common header file - do not edit */
#ifndef COMMON_DATA_H_
#define COMMON_DATA_H_
#include <stdint.h>
#include "bsp_api.h"
#include "r_riic.h"
#include "r_i2c_api.h"
#include "r_i2c_api.h"
#include "sf_i2c.h"
#include "sf_i2c_api.h"
#include "r_elc.h"
#include "r_elc_api.h"
#include "r_cgc.h"
#include "r_cgc_api.h"
#include "r_ioport.h"
#include "r_ioport_api.h"
#include "r_fmi.h"
#include "r_fmi_api.h"
#ifdef __cplusplus
extern "C"
{
#endif
extern const i2c_cfg_t g_i2c1_cfg;
/** I2C on RIIC Instance. */
extern const i2c_master_instance_t g_i2c1;
#ifndef NULL
void NULL(i2c_callback_args_t *p_args);
#endif

extern riic_instance_ctrl_t g_i2c1_ctrl;
extern const riic_extended_cfg g_i2c1_extend;
#define SYNERGY_NOT_DEFINED (1)            
#if (SYNERGY_NOT_DEFINED == SYNERGY_NOT_DEFINED)
#define g_i2c1_P_TRANSFER_TX (NULL)
#else
#define g_i2c1_P_TRANSFER_TX (&SYNERGY_NOT_DEFINED)
#endif
#if (SYNERGY_NOT_DEFINED == SYNERGY_NOT_DEFINED)
#define g_i2c1_P_TRANSFER_RX (NULL)
#else
#define g_i2c1_P_TRANSFER_RX (&SYNERGY_NOT_DEFINED)
#endif
#undef SYNERGY_NOT_DEFINED
#define g_i2c1_P_EXTEND (&g_i2c1_extend)
extern sf_i2c_bus_t g_sf_i2c_bus1;
extern i2c_api_master_t const g_i2c_master_on_riic;

#define g_sf_i2c_bus1_CHANNEL        (1)
#define g_sf_i2c_bus1_RATE           (I2C_RATE_FAST)
#define g_sf_i2c_bus1_SLAVE          (0)
#define g_sf_i2c_bus1_ADDR_MODE      (I2C_ADDR_MODE_7BIT)          
#define g_sf_i2c_bus1_SDA_DELAY      (0)  
#define g_sf_i2c_bus1_P_CALLBACK     (NULL)
#define g_sf_i2c_bus1_P_CONTEXT      (&g_i2c1)
#define g_sf_i2c_bus1_RXI_IPL        ((2))
#define g_sf_i2c_bus1_TXI_IPL        ((2))
#define g_sf_i2c_bus1_TEI_IPL        ((2))            
#define g_sf_i2c_bus1_ERI_IPL        ((2))

/** These are obtained by macros in the I2C driver XMLs. */
#define g_sf_i2c_bus1_P_TRANSFER_TX  (g_i2c1_P_TRANSFER_TX)
#define g_sf_i2c_bus1_P_TRANSFER_RX  (g_i2c1_P_TRANSFER_RX)            
#define g_sf_i2c_bus1_P_EXTEND       (g_i2c1_P_EXTEND)
/** ELC Instance */
extern const elc_instance_t g_elc;
/** CGC Instance */
extern const cgc_instance_t g_cgc;
/** IOPORT Instance */
extern const ioport_instance_t g_ioport;
/** FMI on FMI Instance. */
extern const fmi_instance_t g_fmi;
void g_common_init(void);
#ifdef __cplusplus
} /* extern "C" */
#endif
#endif /* COMMON_DATA_H_ */
