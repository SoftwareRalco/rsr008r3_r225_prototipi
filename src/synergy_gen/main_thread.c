/* generated thread source file - do not edit */
#include "main_thread.h"

TX_THREAD main_thread;
void main_thread_create(void);
static void main_thread_func(ULONG thread_input);
static uint8_t main_thread_stack[2048] BSP_PLACE_IN_SECTION_V2(".stack.main_thread") BSP_ALIGN_VARIABLE_V2(BSP_STACK_ALIGNMENT);
void tx_startup_err_callback(void *p_instance, void *p_data);
void tx_startup_common_init(void);
can_bit_timing_cfg_t g_can0_bit_timing_cfg =
{ .baud_rate_prescaler = 3,
  .time_segment_1 = CAN_TIME_SEGMENT1_TQ12,
  .time_segment_2 = CAN_TIME_SEGMENT2_TQ3,
  .synchronization_jump_width = CAN_SYNC_JUMP_WIDTH_TQ1, };

uint32_t g_can0_mailbox_mask[CAN_NO_OF_MAILBOXES_g_can0 / 4] =
{ 0x1FFFFFFF,
#if CAN_NO_OF_MAILBOXES_g_can0 > 4
        0x1FFFFFFF,
#endif
#if CAN_NO_OF_MAILBOXES_g_can0 > 8
        0x1FFFFFFF,
        0x1FFFFFFF,
#endif
#if CAN_NO_OF_MAILBOXES_g_can0 > 16
        0x1FFFFFFF,
        0x1FFFFFFF,
        0x1FFFFFFF,
        0x1FFFFFFF,
#endif
    };

static const can_extended_cfg_t g_can0_extended_cfg =
{ .clock_source = CAN_CLOCK_SOURCE_PCLKB, .p_mailbox_mask = g_can0_mailbox_mask, };

can_mailbox_t g_can0_mailbox[CAN_NO_OF_MAILBOXES_g_can0] =
{
{ .mailbox_id = 0, .mailbox_type = CAN_MAILBOX_TRANSMIT, .frame_type = CAN_FRAME_TYPE_DATA },
  { .mailbox_id = 0x7A0, .mailbox_type = CAN_MAILBOX_RECEIVE, .frame_type = CAN_FRAME_TYPE_DATA },
  { .mailbox_id = 0, .mailbox_type = CAN_MAILBOX_RECEIVE, .frame_type = CAN_FRAME_TYPE_DATA, },
  { .mailbox_id = 0, .mailbox_type = CAN_MAILBOX_RECEIVE, .frame_type = CAN_FRAME_TYPE_DATA },
#if CAN_NO_OF_MAILBOXES_g_can0 > 4
        {
            .mailbox_id = 0x780,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA
        },
        {
            .mailbox_id = 5,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA
        },
        {
            .mailbox_id = 6,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA
        },
        {
            .mailbox_id = 7,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA
        },
#endif
#if CAN_NO_OF_MAILBOXES_g_can0 > 8
        {
            .mailbox_id = 8,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA
        },
        {
            .mailbox_id = 9,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA
        },
        {
            .mailbox_id = 10,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA
        },
        {
            .mailbox_id = 11,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA
        },
        {
            .mailbox_id = 12,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA,
        },
        {
            .mailbox_id = 13,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA
        },
        {
            .mailbox_id = 14,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA
        },
        {
            .mailbox_id = 15,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA
        },
#endif  
#if CAN_NO_OF_MAILBOXES_g_can0 > 16
        {
            .mailbox_id = 16,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA
        },

        {
            .mailbox_id = 17,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA
        },
        {
            .mailbox_id = 18,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA
        },
        {
            .mailbox_id = 19,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA
        },
        {
            .mailbox_id = 20,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA
        },
        {
            .mailbox_id = 21,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA
        },
        {
            .mailbox_id = 22,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA,
        },
        {
            .mailbox_id = 23,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA
        },
        {
            .mailbox_id = 24,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA
        },
        {
            .mailbox_id = 25,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA
        },
        {
            .mailbox_id = 26,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA
        },
        {
            .mailbox_id = 27,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA
        },
        {
            .mailbox_id = 28,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA
        },
        {
            .mailbox_id = 29,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA
        },
        {
            .mailbox_id = 30,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA
        },
        {
            .mailbox_id = 31,
            .mailbox_type = CAN_MAILBOX_RECEIVE,
            .frame_type = CAN_FRAME_TYPE_DATA
        }
#endif  
    };

#if (5) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_can0) && !defined(SSP_SUPPRESS_ISR_CAN0)
SSP_VECTOR_DEFINE_CHAN(can_error_isr, CAN, ERROR, 0);
#endif
#endif
#if (5) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_can0) && !defined(SSP_SUPPRESS_ISR_CAN0)
SSP_VECTOR_DEFINE_CHAN(can_mailbox_rx_isr, CAN, MAILBOX_RX, 0);
#endif
#endif
#if (5) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_can0) && !defined(SSP_SUPPRESS_ISR_CAN0)
SSP_VECTOR_DEFINE_CHAN(can_mailbox_tx_isr, CAN, MAILBOX_TX, 0);
#endif
#endif

static can_instance_ctrl_t g_can0_ctrl;
static const can_cfg_t g_can0_cfg =
{ .channel = 0, .p_bit_timing = &g_can0_bit_timing_cfg, .id_mode = CAN_ID_MODE_STANDARD, .mailbox_count =
          CAN_NO_OF_MAILBOXES_g_can0,
  .p_mailbox = g_can0_mailbox, .message_mode = CAN_MESSAGE_MODE_OVERWRITE, .p_callback = irq_can_callback, .p_extend =
          &g_can0_extended_cfg,
  .p_context = &g_can0, .error_ipl = (5), .mailbox_tx_ipl = (5), .mailbox_rx_ipl = (5), };
/* Instance structure to use this module. */
const can_instance_t g_can0 =
{ .p_ctrl = &g_can0_ctrl, .p_cfg = &g_can0_cfg, .p_api = &g_can_on_can };
/** Get driver cfg from bus and use all same settings except slave address and addressing mode. */
const i2c_cfg_t g_sf_i2c_device_io_i2c_cfg =
{ .channel = g_sf_i2c_bus1_CHANNEL,
  .rate = g_sf_i2c_bus1_RATE,
  .slave = 0x20,
  .addr_mode = I2C_ADDR_MODE_7BIT,
  .sda_delay = g_sf_i2c_bus1_SDA_DELAY,
  .p_transfer_tx = g_sf_i2c_bus1_P_TRANSFER_TX,
  .p_transfer_rx = g_sf_i2c_bus1_P_TRANSFER_RX,
  .p_callback = g_sf_i2c_bus1_P_CALLBACK,
  .p_context = g_sf_i2c_bus1_P_CONTEXT,
  .rxi_ipl = g_sf_i2c_bus1_RXI_IPL,
  .txi_ipl = g_sf_i2c_bus1_TXI_IPL,
  .tei_ipl = g_sf_i2c_bus1_TEI_IPL,
  .eri_ipl = g_sf_i2c_bus1_ERI_IPL,
  .p_extend = g_sf_i2c_bus1_P_EXTEND, };

sf_i2c_instance_ctrl_t g_sf_i2c_device_io_ctrl =
{ .p_lower_lvl_ctrl = &g_i2c1_ctrl, };
const sf_i2c_cfg_t g_sf_i2c_device_io_cfg =
{ .p_bus = (sf_i2c_bus_t *) &g_sf_i2c_bus1, .p_lower_lvl_cfg = &g_sf_i2c_device_io_i2c_cfg, };
/* Instance structure to use this module. */
const sf_i2c_instance_t g_sf_i2c_device_io =
{ .p_ctrl = &g_sf_i2c_device_io_ctrl, .p_cfg = &g_sf_i2c_device_io_cfg, .p_api = &g_sf_i2c_on_sf_i2c };
/** Get driver cfg from bus and use all same settings except slave address and addressing mode. */
const i2c_cfg_t g_sf_i2c_device_dac_i2c_cfg =
{ .channel = g_sf_i2c_bus1_CHANNEL,
  .rate = g_sf_i2c_bus1_RATE,
  .slave = 0x60,
  .addr_mode = I2C_ADDR_MODE_7BIT,
  .sda_delay = g_sf_i2c_bus1_SDA_DELAY,
  .p_transfer_tx = g_sf_i2c_bus1_P_TRANSFER_TX,
  .p_transfer_rx = g_sf_i2c_bus1_P_TRANSFER_RX,
  .p_callback = g_sf_i2c_bus1_P_CALLBACK,
  .p_context = g_sf_i2c_bus1_P_CONTEXT,
  .rxi_ipl = g_sf_i2c_bus1_RXI_IPL,
  .txi_ipl = g_sf_i2c_bus1_TXI_IPL,
  .tei_ipl = g_sf_i2c_bus1_TEI_IPL,
  .eri_ipl = g_sf_i2c_bus1_ERI_IPL,
  .p_extend = g_sf_i2c_bus1_P_EXTEND, };

sf_i2c_instance_ctrl_t g_sf_i2c_device_dac_ctrl =
{ .p_lower_lvl_ctrl = &g_i2c1_ctrl, };
const sf_i2c_cfg_t g_sf_i2c_device_dac_cfg =
{ .p_bus = (sf_i2c_bus_t *) &g_sf_i2c_bus1, .p_lower_lvl_cfg = &g_sf_i2c_device_dac_i2c_cfg, };
/* Instance structure to use this module. */
const sf_i2c_instance_t g_sf_i2c_device_dac =
{ .p_ctrl = &g_sf_i2c_device_dac_ctrl, .p_cfg = &g_sf_i2c_device_dac_cfg, .p_api = &g_sf_i2c_on_sf_i2c };
#if (10) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_flash0) && !defined(SSP_SUPPRESS_ISR_FLASH)
SSP_VECTOR_DEFINE( fcu_frdyi_isr, FCU, FRDYI);
#endif
#endif
flash_lp_instance_ctrl_t g_flash0_ctrl;
const flash_cfg_t g_flash0_cfg =
{ .data_flash_bgo = true, .p_callback = NULL, .p_context = &g_flash0, .irq_ipl = (10),

};
/* Instance structure to use this module. */
const flash_instance_t g_flash0 =
{ .p_ctrl = &g_flash0_ctrl, .p_cfg = &g_flash0_cfg, .p_api = &g_flash_on_flash_lp };
#if (1) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_timer_mot4) && !defined(SSP_SUPPRESS_ISR_GPT4)
SSP_VECTOR_DEFINE_CHAN(gpt_counter_overflow_isr, GPT, COUNTER_OVERFLOW, 4);
#endif
#endif
static gpt_instance_ctrl_t g_timer_mot4_ctrl;
static const timer_on_gpt_cfg_t g_timer_mot4_extend =
{ .gtioca =
{ .output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW },
  .gtiocb =
  { .output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW },
  .shortest_pwm_signal = GPT_SHORTEST_LEVEL_OFF, };
static const timer_cfg_t g_timer_mot4_cfg =
{ .mode = TIMER_MODE_PERIODIC, .period = 20000, .unit = TIMER_UNIT_PERIOD_USEC, .duty_cycle = 50, .duty_cycle_unit =
          TIMER_PWM_UNIT_RAW_COUNTS,
  .channel = 4, .autostart = true, .p_callback = irq_motore_4_callback, .p_context = &g_timer_mot4, .p_extend =
          &g_timer_mot4_extend,
  .irq_ipl = (1), };
/* Instance structure to use this module. */
const timer_instance_t g_timer_mot4 =
{ .p_ctrl = &g_timer_mot4_ctrl, .p_cfg = &g_timer_mot4_cfg, .p_api = &g_timer_on_gpt };
#if (1) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_timer_mot3) && !defined(SSP_SUPPRESS_ISR_GPT3)
SSP_VECTOR_DEFINE_CHAN(gpt_counter_overflow_isr, GPT, COUNTER_OVERFLOW, 3);
#endif
#endif
static gpt_instance_ctrl_t g_timer_mot3_ctrl;
static const timer_on_gpt_cfg_t g_timer_mot3_extend =
{ .gtioca =
{ .output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW },
  .gtiocb =
  { .output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW },
  .shortest_pwm_signal = GPT_SHORTEST_LEVEL_OFF, };
static const timer_cfg_t g_timer_mot3_cfg =
{ .mode = TIMER_MODE_PERIODIC, .period = 20000, .unit = TIMER_UNIT_PERIOD_USEC, .duty_cycle = 50, .duty_cycle_unit =
          TIMER_PWM_UNIT_RAW_COUNTS,
  .channel = 3, .autostart = true, .p_callback = irq_motore_3_callback, .p_context = &g_timer_mot3, .p_extend =
          &g_timer_mot3_extend,
  .irq_ipl = (1), };
/* Instance structure to use this module. */
const timer_instance_t g_timer_mot3 =
{ .p_ctrl = &g_timer_mot3_ctrl, .p_cfg = &g_timer_mot3_cfg, .p_api = &g_timer_on_gpt };
#if (1) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_timer_mot2) && !defined(SSP_SUPPRESS_ISR_GPT2)
SSP_VECTOR_DEFINE_CHAN(gpt_counter_overflow_isr, GPT, COUNTER_OVERFLOW, 2);
#endif
#endif
static gpt_instance_ctrl_t g_timer_mot2_ctrl;
static const timer_on_gpt_cfg_t g_timer_mot2_extend =
{ .gtioca =
{ .output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW },
  .gtiocb =
  { .output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW },
  .shortest_pwm_signal = GPT_SHORTEST_LEVEL_OFF, };
static const timer_cfg_t g_timer_mot2_cfg =
{ .mode = TIMER_MODE_PERIODIC, .period = 20000, .unit = TIMER_UNIT_PERIOD_USEC, .duty_cycle = 50, .duty_cycle_unit =
          TIMER_PWM_UNIT_RAW_COUNTS,
  .channel = 2, .autostart = true, .p_callback = irq_motore_2_callback, .p_context = &g_timer_mot2, .p_extend =
          &g_timer_mot2_extend,
  .irq_ipl = (1), };
/* Instance structure to use this module. */
const timer_instance_t g_timer_mot2 =
{ .p_ctrl = &g_timer_mot2_ctrl, .p_cfg = &g_timer_mot2_cfg, .p_api = &g_timer_on_gpt };
#if (1) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_timer_mot1) && !defined(SSP_SUPPRESS_ISR_GPT1)
SSP_VECTOR_DEFINE_CHAN(gpt_counter_overflow_isr, GPT, COUNTER_OVERFLOW, 1);
#endif
#endif
static gpt_instance_ctrl_t g_timer_mot1_ctrl;
static const timer_on_gpt_cfg_t g_timer_mot1_extend =
{ .gtioca =
{ .output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW },
  .gtiocb =
  { .output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW },
  .shortest_pwm_signal = GPT_SHORTEST_LEVEL_OFF, };
static const timer_cfg_t g_timer_mot1_cfg =
{ .mode = TIMER_MODE_PERIODIC, .period = 20000, .unit = TIMER_UNIT_PERIOD_USEC, .duty_cycle = 50, .duty_cycle_unit =
          TIMER_PWM_UNIT_RAW_COUNTS,
  .channel = 1, .autostart = true, .p_callback = irq_motore_1_callback, .p_context = &g_timer_mot1, .p_extend =
          &g_timer_mot1_extend,
  .irq_ipl = (1), };
/* Instance structure to use this module. */
const timer_instance_t g_timer_mot1 =
{ .p_ctrl = &g_timer_mot1_ctrl, .p_cfg = &g_timer_mot1_cfg, .p_api = &g_timer_on_gpt };
#if (4) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_timer_1ms) && !defined(SSP_SUPPRESS_ISR_GPT0)
SSP_VECTOR_DEFINE_CHAN(gpt_counter_overflow_isr, GPT, COUNTER_OVERFLOW, 0);
#endif
#endif
static gpt_instance_ctrl_t g_timer_1ms_ctrl;
static const timer_on_gpt_cfg_t g_timer_1ms_extend =
{ .gtioca =
{ .output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW },
  .gtiocb =
  { .output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW },
  .shortest_pwm_signal = GPT_SHORTEST_LEVEL_OFF, };
static const timer_cfg_t g_timer_1ms_cfg =
{ .mode = TIMER_MODE_PERIODIC, .period = 1, .unit = TIMER_UNIT_PERIOD_MSEC, .duty_cycle = 50, .duty_cycle_unit =
          TIMER_PWM_UNIT_RAW_COUNTS,
  .channel = 0, .autostart = true, .p_callback = irq_timer_1ms, .p_context = &g_timer_1ms, .p_extend =
          &g_timer_1ms_extend,
  .irq_ipl = (4), };
/* Instance structure to use this module. */
const timer_instance_t g_timer_1ms =
{ .p_ctrl = &g_timer_1ms_ctrl, .p_cfg = &g_timer_1ms_cfg, .p_api = &g_timer_on_gpt };
#if (BSP_IRQ_DISABLED) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_adc0) && !defined(SSP_SUPPRESS_ISR_ADC0)
SSP_VECTOR_DEFINE_CHAN(adc_scan_end_isr, ADC, SCAN_END, 0);
#endif
#endif
#if (BSP_IRQ_DISABLED) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_adc0) && !defined(SSP_SUPPRESS_ISR_ADC0)
SSP_VECTOR_DEFINE_CHAN(adc_scan_end_b_isr, ADC, SCAN_END_B, 0);
#endif
#endif
adc_instance_ctrl_t g_adc0_ctrl;
const adc_cfg_t g_adc0_cfg =
{ .unit = 0,
  .mode = ADC_MODE_SINGLE_SCAN,
  .resolution = ADC_RESOLUTION_14_BIT,
  .alignment = ADC_ALIGNMENT_RIGHT,
  .add_average_count = ADC_ADD_OFF,
  .clearing = ADC_CLEAR_AFTER_READ_ON,
  .trigger = ADC_TRIGGER_SOFTWARE,
  .trigger_group_b = ADC_TRIGGER_SYNC_ELC,
  .p_callback = NULL,
  .p_context = &g_adc0,
  .scan_end_ipl = (BSP_IRQ_DISABLED),
  .scan_end_b_ipl = (BSP_IRQ_DISABLED),
  .calib_adc_skip = false, };
const adc_channel_cfg_t g_adc0_channel_cfg =
{ .scan_mask = (uint32_t) (
        ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0)
                | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0)
                | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0)
                | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0)
                | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | (0)),
  /** Group B channel mask is right shifted by 32 at the end to form the proper mask */
  .scan_mask_group_b = (uint32_t) (
          (((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0)
                  | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0)
                  | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0)
                  | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0)
                  | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0) | (0)) >> 32),
  .priority_group_a = ADC_GROUP_A_PRIORITY_OFF, .add_mask = (uint32_t) (
          (0) | (0) | (0) | (0) | (0) | (0) | (0) | (0) | (0) | (0) | (0) | (0) | (0) | (0) | (0) | (0) | (0) | (0)
                  | (0) | (0) | (0) | (0) | (0) | (0) | (0) | (0) | (0) | (0) | (0) | (0)),
  .sample_hold_mask = (uint32_t) ((0) | (0) | (0)), .sample_hold_states = 24, };
/* Instance structure to use this module. */
const adc_instance_t g_adc0 =
{ .p_ctrl = &g_adc0_ctrl, .p_cfg = &g_adc0_cfg, .p_channel_cfg = &g_adc0_channel_cfg, .p_api = &g_adc_on_adc };
TX_EVENT_FLAGS_GROUP g_can_flags;
extern bool g_ssp_common_initialized;
extern uint32_t g_ssp_common_thread_count;
extern TX_SEMAPHORE g_ssp_common_initialized_semaphore;

void main_thread_create(void)
{
    /* Increment count so we will know the number of ISDE created threads. */
    g_ssp_common_thread_count++;

    /* Initialize each kernel object. */
    UINT err_g_can_flags;
    err_g_can_flags = tx_event_flags_create (&g_can_flags, (CHAR *) "Can Status");
    if (TX_SUCCESS != err_g_can_flags)
    {
        tx_startup_err_callback (&g_can_flags, 0);
    }

    UINT err;
    err = tx_thread_create (&main_thread, (CHAR *) "Main Thread", main_thread_func, (ULONG) NULL, &main_thread_stack,
                            2048, 1, 1, 1, TX_AUTO_START);
    if (TX_SUCCESS != err)
    {
        tx_startup_err_callback (&main_thread, 0);
    }
}

static void main_thread_func(ULONG thread_input)
{
    /* Not currently using thread_input. */
    SSP_PARAMETER_NOT_USED (thread_input);

    /* Initialize common components */
    tx_startup_common_init ();

    /* Initialize each module instance. */

    /* Enter user code for this thread. */
    main_thread_entry ();
}
