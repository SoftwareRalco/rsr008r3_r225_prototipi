/* generated common source file - do not edit */
#include "common_data.h"
#if !defined(SSP_SUPPRESS_ISR_g_i2c1) && !defined(SSP_SUPPRESS_ISR_IIC1)
SSP_VECTOR_DEFINE_CHAN(iic_rxi_isr, IIC, RXI, 1);
#endif
#if !defined(SSP_SUPPRESS_ISR_g_i2c1) && !defined(SSP_SUPPRESS_ISR_IIC1)
SSP_VECTOR_DEFINE_CHAN(iic_txi_isr, IIC, TXI, 1);
#endif
#if !defined(SSP_SUPPRESS_ISR_g_i2c1) && !defined(SSP_SUPPRESS_ISR_IIC1)
SSP_VECTOR_DEFINE_CHAN(iic_tei_isr, IIC, TEI, 1);
#endif
#if !defined(SSP_SUPPRESS_ISR_g_i2c1) && !defined(SSP_SUPPRESS_ISR_IIC1)
SSP_VECTOR_DEFINE_CHAN(iic_eri_isr, IIC, ERI, 1);
#endif
riic_instance_ctrl_t g_i2c1_ctrl;
const riic_extended_cfg g_i2c1_extend =
{ .timeout_mode = RIIC_TIMEOUT_MODE_SHORT, };
const i2c_cfg_t g_i2c1_cfg =
{ .channel = 1, .rate = I2C_RATE_FAST, .slave = 0, .addr_mode = I2C_ADDR_MODE_7BIT,
#define SYNERGY_NOT_DEFINED (1)            
#if (SYNERGY_NOT_DEFINED == SYNERGY_NOT_DEFINED)
  .p_transfer_tx = NULL,
#else
  .p_transfer_tx = &SYNERGY_NOT_DEFINED,
#endif
#if (SYNERGY_NOT_DEFINED == SYNERGY_NOT_DEFINED)
  .p_transfer_rx = NULL,
#else
  .p_transfer_rx = &SYNERGY_NOT_DEFINED,
#endif
#undef SYNERGY_NOT_DEFINED	
  .p_callback = NULL,
  .p_context = (void *) &g_i2c1, .rxi_ipl = (2), .txi_ipl = (2), .tei_ipl = (2), .eri_ipl = (2), .p_extend =
          &g_i2c1_extend, };
/* Instance structure to use this module. */
const i2c_master_instance_t g_i2c1 =
{ .p_ctrl = &g_i2c1_ctrl, .p_cfg = &g_i2c1_cfg, .p_api = &g_i2c_master_on_riic };
static TX_MUTEX sf_bus_mutex_g_sf_i2c_bus1;
static TX_EVENT_FLAGS_GROUP sf_bus_eventflag_g_sf_i2c_bus1;
static sf_i2c_instance_ctrl_t *sf_curr_ctrl_g_sf_i2c_bus1;
static sf_i2c_instance_ctrl_t *sf_curr_bus_ctrl_g_sf_i2c_bus1;
sf_i2c_bus_t g_sf_i2c_bus1 =
{ .p_bus_name = (uint8_t *) "g_sf_i2c_bus1",
  .channel = 1,
  .p_lock_mutex = &sf_bus_mutex_g_sf_i2c_bus1,
  .p_sync_eventflag = &sf_bus_eventflag_g_sf_i2c_bus1,
  .pp_curr_ctrl = (sf_i2c_ctrl_t **) &sf_curr_ctrl_g_sf_i2c_bus1,
  .p_lower_lvl_api = (i2c_api_master_t *) &g_i2c_master_on_riic,
  .device_count = 0,
  .pp_curr_bus_ctrl = (sf_i2c_ctrl_t **) &sf_curr_bus_ctrl_g_sf_i2c_bus1, };
const elc_instance_t g_elc =
{ .p_api = &g_elc_on_elc, .p_cfg = NULL };
const cgc_instance_t g_cgc =
{ .p_api = &g_cgc_on_cgc, .p_cfg = NULL };
const ioport_instance_t g_ioport =
{ .p_api = &g_ioport_on_ioport, .p_cfg = NULL };
/* Instance structure to use this module. */
const fmi_instance_t g_fmi =
{ .p_api = &g_fmi_on_fmi };
void g_common_init(void)
{
}
