#include "main_thread.h"

#define MAIN_EXT
#include "include.h"


#define calcTicks(x)    ((x * TX_TIMER_TICKS_PER_SECOND) / 1000)        // x in ms


/* Main Thread entry function */
void main_thread_entry(void)
{
    uint8_t level = IOPORT_LEVEL_LOW;
    ssp_err_t api_err;

    mainApplication();


    /* TODO: add your own code here */
    while (1)
    {
        if (level == IOPORT_LEVEL_HIGH)
            level = IOPORT_LEVEL_LOW;
        else
            level = IOPORT_LEVEL_HIGH;
        g_ioport.p_api->pinWrite(PIN_LED_D31, level);

        tx_thread_sleep(calcTicks(500));
    }
}
