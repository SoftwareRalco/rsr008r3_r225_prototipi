//--------------------------------------------------------------
// PRIMA STESURA
// Riva ing. Franco M.
// Besana Brianza (MI)
//
// REVISIONI SUCCESSIVE
// Giovanni Corvini
// Melegnano (MI)
//--------------------------------------------------------------
//
// PROJECT:	  RSR008 - Low Cost Collimator
// FILE:      sys_lcd_driver.h
// REVISIONS: 0.00 - 22/09/2008 - First Definition
//
//--------------------------------------------------------------



//*****************************
//**     DEFINIZIONI         **
//*****************************

#define LEN_DISPLAY         40      // numero di caratteri del display


enum {
    ST_LCD_L1 = 0,
    ST_LCD_WL1,
    ST_LCD_L2,
    ST_LCD_WL2
};


/**********************************************************************************/
/* LCD commands - use LCD_write function to write these commands to the LCD.	  */
/**********************************************************************************/
/* Clear LCD display and home cursor */
#define LCD_CLEAR        0x01
/* move cursor to line 1 */
#define LCD_HOME_L1      0x80
/* move cursor to line 2 */
#define LCD_HOME_L2      0xC0
/* Cursor auto decrement after R/W */
#define CURSOR_MODE_DEC  0x04
/* Cursor auto increment after R/W */
#define CURSOR_MODE_INC  0x06
/* Setup, 4 bits,2 lines, 5X7 */
#define FUNCTION_SET     0x28
/* Display ON with Cursor */
#define LCD_CURSOR_ON    0x0E
/* Display ON with Cursor off */
#define LCD_CURSOR_OFF   0x0C
/* Display on with blinking cursor */
#define LCD_CURSOR_BLINK 0x0D
/*Move Cursor Left One Position */
#define LCD_CURSOR_LEFT  0x10
/* Move Cursor Right One Position */
#define LCD_CURSOR_RIGHT 0x14

#define LCD_DISPLAY_ON   0x04
#define LCD_TWO_LINE     0x08


#define LCD_4_BIT           // togliere definizione per uso a 8 bit

#define MODO_1_NIBBLE         0     // scrive solo il nibble alto
#define MODO_2_NIBBLE         1     // scrive 2 nibble




#ifdef _SYS_LCD_
#define lcd_ext
#else
#define lcd_ext extern
#endif

lcd_ext UINT8 lcd_stop;
lcd_ext UINT8 buffer_lcd[LEN_DISPLAY + 10];       // contiene quello visualizzato su LCD + zeri
lcd_ext UINT8 punt_buff_lcd;       // puntatore al buffer sopra
lcd_ext UINT8 stato_write_char;
lcd_ext UINT8 ptLcdMain;




//****************************
//  PROTOTIPI DI FUNZIONE
//****************************
void lcd_init(void);
void lcd_data(UINT8 data);
void lcd_command_4bit(UINT8 modo, UINT8 data);

void write_char(void);

void sys_lcd_free(void);
void sys_lcd_mode(UINT8 c);
void sys_lcd_out(UINT8 c);
void sys_print_msg (char r, const char s[]);
void sys_print_number (char r, uint16_t n, char d, char p, bool b);
void sys_lcd_init(void);

void sys_print_msg (char r, const char s[]);
void sys_print_integer (char r, uint16_t n, uchar c, bool b);
void sys_print_byte (char r, uchar n, bool b);

