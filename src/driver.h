

#ifdef DRIVER_H
#define drv_ext

uint32_t pin_stepper_dmode0[MAX_STEPPER] = {       // deve seguire l'enum dei motori
    PIN_STEP_A_DMODE0,
    PIN_STEP_B_DMODE0,
    PIN_STEP_C_DMODE0,
    PIN_STEP_D_DMODE0
};

uint32_t pin_stepper_dmode1[MAX_STEPPER] = {       // deve seguire l'enum dei motori
    PIN_STEP_A_DMODE1,
    PIN_STEP_B_DMODE1,
    PIN_STEP_C_DMODE1,
    PIN_STEP_D_DMODE1
};

uint32_t pin_stepper_dmode2[MAX_STEPPER] = {       // deve seguire l'enum dei motori
    PIN_STEP_A_DMODE2,
    PIN_STEP_B_DMODE2,
    PIN_STEP_C_DMODE2,
    PIN_STEP_D_DMODE2
};

uint32_t pin_stepper_reset[MAX_STEPPER] = {       // deve seguire l'enum dei motori
    PIN_STEP_A_RESET,
    PIN_STEP_B_RESET,
    PIN_STEP_C_RESET,
    PIN_STEP_D_RESET
};

uint16_t DAC_VALORI_RUN[MAX_STEPPER] = {
   2250,
   2250,
   4095,
   2250
};

uint16_t DAC_VALORI_IDLE[MAX_STEPPER] = {
  1000,
  1000,
  1000,
  1000
};
#else
#define drv_ext extern
extern uint32_t pin_stepper_dmode0[MAX_STEPPER];
extern uint32_t pin_stepper_dmode1[MAX_STEPPER];
extern uint32_t pin_stepper_dmode2[MAX_STEPPER];
extern uint32_t pin_stepper_reset[MAX_STEPPER];
extern uint16_t DAC_VALORI_RUN[MAX_STEPPER];
extern uint16_t DAC_VALORI_IDLE[MAX_STEPPER];
#endif





//*******************
//**  DEFINIZIONI  **
//*******************


#define ADDR_I2C_DAC        0xC0



enum {
    BLOCK_LED_NONE = 0,
    BLOCK_LED_ON,
    BLOCK_LED_OFF,
    BLOCK_LED_1_4_BLINK
};




/**********************************
**  DEFINIZIONE STATO DEI LED
***********************************/
#define LED_OFF                 1
#define LED_ON                  2
#define LED_BLINK_SLOW          3
#define LED_BLINK_FAST          4
#define LED_BLINK_SLOW_TIME_OFF 5       // blink per tot tempo e poi off
#define LED_BLINK_SLOW_TIME_ON  6       // blink per tot tempo e poi on
#define LED_BLINK_FAST_TIME_OFF 7
#define LED_BLINK_FAST_TIME_ON  8
#define LED_TIME_ON             9       // on per tot tempo

#define TIME_PERIODO_LED    5       // n*1/10 sec



//  TB67S109
#define SMORZ_12        0
#define SMORZ_37        1
#define SMORZ_75        2
#define SMORZ_100       3

#define STEP_0          0
#define STEP_1_1        3
#define STEP_1_2L       4
#define STEP_1_2H       5
#define STEP_1_4        6
#define STEP_1_8        7
#define STEP_1_16       8
#define STEP_1_32       9




//*****************
//**  VARIABILI  **
//*****************

//  ****  DAC  ****
drv_ext UINT16 dac_value[MAX_STEPPER];
drv_ext UINT16 dac_hold[MAX_STEPPER];


drv_ext volatile UINT8 buffer_digitali[NUM_DIG], buffer_latch[NUM_DIG];
drv_ext volatile UINT8 rimb_dig[NUM_DIG];       // mantiene il valore corrente di antirimbalzo
drv_ext volatile UINT8 HomeSensor[NUM_DIG];
drv_ext volatile UINT8 buffer_prec[NUM_DIG];

drv_ext volatile UINT8 Set_MicroStep[MAX_STEPPER], Set_PercTorque[MAX_STEPPER];

drv_ext volatile uint8_t i2c_command[20];

drv_ext union
{
    UINT8 all;
    struct bitt8 bit8;
} volatile pca8575_port0, pca8575_port0_hold, pca8575_port1, pca8575_port1_hold;




//*****************************
//**  PROTOTIPI DI FUNZIONE  **
//*****************************

void write_dac(void);
void write_io(void);

void init_dac_i2c(void);
void init_io_i2c(void);
void Init_MotorsTimers(void);

void Luce_TIMER_START(void);
void Luce_TIMER_STOP(void);
uint8_t Luce_Read_Pin(void);

void set_TB67S109_uStep(uint8_t stepper, uint8_t uStep);

void var_driver_init(void);

void ADC_init(void);


