//--------------------------------------------------------------
// PRIMA STESURA
// Riva ing. Franco M.
// Besana Brianza (MI)
//
// REVISIONI SUCCESSIVE
// Giovanni Corvini
// Melegnano (MI)
//--------------------------------------------------------------
//
// PROJECT:	  RSR008 - Low Cost Collimator
// FILE:      sys_calibration.h
// REVISIONS: 0.00 - 22/09/2008 - First Definition
//
//--------------------------------------------------------------

#define MAGNITUDE_RESOLUTION	4

#ifdef _SYS_CALIBRATION_
	volatile uword g_uwCalibrationTimer=0;
	bool g_bUserCalInProgress=false;
	bool g_bCalInProgress=false;
	bool g_bSerCalInProgress=false;
	bool g_bUserCalRequest = false;
	volatile uword g_uwPushButtonDelay=0;
    UINT16 IrisPotMin, IrisPotMax;
    UINT32 IrisStepMax, IrisStepTemp;
    SINT8 StatusConfig;
    UINT8 FlagSetIris;
    UINT8 NewValIris;
    UINT16 DiffAdc, DiffAdcPrec;
    UINT8 NumMsg;
#else
    volatile extern uword g_uwCalibrationTimer;
	extern bool g_bUserCalInProgress;
	extern bool g_bCalInProgress;
	extern bool g_bSerCalInProgress;
	extern bool g_bUserCalRequest;
	extern volatile uword g_uwPushButtonDelay;
    extern UINT16 IrisPotMin, IrisPotMax;
    extern UINT32 IrisStepMax, IrisStepTemp;
    extern SINT8 StatusConfig;
    extern UINT8 FlagSetIris;
    extern UINT8 NewValIris;
    extern UINT16 DiffAdc, DiffAdcPrec;
    extern UINT8 NumMsg;
#endif

void sys_board_calibration(void);
void wait_key_filter_up (uword uwDelayMsec);
void wait_key_exit_up (uword uwDelayMsec);

