

#define TEST_IMQ

#define _SYS_MAIN_
#define MAIN_H

#include "include.h"

#define ENC_MOVE_TIME       25  //msec
#define ENC_INV_TIME        100 //msec
#define DEL_HOME    'H'



/*******************************************************************************************************************************************************

            3.00    10/12/2018  - Versione Iniziale presa da R225 ver 2.20

*******************************************************************************************************************************************************/
// per cambiare la versione cercare VERSIONE_FIRMWARE e VERSIONE_FW in "global.h"

#define VERSIONE_FIRMWARE   "R225S300"
const UINT8 R_release[] = {' ', ' ', ' ', ' ', ' ' };




void mainApplication(void)
{
    ssp_err_t api_err;
    UINT32 temp32;
    UINT16 time;
    UINT8 serial_selection;
    UINT8 DisableLoop = 0;

    __disable_irq();

    variable_init();
    timer_blink = 1;

    allarmi_totale = 0;
    sys_init();
    sys_dataflash_init();

//  sys_can_init(g_ucWorkModality);

    sys_control_init(TRUE);

    if (ee_ConfigColl.ucSwContext)    // controlla se arriva da un reset con i due tasti
    {
        ee_ConfigColl.ucSwContext = 0;
        g_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
        g_ucWorkModality = CONFIGURATION;
    }

// solo se protocollo SEDECAL parte con stato manuale forzato
    if (ee_ConfigColl.ucCanProtocol == CAN_SEDECAL)
    {
        g_bManualRequestFromCan = TRUE;
    }

    __enable_irq();


    api_err = g_sf_i2c_device_dac.p_api->open(g_sf_i2c_device_dac.p_ctrl, g_sf_i2c_device_dac.p_cfg);
    if (api_err != SSP_SUCCESS)
    {
        while (1)
        {
            __asm("nop");
        }
    }
    init_dac_i2c();

    api_err = g_sf_i2c_device_io.p_api->open(g_sf_i2c_device_io.p_ctrl, g_sf_i2c_device_io.p_cfg);
    if (api_err != SSP_SUCCESS)
    {
        while (1)
        {
            __asm("nop");
        }
    }
    init_io_i2c();

    tx_thread_sleep(1);

    // start irq 1 ms
    api_err = g_timer_1ms.p_api->open(g_timer_1ms.p_ctrl, g_timer_1ms.p_cfg);
    api_err = g_timer_1ms.p_api->start(g_timer_1ms.p_ctrl);


    frequenza_timer = 50UL << 8;
    Init_MotorsTimers();


    //waiting for display supply stabilization (1 sec.)
    while (!g_uwTimeSecondCounter);

    if (ee_ConfigColl.ucDisplayType != DISPLAY_NONE)
    {
//      sys_lcd_init();
        lcd_init();
        sys_display_init();
    }
    lcd_stop = FALSE;       // partenza scrittura LCD

    sys_cross_init();
    sys_long_init();
    LED_SIGNAL_OFF;

    //Release the stepper drivers from the reset
//TODO:     rst_all_step = 0;



    //TODO:    sby_all_step = 1;

    LASER1_ON;      // per alimentare il potenziometro


#ifdef _DEBUG_
    num_seriale_metro = 0;
    num_seriale_config = -1;
#endif
#ifdef _RELEASE_
    num_seriale_metro = 0;
    num_seriale_config = 1;
#endif


    serial_init_config();       // inizializza seriale della configurazione
    if (ee_ConfigColl.ucMeterEnabled == TRUE)
    {
//TODO:        US_POWER = 1;       // alimentazione modulino
        serial_init_metro();        // se abilitato inizializza metro ad ultrasuoni
    }

    switch (g_ucWorkModality)       // inizializzazione VARIE a seconda della modalita'
    {
    case COLLIMATORWORK:
        sys_control_led_and_relays(RED);
        break;
    case CONFIGURATION:
        sys_control_led_and_relays(NONE);
        break;
    case CALIBRATION:
        sys_control_led_and_relays(NONE);
        break;
    case BOARDTEST:
        sys_control_led_and_relays(NONE);
        break;
    }


#ifdef SYS_CANOPEN
    initCanOpen();
#else
    CAN_init(0);
#endif



#ifndef SYS_CANOPEN
//TODO:    CAN_crea_slot(g_ucWorkModality);
    ID_OtherCommand = ee_CanColl.unIdOtherCommands;
#endif
                                // particolari configurazioni di default
    switch (ee_ConfigColl.ucCanProtocol)
    {
    case CAN_GMM:
        UpdateRates.un7F0 = 200;
        break;
    }

#ifdef TEST_VITA
    UpdateRates.un7F0 = 1000;
    UpdateRates.un7F1 = 1000;
#endif


    stato_tastiera = ST_KEY_PREP_IDLE;
    key_pressed = FALSE;
    key_press_event = FALSE;
    in_calibrazione = TRUE;



                                    // **********************************
                                    //     LOOP PRINCIPALE
                                    // **********************************
    while (DisableLoop == 0)
    {
        tx_thread_sleep(1);

//        g_ioport.p_api->pinWrite(PIN_LED_D31, IOPORT_LEVEL_LOW);
//        g_ioport.p_api->pinWrite(PIN_LED_D31, IOPORT_LEVEL_HIGH);

        write_dac();
        write_io();


        if (ee_ConfigColl.ucMeterEnabled == TRUE)
        {
            receive_metro();
        }

        check_keys_switch_context();

        if (g_bUserCalRequest == true)
            g_ucWorkModality = CALIBRATION;

        controlla_generazione_eventi();

        switch (g_ucWorkModality)
        {
        case INVALID:
            break;

        case COLLIMATORWORK:

            invio_tasti_villa();
            controlla_posizione_pot();

            sys_display_update_screen();
//            ricevi_byte_seriale();      // accoda nella fifo byte da RS232
            check_rx_messaggio();       // ricezione messaggio di configurazione da seriale

#ifdef SYS_CANOPEN
            DisableLoop = processCanOpen();
#else
            sys_can_work();
#endif
//            sys_can_main_process();
            sys_delayed_check();
            sys_control_keyboard();
            sys_control_inclination();
            sys_control_remote();
            sys_control_check_for_manual();
//              sys_cross_process();
//              sys_long_process();
            sys_filter_process();
            sys_control_led();
//            sys_control_process();          // controlla FILTRO
            sys_control_master(TRUE);   // controlla funzione Cross/Long/Iride
            sys_iris_process();         // ATTENZIONE: deve stare dopo "sys_control_master()"
            check_spostamento_lamelle();
            sys_control_light();
            sys_eeprom_sync();

#ifdef TEST_FIERA
            Test_Fiera();
#endif
#ifdef TEST_IMQ
            Test_IMQ();
#endif
            break;

        case CALIBRATION:
//            sys_can_under_test();
//            ricevi_byte_seriale();      // accoda nella fifo byte da RS232
            check_rx_messaggio();       // ricezione messaggio di configurazione da seriale
            sys_can_config();           //ricezione messaggi canbus
            sys_board_calibration();
//            sys_control_process();
            sys_control_master(FALSE);
            sys_eeprom_sync();
            check_tx_100();
            sys_control_inclination();
            break;

        case CONFIGURATION:
            manda_msg_stato();          // invia messaggi di stato se necessario
//            ricevi_byte_seriale();      // accoda nella fifo byte da RS232
            check_rx_messaggio();       // ricezione messaggio di configurazione da seriale
            sys_can_config();           //ricezione messaggi canbus
            sys_filter_process();
//            sys_can_under_test();
            sys_display_manager();
            sys_eeprom_sync();
//            sys_control_process();
            sys_control_master(FALSE);
            sys_control_inclination();
            sys_Main_GetSecond(&time);
            if (time >= 2)        //Wait a while to allow a safe boards startup
            {
                gestione_tastiera();
            }
            break;

        case BOARDTEST:
//            sys_can_under_test();
            sys_can_config();           //ricezione messaggi canbus
//TODO:            sys_stepper_selftest();
//            sys_control_process();
            sys_control_inclination();
            break;
        }

    }

    // Disable interrupts
    __disable_irq();

    // disabilita motori
    set_TB67S109_uStep(STEPPER_A, 0);
    set_TB67S109_uStep(STEPPER_B, 0);
    set_TB67S109_uStep(STEPPER_C, 0);
    set_TB67S109_uStep(STEPPER_D, 0);

    Luce_TIMER_STOP();     // spegne luce, laser

    while (1);



}







//*********************************************
//  controlla la ricezione del metro US
//*********************************************
void receive_metro(void)
{
    UINT8 dato;
    UINT16 temp16;

    if (timer_rx_metro == 0)        // se scaduto il tempo e' il primo carattere
    {
        misura_stato = 0;
    }

    if (PtSerIM != PtSerFM)     // controlla se ricevuto un carattere
    {
        dato = FifoSerM[PtSerFM++];     // preleva carattere
        PtSerFM &= (DIM_FIFO_SER - 1);

        if (misura_stato == 0)        // se scaduto il tempo e' il primo carattere
        {
            misura_stato++;
            misura_temp = (UINT16)dato << 8;
        }
        else if (misura_stato == 1)
        {
            misura_stato++;
            misura_temp |= dato;
            misura_temp *= 10;
            misura_temp += (UINT16)ee_ConfigColl.OffsetMetro;
            temp16 = misura_temp / 10;
            dato = misura_temp % 10;
            if (dato >= 5)
                temp16++;
            misura_metro = temp16;
        }
    }
}










//*************************************************************
//  controlla se i tasti FILTRO e LUCE sono premuti per
//  almeno 3 secondi insieme
//  se si resetta il collimatore ed entra nella modalita'
//  Configurazione o viceversa
//*************************************************************
void check_keys_switch_context(void)
{
    static UINT8 inCal = FALSE;
    UINT16 time;

    time = ee_ConfigColl.decSecondsConfig;

//  tasto LAMPADA per TEST
    if ((LAMP_SWITCH == 1) && (FILTER_SWITCH == 0))
    {
        if ((timer_keys_lamp >= time) && (inCal == FALSE))
        {
            inCal = TRUE;
            Luce_TIMER_STOP;
            g_ucWorkModality = CALIBRATION;
        }
    }
    else
    {
        timer_keys_lamp = 0;
        inCal = FALSE;
    }


 //  ENTRAMBI PREMUTI per CONFIGURAZIONE

    if ((!FILTER_SWITCH) || (!LAMP_SWITCH))     // se almeno un tasto NON premuto resetta timer
    {
        timer_keys_switch = 0;
    }

    switch (stato_switch_context)
    {
    case 0:
        if (timer_keys_switch >= time)     // tasti premuti per n secondi
        {
            if (g_ucWorkModality == COLLIMATORWORK)
            {
                ee_ConfigColl.ucSwContext = 1;
                g_ulUpdateEeprom |= EEP_UPDATE_COLLIMATOR;
            }
            stato_switch_context = 1;
        }
        break;

    case 1:     // attesa rilascio dei tasti
        if ((!FILTER_SWITCH) && (!LAMP_SWITCH))     // attende rilascio dei due tasti
        {
            resetCollimator();
        }
        break;
    }

}




void resetCollimator(void)
{
      __asm(" nop");
      NVIC_SystemReset();
      __asm(" nop");

}







void variable_init(void)
{
    UINT8 i;


    s_ucFilterIndex1 = 0;
    s_ucFilterIndex2 = 0;
    s_ucFilterIndex3 = 0;
    s_ucFilterIndex4 = 0;
    s_uwFilterOut1 = 0;
    s_uwFilterOut2 = 0;
    s_uwFilterOut3 = 0;
    s_uwFilterOut4 = 0;

    _ok_tx_ser = FALSE;
    _ok_dato_seriale = FALSE;


    g_uwActualDff = 0;
    dff_attuale = 0;
    inclinazione_cross = 0;
    mult_incl_cross = 0;
    inclinazione_long = 0;
    mult_incl_long = 0;

    for (i = 0; i < MAX_STEPPER; i++)
    {
        comando_main[i] = FALSE;
        i_MotPosizione[i] = 0;
        mi_MotPosizione[i] = 0;
        m_MotPosizione[i] = 0;
        stato_motore_irq[i] = ST_MOT_FERMO;
        stato_mot[i].all = 0;
        op_mot[i] = OP_SPEED_FERMO;
        formato_min[i] = 0;
        formato_max[i] = 430;
        KeyReturnAuto[i] = FALSE;
        MovimentoManuale[i] = FALSE;
        Set_MicroStep[i] = 0xFF;
        Set_PercTorque[i] = 0xFF;
        dac_hold[i] = 0xFFFF;
    }

    ixTI = 0;
    ixTF = 0;
    ix0I = 0;
    ix0F = 0;
    ixMI = 0;
    ixMF = 0;

    block_tx = FALSE;

    enable_variazione = FALSE;
    timeout_taratura = 0;
    g_uwTimeSecondCounter = 0;
    g_uwTimeMilliSecondCounter = 0;
    Iris_Variable_init(TRUE);

//    CollInReset = 0;
    CollInReset |= STATO_LAMELLE_RESET;
    CollInReset |= STATO_FILTRO_RESET;
    CollInReset |= STATO_IRIDE_RESET;

    EnableFilterKey = TRUE;
    EnableLampKey = TRUE;
    EnableKnobs = TRUE;
    EnableIrisKey = TRUE;
    EnableIrisKey_old = FALSE;

    stato_iride = 0;
    stato_switch_context = 0;
    timer_keys_switch = 0;
    timer_keys_lamp = 0;

    RequestLightStatus = COM_LIGHT_IDLE;

    for (i = 0; i < 10; i++)
    {
        NewFormat[i] = FALSE;
        FormatoCross[i] = 0;
        FormatoLong[i] = 0;
    }
    LastFixedShutter = 0;
    LastFixedIris = 0;

    ResetPosizione = FALSE;
    DebugIncl = FALSE;

    lcd_stop = TRUE;
    DisplayFTD = FALSE;
    g_bTomography = FALSE;
    AutoLimite = FALSE;

    VisualizzaFiltro = FALSE;
    FiltroInErrore = FALSE;

    rimbalzo_home_shutter = RIMBALZO_HOME;
    EnableInvioEventi = FALSE;
    TestPassi = FALSE;

    tasti_villa = 0;

    angolo_colonna = 0;
    angolo_colonna_hold = -128;

}


void Iris_Variable_init(UINT8 reset)
{
    if (reset == TRUE)
    {
        g_ucIrisStatus = IRIS_INIT;
        g_ucIrisLedReq = RED;
    }
    fluoroscopia_on = FALSE;
    era_con_tasti = FALSE;
    ShutterFollow[STEPPER_CROSS] = FALSE;
    ShutterFollow[STEPPER_LONG] = FALSE;
    ShutterFollow[STEPPER_LONG2] = FALSE;
    LastIrisFormat = iride_calib_mm[IRIS_FORMAT_MAX + 1];
    iride_manual = FALSE;
    muovi_iride = FALSE;
    set_passi_max = FALSE;
    CampoIride = 0;
}






//********************************************************************
//  calcola le posizioni teoriche dei potenziometri per confronto
//********************************************************************
void controlla_posizione_pot(void)
{
    UINT8 flag;

    flag = FALSE;


    if ((ee_ConfigColl.ucHardwareType == HARDWARE_YES_POT) &&      // se non ha i potenziometri salta indietro
        (in_calibrazione == FALSE))
    {
        flag = TRUE;
//        if (timer_step_a_s == 1)        // controlla che il motore CROSS sia in movimento
        if (Steppers[STEPPER_CROSS].StepperData.StepperBits.bMoving)
        {
            motore_a_in_mov = TRUE;     // se si setta un timer
            timer_stop_a = TIME_CHECK_POT;      // pausa
        }
        else
        {
            if (motore_a_in_mov == TRUE)    // se il motore era in movimento
            {
                if (timer_stop_a == 0)      // attende la fine del timer per il controllo pot
                {
                    motore_a_in_mov = FALSE;
                    pos_cross = calcola_pos_pot(POT_CROSS, g_lVPos[STEPPER_A]);

                    if (((SINT16)g_nFilteredAdc[POT_CROSS] >= (pos_cross - DELTA_POT_ALARM)) &&
                        ((SINT16)g_nFilteredAdc[POT_CROSS] <= (pos_cross + DELTA_POT_ALARM)))
                    {           // qui tutto OK
                        allarmi_totale &= ~ALARM_CROSS;
                    }
                    else        // qui in allarme
                    {
                        err_counter_cross++;
                        allarmi_totale |= ALARM_CROSS;
    //                    g_ucCrossLedReq = RED;      // led rosso per errore
                    }
                }
            }
        }


//        if (timer_step_b_s == 1)        // controlla che il motore LONG sia in movimento
        if (Steppers[STEPPER_LONG].StepperData.StepperBits.bMoving)
        {
            motore_b_in_mov = TRUE;     // se si setta un timer
            timer_stop_b = TIME_CHECK_POT;      // pausa
        }
        else
        {
            if (motore_b_in_mov == TRUE)    // se il motore era in movimento
            {
                if (timer_stop_b == 0)      // attende la fine del timer per il controllo pot
                {
                    motore_b_in_mov = FALSE;
                    pos_long = calcola_pos_pot(POT_LONG, g_lVPos[STEPPER_B]);

                    if (((SINT16)g_nFilteredAdc[POT_LONG] >= (pos_long - DELTA_POT_ALARM)) &&
                        ((SINT16)g_nFilteredAdc[POT_LONG] <= (pos_long + DELTA_POT_ALARM)))
                    {           // qui tutto OK
                        allarmi_totale &= ~ALARM_LONG;
                    }
                    else        // qui in allarme
                    {
                        err_counter_long++;
                        allarmi_totale |= ALARM_LONG;
    //                    g_ucLongLedReq = RED;      // led rosso per errore
                    }
                }
            }
        }
    }


    if (ee_ConfigColl.ucIrisPresent == IRIDE_YES)
    {
        flag = TRUE;
        if (stato_iride & IRIDE_MOVE)     // e' in movimento?
        {
            motore_iride_in_mov = TRUE;     // se si setta un timer
            timer_stop_iride = TIME_CHECK_POT;      // pausa
        }
        else
        {
            if (motore_iride_in_mov == TRUE)    // se il motore era in movimento
            {
                if (timer_stop_iride == 0)      // attende la fine del timer per il controllo pot
                {
                    motore_iride_in_mov = FALSE;
                    if (LinIrisMm(g_uwActualDff, (uword)g_lIrisActualSteps, &g_uwActualIrisToBeSent, TRUE))
                    {
                        if (((SINT16)adc_iride >= (pos_iride - DELTA_POT_ALARM)) &&
                            ((SINT16)adc_iride <= (pos_iride + DELTA_POT_ALARM)))
                        {           // qui tutto OK
                            allarmi_totale &= ~ALARM_IRIS;
                        }
                        else        // qui in allarme
                        {
                            err_counter_iride++;
                            allarmi_totale |= ALARM_IRIS;
                        }
                    }
                }
            }
        }
    }



//    if (flag == TRUE)       // solo se potenziometri configurati o iride presente
    {
        if (KEY_AUTO)       // controllo posizione chiave per reset
        {
            if (pos_chiave_prec == FALSE)
            {
                pos_chiave_prec = TRUE;
                if (allarmi_totale & ALARM_CHECK)
                {
                    allarmi_totale &= ~ALARM_CROSS;
                    allarmi_totale &= ~ALARM_LONG;
                    allarmi_totale &= ~ALARM_IRIS;
                    g_bReqHomeStepperA=true;
                    config_movimento(STEPPER_CROSS, MSG_MOV_HOME, 0, MOV_RESET);
                    g_bReqHomeStepperB=true;
                    config_movimento(STEPPER_LONG, MSG_MOV_HOME, 0, MOV_RESET);
                    ReqHomeIris = TRUE;
                }
                else
                {
                    g_uwIrisFormatFromCan = LastIrisFormat;
                    muovi_iride = TRUE;
                }
            }
        }
        else
        {
            if (pos_chiave_prec == TRUE)
            {
                pos_chiave_prec = FALSE;
                set_passi_max = TRUE;
            }
        }
    }

}



//--------------------------------------------------------------
void sys_delayed_check(void){

    static uchar s_ucDelayedStatus=DEL_HOME;

}





//***********************************************
//  verifica se deve spostare le lamelle
//  per seguire l'iride
//***********************************************
void check_spostamento_lamelle(void)
{
    UINT8 flag;

    if (fluoroscopia_on == TRUE)       // iride configurato?
    {
        check_and_move();
    }
    else
    {
        ShutterFollow[STEPPER_CROSS] = FALSE;
        ShutterFollow[STEPPER_LONG] = FALSE;
        ShutterFollow[STEPPER_LONG2] = FALSE;
    }
/*
    if (fluoroscopia_on == TRUE)       // iride configurato?
    {
        if (stato_iride & IRIDE_MOVE)     // e' in movimento?
        {
            if (stato_iride & IRIDE_MAN)     // e' in movimento con i tasti?
            {
                era_con_tasti = TRUE;
                if (timer_spost_lamelle == 0)
                {
//                    timer_spost_lamelle = 20;       // n*10 ms
                    check_and_move();
                }
            }
        }
        else
        {

//            if (era_con_tasti == TRUE)
            {
                if (timer_spost_lamelle == 0)
                {
                    era_con_tasti = FALSE;
                    timer_spost_lamelle = 20;   // n*10 ms
                    check_and_move();
                }
            }
        }
    }
*/
}


// TODO: rivedere check_and_move()
// TODO: rivedere gestione Iride?
// TODO: implementare movimento iride in CaricaDFFeFormato()?
// TODO: vedere email Apelem per gestione Iride
//      - quando si imposta IrideIndipendente = TRUE, le lamelle sono indipendenti o non devono andare oltre la tangente iride?
// TODO: rivedere filtrazione pot ASR003
// TODO: cosa succede se siamo in ShutterFollow e ruotiamo il collimatore?


//**********************************************
//  insegue l'iride
//**********************************************
void check_and_move(void)
{
    UINT16 Cross, Long, temp16, Iris;

    if (g_uwActualDff == 0)
        return;

    LinIrisMm(g_uwActualDff, (uword)g_lIrisActualSteps, &Iris, FALSE);

    Cross = Iris + (UINT16)ee_ConfigColl.OffsetLamelle;
    temp16 = CheckMaxShutter(STEPPER_CROSS, g_uwActualDff, 1);
    if (Cross > temp16)
        Cross = temp16;
    Iris_MaxCross = Cross;
    if (ShutterFollow[STEPPER_CROSS] == TRUE)
    {
        formato_mm[STEPPER_CROSS] = Cross;
    }
    else
    {
        if (g_uwActualCrossToBeSent > (Iris + ((UINT16)ee_ConfigColl.OffsetLamelle / 2)))        // riaggancia se supera il campo
        {
            if (ManualStatus[STEPPER_CROSS] == FALSE)
            {
                ShutterFollow[STEPPER_CROSS] = TRUE;
                formato_mm[STEPPER_CROSS] = Cross;  // TODO: mettere FormatoCross[] ?
            }
        }
    }

    Long = Iris + (UINT16)ee_ConfigColl.OffsetLamelle;
    temp16 = CheckMaxShutter(STEPPER_LONG, g_uwActualDff, 1);
    if (Long > temp16)
        Long = temp16;
    Iris_MaxLong = Long;
    if (ShutterFollow[STEPPER_LONG] == TRUE)
    {
        formato_mm[STEPPER_LONG] = Long;
    }
    else
    {
        if (g_uwActualLongToBeSent > (Iris + ((UINT16)ee_ConfigColl.OffsetLamelle / 2)))        // riaggancia se supera il campo
        {
            if (ManualStatus[STEPPER_LONG] == FALSE)
            {
                ShutterFollow[STEPPER_LONG] = TRUE;
                formato_mm[STEPPER_LONG] = Long;        // TODO: mettere FormatoLong[] ?
            }
        }
    }

}


/************************************************************************
 calcola la posizione teorica del potenziometro

  formula retta: y = mx + q

     (y2 - y1)     (y2 - y1)
m = ----------- =  ---------
     (x2 - x1)        x2   ( x1 = 0 per definizione)

     x2y1 - x1y2     x2y1
q = ------------- = ------ = y1
       x2 - x1        x2


da cui:

    (y2 - y1)
y = --------- * x + y1
       x2

dove:
x1, x2 = posizione lamella in passi (a tutto chiuso e tutto aperto)
y1, y2 = valore potenziometro in posizione tutto chiuso e tutto aperto

return:
SINT16 posizione teorica potenziometro secondo i passi

**************************************************************************/
SINT16 calcola_pos_pot(UINT8 pot, UINT32 steps)
{
    UINT32 temp32, passi;
    UINT8 i;
    UINT32 PotMax, PotMin, StepMax, StepMin;

    if (pot == POT_CROSS)
    {
        for (i = 1; i < NUM_TAR_POT; i++)
        {
            StepMin = (g_MaxSteps[STEPPER_CROSS] * ((UINT32)i - 1UL)) / (NUM_TAR_POT - 1);
            StepMax = (g_MaxSteps[STEPPER_CROSS] * (UINT32)i) / (NUM_TAR_POT - 1);
            if (steps <= StepMax)
            {
                PotMin = (UINT32)ee_ConfigColl.uwPotCross[i - 1];
                PotMax = (UINT32)ee_ConfigColl.uwPotCross[i];
                break;
            }
        }
    }
    else
    {
        for (i = 1; i < NUM_TAR_POT; i++)
        {
            StepMin = (g_MaxSteps[STEPPER_LONG] * ((UINT32)i - 1UL)) / (NUM_TAR_POT - 1);
            StepMax = (g_MaxSteps[STEPPER_LONG] * (UINT32)i) / (NUM_TAR_POT - 1);
            if (steps <= StepMax)
            {
                PotMin = (UINT32)ee_ConfigColl.uwPotLong[i - 1];
                PotMax = (UINT32)ee_ConfigColl.uwPotLong[i];
                break;
            }
        }
    }

    passi = steps - StepMin;
    temp32 = PotMax - PotMin;
    temp32 *= passi;
    temp32 /= (StepMax - StepMin);
    temp32 += PotMin;

    return (SINT16)temp32;
}






//--------------------------------------------------------------
// SYSTEM FUNCTIONS
//--------------------------------------------------------------
void sys_Main_GetMilliSecond(uword* puwTimeCounter){
    *puwTimeCounter=g_uwTimeMilliSecondCounter;
}

//--------------------------------------------------------------
void sys_Main_GetSecond(uword* puwTimeCounter){
    *puwTimeCounter=g_uwTimeSecondCounter;
}






//*************************************************
//  inizializzazione varie
//*************************************************
void sys_init(void)
{
    ioport_level_t pinlevel;
    uchar ucIndex;
    UINT16 i;

    //Hardware setup
    ConfigurePortPins();

    for (ucIndex=0; ucIndex<MAX_STEPPER; ucIndex++){
        Steppers[ucIndex].StepperData.StepperBits.bHome = false;
        Steppers[ucIndex].StepperData.StepperBits.bErrorHome = false;
        Steppers[ucIndex].StepperData.StepperBits.bMoving = false;
        Steppers[ucIndex].StepperData.StepperBits.bChecking = false;
        Steppers[ucIndex].ucStepperError = 0;

        g_uwActFreq[ucIndex]  = 0;
        g_uwSetFreq[ucIndex]  = 0;
        g_uwRampTime[ucIndex] = 0;
        g_uwAccStep[ucIndex]  = 0;
        g_uwHomeDelay[ucIndex] = 0;
        g_uwLowPowerDelay[ucIndex] = 0;
        g_uwHighPowerDelay[ucIndex] = 0;
        g_lVPos[ucIndex] = 0;
        g_lRun[ucIndex] = 0;
        g_lPos[ucIndex] = 0;
        g_nDelta[ucIndex]  = 0;
        g_lDecPos[ucIndex] = 0;
        g_cDecel[ucIndex]   = 0;
        g_lFromPos[ucIndex] = 0;
        g_lMidPoint[ucIndex] = 0;
        g_lDestination[ucIndex] = 0;
    }

    rimb_key_filter = 0;        // antirimbalzo iniziale per controllo tasti all'accensione
    rimb_key_lamp = 0;

    for (i = 0; i < 1000; i++)
    {
        g_ioport.p_api->pinRead(PIN_FILTER_SWITCH, &pinlevel);
        if (pinlevel == 0)      // tasto frontale
        {
            if (rimb_key_filter == 500)
                FILTER_SWITCH = 1;                  // qui in Home
            else
                rimb_key_filter++;
        }
        else
        {
            if (rimb_key_filter == 0)
                FILTER_SWITCH = 0;
            else
                rimb_key_filter--;
        }


        g_ioport.p_api->pinRead(PIN_LAMP_SWITCH, &pinlevel);
        if (pinlevel == 0)      // tasto frontale
        {
            if (rimb_key_lamp == 500)
                LAMP_SWITCH = 1;                  // qui in Home
            else
                rimb_key_lamp++;
        }
        else
        {
            if (rimb_key_lamp == 0)
                LAMP_SWITCH = 0;
            else
                rimb_key_lamp--;
        }
    }


    //Check the Actual Modality
    if (LAMP_SWITCH){
        if (FILTER_SWITCH){
            //Board Testing
            g_ucWorkModality = BOARDTEST;
        } else{
            //Internal Calibration
            g_ucWorkModality = CALIBRATION;
        }
    } else {
        if (FILTER_SWITCH){
            //Configuration
            g_ucWorkModality = CONFIGURATION;
        } else{
            //Normal working
            g_ucWorkModality = COLLIMATORWORK;
        }
    }

    rimb_key_filter = 0;
    rimb_key_lamp = 0;
    FILTER_SWITCH = 0;
    LAMP_SWITCH = 0;


//    g_ucWorkModality = CONFIGURATION;

    //All the can messages apart one are disabled
    UpdateRates.un7F0=0;
    UpdateRates.un7F1=0;
    UpdateRates.un7F9=0;
    UpdateRates.un7FC=0;
    UpdateRates.un100=250;
    //ATS
    UpdateRates.un082=100;
    UpdateRates.un083=200;
    UpdateRates.un084=1000;
    UpdateRates.un085=200;
    UpdateRates.un101=1000;
    UpdateRates.un103=1000;

    g_uwWaitResetDelay = 0;

    //Reset digital filtering
    memset (g_uwFilterAdxlXArray, 0, ADXLX_ORDER+1);
    memset (g_uwFilterAdxlYArray, 0, ADXLY_ORDER+1);
    memset (g_uwFilterCrossArray, 0, CROSS_ORDER+1);
    memset (g_uwFilterLongArray, 0, LONG_ORDER+1);

    timer_test = 0;

    ixLogEvI = 0;
    ixLogEvF = 0;

}



//*************************************************
//  configura lo stato dell'I/O
//*************************************************
void ConfigurePortPins(void)
{

}






//***************************************************
//  inizializzazione dei dati in flash
//***************************************************
void sys_dataflash_init(void)
{
    UINT32 temp32;
    uchar ucIndex;
    uchar ucRc;
    bool bNewStruct;
    VersionStruct ee_TempRSR008B_Version;
    UINT8 init = FALSE;

    //Reset Global variables
    bNewStruct = false;

    //Reset Internal Eeprom Flag
    g_InternalFlag.bit.bEepromOk = true;

    //Read Software/Hardware version
    memcpy(ee_RSR008B_Version.cbVersion, VERSIONE_FIRMWARE, 8);

    //Check EEPROM
//    EE_data.record_label = 1;
//    ucRc = NV_Read(&EE_data);
//    if (ucRc)
//    {
        init = TRUE;
        // Set default values because this is the first time this program is being run
        memset(ee_RSR008B_Version.cbSerial, 0, 9);
        memcpy(ee_RSR008B_Version.cbSerial, "S/N00000", 8);
//        EE_data.record_label = 1;
//        EE_data.pData = (uint *) & ee_RSR008B_Version;
//        EE_data.data_size = ((sizeof (VersionStruct) + 1) / 2 - 1); /* size in words (-1) */
//        if (!NV_Write(&EE_data))
            g_InternalFlag.bit.bEepromInitialized = true;
//        else
//            g_InternalFlag.bit.bEepromError = true;
//    }
//    else
//    {
//        // Load last saved values to use as initial values
//        ee_TempRSR008B_Version = *(VersionStruct NV_FAR *) EE_data.pData;
//        if (memcmp(&ee_TempRSR008B_Version.cbVersion, &ee_RSR008B_Version.cbVersion, 8))
//        {
//            bNewStruct = true;
//            // Set default values because this is the first time this program is being run
//            memset(ee_RSR008B_Version.cbSerial, 0, 9);
//            memcpy(ee_RSR008B_Version.cbSerial, "S/N00000", 8);
//            EE_data.record_label = 1;
//            EE_data.pData = (uint *) & ee_RSR008B_Version;
//            EE_data.data_size = ((sizeof (VersionStruct) + 1) / 2 - 1); /* size in words (-1) */
//            if (!NV_Write(&EE_data))
//                g_InternalFlag.bit.bEepromInitialized = true;
//            else
//                g_InternalFlag.bit.bEepromError = true;
//        }
//        memcpy(ee_RSR008B_Version.cbSerial, ee_TempRSR008B_Version.cbSerial, 8);
//    }

//    EE_data.record_label = 2;
//    ucRc = NV_Read(&EE_data);
//    if (ucRc || bNewStruct)
//    {
        // Set default values because this is the first time this program is being run
        init = TRUE;
        for (ucIndex = 0; ucIndex < MAX_STEPPER; ucIndex++)
        {
            ee_CanAddressConfig[ucIndex].uwBoardAddress = 0x0700 + ucIndex * 0x10;
        }
//        EE_data.record_label = 2;
//        EE_data.pData = (uint *) & ee_CanAddressConfig;
//        EE_data.data_size = ((sizeof (ee_CanAddressConfig) + 1) / 2 - 1); /* size in words (-1) */
//        if (!NV_Write(&EE_data))
            g_InternalFlag.bit.bEepromInitialized = true;
//        else
//            g_InternalFlag.bit.bEepromError = true;
//    }
//    else
//    {
//        // Load last saved values to use as initial values
//        for (ucIndex = 0; ucIndex < MAX_STEPPER; ucIndex++)
//        {
//            ee_CanAddressConfig[ucIndex] = *(CanAddressConfig NV_FAR *) EE_data.pData;
//            EE_data.pData += sizeof (CanAddressConfig) / 2;
//        }
//    }

//    EE_data.record_label = 3;
//    ucRc = NV_Read(&EE_data);
//    if (ucRc || bNewStruct)
//    {
        // Set default values because this is the first time this program is being run
        init = TRUE;
        for (ucIndex = 0; ucIndex < MAX_STEPPER; ucIndex++)
        {
            ee_ConfigBoards[ucIndex].ucStep = 0x08; //Step: 1/16
            ee_ConfigBoards[ucIndex].ucPhoto = PHOTO_LOW; //Photo: low
            ee_ConfigBoards[ucIndex].ucDecay = 0x01; //Decay: fast
            ee_ConfigBoards[ucIndex].uwFMin = DEF_FMIN; //Fmin: era 1000
            ee_ConfigBoards[ucIndex].uwFMax = 1500; //Fmax: era 1500
            ee_ConfigBoards[ucIndex].uwRamp = 100; //Ramp: era 100
            //          ee_ConfigBoards[ucIndex].uwFMaxFast = 1500; //Fmax in modalita' FAST
            ee_ConfigBoards[ucIndex].uwFMaxFast = 6000; //Fmax in modalita' FAST

            switch (ucIndex)
            {
            case STEPPER_A: // inteso come Cross solo per calcoli DFF
                ee_ConfigBoards[ucIndex].uwMaxSteps = DEF_PASSI_A_STD; // passi massimi da tutto chiuso a tutto aperto
                ee_ConfigBoards[ucIndex].uwRefSize = DEF_SIZE_A_STD; // formato di riferimento in mm
                ee_ConfigBoards[ucIndex].uwRefSteps = DEF_PASSI_A_STD; // formato di riferimento in passi
                ee_ConfigBoards[ucIndex].ucTorque = TORQUE_LAMELLE; //Torque
                break;
            case STEPPER_B: // inteso come Long solo per calcoli DFF
            case STEPPER_D: // inteso come Long solo per calcoli DFF
                ee_ConfigBoards[ucIndex].uwMaxSteps = DEF_PASSI_B_STD; // passi massimi da tutto chiuso a tutto aperto
                ee_ConfigBoards[ucIndex].uwRefSize = DEF_SIZE_B_STD; // formato di riferimento in mm
                ee_ConfigBoards[ucIndex].uwRefSteps = DEF_PASSI_B_STD; // formato di riferimento in passi
                ee_ConfigBoards[ucIndex].ucTorque = TORQUE_LAMELLE; //Torque
                break;
            case STEPPER_FILTER:
                ee_ConfigBoards[ucIndex].uwFMin = 500; //Fmin: era 10000
                ee_ConfigBoards[ucIndex].uwFMax = 10000; //Fmax: era 10000
                ee_ConfigBoards[ucIndex].ucPhoto = PHOTO_HIGH; //Photo: high
                ee_ConfigBoards[ucIndex].uwRamp = 150; //Ramp: era 100
                ee_ConfigBoards[ucIndex].ucTorque = TORQUE_FILTRO_OLD; //Torque
                break;
            }
        }
//        EE_data.record_label = 3;
//        EE_data.pData = (uint *) & ee_ConfigBoards;
//        EE_data.data_size = ((sizeof (ee_ConfigBoards) + 1) / 2 - 1); /* size in words (-1) */
//        if (!NV_Write(&EE_data))
            g_InternalFlag.bit.bEepromInitialized = true;
//        else
//            g_InternalFlag.bit.bEepromError = true;
//    }
//    else
//    {
//        // Load last saved values to use as initial values
//        for (ucIndex = 0; ucIndex < MAX_STEPPER; ucIndex++)
//        {
//            ee_ConfigBoards[ucIndex] = *(BoardConfiguration NV_FAR *) EE_data.pData;
//            EE_data.pData += sizeof (BoardConfiguration) / 2;
//        }
//    }

//    EE_data.record_label = 4;
//    ucRc = NV_Read(&EE_data);
//    if (ucRc || bNewStruct)
//    {
        // Set default values because this is the first time this program is being run
        init = TRUE;
        for (ucIndex = 0; ucIndex < MAX_STEPPER; ucIndex++)
        {
            ee_OptionConfig[ucIndex].OptionData.OptionBits.bStepCheck = 1; //b0=1
            ee_OptionConfig[ucIndex].OptionData.OptionBits.bStartupReset = 1; //b1=1
            ee_OptionConfig[ucIndex].OptionData.OptionBits.ucResidualTorque = 4; //b4=1, b3=0, b2=0
            ee_OptionConfig[ucIndex].OptionData.OptionBits.bDummyMove = 0; //b5=0
            ee_OptionConfig[ucIndex].OptionData.OptionBits.bRevToReset = 1; //b6=1 0.53
            ee_OptionConfig[ucIndex].ucStepperThreshold = THR_DEFAULT_STEPS;
            ee_OptionConfig[ucIndex].ucBladeResetTime = HOME_DEFAULT_TIME;
            ee_OptionConfig[ucIndex].ucNoTorqueTime = NOTORQUE_DEFAULT_TIME;
            if (ucIndex == STEPPER_FILTER)
            {
                ee_OptionConfig[ucIndex].OptionData.OptionBits.bRevToReset = 0; //b6=0 0.53
                ee_OptionConfig[ucIndex].ucNoTorqueTime = NOTORQUE_FILT_DEF_TIME;
                ee_OptionConfig[ucIndex].OptionData.OptionBits.bStartupReset = 0; //0.35 correction
                ee_OptionConfig[ucIndex].ucBladeResetTime = HOME_FILTER_DEFAULT_TIME;
            }
        }
//        EE_data.record_label = 4;
//        EE_data.pData = (uint *) & ee_OptionConfig;
//        EE_data.data_size = ((sizeof (ee_OptionConfig) + 1) / 2 - 1); /* size in words (-1) */
//        if (!NV_Write(&EE_data))
            g_InternalFlag.bit.bEepromInitialized = true;
//        else
//            g_InternalFlag.bit.bEepromError = true;
//    }
//    else
//    {
//        // Load last saved values to use as initial values
//        for (ucIndex = 0; ucIndex < MAX_STEPPER; ucIndex++)
//        {
//            ee_OptionConfig[ucIndex] = *(OptionConfiguration NV_FAR *) EE_data.pData;
//            EE_data.pData += sizeof (OptionConfiguration) / 2;
//        }
//    }

//    EE_data.record_label = 5;
//    ucRc = NV_Read(&EE_data);
//    if (ucRc || bNewStruct)
//    {
        // Set default values because this is the first time this program is being run
        init = TRUE;
        temp32 = ((UINT32) ee_ConfigBoards[STEPPER_A].uwMaxSteps * 240UL) / 495UL;
        ee_WindowConfig[WINDOW_0].lPosStepperA = temp32;
        temp32 = ((UINT32) ee_ConfigBoards[STEPPER_B].uwMaxSteps * 240UL) / 495UL;
        ee_WindowConfig[WINDOW_0].lPosStepperB = temp32;
        ee_WindowConfig[WINDOW_0].lPosStepperC = K_FILTER_STEPS_DEF;

        temp32 = ((UINT32) ee_ConfigBoards[STEPPER_A].uwMaxSteps * 350UL) / 495UL;
        ee_WindowConfig[WINDOW_1].lPosStepperA = temp32;
        temp32 = ((UINT32) ee_ConfigBoards[STEPPER_B].uwMaxSteps * 350UL) / 495UL;
        ee_WindowConfig[WINDOW_1].lPosStepperB = temp32;
        ee_WindowConfig[WINDOW_1].lPosStepperC = (K_FILTER_STEPS_DEF * 2);

        ee_WindowConfig[WINDOW_2].lPosStepperA = ee_ConfigBoards[STEPPER_A].uwMaxSteps;
        ee_WindowConfig[WINDOW_2].lPosStepperB = ee_ConfigBoards[STEPPER_B].uwMaxSteps;
        ee_WindowConfig[WINDOW_2].lPosStepperC = (K_FILTER_STEPS_DEF * 3);

        ee_WindowConfig[WINDOW_3].lPosStepperA = 0;
        ee_WindowConfig[WINDOW_3].lPosStepperB = 0;
        ee_WindowConfig[WINDOW_3].lPosStepperC = 0;

//        EE_data.record_label = 5;
//        EE_data.pData = (uint *) & ee_WindowConfig;
//        EE_data.data_size = ((sizeof (ee_WindowConfig) + 1) / 2 - 1); /* size in words (-1) */
//        if (!NV_Write(&EE_data))
            g_InternalFlag.bit.bEepromInitialized = true;
//        else
//            g_InternalFlag.bit.bEepromError = true;
//    }
//    else
//    {
//        // Load last saved values to use as initial values
//        for (ucIndex = 0; ucIndex < MAX_WINDOW; ucIndex++)
//        {
//            ee_WindowConfig[ucIndex] = *(WindowConfiguration NV_FAR *) EE_data.pData;
//            EE_data.pData += sizeof (WindowConfiguration) / 2;
//        }
//    }

//    EE_data.record_label = CAN_LABEL;
//    ucRc = NV_Read(&EE_data);
//    if (ucRc || bNewStruct)
//    {
        // Set default values because this is the first time this program is being run
        init = TRUE;

        ee_CanColl.unIdCommand = ID_COM_COLL;
        ee_CanColl.unIdStatus = ID_STATUS;
        ee_CanColl.unIdDisplay = 0x7C0;
        ee_CanColl.unIdLamp = 0x700;
        ee_CanColl.unIdRot = ADDRESS_ROT;
        ee_CanColl.unIdIride = ADDRESS_IRIS;
        ee_CanColl.unIdOtherCommands = ID_COM_COLL_OTHER;

//        EE_data.record_label = CAN_LABEL;
//        EE_data.pData = (uint *) & ee_CanColl;
//        EE_data.data_size = ((sizeof (ee_CanColl) + 1) / 2 - 1); /* size in words (-1) */
//        if (!NV_Write(&EE_data))
            g_InternalFlag.bit.bEepromInitialized = true;
//        else
//            g_InternalFlag.bit.bEepromError = true;
//    }
//    else
//    {
//        // Load last saved values to use as initial values
//        ee_CanColl = *(CollCanStruct NV_FAR *) EE_data.pData;
//    }
//
//    EE_data.record_label = COLLIMATOR_LABEL;
//    ucRc = NV_Read(&EE_data);
//    if (ucRc || bNewStruct)
//    {
        // Set default values because this is the first time this program is being run
        init = TRUE;

        ee_ConfigColl.ucLanguage = ENG_LANG;
        ee_ConfigColl.ucCmUnit = CM_UNIT;
        ee_ConfigColl.ucShowDff = true;
        ee_ConfigColl.ucRampTime = 0; //1 second
#ifdef SYS_CANOPEN
        ee_ConfigColl.ucFilter = true;
#else
        ee_ConfigColl.ucFilter = FALSE;
#endif

#ifndef TEST_FIERA
        ee_ConfigColl.ucFilter = FALSE;
#else
        ee_ConfigColl.ucFilter = TRUE;
#endif

        ee_ConfigColl.ucFilterType = MM2_FILTER;
        for (ucIndex = 0; ucIndex < NUM_CHAR_CUSTOM_FILTER; ucIndex++)  // riempie di spazi
        {
            ee_ConfigColl.NomiFiltroCustom[0][ucIndex] = ' ';
            ee_ConfigColl.NomiFiltroCustom[1][ucIndex] = ' ';
            ee_ConfigColl.NomiFiltroCustom[2][ucIndex] = ' ';
            ee_ConfigColl.NomiFiltroCustom[3][ucIndex] = ' ';
        }

        ee_ConfigColl.NomiFiltroCustom[0][0] = 'F';
        ee_ConfigColl.NomiFiltroCustom[0][1] = '0';
        ee_ConfigColl.NomiFiltroCustom[0][7] = 0;
        ee_ConfigColl.NomiFiltroCustom[0][NUM_CHAR_CUSTOM_FILTER] = 0;
        ee_ConfigColl.NomiFiltroCustom[1][0] = 'F';
        ee_ConfigColl.NomiFiltroCustom[1][1] = '1';
        ee_ConfigColl.NomiFiltroCustom[1][7] = 0;
        ee_ConfigColl.NomiFiltroCustom[1][NUM_CHAR_CUSTOM_FILTER] = 0;
        ee_ConfigColl.NomiFiltroCustom[2][0] = 'F';
        ee_ConfigColl.NomiFiltroCustom[2][1] = '2';
        ee_ConfigColl.NomiFiltroCustom[2][7] = 0;
        ee_ConfigColl.NomiFiltroCustom[2][NUM_CHAR_CUSTOM_FILTER] = 0;
        ee_ConfigColl.NomiFiltroCustom[3][0] = 'F';
        ee_ConfigColl.NomiFiltroCustom[3][1] = '3';
        ee_ConfigColl.NomiFiltroCustom[3][7] = 0;
        ee_ConfigColl.NomiFiltroCustom[3][NUM_CHAR_CUSTOM_FILTER] = 0;

        ee_ConfigColl.ucAutoLight = false;

        ee_ConfigColl.ucInclinometer = false;
        ee_ConfigColl.ucAutoLimits = false;
        ee_ConfigColl.ucLightTime = 1; //30 seconds
        ee_ConfigColl.ucLatDffSx = LAT_DFF_CAN;
        ee_ConfigColl.ucLatDffDx = LAT_DFF_CAN;
        ee_ConfigColl.ucLatRecSx = LAT_REC_CAN;

        ee_ConfigColl.ucVertDff = VERT_DFF_CAN;
        ee_ConfigColl.ucVertRec = VERT_REC_CAN;
        ee_ConfigColl.ucLatRecDx = LAT_REC_CAN;

        ee_ConfigColl.uwFixedDff = DFF_DEF;
        ee_ConfigColl.ucInputDffPot = POT_SINGLE_STAND;
        ee_ConfigColl.uwMinDff = 80;
        ee_ConfigColl.uwMaxDff = 180;
        ee_ConfigColl.ucDftVert = 0;
        ee_ConfigColl.ucDftSx = 0;
        ee_ConfigColl.ucDftDx = 0;
        ee_ConfigColl.ucCross = true;
        ee_ConfigColl.ucLong = true;
        ee_ConfigColl.ucIrisPresent = IRIDE_NO;
        ee_ConfigColl.ucIrisEnabled = TRUE;
        ee_ConfigColl.ucInclDegrees = 3;
        ee_ConfigColl.ucFilterCheck = false;
        ee_ConfigColl.ucShowLock = true;
        ee_ConfigColl.ucShowAngle = false;
        ee_ConfigColl.ucIrisShow = false;
        ee_ConfigColl.ucIrisIndipendent = false;
        ee_ConfigColl.bSquareRtz = false;
        ee_ConfigColl.ucMeterEnabled = FALSE;

        ee_ConfigColl.ucDeltaCrossOpenClose = DELTA_CROSS_OPEN_CLOSE;
        ee_ConfigColl.ucDeltaLongOpenClose = DELTA_LONG_OPEN_CLOSE;
        ee_ConfigColl.ucDeltaCrossOpenCloseMan = DELTA_CROSS_OPEN_CLOSE_MAN;
        ee_ConfigColl.ucDeltaLongOpenCloseMan = DELTA_LONG_OPEN_CLOSE_MAN;
        ee_ConfigColl.scCorrCrossDff = DELTA_DFF_MIN;
        ee_ConfigColl.scCorrLongDff = DELTA_DFF_MIN;
        ee_ConfigColl.scCorrIrideDff = DELTA_DFF_MIN;

        ee_ConfigColl.ucDisplayType = DISPLAY_STANDARD;
        ee_ConfigColl.ucAnalogInput = DIGITAL_IN;
        ee_ConfigColl.ucCanProtocol = CAN_STANDARD;
        ee_ConfigColl.ucLcdFree = LCD_PROTO; // il Nome Collimatore segue il Nome Protocollo
        for (ucIndex = 0; ucIndex < 7; ucIndex++)
            ee_ConfigColl.ucLcdName[ucIndex] = ' ';
        ee_ConfigColl.ucLcdName[7] = 0; // terminatore


        ee_ConfigColl.ucCanSpeed = CAN_500K;

        ee_ConfigColl.ucTipoProx = TIPO_PROX_NPN;
        ee_ConfigColl.ucSwContext = 0;

        ee_ConfigColl.ucRotation = ROTAZIONE_NO;
        ee_ConfigColl.ucHardwareType = HARDWARE_NO_POT;
        for (ucIndex = 0; ucIndex < NUM_TAR_POT; ucIndex++)
        {
            ee_ConfigColl.uwPotCross[ucIndex] = (1023 * ucIndex) / (NUM_TAR_POT - 1);
            ee_ConfigColl.uwPotLong[ucIndex] = (1023 * ucIndex) / (NUM_TAR_POT - 1);
        }
        ee_ConfigColl.ucAlarm_0x100 = ALARM_0x100_NO;
        ee_ConfigColl.OffsetLamelle = OFFSET_LAMELLE;
        ee_ConfigColl.OffsetMetro = OFFSET_METRO;
        ee_ConfigColl.CorrType = TIPO_CORR_PERC; // default
        ee_ConfigColl.EncMaxFreq = DEF_ENC_MAX_FREQ;
        ee_ConfigColl.EncMaxSpeed = DEF_ENC_MAX_SPEED;
        ee_ConfigColl.EncAccel = DEF_ENC_ACCEL;
        ee_ConfigColl.TipoManualSpost = TIPO_MAN_SPOST_OLD;
        ee_ConfigColl.ManualSpeed = SPOST_MM_DEFAULT;
        ee_ConfigColl.EnFormatiMin = FALSE;
        ee_ConfigColl.ucManualColl = DEF_MANUAL_COLL;
        ee_ConfigColl.ucVisuaApeIris = DEF_VISUA_APE_IRIS;
        ee_ConfigColl.TempoAccensioneLamp = 30; // in secondi tempo di default accensione lampada
        ee_ConfigColl.TempoInvio7F5 = 0; // in ms - invio msg 0x7F5
        ee_ConfigColl.ucFiltraggioIncl = FALSE;
        ee_ConfigColl.ucRisposte7F8 = DEF_RISPOSTE_7F8;
        ee_ConfigColl.TastieraVilla = DEF_TASTIERA_VILLA;
        ee_ConfigColl.IdOffsetTastieraVilla = 0x04;
        ee_ConfigColl.ASR003 = DEF_ABILITAZIONE_ASR003;
        ee_ConfigColl.displayAngoloTubo = DEF_DISPLAY_TUBO;
        ee_ConfigColl.JitterKnobs = DEF_JITTER_KNOBS;
        ee_ConfigColl.decSecondsConfig = DEF_SECONDS_CONFIG;
        ee_ConfigColl.TempoInvioInclinometro = 0;
        ee_ConfigColl.OffsetZero = TRUE;

//        EE_data.record_label = COLLIMATOR_LABEL;
//        EE_data.pData = (uint *) & ee_ConfigColl;
//        EE_data.data_size = ((sizeof (ee_ConfigColl) + 1) / 2 - 1); /* size in words (-1) */
//        if (!NV_Write(&EE_data))
            g_InternalFlag.bit.bEepromInitialized = true;
//        else
//            g_InternalFlag.bit.bEepromError = true;
//    }
//    else
//    {
//        // Load last saved values to use as initial values
//        ee_ConfigColl = *(CollConfigStruct NV_FAR *) EE_data.pData;
//        IrideIndipendente = ee_ConfigColl.ucIrisIndipendent;
//    }
    KeyLightTimer = (UINT16) ee_ConfigColl.TempoAccensioneLamp * BASE_TIMER_LUCE;


//    EE_data.record_label = FORMAT_LABEL;
//    ucRc = NV_Read(&EE_data);
//    if (ucRc || bNewStruct)
//    {
        // Set default values because this is the first time this program is being run
        init = TRUE;
        for (ucIndex = 0; ucIndex < MAX_FORMATS; ucIndex++)
        {
            ee_FixFormatColl.ucFixedCmCross[ucIndex] = CROSS_FORMAT_CM;
            ee_FixFormatColl.ucFixedCmLong[ucIndex] = LONG_FORMAT_CM;
            ee_FixFormatColl.ucFixedInCross[ucIndex] = CROSS_FORMAT_IN;
            ee_FixFormatColl.ucFixedInLong[ucIndex] = LONG_FORMAT_IN;
            ee_FixFormatColl.ucFixedCmIris[ucIndex] = IRIS_FORMAT_CM;
            ee_FixFormatColl.ucFixedInIris[ucIndex] = IRIS_FORMAT_IN;
        }

        ee_FixFormatColl.ucNumFormats = 0;
        ee_FixFormatColl.ucNumFormatsIris = 0;

//        EE_data.record_label = FORMAT_LABEL;
//        EE_data.pData = (uint *) & ee_FixFormatColl;
//        EE_data.data_size = ((sizeof (ee_FixFormatColl) + 1) / 2 - 1); /* size in words (-1) */
//        if (!NV_Write(&EE_data))
            g_InternalFlag.bit.bEepromInitialized = true;
//        else
//            g_InternalFlag.bit.bEepromError = true;
//    }
//    else
//    {
//        // Load last saved values to use as initial values
//        ee_FixFormatColl = *(CollFormatStruct NV_FAR *) EE_data.pData;
//    }


//    EE_data.record_label = OFFSET_LABEL;
//    ucRc = NV_Read(&EE_data);
//    if (ucRc || bNewStruct)
//    {
//        // Set default values because this is the first time this program is being run
        init = TRUE;
        ee_OffsetColl.nInclOffsetX = 486;
        ee_OffsetColl.nInclOffsetY = 370;
        ee_OffsetColl.InclOffsetDxX = 380;
        ee_OffsetColl.InclOffsetDxY = 480;
        ee_OffsetColl.InclOffsetSxX = 380;
        ee_OffsetColl.InclOffsetSxY = 260;
        ee_OffsetColl.nMaxCross = 430;
        ee_OffsetColl.nMaxLong = 430;
        ee_OffsetColl.ucEncoderSens = ENC_MOVE_TIME;
        ee_OffsetColl.ucEncoderInv = ENC_INV_TIME;
        ee_OffsetColl.bSquareRotated = false;

//        EE_data.record_label = OFFSET_LABEL;
//        EE_data.pData = (uint *) & ee_OffsetColl;
//        EE_data.data_size = ((sizeof (ee_OffsetColl) + 1) / 2 - 1); /* size in words (-1) */
//        if (!NV_Write(&EE_data))
            g_InternalFlag.bit.bEepromInitialized = true;
//        else
//            g_InternalFlag.bit.bEepromError = true;
//    }
//    else
//    {
//        // Load last saved values to use as initial values
//        ee_OffsetColl = *(CollOffsetStruct NV_FAR *) EE_data.pData;
//    }



//    EE_data.record_label = TOP_BUCKY_LABEL;
//    ucRc = NV_Read(&EE_data);
//    if (ucRc || bNewStruct)
//    {
        // Set default values because this is the first time this program is being run
        init = TRUE;
        memcpy(&ee_TopBuckyColl, &BuckyMatrix, sizeof (BuckyMatrix));

//        EE_data.record_label = TOP_BUCKY_LABEL;
//        EE_data.pData = (uint *) & ee_TopBuckyColl;
//        EE_data.data_size = ((sizeof (ee_TopBuckyColl) + 1) / 2 - 1); /* size in words (-1) */
//        if (!NV_Write(&EE_data))
            g_InternalFlag.bit.bEepromInitialized = true;
//        else
//            g_InternalFlag.bit.bEepromError = true;
//    }
//    else
//    {
//        // Load last saved values to use as initial values
//        ee_TopBuckyColl = *(CollTopBuckyStruct NV_FAR *) EE_data.pData;
//    }

//    EE_data.record_label = SX_BUCKY_LABEL;
//    ucRc = NV_Read(&EE_data);
//    if (ucRc || bNewStruct)
//    {
        // Set default values because this is the first time this program is being run
        init = TRUE;
        memcpy(&ee_SxBuckyColl, &BuckyMatrix, sizeof (BuckyMatrix));

//        EE_data.record_label = SX_BUCKY_LABEL;
//        EE_data.pData = (uint *) & ee_SxBuckyColl;
//        EE_data.data_size = ((sizeof (ee_SxBuckyColl) + 1) / 2 - 1); /* size in words (-1) */
//        if (!NV_Write(&EE_data))
            g_InternalFlag.bit.bEepromInitialized = true;
//        else
//            g_InternalFlag.bit.bEepromError = true;
//    }
//    else
//    {
//        // Load last saved values to use as initial values
//        ee_SxBuckyColl = *(CollSxBuckyStruct NV_FAR *) EE_data.pData;
//    }

//    EE_data.record_label = DX_BUCKY_LABEL;
//    ucRc = NV_Read(&EE_data);
//    if (ucRc || bNewStruct)
//    {
        // Set default values because this is the first time this program is being run
        init = TRUE;
        memcpy(&ee_DxBuckyColl, &BuckyMatrix, sizeof (BuckyMatrix));

//        EE_data.record_label = DX_BUCKY_LABEL;
//        EE_data.pData = (uint *) & ee_DxBuckyColl;
//        EE_data.data_size = ((sizeof (ee_DxBuckyColl) + 1) / 2 - 1); /* size in words (-1) */
//        if (!NV_Write(&EE_data))
            g_InternalFlag.bit.bEepromInitialized = true;
//        else
//            g_InternalFlag.bit.bEepromError = true;
//    }
//    else
//    {
//        // Load last saved values to use as initial values
//        ee_DxBuckyColl = *(CollDxBuckyStruct NV_FAR *) EE_data.pData;
//    }

//    EE_data.record_label = CAL_LABEL;
//    ucRc = NV_Read(&EE_data);
//    if (ucRc || bNewStruct)
//    {
        // Set default values because this is the first time this program is being run
        init = TRUE;
        ee_CalColl.uwLowTable = 0;
        ee_CalColl.uwTable25 = 250;
        ee_CalColl.uwStandUp = 1023;
        ee_CalColl.uwStandLo50 = 500;
        ee_CalColl.uwStand100 = 250;
        ee_CalColl.uwCollTable = 100;
        ee_CalColl.DffDxPotMin = 0;
        ee_CalColl.DffDxMisMin = 80;
        ee_CalColl.DffDxPotMin = 1000;
        ee_CalColl.DffDxMisMax = 250;
        ee_CalColl.DffSxPotMin = 0;
        ee_CalColl.DffSxMisMin = 80;
        ee_CalColl.DffSxPotMin = 1000;
        ee_CalColl.DffSxMisMax = 250;

        ee_CalColl.uwDffHor[0] = 90;
        ee_CalColl.uwDffHor[1] = 110;
        ee_CalColl.uwDffHor[2] = 120;
        ee_CalColl.uwDffHor[3] = 140;
        ee_CalColl.uwDffHor[4] = 160;
        ee_CalColl.uwDffHor[5] = 200;
        ee_CalColl.uwDffHor[6] = 205;

//        EE_data.record_label = CAL_LABEL;
//        EE_data.pData = (uint *) & ee_CalColl;
//        EE_data.data_size = ((sizeof (ee_CalColl) + 1) / 2 - 1); /* size in words (-1) */
//        if (!NV_Write(&EE_data))
            g_InternalFlag.bit.bEepromInitialized = true;
//        else
//            g_InternalFlag.bit.bEepromError = true;
//    }
//    else
//    {
//        // Load last saved values to use as initial values
//        ee_CalColl = *(CollCalibrateStruct NV_FAR *) EE_data.pData;
//    }
//

//    EE_data.record_label = FILTER_LABEL;
//    ucRc = NV_Read(&EE_data);
//    if (ucRc || bNewStruct)
//    {
        // Set default values because this is the first time this program is being run
        init = TRUE;
        ee_FilterConfig.TipoMotore = MOTORE_FILTRO_DEF;
        ee_FilterConfig.uwSteps = K_FILTER_STEPS_DEF;
        ee_FilterConfig.StepsResetMin = FILTER_RESET_MIN_OLD;
        ee_FilterConfig.StepsResetMax = FILTER_RESET_MAX_OLD;
#ifndef TEST_FIERA
        ee_FilterConfig.uwType = FILTER_5_HOLES;
#else
        ee_FilterConfig.uwType = FILTER_1_HOLE;
#endif
        ee_FilterConfig.Config_Filtro[0] = 0;
        ee_FilterConfig.Config_Filtro[1] = 1;
        ee_FilterConfig.Config_Filtro[2] = 2;
        ee_FilterConfig.Config_Filtro[3] = 3;
//        EE_data.record_label = FILTER_LABEL;
//        EE_data.pData = (uint *) & ee_FilterConfig;
//        EE_data.data_size = ((sizeof (ee_FilterConfig) + 1) / 2 - 1); /* size in words (-1) */
//        if (!NV_Write(&EE_data))
            g_InternalFlag.bit.bEepromInitialized = true;
//        else
//            g_InternalFlag.bit.bEepromError = true;
//    }
//    else
//    {
//        // Load last saved values to use as initial values
//        ee_FilterConfig = *(FilterConfigStruct NV_FAR *) EE_data.pData;
//    }


//    EE_data.record_label = IRIS_LABEL;
//    ucRc = NV_Read(&EE_data);
//    if (ucRc || bNewStruct)
//    {
        // Set default values because this is the first time this program is being run
        init = TRUE;
        for (ucIndex = 0; ucIndex < (IRIS_FORMAT_MAX + 2); ucIndex++)
        {
            ee_IrisSteps.iride_calib_pot[ucIndex] = 0;
            ee_IrisSteps.iride_calib_passi[ucIndex] = 0;
        }
        ee_IrisSteps.iride_calib_pot[IRIS_FORMAT_MAX + 1] = 1023;
        ee_IrisSteps.iride_calib_passi[IRIS_FORMAT_MAX + 1] = 10000;

//        EE_data.record_label = IRIS_LABEL;
//        EE_data.pData = (uint *) & ee_IrisSteps;
//        EE_data.data_size = ((sizeof (ee_IrisSteps) + 1) / 2 - 1); /* size in words (-1) */
//        if (!NV_Write(&EE_data))
            g_InternalFlag.bit.bEepromInitialized = true;
//        else
//            g_InternalFlag.bit.bEepromError = true;
//    }
//    else
//    {
//        // Load last saved values to use as initial values
//        ee_IrisSteps = *(IrisStepsStruct NV_FAR *) EE_data.pData;
//    }


    if (g_InternalFlag.bit.bEepromInitialized || g_InternalFlag.bit.bEepromError)
        g_InternalFlag.bit.bEepromOk = false;


    ee_ConfigColl.ucFilter = TRUE;
    ee_FilterConfig.TipoMotore = MOTORE_FILTRO_NEW;
    ee_FilterConfig.uwSteps = K_FILTER_STEPS_NEW;
    ee_FilterConfig.StepsResetMin = FILTER_RESET_MIN_NEW;
    ee_FilterConfig.StepsResetMax = FILTER_RESET_MAX_NEW;
    ee_FilterConfig.uwType = FILTER_5_HOLES;
    ee_ConfigBoards[STEPPER_C].uwFMin = 500;
    ee_ConfigBoards[STEPPER_C].uwFMax = 10000;
    ee_ConfigBoards[STEPPER_FILTER].ucStep = 0x06; //Step: 1/4

#ifdef _IRIDE_
    if (init == TRUE)
    {
        ee_ConfigBoards[STEPPER_A].uwMaxSteps = DEF_PASSI_A_IRIDE; // passi massimi da tutto chiuso a tutto aperto
        ee_ConfigBoards[STEPPER_A].uwRefSize = DEF_SIZE_A_IRIDE; // formato di riferimento in mm
        ee_ConfigBoards[STEPPER_A].uwRefSteps = DEF_PASSI_A_IRIDE; // formato di riferimento in passi
        ee_WindowConfig[WINDOW_2].lPosStepperA = DEF_PASSI_A_IRIDE;
        g_MaxSteps[STEPPER_CROSS] = DEF_PASSI_A_IRIDE;
        g_RefSize[STEPPER_CROSS] = DEF_SIZE_A_IRIDE;
        g_RefSteps[STEPPER_CROSS] = DEF_PASSI_A_IRIDE;
        ee_ConfigBoards[STEPPER_B].uwMaxSteps = DEF_PASSI_B_IRIDE; // passi massimi da tutto chiuso a tutto aperto
        ee_ConfigBoards[STEPPER_B].uwRefSize = DEF_SIZE_B_IRIDE; // formato di riferimento in mm
        ee_ConfigBoards[STEPPER_B].uwRefSteps = DEF_PASSI_B_IRIDE; // formato di riferimento in passi
        ee_WindowConfig[WINDOW_2].lPosStepperB = DEF_PASSI_B_IRIDE;
        g_MaxSteps[STEPPER_LONG] = DEF_PASSI_B_IRIDE;
        g_RefSize[STEPPER_LONG] = DEF_SIZE_B_IRIDE;
        g_RefSteps[STEPPER_LONG] = DEF_PASSI_B_IRIDE;

        ee_ConfigColl.ucIrisPresent = IRIDE_YES;
        ee_ConfigColl.ucIrisEnabled = TRUE;
        //      ee_ConfigColl.ucVertDff = VERT_DFF_FIXED;
        //      ee_ConfigColl.ucVertRec = VERT_REC_FISSI;

        //        ee_ConfigColl.ucFilter = TRUE;
        ee_ConfigColl.ucFilter = FALSE;
        ee_FilterConfig.TipoMotore = MOTORE_FILTRO_NEW;
        ee_FilterConfig.uwSteps = K_FILTER_STEPS_NEW;
        ee_FilterConfig.StepsResetMin = FILTER_RESET_MIN_NEW;
        ee_FilterConfig.StepsResetMax = FILTER_RESET_MAX_NEW;
        ee_FilterConfig.uwType = FILTER_5_HOLES;
        ee_ConfigBoards[STEPPER_C].uwFMin = 500;
        ee_ConfigBoards[STEPPER_C].uwFMax = 10000;
        ee_ConfigBoards[STEPPER_FILTER].ucStep = 0x06; //Step: 1/4
        g_ulUpdateEeprom |= 0x000FFFFF;
    }
#endif
}

//--------------------------------------------------------------
void sys_eeprom_sync(void)
{

//    if (!g_ulUpdateEeprom)
//        return;
//
//    if (g_ulUpdateEeprom & EEP_UPDATE_COLLIMATOR){
//        EE_data.record_label = COLLIMATOR_LABEL;
//        EE_data.pData = (uint *)&ee_ConfigColl;
//        EE_data.data_size = ((sizeof(ee_ConfigColl)+1)/2-1); /* size in words (-1) */
//        g_ulUpdateEeprom&=~EEP_UPDATE_COLLIMATOR;
//        if (!NV_Write(&EE_data))
//            g_InternalFlag.bit.bEepromUpdated=true;
//        else
//            g_InternalFlag.bit.bEepromError=true;
//        return;
//    }
//    if (g_ulUpdateEeprom & EEP_UPDATE_OFFSET){
//        EE_data.record_label = OFFSET_LABEL;
//        EE_data.pData = (uint *)&ee_OffsetColl;
//        EE_data.data_size = ((sizeof(ee_OffsetColl)+1)/2-1); /* size in words (-1) */
//        g_ulUpdateEeprom&=~EEP_UPDATE_OFFSET;
//        if (!NV_Write(&EE_data))
//            g_InternalFlag.bit.bEepromUpdated=true;
//        else
//            g_InternalFlag.bit.bEepromError=true;
//        return;
//    }
//    if (g_ulUpdateEeprom & EEP_UPDATE_CAL){
//        EE_data.record_label = 6;
//        EE_data.pData = (uint *)&ee_CalAdcConfig;
//        EE_data.data_size = ((sizeof(ee_CalAdcConfig)+1)/2-1); /* size in words (-1) */
//        g_ulUpdateEeprom&=~EEP_UPDATE_CAL;
//        if (!NV_Write(&EE_data))
//            g_InternalFlag.bit.bEepromUpdated=true;
//        else
//            g_InternalFlag.bit.bEepromError=true;
//        return;
//    }
//    if (g_ulUpdateEeprom & EEP_UPDATE_WINDOWS){
//        EE_data.record_label = 5;
//        EE_data.pData = (uint *)&ee_WindowConfig;
//        EE_data.data_size = ((sizeof(ee_WindowConfig)+1)/2-1); /* size in words (-1) */
//        g_ulUpdateEeprom&=~EEP_UPDATE_WINDOWS;
//        if (!NV_Write(&EE_data))
//            g_InternalFlag.bit.bEepromUpdated=true;
//        else
//            g_InternalFlag.bit.bEepromError=true;
//        return;
//    }
//    if (g_ulUpdateEeprom & EEP_UPDATE_OPTIONS){
//        EE_data.record_label = 4;
//        EE_data.pData = (uint *)&ee_OptionConfig;
//        EE_data.data_size = ((sizeof(ee_OptionConfig)+1)/2-1); /* size in words (-1) */
//        g_ulUpdateEeprom&=~EEP_UPDATE_OPTIONS;
//        if (!NV_Write(&EE_data))
//            g_InternalFlag.bit.bEepromUpdated=true;
//        else
//            g_InternalFlag.bit.bEepromError=true;
//        return;
//    }
//    if ((g_ulUpdateEeprom & EEP_UPDATE_DMODE)||(g_ulUpdateEeprom & EEP_UPDATE_DSPEED)){
//        EE_data.record_label = 3;
//        EE_data.pData = (uint *)&ee_ConfigBoards;
//        EE_data.data_size = ((sizeof(ee_ConfigBoards)+1)/2-1); /* size in words (-1) */
//        g_ulUpdateEeprom&=~EEP_UPDATE_DMODE;
//        g_ulUpdateEeprom&=~EEP_UPDATE_DSPEED;
//        if (!NV_Write(&EE_data))
//            g_InternalFlag.bit.bEepromUpdated=true;
//        else
//            g_InternalFlag.bit.bEepromError=true;
//        return;
//    }
//    if (g_ulUpdateEeprom & EEP_UPDATE_CAN_ADDRESS){
//        EE_data.record_label = 2;
//        EE_data.pData = (uint *)&ee_CanAddressConfig;
//        EE_data.data_size = ((sizeof(ee_CanAddressConfig)+1)/2-1); /* size in words (-1) */
//        g_ulUpdateEeprom&=~EEP_UPDATE_CAN_ADDRESS;
//        if (!NV_Write(&EE_data))
//            g_InternalFlag.bit.bEepromUpdated=true;
//        else
//            g_InternalFlag.bit.bEepromError=true;
//        return;
//    }
//    if (g_ulUpdateEeprom & EEP_UPDATE_SERIAL){
//        EE_data.record_label = 1;
//        EE_data.pData = (uint *)&ee_RSR008B_Version;
//        EE_data.data_size = ((sizeof(ee_RSR008B_Version)+1)/2-1); /* size in words (-1) */
//        g_ulUpdateEeprom&=~EEP_UPDATE_SERIAL;
//        if (!NV_Write(&EE_data))
//            g_InternalFlag.bit.bEepromUpdated=true;
//        else
//            g_InternalFlag.bit.bEepromError=true;
//        return;
//    }
//    if (g_ulUpdateEeprom & EEP_UPDATE_TOPBUCKY){
//        EE_data.record_label = TOP_BUCKY_LABEL;
//        EE_data.pData = (uint *)&ee_TopBuckyColl;
//        EE_data.data_size = ((sizeof(ee_TopBuckyColl)+1)/2-1); /* size in words (-1) */
//        if (!NV_Write(&EE_data))
//            g_InternalFlag.bit.bEepromUpdated=true;
//        else
//            g_InternalFlag.bit.bEepromError=true;
//        g_ulUpdateEeprom&=~EEP_UPDATE_TOPBUCKY;
//        return;
//    }
//    if (g_ulUpdateEeprom & EEP_UPDATE_SXBUCKY){
//        EE_data.record_label = SX_BUCKY_LABEL;
//        EE_data.pData = (uint *)&ee_SxBuckyColl;
//        EE_data.data_size = ((sizeof(ee_SxBuckyColl)+1)/2-1); /* size in words (-1) */
//        if (!NV_Write(&EE_data))
//            g_InternalFlag.bit.bEepromUpdated=true;
//        else
//            g_InternalFlag.bit.bEepromError=true;
//        g_ulUpdateEeprom&=~EEP_UPDATE_SXBUCKY;
//        return;
//    }
//    if (g_ulUpdateEeprom & EEP_UPDATE_DXBUCKY){
//        EE_data.record_label = DX_BUCKY_LABEL;
//        EE_data.pData = (uint *)&ee_DxBuckyColl;
//        EE_data.data_size = ((sizeof(ee_DxBuckyColl)+1)/2-1); /* size in words (-1) */
//        if (!NV_Write(&EE_data))
//            g_InternalFlag.bit.bEepromUpdated=true;
//        else
//            g_InternalFlag.bit.bEepromError=true;
//        g_ulUpdateEeprom&=~EEP_UPDATE_DXBUCKY;
//        return;
//    }
//    if (g_ulUpdateEeprom & EEP_UPDATE_CALIB){
//        EE_data.record_label = CAL_LABEL;
//        EE_data.pData = (uint *)&ee_CalColl;
//        EE_data.data_size = ((sizeof(ee_CalColl)+1)/2-1); /* size in words (-1) */
//        if (!NV_Write(&EE_data))
//            g_InternalFlag.bit.bEepromUpdated=true;
//        else
//            g_InternalFlag.bit.bEepromError=true;
//        g_ulUpdateEeprom&=~EEP_UPDATE_CALIB;
//        return;
//    }
//    if (g_ulUpdateEeprom & EEP_UPDATE_FILTER){
//        EE_data.record_label = FILTER_LABEL;
//        EE_data.pData = (uint *)&ee_FilterConfig;
//        EE_data.data_size = ((sizeof(ee_FilterConfig)+1)/2-1); /* size in words (-1) */
//        if (!NV_Write(&EE_data))
//            g_InternalFlag.bit.bEepromUpdated=true;
//        else
//            g_InternalFlag.bit.bEepromError=true;
//        g_ulUpdateEeprom&=~EEP_UPDATE_FILTER;
//        return;
//    }
//    if (g_ulUpdateEeprom & EEP_UPDATE_IRIS){
//        EE_data.record_label = IRIS_LABEL;
//        EE_data.pData = (uint *)&ee_IrisSteps;
//        EE_data.data_size = ((sizeof(ee_IrisSteps)+1)/2-1); /* size in words (-1) */
//        if (!NV_Write(&EE_data))
//            g_InternalFlag.bit.bEepromUpdated=true;
//        else
//            g_InternalFlag.bit.bEepromError=true;
//        g_ulUpdateEeprom&=~EEP_UPDATE_IRIS;
//        return;
//    }
//    if (g_ulUpdateEeprom & EEP_UPDATE_CAN){
//        EE_data.record_label = CAN_LABEL;
//        EE_data.pData = (uint *)&ee_CanColl;
//        EE_data.data_size = ((sizeof(ee_CanColl)+1)/2-1); /* size in words (-1) */
//        if (!NV_Write(&EE_data))
//            g_InternalFlag.bit.bEepromUpdated=true;
//        else
//            g_InternalFlag.bit.bEepromError=true;
//        g_ulUpdateEeprom&=~EEP_UPDATE_CAN;
//        return;
//    }
//    if (g_ulUpdateEeprom & EEP_UPDATE_FORMAT){
//        EE_data.record_label = FORMAT_LABEL;
//        EE_data.pData = (uint *)&ee_FixFormatColl;
//        EE_data.data_size = ((sizeof(ee_FixFormatColl)+1)/2-1); /* size in words (-1) */
//        if (!NV_Write(&EE_data))
//            g_InternalFlag.bit.bEepromUpdated=true;
//        else
//            g_InternalFlag.bit.bEepromError=true;
//        g_ulUpdateEeprom&=~EEP_UPDATE_FORMAT;
//        return;
//    }
//
}



//******************************************************
//  calcola e salva CRC
//  se erase == TRUE cancella i CRC16
//******************************************************
void calcola_crc(UINT8 erase)
{
    UINT16 crc16m, crc16a, crc16i;
    UINT32 temp32;
    ssp_err_t flash_err;


    crc16m = CRC16((UINT8 *)&veeMainInfo, MAININFOSIZE);
    crc16a = CRC16((UINT8 *)&veeAnalog, ANALOGSIZE);
    crc16i = CRC16((UINT8 *)&dato_ee_iride, IRIDESIZE);

    if (erase == FALSE)
    {
        veeCheckInfo.crc16_main = crc16m;
        veeCheckInfo.crc16_analog = crc16a;
        veeCheckInfo.crc16_iride = crc16i;
    }
    else
    {
        veeCheckInfo.crc16_main = crc16m - 1;   // cosi' sono sicuramente diversi
        veeCheckInfo.crc16_analog = crc16a - 1;
        veeCheckInfo.crc16_iride = crc16i - 1;
    }

//TODO:    flash_err =g_flash0.p_api->erase(g_flash0.p_ctrl, FLASH_BLOCK_VIRT_EEPROM_CHECK, 2);
    if(flash_err != SSP_SUCCESS)
    {
        g_InternalFlag.bit.bEepromError = true;
        return;
    }

    temp32 = CHECKINFOSIZE / FLASH_MIN_PGM_SIZE_DF + 1;
    temp32 *= FLASH_MIN_PGM_SIZE_DF;
//TODO:    flash_err =g_flash0.p_api->write(g_flash0.p_ctrl, &veeCheckInfo, FLASH_BLOCK_VIRT_EEPROM_CHECK, CHECKINFOSIZE);
    if(flash_err == SSP_SUCCESS)
    {
        g_InternalFlag.bit.bEepromInitialized = true;
    }
    else
    {
        g_InternalFlag.bit.bEepromError = true;
    }
}









//***********************************************************
//  calcola il valore del crc16 su un insieme di dati
//***********************************************************
static uint16_t CRC16 (uint8_t *nData, uint16_t wLength)
{
    static const uint16_t wCRCTable[] = {
        0x0000, 0x1189, 0x2312, 0x329b, 0x4624, 0x57ad, 0x6536, 0x74bf,
        0x8c48, 0x9dc1, 0xaf5a, 0xbed3, 0xca6c, 0xdbe5, 0xe97e, 0xf8f7,
        0x1081, 0x0108, 0x3393, 0x221a, 0x56a5, 0x472c, 0x75b7, 0x643e,
        0x9cc9, 0x8d40, 0xbfdb, 0xae52, 0xdaed, 0xcb64, 0xf9ff, 0xe876,
        0x2102, 0x308b, 0x0210, 0x1399, 0x6726, 0x76af, 0x4434, 0x55bd,
        0xad4a, 0xbcc3, 0x8e58, 0x9fd1, 0xeb6e, 0xfae7, 0xc87c, 0xd9f5,
        0x3183, 0x200a, 0x1291, 0x0318, 0x77a7, 0x662e, 0x54b5, 0x453c,
        0xbdcb, 0xac42, 0x9ed9, 0x8f50, 0xfbef, 0xea66, 0xd8fd, 0xc974,
        0x4204, 0x538d, 0x6116, 0x709f, 0x0420, 0x15a9, 0x2732, 0x36bb,
        0xce4c, 0xdfc5, 0xed5e, 0xfcd7, 0x8868, 0x99e1, 0xab7a, 0xbaf3,
        0x5285, 0x430c, 0x7197, 0x601e, 0x14a1, 0x0528, 0x37b3, 0x263a,
        0xdecd, 0xcf44, 0xfddf, 0xec56, 0x98e9, 0x8960, 0xbbfb, 0xaa72,
        0x6306, 0x728f, 0x4014, 0x519d, 0x2522, 0x34ab, 0x0630, 0x17b9,
        0xef4e, 0xfec7, 0xcc5c, 0xddd5, 0xa96a, 0xb8e3, 0x8a78, 0x9bf1,
        0x7387, 0x620e, 0x5095, 0x411c, 0x35a3, 0x242a, 0x16b1, 0x0738,
        0xffcf, 0xee46, 0xdcdd, 0xcd54, 0xb9eb, 0xa862, 0x9af9, 0x8b70,
        0x8408, 0x9581, 0xa71a, 0xb693, 0xc22c, 0xd3a5, 0xe13e, 0xf0b7,
        0x0840, 0x19c9, 0x2b52, 0x3adb, 0x4e64, 0x5fed, 0x6d76, 0x7cff,
        0x9489, 0x8500, 0xb79b, 0xa612, 0xd2ad, 0xc324, 0xf1bf, 0xe036,
        0x18c1, 0x0948, 0x3bd3, 0x2a5a, 0x5ee5, 0x4f6c, 0x7df7, 0x6c7e,
        0xa50a, 0xb483, 0x8618, 0x9791, 0xe32e, 0xf2a7, 0xc03c, 0xd1b5,
        0x2942, 0x38cb, 0x0a50, 0x1bd9, 0x6f66, 0x7eef, 0x4c74, 0x5dfd,
        0xb58b, 0xa402, 0x9699, 0x8710, 0xf3af, 0xe226, 0xd0bd, 0xc134,
        0x39c3, 0x284a, 0x1ad1, 0x0b58, 0x7fe7, 0x6e6e, 0x5cf5, 0x4d7c,
        0xc60c, 0xd785, 0xe51e, 0xf497, 0x8028, 0x91a1, 0xa33a, 0xb2b3,
        0x4a44, 0x5bcd, 0x6956, 0x78df, 0x0c60, 0x1de9, 0x2f72, 0x3efb,
        0xd68d, 0xc704, 0xf59f, 0xe416, 0x90a9, 0x8120, 0xb3bb, 0xa232,
        0x5ac5, 0x4b4c, 0x79d7, 0x685e, 0x1ce1, 0x0d68, 0x3ff3, 0x2e7a,
        0xe70e, 0xf687, 0xc41c, 0xd595, 0xa12a, 0xb0a3, 0x8238, 0x93b1,
        0x6b46, 0x7acf, 0x4854, 0x59dd, 0x2d62, 0x3ceb, 0x0e70, 0x1ff9,
        0xf78f, 0xe606, 0xd49d, 0xc514, 0xb1ab, 0xa022, 0x92b9, 0x8330,
        0x7bc7, 0x6a4e, 0x58d5, 0x495c, 0x3de3, 0x2c6a, 0x1ef1, 0x0f78
     };

    uint8_t nTemp, val;
    uint16_t wCRCWord = 0xFFFF;
    uint32_t blockAddrStart = 0;
    uint32_t blockAddrEnd = 0;

    while (wLength--)
    {
        val = *nData;
        nData++;
        nTemp = (val ^ wCRCWord) & 0x00FF;
        wCRCWord >>= 8;
        wCRCWord ^= wCRCTable[nTemp];
    }

    return wCRCWord;
}




#include "custom.c"




