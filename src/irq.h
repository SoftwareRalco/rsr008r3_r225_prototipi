/*
 * irq.h
 *
 *  Created on: 25 mag 2017
 *      Author: Giovanni Corvini
 */

#include "r_timer_api.h"

// Definizione delle posizioni per antirimbalzi e stato
enum {
    PHOTO_A = 0,    // non cambiare posizione di Home (deve seguire STEPPER_A, B, C, D)
    PHOTO_B,
    PHOTO_C,
    PHOTO_D,
    IN_PROXY,
    TASTO_LUCE,     // 10
    TASTO_FILTRO,
    IN_KEY,
    IN_SWITCH_ROT,
    NUM_DIG
};

#ifdef IRQ_H
#define irq_ext
UINT8 RIMBALZI_DIG[NUM_DIG] = {      // antirimbalzi per digitali ( n * 1ms)
    3,      // PHOTO_A
    3,      // PHOTO_B
    2,      // PHOTO_C
    3,      // PHOTO_D
    40,     // IN_PROXY
    40,     // TASTO_LUCE
    40,     // TASTO_FILTRO
    40,     // IN_KEY
    40      // IN_SWITCH_ROT
};


#else
#define irq_ext extern
UINT8 RIMBALZI_DIG[NUM_DIG];
#endif


// ***************************
//    DEFINIZIONI
// ***************************



//*****************
//**  VARIABILI  **
//*****************
irq_ext union
    {
        UINT8 all;
        struct bitt8_t _bit;
    } volatile flag_irq0;
#define _blink_veloce           flag_irq0._bit.bit0
#define _blink_medio            flag_irq0._bit.bit1
#define _blink_lento            flag_irq0._bit.bit2

#define NUMERO_LED      8
#define S_LED_HOME_A  0   // posizione nell'array (i led di Home seguono i buffer digitali)
#define S_LED_HOME_B  1
#define S_LED_HOME_C  2
#define S_LED_HOME_D  3
#define S_LED_LIFE    4
#define S_LED_RX_CAN  5
#define S_LED_ERROR   6
#define S_LED_CFG     7
irq_ext volatile UINT8 Stato_Led[NUMERO_LED];
irq_ext volatile UINT16 timer_blinking[NUMERO_LED];      // in 1/100 secondo per lampeggio/accensione LED
irq_ext volatile UINT8 timer_periodo_led;




//*****************************
//**  PROTOTIPI DI FUNZIONE  **
//*****************************

void read_hw_manopole(void);
void Calcola_Encoder(UINT8 stepper, UINT8 value);




