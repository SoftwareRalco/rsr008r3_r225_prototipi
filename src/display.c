#define _SYS_DISPLAY_

//--------------------------------------------------------------
// PRIMA STESURA
// Riva ing. Franco M.
// Besana Brianza (MI)
//
// REVISIONI SUCCESSIVE
// Giovanni Corvini
// Melegnano (MI)
//--------------------------------------------------------------
//
// PROJECT:	  RSR008 - Low Cost Collimator
// FILE:      sys_lcd_driver.c
// REVISIONS: 0.00 - 22/09/2008 - First Definition
//
//--------------------------------------------------------------

//--------------------------------------------------------------
// INCLUDE FILES
//--------------------------------------------------------------
#include "include.h"
/*
#include "sfr32C87.h"
#include "sys_defines.h"

#include "sys_lcd_driver.h"
#include "sys_calibration.h"
#include "sys_can_main.h"
#include "sys_can_under_test.h"
#include "sys_display.h"
#include "virtEE_Variable.h"    // Virtual EEPROM (variable size records) function definitions
#include "sys_high_level.h"
#include "sys_main.h"
*/
#include "string.h"

// GLOBAL VARIABLES
//--------------------------------------------------------------

#define POS_DISPLAY_FILTRO          0xCD
#define NUM_CHAR_DISPLAY_FILTRO     7


static bool g_bBlinkTime=true;
static  uchar cbMessageToDisplay[MAX_MSG_SIZE];
static  uchar cbDisplayMsgQueue[MAX_QUEUE_SIZE][MAX_MSG_SIZE];
static  uchar g_ucActualMsgIndex=0;
const char cbFilterA[5][8]={"0Al    ", "2Al    ", "1Al.2Cu", "1Al.1Cu", "FltErr "};
const char cbFilterB[5][8]={"0Al    ", "1Al    ", "1Al.2Cu", "1Al.1Cu", "FltErr "};
const char cbFilterC[5][8]={"0 Cu   ", "0.1 Cu ", "0.2 Cu ", "0.3 Cu ", "FltErr "};
const char cbFilterX[5][8]={"*******", "*******", "*******", "*******", "*******"};

//--------------------------------------------------------------
uint16_t inches_to_cm(uint16_t n){
    ulong z;
    uint16_t r;
    uint16_t q;

    z = 254 * (ulong) n;
    q = z % 100;
    z = z / 100;
    r = z;
    if (q>=50)
        r++;
    return (r);
}

//--------------------------------------------------------------
uint16_t inches_to_mm(uint16_t n){
    ulong z;
    uint16_t r;
    uint16_t q;

    z = 254 * (ulong) n;
    q = z % 10;
    z = z / 10;
    r = z;
    if (q>=5)
        r++;
    return (r);
}

//--------------------------------------------------------------
uint16_t mm_to_inches (uint16_t n){
    ulong z;
    uint16_t r;
    uint16_t q;

    z = 100 * (ulong) n;
    q = z % 254;
    z = z / 254;
    r = z;
    if (q>127)
        r++;
    return (r);
}

//--------------------------------------------------------------
uint16_t cm_to_inches (uint16_t n){
    ulong z;
    uint16_t r;
    uint16_t q;

    z = 1000 * (ulong) n;
    q = z % 254;
    z = z / 254;
    r = z;
    if (q>127)
        r++;
    return (r);
}

//--------------------------------------------------------------
uint16_t dff_to_inches (uint16_t n){
    ulong z;
    uint16_t r;
    uint16_t q;

    z = 1000 * (ulong) n;
    q = z % 254;
    z = z / 254;
    r = z;
    if (q>127)
        r++;
    return (r);
}

//--------------------------------------------------------------
uint16_t val_abs (int16_t u1, int16_t u2){
    if (u1>=u2)
        return ((uint16_t)(u1-u2));
    else
        return ((uint16_t)(u2-u1));
}

//--------------------------------------------------------------
void sys_display_init (void){

    g_ucActualDisplayType=ee_ConfigColl.ucDisplayType;
    g_ucReqDisplayOperation=NO_OP;
    memset(cbNewDisplayString,0,8);
}

//--------------------------------------------------------------
void sys_display_update_angle(bool bShow){

    if (g_ucActualDisplayType==DISPLAY_NONE)
        return;
    if (g_ucOrienteer != RX_TOMO)
    {
        if (bShow){
            if (g_nDegrees<=180){
                if (g_nDegrees<100){
                    sys_lcd_mode(0xC1);
                    if (g_nYDegrees<0)
                        sys_lcd_out (' ');
                    else
                        sys_lcd_out ('-');
                    sys_print_integer(0xC2, g_nDegrees, 2, false);
                    sys_lcd_mode(0xC4);
                    sys_lcd_out(3);
                }
                else{
                    sys_lcd_mode(0xC1);
                    if (g_nYDegrees<0)
                        sys_lcd_out (' ');
                    else
                        sys_lcd_out ('-');
                    sys_print_integer(0xC2, g_nDegrees, 3, false);
                }

                if (!g_nDegrees){
                    sys_lcd_mode(0xC1);
                    sys_lcd_out (' ');
                }
            }
            return;
        }
    }

    sys_lcd_mode(0xC1);
    sys_lcd_out (' ');
    sys_lcd_out (' ');
    sys_lcd_out (' ');
    sys_lcd_out (' ');
}

//--------------------------------------------------------------
void sys_display_version(uchar ucStep)
{
    UINT8 buffer[30];
    UINT8 i;


    if (g_ucActualDisplayType==DISPLAY_NONE)
        return;

    switch (ucStep){
        case 0:
            sys_print_msg (0x80," R225ACS Collimator ");
            sys_print_msg (0xC0,"Serial number ..... ");
            sys_lcd_mode (0xCE);
            sys_lcd_out (ee_RSR008B_Version.cbSerial[3]);
            sys_lcd_out (ee_RSR008B_Version.cbSerial[4]);
            sys_lcd_out (ee_RSR008B_Version.cbSerial[5]);
            sys_lcd_out (ee_RSR008B_Version.cbSerial[6]);
            sys_lcd_out (ee_RSR008B_Version.cbSerial[7]);
            break;
        case 1:
            sys_print_msg (0x80,"       ASR001       ");
            sys_print_msg (0xC0,"Hw ....      Sw ....");
            sys_lcd_mode (0xC3);
            sys_lcd_out (g_cbAsr001Version[2]);
            sys_lcd_out ('.');
            sys_lcd_out (g_cbAsr001Version[3]);
            if (g_cbAsr001Version[3]=='*')
                sys_lcd_out ('*');
            else
                sys_lcd_out ('0');
            sys_lcd_mode (0xD0);
            sys_lcd_out (g_cbAsr001Version[5]);
            sys_lcd_out ('.');
            sys_lcd_out (g_cbAsr001Version[6]);
            sys_lcd_out (g_cbAsr001Version[7]);
            break;
        case 2:
            if (ee_ConfigColl.ucLcdFree == LCD_PROTO)       // qui il nome sul display segue il nome del protocollo
            {
                switch (ee_ConfigColl.ucCanProtocol)
                {
                case CAN_STANDARD:
                    sys_print_msg (0x80," R225ACS - STANDARD ");
                    break;
                case CAN_SEDECAL:
                    sys_print_msg (0x80," R225ACS - SEDECAL  ");
                    break;
                case CAN_GMM:
                    sys_print_msg (0x80,"   R225ACS - GMM    ");
                    break;
                default:
                    sys_print_msg (0x80,"      R225ACS       ");
                    break;
                }
            }
            else                // qui copia LcdName memorizzato
            {
                sprintf(buffer, " R225ACS - %s       ", ee_ConfigColl.ucLcdName);
                sys_print_msg (0x80, buffer);
            }

#ifndef SYS_CANOPEN
#ifdef _LOADER_
            sys_print_msg (0xC0,"      Sw          BL");
#else
            sys_print_msg (0xC0,"      Sw            ");
#endif
            if ((R_release[0] != ' ') ||        // controlla se deve visualizzare versione di test
                (R_release[1] != ' ') ||
                (R_release[2] != ' ') ||
                (R_release[3] != ' ') ||
                (R_release[4] != ' '))
            {
                sys_lcd_mode (0xC4);
                sys_lcd_out ('S');
                sys_lcd_out ('w');
                sys_lcd_out (' ');
                sys_lcd_out (ee_RSR008B_Version.cbVersion[5]);
                sys_lcd_out ('.');
                sys_lcd_out (ee_RSR008B_Version.cbVersion[6]);
                sys_lcd_out (ee_RSR008B_Version.cbVersion[7]);
                sys_lcd_out(' ');
                sys_lcd_out(R_release[0]);
                sys_lcd_out(R_release[1]);
                sys_lcd_out(R_release[2]);
                sys_lcd_out(R_release[3]);
                sys_lcd_out(R_release[4]);
            }
            else
            {
                sys_lcd_mode (0xC6);
                sys_lcd_out ('S');
                sys_lcd_out ('w');
                sys_lcd_out (' ');
                sys_lcd_out (ee_RSR008B_Version.cbVersion[5]);
                sys_lcd_out ('.');
                sys_lcd_out (ee_RSR008B_Version.cbVersion[6]);
                sys_lcd_out (ee_RSR008B_Version.cbVersion[7]);
            }
#else
            sys_print_msg (0xC0,"                    ");
            sys_lcd_mode (0xC1);
            for (i = 0; i < 17; i++)
            {
                sys_lcd_out(CO_OD_ROM.manufacturerSoftwareVersion[i]);
            }
#endif
            break;
        case 3:
            sys_print_msg (0x80,"       ASR003       ");
            sys_print_msg (0xC0,"Hw ....      Sw ....");
            sys_lcd_mode (0xC3);
            sys_lcd_out (g_cbAsr003Version[2]);
            sys_lcd_out ('.');
            sys_lcd_out (g_cbAsr003Version[3]);
            if (g_cbAsr003Version[3]=='*')
                sys_lcd_out ('*');
            else
                sys_lcd_out ('0');
            sys_lcd_mode (0xD0);
            sys_lcd_out (g_cbAsr003Version[5]);
            sys_lcd_out ('.');
            sys_lcd_out (g_cbAsr003Version[6]);
            sys_lcd_out (g_cbAsr003Version[7]);
            break;
        case 4:
            sys_print_msg (0x80,"       IRIS         ");
            sys_print_msg (0xC0,"Hw ....      Sw ... ");
            sys_lcd_mode (0xC3);
            sys_lcd_out (g_cbIrisVersion[0]);
            sys_lcd_out (g_cbIrisVersion[1]);
            sys_lcd_out (g_cbIrisVersion[2]);
            sys_lcd_out (g_cbIrisVersion[3]);
            sys_lcd_out (g_cbIrisVersion[4]);
            sys_lcd_mode (0xD0);
            sys_lcd_out (g_cbIrisVersion[5]);
            sys_lcd_out (g_cbIrisVersion[6]);
            sys_lcd_out (g_cbIrisVersion[7]);
            break;
        case 5:
            sys_print_msg (0x80,"     ROTATION       ");
            sys_print_msg (0xC0,"Hw ....      Sw ... ");
            sys_lcd_mode (0xC3);
            sys_lcd_out (g_cbRotVersion[0]);
            sys_lcd_out (g_cbRotVersion[1]);
            sys_lcd_out (g_cbRotVersion[2]);
            sys_lcd_out (g_cbRotVersion[3]);
            sys_lcd_out (g_cbRotVersion[4]);
            sys_lcd_mode (0xD0);
            sys_lcd_out (g_cbRotVersion[5]);
            sys_lcd_out (g_cbRotVersion[6]);
            sys_lcd_out (g_cbRotVersion[7]);
            break;
    }
}

//--------------------------------------------------------------


//--------------------------------------------------------------

void sys_display_filter(UINT8 error)
{
    UINT8 flag;

    if (g_ucActualDisplayType == DISPLAY_NONE)
        return;
    if (!ee_ConfigColl.ucFilter)
        return;
    if (g_ucWorkModality != COLLIMATORWORK) // se non e' in modalita' normale allora ritorna
        return;

    FiltroInErrore = error;
    VisualizzaFiltro = TRUE;

    if (error)
    {
        flag = FALSE;
        if (g_bTimeToShowFilterError == TRUE)
        {
            g_bTimeToShowFilterError = FALSE;
            g_bBlinkFilterTime = ~g_bBlinkFilterTime; // inverte lampeggio

            if (g_bBlinkFilterTime == 0)
            {
                sys_print_msg(POS_DISPLAY_FILTRO, "       ");
            }
            else
            {
                flag = TRUE;
            }
        }
    }
    else
    {
        flag = TRUE;
    }

    if (flag == TRUE)
    {
        sys_print_msg(POS_DISPLAY_FILTRO, ricava_pt_nome_filtro(g_ucActualFilterType));
    }
}




//**********************************************
//  scorrimento del nome del display
//**********************************************
#define TEMPO_FC_INIZIO     150     // 10 ms
#define TEMPO_FC_CAMBIO      80     // 10 ms
#define TEMPO_FC_BLANK       60     // 10 ms

void scorrimento_filtro(void)
{
    static UINT8 nomeAttuale = 0xFA;
    static UINT8 scorri = FALSE;
    static UINT8 dir = 0;
    static UINT8 pt_inizio = 0;
    static const char *ptStringa;
    static UINT8 lunghezzaNome, diffLunghezza;
    UINT8 flag;

    if (VisualizzaFiltro == FALSE)
        return;

    if (FiltroInErrore)
        return;

    if (g_ucActualFilterType != nomeAttuale)
    {
        nomeAttuale = g_ucActualFilterType;
        pt_inizio = 0;
        timer_scorrimento = TEMPO_FC_INIZIO;
        ptStringa = ricava_pt_nome_filtro(g_ucActualFilterType);

        lunghezzaNome = strlen(ptStringa);
        diffLunghezza = 0;
        scorri = FALSE;
        dir = 0;
        if (lunghezzaNome > NUM_CHAR_DISPLAY_FILTRO)
        {
            diffLunghezza = lunghezzaNome - NUM_CHAR_DISPLAY_FILTRO;
            scorri = TRUE;
        }
    }

    if (scorri)
    {
        flag = TRUE;

        if (timer_scorrimento == 0)
        {
            if (dir == 0)
            {
                if (++pt_inizio >= diffLunghezza)
                {
                    timer_scorrimento = TEMPO_FC_INIZIO;
                    dir = 1;
                }
                else
                {
                    timer_scorrimento = TEMPO_FC_CAMBIO;
                }
            }
            else if (dir == 1)
            {
                dir = 2;
                sys_print_msg(POS_DISPLAY_FILTRO, "       ");
                timer_scorrimento = TEMPO_FC_BLANK;
                flag = FALSE;
            }
            else if (dir == 2)
            {
                pt_inizio = 0;
                timer_scorrimento = TEMPO_FC_INIZIO;
                dir = 0;
            }
/*
            {
                if (--pt_inizio == 0)
                {
                    timer_scorrimento = 100;
                    dir = 0;
                }
                else
                {
                    timer_scorrimento = 50;
                }
            }

*/
            if (flag)
            {
                sys_print_msg(POS_DISPLAY_FILTRO, ptStringa + pt_inizio);
            }
        }

    }



}



//*****************************************************
//  puntatore alla stringa
//*****************************************************
const char* ricava_pt_nome_filtro(UINT8 filtro)
{
    const char *punta;

    switch (ee_ConfigColl.ucFilterType)
    {
    case MM2_FILTER:
        punta = cbFilterA[filtro];
        break;
    case MM1_FILTER:
        punta = cbFilterB[filtro];
        break;
    case CU_FILTER:
        punta = cbFilterC[filtro];
        break;
    case CUSTOM_FILTER:
        punta = &ee_ConfigColl.NomiFiltroCustom[filtro][0];
        break;
    default:
        punta = cbFilterX[filtro];
        break;
    }

    return punta;
}



//--------------------------------------------------------------
void sys_display_clear_screen(void){
    //First Row:  0x80 ... 0x93
    //Second Row: 0xC0 ... 0xD3

    if (g_ucActualDisplayType==DISPLAY_NONE)
        return;
    sys_print_msg (0x80,"                    ");
    sys_print_msg (0xC0,"                    ");
}

//--------------------------------------------------------------
void sys_display_main_screen(void){
    //First Row:  0x80 ... 0x93
    //Second Row: 0xC0 ... 0xD3

    if (g_ucActualDisplayType==DISPLAY_NONE)
        return;

    switch (g_ucActualDisplayType)
    {
    case DISPLAY_STANDARD:
        if (((ee_ConfigColl.ucIrisPresent == IRIDE_NO) ||
            (ee_ConfigColl.ucIrisEnabled == FALSE)) &&
                (ee_ConfigColl.ucMeterEnabled == FALSE))
        {
            sys_print_msg (0x80,"CROSS      LONG     ");
        }
        else
        {
            if (ee_ConfigColl.ucMeterEnabled == FALSE)
            {
                sys_print_msg (0x80,"C:     L:     I:    ");
            }
            else
            {
                sys_print_msg (0x80,"C:     L:    DFT:   ");
            }
        }
        sys_print_msg (0xC0,"                    ");
        sys_display_filter(FALSE);
        break;
    case DISPLAY_SEDECAL:
        sys_display_clear_screen();
        sys_display_filter(FALSE);  //0.42
        break;
    case DISPLAY_FREE:
        sys_display_clear_screen();
        break;
    }
}

//--------------------------------------------------------------
void sys_display_update_screen(void){

    uchar ucRest;
    uchar ucRc;
    uchar temp8;
    static uchar g_ucDisplaystate=DISPLAY_START;
    static uchar s_ucLastDisplayType=DISPLAY_NONE;
    static UINT8 delay;

    if (g_bUserCalInProgress)
        return;
    if (g_bCalInProgress)
        return;
    if (g_bSerCalInProgress)
        return;
    if (g_ucActualDisplayType==DISPLAY_NONE)
        return;

    switch (g_ucDisplaystate){

        case DISPLAY_START:
            sys_display_version(0);
            delay = 4;
            g_ucDisplaystate = DISPLAY_SHOW_VERSION_ASR001;
            break;

        case DISPLAY_SHOW_VERSION_ASR001:
            if (g_uwTimeSecondCounter < delay)
                break;
            if (g_bAsr001Ok)
            {
                sys_display_version(1);
                delay += 2;
            }
            g_ucDisplaystate = DISPLAY_SHOW_VERSION_RSR008;
            break;

        case DISPLAY_SHOW_VERSION_RSR008:
            if (g_uwTimeSecondCounter < delay)
                break;
            sys_display_version(2);
            delay += 4;

            if(ee_ConfigColl.ucIrisPresent == IRIDE_YES)     // richiesta versioni firmware
            {
                tx_temp.dlc = 8;
                tx_temp.id = ee_CanColl.unIdIride + MSG_IRIS_VER;
                copy_tx_temp(TRUE);
            }
            if(ee_ConfigColl.ucRotation == ROTAZIONE_YES)
            {
//TODO:                set_rec_std_dataframe_can0(SLOT_ID_ROTAZIONE, ee_CanColl.unIdRot, TRUE);      // ricezione ROTAZIONE
                tx_temp.dlc = 8;
                tx_temp.id = ee_CanColl.unIdRot + 0x00;
                copy_tx_temp(TRUE);
            }
            g_ucDisplaystate = DISPLAY_SHOW_VERSION_IRIDE;
            break;

        case DISPLAY_SHOW_VERSION_IRIDE:
            if (g_uwTimeSecondCounter < delay)
                break;
            if (g_bIridek)
            {
                sys_display_version(4);
                delay += 2;
            }
            g_ucDisplaystate = DISPLAY_SHOW_VERSION_ROT;
            break;

        case DISPLAY_SHOW_VERSION_ROT:
            if (g_uwTimeSecondCounter < delay)
                break;
            if (g_bRotk)
            {
                sys_display_version(5);
                delay += 2;
            }
            if(ee_ConfigColl.ucRotation == ROTAZIONE_YES)   // disabilita
            {
//TODO:                set_rec_std_dataframe_can0(SLOT_ID_ROTAZIONE, 0, FALSE);
            }
            g_ucDisplaystate = DISPLAY_SHOW_VERSION_ASR003;
            break;

        case DISPLAY_SHOW_VERSION_ASR003:
            if (g_uwTimeSecondCounter < delay)
                break;
            if (g_bAsr003Ok)
            {
                sys_display_version(3);
                delay += 2;
            }
            g_ucDisplaystate = DISPLAY_SHOW_MAIN;
            break;

        case DISPLAY_SHOW_MAIN:
            if (g_uwTimeSecondCounter < delay)
                break;
            g_bInitEnd=true;
            sys_display_main_screen();
            s_ucLastDisplayType=g_ucActualDisplayType;
            g_ucDisplaystate = DISPLAY_RUNNING;
            break;

        case DISPLAY_RUNNING:
            sys_control_update_data();
            ucRc = 0;
            if (g_uwActualDff)
                ucRc=LinCrossMm(g_uwActualDff, (uword)g_lCrossActualSteps, &g_uwActualCrossToBeSent);
            else
                g_uwActualCrossToBeSent=0;
            if (ucRc){
                g_ucActualCross = g_uwActualCrossToBeSent/10;
                ucRest = g_uwActualCrossToBeSent%10;
                if (ucRest>=5)
                    g_ucActualCross++;
            }
            else {
                g_ucActualCross=0;
                g_uwActualCrossToBeSent=0;
            }

            ucRc = 0;
            if (g_uwActualDff)
                ucRc=LinLongMm(g_uwActualDff, (uword)g_lLongActualSteps, &g_uwActualLongToBeSent);
            else
                g_uwActualLongToBeSent=0;
            if (ucRc){
                g_ucActualLong = g_uwActualLongToBeSent/10;
                ucRest = g_uwActualLongToBeSent%10;
                if (ucRest>=5)
                    g_ucActualLong++;
            } else {
                g_ucActualLong=0;
                g_uwActualLongToBeSent=0;
            }

            ucRc = 0;
            if(ee_ConfigColl.ucIrisPresent == IRIDE_YES)
            {
                if (g_uwActualDff)
                    ucRc=LinIrisMm(g_uwActualDff, (uword)g_lIrisActualSteps, &g_uwActualIrisToBeSent, FALSE);
                else
                    g_uwActualIrisToBeSent=0;
            }
            if (ucRc){
                g_ucActualIris = g_uwActualIrisToBeSent/10;
                ucRest = g_uwActualIrisToBeSent%10;
                if (ucRest>=5)
                    g_ucActualIris++;
            } else {
                g_ucActualIris=0;
                g_uwActualIrisToBeSent=0;
            }

            if (visua_main >= 1)
            {
                visua_main = 0;

                switch (s_ucLastDisplayType){
                    case DISPLAY_STANDARD:
                        if (((ee_ConfigColl.ucIrisPresent == IRIDE_NO) ||
                            (ee_ConfigColl.ucIrisEnabled == FALSE)) &&
                                (ee_ConfigColl.ucMeterEnabled == FALSE))
                        {
                            if (ee_ConfigColl.ucCmUnit == CM_UNIT){
                                sys_print_byte(0x86, g_ucActualCross, false);
                                sys_print_byte(0x90, g_ucActualLong, false);
                            } else {
                                sys_print_number(0x86, mm_to_inches(g_uwActualCrossToBeSent), 3, 2,false);
                                sys_print_number(0x90, mm_to_inches(g_uwActualLongToBeSent), 3, 2,false);
                            }
                        }
                        else
                        {
                            if (ee_ConfigColl.ucMeterEnabled == FALSE)
                            {
                                if (ee_ConfigColl.ucCmUnit == CM_UNIT){
                                    sys_print_byte(0x82, g_ucActualCross, false);
                                    sys_print_byte(0x89, g_ucActualLong, false);
                                    if ((fluoroscopia_on == TRUE) || (IrideIndipendente == TRUE) || (ee_ConfigColl.ucVisuaApeIris))
                                        sys_print_byte(0x90, g_ucActualIris, false);
                                    else
                                        sys_print_msg (0x90," --");
                                    sys_lcd_out (' ');
                                } else {
                                    sys_print_number(0x82, mm_to_inches(g_uwActualCrossToBeSent), 3, 2,false);
                                    sys_print_number(0x89, mm_to_inches(g_uwActualLongToBeSent), 3, 2,false);
                                    if ((fluoroscopia_on == TRUE) || (IrideIndipendente == TRUE) || (ee_ConfigColl.ucVisuaApeIris))
                                        sys_print_number(0x90, mm_to_inches(g_uwActualIrisToBeSent), 3, 2,false);
                                    else
                                        sys_print_msg (0x90," --");
                                    sys_lcd_out (' ');
                                }
                            }
                            else
                            {
                                if (ee_ConfigColl.ucCmUnit == CM_UNIT){
                                    sys_print_byte(0x82, g_ucActualCross, false);
                                    sys_print_byte(0x89, g_ucActualLong, false);
                                    sys_print_number(0x91, misura_metro, 3, 0xFF, false);
                                    sys_lcd_out (' ');
                                } else {
                                    sys_print_number(0x82, mm_to_inches(g_uwActualCrossToBeSent), 3, 2,false);
                                    sys_print_number(0x89, mm_to_inches(g_uwActualLongToBeSent), 3, 2,false);
                                    sys_print_number(0x91, mm_to_inches(misura_metro), 3, 2, false);
                                }
                            }
                        }
/*
                        if ((ee_ConfigColl.ucIrisPresent) &&
                            (ee_ConfigColl.ucIrisEnabled))
                        {
                            if (ee_ConfigColl.ucCmUnit == CM_UNIT){
                                sys_print_byte(0x82, g_ucActualCross, false);
                                sys_print_byte(0x89, g_ucActualLong, false);
                                sys_print_byte(0x90, g_ucActualIris, false);
                                sys_lcd_out (' ');
                            } else {
                                sys_print_number(0x82, mm_to_inches(g_uwActualCrossToBeSent), 3, 2,false);
                                sys_print_number(0x89, mm_to_inches(g_uwActualLongToBeSent), 3, 2,false);
                                sys_print_number(0x90, mm_to_inches(g_uwActualIrisToBeSent), 3, 2,false);
                            }
                        } else {
                            if (ee_ConfigColl.ucCmUnit == CM_UNIT){
                                sys_print_byte(0x86, g_ucActualCross, false);
                                sys_print_byte(0x90, g_ucActualLong, false);
                            } else {
                                sys_print_number(0x86, mm_to_inches(g_uwActualCrossToBeSent), 3, 2,false);
                                sys_print_number(0x90, mm_to_inches(g_uwActualLongToBeSent), 3, 2,false);
                            }
                        }

*/
                        if (ee_ConfigColl.ucShowDff)
                        {
                            if (DisplayFTD == FALSE)
                            {
                                if (ee_ConfigColl.ucLanguage == ITA_LANG)
                                {
                                    sys_print_msg (0xC5,"DFF");
                                }
                                else
                                {
                                    sys_print_msg (0xC5,"SID");
                                }
                            }
                            else
                            {
                                sys_print_msg (0xC5,"FTD");
                            }

                            if (ee_ConfigColl.ucCmUnit == CM_UNIT)
                            {
                                sys_print_integer(0xC9, DisplayDFF, 3, false);
//                                sys_print_byte (0xC9,sys_display_dff_to_screen(),false);
                                sys_lcd_out (' ');
                            }
                            else {
                                sys_print_number (0xC8,dff_to_inches(DisplayDFF),3,2,false);
                            }
                        }
                        else
                        {
                            sys_print_msg (0xC5,"        ");
                        }

                        sys_lcd_mode(0xC0);
                        if (ee_ConfigColl.ucShowLock){
                            if (KEY_AUTO)
                                sys_lcd_out(1);
                            else
                                sys_lcd_out(2);
                        } else {
                            sys_lcd_out (' ');
                        }

                        if (ee_ConfigColl.displayAngoloTubo)        // controlla se deve visualizzare angolo colonna
                        {
                            if (angolo_colonna != angolo_colonna_hold)            // visualizzazione ANGOLO COLONNA
                            {
                                angolo_colonna_hold = angolo_colonna;
                                sys_lcd_mode(0xC1);
                                if (angolo_colonna >= 0)
                                {
                                    sys_lcd_out (' ');
                                    temp8 = angolo_colonna;
                                }
                                else
                                {
                                    sys_lcd_out ('-');
                                    temp8 = -angolo_colonna;
                                }
                                if (temp8 > 99)
                                {
                                    temp8 = 99;     // manca lo spazio su display
                                }
                                sys_print_integer(0xC2, temp8, 2, false);
                                sys_lcd_mode(0xC4);
                                sys_lcd_out(3);
                            }
                        }
                        else
                        {
                            if (ee_ConfigColl.ucShowAngle)
                            {
                                if (!g_bTimeToShowAngle)
                                    break;

                                g_bTimeToShowAngle=false;
                                if (g_ucOrienteer==RX_ERROR){
                                    if (g_bBlinkTime){
                                        g_bBlinkTime=false;
                                        sys_display_update_angle(true);
                                    } else {
                                        g_bBlinkTime=true;
                                        sys_display_update_angle(false);
                                    }
                                } else {
                                    g_bBlinkTime=true;
                                    sys_display_update_angle(true);
                                }
                            }
                            else
                            {
                                g_bBlinkTime=false;
                                sys_display_update_angle(false);
                            }
                        }
                        break;

                    case DISPLAY_SEDECAL:
                        if (ee_ConfigColl.ucCmUnit == CM_UNIT){
                            sys_print_number(0x80, g_uwActualCrossToBeSent, 3, 2, false);
                            sys_print_number(0x90, g_uwActualLongToBeSent, 3, 2, false);
                        } else {
                            sys_print_number(0x80, mm_to_inches(g_uwActualCrossToBeSent), 3, 2,false);
                            sys_print_number(0x90, mm_to_inches(g_uwActualLongToBeSent), 3, 2,false);
                        }

                        if (g_ucManualRequest || !KEY_AUTO)
                            sys_print_msg (0x85," MAN");
                        else
                            sys_print_msg (0x85,"AUTO");

                        if (ee_ConfigColl.ucCmUnit == CM_UNIT)
                        {
                            sys_print_integer(0xC9, DisplayDFF, 3, false);
//                            sys_print_byte (0x8B,sys_display_dff_to_screen(),false);
                            sys_lcd_out (' ');
                        }
                        else {
                            sys_print_number (0x8B,dff_to_inches(DisplayDFF),3,2,false);
                        }
                        break;

                    case DISPLAY_FREE:
                        switch (g_ucReqDisplayOperation){
                            case NO_OP:
                                break;
                            case CLEAR_FIRST_LINE:
                                sys_print_msg (0x80,"                    ");
                                g_ucReqDisplayOperation=NO_OP;
                                break;
                            case CLEAR_SECOND_LINE:
                                sys_print_msg (0xC0,"                    ");
                                g_ucReqDisplayOperation=NO_OP;
                                break;
                            case CLEAR_BOTH_LINES:
                                sys_print_msg (0x80,"                    ");
                                sys_print_msg (0xC0,"                    ");
                                g_ucReqDisplayOperation=NO_OP;
                                break;
                            default:
                                if (g_ucReqDisplayOperation<=END_FIRST_LINE)
                                    sys_print_msg(g_ucReqDisplayOperation+0x80, cbNewDisplayString);
                                else
                                    sys_print_msg(g_ucReqDisplayOperation+0xA0, cbNewDisplayString);
                                g_ucReqDisplayOperation=NO_OP;
                                memset(cbNewDisplayString,0,8);
                                break;
                        }
                        break;
                }
            }

            if (g_ucActualDisplayType!=s_ucLastDisplayType){
                //New display required
                g_ucDisplaystate = DISPLAY_SHOW_MAIN;
                break;
            }
    }

}

//--------------------------------------------------------------
bool sys_display_queue_msg(uchar* pucMessage){

    if (g_ucActualMsgIndex>=MAX_QUEUE_SIZE)
        return false;

    memcpy(cbDisplayMsgQueue[g_ucActualMsgIndex], pucMessage, MAX_MSG_SIZE-1);
    g_ucActualMsgIndex++;
    return true;
}

//--------------------------------------------------------------
bool sys_display_unqueue_msg(uchar* pucMessage){

    uchar ucIndex;

    if (!g_ucActualMsgIndex)
        return false;

    memcpy(pucMessage, cbDisplayMsgQueue[0], MAX_MSG_SIZE-1);

    for (ucIndex=0; ucIndex<g_ucActualMsgIndex; ucIndex++)
        memcpy(cbDisplayMsgQueue[ucIndex], cbDisplayMsgQueue[ucIndex+1], MAX_MSG_SIZE-1);

    g_ucActualMsgIndex--;
    return true;
}


//--------------------------------------------------------------
void sys_display_manager(void){

    uchar ucIndex;

    if (g_ucActualDisplayType==DISPLAY_NONE)
        return;

    switch (s_ucDisplayQueueStatus){

        case DISPLAY_START:
            g_ucActualMsgIndex=0;
            for (ucIndex=0; ucIndex<MAX_QUEUE_SIZE; ucIndex++)
                memset(cbDisplayMsgQueue[ucIndex], 0, MAX_MSG_SIZE);
            memset(cbMessageToDisplay, 0, MAX_MSG_SIZE-1);
            sys_print_msg (0x80,"BOARD CONFIGURATION ");
            switch (ee_ConfigColl.ucCanSpeed){
                case CAN_1000K:
                    sys_print_msg (0xC0,"Can Speed 1MBit/s");
                    break;
                case CAN_500K:
                    sys_print_msg (0xC0,"Can Speed 500kBit/s");
                    break;
                case CAN_250K:
                    sys_print_msg (0xC0,"Can Speed 250kBit/s");
                    break;
                case CAN_125K:
                    sys_print_msg (0xC0,"Can Speed 125kBit/s");
                    break;
                case CAN_100K:
                    sys_print_msg (0xC0,"Can Speed 100kBit/s");
                    break;
                case CAN_50K:
                    sys_print_msg (0xC0,"Can Speed 50kBit/s");
                    break;
                case CAN_20K:
                    sys_print_msg (0xC0,"Can Speed 20kBit/s");
                    break;
                case CAN_10K:
                    sys_print_msg (0xC0,"Can Speed 10kBit/s");
                    break;
                default:
                    sys_print_msg (0xC0,"Can Speed Error");
                    break;
            }

            g_uwDisplayManagerTimer=DISPLAY_RATE_W_MSG;
            s_ucDisplayQueueStatus=DISPLAY_RUNNING;
            break;

        case DISPLAY_RUNNING:
            if (g_uwDisplayManagerTimer)
                return;

            if (g_ucActualMsgIndex){
                sys_display_unqueue_msg(cbMessageToDisplay);
                sys_print_msg (0xC0,"                    ");
                g_uwDisplayManagerTimer=DISPLAY_RATE_W_MSG;
            }
            else {
                memset(cbMessageToDisplay, ' ', MAX_MSG_SIZE-1);
                sys_print_msg (0xC0,"Wait command ....   ");
                g_uwDisplayManagerTimer=DISPLAY_RATE_WO_MSG;
            }

            sys_print_msg (0x80,cbMessageToDisplay);
            break;

        case DISPLAY_HOLDING:
            break;
    }
}
