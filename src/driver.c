/*
 * driver.c
 *
 *  Created on: 25 mag 2017
 *      Author: Giovanni Corvini
 */


#define DRIVER_H

#include "include.h"
#include "main_thread.h"







//**********************************************
//  scrive i valori dac correnti
//**********************************************
void write_dac(void)
{
    ssp_err_t api_err;
    uint8_t flag;

    flag = FALSE;
    if (dac_hold[STEPPER_A] != dac_value[STEPPER_A])
    {
        dac_hold[STEPPER_A] = dac_value[STEPPER_A];
        flag = TRUE;
    }
    if (dac_hold[STEPPER_B] != dac_value[STEPPER_B])
    {
        dac_hold[STEPPER_B] = dac_value[STEPPER_B];
        flag = TRUE;
    }
    if (dac_hold[STEPPER_C] != dac_value[STEPPER_C])
    {
        dac_hold[STEPPER_C] = dac_value[STEPPER_C];
        flag = TRUE;
    }
    if (dac_hold[STEPPER_D] != dac_value[STEPPER_D])
    {
        dac_hold[STEPPER_D] = dac_value[STEPPER_D];
        flag = TRUE;
    }

    if (flag == TRUE)
    {
        i2c_command[0] = 0x00 | (dac_value[STEPPER_A] >> 8);
        i2c_command[1] = (uint8_t)(dac_value[STEPPER_A]);
        i2c_command[2] = 0x00 | (dac_value[STEPPER_B] >> 8);
        i2c_command[3] = (uint8_t)(dac_value[STEPPER_B]);
        i2c_command[4] = 0x00 | (dac_value[STEPPER_C] >> 8);
        i2c_command[5] = (uint8_t)(dac_value[STEPPER_C]);
        i2c_command[6] = 0x00 | (dac_value[STEPPER_D] >> 8);
        i2c_command[7] = (uint8_t)(dac_value[STEPPER_D]);
        api_err = g_sf_i2c_device_dac.p_api->write(g_sf_i2c_device_dac.p_ctrl, &i2c_command[0], 8, FALSE, TX_WAIT_FOREVER);
        if (api_err != SSP_SUCCESS)
        {
            while (1)
            {
                __asm("nop");
            }
        }
    }

}





//**********************************************
//  scrive i valori dac correnti
//**********************************************
void write_io(void)
{
    ssp_err_t api_err;
    uint8_t flag;

    flag = FALSE;
    if (pca8575_port0.all != pca8575_port0_hold.all)
    {
        pca8575_port0_hold.all = pca8575_port0.all;
        flag = TRUE;
    }
    if (pca8575_port1.all != pca8575_port1_hold.all)
    {
        pca8575_port1_hold.all = pca8575_port1.all;
        flag = TRUE;
    }

    if (flag == TRUE)
    {
        i2c_command[0] = pca8575_port0.all;
        i2c_command[1] = pca8575_port1.all;
        api_err = g_sf_i2c_device_io.p_api->write(g_sf_i2c_device_io.p_ctrl, &i2c_command[0], 2, FALSE, TX_WAIT_FOREVER);
        if (api_err != SSP_SUCCESS)
        {
            while (1)
            {
                __asm("nop");
            }
        }
    }
}




//******************************************************************************
//  inizializzazione DAC in i2c MCP4728
//******************************************************************************
void init_dac_i2c(void)
{
    ssp_err_t api_err;

//    api_err = g_sf_i2c_device1.p_api->lock(g_sf_i2c_device1.p_ctrl);
//    if (api_err != SSP_SUCCESS)
//    {
//        while (1)
//        {
//            __asm("nop");
//        }
//    }

    //  setting dei VREF (1=interno a 2,048V)
    i2c_command[0] = 0x8F;
    api_err = g_sf_i2c_device_dac.p_api->write(g_sf_i2c_device_dac.p_ctrl, &i2c_command[0], 1, FALSE, TX_WAIT_FOREVER);
    if (api_err != SSP_SUCCESS)
    {
        while (1)
        {
            __asm("nop");
        }
    }

    //  setting dei POWER DOWN bits
    i2c_command[0] = 0xA0;
    i2c_command[1] = 0x00;
    api_err = g_sf_i2c_device_dac.p_api->write(g_sf_i2c_device_dac.p_ctrl, &i2c_command[0], 2, FALSE, TX_WAIT_FOREVER);
    if (api_err != SSP_SUCCESS)
    {
        while (1)
        {
            __asm("nop");
        }
    }

    //  setting dei GAIN (1=2x)
    i2c_command[0] = 0xCF;
    api_err = g_sf_i2c_device_dac.p_api->write(g_sf_i2c_device_dac.p_ctrl, &i2c_command[0], 1, FALSE, TX_WAIT_FOREVER);
    if (api_err != SSP_SUCCESS)
    {
        while (1)
        {
            __asm("nop");
        }
    }


}



//******************************************************************************
//  inizializzazione I/O Expander in i2c PCA8575
//******************************************************************************
void init_io_i2c(void)
{
    ssp_err_t api_err;

    //  setting I/O port
    i2c_command[0] = 0xFF;
    i2c_command[1] = 0xFF;
    api_err = g_sf_i2c_device_io.p_api->write(g_sf_i2c_device_io.p_ctrl, &i2c_command[0], 2, FALSE, TX_WAIT_FOREVER);
    if (api_err != SSP_SUCCESS)
    {
        while (1)
        {
            __asm("nop");
        }
    }

    pca8575_port0.all = 0xFF;
    pca8575_port1.all = 0xFF;
    pca8575_port0_hold.all = 0xFF;
    pca8575_port1_hold.all = 0xFF;

}





//*******************************************************************************
//  inizializzazione dei timer motore
//*******************************************************************************/
void Init_MotorsTimers(void)
{
    ssp_err_t err;

    err = R_GPT_TimerOpen(g_timer_mot1.p_ctrl, g_timer_mot1.p_cfg);
    if(err != SSP_SUCCESS) while (1);

    err = R_GPT_TimerOpen(g_timer_mot2.p_ctrl, g_timer_mot2.p_cfg);
    if(err != SSP_SUCCESS) while (1);

    err = R_GPT_TimerOpen(g_timer_mot3.p_ctrl, g_timer_mot3.p_cfg);
    if(err != SSP_SUCCESS) while (1);

    err = R_GPT_TimerOpen(g_timer_mot4.p_ctrl, g_timer_mot4.p_cfg);
    if(err != SSP_SUCCESS) while (1);

}




//**************************************
//**  inizializza periferica ADC
//**************************************
void ADC_init(void)
{
    ssp_err_t err;

    //
    err = g_adc0.p_api->open (g_adc0.p_ctrl, g_adc0.p_cfg);
    if(err != SSP_SUCCESS) while (1);

    err = g_adc0.p_api->scanCfg (g_adc0.p_ctrl, g_adc0.p_channel_cfg);
    if(err != SSP_SUCCESS) while (1);

    err = g_adc0.p_api->scanStart (g_adc0.p_ctrl);
    if(err != SSP_SUCCESS) while (1);


}





void Luce_TIMER_START(void)
{
    g_ioport.p_api->pinWrite(PIN_LIGHT_ON, IOPORT_LEVEL_HIGH);
}


void Luce_TIMER_STOP(void)
{
    g_ioport.p_api->pinWrite(PIN_LIGHT_ON, IOPORT_LEVEL_LOW);
}


uint8_t Luce_Read_Pin(void)
{
    ioport_level_t pinlevel;

    g_ioport.p_api->pinRead(PIN_LIGHT_ON, &pinlevel);
    return pinlevel;
}





//******************************************
//  set dei parametri del TB67S109
//******************************************
void set_TB67S109_uStep(uint8_t stepper, uint8_t uStep)
{
    uint8_t level0;
    uint8_t level1;
    uint8_t level2;

    switch (uStep)
    {
    case STEP_0:
        level0 = IOPORT_LEVEL_LOW;
        level1 = IOPORT_LEVEL_LOW;
        level2 = IOPORT_LEVEL_LOW;
        break;
    case STEP_1_1:
        level0 = IOPORT_LEVEL_LOW;
        level1 = IOPORT_LEVEL_LOW;
        level2 = IOPORT_LEVEL_HIGH;
        break;
    case STEP_1_2L:
        level0 = IOPORT_LEVEL_LOW;
        level1 = IOPORT_LEVEL_HIGH;
        level2 = IOPORT_LEVEL_LOW;
        break;
    case STEP_1_2H:
        level0 = IOPORT_LEVEL_HIGH;
        level1 = IOPORT_LEVEL_LOW;
        level2 = IOPORT_LEVEL_LOW;
        break;
    case STEP_1_4:
        level0 = IOPORT_LEVEL_LOW;
        level1 = IOPORT_LEVEL_HIGH;
        level2 = IOPORT_LEVEL_HIGH;
        break;
    case STEP_1_8:
        level0 = IOPORT_LEVEL_HIGH;
        level1 = IOPORT_LEVEL_LOW;
        level2 = IOPORT_LEVEL_HIGH;
        break;
    case STEP_1_16:
        level0 = IOPORT_LEVEL_HIGH;
        level1 = IOPORT_LEVEL_HIGH;
        level2 = IOPORT_LEVEL_LOW;
        break;
    case STEP_1_32:
        level0 = IOPORT_LEVEL_HIGH;
        level1 = IOPORT_LEVEL_HIGH;
        level2 = IOPORT_LEVEL_HIGH;
        break;
    default:    // 1/16°
        level0 = IOPORT_LEVEL_HIGH;
        level1 = IOPORT_LEVEL_HIGH;
        level2 = IOPORT_LEVEL_LOW;
        break;
    }

    g_ioport.p_api->pinWrite(pin_stepper_dmode0[stepper], level0);
    g_ioport.p_api->pinWrite(pin_stepper_dmode1[stepper], level1);
    g_ioport.p_api->pinWrite(pin_stepper_dmode2[stepper], level2);

    __asm("nop");
    g_ioport.p_api->pinWrite(pin_stepper_reset[stepper], IOPORT_LEVEL_HIGH);
    __asm("nop");
    __asm("nop");
    __asm("nop");
    __asm("nop");
    g_ioport.p_api->pinWrite(pin_stepper_reset[stepper], IOPORT_LEVEL_LOW);

}







