


//*********************************************
//  valori di default dipendenti dal cliente
//*********************************************
void inizializza_valori_eeprom(void)
{

    ee_FilterConfig.TipoMotore = MOTORE_FILTRO_NEW;
    ee_FilterConfig.uwSteps = K_FILTER_STEPS_NEW;
    ee_FilterConfig.StepsResetMin = FILTER_RESET_MIN_NEW;
    ee_FilterConfig.StepsResetMax = FILTER_RESET_MAX_NEW;
    ee_FilterConfig.uwType = FILTER_5_HOLES;
    ee_ConfigBoards[STEPPER_C].uwFMin = 500;
    ee_ConfigBoards[STEPPER_C].uwFMax = 10000;
    ee_ConfigBoards[STEPPER_FILTER].ucStep = 0x06; //Step: 1/4
    ee_ConfigColl.ucFilter = TRUE;
    ee_ConfigColl.TastieraVilla = FALSE;
    ee_ConfigColl.ucAutoLight = AUTOLIGHT_YES;
    ee_ConfigColl.uwMinDff = 50;
    ee_ConfigColl.uwMaxDff = 250;

    ee_ConfigColl.ucIrisPresent = IRIDE_YES;
    ee_ConfigColl.ucIrisEnabled = TRUE;


    ee_ConfigBoards[STEPPER_A].uwMaxSteps = DEF_PASSI_A_IRIDE; // passi massimi da tutto chiuso a tutto aperto
    ee_ConfigBoards[STEPPER_A].uwRefSize = DEF_SIZE_A_IRIDE; // formato di riferimento in mm
    ee_ConfigBoards[STEPPER_A].uwRefSteps = DEF_PASSI_A_IRIDE; // formato di riferimento in passi
    ee_WindowConfig[WINDOW_2].lPosStepperA = DEF_PASSI_A_IRIDE;
    g_MaxSteps[STEPPER_CROSS] = DEF_PASSI_A_IRIDE;
    g_RefSize[STEPPER_CROSS] = DEF_SIZE_A_IRIDE;
    g_RefSteps[STEPPER_CROSS] = DEF_PASSI_A_IRIDE;
    ee_ConfigBoards[STEPPER_B].uwMaxSteps = DEF_PASSI_B_IRIDE; // passi massimi da tutto chiuso a tutto aperto
    ee_ConfigBoards[STEPPER_B].uwRefSize = DEF_SIZE_B_IRIDE; // formato di riferimento in mm
    ee_ConfigBoards[STEPPER_B].uwRefSteps = DEF_PASSI_B_IRIDE; // formato di riferimento in passi
    ee_WindowConfig[WINDOW_2].lPosStepperB = DEF_PASSI_B_IRIDE;
    g_MaxSteps[STEPPER_LONG] = DEF_PASSI_B_IRIDE;
    g_RefSize[STEPPER_LONG] = DEF_SIZE_B_IRIDE;
    g_RefSteps[STEPPER_LONG] = DEF_PASSI_B_IRIDE;


}




